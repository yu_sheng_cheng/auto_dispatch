﻿using Industry4._0.Modbus.RTU;
using Industry4._0.Transports;
using SwissKnife;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ModbusExample
{
    class Program
    {
        static void Main(string[] args)
        {
            ModbusRTUMaster modbus = new ModbusRTUMaster();
            ISerialPortTransport serialPort = modbus.SerialPortTransport;
            serialPort.PortName = "COM6";
            serialPort.BaudRate = 9600;
            serialPort.DataBits = 8;
            serialPort.Parity = Parity.None;
            serialPort.StopBits = StopBits.One;

            Task.Run(async () =>
            {
                ArrayBuilder<bool> contacts = new ArrayBuilder<bool>();

                OperationResult result = await modbus.ReadBoolAsync("0", 9, contacts);
                if (!result.IsSuccess)
                {
                    Console.WriteLine("{0}", result);
                    return;
                }

                int index = 0;
                foreach (var b in contacts)
                {
                    Console.WriteLine("X{0}: {1}", index++, b ? "ON" : "OFF");
                }

                ArrayBuilder<bool> coils = new ArrayBuilder<bool>();
                for (int i = 0; i < 8; ++i)
                {
                    coils[i] = (i & 0x01) == 0;
                }

                result = await modbus.WriteAsync("0", coils);
                if (!result.IsSuccess)
                {
                    Console.WriteLine("{0}", result);
                    return;
                }

                Thread.Sleep(5000);

                for (int i = 0; i < 8; ++i)
                {
                    coils[i] = (i & 0x01) == 1;
                }

                result = await modbus.WriteAsync("0", coils);
                if (!result.IsSuccess)
                {
                    Console.WriteLine("{0}", result);
                    return;
                }
            });

            Console.ReadLine();
        }
    }
}
