﻿using SwissKnife;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Industry4._0.IoT
{
    public interface IThingMessage
    {
        bool IsEndsWith { get; }

        string EndString { get; }

        int HeaderLength { get; }

        int GetContentLengthFromHeader(ArrayBuilder<byte> receiveBuffer, ArrayBuilder<byte> sendBuffer);

        bool ValidateHeader(ArrayBuilder<byte> receiveBuffer, ArrayBuilder<byte> sendBuffer);

        bool Validate(ArrayBuilder<byte> receiveBuffer, ArrayBuilder<byte> sendBuffer); 
    }
}
