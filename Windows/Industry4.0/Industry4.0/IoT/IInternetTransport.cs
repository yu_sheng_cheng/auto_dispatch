﻿using Industry4._0.Common;
using Industry4._0.Transports;
using Net.AsyncSocket.Tcp;
using SwissKnife;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Industry4._0.IoT
{
    public interface IInternetTransport : ITransport
    {
        bool IsPersistentConnection { get; set; }

        IPEndPoint RemoteEndPoint { get; set; }

        int ReceiveBufferSize { get; set; }

        int SendBufferSize { get; set; }

        int KeepAliveTime { get; set; }

        int KeepAliveInterval { get; set; }

        Func<AsyncTcpClient, Task<OperationResult>> Connected { get; set; }

        Task<OperationResult> SendThenReceiveAsync(AsyncTcpClient session, SendReceiveBuffer sendReceiveBuffer);
    }
}
