﻿using SwissKnife;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Industry4._0.IoT
{
    public interface IThing
    {
        Task<OperationResult<bool>> ReadBoolAsync(string address);
        Task<OperationResult> ReadBoolAsync(string address, ushort length, ArrayBuilder<bool> buffer);
        Task<OperationResult<sbyte>> ReadInt8Async(string address);
        Task<OperationResult> ReadInt8Async(string address, ushort count, ArrayBuilder<sbyte> buffer);
        Task<OperationResult<byte>> ReadUInt8Async(string address);
        Task<OperationResult> ReadUInt8Async(string address, ushort count, ArrayBuilder<byte> buffer);
        Task<OperationResult<short>> ReadInt16Async(string address);
        Task<OperationResult> ReadInt16Async(string address, ushort count, ArrayBuilder<short> buffer);
        Task<OperationResult<ushort>> ReadUInt16Async(string address);
        Task<OperationResult> ReadUInt16Async(string address, ushort count, ArrayBuilder<ushort> buffer);
        Task<OperationResult<int>> ReadInt32Async(string address);
        Task<OperationResult> ReadInt32Async(string address, ushort count, ArrayBuilder<int> buffer);
        Task<OperationResult<uint>> ReadUInt32Async(string address);
        Task<OperationResult> ReadUInt32Async(string address, ushort count, ArrayBuilder<uint> buffer);
        Task<OperationResult<long>> ReadInt64Async(string address);
        Task<OperationResult> ReadInt64Async(string address, ushort count, ArrayBuilder<long> buffer);
        Task<OperationResult<ulong>> ReadUInt64Async(string address);
        Task<OperationResult> ReadUInt64Async(string address, ushort count, ArrayBuilder<ulong> buffer);
        Task<OperationResult<float>> ReadFloatAsync(string address);
        Task<OperationResult> ReadFloatAsync(string address, ushort count, ArrayBuilder<float> buffer);
        Task<OperationResult<double>> ReadDoubleAsync(string address);
        Task<OperationResult> ReadDoubleAsync(string address, ushort count, ArrayBuilder<double> buffer);
        Task<OperationResult<string>> ReadStringAsync(string address, ushort length, Encoding encoding);

        Task<OperationResult> WriteAsync(string address, bool value);
        Task<OperationResult> WriteAsync(string address, ArrayBuilder<bool> values);
        Task<OperationResult> WriteAsync(string address, sbyte value);
        Task<OperationResult> WriteAsync(string address, ArrayBuilder<sbyte> values);
        Task<OperationResult> WriteAsync(string address, byte value);
        Task<OperationResult> WriteAsync(string address, ArrayBuilder<byte> values);
        Task<OperationResult> WriteAsync(string address, short value);
        Task<OperationResult> WriteAsync(string address, ArrayBuilder<short> values);
        Task<OperationResult> WriteAsync(string address, ushort value);
        Task<OperationResult> WriteAsync(string address, ArrayBuilder<ushort> values);
        Task<OperationResult> WriteAsync(string address, int value);
        Task<OperationResult> WriteAsync(string address, ArrayBuilder<int> values);
        Task<OperationResult> WriteAsync(string address, uint value);
        Task<OperationResult> WriteAsync(string address, ArrayBuilder<uint> values);
        Task<OperationResult> WriteAsync(string address, long value);
        Task<OperationResult> WriteAsync(string address, ArrayBuilder<long> values);
        Task<OperationResult> WriteAsync(string address, ulong value);
        Task<OperationResult> WriteAsync(string address, ArrayBuilder<ulong> values);
        Task<OperationResult> WriteAsync(string address, float value);
        Task<OperationResult> WriteAsync(string address, ArrayBuilder<float> values);
        Task<OperationResult> WriteAsync(string address, double value);
        Task<OperationResult> WriteAsync(string address, ArrayBuilder<double> values);
        Task<OperationResult> WriteStringAsync(string address, string value, Encoding encoding);
    }
}
