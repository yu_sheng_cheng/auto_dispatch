﻿using Industry4._0.Common;
using Industry4._0.Transports;
using Net;
using Net.AsyncSocket.Tcp;
using SwissKnife;
using SwissKnife.Log;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Industry4._0.IoT
{
    public sealed class InternetTransport<T> : InternetTransport where T : IThingMessage, new()
    {
        public InternetTransport() : base(new T())
        {

        }
    }

    public class InternetTransport : TransportAbstract, IInternetTransport
    {
        private const int Free = 0;
        private const int GetConnection = 1;
        private const int InProgress = 2;
        private const int Disposed = 3;

        private readonly TcpSocketConfiguration _tcpConfiguration = new TcpSocketConfiguration();
        private AsyncTcpClient _session;
        private IPEndPoint _remoteEndPoint;
        private int _status;
        private readonly AsynchronousTimeoutSignal _timeoutSignal;
        private AsyncTcpClient _connectingClient;

        public sealed override AsynchronousTimeoutSignal TimeoutSignal { get { return _timeoutSignal; } }

        public bool IsPersistentConnection { get; set; } = true;

        public IPEndPoint RemoteEndPoint
        {
            get { return _remoteEndPoint; }
            set { Volatile.Write(ref _remoteEndPoint, value); }
        }

        public int ReceiveBufferSize { get; set; } = 8 << 10;

        public int SendBufferSize { get; set; } = 8 << 10;

        public int KeepAliveTime { get; set; } = 10000;

        public int KeepAliveInterval { get; set; } = 1000;

        public Func<AsyncTcpClient, Task<OperationResult>> Connected { get; set; }

        public InternetTransport(IThingMessage protocol) : base(protocol)
        {
            _timeoutSignal = new AsynchronousTimeoutSignal((o) =>
            {
                AsyncTcpClient session = o as AsyncTcpClient;
                ThreadPool.QueueUserWorkItem((_) => session.Dispose());
            });
        }

        public sealed override void Dispose()
        {
            if (Interlocked.Exchange(ref _status, Disposed) == Disposed)
                return;

            AsyncTcpClient connectingClient = Volatile.Read(ref _connectingClient);
            if (connectingClient != null)
                connectingClient.Dispose();

            AsyncTcpClient session = Volatile.Read(ref _session);
            if (session != null)
                session.Dispose();
        }

        public sealed override async Task<OperationResult> SendThenReceiveAsync(SendReceiveBuffer sendReceiveBuffer)
        {
            if (sendReceiveBuffer == null)
                throw new ArgumentNullException("sendReceiveBuffer");
            if (sendReceiveBuffer.Send.Length == 0)
                throw new ArgumentException("Send buffer has no data in it.", "sendReceiveBuffer");

            IPEndPoint remoteEndPoint = _remoteEndPoint;

            if (remoteEndPoint == null)
                throw new InvalidOperationException("RemoteEndPoint property was not set before calling this method.");

            switch (Interlocked.CompareExchange(ref _status, GetConnection, Free))
            {
                case GetConnection:
                case InProgress:
                    throw new InvalidOperationException("Previous transport operation is still in progress.");
                case Disposed:
                    return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.ObjectDisposed, SR.ObjectDisposed);
            }

            Func<AsyncTcpClient, Task<OperationResult>> onConnected = null;

            if (_session != null)
            {
                if (!_session.RemoteEndPoint.Equals(remoteEndPoint))
                {
                    _session.Dispose();
                    _session = null;
                }
            }

            if (_session == null)
            {
                OperationResult<AsyncTcpClient> connectResult = await CreateSessionAsync(remoteEndPoint).ConfigureAwait(false);
                if (!connectResult.IsSuccess)
                {
                    Interlocked.CompareExchange(ref _status, Free, GetConnection);
                    return connectResult;
                }
                _session = connectResult.Value;
                onConnected = Connected;
            }

            if (Interlocked.CompareExchange(ref _status, InProgress, GetConnection) == Disposed)
            {
                _session.Dispose();
                _session = null;
                return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.ObjectDisposed, SR.ObjectDisposed);
            }

            OperationResult replyResult;
            bool timeout;

            _timeoutSignal.Register(_session, out timeout);

            if (timeout)
            {
                _timeoutSignal.UnRegister(out timeout);
                Log.w(LogKeyword, "SendThenReceiveAsync: Communication timeout after getting connection");
                replyResult = OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.Timeout, SR.CommunicationTimeout);
            }
            else
            {
                if (onConnected != null)
                {
                    replyResult = await onConnected(_session).ConfigureAwait(false);
                    if (replyResult.IsSuccess)
                        replyResult = await SendThenReceiveAsync(new Transceiver(_session), sendReceiveBuffer).ConfigureAwait(false);
                }
                else
                    replyResult = await SendThenReceiveAsync(new Transceiver(_session), sendReceiveBuffer).ConfigureAwait(false);

                _timeoutSignal.UnRegister(out timeout);
                if (timeout)
                {
                    Log.w(LogKeyword, "SendThenReceiveAsync: Communication timeout");

                    if (IsPersistentConnection && replyResult.IsSuccess)
                        replyResult = OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.Timeout, SR.CommunicationTimeout);
                }
            }

            if (!IsPersistentConnection | !replyResult.IsSuccess)
            {
                _session.Dispose();
                _session = null;
            }

            Interlocked.CompareExchange(ref _status, Free, InProgress);
            return replyResult;
        }

        private async Task<OperationResult<AsyncTcpClient>> CreateSessionAsync(IPEndPoint remoteEndPoint)
        {
            AsyncTcpClient client = null;
            bool success = false;

            try
            {
                client = new AsyncTcpClient(remoteEndPoint);

                TcpSocketConfiguration configuration = client.Configuration;

                configuration.ReceiveBufferSize = ReceiveBufferSize;
                configuration.SendBufferSize = SendBufferSize;
                configuration.IsKeepAliveEnabled = true;
                configuration.KeepAliveTime = KeepAliveTime;
                configuration.KeepAliveInterval = KeepAliveInterval;

                Volatile.Write(ref _connectingClient, client);
                if (Interlocked.CompareExchange(ref _status, GetConnection, GetConnection) == Disposed)
                    return OperationResultFactory.CreateFailedResult<AsyncTcpClient>(GetType().FullName, (int)Industry4Dot0Error.ObjectDisposed, SR.ObjectDisposed);

                if (!await ConnectAsync(client))
                    return OperationResultFactory.CreateFailedResult<AsyncTcpClient>(GetType().FullName, (int)Industry4Dot0Error.Timeout, SR.CommunicationTimeout);

                OperationResult<AsyncTcpClient> result = OperationResultFactory.CreateSuccessfulResult(client, SR.ConnectingSuccess);
                success = true;
                return result;
            }
            catch (Exception e)
            {
                if (!(e is ObjectDisposedException))
                    Log.e(LogKeyword, "CreateSessionAsync:", e);
                return OperationResultFactory.CreateFailedResult<AsyncTcpClient>(GetType().FullName, SR.ConnectingFailed, e);
            }
            finally
            {
                _connectingClient = null;
                if (!success && client != null)
                    client.Dispose();
            }
        }

        private async Task<bool> ConnectAsync(AsyncTcpClient client)
        {
            bool timeout;

            _timeoutSignal.Register(client, out timeout);
            if (timeout)
            {
                _timeoutSignal.UnRegister(out timeout);
                return false;
            }

            try
            {
                await client.ConnectAsync().ConfigureAwait(false);
                _timeoutSignal.UnRegister(out timeout);
                return !timeout;
            }
            catch
            {
                _timeoutSignal.UnRegister(out timeout);
                if (timeout)
                    Log.w(LogKeyword, "ConnectAsync: Timeout");
                throw;
            }
        }

        public Task<OperationResult> SendThenReceiveAsync(AsyncTcpClient session, SendReceiveBuffer sendReceiveBuffer)
        {
            if (session == null)
                throw new ArgumentNullException("session");
            if (sendReceiveBuffer == null)
                throw new ArgumentNullException("sendReceiveBuffer");
            if (sendReceiveBuffer.Send.Length == 0)
                throw new ArgumentException("Send buffer has no data in it.", "sendReceiveBuffer");

            return SendThenReceiveAsync(new Transceiver(session), sendReceiveBuffer);
        }

        protected override Task SendAsync(Transceiver transceiver, byte[] data, int offset, int count)
        {
            AsyncTcpClient session = transceiver.Instance as AsyncTcpClient;
            return session.SendAsync(data, offset, count);
        }

        protected override Task<int> ReceiveAsync(Transceiver transceiver, byte[] buffer, int offset, int size)
        {
            AsyncTcpClient session = transceiver.Instance as AsyncTcpClient;
            return session.ReceiveAsync(buffer, offset, size);
        }
    }
}
