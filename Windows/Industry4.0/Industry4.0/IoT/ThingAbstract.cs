﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Industry4._0.Common;
using Industry4._0.Transports;
using SwissKnife;
using SwissKnife.Conversion;
using SwissKnife.Log;
using SwissKnife.Threading;

namespace Industry4._0.IoT
{
    public abstract class ThingAbstract<T1, T2> : ThingAbstract<T2> where T1 : ITransport, new() where T2 : IByteConverter, new()
    {
        public ThingAbstract()
        {
            Transport = new T1();
        }
    }

    public abstract class ThingAbstract<T> : IThing, IDisposable where T : IByteConverter, new()
    {
        private string _logKeyword;
        private byte _bytesPerWord;
        private ITransport _transport;
        private readonly SendReceiveBuffer _sendReceiveBuffer = new SendReceiveBuffer();
        private ArrayBuilder<bool> _boolArray = new ArrayBuilder<bool>();
        private ArrayBuilder<sbyte> _sbyteArray = new ArrayBuilder<sbyte>();
        private ArrayBuilder<byte> _byteArray = new ArrayBuilder<byte>();
        private ArrayBuilder<short> _shortArray = new ArrayBuilder<short>();
        private ArrayBuilder<ushort> _ushortArray = new ArrayBuilder<ushort>();
        private ArrayBuilder<int> _intArray = new ArrayBuilder<int>();
        private ArrayBuilder<uint> _uintArray = new ArrayBuilder<uint>();
        private ArrayBuilder<long> _longArray = new ArrayBuilder<long>();
        private ArrayBuilder<ulong> _ulongArray = new ArrayBuilder<ulong>();
        private ArrayBuilder<float> _floatArray = new ArrayBuilder<float>();
        private ArrayBuilder<double> _doubleArray = new ArrayBuilder<double>();
        private ArrayBuilder<byte> _buffer = new ArrayBuilder<byte>();
        private bool disposed;

        private readonly Action<byte[], int, int, short[], int> ToInt16;
        private readonly Action<byte[], int, int, ushort[], int> ToUInt16;
        private readonly Action<byte[], int, int, int[], int> ToInt32;
        private readonly Action<byte[], int, int, uint[], int> ToUInt32;
        private readonly Action<byte[], int, int, long[], int> ToInt64;
        private readonly Action<byte[], int, int, ulong[], int> ToUInt64;
        private readonly Action<byte[], int, int, float[], int> ToSingle;
        private readonly Action<byte[], int, int, double[], int> ToDouble;

        private readonly Action<short[], int, int, byte[], int> GetInt16Bytes;
        private readonly Action<ushort[], int, int, byte[], int> GetUInt16Bytes;
        private readonly Action<int[], int, int, byte[], int> GetInt32Bytes;
        private readonly Action<uint[], int, int, byte[], int> GetUInt32Bytes;
        private readonly Action<long[], int, int, byte[], int> GetInt64Bytes;
        private readonly Action<ulong[], int, int, byte[], int> GetUInt64Bytes;
        private readonly Action<float[], int, int, byte[], int> GetSingleBytes;
        private readonly Action<double[], int, int, byte[], int> GetDoubleBytes;

        protected ILog Log { get; }

        public string LogKeyword
        {
            get { return _logKeyword; }
            set
            {
                _logKeyword = value;
                _transport.LogKeyword = value;
            }
        }

        protected ITransport Transport
        {
            get { return _transport; }
            set
            {
                if (_transport != null)
                    throw new InvalidOperationException("This operation is only allowed once per object.");
                _transport = value ?? throw new ArgumentNullException("Transport");
            }
        }

        public AsynchronousTimeoutSignal TimeoutSignal { get { return _transport.TimeoutSignal; } }

        public byte BytesPerWord
        {
            get { return _bytesPerWord; }
            protected set
            {
                if (value != 1 && value != 2)
                    throw new ArgumentException("The 'BytesPerWord' property must be equal to one or two.", "BytesPerWord");

                _bytesPerWord = value;
                ByteOrderPerWord = (value == 2) ? (byte)1 : (byte)0;
            }
        }

        public byte ByteOrderPerWord { get; private set; }

        public IByteConverter ByteConverter { get; } = new T();

        protected bool NeedsTrailingZeroAtEndOfString { get; set; } = true;

        public ThingAbstract() : this(2) { }

        protected ThingAbstract(byte bytesPerWord)
        {
            Log = Industry4Dot0Environment.Log;

            BytesPerWord = bytesPerWord;

            ToInt16 = ByteConverter.ToInt16;
            ToUInt16 = ByteConverter.ToUInt16;
            ToInt32 = ByteConverter.ToInt32;
            ToUInt32 = ByteConverter.ToUInt32;
            ToInt64 = ByteConverter.ToInt64;
            ToUInt64 = ByteConverter.ToUInt64;
            ToSingle = ByteConverter.ToSingle;
            ToDouble = ByteConverter.ToDouble;

            GetInt16Bytes = ByteConverter.GetBytes;
            GetUInt16Bytes = ByteConverter.GetBytes;
            GetInt32Bytes = ByteConverter.GetBytes;
            GetUInt32Bytes = ByteConverter.GetBytes;
            GetInt64Bytes = ByteConverter.GetBytes;
            GetUInt64Bytes = ByteConverter.GetBytes;
            GetSingleBytes = ByteConverter.GetBytes;
            GetDoubleBytes = ByteConverter.GetBytes;
        }

        public virtual void Dispose()
        {
            if (disposed)
                return;
            _transport.Dispose();
            disposed = true;
        }

        public async Task<OperationResult<bool>> ReadBoolAsync(string address)
        {
            OperationResult result = await ReadBoolAsync(address, 1, _boolArray).ConfigureAwait(false);
            return result.IsSuccess ? OperationResultFactory.CreateSuccessfulResult(_boolArray[0], SR.Success) : OperationResultFactory.CreateFailedResult<bool>(result);
        }

        public async Task<OperationResult> ReadBoolAsync(string address, ushort length, ArrayBuilder<bool> buffer)
        {
            if (disposed)
                return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.ObjectDisposed, SR.ObjectDisposed);

            if (address == null)
                throw new ArgumentNullException("address");
            if (length == 0)
                throw new ArgumentException("The length argument must be greater than zero.", "length");
            if (buffer == null)
                throw new ArgumentNullException("buffer");

            _sendReceiveBuffer.Clear();
            buffer.Clear();

            OperationResult result = await ReadBitsAsync(address, length, _sendReceiveBuffer, buffer);

            if (result.IsSuccess)
            {
                if (buffer.Length != length)
                    return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.UnexpectedLengthWasReceived, SR.UnexpectedLengthWasReceived);
            }
            return result;
        }

        protected virtual Task<OperationResult> ReadBitsAsync(string address, ushort length, SendReceiveBuffer sendReceiveBuffer, ArrayBuilder<bool> buffer)
        {
            return Task.FromResult(OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.FunctionNotSupported, SR.FunctionNotSupported));
        }

        public async Task<OperationResult<sbyte>> ReadInt8Async(string address)
        {
            if (_bytesPerWord != 1)
                return OperationResultFactory.CreateFailedResult<sbyte>(GetType().FullName, (int)Industry4Dot0Error.FunctionNotSupported, SR.FunctionNotSupported);

            OperationResult result = await ReadWords(address, 1, _sbyteArray, null, 1);
            return result.IsSuccess ? OperationResultFactory.CreateSuccessfulResult(_sbyteArray[0], SR.Success) : OperationResultFactory.CreateFailedResult<sbyte>(result);
        }

        public Task<OperationResult> ReadInt8Async(string address, ushort count, ArrayBuilder<sbyte> buffer)
        {
            if (!IsAlignedOnWordBoundary(count))
            {
                string message = string.Format(SR.LengthMustBeAMultipleOfN, _bytesPerWord.ToString());
                return Task.FromResult(OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.InvalidArgument, message));
            }
            return ReadWords(address, GetWords(count), buffer, null, 1);
        }

        public async Task<OperationResult<byte>> ReadUInt8Async(string address)
        {
            if (_bytesPerWord != 1)
                return OperationResultFactory.CreateFailedResult<byte>(GetType().FullName, (int)Industry4Dot0Error.FunctionNotSupported, SR.FunctionNotSupported);

            OperationResult result = await ReadWords(address, 1, _byteArray, null, 1);
            return result.IsSuccess ? OperationResultFactory.CreateSuccessfulResult(_byteArray[0], SR.Success) : OperationResultFactory.CreateFailedResult<byte>(result);
        }

        public Task<OperationResult> ReadUInt8Async(string address, ushort count, ArrayBuilder<byte> buffer)
        {
            if (!IsAlignedOnWordBoundary(count))
            {
                string message = string.Format(SR.LengthMustBeAMultipleOfN, _bytesPerWord.ToString());
                return Task.FromResult(OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.InvalidArgument, message));
            }
            return ReadWords(address, GetWords(count), buffer, null, 1);
        }

        public async Task<OperationResult<short>> ReadInt16Async(string address)
        {
            OperationResult result = await ReadWords(address, GetWords(2), _shortArray, ToInt16, 2);
            return result.IsSuccess ? OperationResultFactory.CreateSuccessfulResult(_shortArray[0], SR.Success) : OperationResultFactory.CreateFailedResult<short>(result);
        }

        public Task<OperationResult> ReadInt16Async(string address, ushort count, ArrayBuilder<short> buffer)
        {
            return ReadWords(address, GetWords(2 * count), buffer, ToInt16, 2);
        }

        public async Task<OperationResult<ushort>> ReadUInt16Async(string address)
        {
            OperationResult result = await ReadWords(address, GetWords(2), _ushortArray, ToUInt16, 2);
            return result.IsSuccess ? OperationResultFactory.CreateSuccessfulResult(_ushortArray[0], SR.Success) : OperationResultFactory.CreateFailedResult<ushort>(result);
        }

        public Task<OperationResult> ReadUInt16Async(string address, ushort count, ArrayBuilder<ushort> buffer)
        {
            return ReadWords(address, GetWords(2 * count), buffer, ToUInt16, 2);
        }

        public async Task<OperationResult<int>> ReadInt32Async(string address)
        {
            OperationResult result = await ReadWords(address, GetWords(4), _intArray, ToInt32, 4);
            return result.IsSuccess ? OperationResultFactory.CreateSuccessfulResult(_intArray[0], SR.Success) : OperationResultFactory.CreateFailedResult<int>(result);
        }

        public Task<OperationResult> ReadInt32Async(string address, ushort count, ArrayBuilder<int> buffer)
        {
            return ReadWords(address, GetWords(4 * count), buffer, ToInt32, 4);
        }

        public async Task<OperationResult<uint>> ReadUInt32Async(string address)
        {
            OperationResult result = await ReadWords(address, GetWords(4), _uintArray, ToUInt32, 4);
            return result.IsSuccess ? OperationResultFactory.CreateSuccessfulResult(_uintArray[0], SR.Success) : OperationResultFactory.CreateFailedResult<uint>(result);
        }

        public Task<OperationResult> ReadUInt32Async(string address, ushort count, ArrayBuilder<uint> buffer)
        {
            return ReadWords(address, GetWords(4 * count), buffer, ToUInt32, 4);
        }

        public async Task<OperationResult<long>> ReadInt64Async(string address)
        {
            OperationResult result = await ReadWords(address, GetWords(8), _longArray, ToInt64, 8);
            return result.IsSuccess ? OperationResultFactory.CreateSuccessfulResult(_longArray[0], SR.Success) : OperationResultFactory.CreateFailedResult<long>(result);
        }

        public Task<OperationResult> ReadInt64Async(string address, ushort count, ArrayBuilder<long> buffer)
        {
            return ReadWords(address, GetWords(8 * count), buffer, ToInt64, 8);
        }

        public async Task<OperationResult<ulong>> ReadUInt64Async(string address)
        {
            OperationResult result = await ReadWords(address, GetWords(8), _ulongArray, ToUInt64, 8);
            return result.IsSuccess ? OperationResultFactory.CreateSuccessfulResult(_ulongArray[0], SR.Success) : OperationResultFactory.CreateFailedResult<ulong>(result);
        }

        public Task<OperationResult> ReadUInt64Async(string address, ushort count, ArrayBuilder<ulong> buffer)
        {
            return ReadWords(address, GetWords(8 * count), buffer, ToUInt64, 8);
        }

        public async Task<OperationResult<float>> ReadFloatAsync(string address)
        {
            OperationResult result = await ReadWords(address, GetWords(4), _floatArray, ToSingle, 4);
            return result.IsSuccess ? OperationResultFactory.CreateSuccessfulResult(_floatArray[0], SR.Success) : OperationResultFactory.CreateFailedResult<float>(result);
        }

        public Task<OperationResult> ReadFloatAsync(string address, ushort count, ArrayBuilder<float> buffer)
        {
            return ReadWords(address, GetWords(4 * count), buffer, ToSingle, 4);
        }

        public async Task<OperationResult<double>> ReadDoubleAsync(string address)
        {
            OperationResult result = await ReadWords(address, GetWords(8), _doubleArray, ToDouble, 8);
            return result.IsSuccess ? OperationResultFactory.CreateSuccessfulResult(_doubleArray[0], SR.Success) : OperationResultFactory.CreateFailedResult<double>(result);
        }

        public Task<OperationResult> ReadDoubleAsync(string address, ushort count, ArrayBuilder<double> buffer)
        {
            return ReadWords(address, GetWords(8 * count), buffer, ToDouble, 8);
        }

        public async Task<OperationResult<string>> ReadStringAsync(string address, ushort length, Encoding encoding)
        {
            if (encoding == null)
                throw new ArgumentNullException("encoding");

            OperationResult result = await ReadWords(address, length, _byteArray, null, 1);

            if (!result.IsSuccess)
                return OperationResultFactory.CreateFailedResult<string>(result);

            string value = ByteConverter.GetString(_byteArray.Buffer, 0, _byteArray.Length, encoding);

            return OperationResultFactory.CreateSuccessfulResult(value, SR.Success);
        }

        private async Task<OperationResult> ReadWords<T1>(string address, int length, ArrayBuilder<T1> buffer, Action<byte[], int, int, T1[], int> convert, int bytesToConvert)
        {
            if (disposed)
                return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.ObjectDisposed, SR.ObjectDisposed);

            if (address == null)
                throw new ArgumentNullException("address");
            if (length > ushort.MaxValue | length == 0)
                throw new ArgumentOutOfRangeException("length");
            if (buffer == null)
                throw new ArgumentNullException("buffer");

            _sendReceiveBuffer.Clear();

            OperationResult<int, int> result = await ReadWords(address, (ushort)length, _sendReceiveBuffer).ConfigureAwait(false);
            if (!result.IsSuccess)
                return result;

            byte[] receiveBuffer = _sendReceiveBuffer.Receive.Buffer;
            int startIndex = result.Value1;
            int bytes = result.Value2;

            if (bytes != (length << ByteOrderPerWord))
                return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.UnexpectedLengthWasReceived, SR.UnexpectedLengthWasReceived);

            buffer.Clear();

            if (convert == null)
            {
#if DEBUG
                if (bytesToConvert != 1)
                    throw new BugException("The value of bytesToConvert must be equal to 1 when the value of convert is equal to null.");
#endif
                buffer.Length = bytes;
                Buffer.BlockCopy(receiveBuffer, startIndex, buffer.Buffer, 0, bytes);
            }
            else
            {
                buffer.Length = bytes / bytesToConvert;
                convert.Invoke(receiveBuffer, startIndex, bytes, buffer.Buffer, 0);
            }

            return OperationResultFactory.CreateSuccessfulResult(SR.Success);
        }

        protected abstract Task<OperationResult<int, int>> ReadWords(string address, ushort length, SendReceiveBuffer sendReceiveBuffer);

        public Task<OperationResult> WriteAsync(string address, bool value)
        {
            _boolArray.Clear();
            _boolArray[0] = value;
            return WriteAsync(address, _boolArray);
        }

        public Task<OperationResult> WriteAsync(string address, ArrayBuilder<bool> values)
        {
            if (disposed)
                return Task.FromResult(OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.ObjectDisposed, SR.ObjectDisposed));

            if (address == null)
                throw new ArgumentNullException("address");
            if (values == null)
                throw new ArgumentNullException("values");

            int length = values.Length;
            if (length > ushort.MaxValue | length == 0)
                throw new ArgumentOutOfRangeException("values");

            _sendReceiveBuffer.Clear();
            return WriteBitsAsync(address, values, _sendReceiveBuffer);
        }

        protected virtual Task<OperationResult> WriteBitsAsync(string address, ArrayBuilder<bool> values, SendReceiveBuffer sendReceiveBuffer)
        {
            return Task.FromResult(OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.FunctionNotSupported, SR.FunctionNotSupported));
        }

        public Task<OperationResult> WriteAsync(string address, sbyte value)
        {
            if (_bytesPerWord != 1)
                return Task.FromResult(OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.FunctionNotSupported, SR.FunctionNotSupported));

            _sbyteArray.Clear();
            _sbyteArray[0] = value;
            return WriteWords(address, _sbyteArray, null, 1);
        }

        public Task<OperationResult> WriteAsync(string address, ArrayBuilder<sbyte> values)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            if (!IsAlignedOnWordBoundary(values.Length))
            {
                string message = string.Format(SR.LengthMustBeAMultipleOfN, _bytesPerWord.ToString());
                return Task.FromResult(OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.InvalidArgument, message));
            }
            return WriteWords(address, values, null, 1);
        }

        public Task<OperationResult> WriteAsync(string address, byte value)
        {
            if (_bytesPerWord != 1)
                return Task.FromResult(OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.FunctionNotSupported, SR.FunctionNotSupported));

            _byteArray.Clear();
            _byteArray[0] = value;
            return WriteWords(address, _byteArray, null, 1);
        }

        public Task<OperationResult> WriteAsync(string address, ArrayBuilder<byte> values)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            if (!IsAlignedOnWordBoundary(values.Length))
            {
                string message = string.Format(SR.LengthMustBeAMultipleOfN, _bytesPerWord.ToString());
                return Task.FromResult(OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.InvalidArgument, message));
            }
            return WriteWords(address, values, null, 1);
        }

        public Task<OperationResult> WriteAsync(string address, short value)
        {
            _shortArray.Clear();
            _shortArray[0] = value;
            return WriteWords(address, _shortArray, GetInt16Bytes, 2);
        }

        public Task<OperationResult> WriteAsync(string address, ArrayBuilder<short> values)
        {
            return WriteWords(address, values, GetInt16Bytes, 2);
        }

        public Task<OperationResult> WriteAsync(string address, ushort value)
        {
            _ushortArray.Clear();
            _ushortArray[0] = value;
            return WriteWords(address, _ushortArray, GetUInt16Bytes, 2);
        }

        public Task<OperationResult> WriteAsync(string address, ArrayBuilder<ushort> values)
        {
            return WriteWords(address, values, GetUInt16Bytes, 2);
        }

        public Task<OperationResult> WriteAsync(string address, int value)
        {
            _intArray.Clear();
            _intArray[0] = value;
            return WriteWords(address, _intArray, GetInt32Bytes, 4);
        }

        public Task<OperationResult> WriteAsync(string address, ArrayBuilder<int> values)
        {
            return WriteWords(address, values, GetInt32Bytes, 4);
        }

        public Task<OperationResult> WriteAsync(string address, uint value)
        {
            _uintArray.Clear();
            _uintArray[0] = value;
            return WriteWords(address, _uintArray, GetUInt32Bytes, 4);
        }

        public Task<OperationResult> WriteAsync(string address, ArrayBuilder<uint> values)
        {
            return WriteWords(address, values, GetUInt32Bytes, 4);
        }

        public Task<OperationResult> WriteAsync(string address, long value)
        {
            _longArray.Clear();
            _longArray[0] = value;
            return WriteWords(address, _longArray, GetInt64Bytes, 8);
        }

        public Task<OperationResult> WriteAsync(string address, ArrayBuilder<long> values)
        {
            return WriteWords(address, values, GetInt64Bytes, 8);
        }

        public Task<OperationResult> WriteAsync(string address, ulong value)
        {
            _ulongArray.Clear();
            _ulongArray[0] = value;
            return WriteWords(address, _ulongArray, GetUInt64Bytes, 8);
        }

        public Task<OperationResult> WriteAsync(string address, ArrayBuilder<ulong> values)
        {
            return WriteWords(address, values, GetUInt64Bytes, 8);
        }

        public Task<OperationResult> WriteAsync(string address, float value)
        {
            _floatArray.Clear();
            _floatArray[0] = value;
            return WriteWords(address, _floatArray, GetSingleBytes, 4);
        }

        public Task<OperationResult> WriteAsync(string address, ArrayBuilder<float> values)
        {
            return WriteWords(address, values, GetSingleBytes, 4);
        }

        public Task<OperationResult> WriteAsync(string address, double value)
        {
            _doubleArray.Clear();
            _doubleArray[0] = value;
            return WriteWords(address, _doubleArray, GetDoubleBytes, 8);
        }

        public Task<OperationResult> WriteAsync(string address, ArrayBuilder<double> values)
        {
            return WriteWords(address, values, GetDoubleBytes, 8);
        }

        public virtual Task<OperationResult> WriteStringAsync(string address, string value, Encoding encoding)
        {
            _byteArray.Clear();
            _byteArray.Append(ByteConverter.GetBytes(value, encoding));

            int bytes = _byteArray.Length;

            if (NeedsTrailingZeroAtEndOfString)
            {
                int zeros = 1;

                if (encoding.CodePage == Encoding.Unicode.CodePage | encoding.CodePage == Encoding.BigEndianUnicode.CodePage)
                    zeros = 2;

                bytes += zeros;
            }

            _byteArray.Length = (bytes + (_bytesPerWord - 1)) & ~(_bytesPerWord - 1);
            return WriteWords(address, _byteArray, null, 1);
        }

        private Task<OperationResult> WriteWords<T1>(string address, ArrayBuilder<T1> values, Action<T1[], int, int, byte[], int> convert, int bytesRequired)
        {
            if (disposed)
                return Task.FromResult(OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.ObjectDisposed, SR.ObjectDisposed));

            if (address == null)
                throw new ArgumentNullException("address");
            if (values == null)
                throw new ArgumentNullException("values");

            int count = values.Length;
            if (count > ((ushort.MaxValue << ByteOrderPerWord) / bytesRequired) | count == 0)
                throw new ArgumentOutOfRangeException("values");

            int bytes = bytesRequired * count;

            _buffer.Clear();
            _buffer.Length = bytes;

            if (convert == null)
            {
#if DEBUG
                if (bytesRequired != 1)
                    throw new BugException("The value of bytesRequired must be equal to 1 when the value of convert is equal to null.");
#endif               
                Buffer.BlockCopy(values.Buffer, 0, _buffer.Buffer, 0, bytes);
            }
            else
                convert.Invoke(values.Buffer, 0, count, _buffer.Buffer, 0);

            _sendReceiveBuffer.Clear();
            return WriteWords(address, (ushort)GetWords(bytes), _buffer, _sendReceiveBuffer);
        }

        protected abstract Task<OperationResult> WriteWords(string address, ushort length, ArrayBuilder<byte> data, SendReceiveBuffer sendReceiveBuffer);

        private bool IsAlignedOnWordBoundary(int bytes)
        {
            return (bytes & (_bytesPerWord - 1)) == 0;
        }

        private int GetWords(int bytes)
        {
            return bytes >> ByteOrderPerWord;
        }
    }
}
