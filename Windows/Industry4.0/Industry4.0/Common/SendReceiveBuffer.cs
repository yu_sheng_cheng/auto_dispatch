﻿using SwissKnife;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Industry4._0.Common
{
    public class SendReceiveBuffer
    {
        public ArrayBuilder<byte> Send { get; } = new ArrayBuilder<byte>();

        public ArrayBuilder<byte> Receive { get; } = new ArrayBuilder<byte>();

        public void Clear()
        {
            Send.Clear();
            Receive.Clear();
        }
    }
}
