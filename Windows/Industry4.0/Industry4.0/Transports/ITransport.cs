﻿using Industry4._0.Common;
using SwissKnife;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Industry4._0.Transports
{
    public interface ITransport : IDisposable
    {
        string LogKeyword { get; set; }

        AsynchronousTimeoutSignal TimeoutSignal { get; }

        Task<OperationResult> SendThenReceiveAsync(SendReceiveBuffer sendReceiveBuffer);
    }
}
