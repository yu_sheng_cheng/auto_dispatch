﻿using Industry4._0.Common;
using Industry4._0.IoT;
using SwissKnife;
using SwissKnife.Log;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Industry4._0.Transports
{
    public abstract class TransportAbstract : ITransport
    {
        protected struct Transceiver
        {
            public object Instance;

            public Transceiver(object instance)
            {
                Instance = instance;
            }
        }

        private readonly IThingMessage _protocol;
        private readonly byte[] _endString;

        protected ILog Log { get; }

        public string LogKeyword { get; set; }

        public abstract AsynchronousTimeoutSignal TimeoutSignal { get; }

        public TransportAbstract(IThingMessage protocol)
        {
            _protocol = protocol ?? throw new ArgumentNullException("protocol");
            if (_protocol.IsEndsWith)
            {
                _endString = Encoding.ASCII.GetBytes(_protocol.EndString);
                if (_endString.Length == 0)
                    throw new ArgumentException("The 'EndString' property cannot be an empty string.", "protocol");
            }

            Log = Industry4Dot0Environment.Log;
        }

        public abstract void Dispose();

        public abstract Task<OperationResult> SendThenReceiveAsync(SendReceiveBuffer sendReceiveBuffer);

        protected async Task<OperationResult> SendThenReceiveAsync(Transceiver transceiver, SendReceiveBuffer sendReceiveBuffer)
        {
            string errorMessage = SR.SendFailure;

            try
            {
                await SendAsync(transceiver, sendReceiveBuffer.Send.Buffer, 0, sendReceiveBuffer.Send.Length).ConfigureAwait(false);

                errorMessage = SR.ReceiveFailure;

                sendReceiveBuffer.Receive.Clear();

                if (_protocol.IsEndsWith)
                {
                    OperationResult result = await ReceiveStringAsync(transceiver, sendReceiveBuffer.Receive).ConfigureAwait(false);
                    if (!result.IsSuccess)
                        return result;
                }
                else
                {
                    await ReceiveMessageAsync(transceiver, sendReceiveBuffer.Receive, _protocol.HeaderLength).ConfigureAwait(false);

                    if (!_protocol.ValidateHeader(sendReceiveBuffer.Receive, sendReceiveBuffer.Send))
                        return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.MessageHeaderError, SR.MessageHeaderValidatedFailed);

                    int contentLength = _protocol.GetContentLengthFromHeader(sendReceiveBuffer.Receive, sendReceiveBuffer.Send);

                    if (contentLength == 0)
                    {
                        if (sendReceiveBuffer.Receive.Length > _protocol.HeaderLength)
                            return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.MessageContentLengthError, SR.MessageContentLengthValidatedFailed);
                        return OperationResultFactory.CreateSuccessfulResult(SR.Success);
                    }

                    int messageLength = _protocol.HeaderLength + contentLength;

                    await ReceiveMessageAsync(transceiver, sendReceiveBuffer.Receive, messageLength).ConfigureAwait(false);

                    if (sendReceiveBuffer.Receive.Length > messageLength)
                        return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.MessageContentLengthError, SR.MessageContentLengthValidatedFailed);
                }

                if (!_protocol.Validate(sendReceiveBuffer.Receive, sendReceiveBuffer.Send))
                    return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.MessageContentError, SR.MessageContentValidatedFailed);

                return OperationResultFactory.CreateSuccessfulResult(SR.Success);
            }
            catch (Exception e)
            {
                if (!(e is ObjectDisposedException))
                    Log.e(LogKeyword, "SendThenReceiveAsync:", e);
                return OperationResultFactory.CreateFailedResult(GetType().FullName, errorMessage, e);
            }
        }

        private async Task ReceiveMessageAsync(Transceiver transceiver, ArrayBuilder<byte> receiveBuffer, int size)
        {
            int bytesRead = receiveBuffer.Length;

            if (bytesRead >= size)
                return;

            receiveBuffer.EnsureCapacity(size);

            int capacity = receiveBuffer.Capacity;

            while (bytesRead < size)
            {
                bytesRead += await ReceiveAsync(transceiver, receiveBuffer.Buffer, bytesRead, capacity - bytesRead).ConfigureAwait(false);
            }

            receiveBuffer.Length = bytesRead;
        }

        private async Task<OperationResult> ReceiveStringAsync(Transceiver transceiver, ArrayBuilder<byte> receiveBuffer)
        {
            receiveBuffer.EnsureCapacity(64);

            int capacity = receiveBuffer.Capacity;
            int bytesRead = 0, start = 0;
            int endStringLength = _endString.Length;

            while (true)
            {
                bytesRead += await ReceiveAsync(transceiver, receiveBuffer.Buffer, bytesRead, capacity - bytesRead).ConfigureAwait(false);

                if ((bytesRead - start) >= endStringLength)
                {
                    int end = bytesRead - endStringLength;

                    while (start <= end)
                    {
                        bool found = true;

                        for (int i = 0; i < endStringLength; ++i)
                        {
                            if (receiveBuffer[start + i] != _endString[i])
                            {
                                found = false;
                                break;
                            }
                        }

                        if (!found)
                        {
                            ++start;
                            continue;
                        }

                        if (start != end)
                            return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.UnexpectedEndStringWasReceived, SR.UnexpectedEndStringWasReceived);

                        receiveBuffer.Length = bytesRead - endStringLength;
                        return OperationResultFactory.CreateSuccessfulResult();
                    }
                }

                if (bytesRead == capacity)
                {
                    if (capacity >= ushort.MaxValue)
                        return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.MessageLengthLimitExceeded, SR.MessageLengthLimitExceeded);

                    receiveBuffer.EnsureCapacity(capacity * 2);
                    capacity = receiveBuffer.Capacity;
                }
            }
        }

        protected abstract Task SendAsync(Transceiver transceiver, byte[] data, int offset, int count);

        protected abstract Task<int> ReceiveAsync(Transceiver transceiver, byte[] buffer, int offset, int size);
    }
}
