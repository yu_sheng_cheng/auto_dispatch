﻿using Industry4._0.Common;
using Industry4._0.IoT;
using SwissKnife;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Industry4._0.Transports
{
    public sealed class SerialPortTransport<T> : SerialPortTransport where T : IThingMessage, new()
    {
        public SerialPortTransport() : base(new T())
        {

        }
    }

    public class SerialPortTransport : TransportAbstract, ISerialPortTransport
    {
        private const int Free = 0;
        private const int InProgress = 1;
        private const int Disposed = 2;

        private const int MaxTimeout = 3600000;
        private const int MinTimeout = 10;
        private const int DefaultTimeout = 500;

        private readonly SerialPort _serialPort = new SerialPort();
        private int _timeout = DefaultTimeout;
        private int _delayedTimeOfBeforeStartingReading;
        private int _delayedReadingTime;
        private int _elapsedTime;
        private bool _isDiscardExistingBeforeSending;
        private int _status;
        private readonly AsynchronousTimeoutSignal _watchdogExpiredSignal;
        private object _watchdogId = new object();
        private bool _watchdogExpired;

        public string PortName { get; set; } = "COM1";

        public int BaudRate { get; set; } = 9600;

        public int DataBits { get; set; } = 8;

        public Parity Parity { get; set; } = Parity.None;

        public StopBits StopBits { get; set; } = StopBits.One;

        public int DelayedTimeOfBeforeStartingReading
        {
            get { return _delayedTimeOfBeforeStartingReading; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("DelayedTimeOfBeforeStartingReading");
                _delayedTimeOfBeforeStartingReading = value;
            }
        }

        public int Timeout
        {
            get { return _timeout; }
            set
            {
                if (value > MaxTimeout)
                    _timeout = MaxTimeout;
                else if (value < MinTimeout)
                    _timeout = MinTimeout;
                else
                    _timeout = value;
            }
        }

        public int WriteBufferSize { get; set; } = 2048;

        public sealed override AsynchronousTimeoutSignal TimeoutSignal { get { return _watchdogExpiredSignal; } }

        public SerialPortTransport(IThingMessage protocol) : base(protocol)
        {
            _watchdogExpiredSignal = new AsynchronousTimeoutSignal(id =>
            {
                if (Interlocked.CompareExchange(ref _watchdogId, null, id) == id)
                {
                    _watchdogExpired = true;
                    Interlocked.Exchange(ref _watchdogId, new object());
                }
            });
        }

        public sealed override void Dispose()
        {
            if (Interlocked.Exchange(ref _status, Disposed) == Disposed)
                return;
            try { _serialPort.Dispose(); } catch { }
        }

        public sealed override async Task<OperationResult> SendThenReceiveAsync(SendReceiveBuffer sendReceiveBuffer)
        {
            if (sendReceiveBuffer == null)
                throw new ArgumentNullException("sendReceiveBuffer");
            if (sendReceiveBuffer.Send.Length == 0)
                throw new ArgumentException("Send buffer has no data in it.", "sendReceiveBuffer");

            switch (Interlocked.CompareExchange(ref _status, InProgress, Free))
            {
                case InProgress:
                    throw new InvalidOperationException("Previous transport operation is still in progress.");
                case Disposed:
                    return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.ObjectDisposed, SR.ObjectDisposed);
            }

            try
            {
                if (_serialPort.IsOpen)
                {
                    if (NeedsReopen())
                        try { _serialPort.Close(); } catch { }
                }

                if (!_serialPort.IsOpen)
                {
                    _isDiscardExistingBeforeSending = false;

                    OperationResult openResult = OpenSerialPort();
                    if (!openResult.IsSuccess)
                        return openResult;
                    if (Interlocked.CompareExchange(ref _status, InProgress, InProgress) == Disposed)
                    {
                        try { _serialPort.Dispose(); } catch { }
                        return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.ObjectDisposed, SR.ObjectDisposed);
                    }
                }

                OperationResult replyResult;
                bool timeout;

                _elapsedTime = 0;
                _watchdogExpired = false;

                _watchdogExpiredSignal.Register(_watchdogId, out timeout);
                if (timeout)
                {
                    _watchdogExpiredSignal.UnRegister(out timeout);
                    Log.w(LogKeyword, "SendThenReceiveAsync: Communication timeout after opening serial port");
                    replyResult = OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.Timeout, SR.CommunicationTimeout);        
                }
                else
                {
                    replyResult = await SendThenReceiveAsync(new Transceiver(_serialPort), sendReceiveBuffer).ConfigureAwait(false);
                    _watchdogExpiredSignal.UnRegister(out timeout);
                    if (timeout)
                    {
                        if (Interlocked.Exchange(ref _watchdogId, null) == null) // watchdog expired signal is being processed
                        {
                            SpinWait spinwait = new SpinWait();
                            while (Interlocked.CompareExchange(ref _watchdogId, null, null) == null)
                            {
                                spinwait.SpinOnce();
                            }
                        }
                        else
                            Interlocked.Exchange(ref _watchdogId, new object());

                        Log.w(LogKeyword, "SendThenReceiveAsync: Communication timeout");
                    }
                }
                    
                return replyResult;
            }
            finally
            {
                Interlocked.CompareExchange(ref _status, Free, InProgress);
            }
        }

        private bool NeedsReopen()
        {
            return (PortName != _serialPort.PortName | BaudRate != _serialPort.BaudRate) ||
                (DataBits != _serialPort.DataBits | Parity != _serialPort.Parity | StopBits != _serialPort.StopBits | WriteBufferSize != _serialPort.WriteBufferSize);
        }

        private OperationResult OpenSerialPort()
        {
            try
            {
                _serialPort.PortName = PortName;
                _serialPort.BaudRate = BaudRate;
                _serialPort.DataBits = DataBits;
                _serialPort.Parity = Parity;
                _serialPort.StopBits = StopBits;
                _serialPort.WriteBufferSize = WriteBufferSize;
                _serialPort.Open();

                int bitsPerFrame = 1 + _serialPort.DataBits + (_serialPort.Parity == Parity.None ? 0 : 1); // start bit + data bit + parity
                switch (_serialPort.StopBits)
                {
                    case StopBits.One:
                        ++bitsPerFrame;
                        break;
                    case StopBits.OnePointFive:
                    case StopBits.Two:
                        bitsPerFrame += 2;
                        break;
                }
                _delayedReadingTime = 1000 * 4 * bitsPerFrame / BaudRate; // delayed 4 characters (milliseconds)
                if (_delayedReadingTime == 0)
                    _delayedReadingTime = 1;
                return OperationResultFactory.CreateSuccessfulResult(SR.SerialPortOpenSuccess);
            }
            catch (Exception e)
            {
                Log.e(LogKeyword, "SerialPort open:", e);
                return OperationResultFactory.CreateFailedResult(GetType().FullName, SR.SerialPortOpenFailed, e);
            }
        }

        protected sealed override Task SendAsync(Transceiver transceiver, byte[] data, int offset, int count)
        {
            try
            {
                SerialPort serialPort = transceiver.Instance as SerialPort;

                if (_isDiscardExistingBeforeSending)
                {
                    _isDiscardExistingBeforeSending = false;
                    serialPort.DiscardInBuffer();
                }

                serialPort.Write(data, offset, count);
                return Task.CompletedTask;
            }
            catch (InvalidOperationException)
            {
                throw new ObjectDisposedException(GetType().FullName);
            }
        }

        protected sealed override async Task<int> ReceiveAsync(Transceiver transceiver, byte[] buffer, int offset, int size)
        {
            SerialPort serialPort = transceiver.Instance as SerialPort;
            int delayedReadingTime = _delayedReadingTime;

            if (offset == 0) // first
            {
                int delayedTimeOfBeforeStartingReading = Volatile.Read(ref _delayedTimeOfBeforeStartingReading);
                if (delayedTimeOfBeforeStartingReading > 0)
                    delayedReadingTime = delayedTimeOfBeforeStartingReading;
            }

            try
            {
                while (true)
                {
                    if (_elapsedTime >= _timeout || Volatile.Read(ref _watchdogExpired))
                    {
                        _isDiscardExistingBeforeSending = true;
                        throw new TimeoutException();
                    }

                    int millisecondsDelay = (_elapsedTime + delayedReadingTime) <= _timeout ? delayedReadingTime : (_timeout - _elapsedTime);
                    int timeNow = Environment.TickCount;
                    await Task.Delay(millisecondsDelay).ConfigureAwait(false);
                    _elapsedTime += Environment.TickCount - timeNow;

                    if (serialPort.BytesToRead > 0)
                        return serialPort.Read(buffer, offset, size);

                    delayedReadingTime = _delayedReadingTime;
                }
            }
            catch (InvalidOperationException)
            {
                throw new ObjectDisposedException(GetType().FullName);
            }
        }
    }
}
