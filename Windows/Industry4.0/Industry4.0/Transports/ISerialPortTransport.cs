﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Industry4._0.Transports
{
    public interface ISerialPortTransport
    {
        string PortName { get; set; }

        int BaudRate { get; set; }

        int DataBits { get; set; }

        Parity Parity { get; set; }

        StopBits StopBits { get; set; }
    }
}
