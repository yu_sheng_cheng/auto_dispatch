﻿using Industry4._0.IoT;
using SwissKnife;
using SwissKnife.Conversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Industry4._0.Modbus
{
    public abstract class Modbus : IThingMessage
    {
        public const byte MaxPDUSize = 253;

        public const byte ReadCoils = 0x01; // Read Discrete Output Coils
        public const byte ReadContacts = 0x02; // Read Discrete Input Contacts
        public const byte ReadAnalogOutputs = 0x03; // Read Analog Output Holding Registers
        public const byte ReadAnalogInputs = 0x04; // Read Analog Input Registers
        public const byte WriteSingleCoil = 0x05; // Write Single Discrete Output Coil
        public const byte WriteSingleAnalogOutput = 0x06; // Write Single Analog Output Holding Register
        public const byte WriteMultipleCoils = 0x0F; // Write Multiple Discrete Output Coils
        public const byte WriteMultipleAnalogOutputs = 0x10; // Write Multiple Analog Output Holding Registers

        public const byte IllegalFunction = 0x01; // The function code received in the query is not an allowable action for the slave.
                                                  // This may be because the function code is only applicable to newer devices, and was not implemented in the unit selected.
                                                  // It could also indicate that the slave is in the wrong state to process a request of this type,
                                                  // for example because it is unconfigured and is being asked to return register values.
                                                  // If a Poll Program Complete command was issued, this code indicates that no program function preceded it.
        public const byte IllegalDataAddress = 0x02; // The data address received in the query is not an allowable address for the slave. More specifically,
                                                     // the combination of reference number and transfer length is invalid. For a controller with 100 registers,
                                                     // a request with offset 96 and length 4 would succeed, a request with offset 96 and length 5 will generate exception 02.
        public const byte IllegalDataValue = 0x03; // A value contained in the query data field is not an allowable value for the slave.
                                                   // This indicates a fault in the structure of remainder of a complex request, such as that the implied length is incorrect.
                                                   // It specifically does NOT mean that a data item submitted for storage in a register has a value outside the expectation of
                                                   // the application program, since the MODBUS protocol is unaware of the significance of any particular value of any particular register.
        public const byte SlaveDeviceFailure = 0x04; // An unrecoverable error occurred while the slave was attempting to perform the requested action.
        public const byte Acknowledge = 0x05; // Specialized use in conjunction with programming commands.
                                              // The slave has accepted the request and is processing it, but a long duration of time will be required to do so.
                                              // This response is returned to prevent a timeout error from occurring in the master.
                                              // The master can next issue a Poll Program Complete message to determine if processing is completed.
        public const byte SlaveDeviceBusy = 0x06; // Specialized use in conjunction with programming commands.
                                                  // The slave is engaged in processing a long-duration program command.
                                                  // The master should retransmit the message later when the slave is free.
        public const byte NegativeAcknowledge = 0x07; // The slave cannot perform the program function received in the query.
                                                      // This code is returned for an unsuccessful programming request using function code 13 or 14 decimal.
                                                      // The master should request diagnostic or error information from the slave.
        public const byte MemoryParityError = 0x08; // Specialized use in conjunction with function codes 20 and 21 and reference type 6,
                                                    // to indicate that the extended file area failed to pass a consistency check.
                                                    // The slave attempted to read extended memory or record file, but detected a parity error in memory.
                                                    // The master can retry the request, but service may be required on the slave device.
        public const byte GatewayPathUnavailable = 0x0A; // Specialized use in conjunction with gateways, indicates that the gateway was unable to allocate an internal communication path
                                                         // from the input port to the output port for processing the request. Usually means the gateway is misconfigured or overloaded.
        public const byte GatewayTargetDeviceFailedToRespond = 0x0B; // Specialized use in conjunction with gateways, indicates that no response was obtained from the target device.
                                                                     // Usually means that the device is not present on the network.

        public const ushort CoilTrueValue = 0xFF00;
        
        private const string AccessOutOfRange = "The sum of the address and length arguments must be less than or equal to 65536.";
        private const string AccessBytesLimitExceeded = "The access bytes limit was exceeded.";

        public IByteConverter ByteConverter { get; } = new BigEndianConverter();

        public bool IsEndsWith { get { return false; } }

        public string EndString => throw new NotImplementedException();

        public abstract int HeaderLength { get; }

        public void BuildCoilReadCommand(ArrayBuilder<byte> buffer, ushort address, ushort length)
        {
            BuildReadCommand(buffer, ReadCoils, address, length);
        }

        public void BuildContactReadCommand(ArrayBuilder<byte> buffer, ushort address, ushort length)
        {
            BuildReadCommand(buffer, ReadContacts, address, length);
        }

        public void BuildAnalogOutputReadCommand(ArrayBuilder<byte> buffer, ushort address, ushort length)
        {
            BuildReadCommand(buffer, ReadAnalogOutputs, address, length);
        }

        public void BuildAnalogInputReadCommand(ArrayBuilder<byte> buffer, ushort address, ushort length)
        {
            BuildReadCommand(buffer, ReadAnalogInputs, address, length);
        }

        public void BuildCoilSingleWriteCommand(ArrayBuilder<byte> buffer, ushort address, bool value)
        {
            BuildSingleWriteCommand(buffer, true, address, value ? CoilTrueValue : (ushort)0x0000);
        }

        public void BuildAnalogOutputSingleWriteCommand(ArrayBuilder<byte> buffer, ushort address, ushort value)
        {
            BuildSingleWriteCommand(buffer, false, address, value);
        }

        public void BuildCoilMultipleWriteCommand(ArrayBuilder<byte> buffer, ushort address, ushort length, ArrayBuilder<byte> data)
        {
            BuildMultipleWriteCommand(buffer, true, address, length, data);
        }

        public void BuildAnalogOutputMultipleWriteCommand(ArrayBuilder<byte> buffer, ushort address, ushort length, ArrayBuilder<byte> data)
        {
            BuildMultipleWriteCommand(buffer, false, address, length, data);
        }

        public void BuildReadCommand(ArrayBuilder<byte> buffer, byte function, ushort address, ushort length)
        {
            if (buffer == null)
                throw new ArgumentNullException("buffer");
            if (length == 0)
                throw new ArgumentException("The length argument must be greater than zero.", "length");

            if (!IsAccessRangeValid(address, length))
                throw new ArgumentException(AccessOutOfRange);

            int bytesRead = GetAccessBytes(function, length);
            if (IsAccessBytesExceededLimit(bytesRead))
                throw new ArgumentException(AccessBytesLimitExceeded);

            buffer.Clear();
            PrepareADUHeader(buffer);
            PackReadPDU(buffer, function, address, length);
            CompleteADUPack(buffer);
        }

        public void BuildSingleWriteCommand(ArrayBuilder<byte> buffer, byte function, ushort address, ushort value)
        {
            if (function != WriteSingleCoil && function != WriteSingleAnalogOutput)
                throw new ArgumentException($"Illegal function: {function.ToString()}.", "function");

            BuildSingleWriteCommand(buffer, function == WriteSingleCoil, address, value);
        }

        public void BuildSingleWriteCommand(ArrayBuilder<byte> buffer, bool coil, ushort address, ushort value)
        {
            if (buffer == null)
                throw new ArgumentNullException("buffer");

            buffer.Clear();
            PrepareADUHeader(buffer);
            PackSingleWritePDU(buffer, coil ? WriteSingleCoil : WriteSingleAnalogOutput, address, value);
            CompleteADUPack(buffer);
        }

        public void BuildMultipleWriteCommand(ArrayBuilder<byte> buffer, byte function, ushort address, ushort length, ArrayBuilder<byte> data)
        {
            if (function != WriteMultipleCoils && function != WriteMultipleAnalogOutputs)
                throw new ArgumentException($"Illegal function: {function.ToString()}.", "function");

            BuildMultipleWriteCommand(buffer, function == WriteMultipleCoils, address, length, data);
        }

        public void BuildMultipleWriteCommand(ArrayBuilder<byte> buffer, bool coil, ushort address, ushort length, ArrayBuilder<byte> data)
        {
            if (buffer == null)
                throw new ArgumentNullException("buffer");
            if (length == 0)
                throw new ArgumentException("The length argument must be greater than zero.", "length");
            if (data == null)
                throw new ArgumentNullException("data");

            if (!IsAccessRangeValid(address, length))
                throw new ArgumentException(AccessOutOfRange);

            int bytesToWrite = GetAccessBytes(!coil, length);

            if (bytesToWrite != data.Length)
                throw new ArgumentException("The ArrayBuilder<byte> length must be equal bytes to write.", "data");
            if (IsAccessBytesExceededLimit(bytesToWrite))
                throw new ArgumentException(AccessBytesLimitExceeded);

            buffer.Clear();
            PrepareADUHeader(buffer);
            PackMultipleWritePDU(buffer, coil ? WriteMultipleCoils : WriteMultipleAnalogOutputs, address, length, new ArraySegment<byte>(data.Buffer, 0, data.Length));
            CompleteADUPack(buffer);
        }

        public bool IsAccessRangeValid(ushort address, ushort length)
        {
            return address + length <= ushort.MaxValue + 1;
        }

        public int GetAccessBytes(byte function, ushort length)
        {
            return GetAccessBytes(IsAnalogAccessFunction(function), length);
        }

        public int GetAccessBytes(bool analog, ushort length)
        {
            return analog ? length * 2 : (length + 7) / 8;
        }

        public bool IsDataAccessFunction(byte function)
        {
            switch (function)
            {
                case ReadCoils:
                case ReadContacts:
                case ReadAnalogOutputs:
                case ReadAnalogInputs:
                case WriteSingleCoil:
                case WriteSingleAnalogOutput:
                case WriteMultipleCoils:
                case WriteMultipleAnalogOutputs:
                    return true;
                default:
                    return false;
            }
        }

        public bool IsDiscreteAccessFunction(byte function)
        {
            return !IsAnalogAccessFunction(function);
        }

        public bool IsAnalogAccessFunction(byte function)
        {
            switch (function)
            {
                case ReadCoils:
                case ReadContacts:
                case WriteSingleCoil:
                case WriteMultipleCoils:
                    return false;
                case ReadAnalogOutputs:
                case ReadAnalogInputs:
                case WriteSingleAnalogOutput:
                case WriteMultipleAnalogOutputs:
                    return true;
                default:
                    throw new ArgumentException($"Illegal function: {function.ToString()}.", "function");
            }
        }

        public abstract bool IsAccessBytesExceededLimit(int bytes);

        protected abstract void PrepareADUHeader(ArrayBuilder<byte> buffer);

        protected abstract void PackReadPDU(ArrayBuilder<byte> buffer, byte function, ushort address, ushort length);

        protected abstract void PackSingleWritePDU(ArrayBuilder<byte> buffer, byte function, ushort address, ushort value);

        protected abstract void PackMultipleWritePDU(ArrayBuilder<byte> buffer, byte function, ushort address, ushort length, ArraySegment<byte> data);

        protected abstract void CompleteADUPack(ArrayBuilder<byte> buffer);

        public abstract int GetContentLengthFromHeader(ArrayBuilder<byte> receiveBuffer, ArrayBuilder<byte> sendBuffer);

        public abstract bool ValidateHeader(ArrayBuilder<byte> receiveBuffer, ArrayBuilder<byte> sendBuffer);

        public abstract bool Validate(ArrayBuilder<byte> receiveBuffer, ArrayBuilder<byte> sendBuffer);

        public abstract OperationResult<int, int> ParseMessageData(ArrayBuilder<byte> message);

        public static string GetExceptionMeaning(byte exception)
        {
            switch(exception)
            {
                case IllegalFunction: return SR.ModbusIllegalFunction;
                case IllegalDataAddress: return SR.ModbusIllegalDataAddress;
                case IllegalDataValue: return SR.ModbusIllegalDataValue;
                case SlaveDeviceFailure: return SR.ModbusSlaveDeviceFailure;
                case Acknowledge: return SR.ModbusAcknowledge;
                case SlaveDeviceBusy: return SR.ModbusSlaveDeviceBusy;
                case NegativeAcknowledge: return SR.ModbusNegativeAcknowledge;
                case MemoryParityError: return SR.ModbusMemoryParityError;
                case GatewayPathUnavailable: return SR.ModbusGatewayPathUnavailable;
                case GatewayTargetDeviceFailedToRespond: return SR.ModbusGatewayTargetDeviceFailedToRespond;
                default: return SR.ModbusCustomException;
            }
        }
    }
}
