﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwissKnife;
using SwissKnife.Exceptions;

namespace Industry4._0.Modbus.RTU
{
    public class ModbusRTU : Modbus
    {
        public const ushort ModbusCRC16Polynomial = 0xA001;

        public const byte RTUMaxPDUSize = 252;
        public const byte RTUMaxDataBytesPerPDU = RTUMaxPDUSize - 6; // 6: function code + address + length + data length

        private byte _slaveId = 0x01;

        public byte SlaveId
        {
            get { return _slaveId; }
            set
            {
                if (_slaveId > 247)
                    throw new ArgumentOutOfRangeException("SlaveId");
                _slaveId = value;
            }
        }

        public override int HeaderLength { get { return 5; } }

        public override bool IsAccessBytesExceededLimit(int bytes)
        {
            return bytes > RTUMaxDataBytesPerPDU;
        }

        protected override void PrepareADUHeader(ArrayBuilder<byte> buffer)
        {
            buffer[0] = _slaveId;
        }

        protected override void PackReadPDU(ArrayBuilder<byte> buffer, byte function, ushort address, ushort length)
        {
            int startIndex = buffer.Length;

            buffer.Length = startIndex + 5;

            buffer[startIndex] = function;
            ByteConverter.GetBytes(address, buffer.Buffer, startIndex + 1);
            ByteConverter.GetBytes(length, buffer.Buffer, startIndex + 3);
        }

        protected override void PackSingleWritePDU(ArrayBuilder<byte> buffer, byte function, ushort address, ushort value)
        {
            int startIndex = buffer.Length;

            buffer.Length = startIndex + 5;

            buffer[startIndex] = function;
            ByteConverter.GetBytes(address, buffer.Buffer, startIndex + 1);
            ByteConverter.GetBytes(value, buffer.Buffer, startIndex + 3);
        }

        protected override void PackMultipleWritePDU(ArrayBuilder<byte> buffer, byte function, ushort address, ushort length, ArraySegment<byte> data)
        {
            int startIndex = buffer.Length;

            buffer.Length = startIndex + 6 + data.Count;

            buffer[startIndex] = function;
            ByteConverter.GetBytes(address, buffer.Buffer, startIndex + 1);
            ByteConverter.GetBytes(length, buffer.Buffer, startIndex + 3);
            buffer[startIndex + 5] = (byte)data.Count;
            buffer.Insert(startIndex + 6, data.Array, data.Offset, data.Count);
        }

        protected override void CompleteADUPack(ArrayBuilder<byte> buffer)
        {
            int crcStartIndex = buffer.Length;

            ushort crc = CRC.CRC16(buffer.Buffer, 0, crcStartIndex, ModbusCRC16Polynomial);
            buffer[crcStartIndex] = (byte)(crc & 0xFF);
            buffer[crcStartIndex + 1] = (byte)((crc >> 8) & 0xFF);
        }

        public override int GetContentLengthFromHeader(ArrayBuilder<byte> receiveBuffer, ArrayBuilder<byte> sendBuffer)
        {
            byte function = sendBuffer[1];

            if (receiveBuffer[1] != function) // exception
                return 0;

            int messageLength;

            switch (function)
            {
                case ReadCoils:
                case ReadContacts:
                case ReadAnalogOutputs:
                case ReadAnalogInputs:
                    messageLength = 5 + receiveBuffer[2]; // 5: slave id + function code + data length + crc
                    break;
                case WriteSingleCoil:
                case WriteSingleAnalogOutput:
                    messageLength = sendBuffer.Length;
                    break;
                case WriteMultipleCoils:
                case WriteMultipleAnalogOutputs:
                    messageLength = 8; // 8: slave id + function code + address + length + crc
                    break;
                default:
                    throw new BugException($"Function code (0x{function.ToString("X2")}): Not implemented.");
            }

            return messageLength - 5; // 5: HeaderLength;
        }

        public override bool ValidateHeader(ArrayBuilder<byte> receiveBuffer, ArrayBuilder<byte> sendBuffer)
        {
            byte slaveId = sendBuffer[0];

            if (receiveBuffer[0] != slaveId)
                return false;

            byte function = sendBuffer[1];

            if (receiveBuffer[1] != function)
            {
                if (receiveBuffer[1] != (function | 0x80))
                    return false;

                return CheckCRC(receiveBuffer.Buffer, 5);
            }

            switch (function)
            {
                case WriteSingleCoil:
                case WriteSingleAnalogOutput:
                    for (int i = 2; i <= 3; ++i)
                    {
                        if (receiveBuffer[i] != sendBuffer[i])
                            return false;
                    }
                    break;
                case WriteMultipleCoils:
                case WriteMultipleAnalogOutputs:
                    for (int i = 2; i <= 4; ++i)
                    {
                        if (receiveBuffer[i] != sendBuffer[i])
                            return false;
                    }
                    break;
            }
            return true;
        }

        public override bool Validate(ArrayBuilder<byte> receiveBuffer, ArrayBuilder<byte> sendBuffer)
        {
            return CheckCRC(receiveBuffer.Buffer, receiveBuffer.Length);
        }

        private bool CheckCRC(byte[] content, int length)
        {
            int crcStartIndex = length - 2;
            ushort crc = CRC.CRC16(content, 0, crcStartIndex, ModbusCRC16Polynomial);
            return content[crcStartIndex] == (crc & 0xFF) && content[crcStartIndex + 1] == ((crc >> 8) & 0xFF);
        }

        public override OperationResult<int, int> ParseMessageData(ArrayBuilder<byte> message)
        {
            if (message == null)
                throw new ArgumentNullException("message");
            if (message.Length < 5)
                throw new ArgumentException("Length property of message argument must be greater than or equal to 5.", "message");

            byte function = message[1];

            if ((function & 0x80) == 0x80)
            {
                byte exception = message[2];
                return OperationResultFactory.CreateFailedResult<int, int>(GetType().FullName, (int)Industry4Dot0Error.ModbusException, exception, GetExceptionMeaning(exception));
            }

            int startIndex = 3;
            int length = message.Length - 5; // 5: slave id + function code + data length + crc

            switch (function)
            {
                case ReadCoils:
                case ReadContacts:
                    return OperationResultFactory.CreateSuccessfulResult(startIndex, length, SR.Success);
                case ReadAnalogOutputs:
                case ReadAnalogInputs:
                    if ((length & 0x1) == 0x1)
                        return OperationResultFactory.CreateFailedResult<int, int>(GetType().FullName, (int)Industry4Dot0Error.UnexpectedLengthWasReceived, SR.UnexpectedLengthWasReceived);
                    return OperationResultFactory.CreateSuccessfulResult(startIndex, length, SR.Success);
                default:
                    return OperationResultFactory.CreateSuccessfulResult(0, 0, SR.Success);
            }
        }
    }
}
