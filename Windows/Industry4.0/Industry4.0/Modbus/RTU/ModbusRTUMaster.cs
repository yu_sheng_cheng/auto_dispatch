﻿using Industry4._0.Transports;
using SwissKnife.Conversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Industry4._0.Modbus.RTU
{
    public class ModbusRTUMaster<T> : ModbusMaster<T, ModbusRTU> where T : IByteConverter, new()
    {
        private readonly ModbusRTU _modbus;

        public ISerialPortTransport SerialPortTransport { get { return Transport as ISerialPortTransport; } }

        public byte SlaveId { get { return _modbus.SlaveId; } set { _modbus.SlaveId = value; } }

        public ModbusRTUMaster()
        {
            _modbus = Modbus as ModbusRTU;

            SerialPortTransport transport = new SerialPortTransport(_modbus);
            Transport = transport;
        }
    }

    public class ModbusRTUMaster : ModbusRTUMaster<BigEndianConverter>
    {

    }
}
