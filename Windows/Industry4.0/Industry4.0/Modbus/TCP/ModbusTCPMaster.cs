﻿using Industry4._0.IoT;
using Net.AsyncSocket.Tcp;
using SwissKnife;
using SwissKnife.Conversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Industry4._0.Modbus.TCP
{
    public class ModbusTCPMaster<T> : ModbusMaster<T, ModbusTCP> where T : IByteConverter, new()
    {
        public IInternetTransport InternetTransport { get { return Transport as IInternetTransport; } }

        public ModbusTCPMaster()
        {
            InternetTransport transport = new InternetTransport(Modbus);
            transport.Connected = OnConnected;
            Transport = transport;
        }

        protected virtual Task<OperationResult> OnConnected(AsyncTcpClient session)
        {
            return Task.FromResult(OperationResultFactory.CreateSuccessfulResult());
        }
    }

    public class ModbusTCPMaster : ModbusTCPMaster<BigEndianConverter>
    {

    }
}
