﻿using Industry4._0.Modbus.RTU;
using SwissKnife;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Industry4._0.Modbus.TCP
{
    public sealed class ModbusTCP : ModbusRTU
    {
        public const ushort TwoPDUProtocolId = 0x5487;
        public const ushort ThreePDUProtocolId = 0x9487;

        private ushort _transactionId;

        public byte UnitId { get; set; } = 0;

        public override int HeaderLength { get { return 7; } }

        public ModbusTCP()
        {
            Random random = new Random();
            _transactionId = (ushort)random.Next(0, ushort.MaxValue);
        }

        public override bool IsAccessBytesExceededLimit(int bytes)
        {
            return bytes > 3 * RTUMaxDataBytesPerPDU;
        }

        protected override void PrepareADUHeader(ArrayBuilder<byte> buffer)
        {
            buffer.Length = 7;

            ushort transactionId = unchecked((ushort)(_transactionId + 1));
            _transactionId = transactionId;

            ByteConverter.GetBytes(transactionId, buffer.Buffer, 0);
            // Protocal ID (2bytes)
            // Length (2bytes)
            buffer[6] = UnitId;
        }

        protected override void PackMultipleWritePDU(ArrayBuilder<byte> buffer, byte function, ushort address, ushort length, ArraySegment<byte> data)
        {
            bool analog = function == WriteMultipleAnalogOutputs;
            int offset = data.Offset;
            int bytesToWrite = data.Count;

            while (bytesToWrite > 0)
            {
                int bytesWritten = bytesToWrite <= RTUMaxDataBytesPerPDU ? bytesToWrite : RTUMaxDataBytesPerPDU;

                ushort registerCount;

                if (analog)
                    registerCount = (ushort)(bytesWritten / 2);
                else
                {
                    registerCount = (ushort)(bytesWritten * 8);
                    if (registerCount > length)
                        registerCount = length;
                }

                base.PackMultipleWritePDU(buffer, function, address, registerCount, new ArraySegment<byte>(data.Array, offset, bytesWritten));

                bytesToWrite -= bytesWritten;
                offset += bytesWritten;
                address = unchecked((ushort)(address + registerCount));
                length -= registerCount;
            }
        }

        protected override void CompleteADUPack(ArrayBuilder<byte> buffer)
        {
            if (buffer.Length > 7 + RTUMaxPDUSize) // 7 (Transaction Id, Protocal ID, Length, Unit Id)
            {
                buffer[4] = RTUMaxPDUSize;

                int bytesRemaining = buffer.Length - (7 + RTUMaxPDUSize);

                if (bytesRemaining > RTUMaxPDUSize)
                {
                    ByteConverter.GetBytes(ThreePDUProtocolId, buffer.Buffer, 2);
                    buffer[5] = RTUMaxPDUSize;
                    bytesRemaining -= RTUMaxPDUSize;
                    buffer[6] = (byte)bytesRemaining;
                }
                else
                {
                    ByteConverter.GetBytes(TwoPDUProtocolId, buffer.Buffer, 2);
                    buffer[5] = (byte)bytesRemaining;
                }
            }
            else
                ByteConverter.GetBytes((ushort)(buffer.Length - 6), buffer.Buffer, 4);
        }

        public override int GetContentLengthFromHeader(ArrayBuilder<byte> receiveBuffer, ArrayBuilder<byte> sendBuffer)
        {
            int messageLength;

            if (!ExtractMessageLength(receiveBuffer, out messageLength))
                throw new BugException("Should not go in here.");

            return messageLength - HeaderLength;
        }

        public override bool ValidateHeader(ArrayBuilder<byte> receiveBuffer, ArrayBuilder<byte> sendBuffer)
        {
            if (receiveBuffer[0] != sendBuffer[0] | receiveBuffer[1] != sendBuffer[1])
                return false;

            return ExtractMessageLength(receiveBuffer, out int unused);
        }

        public override bool Validate(ArrayBuilder<byte> receiveBuffer, ArrayBuilder<byte> sendBuffer)
        {
            return true;
        }

        public override OperationResult<int, int> ParseMessageData(ArrayBuilder<byte> message)
        {
            if (message == null)
                throw new ArgumentNullException("message");
            if (message.Length < HeaderLength)
                throw new ArgumentException($"Length property of message argument must be greater than or equal to {HeaderLength.ToString()}.", "message");

            int messageLength;

            if (!ExtractMessageLength(message, out messageLength) || message.Length != messageLength)
                throw new BugException("It need be validated before calling the method.");

            byte function = message[7];

            if ((function & 0x80) == 0x80)
            {
                byte exception = message[8];
                return OperationResultFactory.CreateFailedResult<int, int>(GetType().FullName, (int)Industry4Dot0Error.ModbusException, exception, GetExceptionMeaning(exception));
            }

            switch (function)
            {
                case ReadCoils:
                case ReadContacts:
                    return ParseMessageData(message, true);
                case ReadAnalogOutputs:
                case ReadAnalogInputs:
                    return ParseMessageData(message, false);
                default:
                    return OperationResultFactory.CreateSuccessfulResult(0, 0, SR.Success);
            }
        }

        private OperationResult<int, int> ParseMessageData(ArrayBuilder<byte> message, bool discrete)
        {
            ushort protocolId = ByteConverter.ToUInt16(message.Buffer, 2);

            byte[] buffer = message.Buffer;
            int secondPDUStartIndex = HeaderLength + message[4];
            int thirdPDUStartIndex = secondPDUStartIndex + message[5];
            int length;

            switch (protocolId)
            {
                case 0:
                    length = message.Length - 9;
                    break;
                case TwoPDUProtocolId:
                    Array.Copy(buffer, secondPDUStartIndex + 2, buffer, secondPDUStartIndex, message[5] - 2);
                    length = message.Length - 11;
                    break;
                case ThreePDUProtocolId:
                    int secondPDUDataLength = message[5] - 2;
                    Array.Copy(buffer, secondPDUStartIndex + 2, buffer, secondPDUStartIndex, secondPDUDataLength);
                    Array.Copy(buffer, thirdPDUStartIndex + 2, buffer, secondPDUStartIndex + secondPDUDataLength, message[6] - 2);
                    length = message.Length - 13;
                    break;
                default:
                    throw new BugException("Should not go in here.");
            }

            if (!discrete && (length & 0x1) == 0x1)
                return OperationResultFactory.CreateFailedResult<int, int>(GetType().FullName, (int)Industry4Dot0Error.UnexpectedLengthWasReceived, SR.UnexpectedLengthWasReceived);
            return OperationResultFactory.CreateSuccessfulResult(9, length, SR.Success);
        }

        private bool ExtractMessageLength(ArrayBuilder<byte> message, out int result)
        {
            ushort protocolId = ByteConverter.ToUInt16(message.Buffer, 2);

            result = 0;

            switch (protocolId)
            {
                case 0:
                    ushort length = ByteConverter.ToUInt16(message.Buffer, 4);
                    if (length < 3 | length > 255)
                        return false;
                    result = 6 + length;
                    break;
                case TwoPDUProtocolId:
                    if (message[4] < 2 | message[5] < 2)
                        return false;
                    result = HeaderLength + message[4] + message[5];
                    break;
                case ThreePDUProtocolId:
                    if (message[4] < 2 | message[5] < 2 | message[6] < 2)
                        return false;
                    result = HeaderLength + message[4] + message[5] + message[6];
                    break;
                default:
                    return false;
            }

            return true;
        }
    }
}
