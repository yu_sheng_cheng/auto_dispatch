﻿using Industry4._0.Common;
using Industry4._0.IoT;
using Industry4._0.Transports;
using SwissKnife;
using SwissKnife.Conversion;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Industry4._0.Modbus
{
    public abstract class ModbusMaster<T1, T2> : ThingAbstract<T1> where T1 : IByteConverter, new() where T2 : Modbus, new()
    {
        private readonly ArrayBuilder<byte> _buffer = new ArrayBuilder<byte>();

        protected Modbus Modbus { get; } = new T2();

        public bool IsRegisterAddressStartsWithZero { get; set; } = true;

        protected override Task<OperationResult<int, int>> ReadWords(string address, ushort length, SendReceiveBuffer sendReceiveBuffer)
        {
            byte function = Modbus.ReadAnalogInputs;
            ushort baseAddress;

            if (!TryParseAddress(address, out baseAddress, ref function) || (function != Modbus.ReadAnalogInputs && function != Modbus.ReadAnalogOutputs))
                return Task.FromResult(OperationResultFactory.CreateFailedResult<int, int>(GetType().FullName, (int)Industry4Dot0Error.NotSupportedAddress, SR.NotSupportedAddress));

            return ReadAsync(function, baseAddress, length, sendReceiveBuffer);
        }

        protected override Task<OperationResult> WriteWords(string address, ushort length, ArrayBuilder<byte> data, SendReceiveBuffer sendReceiveBuffer)
        {
            byte function = Modbus.WriteMultipleAnalogOutputs;
            ushort baseAddress;

            if (!TryParseAddress(address, out baseAddress, ref function))
                return Task.FromResult(OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.NotSupportedAddress, SR.NotSupportedAddress));

            return WriteAsync(Modbus.WriteMultipleAnalogOutputs, baseAddress, length, data, sendReceiveBuffer);
        }

        protected override async Task<OperationResult> ReadBitsAsync(string address, ushort length, SendReceiveBuffer sendReceiveBuffer, ArrayBuilder<bool> buffer)
        {
            byte function = Modbus.ReadContacts;
            ushort baseAddress;

            if (!TryParseAddress(address, out baseAddress, ref function) || (function != Modbus.ReadCoils && function != Modbus.ReadContacts))
                return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.NotSupportedAddress, SR.NotSupportedAddress);

            OperationResult<int, int> result = await ReadAsync(function, baseAddress, length, sendReceiveBuffer);
            if (!result.IsSuccess)
                return result;

            buffer.EnsureCapacity(length);
            SwissKnife.Conversion.Convert.ByteArrayToBoolArray(sendReceiveBuffer.Receive.Buffer, result.Value1, result.Value2, buffer.Buffer, 0);
            buffer.Length = length;
            return result;
        }

        protected override Task<OperationResult> WriteBitsAsync(string address, ArrayBuilder<bool> values, SendReceiveBuffer sendReceiveBuffer)
        {
            byte function = Modbus.WriteMultipleCoils;
            ushort baseAddress;

            if (!TryParseAddress(address, out baseAddress, ref function))
                return Task.FromResult(OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.NotSupportedAddress, SR.NotSupportedAddress));

            int count = values.Length;

            _buffer.Length = (count + 7) / 8;
            SwissKnife.Conversion.Convert.BoolArrayToByteArray(values.Buffer, 0, count, _buffer.Buffer, 0);
            return WriteAsync(Modbus.WriteMultipleCoils, baseAddress, (ushort)count, _buffer, sendReceiveBuffer);
        }

        private async Task<OperationResult<int, int>> ReadAsync(byte function, ushort address, ushort length, SendReceiveBuffer sendReceiveBuffer)
        {
            OperationResult result = ValidateReadWriteRangeInput(function, address, length);
            if (!result.IsSuccess)
                return OperationResultFactory.CreateFailedResult<int, int>(result);

            Modbus.BuildReadCommand(sendReceiveBuffer.Send, function, address, length);

            result = await Transport.SendThenReceiveAsync(sendReceiveBuffer).ConfigureAwait(false);
            if (!result.IsSuccess)
                return OperationResultFactory.CreateFailedResult<int, int>(result);

            OperationResult<int, int> replyResult = Modbus.ParseMessageData(sendReceiveBuffer.Receive);
            if (!replyResult.IsSuccess)
                return replyResult;

            if (replyResult.Value2 != Modbus.GetAccessBytes(function, length))
                return OperationResultFactory.CreateFailedResult<int, int>(GetType().FullName, (int)Industry4Dot0Error.UnexpectedLengthWasReceived, SR.UnexpectedLengthWasReceived);

            return replyResult;
        }

        private async Task<OperationResult> WriteAsync(byte function, ushort address, ushort length, ArrayBuilder<byte> data, SendReceiveBuffer sendReceiveBuffer)
        {
            OperationResult result = ValidateReadWriteRangeInput(function, address, length);
            if (!result.IsSuccess)
                return OperationResultFactory.CreateFailedResult(result);

            Modbus.BuildMultipleWriteCommand(sendReceiveBuffer.Send, function, address, length, data);

            result = await Transport.SendThenReceiveAsync(sendReceiveBuffer).ConfigureAwait(false);
            if (!result.IsSuccess)
                return result;

            return Modbus.ParseMessageData(sendReceiveBuffer.Receive);
        }

        private bool TryParseAddress(string address, out ushort result, ref byte function)
        {
            result = 0;
            byte specifiedFunction = function;

            if (address.Length >= 3)
            {
                if (address.StartsWith("F:", true, CultureInfo.InvariantCulture))
                {
                    int index = address.IndexOf(';', 2);

                    if (index == -1)
                        return false;

                    string value = address.Substring(2, index - 2);

                    if (value.StartsWith("0x"))
                    {
                        if (!byte.TryParse(value.Substring(2), NumberStyles.HexNumber, CultureInfo.InvariantCulture, out specifiedFunction))
                            return false;
                    }
                    else
                    {
                        if (!byte.TryParse(value, out specifiedFunction))
                            return false;
                    }

                    if (!Modbus.IsDataAccessFunction(specifiedFunction))
                        return false;

                    address = address.Substring(index + 1);
                }
            }

            if (!ushort.TryParse(address, out result))
                return false;

            if (!IsRegisterAddressStartsWithZero)
            {
                if (result == 0)
                    return false;
                --result;
            }
            function = specifiedFunction;
            return true;
        }

        private OperationResult ValidateReadWriteRangeInput(byte function, ushort address, ushort length)
        {       
            if (!Modbus.IsAccessRangeValid(address, length))
                return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.IllegalAccessRange, SR.IllegalAccessRange);

            if (Modbus.IsAccessBytesExceededLimit(Modbus.GetAccessBytes(function, length)))
                return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.AccessLimitExceeded, SR.AccessLimitExceeded);

            return OperationResultFactory.CreateSuccessfulResult();
        }
    }
}
