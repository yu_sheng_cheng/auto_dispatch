﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Industry4._0
{
    public static class SR
    {
        private static StringResource _sr;

        public static string Success { get { return _sr.Success; } }
        public static string ConnectingFailed { get { return _sr.ConnectingFailed; } }
        public static string ConnectingSuccess { get { return _sr.ConnectingSuccess; } }
        public static string SerialPortOpenFailed { get { return _sr.SerialPortOpenFailed; } }
        public static string SerialPortOpenSuccess { get { return _sr.SerialPortOpenSuccess; } }
        public static string SendFailure { get { return _sr.SendFailure; } }
        public static string ReceiveFailure { get { return _sr.ReceiveFailure; } }
        public static string MessageHeaderValidatedFailed { get { return _sr.MessageHeaderValidatedFailed; } }
        public static string MessageContentLengthValidatedFailed { get { return _sr.MessageContentLengthValidatedFailed; } }
        public static string MessageContentValidatedFailed { get { return _sr.MessageContentValidatedFailed; } }
        public static string MessageLengthLimitExceeded { get { return _sr.MessageLengthLimitExceeded; } }
        public static string MessageEndsWithoutSpecifiedString { get { return _sr.MessageEndsWithoutSpecifiedString; } }
        public static string FunctionNotSupported { get { return _sr.FunctionNotSupported; } }
        public static string UnexpectedLengthWasReceived { get { return _sr.UnexpectedLengthWasReceived; } }
        public static string UnexpectedEndStringWasReceived { get { return _sr.UnexpectedEndStringWasReceived; } }
        public static string NotSupportedAddress { get { return _sr.NotSupportedAddress; } }
        public static string OperationBadReturn { get { return _sr.OperationBadReturn; } }
        public static string AddressNonAligned { get { return _sr.AddressNonAligned; } }
        public static string LengthMustBeAMultipleOfN { get { return _sr.LengthMustBeAMultipleOfN; } }
        public static string OutOfRange { get { return _sr.OutOfRange; } }
        public static string NumberOfBlocks { get { return _sr.NumberOfBlocks; } }
        public static string BlockLength { get { return _sr.BlockLength; } }
        public static string IllegalAccessRange { get { return _sr.IllegalAccessRange; } }
        public static string AccessLimitExceeded { get { return _sr.AccessLimitExceeded; } }
        public static string CommunicationTimeout { get { return _sr.CommunicationTimeout; } }
        public static string ObjectDisposed { get { return _sr.ObjectDisposed; } }
        public static string ModbusIllegalFunction { get { return _sr.ModbusIllegalFunction; } }
        public static string ModbusIllegalDataAddress { get { return _sr.ModbusIllegalDataAddress; } }
        public static string ModbusIllegalDataValue { get { return _sr.ModbusIllegalDataValue; } }
        public static string ModbusSlaveDeviceFailure { get { return _sr.ModbusSlaveDeviceFailure; } }
        public static string ModbusAcknowledge { get { return _sr.ModbusAcknowledge; } }
        public static string ModbusSlaveDeviceBusy { get { return _sr.ModbusSlaveDeviceBusy; } }
        public static string ModbusNegativeAcknowledge { get { return _sr.ModbusNegativeAcknowledge; } }
        public static string ModbusMemoryParityError { get { return _sr.ModbusMemoryParityError; } }
        public static string ModbusGatewayPathUnavailable { get { return _sr.ModbusGatewayPathUnavailable; } }
        public static string ModbusGatewayTargetDeviceFailedToRespond { get { return _sr.ModbusGatewayTargetDeviceFailedToRespond; } }
        public static string ModbusCustomException { get { return _sr.ModbusCustomException; } }

        static SR()
        {
            _sr = StringResource.Load();
        }

        public static void SetStringResource(StringResource sr)
        {
            _sr = sr;
        }
    }

    [XmlRoot(ElementName = "Industry4.0", Namespace = "Industry4.0.Cultures.StringResource.xaml", IsNullable = false)]
    public class StringResource
    {
        internal static StringResource Load()
        {
            return Load(string.Empty);
        }

        internal static StringResource Load(string name)
        {
            var assembly = Assembly.GetExecutingAssembly();

            if (!string.IsNullOrEmpty(name))
                name = $"_{name}";

            using (var stream = assembly.GetManifestResourceStream($"{assembly.GetName().Name.Replace(".", "._")}.Cultures.StringResource{name}.xml"))
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    var serializer = new XmlSerializer(typeof(StringResource));
                    return (StringResource)serializer.Deserialize(reader);
                }
            }
        }

        public string Success { get; set; }
        public string ConnectingFailed { get; set; }
        public string ConnectingSuccess { get; set; }
        public string SerialPortOpenFailed { get; set; }
        public string SerialPortOpenSuccess { get; set; }
        public string SendFailure { get; set; }
        public string ReceiveFailure { get; set; }
        public string MessageHeaderValidatedFailed { get; set; }
        public string MessageContentLengthValidatedFailed { get; set; }
        public string MessageContentValidatedFailed { get; set; }
        public string MessageLengthLimitExceeded { get; set; }
        public string MessageEndsWithoutSpecifiedString { get; set; }
        public string FunctionNotSupported { get; set; }
        public string UnexpectedLengthWasReceived { get; set; }
        public string UnexpectedEndStringWasReceived { get; set; }
        public string NotSupportedAddress { get; set; }
        public string OperationBadReturn { get; set; }
        public string AddressNonAligned { get; set; }
        public string LengthMustBeAMultipleOfN { get; set; }
        public string OutOfRange { get; set; }
        public string NumberOfBlocks { get; set; }
        public string BlockLength { get; set; }
        public string IllegalAccessRange { get; set; }
        public string AccessLimitExceeded { get; set; }
        public string CommunicationTimeout { get; set; }
        public string ObjectDisposed { get; set; }
        public string ModbusIllegalFunction { get; set; }
        public string ModbusIllegalDataAddress { get; set; }
        public string ModbusIllegalDataValue { get; set; }
        public string ModbusSlaveDeviceFailure { get; set; }
        public string ModbusAcknowledge { get; set; }
        public string ModbusSlaveDeviceBusy { get; set; }
        public string ModbusNegativeAcknowledge { get; set; }
        public string ModbusMemoryParityError { get; set; }
        public string ModbusGatewayPathUnavailable { get; set; }
        public string ModbusGatewayTargetDeviceFailedToRespond { get; set; }
        public string ModbusCustomException { get; set; }
    }
}
