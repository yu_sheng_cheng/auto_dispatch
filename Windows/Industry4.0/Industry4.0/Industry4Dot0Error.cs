﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Industry4._0
{
    public enum Industry4Dot0Error : short
    {
        Error = -1,
        Success = 0,
        Timeout,
        ObjectDisposed,
        ArgumentOutOfRange,
        InvalidArgument,
        MessageHeaderError = 1024,
        MessageContentLengthError,
        MessageContentError,
        MessageLengthLimitExceeded,
        MessageEndsWithoutSpecifiedString,
        FunctionNotSupported,
        UnexpectedLengthWasReceived,
        UnexpectedEndStringWasReceived,
        NotSupportedAddress,
        IllegalAccessRange,
        AccessLimitExceeded,
        OperationBadReturn,
        ModbusException = 2048
    }
}
