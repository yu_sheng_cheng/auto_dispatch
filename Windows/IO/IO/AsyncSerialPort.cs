﻿using SwissKnife;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IO
{
    public sealed class AsyncSerialPort : SerialPort
    {
        private const int MaxTimeout = 86400000;
        private const int MinTimeout = 10;
        private const int DefaultTimeout = 500;

        private readonly ArrayBuilder<byte> _lineBuffer = new ArrayBuilder<byte>();
        private byte[] _newLineBytes;
        private int _timeout = DefaultTimeout;
        private int _delayedTimeOfBeforeStartingReading;
        private int _delayedReadingTime;

        public new Encoding Encoding
        {
            get { return base.Encoding; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("Encoding");

                if (!(value is ASCIIEncoding | value is UTF8Encoding))
                    throw new ArgumentException($"AsyncSerialPort does not support encoding '{value.WebName}'. The supported encodings include ASCIIEncoding, UTF8Encoding.");

                base.Encoding = value;

                _newLineBytes = base.Encoding.GetBytes(base.NewLine);
            }
        }

        public new string NewLine
        {
            get { return base.NewLine; }
            set
            {
                if (value != base.NewLine)
                {
                    base.NewLine = value;

                    _newLineBytes = base.Encoding.GetBytes(base.NewLine);
                }
            }
        }

        public int AsyncTimeout
        {
            get { return _timeout; }
            set
            {
                if (value > MaxTimeout)
                    _timeout = MaxTimeout;
                else if (value < MinTimeout)
                    _timeout = MinTimeout;
                else
                    _timeout = value;
            }
        }

        public int DelayedTimeOfBeforeStartingReading
        {
            get { return _delayedTimeOfBeforeStartingReading; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("DelayedTimeOfBeforeStartingReading");

                _delayedTimeOfBeforeStartingReading = value;
            }
        }

        public AsyncSerialPort() : this("COM1", 9600, Parity.None, 8) { }

        public AsyncSerialPort(string portName) : this(portName, 9600, Parity.None, 8) { }

        public AsyncSerialPort(string portName, int baudRate) : this(portName, baudRate, Parity.None, 8) { }

        public AsyncSerialPort(string portName, int baudRate, Parity parity) : this(portName, baudRate, parity, 8) { }

        public AsyncSerialPort(string portName, int baudRate, Parity parity, int dataBits) : base(portName, baudRate, parity, dataBits)
        {
            _newLineBytes = base.Encoding.GetBytes(base.NewLine);
        }

        public new void Open()
        {
            base.Open();

            int bitsPerFrame = 1 + DataBits + (Parity == Parity.None ? 0 : 1); // start bit + data bit + parity
            switch (StopBits)
            {
                case StopBits.One:
                    ++bitsPerFrame;
                    break;
                case StopBits.OnePointFive:
                case StopBits.Two:
                    bitsPerFrame += 2;
                    break;
            }

            _delayedReadingTime = 1000 * 4 * bitsPerFrame / BaudRate; // delayed 4 characters (milliseconds)
            if (_delayedReadingTime == 0)
                _delayedReadingTime = 1;

            _lineBuffer.Clear();
        }

        public new void Close()
        {
            base.Close();

            _lineBuffer.Clear();
        }

        public new void DiscardInBuffer()
        {
            base.DiscardInBuffer();

            _lineBuffer.Clear();
        }

        public async Task<string> ReadLineAsync()
        {
            _lineBuffer.EnsureCapacity(64);

            byte[] buffer = _lineBuffer.Buffer;
            int capacity = _lineBuffer.Capacity;
            int bytesRead = _lineBuffer.Length;
            byte[] newLineBytes = Volatile.Read(ref _newLineBytes);
            int newLineBytesLength = newLineBytes.Length;
            int start = 0;
            int timeUsed = 0;

            bool stickyPacket = bytesRead != 0;

            while (true)
            {
                if (stickyPacket)
                    stickyPacket = false;
                else
                {
                    var result = await ReadAsync(buffer, bytesRead, capacity - bytesRead, _timeout, timeUsed, bytesRead != 0).ConfigureAwait(false);

                    bytesRead += result.Item1;
                    timeUsed = result.Item2;
                }

                if ((bytesRead - start) >= newLineBytesLength)
                {
                    int end = bytesRead - newLineBytesLength;

                    while (start <= end)
                    {
                        bool found = true;

                        for (int i = 0; i < newLineBytesLength; ++i)
                        {
                            if (buffer[start + i] != newLineBytes[i])
                            {
                                found = false;
                                break;
                            }
                        }

                        if (!found)
                        {
                            ++start;
                            continue;
                        }

                        string line = base.Encoding.GetString(buffer, 0, start);

                        if (start < end)
                        {
                            int length = start + newLineBytesLength;

                            Buffer.BlockCopy(buffer, length, buffer, 0, bytesRead - length);
                        }

                        return line;
                    }
                }

                if (bytesRead == capacity)
                {
                    if (capacity >= ushort.MaxValue)
                        throw new OutOfLimitException("The length of line is too long. It cannot be longer than 65535 characters.");

                    _lineBuffer.EnsureCapacity(capacity * 2);
                    capacity = _lineBuffer.Capacity;
                    buffer = _lineBuffer.Buffer;
                }
            }
        }

        public async Task<int> ReadAsync(byte[] buffer, int offset, int count)
        {
            var result = await ReadAsync(buffer, offset, count, _timeout, 0, false).ConfigureAwait(false);

            return result.Item1;
        }

        private async Task<(int, int)> ReadAsync(byte[] buffer, int offset, int count, int timeout, int timeUsed, bool firstByteReceived)
        {
            int delayedReadingTime = (timeUsed != 0 | _delayedTimeOfBeforeStartingReading == 0) ? _delayedReadingTime : _delayedTimeOfBeforeStartingReading;

            while (true)
            {
                if (timeUsed >= timeout)
                    throw new TimeoutException();

                int millisecondsDelay = (timeUsed + delayedReadingTime) <= timeout ? delayedReadingTime : (timeout - timeUsed);
                int timeNow = Environment.TickCount;
                await Task.Delay(millisecondsDelay).ConfigureAwait(false);
                timeUsed += Environment.TickCount - timeNow;

                if (BytesToRead > 0)
                    return (Read(buffer, offset, count), timeUsed);

                delayedReadingTime = _delayedReadingTime;

                if (!firstByteReceived && delayedReadingTime < 10)
                    delayedReadingTime = 10;
            }
        }
    }
}
