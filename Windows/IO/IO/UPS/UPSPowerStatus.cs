﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IO.UPS
{
    public struct UPSPowerStatus
    {
        public bool IsACLineOnline { get; set; }

        public bool IsLowBattery { get; set; }
    }
}
