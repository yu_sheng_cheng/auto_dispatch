﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IO.UPS.CyberPower
{
    public class CyberPowerUPS : IQueryableUPS, IDisposable
    {
        private readonly AsyncSerialPort _port;

        public CyberPowerUPS(string portName)
        {
            _port = new AsyncSerialPort(portName);

            try
            {
                _port.Open();
                _port.RtsEnable = true;
            }
            catch
            {
                _port.Dispose();
                throw;
            }
        }

        public void Dispose()
        {
            _port.Dispose();
        }

        public UPSPowerStatus QueryPowerStatus()
        {
            UPSPowerStatus powerStatus = new UPSPowerStatus
            {
                IsACLineOnline = _port.CtsHolding,
                IsLowBattery = !_port.CDHolding
            };
            return powerStatus;
        }
    }
}
