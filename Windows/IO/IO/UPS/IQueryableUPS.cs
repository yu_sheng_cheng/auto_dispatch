﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IO.UPS
{
    public interface IQueryableUPS
    {
        UPSPowerStatus QueryPowerStatus();
    }
}
