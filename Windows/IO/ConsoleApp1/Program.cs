﻿using IO.UPS;
using IO.UPS.CyberPower;
using Net;
using Net.AsyncSocket.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
#if false
            CyberPowerUPS ups = new CyberPowerUPS("COM1");

            UPSPowerStatus powerStatus = ups.QueryPowerStatus();
            Console.WriteLine("IsACLineOnline: {0}", powerStatus.IsACLineOnline);
            Console.WriteLine("IsLowBattery: {0}", powerStatus.IsLowBattery);
#endif
            Task.Run(async () =>
            {

                Console.WriteLine("Task.Run ...");
                //using (AsyncTcpClient client = new AsyncTcpClient(IPAddress.Parse("192.168.1.230"), 34980, configuration))
                //using (AsyncTcpClient client = new AsyncTcpClient(IPAddress.Parse("192.168.1.230"), 20000))
                using (AsyncTcpClient client = new AsyncTcpClient(IPAddress.Parse("192.168.3.104"), 20000))
                {
                    TcpSocketConfiguration configuration = client.Configuration;
                    configuration.ReceiveBufferSize = 64 << 10;
                    configuration.SendBufferSize = 64 << 10;
                    configuration.IsKeepAliveEnabled = true;
                    configuration.KeepAliveTime = 10000;
                    configuration.KeepAliveInterval = 1000;

                    await client.ConnectAsync().ConfigureAwait(false);
                    //Console.WriteLine("111");

                    StringBuilder sb = new StringBuilder();

                    sb.Append("MARK \"STOP\"");
                    sb.AppendLine();

                    byte[] reply = new byte[4096];
                    byte[] buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                    int bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                    Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));

                    sb.Clear();
                    sb.Append("LOADPROJECT \"222Rossi.msg\"");
                    sb.AppendLine();

                    buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                    bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                    Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));

                    sb.Clear();
                    sb.Append("SETTEXT \"vbatch\" \"B\"");
                    sb.AppendLine();

                    buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                    bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                    Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));

                    sb.Clear();
                    sb.Append("MARK \"START\"");
                    sb.AppendLine();

                    buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                    bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                    Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));

#if false

                    sb.Append("BUFFERCLEAR");
                    sb.AppendLine();
                    byte[]  buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                    byte[] reply = new byte[4096];

                    int bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                    Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));

                    sb.Clear();
                    sb.Append("BUFFERDATA 1 \"GG\"");
                    sb.AppendLine();
                    buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                    

                   bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                    Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));

                    sb.Clear();
                    sb.Append("SETTEXT \"GG\" \"P9999\"");
                    sb.AppendLine();
                    buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                    bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                    Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));
#endif


#if false
                    //sb.Append("GETXML");
                    sb.Append("GETSTATUS");
                    sb.AppendLine();
                    byte[] buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                    byte[] reply = new byte[4096];

                    int bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                    Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));
#endif

#if false
                    //sb.Append("SETXML <?xml version=\"1.0\" encoding=\"UTF-8\"?><G-Series-Project title=\"ppn.msg\" version=\"1.0\"><Line>[Definition]</Line><Line>Aufloesung(V1.0)=2;0;1;0;</Line><Line>Abmessung(V1.0)=800;0;127;0;</Line><Line>BoxSize(V1.0)=50;0;</Line><Line>Direction(V1.0)=0;1;0;1;0;1;</Line><Line>Margin(V1.0)=0;1;0;1;</Line><Line>Group(V1.0)=0;1;1;1;0;1;</Line><Line>Mode(V1.0)=0;1;0;1;</Line><Line>Speed(V1.1)=1;1;0;1;4;</Line><Line>Intelligent MP(V1.0)=0;1;</Line><Line>[Element]</Line><Line>Name(V1.0)=barcode;</Line><Line>Typ(V1.0)=11;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=5;3;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;9;0;</Line><Line>Symbologie(V1.0)=71;0;1;1;1;71;1:1;</Line><Line>Text(V1.0)=#vppn #vbatch;0;0;</Line><Line>Format string(V1.0)=;</Line><Line>Size(V1.0)=45;0;120;10;10;1;1;-100;1;</Line><Line>2D Size(V1.0)=-1;-1;-100;-100;-1;;</Line><Line>2D Options(V1.0)=0;-1;-1;;</Line><Line>Bearer bar(V1.0)=0;-100;</Line><Line>Quiet zone(V1.0)=0;0;0;0;</Line><Line>Encoding(V1.0)=1;4;0;0;23;1;0;0;</Line><Line>PDF417(V1.0)=-1;0;-1;-1;0;-1;-1;;;</Line><Line>PDF417 File(V1.0)=-1;;;</Line><Line>[Element]</Line><Line>Name(V1.0)=ppn;</Line><Line>Typ(V1.0)=2;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=150;0;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>Text(V1.0)=PPN:;</Line><Line>[Element]</Line><Line>Name(V1.0)=batch;</Line><Line>Typ(V1.0)=2;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=150;30;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>Text(V1.0)=Batch:;</Line><Line>[Element]</Line><Line>Name(V1.0)=exp;</Line><Line>Typ(V1.0)=2;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=150;62;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>Text(V1.0)=Expiry Date:;</Line><Line>[Element]</Line><Line>Name(V1.0)=SN;</Line><Line>Typ(V1.0)=2;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=150;93;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>Text(V1.0)=Serial Number:;</Line><Line>[Element]</Line><Line>Name(V1.0)=vppn;</Line><Line>Typ(V1.0)=3;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=360;0;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>VTextSize(V1.0)=10;</Line><Line>Mandatory(V1.0)=0;</Line><Line>BufferdataID(V1.0)=0;</Line><Line>[Element]</Line><Line>Name(V1.0)=vbatch;</Line><Line>Typ(V1.0)=3;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=360;30;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>VTextSize(V1.0)=10;</Line><Line>Mandatory(V1.0)=0;</Line><Line>BufferdataID(V1.0)=0;</Line><Line>[Element]</Line><Line>Name(V1.0)=vexp;</Line><Line>Typ(V1.0)=3;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=360;62;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>VTextSize(V1.0)=10;</Line><Line>Mandatory(V1.0)=0;</Line><Line>BufferdataID(V1.0)=0;</Line><Line>[Element]</Line><Line>Name(V1.0)=vser;</Line><Line>Typ(V1.0)=3;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=360;93;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>VTextSize(V1.0)=10;</Line><Line>Mandatory(V1.0)=0;</Line><Line>BufferdataID(V1.0)=0;</Line></G-Series-Project>");

                    sb.Append("SETXML <?xml version=\"1.0\" encoding=\"UTF-8\"?><G-Series-Project title=\"222Rossi.msg\" version=\"1.0\"><Line>[Definition]</Line><Line>Aufloesung(V1.0)=2;0;1;0;</Line><Line>Abmessung(V1.0)=800;0;127;0;</Line><Line>BoxSize(V1.0)=50;0;</Line><Line>Direction(V1.0)=0;1;0;1;0;1;</Line><Line>Margin(V1.0)=0;1;0;1;</Line><Line>Group(V1.0)=0;1;1;1;0;1;</Line><Line>Mode(V1.0)=0;1;0;1;</Line><Line>Speed(V1.1)=1;1;0;1;4;</Line><Line>Intelligent MP(V1.0)=0;1;</Line><Line>[Element]</Line><Line>Name(V1.0)=2d barcode;</Line><Line>Typ(V1.0)=10;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=0;0;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;9;0;</Line><Line>Size(V1.0)=0;80;0;0;</Line><Line>Text(V1.1)=ABC#GG#;0;0;</Line><Line>[Element]</Line><Line>Name(V1.0)=ppn;</Line><Line>Typ(V1.0)=2;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=150;0;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>Text(V1.0)=PPN:;</Line><Line>[Element]</Line><Line>Name(V1.0)=batch;</Line><Line>Typ(V1.0)=2;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=150;30;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>Text(V1.0)=Batch:;</Line><Line>[Element]</Line><Line>Name(V1.0)=exp;</Line><Line>Typ(V1.0)=2;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=150;62;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>Text(V1.0)=Expiry Date:;</Line><Line>[Element]</Line><Line>Name(V1.0)=SN;</Line><Line>Typ(V1.0)=2;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=150;93;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>Text(V1.0)=Serial Number:;</Line><Line>[Element]</Line><Line>Name(V1.0)=vppn;</Line><Line>Typ(V1.0)=3;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=360;0;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>VTextSize(V1.0)=10;</Line><Line>Mandatory(V1.0)=0;</Line><Line>BufferdataID(V1.0)=0;</Line><Line>[Element]</Line><Line>Name(V1.0)=vbatch;</Line><Line>Typ(V1.0)=3;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=360;30;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>VTextSize(V1.0)=10;</Line><Line>Mandatory(V1.0)=0;</Line><Line>BufferdataID(V1.0)=0;</Line><Line>[Element]</Line><Line>Name(V1.0)=vexp;</Line><Line>Typ(V1.0)=3;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=360;62;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>VTextSize(V1.0)=10;</Line><Line>Mandatory(V1.0)=0;</Line><Line>BufferdataID(V1.0)=0;</Line><Line>[Element]</Line><Line>Name(V1.0)=vser;</Line><Line>Typ(V1.0)=3;1;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=360;93;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;8;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>VTextSize(V1.0)=10;</Line><Line>Mandatory(V1.0)=0;</Line><Line>BufferdataID(V1.0)=0;</Line><Line>[Element]</Line><Line>Name(V1.0)=GG;</Line><Line>Typ(V1.0)=3;0;</Line><Line>Ausrichtung(V1.0)=0;0;</Line><Line>Position(V1.0)=0;0;</Line><Line>Invers(V1.0)=0;</Line><Line>Font(V1.0)=arial.etf;9;0;</Line><Line>Chimney(V1.0)=0;</Line><Line>VTextSize(V1.0)=10;</Line><Line>Mandatory(V1.0)=0;</Line><Line>BufferdataID(V1.0)=0;</Line></G-Series-Project>");

                    // sb.Append("GETXML");

                  //  sb.Append("LOADPROJECT \"test.xml\"");

                    sb.AppendLine();
                    //string str = sb.ToString();
                  //  Console.WriteLine("{0}", sb.ToString());
                    byte[] buffer = Encoding.UTF8.GetBytes(sb.ToString());

                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                   

                    // Thread.Sleep(3000);

                    byte[] reply = new byte[4096];
                    // Console.WriteLine("ffff");

                    int bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                   Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));

                    /*sb.Clear();
                    sb.Append("SAVEPROJECT \"test.xml\"");
                    sb.AppendLine();

                     buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                     bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                     Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply,0, bytesRead));

                    sb.Clear();
                    sb.Append("LOADPROJECT \"test.xml\"");
                    sb.AppendLine();

                    buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                    bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                    Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));*/

                    sb.Clear();
                    sb.Append("SETTEXT \"GG\" \"PROSSI\"");
                    sb.AppendLine();

                    buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                    bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                    Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));

                    sb.Clear();
                    sb.Append("SETTEXT \"vbatch\" \"B\"");
                    sb.AppendLine();

                    buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                    bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                    Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));

                    sb.Clear();
                    sb.Append("SETTEXT \"vexp\" \"E\"");
                    sb.AppendLine();

                    buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                    bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                    Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));

                    sb.Clear();
                    sb.Append("SETTEXT \"vser\" \"S\"");
                    sb.AppendLine();

                    buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                    bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                    Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));

                    sb.Clear();
                    sb.Append("MARK \"START\"");
                    sb.AppendLine();

                    buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                    bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                    Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));


                    sb.Clear();
                    sb.Append("TRIGGER");
                    sb.AppendLine();

                    buffer = Encoding.UTF8.GetBytes(sb.ToString());
                    await client.SendAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                    bytesRead = await client.ReceiveAsync(reply, 0, reply.Length).ConfigureAwait(false);

                    Console.WriteLine("bytesRead: {0}", bytesRead);
                    Console.WriteLine("{0}", Encoding.UTF8.GetString(reply, 0, bytesRead));
#endif
                }
            });

            Console.ReadLine();
        }
    }
}
