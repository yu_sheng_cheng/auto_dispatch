﻿using Net.AsyncSocket.Tcp;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Task.Run(async () =>
             {
                 using (AsyncTcpClient client = new AsyncTcpClient(IPAddress.Parse("192.168.1.87"), 80))
                 {
                     Console.WriteLine("ggggggg");
                     await client.ConnectAsync().ConfigureAwait(false);
                     Console.WriteLine("pppppppp");
                     byte[] data = Encoding.ASCII.GetBytes("D \"Hello\"\r\n");
                     await client.SendAsync(data, 0, data.Length).ConfigureAwait(false);

                     Thread.Sleep(3000);
                     Console.WriteLine("xxxxxxx");
                     byte[] buffer = new byte[128];
                     int bytesRead = await client.ReceiveAsync(buffer, 0, buffer.Length).ConfigureAwait(false);
                     Console.WriteLine("Receive {0} bytes ({1})", bytesRead.ToString(), Encoding.ASCII.GetString(buffer, 0, bytesRead));
                 }
             });*/

            SerialPort port = new SerialPort("COM8");
            port.BaudRate = 9600;
            port.DataBits = 8;
            port.Parity = Parity.None;
            port.StopBits = StopBits.One;

            port.Open();
            port.Write("S\r\n");
           // Thread.Sleep(5000);
            byte[] buffer = new byte[128];
            string result = port.ReadLine();
            Console.WriteLine("{0}", result);

            Console.ReadLine();
        }
    }
}
