﻿using FriedRiceMachine.POS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriedRiceMachine.KDS
{
    class OrderItem
    {
        public ushort OrderNumber { get; }

        public bool IsOrderInserting { get; }

        public Recipe Recipe { get; }

        public FriedRiceFlavor Flavor { get; }

        public bool HasAddedShallot { get; set; } = true;

        public bool HasAddedOnion { get; set; } = true;

        public bool HasAddedFungus { get; set; } = true;

        public bool HasAddedCarrot { get; set; } = true;

        public bool HasAddedCabbage { get; set; } = true;

        public bool HasAddedChili { get; set; } = false;

        public bool IsLessRice { get; set; } = false;

        public FriedRicePot Pot { get; set; }

        public bool CanCancel { get; set; } = true;

        public bool Redo { get; set; }

        public OrderItem(ushort orderNumber, Recipe recipe, FriedRiceFlavor flavor, bool isOrderInserting)
        {
            OrderNumber = orderNumber;
            IsOrderInserting = isOrderInserting;
            Recipe = recipe;
            Flavor = flavor;
        }

        public override string ToString()
        {
            return $"{OrderNumber.ToString("0000")}: {Recipe.Name} ({SR.ToString(Flavor)})";
        }
    }
}
