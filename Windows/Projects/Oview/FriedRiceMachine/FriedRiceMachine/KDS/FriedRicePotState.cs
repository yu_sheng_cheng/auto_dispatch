﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriedRiceMachine.KDS
{
    enum FriedRicePotState : byte
    {
        PreparingToAcceptOrder,
        PreparedDoneToBeAcceptOrder,
        OrderReleased,
        OrderCancelling,
        Feeding,
        FriedRice,
        TakingOffFromPot,       
        ReadyToWashPot,
        WashingPot,
    }
}
