﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriedRiceMachine.KDS
{
    class FriedRicePot
    {
        public string Code { get; }

        public ushort Number { get; }

        public bool AutomatedMode { get; set; }

        public bool HasAlarm { get; set; }

        public FriedRicePotState State { get; private set; }

        public FriedRiceProgress Progress { get; set; }

        public OrderItem ReceivedOrderItem { get; set; }

        public ushort Servings { get; private set; }

        //public bool NeedsWashPot { get { return Servings >= 2; } }
        public bool NeedsWashPot { get { return false; } }

        public FriedRicePot(string code, ushort number)
        {
            Code = code;
            Number = number;
        }

        public void UpdateState(FriedRicePotState newState)
        {
            if (newState == State)
                return;

            State = newState;

            switch (newState)
            {
                case FriedRicePotState.PreparingToAcceptOrder:
                case FriedRicePotState.PreparedDoneToBeAcceptOrder:
                case FriedRicePotState.ReadyToWashPot:
                case FriedRicePotState.WashingPot:
                    Progress = FriedRiceProgress.Idle;
                    ReceivedOrderItem = null;
                    if (newState == FriedRicePotState.WashingPot)
                        Servings = 0;
                    break;
                case FriedRicePotState.OrderReleased:
                case FriedRicePotState.OrderCancelling:
                case FriedRicePotState.Feeding:
                    Progress = FriedRiceProgress.Idle;
                    break;
                case FriedRicePotState.FriedRice:
                    ++Servings;
                    break;
            }
        }
    }
}
