﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriedRiceMachine.KDS
{
    enum FriedRiceProgress : byte
    {
        Idle,
        PreheatingPan,
        OilHeating,
        Toppings,
        StirFrying,
        Finished,
        TakingOffFromPot
    }
}
