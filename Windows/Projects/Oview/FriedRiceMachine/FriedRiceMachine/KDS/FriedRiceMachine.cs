﻿using Automation.Core;
using PLC;
using PLC.Mitsubishi;
using SwissKnife;
using SwissKnife.Exceptions;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FriedRiceMachine.KDS
{
    class FriedRiceMachine : SupervisoryController
    {
        public const byte MessageClassFriedRiceMachine = (byte)MessageClass.Other + 0;
        public const ushort OrderReceivedMessage = 0;

        private readonly MelsecMCNet _melsec;
        private readonly PLCDeviceTransportBlock W0000;
        private readonly PLCDeviceTransportBlock W1000;
        private readonly List<PLCDeviceTransportBlock> _plcWritableBlocks = new List<PLCDeviceTransportBlock>();
        private readonly ArrayBuilder<ushort> W0100 = new ArrayBuilder<ushort>();

        private bool _canOrderToPot;
        private bool _needTransferRecipe;
        private OrderItem _pendingOrderItem;
        private readonly LightSpinLock _lock = new LightSpinLock();

        protected override AsynchronousTimeoutSignal WDTExpiredSignal { get { return _melsec.TimeoutSignal; } }

        public FriedRicePot[] Pots { get; }

        public FriedRiceMachine()
        {
            LogKeyword = "FriedRiceMachine";
            ScanCycleTime = 60;

            IPAddress ipAddress = IPAddress.Parse(ConfigurationManager.AppSettings["PLC.IPAddress"]);
            int port = int.Parse(ConfigurationManager.AppSettings["PLC.Port"]);

            Log.i($"PLC Ethernet Setting: {ipAddress}:{port}");

            _melsec = new MelsecMCNet("Q", "Q03UDV");
            _melsec.InternetTransport.RemoteEndPoint = new IPEndPoint(ipAddress, port);

            W0000 = CreatePLCTransportBlock("W0000", 5);
            W1000 = CreatePLCTransportBlock("W1000", 96);

            Pots = new FriedRicePot[] { new FriedRicePot("A", 1), new FriedRicePot("B", 2), new FriedRicePot("C", 3), new FriedRicePot("D", 4) };
        }

        protected override void CleanUp()
        {
            base.CleanUp();
            _melsec.Dispose();
        }

        private PLCDeviceTransportBlock CreatePLCTransportBlock(string address, ushort length)
        {
            OperationResult<PLCDeviceTransportBlock> result = _melsec.CreateDeviceTransportBlock(address, length);
            if (!result.IsSuccess)
                throw new BugException(result.Message);
            return result.Value;
        }

        protected override async Task<bool> Refresh()
        {
            OperationResult result = await _melsec.ReadUInt16Async("W0100", 164, W0100).ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                Log.i(LogKeyword, $"{_melsec.Type.Name} Read: {result}");
                return false;
            }

            _needTransferRecipe = false;

            uint w0000 = W0000.ReadUInt16(0x00);
            uint w0002 = W0000.ReadUInt16(0x02);
            uint w0004 = W0000.ReadUInt16(0x04);

            int count = Pots.Length;

            for (int i = 0; i < count; ++i)
            {
                FriedRicePot pot = Pots[i];
                ushort value = W0100[(i + 1) * 0x10];

                pot.AutomatedMode = BitOperation.GetBit(value, 0);
                pot.HasAlarm = BitOperation.GetBit(value, 0x0d);

                switch (pot.State)
                {
                    case FriedRicePotState.PreparingToAcceptOrder:
                        if (BitOperation.GetBit(value, 4)) // 訂單製作失敗
                            break;
                        if (BitOperation.GetBit(w0004, (byte)(3 + i))) // 訂單製作失敗確認
                        {
                            BitOperation.SetBit(ref w0004, (byte)(3 + i), false);
                            MachineState.NeedsUpdate();
                        }
                        if (BitOperation.GetBit(value, 1)) // 準備完成，可接受訂單
                            pot.UpdateState(FriedRicePotState.PreparedDoneToBeAcceptOrder);
                        break;
                    case FriedRicePotState.PreparedDoneToBeAcceptOrder:
                        if (!BitOperation.GetBit(value, 1)) // 不能接受訂單
                            pot.UpdateState(FriedRicePotState.PreparingToAcceptOrder);
                        break;
                    case FriedRicePotState.OrderReleased:
                        if (BitOperation.GetBit(value, 2)) // 接受訂單成功確認
                        {
                            BitOperation.SetBit(ref w0004, 1, true); // 訂單接受成功確認
                            MachineState.NeedsUpdate();
                            pot.UpdateState(FriedRicePotState.Feeding);
                        }
                        else if (BitOperation.GetBit(value, 3)) // 接受訂單失敗確認
                        {
                            BitOperation.SetBit(ref w0004, 2, true); // 訂單接受失敗確認
                            MachineState.NeedsUpdate();
                            pot.UpdateState(FriedRicePotState.OrderCancelling);
                        }
                        break;
                    case FriedRicePotState.OrderCancelling:
                        if (!BitOperation.GetBit(value, 3)) // 訂單已完成取消
                        {
                            w0000 = 0; // 鍋站選擇
                            w0002 = 0; // 訂單編號
                            BitOperation.SetBit(ref w0004, 0, false); // 訂單送出
                            BitOperation.SetBit(ref w0004, 2, false); // 訂單接受失敗確認
                            MachineState.NeedsUpdate();

                            _pendingOrderItem.Pot = null;
                            pot.UpdateState(FriedRicePotState.PreparingToAcceptOrder);
                        }
                        break;
                    case FriedRicePotState.Feeding:
                        if (BitOperation.GetBit(value, 3)) // 接受訂單失敗確認
                        {
                            BitOperation.SetBit(ref w0004, 1, false); // 訂單接受成功確認
                            BitOperation.SetBit(ref w0004, 2, true); // 訂單接受失敗確認
                            MachineState.NeedsUpdate();
                            pot.UpdateState(FriedRicePotState.OrderCancelling);
                        }
                        else if (!BitOperation.GetBit(value, 2)) // 補料完成
                        {
                            w0000 = 0; // 鍋站選擇
                            w0002 = 0; // 訂單編號
                            BitOperation.SetBit(ref w0004, 0, false); // 訂單送出
                            BitOperation.SetBit(ref w0004, 1, false); // 訂單接受成功確認 
                            MachineState.NeedsUpdate();

                            _lock.Enter();
                            _pendingOrderItem = null;
                            _lock.Leave();

                            pot.UpdateState(FriedRicePotState.FriedRice);
                            Emit("FeedingCompleted", SignalArgs.Empty);
                        }
                        break;
                    case FriedRicePotState.FriedRice:
                    case FriedRicePotState.TakingOffFromPot:
                        FriedRiceProgress progress = FriedRiceProgress.Idle;
                        if (BitOperation.GetBit(value, 7)) //  熱鍋中
                            progress = FriedRiceProgress.PreheatingPan;
                        else if (BitOperation.GetBit(value, 8)) // 加料中
                            progress = FriedRiceProgress.Toppings;
                        else if (BitOperation.GetBit(value, 9)) // 翻炒中
                            progress = FriedRiceProgress.StirFrying;
                        else if (BitOperation.GetBit(value, 0xA)) // 出鍋中
                            progress = FriedRiceProgress.TakingOffFromPot;
                        else if (BitOperation.GetBit(value, 0xB)) // 炒飯完成
                            progress = FriedRiceProgress.Finished;
                        else if (BitOperation.GetBit(value, 0xC)) // 熱油中
                            progress = FriedRiceProgress.OilHeating;
                        pot.Progress = progress;

                        if (BitOperation.GetBit(value, 4)) // 訂單製作失敗
                        {
                            BitOperation.SetBit(ref w0004, (byte)(3 + i), true); // 訂單製作失敗確認
                            MachineState.NeedsUpdate();

                            OrderItem orderItem = pot.ReceivedOrderItem;
                            orderItem.Pot = null;
                            pot.UpdateState(pot.NeedsWashPot ? FriedRicePotState.ReadyToWashPot : FriedRicePotState.PreparingToAcceptOrder);
                            Emit("FriedRiceMakeFailed", new FriedRiceMakeFailedSignalArgs(orderItem));
                        }
                        else
                        {
                            if (BitOperation.GetBit(value, 0x0A)) //  出鍋中
                            {
                                if (pot.State == FriedRicePotState.FriedRice)
                                    pot.UpdateState(FriedRicePotState.TakingOffFromPot);
                            }
                            else if (pot.State == FriedRicePotState.TakingOffFromPot)
                            {
                                OrderItem orderItem = pot.ReceivedOrderItem;
                                pot.UpdateState(pot.NeedsWashPot ? FriedRicePotState.ReadyToWashPot : FriedRicePotState.PreparingToAcceptOrder);
                                Emit("FriedRiceMakeCompleted", new FriedRiceMakeCompletedSignalArgs(orderItem));
                            }
                        }
                        break;
                    case FriedRicePotState.ReadyToWashPot:
                        if (BitOperation.GetBit(value, 4)) // 訂單製作失敗
                            break;
                        if (BitOperation.GetBit(w0004, (byte)(3 + i))) // 訂單製作失敗確認
                        {
                            BitOperation.SetBit(ref w0004, (byte)(3 + i), false);
                            MachineState.NeedsUpdate();
                        }
                        if (!pot.AutomatedMode)
                            pot.UpdateState(FriedRicePotState.WashingPot);
                        break;
                    case FriedRicePotState.WashingPot:
                        if (pot.AutomatedMode)
                            pot.UpdateState(FriedRicePotState.PreparingToAcceptOrder);
                        break;
                }
            }

            W0000.Write(0x0000, unchecked((ushort)w0000));
            W0000.Write(0x0002, unchecked((ushort)w0002));
            W0000.Write(0x0004, unchecked((ushort)w0004));

            bool hasPot = false;
            _canOrderToPot = true;
            foreach (var pot in Pots)
            {
                switch (pot.State)
                {
                    case FriedRicePotState.PreparedDoneToBeAcceptOrder:
                        hasPot = true;
                        break;
                    case FriedRicePotState.OrderReleased:
                    case FriedRicePotState.OrderCancelling:
                    case FriedRicePotState.Feeding:
                        _canOrderToPot = false;
                        break;
                }
            }
            if (!hasPot)
                _canOrderToPot = false;

            ProcessOrder();
            return true;
        }

        protected override async Task<bool> Update()
        {
            _plcWritableBlocks.Clear();
            if (_needTransferRecipe)
                _plcWritableBlocks.Add(W1000);
            _plcWritableBlocks.Add(W0000);

            OperationResult result = await _melsec.WriteMultipleBlocksAsync(_plcWritableBlocks).ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                Log.i(LogKeyword, $"{_melsec.Type.Name} Write: {result}");
                return false;
            }
            return true;
        }

        protected override void OnIncomingMessage(Message message, bool stopped)
        {
            if (message.Class != MessageClassFriedRiceMachine)
                return;

            switch (message.Id)
            {
                case OrderReceivedMessage:
                    OrderItem orderItem = message.Content as OrderItem;

                    bool success = true;

                    _lock.Enter();
                    if (_pendingOrderItem != null)
                        success = false;
                    else
                        _pendingOrderItem = orderItem;
                    _lock.Leave();

                    if (!success)
                        Log.e(LogKeyword, "The orders are under processing that we can't arrange order");
                    else
                        ProcessOrder();
                    break;
            }
        }

        private void ProcessOrder()
        {
            if (!_canOrderToPot)
                return;

            OrderItem orderItem;

            _lock.Enter();
            try
            {
                orderItem = _pendingOrderItem;
                if (orderItem == null)
                    return;

                if (orderItem.Pot != null)
                    throw new BugException("The order is currently being processed.");

                orderItem.CanCancel = false;
            }
            finally
            {
                _lock.Leave();
            }

            Recipe recipe = orderItem.Recipe;

            foreach (var pot in Pots)
            {
                if (pot.State == FriedRicePotState.PreparedDoneToBeAcceptOrder)
                {
                    W1000.ClearBuffer();

                    W1000.Write(0x1000, recipe.HotPotPower);
                    W1000.Write(0x1001, recipe.HotOilPower);
                    W1000.Write(0x1002, recipe.ScrambledEggPower);
                    W1000.Write(0x1003, recipe.Procedure4Power);
                    W1000.Write(0x1004, recipe.Procedure5Power);
                    W1000.Write(0x1005, recipe.Procedure6Power);
                    W1000.Write(0x1006, recipe.Procedure7Power);
                    W1000.Write(0x1007, recipe.Procedure8Power);
                    W1000.Write(0x1008, recipe.Procedure9Power);
                    W1000.Write(0x1015, recipe.HotOilTime);
                    W1000.Write(0x1016, recipe.ScrambledEggTime);
                    W1000.Write(0x1017, recipe.Procedure4StirFryTime);
                    W1000.Write(0x1018, recipe.Procedure5StirFryTime);
                    W1000.Write(0x1019, recipe.Procedure6StirFryTime);
                    W1000.Write(0x101A, recipe.Procedure7StirFryTime);
                    W1000.Write(0x101B, recipe.Procedure8StirFryTime);
                    W1000.Write(0x101C, recipe.Procedure9StirFryTime);

                    switch (pot.Number)
                    {
                        case 1:
                            W1000.Write(0x101E, recipe.HotAPotTemperature);
                            break;
                        case 2:
                            W1000.Write(0x101E, recipe.HotBPotTemperature);
                            break;
                        case 3:
                            W1000.Write(0x101E, recipe.HotCPotTemperature);
                            break;
                        case 4:
                            W1000.Write(0x101E, recipe.HotDPotTemperature);
                            break;
                    }

                    ushort index = 1;
                    foreach (var material in recipe.FoodMaterials)
                    {
                        uint procedureNumber = material.ProcedureNumber;
                        W1000.Write(0x102B + procedureNumber - 4, index++);
                    }

                    W1000.Write(0x1033, 1);
                    W1000.Write(0x1034, 3);
                    W1000.Write(0x1035 + (uint)recipe.DippingProcedureNumber - 4, 2);

                    W1000.Write(0x103D, recipe.AmountOfOil);
                    W1000.Write(0x103E, recipe.AmountOfEggSauce);
                    W1000.Write(0x103F + (uint)recipe.DippingProcedureNumber - 4, recipe.AmountOfDip);

                    W0000.Write(0x0000, (ushort)(1 << pot.Number));
                    W0000.Write(0x0002, orderItem.OrderNumber);
                    uint w0004 = W0000.ReadUInt16(0x04);
                    BitOperation.SetBit(ref w0004, 0, true);
                    W0000.Write(0x0004, (ushort)w0004);

                    MachineState.NeedsUpdate();

                    orderItem.Pot = pot;
                    pot.ReceivedOrderItem = orderItem;
                    pot.UpdateState(FriedRicePotState.OrderReleased);
                    _needTransferRecipe = true;
                    _canOrderToPot = false;
                    return;
                }
            }

            throw new BugException("Fried rice pot was not found.");
        }

        public bool TryCancelOrder()
        {
            _lock.Enter();
            try
            {
                if (_pendingOrderItem == null)
                    return false;

                if (_pendingOrderItem.CanCancel)
                {
                    _pendingOrderItem = null;
                    return true;
                }

                return false;
            }
            finally
            {
                _lock.Leave();
            }
        }
    }
}
