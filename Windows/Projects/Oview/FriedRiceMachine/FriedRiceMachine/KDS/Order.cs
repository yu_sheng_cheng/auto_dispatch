﻿using FriedRiceMachine.POS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriedRiceMachine.KDS
{
    class Order
    {
        public ushort Number { get; }

        public OrderSource Source { get; }

        public ushort SourceOrderNumber { get; }

        public bool IsOrderInserting { get; }

        public int ItemCount { get; set; }

        public int CompletionCount { get; set; }

        public Order(ushort number, OrderSource source, ushort sourceOrderNumber, bool isOrderInserting)
        {
            Number = number;
            Source = source;
            SourceOrderNumber = sourceOrderNumber;
            IsOrderInserting = isOrderInserting;
        }
    }
}
