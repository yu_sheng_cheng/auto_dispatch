﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FriedRiceMachine.KDS
{
    [XmlRoot(ElementName = "Cookbook", Namespace = "FriedRiceMachine.KDS.Cookbook.xml", IsNullable = false)]
    public class Cookbook
    {
        [XmlArrayItem(ElementName = "Recipe")] public Recipe[] Recipes { get; set; } = new Recipe[0];
    }
}
