﻿using Automation.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriedRiceMachine.KDS
{
    class FriedRiceMakeCompletedSignalArgs : SignalArgs
    {
        public OrderItem OrderItem { get; }

        public FriedRiceMakeCompletedSignalArgs(OrderItem orderItem)
        {
            OrderItem = orderItem;
        }
    }

    class FriedRiceMakeFailedSignalArgs : SignalArgs
    {
        public OrderItem OrderItem { get; }

        public FriedRiceMakeFailedSignalArgs(OrderItem orderItem)
        {
            OrderItem = orderItem;
        }
    }
}
