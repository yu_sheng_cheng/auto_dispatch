﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FriedRiceMachine.KDS
{
    public class FoodMaterial : IComparable<FoodMaterial>
    {
        [XmlAttribute] public string Name { get; set; } = string.Empty;
        [XmlAttribute] public byte ProcedureNumber { get; set; }

        public int CompareTo(FoodMaterial other)
        {
            return ProcedureNumber - other.ProcedureNumber;
        }
    }

    public class Recipe
    {
        [XmlAttribute] public string Name { get; set; } = string.Empty;

        public ushort HotPotPower { get; set; }
        public ushort HotOilPower { get; set; }
        public ushort ScrambledEggPower { get; set; }
        public ushort Procedure4Power { get; set; }
        public ushort Procedure5Power { get; set; }
        public ushort Procedure6Power { get; set; }
        public ushort Procedure7Power { get; set; }
        public ushort Procedure8Power { get; set; }
        public ushort Procedure9Power { get; set; }

        public ushort HotOilTime { get; set; }
        public ushort ScrambledEggTime { get; set; }
        public ushort Procedure4StirFryTime { get; set; }
        public ushort Procedure5StirFryTime { get; set; }
        public ushort Procedure6StirFryTime { get; set; }
        public ushort Procedure7StirFryTime { get; set; }
        public ushort Procedure8StirFryTime { get; set; }
        public ushort Procedure9StirFryTime { get; set; }

        public ushort HotAPotTemperature { get; set; }
        public ushort HotBPotTemperature { get; set; }
        public ushort HotCPotTemperature { get; set; }
        public ushort HotDPotTemperature { get; set; }

        public byte DippingProcedureNumber { get; set; }
        [XmlArrayItem(ElementName = "FoodMaterial")] public FoodMaterial[] FoodMaterials { get; set; } = new FoodMaterial[0];

        public ushort AmountOfOil { get; set; }
        public ushort AmountOfEggSauce { get; set; }
        public ushort AmountOfDip { get; set; }
    }
}
