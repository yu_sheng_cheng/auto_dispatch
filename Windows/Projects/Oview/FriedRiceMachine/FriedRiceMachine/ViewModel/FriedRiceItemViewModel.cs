﻿using FriedRiceMachine.POS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Widget.WPF;

namespace FriedRiceMachine.ViewModel
{
    class FriedRiceItemViewModel : ViewModelBase
    {
        private readonly Menu _menu;
        private readonly FriedRiceItem _dummyItem = new FriedRiceItem() { Name = string.Empty };
        private FriedRiceItem _item;
        private FriedRiceFlavor _flavor = FriedRiceFlavor.PrimaryTaste;
        private bool _hasAddedShallot = true;
        private bool _hasAddedOnion = true;
        private bool _hasAddedFungus = true;
        private bool _hasAddedCarrot = true;
        private bool _hasAddedCabbage = true;
        private bool _hasAddedChili;
        private bool _isLessRice;

        public string Name { get { return _item.Name; } }

        public ushort UnitPrice
        {
            get
            {
                ushort price = _item.Price;

                switch (_flavor)
                {
                    case FriedRiceFlavor.SpicedSalt:
                        price += _menu.SpicedSaltPrice;
                        break;
                    case FriedRiceFlavor.MildSpicy:
                    case FriedRiceFlavor.MediumSpicy:
                    case FriedRiceFlavor.HotSpicy:
                        price += _menu.SpicyPrice;
                        break;
                    case FriedRiceFlavor.BlackPepper:
                        price += _menu.BlackPepperPrice;
                        break;
                }

                if (_hasAddedChili)
                    price += _menu.ChiliPrice;

                return price;
            }
        }

        public bool CanAddSpicedSalt { get { return _item.CanAddSpicedSalt; } }

        public bool CanAddSpicy { get { return _item.CanAddSpicy; } }

        public bool CanAddBlackPepper { get { return _item.CanAddBlackPepper; } }

        public FriedRiceFlavor? Flavor
        {
            get { return _flavor; }
            set
            {
                if (value != null && value != _flavor)
                {
                    _flavor = (FriedRiceFlavor)value;
                    RaisePropertyChanged(nameof(UnitPrice));
                }
            }
        }

        public int Amount { get; set; } = 1;
        
        public bool? HasAddedShallot
        {
            get { return _hasAddedShallot; }
            set
            {
                if (value != null && value != _hasAddedShallot)
                    _hasAddedShallot = (bool)value;
            }
        }

        public bool? HasAddedOnion
        {
            get { return _hasAddedOnion; }
            set
            {
                if (value != null && value != _hasAddedOnion)
                    _hasAddedOnion = (bool)value;
            }
        }

        public bool? HasAddedFungus
        {
            get { return _hasAddedFungus; }
            set
            {
                if (value != null && value != _hasAddedFungus)
                    _hasAddedFungus = (bool)value;
            }
        }

        public bool? HasAddedCarrot
        {
            get { return _hasAddedCarrot; }
            set
            {
                if (value != null && value != _hasAddedCarrot)
                    _hasAddedCarrot = (bool)value;
            }
        }

        public bool? HasAddedCabbage
        {
            get { return _hasAddedCabbage; }
            set
            {
                if (value != null && value != _hasAddedCabbage)
                    _hasAddedCabbage = (bool)value;
            }
        }

        public bool? HasAddedChili
        {
            get { return _hasAddedChili; }
            set
            {
                if (value != null && value != _hasAddedChili)
                {
                    _hasAddedChili = (bool)value;
                    RaisePropertyChanged(nameof(UnitPrice));
                }
            }
        }

        public bool? IsLessRice
        {
            get { return _isLessRice; }
            set
            {
                if (value != null && value != _isLessRice)
                    _isLessRice = (bool)value;
            }
        }

        public FriedRiceItem Item
        {
            get { return _item != _dummyItem ? _item : null; }
            set
            {
                if (value != Item)
                {
                    _item = value ?? _dummyItem;

                    _flavor = FriedRiceFlavor.PrimaryTaste;
                    Amount = 1;
                    _hasAddedShallot = true;
                    _hasAddedOnion = true;
                    _hasAddedFungus = true;
                    _hasAddedCarrot = true;
                    _hasAddedCabbage = true;
                    _hasAddedChili = false;
                    _isLessRice = false;

                    RaisePropertyChanged(nameof(Name));
                    RaisePropertyChanged(nameof(UnitPrice));
                    RaisePropertyChanged(nameof(CanAddSpicedSalt));
                    RaisePropertyChanged(nameof(CanAddSpicy));
                    RaisePropertyChanged(nameof(CanAddBlackPepper));
                    RaisePropertyChanged(nameof(Flavor));
                    RaisePropertyChanged(nameof(Amount));
                    RaisePropertyChanged(nameof(HasAddedShallot));
                    RaisePropertyChanged(nameof(HasAddedOnion));
                    RaisePropertyChanged(nameof(HasAddedFungus));
                    RaisePropertyChanged(nameof(HasAddedCarrot));
                    RaisePropertyChanged(nameof(HasAddedCabbage));
                    RaisePropertyChanged(nameof(HasAddedChili));
                    RaisePropertyChanged(nameof(IsLessRice));
                }
            }
        }

        public FriedRiceItemViewModel(Menu menu)
        {
            _menu = menu;
            _item = _dummyItem;
        }
    }
}
