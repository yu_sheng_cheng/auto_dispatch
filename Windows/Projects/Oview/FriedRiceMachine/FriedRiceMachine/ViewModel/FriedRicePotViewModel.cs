﻿using FriedRiceMachine.KDS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Widget.WPF;

namespace FriedRiceMachine.ViewModel
{
    class FriedRicePotViewModel : ViewModelBase
    {
        private string _mode = string.Empty;
        private bool _hasAlarm;
        private FriedRicePotState _state = FriedRicePotState.PreparingToAcceptOrder;
        private string _stateDisplayName = string.Empty;
        private string _progress = string.Empty;
        private OrderItem _orderItem;
        private string _orderItemDescription = string.Empty;

        public string Code { get; }

        public bool HasAlarm
        {
            get { return _hasAlarm; }
            set
            {
                if (value != _hasAlarm)
                {
                    _hasAlarm = value;
                    RaisePropertyChanged(nameof(HasAlarm));
                }
            }
        }

        public string Mode
        {
            get { return _mode; }
            set
            {
                if (value != _mode)
                {
                    _mode = value;
                    RaisePropertyChanged(nameof(Mode));
                }
            }
        }

        public FriedRicePotState State
        {
            get { return _state; }
            set
            {
                if (value != _state)
                {
                    _state = value;
                    RaisePropertyChanged(nameof(State));
                }
            }
        }

        public string StateDisplayName
        {
            get { return _stateDisplayName; }
            set
            {
                if (value != _stateDisplayName)
                {
                    _stateDisplayName = value;
                    RaisePropertyChanged(nameof(StateDisplayName));
                }
            }
        }

        public string Progress
        {
            get { return _progress; }
            set
            {
                if (value != _progress)
                {
                    _progress = value;
                    RaisePropertyChanged(nameof(Progress));
                }
            }
        }

        public string OrderItemDescription
        {
            get { return _orderItemDescription; }
            set
            {
                if (value != _orderItemDescription)
                {
                    _orderItemDescription = value;
                    RaisePropertyChanged(nameof(OrderItemDescription));
                }
            }
        }

        public FriedRicePotViewModel(string code)
        {
            Code = code;
        }

        public void UpdateAlarmState(bool hasAlarm)
        {
            HasAlarm = hasAlarm;
        }

        public void UpdateMode(bool automatedMode)
        {
            Mode = automatedMode ? "自動" : "手動";
        }

        public void UpdateState(FriedRicePotState state)
        {
            switch (state)
            {
                case FriedRicePotState.PreparingToAcceptOrder:
                    StateDisplayName = "準備中 ...";
                    break;
                case FriedRicePotState.PreparedDoneToBeAcceptOrder:
                    StateDisplayName = "準備完成，可接受訂單";
                    break;
                case FriedRicePotState.OrderReleased:
                    StateDisplayName = "訂單送出";
                    break;
                case FriedRicePotState.OrderCancelling:
                    StateDisplayName = "訂單取消中 ...";
                    break;
                case FriedRicePotState.Feeding:
                    StateDisplayName = "等待補料完成 ...";
                    break;
                case FriedRicePotState.FriedRice:
                    StateDisplayName = "炒飯中...";
                    break;
                case FriedRicePotState.TakingOffFromPot:
                    StateDisplayName = "出鍋中...";
                    break;
                case FriedRicePotState.ReadyToWashPot:
                case FriedRicePotState.WashingPot:
                    StateDisplayName = "需要洗鍋";
                    break;
            }

            State = state;
        }

        public void UpdateFriedRiceProgress(FriedRiceProgress progress)
        {
            switch (progress)
            {
                case FriedRiceProgress.Idle:
                    Progress = "閒置";
                    break;
                case FriedRiceProgress.PreheatingPan:
                    Progress = "熱鍋中";
                    break;
                case FriedRiceProgress.OilHeating:
                    Progress = "熱油中";
                    break;
                case FriedRiceProgress.Toppings:
                    Progress = "加料中";
                    break;
                case FriedRiceProgress.StirFrying:
                    Progress = "翻炒中";
                    break;
                case FriedRiceProgress.Finished:
                    Progress = "炒飯完成";
                    break;
                case FriedRiceProgress.TakingOffFromPot:
                    Progress = "出鍋中";
                    break;
            }
        }

        public void UpdateOrder(OrderItem orderItem)
        {
            if (orderItem != _orderItem)
            {
                _orderItem = orderItem;
                OrderItemDescription = orderItem == null ? string.Empty : orderItem.ToString();
            }
        }
    }
}
