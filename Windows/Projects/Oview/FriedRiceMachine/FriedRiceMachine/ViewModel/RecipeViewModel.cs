﻿using FriedRiceMachine.KDS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Widget.WPF;

namespace FriedRiceMachine.ViewModel
{
    class RecipeViewModel : ViewModelBase
    {
        private readonly Recipe _dummyRecipe = new Recipe();
        private readonly FoodMaterial[] _foodMaterials = new FoodMaterial[] { new FoodMaterial(), new FoodMaterial(), new FoodMaterial(), new FoodMaterial() };
        private Recipe _recipe;

        public string Name { get { return _recipe.Name; } }

        public ushort HotPotPower { get { return _recipe.HotPotPower; } }
        public ushort HotOilPower { get { return _recipe.HotOilPower; } }
        public ushort ScrambledEggPower { get { return _recipe.ScrambledEggPower; } }
        public ushort Procedure4Power { get { return _recipe.Procedure4Power; } }
        public ushort Procedure5Power { get { return _recipe.Procedure5Power; } }
        public ushort Procedure6Power { get { return _recipe.Procedure6Power; } }
        public ushort Procedure7Power { get { return _recipe.Procedure7Power; } }
        public ushort Procedure8Power { get { return _recipe.Procedure8Power; } }
        public ushort Procedure9Power { get { return _recipe.Procedure9Power; } }

        public ushort HotOilTime { get { return _recipe.HotOilTime; } }
        public ushort ScrambledEggTime { get { return _recipe.ScrambledEggTime; } }
        public ushort Procedure4StirFryTime { get { return _recipe.Procedure4StirFryTime; } }
        public ushort Procedure5StirFryTime { get { return _recipe.Procedure5StirFryTime; } }
        public ushort Procedure6StirFryTime { get { return _recipe.Procedure6StirFryTime; } }
        public ushort Procedure7StirFryTime { get { return _recipe.Procedure7StirFryTime; } }
        public ushort Procedure8StirFryTime { get { return _recipe.Procedure8StirFryTime; } }
        public ushort Procedure9StirFryTime { get { return _recipe.Procedure9StirFryTime; } }

        public ushort HotAPotTemperature { get { return _recipe.HotAPotTemperature; } }
        public ushort HotBPotTemperature { get { return _recipe.HotBPotTemperature; } }
        public ushort HotCPotTemperature { get { return _recipe.HotCPotTemperature; } }
        public ushort HotDPotTemperature { get { return _recipe.HotDPotTemperature; } }

        public byte DippingProcedureNumber { get { return _recipe.DippingProcedureNumber; } }

        public string FoodMaterial1Contents { get { return _foodMaterials[0].Name; } }

        public byte FoodMaterial1ProcedureNumber { get { return _foodMaterials[0].ProcedureNumber; } }

        public string FoodMaterial2Contents { get { return _foodMaterials[1].Name; } }

        public byte FoodMaterial2ProcedureNumber { get { return _foodMaterials[1].ProcedureNumber; } }

        public string FoodMaterial3Contents { get { return _foodMaterials[2].Name; } }

        public byte FoodMaterial3ProcedureNumber { get { return _foodMaterials[2].ProcedureNumber; } }

        public string FoodMaterial4Contents { get { return _foodMaterials[3].Name; } }

        public byte FoodMaterial4ProcedureNumber { get { return _foodMaterials[3].ProcedureNumber; } }

        public ushort AmountOfOil { get { return _recipe.AmountOfOil; } }
        public ushort AmountOfEggSauce { get { return _recipe.AmountOfEggSauce; } }
        public ushort AmountOfDip { get { return _recipe.AmountOfDip; } }

        public Recipe Recipe
        {
            get { return _recipe != _dummyRecipe ? _recipe : null; }
            set
            {
                if (value != Recipe)
                {
                    _recipe = value ?? _dummyRecipe;

                    foreach (var foodMaterial in _foodMaterials)
                    {
                        foodMaterial.Name = string.Empty;
                        foodMaterial.ProcedureNumber = (byte)0;
                    }

                    int i = 0;
                    foreach (var foodMaterial in _recipe.FoodMaterials)
                    {
                        _foodMaterials[i].Name = foodMaterial.Name;
                        _foodMaterials[i].ProcedureNumber = foodMaterial.ProcedureNumber;
                        ++i;
                    }

                    RaisePropertyChanged(nameof(Name));
                    RaisePropertyChanged(nameof(HotPotPower));
                    RaisePropertyChanged(nameof(HotOilPower));
                    RaisePropertyChanged(nameof(ScrambledEggPower));
                    RaisePropertyChanged(nameof(Procedure4Power));
                    RaisePropertyChanged(nameof(Procedure5Power));
                    RaisePropertyChanged(nameof(Procedure6Power));
                    RaisePropertyChanged(nameof(Procedure7Power));
                    RaisePropertyChanged(nameof(Procedure8Power));
                    RaisePropertyChanged(nameof(Procedure9Power));
                    RaisePropertyChanged(nameof(HotOilTime));
                    RaisePropertyChanged(nameof(ScrambledEggTime));
                    RaisePropertyChanged(nameof(Procedure4StirFryTime));
                    RaisePropertyChanged(nameof(Procedure5StirFryTime));
                    RaisePropertyChanged(nameof(Procedure6StirFryTime));
                    RaisePropertyChanged(nameof(Procedure7StirFryTime));
                    RaisePropertyChanged(nameof(Procedure8StirFryTime));
                    RaisePropertyChanged(nameof(Procedure9StirFryTime));
                    RaisePropertyChanged(nameof(HotAPotTemperature));
                    RaisePropertyChanged(nameof(HotBPotTemperature));
                    RaisePropertyChanged(nameof(HotCPotTemperature));
                    RaisePropertyChanged(nameof(HotDPotTemperature));
                    RaisePropertyChanged(nameof(DippingProcedureNumber));
                    RaisePropertyChanged(nameof(FoodMaterial1Contents));
                    RaisePropertyChanged(nameof(FoodMaterial1ProcedureNumber));
                    RaisePropertyChanged(nameof(FoodMaterial2Contents));
                    RaisePropertyChanged(nameof(FoodMaterial2ProcedureNumber));
                    RaisePropertyChanged(nameof(FoodMaterial3Contents));
                    RaisePropertyChanged(nameof(FoodMaterial3ProcedureNumber));
                    RaisePropertyChanged(nameof(FoodMaterial4Contents));
                    RaisePropertyChanged(nameof(FoodMaterial4ProcedureNumber));
                    RaisePropertyChanged(nameof(AmountOfOil));
                    RaisePropertyChanged(nameof(AmountOfEggSauce));
                    RaisePropertyChanged(nameof(AmountOfDip));
                }
            }
        }

        public RecipeViewModel()
        {
            _recipe = _dummyRecipe;
        }
    }
}
