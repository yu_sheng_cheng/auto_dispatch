﻿using FriedRiceMachine.POS;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml;
using System.Xml.Serialization;
using Widget.WPF;

namespace FriedRiceMachine.ViewModel
{
    class SelectableRecipe : ViewModelBase
    {
        private bool _isSelected;

        public KDS.Recipe Recipe { get; }

        public string Name { get { return Recipe.Name; } }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (value != _isSelected)
                {
                    _isSelected = value;
                    RaisePropertyChanged(nameof(IsSelected));
                }
            }
        }

        public SelectableRecipe(KDS.Recipe recipe)
        {
            Recipe = recipe;
        }
    }

    class MainViewModel : ViewModelBase
    {
        private readonly Menu _menu;
        private readonly List<SelectableRecipe> _recipes = new List<SelectableRecipe>();
        [ContractPublicPropertyName("CreatingOrder")]
        private Order _order = new Order(1);
        private SelectableRecipe _selectedRecipe;

        private bool _onHomePage = true;
        private bool _isCreatingOrder;
        private bool _isEditingRecipe;

        public KDSViewModel KDSViewModel { get; set; }

        public Order CreatingOrder { get { return _order; } }

        public IEnumerable<FriedRiceItem> FriedRiceItems { get { return _menu.FriedRiceItems; } }

        public FriedRiceItemViewModel FriedRiceItemViewModel { get; }

        public bool HasSelectedFriedRiceItem { get { return FriedRiceItemViewModel.Item != null; } }

        public IEnumerable<SelectableRecipe> Recipes { get { return _recipes; } }

        public RecipeViewModel RecipeViewModel { get; } = new RecipeViewModel();

        public bool HasSelectedRecipe { get { return _selectedRecipe != null; } }

        public bool OnHomePage
        {
            get { return _onHomePage; }
            set
            {
                if (value != _onHomePage)
                {
                    _onHomePage = value;
                    RaisePropertyChanged(nameof(OnHomePage));
                }
            }
        }

        public bool IsCreatingOrder
        {
            get { return _isCreatingOrder; }
            set
            {
                if (value != _isCreatingOrder)
                {
                    _isCreatingOrder = value;
                    RaisePropertyChanged(nameof(IsCreatingOrder));
                }
            }
        }

        public bool IsEditingRecipe
        {
            get { return _isEditingRecipe; }
            set
            {
                if (value != _isEditingRecipe)
                {
                    _isEditingRecipe = value;
                    RaisePropertyChanged(nameof(IsEditingRecipe));
                }
            }
        }

        public ICommand GoHomeCommand { get; }

        public ICommand CreateOrderCommand { get; }

        public ICommand SelectFriedRiceItemCommand { get; }

        public ICommand DeleteOrderItemCommand { get; }

        public ICommand CancelOrderCommand { get; }

        public ICommand EditRecipeCommand { get; }

        public ICommand SelectRecipeCommand { get; }

        public ICommand UnselectRecipeCommand { get; }

        public MainViewModel()
        {
            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"POS\Menu.xml"), Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(Menu));
                _menu = (Menu)serializer.Deserialize(reader);
            }

            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"KDS\Cookbook.xml"), Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(KDS.Cookbook));
                KDS.Cookbook cookbook = (KDS.Cookbook)serializer.Deserialize(reader);
                foreach (var recipe in cookbook.Recipes)
                {
                    _recipes.Add(new SelectableRecipe(recipe));
                }
            }

            FriedRiceItemViewModel = new FriedRiceItemViewModel(_menu);

            GoHomeCommand = new RelayCommand(GoHome);
            CreateOrderCommand = new RelayCommand(CreateOrder);
            SelectFriedRiceItemCommand = new RelayCommand<FriedRiceItem>(SelectFriedRiceItem);
            DeleteOrderItemCommand = new RelayCommand<OrderItem>(DeleteOrderItem);
            CancelOrderCommand = new RelayCommand(CancelOrder);
            EditRecipeCommand = new RelayCommand(EditRecipe);
            SelectRecipeCommand = new RelayCommand<SelectableRecipe>(SelectRecipe);
            UnselectRecipeCommand = new RelayCommand(UnselectRecipe);
        }

        private void GoHome()
        {
            if (_onHomePage)
                return;

            if (_isCreatingOrder)
            {
                //CancelOrder();
                _order.Reset();
                FriedRiceItemViewModel.Item = null;
                IsCreatingOrder = false;
            }
            else if (_isEditingRecipe)
            {
                UnselectRecipe();
                IsEditingRecipe = false;
            }

            OnHomePage = true;
        }

        private void CreateOrder()
        {
            if (!_onHomePage)
                return;

            OnHomePage = false;
            IsCreatingOrder = true;
        }

        public void SelectFoodSeries(FoodSeries series)
        {
            FriedRiceItemViewModel.Item = null;
        }

        private void SelectFriedRiceItem(FriedRiceItem item)
        {
            if (!IsCreatingOrder)
                return;

            FriedRiceItemViewModel.Item = item;
        }

        public bool OrderFriedRice()
        {
            if (!HasSelectedFriedRiceItem)
                return false;

            FriedRiceItem item = FriedRiceItemViewModel.Item;

            FriedRice friedRice = new FriedRice(item.Name, (FriedRiceFlavor)FriedRiceItemViewModel.Flavor);
            friedRice.HasAddedShallot = (bool)FriedRiceItemViewModel.HasAddedShallot;
            friedRice.HasAddedOnion = (bool)FriedRiceItemViewModel.HasAddedOnion;
            friedRice.HasAddedFungus = (bool)FriedRiceItemViewModel.HasAddedFungus;
            friedRice.HasAddedCarrot = (bool)FriedRiceItemViewModel.HasAddedCarrot;
            friedRice.HasAddedCabbage = (bool)FriedRiceItemViewModel.HasAddedCabbage;
            friedRice.HasAddedChili = (bool)FriedRiceItemViewModel.HasAddedChili;
            friedRice.IsLessRice = (bool)FriedRiceItemViewModel.IsLessRice;

            OrderItem orderItem = new FriedRiceOrderItem(friedRice, FriedRiceItemViewModel.Amount, FriedRiceItemViewModel.UnitPrice);
            _order.AddItem(orderItem);

            FriedRiceItemViewModel.Item = null;
            return true;
        }

        public void DeleteOrderItem(OrderItem item)
        {
            _order.RemoveItem(item);
        }

        public bool PostOrder()
        {
            if (!_order.HasOrderItem)
                return false;

            KDSViewModel.SendOrder(_order);

            _order.Reset(unchecked((ushort)(_order.Number + 1)));
            FriedRiceItemViewModel.Item = null;
            return true;
        }

        private void CancelOrder()
        {
            if (!_order.HasOrderItem)
                return;
            _order.Reset();
        }

        private void EditRecipe()
        {
            if (!_onHomePage)
                return;

            OnHomePage = false;
            IsEditingRecipe = true;
        }

        private void SelectRecipe(SelectableRecipe recipe)
        {
            if (!_isEditingRecipe | recipe == _selectedRecipe)
                return;

            if (_selectedRecipe != null)
                _selectedRecipe.IsSelected = false;

            recipe.IsSelected = true;
            RecipeViewModel.Recipe = recipe.Recipe;

            bool needsRaise = _selectedRecipe == null;

            _selectedRecipe = recipe;

            if (needsRaise)
                RaisePropertyChanged(nameof(HasSelectedRecipe));
        }

        private void UnselectRecipe()
        {
            if (_selectedRecipe != null)
            {
                _selectedRecipe.IsSelected = false;
                _selectedRecipe = null;
                RaisePropertyChanged(nameof(HasSelectedRecipe));
            }
            RecipeViewModel.Recipe = null;
        }

        public void UpdateRecipe(KDS.Recipe newRecipe)
        {
            SelectableRecipe selectableRecipe = _recipes.FirstOrDefault(sr => sr.Recipe.Name == newRecipe.Name);
            if (selectableRecipe == null)
                return;

            KDS.Recipe recipe = selectableRecipe.Recipe;

            recipe.HotPotPower = newRecipe.HotPotPower;
            recipe.HotOilPower = newRecipe.HotOilPower;
            recipe.ScrambledEggPower = newRecipe.ScrambledEggPower;
            recipe.Procedure4Power = newRecipe.Procedure4Power;
            recipe.Procedure5Power = newRecipe.Procedure5Power;
            recipe.Procedure6Power = newRecipe.Procedure6Power;
            recipe.Procedure7Power = newRecipe.Procedure7Power;
            recipe.Procedure8Power = newRecipe.Procedure8Power;
            recipe.Procedure9Power = newRecipe.Procedure9Power;

            recipe.HotOilTime = newRecipe.HotOilTime;
            recipe.ScrambledEggTime = newRecipe.ScrambledEggTime;
            recipe.Procedure4StirFryTime = newRecipe.Procedure4StirFryTime;
            recipe.Procedure5StirFryTime = newRecipe.Procedure5StirFryTime;
            recipe.Procedure6StirFryTime = newRecipe.Procedure6StirFryTime;
            recipe.Procedure7StirFryTime = newRecipe.Procedure7StirFryTime;
            recipe.Procedure8StirFryTime = newRecipe.Procedure8StirFryTime;
            recipe.Procedure9StirFryTime = newRecipe.Procedure9StirFryTime;

            recipe.HotAPotTemperature = newRecipe.HotAPotTemperature;
            recipe.HotBPotTemperature = newRecipe.HotBPotTemperature;
            recipe.HotCPotTemperature = newRecipe.HotCPotTemperature;
            recipe.HotDPotTemperature = newRecipe.HotDPotTemperature;

            recipe.DippingProcedureNumber = newRecipe.DippingProcedureNumber;

            int count = newRecipe.FoodMaterials.Length;
            recipe.FoodMaterials = new KDS.FoodMaterial[count];
            Array.Copy(newRecipe.FoodMaterials, recipe.FoodMaterials, count);

            recipe.AmountOfOil = newRecipe.AmountOfOil;
            recipe.AmountOfEggSauce = newRecipe.AmountOfEggSauce;
            recipe.AmountOfDip = newRecipe.AmountOfDip;

            KDS.Cookbook cookbook = new KDS.Cookbook();
            cookbook.Recipes = new KDS.Recipe[_recipes.Count];

            int i = 0;
            foreach (var sr in _recipes)
            {
                cookbook.Recipes[i++] = sr.Recipe;
            }

            XmlSerializer serializer = new XmlSerializer(typeof(KDS.Cookbook));

            using (var fs = new FileStream(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"KDS\Cookbook.xml"), FileMode.Create))
            {
                using (var writer = new XmlTextWriter(fs, Encoding.UTF8))
                {
                    writer.Formatting = Formatting.Indented;

                    serializer.Serialize(writer, cookbook);
                }
            }

            KDSViewModel.UpdateRecipe(recipe);
        }
    }
}
