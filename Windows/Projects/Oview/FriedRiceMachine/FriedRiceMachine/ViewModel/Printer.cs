﻿using FriedRiceMachine.POS;
using SwissKnife.Exceptions;
using SwissKnife.Log;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Unity;

namespace FriedRiceMachine.ViewModel
{
    class Label
    {
        public ushort OrderNumber { get; set; }

        public OrderSource Source { get; set; }

        public ushort SourceOrderNumber { get; set; }

        public int ItemCount { get; set; }

        public int CompletionCount { get; set; }

        public string Name { get; set; }

        public FriedRiceFlavor Flavor { get; set; }

        public bool HasAddedShallot { get; set; }
       
        public bool HasAddedOnion { get; set; }
      
        public bool HasAddedFungus { get; set; }

        public bool HasAddedCarrot { get; set; }

        public bool HasAddedCabbage { get; set; }

        public bool HasAddedChili { get; set; }

        public bool IsLessRice { get; set; }
    }

    class Printer : IDisposable
    {
        private readonly ILog _log;
        private readonly PrintDocument _printDocument = new PrintDocument();
        private readonly ConcurrentQueue<Label> _labels = new ConcurrentQueue<Label>();
        private Label _printingLabel;
        private int _labelCount;

        public Printer()
        {
            IUnityContainer container = Environment.UnityContainer;

            _log = container.Resolve<ILog>();

            _printDocument.PrintController = new StandardPrintController();
            _printDocument.PrinterSettings.PrinterName = "Argox iX4-240 PPLZ";
            _printDocument.PrintPage += PrintDocument_PrintPage;
        }

        public void Dispose()
        {
            _printDocument.PrintPage -= PrintDocument_PrintPage;
            _printDocument.Dispose();
        }

        public void PrintLable(Label label)
        {
            _labels.Enqueue(label);
            if (Interlocked.Increment(ref _labelCount) == 1)
            {
                ThreadPool.QueueUserWorkItem((_) =>
                {
                    try
                    {
                        while (true)
                        {
                            bool success = _labels.TryDequeue(out _printingLabel);
                            if (!success)
                                throw new BugException("Should not go in here.");

                            _printDocument.Print();

                            if (Interlocked.Decrement(ref _labelCount) == 0)
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        _log.f("Printer", "Print:", e);
                    }
                });
            }
        }

        private void PrintDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            Label label = _printingLabel;
           
            _printingLabel = null;

            Graphics graphics = e.Graphics;

            using (Font font = new Font("Microsoft JhengHei", 10, FontStyle.Bold))
            using (Font mediumFont = new Font("Microsoft JhengHei", 12, FontStyle.Bold))
            using (Font largeFont = new Font("Microsoft JhengHei", 14, FontStyle.Bold))
            using (StringFormat sfCenterNear = new StringFormat())
            using (StringFormat sfFarNear = new StringFormat())
            using (StringFormat sfFarFar = new StringFormat())
            using (StringFormat sfNearFar = new StringFormat())
            {
                sfCenterNear.Alignment = StringAlignment.Center;
                sfCenterNear.LineAlignment = StringAlignment.Near;
                sfFarNear.Alignment = StringAlignment.Far;
                sfFarNear.LineAlignment = StringAlignment.Near;
                sfFarFar.Alignment = StringAlignment.Far;
                sfFarFar.LineAlignment = StringAlignment.Far;
                sfNearFar.Alignment = StringAlignment.Near;
                sfNearFar.LineAlignment = StringAlignment.Far;

                graphics.DrawString(label.OrderNumber.ToString("0000"), font, Brushes.Black, 6, 4);

                Rectangle bounds = new Rectangle(6, 4, 188, font.Height);

                switch (label.Source)
                {
                    case OrderSource.FengKuai:
                        graphics.DrawString("H", font, Brushes.Black, bounds, sfCenterNear);
                        break;
                    case OrderSource.Line:
                        graphics.DrawString("L", font, Brushes.Black, bounds, sfCenterNear);
                        break;
                    case OrderSource.Foodpanda:
                        graphics.DrawString($"F#{label.SourceOrderNumber.ToString()}", font, Brushes.Black, bounds, sfCenterNear);
                        break;
                }

                bounds = new Rectangle(0, 4, 200, font.Height);
                graphics.DrawString($"{label.CompletionCount.ToString()}/{label.ItemCount.ToString()}", font, Brushes.Black, bounds, sfFarNear);

                bounds = new Rectangle(0, 24, 200, font.Height + 4);

                switch(label.Flavor)
                {
                    case FriedRiceFlavor.PrimaryTaste:
                        graphics.DrawString($"原味 {label.Name}", mediumFont, Brushes.Black, bounds, sfCenterNear);
                        break;
                    case FriedRiceFlavor.SpicedSalt:
                        graphics.DrawString($"白胡椒 {label.Name}", mediumFont, Brushes.Black, bounds, sfCenterNear);
                        break;
                    case FriedRiceFlavor.BlackPepper:
                        graphics.DrawString($"黑胡椒 {label.Name}", mediumFont, Brushes.Black, bounds, sfCenterNear);
                        break;
                    case FriedRiceFlavor.MildSpicy:
                        graphics.DrawString($"川辣(小) {label.Name}", mediumFont, Brushes.Black, bounds, sfCenterNear);
                        break;
                    case FriedRiceFlavor.MediumSpicy:
                        graphics.DrawString($"川辣(中) {label.Name}", mediumFont, Brushes.Black, bounds, sfCenterNear);
                        break;
                    case FriedRiceFlavor.HotSpicy:
                        graphics.DrawString($"川辣(大) {label.Name}", mediumFont, Brushes.Black, bounds, sfCenterNear);
                        break;
                }

                //graphics.DrawString($"{label.Name} ({SR.ToString(label.Flavor)})", mediumFont, Brushes.Black, bounds, sfCenterNear);
  
                StringBuilder sb = new StringBuilder();
                int n = 0;

                //sb.Append("不加");
                if (!label.HasAddedShallot)
                {
                    ++n;
                    sb.Append("X青蔥");
                }
                if (!label.HasAddedOnion)
                {
                    ++n;
                    sb.Append(n == 1 ? "X洋蔥" : ",X洋蔥");
                }
                if (!label.HasAddedFungus)
                {
                    ++n;
                    sb.Append(n == 1 ? "X木耳" : ",X木耳");
                }
                if (!label.HasAddedCarrot)
                {
                    ++n;
                    sb.Append(n == 1 ? "X紅蘿蔔" : ",X紅蘿蔔");
                }
                if (!label.HasAddedCabbage)
                {
                    ++n;
                    sb.Append(n == 1 ? "X高麗菜" : ",X高麗菜");
                }

                if (n > 0)
                {
                    bounds = new Rectangle(0, 48, 200, font.Height + 4);
                    graphics.DrawString(sb.ToString(), font, Brushes.Black, bounds, sfCenterNear);
                }

                sb.Clear();
                n = 0;

                if (label.HasAddedChili)
                {
                    ++n;
                    sb.Append("+生辣椒");
                }
                if (label.IsLessRice)
                {
                    ++n;
                    sb.Append(n == 1 ? "飯少" : " 飯少");
                }

                if (n > 0)
                {
                    bounds = new Rectangle(0, 64, 200, font.Height + 4);
                    graphics.DrawString(sb.ToString(), font, Brushes.Black, bounds, sfCenterNear);
                }

                bounds = new System.Drawing.Rectangle(0, 0, 196, 110);
                graphics.DrawString("建議趁熱享用", font, Brushes.Black, bounds, sfFarFar);

                bounds = new System.Drawing.Rectangle(6, 0, 194, 115);
                graphics.DrawString("豐快", largeFont, Brushes.Black, bounds, sfNearFar);
            }
        }
    }
}
