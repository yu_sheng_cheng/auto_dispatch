﻿using FriedRiceMachine.KDS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Widget.WPF;

namespace FriedRiceMachine.ViewModel
{
    enum PreparationReason : byte
    {
        NormalOrder,
        OrderInserting,
        Redo
    }

    class PreparationNotification : ViewModelBase
    {
        private bool _visible = true;
        private string _potCode = string.Empty;

        public bool Visible
        {
            get { return _visible; }
            set
            {
                if (value != _visible)
                {
                    _visible = value;
                    RaisePropertyChanged(nameof(Visible));
                }
            }
        }

        public PreparationReason Reason { get; }

        public ushort OrderNumber { get; }

        public string Title { get; private set; }

        public string PotCode
        {
            get { return _potCode; }
            set
            {
                if (value != _potCode)
                {
                    _potCode = value;
                    RaisePropertyChanged(nameof(PotCode));
                }
            }
        }

        //public string[] Contents { get; }

        public bool HasAddedShallot { get; }

        public bool HasAddedOnion { get; }

        public bool HasAddedFungus { get; }

        public bool HasAddedCarrot { get; }

        public bool HasAddedCabbage { get; }

        public bool HasAddedChili { get; }

        public bool IsLessRice { get; }

        public PreparationNotification(OrderItem orderItem)
        {
            if (orderItem.Redo)
                Reason = PreparationReason.Redo;
            else if (orderItem.IsOrderInserting)
                Reason = PreparationReason.OrderInserting;
            else
                Reason = PreparationReason.NormalOrder;

            OrderNumber = orderItem.OrderNumber;

            Recipe recipe = orderItem.Recipe;

            Title = $"{OrderNumber.ToString("0000")} {recipe.Name} ({SR.ToString(orderItem.Flavor)})";

            /*Contents = new string[recipe.FoodMaterials.Length];

            int i = 0;
            foreach (var foodMaterial in recipe.FoodMaterials)
            {
                Contents[i++] = foodMaterial.Name;
            }*/

            HasAddedShallot = orderItem.HasAddedShallot;
            HasAddedOnion = orderItem.HasAddedOnion;
            HasAddedFungus = orderItem.HasAddedFungus;
            HasAddedCarrot = orderItem.HasAddedCarrot;
            HasAddedCabbage = orderItem.HasAddedCabbage;
            HasAddedChili = orderItem.HasAddedChili;
            IsLessRice = orderItem.IsLessRice;
        }
    }
}
