﻿#define HAS_PLC
using Automation.Core;
using FriedRiceMachine.KDS;
using SwissKnife.Exceptions;
using SwissKnife.Log;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using System.Xml.Serialization;
using Unity;
using Widget.WPF;

namespace FriedRiceMachine.ViewModel
{
    class KDSViewModel : ViewModelBase, IDisposable
    {
        private const byte MAX_PREPARATION_NOTIFICATIONS = 7;

        private readonly ILog _log;
        private readonly Dispatcher _dispatcher = Dispatcher.CurrentDispatcher;
        private readonly KDS.FriedRiceMachine _machine;
        private readonly Dictionary<string, Recipe> _recipeDictionary = new Dictionary<string, Recipe>();
        private readonly Dictionary<ushort, Order> _orderDictionary = new Dictionary<ushort, Order>();
        private readonly DispatcherTimer _pollingTimer = new DispatcherTimer();
        private readonly Printer _printer;
        private readonly List<OrderItem> _orderItems = new List<OrderItem>();
        private bool _isPreparationNotificationBufferEmpty = true;
        private bool _orderUnderProcessing;
        private sbyte _preparationNotificationCollectionVisualOffset;

        public FriedRicePotViewModel[] Pots { get; }

        public ObservableCollection<PreparationNotification> PreparationNotifications { get; } = new ObservableCollection<PreparationNotification>();

        public int QuantityOfPreparation { get { return _orderItems.Count; } }

        public bool IsPreparationNotificationBufferEmpty
        {
            get { return _isPreparationNotificationBufferEmpty; }
            set
            {
                if (value != _isPreparationNotificationBufferEmpty)
                {
                    _isPreparationNotificationBufferEmpty = value;
                    RaisePropertyChanged(nameof(IsPreparationNotificationBufferEmpty));
                }
            }
        }

        private int PreparationNotificationCount { get { return PreparationNotifications.Count + _preparationNotificationCollectionVisualOffset; } }

        public ICommand RemovePreparationNotificationCommand { get; }

        public KDSViewModel()
        {
            IUnityContainer container = Environment.UnityContainer;

            _log = container.Resolve<ILog>();

            using (var reader = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"KDS\Cookbook.xml"), Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(Cookbook));
                Cookbook cookbook = (Cookbook)serializer.Deserialize(reader);
                foreach (var recipe in cookbook.Recipes)
                {
                    if (!_recipeDictionary.ContainsKey(recipe.Name))
                    {
                        _recipeDictionary.Add(recipe.Name, recipe);
                        Array.Sort(recipe.FoodMaterials, 0, recipe.FoodMaterials.Length);
                    }
                }
            }

            _pollingTimer.Interval = TimeSpan.FromMilliseconds(100);
            _pollingTimer.Tick += PollingTimer_Expired;

            RemovePreparationNotificationCommand = new RelayCommand<PreparationNotification>(RemovePreparationNotification);

            _printer = new Printer();

            try
            {
                _machine = new KDS.FriedRiceMachine();

                Pots = new FriedRicePotViewModel[_machine.Pots.Length];
                int i = 0;
                foreach (var pot in _machine.Pots)
                {
                    Pots[i++] = new FriedRicePotViewModel(pot.Code);
                }
            }
            catch
            {
                _printer.Dispose();
                throw;
            }

            _machine.Connect("FeedingCompleted", FriedRicePot_FeedingCompleted);
            _machine.Connect("FriedRiceMakeCompleted", FriedRicePot_FriedRiceMakeCompleted);
            _machine.Connect("FriedRiceMakeFailed", FriedRicePot_FriedRiceMakeFailed);
            _machine.Start();

            _pollingTimer.Start();
        }

        public void Dispose()
        {
            _pollingTimer.Stop();
#if HAS_PLC
            //_machine.Dispose();
            _machine.DisposeAsync().Wait(1000);
#else
            _machine.DisposeAsync().Wait(1000);
#endif
            _printer.Dispose();
        }

        private void PollingTimer_Expired(object sender, EventArgs e)
        {
            int i = 0;
            foreach (var pot in _machine.Pots)
            {
                FriedRicePotViewModel viewModel = Pots[i++];
                viewModel.UpdateAlarmState(pot.HasAlarm);
                viewModel.UpdateMode(pot.AutomatedMode);
                viewModel.UpdateState(pot.State);
                viewModel.UpdateFriedRiceProgress(pot.Progress);
                viewModel.UpdateOrder(pot.ReceivedOrderItem);
            }

            if (_orderUnderProcessing)
            {
                int preparationNotificationCount = PreparationNotificationCount;

                if (preparationNotificationCount > 0)
                {
                    var pot = _orderItems[0].Pot;

                    PreparationNotifications[-_preparationNotificationCollectionVisualOffset].PotCode = pot != null ? pot.Code : string.Empty;
                }
            }
        }

        public void SendOrder(POS.Order posOrder)
        {
            ushort orderNumber = posOrder.Number;
            bool isOrderInserting = posOrder.IsOrderInserting;

            Order kdsOrder = null;
            List<OrderItem> kdsOrderItems = null;

            foreach (var posOrderItem in posOrder.Items)
            {
                if (posOrderItem.FoodSeries != POS.FoodSeries.FriedRice)
                    continue;

                POS.FriedRiceOrderItem friedRiceOrderItem = posOrderItem as POS.FriedRiceOrderItem;
                POS.FriedRice friedRice = friedRiceOrderItem.FriedRice;

                string name = friedRice.Name;
                Recipe recipe;

                if (!_recipeDictionary.ContainsKey(name))
                {
                    _log.d("KDSViewModel", $"{name} recipe does not exist");
                    continue;
                }

                recipe = _recipeDictionary[name];

                if (kdsOrder == null)
                {
                    kdsOrder = new Order(orderNumber, (POS.OrderSource)posOrder.Source, posOrder.SourceOrderNumber, isOrderInserting);
                    kdsOrderItems = new List<OrderItem>();
                }

                int amount = posOrderItem.Amount;
                POS.FriedRiceFlavor flavor = friedRice.Flavor;

                while (amount-- > 0)
                {
                    OrderItem item = new OrderItem(orderNumber, recipe, flavor, isOrderInserting);
                    item.HasAddedShallot = friedRice.HasAddedShallot;
                    item.HasAddedOnion = friedRice.HasAddedOnion;
                    item.HasAddedFungus = friedRice.HasAddedFungus;
                    item.HasAddedCarrot = friedRice.HasAddedCarrot;
                    item.HasAddedCabbage = friedRice.HasAddedCabbage;
                    item.HasAddedChili = friedRice.HasAddedChili;
                    item.IsLessRice = friedRice.IsLessRice;

                    kdsOrderItems.Add(item);
                }
            }

            if (kdsOrder == null || kdsOrderItems.Count == 0)
                return;

            kdsOrder.ItemCount = kdsOrderItems.Count;
            _orderDictionary.Add(orderNumber, kdsOrder);

            if (isOrderInserting)
                InsertOrderItems(kdsOrderItems, false);
            else
            {
                int insertionIndex = _orderItems.Count;

                _orderItems.AddRange(kdsOrderItems);
                RaisePropertyChanged(nameof(QuantityOfPreparation));
                IsPreparationNotificationBufferEmpty = _orderItems.Count <= MAX_PREPARATION_NOTIFICATIONS;

                AddPreparationNotification(insertionIndex);

                if (!_orderUnderProcessing)
                {
                    _orderUnderProcessing = true;
                    _machine.PostMessage(new Message(KDS.FriedRiceMachine.MessageClassFriedRiceMachine, KDS.FriedRiceMachine.OrderReceivedMessage, _orderItems[0]));
                }
            }
        }

        private void RemovePreparationNotification(PreparationNotification notification)
        {
            if (!PreparationNotifications.Remove(notification))
                return;

            ++_preparationNotificationCollectionVisualOffset;

            int preparationNotificationCount = PreparationNotificationCount;

            if (_orderItems.Count > preparationNotificationCount)
                PreparationNotifications.Add(new PreparationNotification(_orderItems[preparationNotificationCount]));
        }

        public void UpdateRecipe(Recipe recipe)
        {
            if (!_recipeDictionary.ContainsKey(recipe.Name))
                return;

            _recipeDictionary.Remove(recipe.Name);
            _recipeDictionary.Add(recipe.Name, recipe);
            Array.Sort(recipe.FoodMaterials, 0, recipe.FoodMaterials.Length);
        }

        private void FriedRicePot_FeedingCompleted(SignalArgs args)
        {
            _dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
            {               
                _orderItems.RemoveAt(0);
                RaisePropertyChanged(nameof(QuantityOfPreparation));
                IsPreparationNotificationBufferEmpty = _orderItems.Count <= MAX_PREPARATION_NOTIFICATIONS;

                int preparationNotificationCount = PreparationNotificationCount;
                if (preparationNotificationCount > 0)
                {
                    PreparationNotifications[-_preparationNotificationCollectionVisualOffset].Visible = false;
                    --_preparationNotificationCollectionVisualOffset;
                }

                if (_orderItems.Count > 0)
                    _machine.PostMessage(new Message(KDS.FriedRiceMachine.MessageClassFriedRiceMachine, KDS.FriedRiceMachine.OrderReceivedMessage, _orderItems[0]));
                else
                    _orderUnderProcessing = false;
            }));
        }

        private void FriedRicePot_FriedRiceMakeCompleted(SignalArgs args)
        {
            _dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
            {
                OrderItem orderItem = (args as FriedRiceMakeCompletedSignalArgs).OrderItem;
                Order order = _orderDictionary[orderItem.OrderNumber];

                int completionCount = order.CompletionCount + 1;

                order.CompletionCount = completionCount;

                Label label = new Label();
                label.OrderNumber = order.Number;
                label.Source = order.Source;
                label.SourceOrderNumber = order.SourceOrderNumber;
                label.ItemCount = order.ItemCount;
                label.CompletionCount = order.CompletionCount;
                label.Name = orderItem.Recipe.Name;
                label.Flavor = orderItem.Flavor;
                label.HasAddedShallot = orderItem.HasAddedShallot;
                label.HasAddedOnion = orderItem.HasAddedOnion;
                label.HasAddedFungus = orderItem.HasAddedFungus;
                label.HasAddedCarrot = orderItem.HasAddedCarrot;
                label.HasAddedCabbage = orderItem.HasAddedCabbage;
                label.HasAddedChili = orderItem.HasAddedChili;
                label.IsLessRice = orderItem.IsLessRice;

                _printer.PrintLable(label);

                if (completionCount == order.ItemCount)
                    _orderDictionary.Remove(order.Number);
            }));
        }

        private void FriedRicePot_FriedRiceMakeFailed(SignalArgs args)
        {
            _dispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(() =>
            {
                OrderItem orderItem = (args as FriedRiceMakeFailedSignalArgs).OrderItem;
                orderItem.Redo = true;

                InsertOrderItems(new OrderItem[] { orderItem }, true);
            }));
        }

        private void InsertOrderItems(ICollection<OrderItem> orderItems, bool redo)
        {
            if (_orderUnderProcessing)
            {
                int insertionIndex = -1;

                OrderItem firstOrderItem = _orderItems[0];

                bool needsTryCancel = false;

                if (!firstOrderItem.Redo)
                {
                    if (redo | !firstOrderItem.IsOrderInserting)
                        needsTryCancel = true;
                }

                if (needsTryCancel)
                {
                    if (_machine.TryCancelOrder())
                    {
                        _orderUnderProcessing = false;
                        insertionIndex = 0;
                    }
                }

                if (insertionIndex == -1)
                    insertionIndex = FindInsertionIndex(redo);

                _orderItems.InsertRange(insertionIndex, orderItems);
                RaisePropertyChanged(nameof(QuantityOfPreparation));
                IsPreparationNotificationBufferEmpty = _orderItems.Count <= MAX_PREPARATION_NOTIFICATIONS;

                int preparationNotificationCount = PreparationNotificationCount;

                if (insertionIndex < preparationNotificationCount)
                {
                    int insertionPosition = -_preparationNotificationCollectionVisualOffset + insertionIndex;
                    int max = MAX_PREPARATION_NOTIFICATIONS - insertionPosition;
                    int count = preparationNotificationCount - insertionIndex;

                    if (orderItems.Count < max)
                    {
                        int insertionCount = orderItems.Count;

                        int n = (max - insertionCount) - count;
                        if (n < 0)
                        {
                            while (n++ < 0)
                            {
                                PreparationNotifications.RemoveAt(PreparationNotifications.Count - 1);
                            }
                        }

                        int i = insertionIndex;

                        while (insertionCount-- > 0)
                        {
                            PreparationNotifications.Insert(insertionPosition++, new PreparationNotification(_orderItems[i++]));
                        }
                    }
                    else
                    {
                        while (count-- > 0)
                            PreparationNotifications.RemoveAt(PreparationNotifications.Count - 1);

                        int i = insertionIndex;

                        while (max-- > 0)
                        {
                            PreparationNotifications.Add(new PreparationNotification(_orderItems[i++]));
                        }
                    }
                }
                else if (insertionIndex == preparationNotificationCount)
                    AddPreparationNotification(insertionIndex);
            }
            else
            {
#if DEBUG
                if (_orderItems.Count != 0)
                    throw new BugException("Should not go in here.");
#endif
                _orderItems.AddRange(orderItems);
                RaisePropertyChanged(nameof(QuantityOfPreparation));
                IsPreparationNotificationBufferEmpty = _orderItems.Count <= MAX_PREPARATION_NOTIFICATIONS;

                AddPreparationNotification(0);
            }

            if (!_orderUnderProcessing)
            {
                _orderUnderProcessing = true;
                _machine.PostMessage(new Message(KDS.FriedRiceMachine.MessageClassFriedRiceMachine, KDS.FriedRiceMachine.OrderReceivedMessage, _orderItems[0]));
            }
        }

        private int FindInsertionIndex(bool redo)
        {
#if DEBUG
            if (_orderItems.Count == 0)
                throw new BugException("Should not go in here.");
#endif
            int count = _orderItems.Count;
            int index = 1;

            for (; index < count; ++index)
            {
                OrderItem orderItem = _orderItems[index];

                if (orderItem.Redo)
                    continue;

                if (redo)
                    break;

                if (!orderItem.IsOrderInserting)
                    break;
            }

            return index;
        }

        private void AddPreparationNotification(int startIndex)
        {
            int count = _orderItems.Count;

            for (int i = startIndex; i < count; ++i)
            {
                if (PreparationNotifications.Count >= MAX_PREPARATION_NOTIFICATIONS)
                    return;

                PreparationNotifications.Add(new PreparationNotification(_orderItems[i]));
            }
        }
    }
}
