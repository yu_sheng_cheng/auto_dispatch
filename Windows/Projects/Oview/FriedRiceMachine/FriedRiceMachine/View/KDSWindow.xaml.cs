﻿using FriedRiceMachine.ViewModel;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Widget;

namespace FriedRiceMachine.View
{
    /// <summary>
    /// KDSWindow.xaml 的互動邏輯
    /// </summary>
    public partial class KDSWindow : MetroWindow
    {
        internal KDSViewModel ViewModel { get { return DataContext as KDSViewModel; } }

        public KDSWindow()
        {
            InitializeComponent();
        }

        private void MetroWindow_SourceInitialized(object sender, EventArgs e)
        {
            HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);
            source.AddHook(new HwndSourceHook(WndProc));
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case Win32.WM_SYSCOMMAND:
                    switch (wParam.ToInt32() & 0xFFF0)
                    {
                        case Win32.SC_MOVE:
                        case Win32.SC_RESTORE:
                            handled = true;
                            break;
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        private void MetroWindow_Closed(object sender, EventArgs e)
        {
            ViewModel.Dispose();
        }

        private void PreparationNotification_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (e.NewSize.Height != 0.0)
                return;

            var element = sender as Grid;
            ViewModel.RemovePreparationNotificationCommand.Execute(element.Tag);
        }

        private void ReminderMessageFlyout_IsOpenChanged(object sender, RoutedEventArgs e)
        {
            Flyout flyout = sender as Flyout;
            flyout.VerticalAlignment = VerticalAlignment.Bottom;
        }
    }
}
