﻿using FriedRiceMachine.POS;
using FriedRiceMachine.ViewModel;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Widget;

namespace FriedRiceMachine.View
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private readonly KDSWindow _kdsWindow;
        private bool _hasSelectedFoodSeries;

        public MainWindow()
        {
            InitializeComponent();

            _kdsWindow = new KDSWindow();

            MainViewModel viewModel = DataContext as MainViewModel;
            viewModel.KDSViewModel = _kdsWindow.ViewModel;
        }

        private void MetroWindow_SourceInitialized(object sender, EventArgs e)
        {
            HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);
            source.AddHook(new HwndSourceHook(WndProc));
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case Win32.WM_SYSCOMMAND:
                    switch (wParam.ToInt32() & 0xFFF0)
                    {
                        case Win32.SC_MOVE:
                        case Win32.SC_RESTORE:
                            handled = true;
                            break;
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (var screen in System.Windows.Forms.Screen.AllScreens)
            {
                if (!screen.Primary)
                {
                    var workingArea = screen.WorkingArea;

                    _kdsWindow.Left = workingArea.Left;
                    _kdsWindow.Top = workingArea.Top;
                }
            }

            _kdsWindow.Owner = this;
            _kdsWindow.Show();
            _kdsWindow.WindowState = WindowState.Maximized;
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = DataContext as MainViewModel;

            viewModel.GoHomeCommand.Execute(null);
            OrderFlipView.SelectedIndex = 0;
        }

        private void OrderFlipView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var flipview = sender as FlipView;

            FriedRiceItemsControl.Visibility = Visibility.Collapsed;
            FriedRiceDetailsGrid.Visibility = Visibility.Collapsed;

            switch (flipview.SelectedIndex)
            {
                case 0:
                case 1:
                    _hasSelectedFoodSeries = false;
                    break;
                case 2:
                    if (_hasSelectedFoodSeries)
                        FriedRiceItemsControl.Visibility = Visibility.Visible;
                    else
                        OrderFlipView.SelectedIndex = 0;
                    break;
                case 3:
                    var viewModel = DataContext as MainViewModel;
                    if (viewModel.HasSelectedFriedRiceItem)
                        FriedRiceDetailsGrid.Visibility = Visibility.Visible;
                    else
                        OrderFlipView.SelectedIndex = 1;
                    break;
            }
        }

        private void SourceOrderNumberNumericUpDown_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        {
            NumericUpDown numericUpDown = sender as NumericUpDown;
            if (numericUpDown.Value.HasValue)
            {
                var viewModel = DataContext as MainViewModel;
                viewModel.CreatingOrder.SourceOrderNumber = (ushort)numericUpDown.Value;
            }
        }

        private void FriedRiceSeriesTile_Click(object sender, RoutedEventArgs e)
        {
            _hasSelectedFoodSeries = true;
            OrderFlipView.SelectedIndex = 2;

            var viewModel = DataContext as MainViewModel;
            viewModel.SelectFoodSeries(FoodSeries.FriedRice);
        }

        private void FriedRiceItemTile_Click(object sender, RoutedEventArgs e)
        {
            Tile tile = sender as Tile;

            var viewModel = DataContext as MainViewModel;
            viewModel.SelectFriedRiceItemCommand.Execute(tile.CommandParameter);

            OrderFlipView.SelectedIndex = 3;
        }

        private void AmountNumericUpDown_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
        {
            NumericUpDown numericUpDown = sender as NumericUpDown;
            if (numericUpDown.Value.HasValue)
            {
                var viewModel = DataContext as MainViewModel;
                viewModel.FriedRiceItemViewModel.Amount = (int)numericUpDown.Value;
            }
        }

        private void OrderFriedRiceButton_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = DataContext as MainViewModel;

            if (viewModel.OrderFriedRice())
                OrderFlipView.SelectedIndex = 2;
        }

        private void PostOrderButton_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = DataContext as MainViewModel;

            if (viewModel.PostOrder())
                OrderFlipView.SelectedIndex = 0;
        }

        private void CancelOrderButton_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = DataContext as MainViewModel;

            viewModel.CancelOrderCommand.Execute(null);
            OrderFlipView.SelectedIndex = 0;
        }

        private void UpdateRecipeButton_Click(object sender, RoutedEventArgs e)
        {
            var viewModel = DataContext as MainViewModel;

            KDS.Recipe recipe = new KDS.Recipe();
            recipe.Name = viewModel.RecipeViewModel.Recipe.Name;
            recipe.HotPotPower = HotPotPowerNumericUpDown.Value.HasValue ? (ushort)HotPotPowerNumericUpDown.Value : (ushort)0;
            recipe.HotOilPower = HotOilPowerNumericUpDown.Value.HasValue ? (ushort)HotOilPowerNumericUpDown.Value : (ushort)0;
            recipe.ScrambledEggPower = ScrambledEggPowerNumericUpDown.Value.HasValue ? (ushort)ScrambledEggPowerNumericUpDown.Value : (ushort)0;
            recipe.Procedure4Power = Procedure4PowerNumericUpDown.Value.HasValue ? (ushort)Procedure4PowerNumericUpDown.Value : (ushort)0;
            recipe.Procedure5Power = Procedure5PowerNumericUpDown.Value.HasValue ? (ushort)Procedure5PowerNumericUpDown.Value : (ushort)0;
            recipe.Procedure6Power = Procedure6PowerNumericUpDown.Value.HasValue ? (ushort)Procedure6PowerNumericUpDown.Value : (ushort)0;
            recipe.Procedure7Power = Procedure7PowerNumericUpDown.Value.HasValue ? (ushort)Procedure7PowerNumericUpDown.Value : (ushort)0;
            recipe.Procedure8Power = Procedure8PowerNumericUpDown.Value.HasValue ? (ushort)Procedure8PowerNumericUpDown.Value : (ushort)0;
            recipe.Procedure9Power = Procedure9PowerNumericUpDown.Value.HasValue ? (ushort)Procedure9PowerNumericUpDown.Value : (ushort)0;
            recipe.HotOilTime = HotOilTimeNumericUpDown.Value.HasValue ? (ushort)HotOilTimeNumericUpDown.Value : (ushort)0;
            recipe.ScrambledEggTime = ScrambledEggTimeNumericUpDown.Value.HasValue ? (ushort)ScrambledEggTimeNumericUpDown.Value : (ushort)0;
            recipe.Procedure4StirFryTime = Procedure4StirFryTimeNumericUpDown.Value.HasValue ? (ushort)Procedure4StirFryTimeNumericUpDown.Value : (ushort)0;
            recipe.Procedure5StirFryTime = Procedure5StirFryTimeNumericUpDown.Value.HasValue ? (ushort)Procedure5StirFryTimeNumericUpDown.Value : (ushort)0;
            recipe.Procedure6StirFryTime = Procedure6StirFryTimeNumericUpDown.Value.HasValue ? (ushort)Procedure6StirFryTimeNumericUpDown.Value : (ushort)0;
            recipe.Procedure7StirFryTime = Procedure7StirFryTimeNumericUpDown.Value.HasValue ? (ushort)Procedure7StirFryTimeNumericUpDown.Value : (ushort)0;
            recipe.Procedure8StirFryTime = Procedure8StirFryTimeNumericUpDown.Value.HasValue ? (ushort)Procedure8StirFryTimeNumericUpDown.Value : (ushort)0;
            recipe.Procedure9StirFryTime = Procedure9StirFryTimeNumericUpDown.Value.HasValue ? (ushort)Procedure9StirFryTimeNumericUpDown.Value : (ushort)0;
            recipe.HotAPotTemperature = HotAPotTemperatureNumericUpDown.Value.HasValue ? (ushort)HotAPotTemperatureNumericUpDown.Value : (ushort)0;
            recipe.HotBPotTemperature = HotBPotTemperatureNumericUpDown.Value.HasValue ? (ushort)HotBPotTemperatureNumericUpDown.Value : (ushort)0;
            recipe.HotCPotTemperature = HotCPotTemperatureNumericUpDown.Value.HasValue ? (ushort)HotCPotTemperatureNumericUpDown.Value : (ushort)0;
            recipe.HotDPotTemperature = HotDPotTemperatureNumericUpDown.Value.HasValue ? (ushort)HotDPotTemperatureNumericUpDown.Value : (ushort)0;
            recipe.DippingProcedureNumber = DippingProcedureNumberNumericUpDown.Value.HasValue ? (byte)DippingProcedureNumberNumericUpDown.Value : (byte)4;

            List<KDS.FoodMaterial> foodMaterials = new List<KDS.FoodMaterial>();
            if (!string.IsNullOrWhiteSpace(FoodMaterial1ContentsTextBox.Text))
            {
                KDS.FoodMaterial foodMaterial = new KDS.FoodMaterial();
                foodMaterial.Name = FoodMaterial1ContentsTextBox.Text;
                foodMaterial.ProcedureNumber = FoodMaterial1ProcedureNumberNumericUpDown.Value.HasValue ? (byte)FoodMaterial1ProcedureNumberNumericUpDown.Value : (byte)4;
                foodMaterials.Add(foodMaterial);
            }
            if (!string.IsNullOrWhiteSpace(FoodMaterial2ContentsTextBox.Text))
            {
                KDS.FoodMaterial foodMaterial = new KDS.FoodMaterial();
                foodMaterial.Name = FoodMaterial2ContentsTextBox.Text;
                foodMaterial.ProcedureNumber = FoodMaterial2ProcedureNumberNumericUpDown.Value.HasValue ? (byte)FoodMaterial2ProcedureNumberNumericUpDown.Value : (byte)4;
                foodMaterials.Add(foodMaterial);
            }
            if (!string.IsNullOrWhiteSpace(FoodMaterial3ContentsTextBox.Text))
            {
                KDS.FoodMaterial foodMaterial = new KDS.FoodMaterial();
                foodMaterial.Name = FoodMaterial3ContentsTextBox.Text;
                foodMaterial.ProcedureNumber = FoodMaterial3ProcedureNumberNumericUpDown.Value.HasValue ? (byte)FoodMaterial3ProcedureNumberNumericUpDown.Value : (byte)4;
                foodMaterials.Add(foodMaterial);
            }
            if (!string.IsNullOrWhiteSpace(FoodMaterial4ContentsTextBox.Text))
            {
                KDS.FoodMaterial foodMaterial = new KDS.FoodMaterial();
                foodMaterial.Name = FoodMaterial4ContentsTextBox.Text;
                foodMaterial.ProcedureNumber = FoodMaterial4ProcedureNumberNumericUpDown.Value.HasValue ? (byte)FoodMaterial4ProcedureNumberNumericUpDown.Value : (byte)4;
                foodMaterials.Add(foodMaterial);
            }

            recipe.FoodMaterials = foodMaterials.ToArray();

            recipe.AmountOfOil = AmountOfOilNumericUpDown.Value.HasValue ? (ushort)AmountOfOilNumericUpDown.Value : (ushort)0;
            recipe.AmountOfEggSauce = AmountOfEggSauceNumericUpDown.Value.HasValue ? (ushort)AmountOfEggSauceNumericUpDown.Value : (ushort)0;
            recipe.AmountOfDip = AmountOfDipNumericUpDown.Value.HasValue ? (ushort)AmountOfDipNumericUpDown.Value : (ushort)0;

            viewModel.UpdateRecipe(recipe);
            viewModel.UnselectRecipeCommand.Execute(null);
        }

        private void UIElement_ManipulationBoundaryFeedback(object sender, ManipulationBoundaryFeedbackEventArgs e)
        {
            e.Handled = true;
        }
    }
}
