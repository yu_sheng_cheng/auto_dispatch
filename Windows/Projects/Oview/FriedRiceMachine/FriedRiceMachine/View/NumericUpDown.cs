﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FriedRiceMachine.View
{
    class NumericUpDown : MahApps.Metro.Controls.NumericUpDown
    {
        public NumericUpDown()
        {
            LostFocus += NumericUpDown_LostFocus;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            var textBox = GetTemplateChild("PART_TextBox") as TextBox;
            if (textBox != null)
                InputMethod.SetIsInputMethodEnabled(textBox, false);
        }

        private void NumericUpDown_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!Value.HasValue)
                Value = Minimum;
        }
    }
}
