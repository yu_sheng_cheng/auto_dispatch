﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace FriedRiceMachine.View
{
    class FlipView : MahApps.Metro.Controls.FlipView
    {
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            var button = GetTemplateChild("PART_BackButton") as Button;
            if (button != null)
                button.IsTabStop = false;
            button = GetTemplateChild("PART_ForwardButton") as Button;
            if (button != null)
                button.IsTabStop = false;
        }
    }
}
