﻿using FriedRiceMachine.POS;
using SwissKnife;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace FriedRiceMachine.View
{
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType != typeof(Visibility))
                throw new InvalidOperationException("The target must be a Visibility");

            return (bool)value ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }

    [ValueConversion(typeof(FriedRiceFlavor), typeof(bool))]
    class FriedRiceFlavorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            FriedRiceFlavor? checkvalue = value as FriedRiceFlavor?;

            if (checkvalue == null || parameter == null)
                return false;

            FriedRiceFlavor targetvalue;

            if (!EnumUtils.TryParse(parameter.ToString(), out targetvalue))
                return false;

            return targetvalue == checkvalue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || parameter == null)
                return null;

            if ((bool)value)
            {
                FriedRiceFlavor targetvalue;

                if (EnumUtils.TryParse(parameter.ToString(), out targetvalue))
                    return targetvalue;
            }

            return null;
        }
    }

    [ValueConversion(typeof(OrderSource), typeof(bool))]
    class OrderSourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            OrderSource? checkvalue = value as OrderSource?;

            if (checkvalue == null || parameter == null)
                return false;

            OrderSource targetvalue;

            if (!EnumUtils.TryParse(parameter.ToString(), out targetvalue))
                return false;

            return targetvalue == checkvalue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || parameter == null)
                return null;

            if ((bool)value)
            {
                OrderSource targetvalue;

                if (EnumUtils.TryParse(parameter.ToString(), out targetvalue))
                    return targetvalue;
            }

            return null;
        }
    }
}
