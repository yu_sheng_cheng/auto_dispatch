﻿using Automation;
using FriedRiceMachine.View;
using Industry4._0;
using SwissKnife.Log;
using SwissKnife.Log.AppLogger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace FriedRiceMachine
{
    /// <summary>
    /// App.xaml 的互動邏輯
    /// </summary>
    public partial class App : Application
    {
        private Mutex _mutex;

        private void App_Startup(object sender, StartupEventArgs e)
        {
            bool createdNew;

            _mutex = new Mutex(true, "9DFADE9B-6834-484A-ABE7-AE1EEC51AC0A", out createdNew);

            if (!createdNew)
            {
                Current.Shutdown();
                return;
            }

            IUnityContainer container = new UnityContainer();
            container.RegisterType<ILog, Logger>(new ContainerControlledLifetimeManager(), new InjectionConstructor(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Applogger.xml")));

            ILog log = container.Resolve<ILog>();
            log.i("FriedRiceMachine starting");

            Industry4Dot0Environment.Log = log;
            AutomationEnvironment.Log = log;

            Environment.UnityContainer = container;

            Exit += App_Exit;

            DispatcherUnhandledException += (o, ea) => Backtrace("Dispatcher", ea.Exception);
            AppDomain.CurrentDomain.UnhandledException += (o, ea) =>
            {
                Exception exception = ea.ExceptionObject as Exception;
                if (exception == null)
                    Backtrace("AppDomain", new Exception("Unknown error. Exception object is null"));
                else
                    Backtrace("AppDomain", exception);
            };
            TaskScheduler.UnobservedTaskException += (o, ea) =>
            {
                ea.SetObserved();
                Environment.UnityContainer.Resolve<ILog>().w("TaskScheduler", $"UnobservedTaskException occurs", ea.Exception);
            };

            new MainWindow().Show();
        }

        private void App_Exit(object sender, ExitEventArgs e)
        {
            IUnityContainer container = Environment.UnityContainer;

            container.Resolve<ILog>().i("FriedRiceMachine shutting down");
            container.Dispose();
        }

        private void Backtrace(string source, Exception ex)
        {
            string directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ErrorReport");

            Directory.CreateDirectory(directory);

            string file = Path.Combine(directory, string.Format("{0}-{1:yyyy-MM-dd_HH-mm-ss}.txt", source, DateTime.Now));

            using (StreamWriter sw = new StreamWriter(file, false, Encoding.UTF8))
            {
                sw.Write(ex.ToString());
            }
        }
    }
}
