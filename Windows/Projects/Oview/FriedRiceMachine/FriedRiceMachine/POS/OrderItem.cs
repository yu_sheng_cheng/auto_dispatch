﻿using FriedRiceMachine.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriedRiceMachine.POS
{
    class OrderItem
    {
        public FoodSeries FoodSeries { get; }

        public string DisplayName { get; }

        public int Amount { get; }

        public ushort UnitPrice { get; }

        public OrderItem(FoodSeries foodSeries, string displayName, int amount, ushort unitPrice)
        {
            FoodSeries = foodSeries;
            DisplayName = displayName;
            Amount = amount;
            UnitPrice = unitPrice;
        }
    }

    class FriedRiceOrderItem : OrderItem
    {
        public FriedRice FriedRice { get; }

        public FriedRiceOrderItem(FriedRice friedRice, int amount, ushort unitPrice) : base(FoodSeries.FriedRice, friedRice.ToString(), amount, unitPrice)
        {
            FriedRice = friedRice;
        }
    }
}
