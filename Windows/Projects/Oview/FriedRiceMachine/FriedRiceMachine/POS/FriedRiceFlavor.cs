﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriedRiceMachine.POS
{
    enum FriedRiceFlavor
    {
        PrimaryTaste,
        SpicedSalt,
        MildSpicy,
        MediumSpicy,
        HotSpicy,
        BlackPepper
    }
}
