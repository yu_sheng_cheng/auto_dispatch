﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FriedRiceMachine.POS
{
    public class FriedRiceItem
    {
        [XmlAttribute] public string Name { get; set; }
        [XmlAttribute] public ushort Price { get; set; }
        [XmlAttribute] public bool CanAddSpicedSalt { get; set; }
        [XmlAttribute] public bool CanAddSpicy { get; set; }
        [XmlAttribute] public bool CanAddBlackPepper { get; set; }
    }
}
