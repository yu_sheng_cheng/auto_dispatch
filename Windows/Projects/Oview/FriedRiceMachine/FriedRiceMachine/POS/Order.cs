﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Widget.WPF;

namespace FriedRiceMachine.POS
{
    class Order : ViewModelBase
    {
        private OrderSource _source = OrderSource.FengKuai;
        private ushort _sourceOrderNumber;
        [ContractPublicPropertyName("IsOrderInserting")]
        private bool _needOrderInserting;

        public ushort Number { get; private set; }

        public OrderSource? Source
        {
            get { return _source; }
            set
            {
                if (value != null && value != _source)
                {
                    _source = (OrderSource)value;
                    RaisePropertyChanged(nameof(SourceDisplayName));
                    RaisePropertyChanged(nameof(HasSourceOrderNumber));
                }
            }
        }

        public bool HasSourceOrderNumber
        {
            get
            {
                return _source == OrderSource.Foodpanda;
            }
        }

        public ushort SourceOrderNumber
        {
            get { return _sourceOrderNumber; }
            set
            {
                if (value != _sourceOrderNumber)
                {
                    _sourceOrderNumber = value;
                    RaisePropertyChanged(nameof(SourceDisplayName));
                }
            }
        }

        public string SourceDisplayName
        {
            get
            {
                switch (_source)
                {
                    case OrderSource.FengKuai:
                        return "Feng Kuai";
                    case OrderSource.Line:
                        return "Line";
                    case OrderSource.Foodpanda:
                        return $"Foodpanda #{SourceOrderNumber.ToString()}";
                    default:
                        return string.Empty;
                }
            }
        }

        public bool IsOrderInserting { get { return _needOrderInserting; } }

        public bool? NeedOrderInserting
        {
            get { return _needOrderInserting; }
            set
            {
                if (value != null && value != _needOrderInserting)
                {
                    _needOrderInserting = (bool)value;
                    RaisePropertyChanged(nameof(IsOrderInserting));
                }
            }
        }

        public ObservableCollection<OrderItem> Items { get; } = new ObservableCollection<OrderItem>();

        public bool HasOrderItem { get { return Items.Count != 0; } }

        public int AmountOfMoney { get; private set; }

        public Order(ushort number)
        {
            Number = number;
        }

        public void AddItem(OrderItem item)
        {
            Items.Add(item);
            if (Items.Count == 1)
                RaisePropertyChanged(nameof(HasOrderItem));
            AmountOfMoney += item.Amount * item.UnitPrice;
            RaisePropertyChanged(nameof(AmountOfMoney));
        }

        public void RemoveItem(OrderItem item)
        {
            if (Items.Remove(item))
            {
                if (Items.Count == 0)
                    RaisePropertyChanged(nameof(HasOrderItem));
                AmountOfMoney -= item.Amount * item.UnitPrice;
                RaisePropertyChanged(nameof(AmountOfMoney));
            }
        }

        public void Reset()
        {
            Reset(Number);
        }

        public void Reset(ushort newNumber)
        {
            Number = newNumber;
            _source = OrderSource.FengKuai;
            //_sourceOrderNumber = 0;
            _needOrderInserting = false;
            Items.Clear();
            AmountOfMoney = 0;
            RaisePropertyChanged(nameof(Number));
            RaisePropertyChanged(nameof(Source));
            RaisePropertyChanged(nameof(HasSourceOrderNumber));
            RaisePropertyChanged(nameof(SourceOrderNumber));
            RaisePropertyChanged(nameof(SourceDisplayName));
            RaisePropertyChanged(nameof(IsOrderInserting));
            RaisePropertyChanged(nameof(NeedOrderInserting));
            RaisePropertyChanged(nameof(HasOrderItem));
            RaisePropertyChanged(nameof(AmountOfMoney));
        }
    }
}
