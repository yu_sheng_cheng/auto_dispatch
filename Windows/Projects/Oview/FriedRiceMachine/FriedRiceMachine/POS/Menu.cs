﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FriedRiceMachine.POS
{
    [XmlRoot(ElementName = "Menu", Namespace = "FriedRiceMachine.POS.Menu.xml", IsNullable = false)]
    public class Menu
    {
        [XmlAttribute] public byte SpicedSaltPrice { get; set; }
        [XmlAttribute] public byte SpicyPrice { get; set; }
        [XmlAttribute] public byte BlackPepperPrice { get; set; }
        [XmlAttribute] public byte ChiliPrice { get; set; }
        [XmlArrayItem(ElementName = "Item")] public FriedRiceItem[] FriedRiceItems { get; set; }
    }
}
