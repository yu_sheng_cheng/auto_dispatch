﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriedRiceMachine.POS
{
    class FriedRice
    {
        public string Name { get; }

        public FriedRiceFlavor Flavor { get; }

        public bool HasAddedShallot { get; set; } = true;

        public bool HasAddedOnion { get; set; } = true;

        public bool HasAddedFungus { get; set; } = true;

        public bool HasAddedCarrot { get; set; } = true;

        public bool HasAddedCabbage { get; set; } = true;

        public bool HasAddedChili { get; set; } = false;

        public bool IsLessRice { get; set; } = false;

        public FriedRice(string name, FriedRiceFlavor flavor)
        {
            Name = name;
            Flavor = flavor;
        }

        public override string ToString()
        {
            return $"{Name} ({SR.ToString(Flavor)})";
        }
    }
}
