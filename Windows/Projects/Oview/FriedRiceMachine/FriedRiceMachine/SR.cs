﻿using FriedRiceMachine.POS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FriedRiceMachine
{
    static class SR
    {
        public static string ToString(FriedRiceFlavor flavor)
        {
            switch (flavor)
            {
                case FriedRiceFlavor.PrimaryTaste:
                    return "原味";
                case FriedRiceFlavor.SpicedSalt:
                    return "椒鹽";
                case FriedRiceFlavor.MildSpicy:
                    return "小辣";
                case FriedRiceFlavor.MediumSpicy:
                    return "中辣";
                case FriedRiceFlavor.HotSpicy:
                    return "大辣";
                case FriedRiceFlavor.BlackPepper:
                    return "黑胡椒";
                default:
                    return string.Empty;
            }
        }
    }
}
