﻿using SwissKnife.Log;
using SwissKnife.Log.AppLogger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace AutomatedDispensing
{
    /// <summary>
    /// App.xaml 的互動邏輯
    /// </summary>
    public partial class App : Application
    {
        private Mutex _mutex;

        private void App_Startup(object sender, StartupEventArgs e)
        {
            bool createdNew;

            _mutex = new Mutex(true, "DEB6FED8-F1DD-4E8D-B946-0A68EFF80E09", out createdNew);

            if (!createdNew)
            {
                Current.Shutdown();
                return;
            }

            IUnityContainer container = new UnityContainer();
            container.RegisterType<ILog, Logger>(new ContainerControlledLifetimeManager(), new InjectionConstructor(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Applogger.xml")));
            container.RegisterType<AutomatedDispensingController>(new ContainerControlledLifetimeManager());

            ILog log = container.Resolve<ILog>();
            log.i("AutomatedDispensing starting ...");

            Industry4Dot0Environment.Log = log;
            AutomationEnvironment.Log = log;

            Environment.UnityContainer = container;
            Environment.EquipmentProfileRootFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Profiles");
            Environment.StringResourceRootFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "StringResources");

            Exit += App_Exit;

            DispatcherUnhandledException += (o, ea) => Backtrace("Dispatcher", ea.Exception);
            AppDomain.CurrentDomain.UnhandledException += (o, ea) =>
            {
                Exception exception = ea.ExceptionObject as Exception;
                if (exception == null)
                    Backtrace("AppDomain", new Exception("Unknown error. Exception object is null"));
                else
                    Backtrace("AppDomain", exception);
            };
            TaskScheduler.UnobservedTaskException += (o, ea) =>
            {
                ea.SetObserved();
                Environment.UnityContainer.Resolve<ILog>().w("TaskScheduler", $"UnobservedTaskException occurs", ea.Exception);
            };

            var controller = container.Resolve<AutomatedDispensingController>();
            controller.IsTuning = true;
            controller.AlarmMessageIni = new IniParser(Path.Combine(Environment.StringResourceRootFolder, "AlarmMessage.ini"));

            Environment.PositionIni = new IniParser(Path.Combine(Environment.StringResourceRootFolder, "Position.ini"));
            Environment.IOWatcher = new IOWatcher();

            Build();

            controller.Start();

            new Tuning.View.MainWindow().Show();
        }
    }
}
