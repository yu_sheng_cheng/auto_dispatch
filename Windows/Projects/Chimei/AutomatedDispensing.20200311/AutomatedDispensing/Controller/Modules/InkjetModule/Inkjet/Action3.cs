﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Enums;
using Automation.Objects.Movable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.InkjetModule.Inkjet
{
    partial class InkjetDirector
    {
        private sealed class Action3 : DecompositionAction
        {
            public Action3(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "噴碼前後移載汽缸", "前進", OnInkjetTransferCylinderForwardCompleted);
            }

            private void OnInkjetTransferCylinderForwardCompleted(IMovableObject movableObject, CompletionResult result)
            {
                SignalAndWait(movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action4(Director);
            }
        }
    }
}
