﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.InkjetModule
{
    sealed class InkjetPrinterDocument
    {
        private const ushort MaxCharactersPerRow = 10;

        public string ProjectName { get; }

        public IEnumerable<KeyValuePair<string, string>> VariableTexts { get; }

        public InkjetPrinterDocument(string lotNumber, string plantUnit, string samplingPoint, string mic)
        {
            if (lotNumber == null)
                throw new ArgumentNullException("lotNumber");

            if (plantUnit == null)
                throw new ArgumentNullException("plantUnit");

            if (samplingPoint == null)
                throw new ArgumentNullException("samplingPoint");

            if (mic == null)
                throw new ArgumentNullException("mic");

            ProjectName = "onsitesample.msg";

            VariableTexts = new KeyValuePair<string, string>[]
            {
                new KeyValuePair<string, string>("2d barcode", $"{lotNumber} {plantUnit}/{samplingPoint} {mic}"),
                new KeyValuePair<string, string>("1", AlignCenter(plantUnit)),
                new KeyValuePair<string, string>("2", AlignCenter(samplingPoint)),
                new KeyValuePair<string, string>("3", AlignCenter(mic))
            };
        }

        private string AlignCenter(string text)
        {
            int width = text.Length;

            int count = MaxCharactersPerRow - width;

            return count >= 1 ? text.PadLeft(width + count) : text;
        }
    }
}
