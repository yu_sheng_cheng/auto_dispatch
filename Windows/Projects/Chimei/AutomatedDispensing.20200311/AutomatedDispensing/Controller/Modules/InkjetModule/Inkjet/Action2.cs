﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Enums;
using Automation.Objects.Movable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.InkjetModule.Inkjet
{
    partial class InkjetDirector
    {
        private sealed class Action2 : DecompositionAction
        {
            public Action2(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "噴碼夾爪汽缸", "夾持", OnInkjetGripperCylinderHoldCompleted);
            }

            private void OnInkjetGripperCylinderHoldCompleted(IMovableObject movableObject, CompletionResult result)
            {
                SignalAndWait(movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action3(Director);
            }
        }
    }
}
