﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.InkjetModule.Inkjet
{
    sealed partial class InkjetDirector : ContinuousActionDirector
    {
        private InkjetPrinterDocument _document;
        private Action<string> _barcodeReceived;
        private string _barcode = string.Empty;

        public InkjetPrinterDocument Document
        {
            get { return _document; }
            set
            {
                if (IsBusy)
                    throw new InvalidOperationException($"{Name} has already been started.");

                _document = value;
            }
        }

        public Action<string> BarcodeReceived
        {
            get { return _barcodeReceived; }
            set
            {
                if (IsBusy)
                    throw new InvalidOperationException($"{Name} has already been started.");

                _barcodeReceived = value;
            }
        }

        public bool HasBottle { get; set; }

        public string Barcode
        {
            get { return _barcode; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    _barcode = string.Empty;
                else
                {
                    _barcode = value;
                    _barcodeReceived?.Invoke(value);
                }
            }
        }

        public InkjetDirector() : base("噴碼模組噴碼")
        {

        }

        protected override void Reinitialize()
        {
            base.Reinitialize();

            HasBottle = false;
            _barcode = string.Empty;
        }

        protected override DecompositionAction CreateFirstAction()
        {
            return new Action1(this);
        }

        protected override bool CheckIfObjectExists(SupervisoryController controller)
        {
            return true;
        }
    }
}
