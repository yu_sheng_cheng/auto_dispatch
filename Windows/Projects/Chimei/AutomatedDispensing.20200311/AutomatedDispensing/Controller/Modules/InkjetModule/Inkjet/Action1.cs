﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Enums;
using Automation.Objects.Movable;

namespace AutomatedDispensing.Controller.Modules.InkjetModule.Inkjet
{
    partial class InkjetDirector
    {
        private sealed class Action1 : DecompositionAction
        {
            public Action1(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "噴碼翻轉汽缸", "垂直", OnInkjetFlipCylinderCompleted);
            }

            private void OnInkjetFlipCylinderCompleted(IMovableObject movableObject, CompletionResult result)
            {
                SignalAndWait(movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action2(Director);
            }
        }
    }
}
