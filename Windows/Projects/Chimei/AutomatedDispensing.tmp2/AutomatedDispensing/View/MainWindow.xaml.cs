﻿using AutomatedDispensing.ViewModel;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Widget;
using Widget.WPF;

namespace AutomatedDispensing.View
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private readonly Feature[] _features;
        private readonly Storyboard _storyboard = new Storyboard();
        private Feature _activeFeature;
        private bool _isStoryboardPlaying;
        private bool _featureToolBarVisible = true;

        public ObservableCollection<Feature> Features { get; } = new ObservableCollection<Feature>();

        public MainWindow()
        {
            InitializeComponent();

            _features = new Feature[]
            {
                new Feature("重新登入", "Images/calculator.png", 0, new UserControl1()),
                new Feature("安全門", "Images/Photos Icon.png", 1, new UserControl1()),
                new Feature("機台設定", "Images/Settings.png", 2, new UserControl1()),
                new Feature("回原點", "Images/Find My.png", 3, new UserControl1()),
                new Feature("手動操作", "Images/measure.png", 4, new UserControl1()),
                new Feature("自動運轉", "Images/voice memos.png", 5, new UserControl1()),
                new Feature("警報紀錄", "Images/News.png", 6, new UserControl1()),
                new Feature("提醒事項", "Images/reminders.png", 7, new UserControl1()),
                new Feature("監看IO", "Images/Camera.png", 8, new UserControl1()),
                new Feature("設備斷電", "Images/apple tv.png", 9, new UserControl1()),
                new Feature("資料庫", "Images/Ibooks.png", 10, new UserControl1()),
                new Feature("工具", "Images/mail.png", 11, new UserControl1()),
                new Feature("關於 設備軟體", "Images/Notes.png", 12, new UserControl1()),
                new Feature("關閉軟體", "Images/my shortcuts.png", 13, new UserControl())
            };

            foreach (var f in _features)
            {
                Features.Add(f);
            }

            FeatureListBox.DataContext = this;

            _storyboard.Completed += Storyboard_Completed;
        }

        private void MetroWindow_SourceInitialized(object sender, EventArgs e)
        {
            HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);
            source.AddHook(new HwndSourceHook(WndProc));
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case Win32.WM_SYSCOMMAND:
                    switch (wParam.ToInt32() & 0xFFF0)
                    {
                        case Win32.SC_MOVE:
                        case Win32.SC_RESTORE:
                            handled = true;
                            break;
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        private void MetroWindow_Closed(object sender, EventArgs e)
        {
            var viewModel = DataContext as MainViewModel;

            viewModel.Close();
        }

        private void FeatureListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Feature activeFeature = FeatureListBox.SelectedItem as Feature;

            if (activeFeature != null)
            {
                if (_isStoryboardPlaying)
                {
                    _storyboard.Stop();
                    _storyboard.Children.Clear();
                }

                _featureToolBarVisible = false;

                ItemContainerGenerator itemContainerGenerator = FeatureListBox.ItemContainerGenerator;
                int count = _features.Length;

                for (int i = 0; i < count; ++i)
                {
                    Feature feature = _features[i];

                    if (feature == activeFeature)
                        continue;

                    ListBoxItem listBoxItem = itemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;

                    DoubleAnimation fadeOutAnimation = feature.FadeOutAnimation;

                    Storyboard.SetTarget(fadeOutAnimation, listBoxItem);
                    Storyboard.SetTargetProperty(fadeOutAnimation, new PropertyPath(ListBoxItem.OpacityProperty));

                    _storyboard.Children.Add(fadeOutAnimation);
                }

                _storyboard.Begin();
            }

            _activeFeature = activeFeature;
        }

        private void ActiveFeatureTile_Click(object sender, RoutedEventArgs e)
        {
            if (FeatureListBox.SelectedItem as Feature == null)
                return;

            if (_isStoryboardPlaying)
                return;

            if (_featureToolBarVisible)
            {
                ItemContainerGenerator itemContainerGenerator = FeatureListBox.ItemContainerGenerator;
                int count = _features.Length;

                for (int i = 0; i < count; ++i)
                {
                    Feature feature = _features[i];

                    if (feature == _activeFeature)
                        continue;

                    ListBoxItem listBoxItem = itemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;

                    DoubleAnimation fadeOutAnimation = feature.FadeOutAnimation;

                    Storyboard.SetTarget(fadeOutAnimation, listBoxItem);
                    Storyboard.SetTargetProperty(fadeOutAnimation, new PropertyPath(ListBoxItem.OpacityProperty));

                    _storyboard.Children.Add(fadeOutAnimation);
                }
            }
            else
            {
                int count = _features.Length;

                for (int i = 0; i < count; ++i)
                {
                    Feature feature = _features[i];

                    if (feature == _activeFeature)
                        continue;

                    Features.Insert(i, feature);
                }

                ItemContainerGenerator itemContainerGenerator = FeatureListBox.ItemContainerGenerator;
                count = _features.Length;

                for (int i = 0; i < count; ++i)
                {
                    Feature feature = _features[i];

                    if (feature == _activeFeature)
                        continue;

                    ListBoxItem listBoxItem = itemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;

                    DoubleAnimation fadeInAnimation = feature.FadeInAnimation;

                    Storyboard.SetTarget(fadeInAnimation, listBoxItem);
                    Storyboard.SetTargetProperty(fadeInAnimation, new PropertyPath(ListBoxItem.OpacityProperty));

                    _storyboard.Children.Add(fadeInAnimation);
                }
            }

            _featureToolBarVisible = !_featureToolBarVisible;
            _storyboard.Begin();
            _isStoryboardPlaying = true;
        }

        private void ActiveFeatureTile_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (FeatureListBox.SelectedItem as Feature == null)
                return;

            if (!_featureToolBarVisible)
            {
                if (_isStoryboardPlaying)
                {
                    _storyboard.Stop();
                    _storyboard.Children.Clear();
                    _isStoryboardPlaying = false;

                    ItemContainerGenerator itemContainerGenerator = FeatureListBox.ItemContainerGenerator;

                    for (int i = _features.Length - 1; i >= 0; --i)
                    {
                        ListBoxItem listBoxItem = itemContainerGenerator.ContainerFromIndex(i) as ListBoxItem;
                        listBoxItem.Opacity = 1;
                    }
                }
                else
                {
                    int count = _features.Length;

                    for (int i = 0; i < count; ++i)
                    {
                        Feature feature = _features[i];

                        if (feature == _activeFeature)
                            continue;

                        Features.Insert(i, feature);
                    }
                }
                
                _featureToolBarVisible = true;
            }

            FeatureListBox.SelectedItem = null;
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            if (!_featureToolBarVisible)
            {
                int count = _features.Length;

                for (int i = count - 1; i >= 0; --i)
                {
                    Feature feature = _features[i];

                    if (feature == _activeFeature)
                        continue;

                    Features.RemoveAt(i);
                }
            }

            _storyboard.Children.Clear();
            _isStoryboardPlaying = false;
        }
    }

    public class Feature : ViewModelBase
    {
        private readonly DoubleAnimation _fadeOutAnimation = new DoubleAnimation();

        public string Name { get; }

        public string IconUri { get; }

        public int Sequence { get; }

        public UserControl View { get; }

        public DoubleAnimation FadeInAnimation { get; }

        public DoubleAnimation FadeOutAnimation { get; }

        public Feature(string name, string iconUri, int sequence, UserControl view)
        {
            Name = name;
            IconUri = iconUri;
            Sequence = sequence;
            View = view;

            DoubleAnimation fadeInAnimation = new DoubleAnimation();
            fadeInAnimation.From = 0.01;
            fadeInAnimation.To = 1;
            fadeInAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(500));

            FadeInAnimation = fadeInAnimation;

            DoubleAnimation fadeOutAnimation = new DoubleAnimation();

            fadeOutAnimation.From = 1;
            fadeOutAnimation.To = 0;
            fadeOutAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(500));
            fadeOutAnimation.BeginTime = TimeSpan.FromMilliseconds(50);

            FadeOutAnimation = fadeOutAnimation;
        }
    }
}
