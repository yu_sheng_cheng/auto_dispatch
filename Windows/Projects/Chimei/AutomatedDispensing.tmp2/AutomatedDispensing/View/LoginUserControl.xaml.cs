﻿using AutomatedDispensing.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AutomatedDispensing.View
{
    /// <summary>
    /// LoginUserControl.xaml 的互動邏輯
    /// </summary>
    public partial class LoginUserControl : UserControl
    {
        private Storyboard _storyboard;
        private bool _isAnimationPlaying;

        public LoginUserControl()
        {
            InitializeComponent();
        }

        private void LoginTile_Click(object sender, RoutedEventArgs e)
        {
            if (_isAnimationPlaying)
                return;

            var viewModel = DataContext as LoginViewModel;

            if (viewModel.IsEmployeeIdEmpty)
                return;

            if (!viewModel.Login())
            {
                if (_storyboard == null)
                {
                    _storyboard = RootGrid.Resources["LoginFailedStoryboard"] as Storyboard;
                    _storyboard.Completed += Storyboard_Completed;
                }

                _isAnimationPlaying = true;
                _storyboard.Begin();
            }
        }

        private void Storyboard_Completed(object sender, EventArgs e)
        {
            _isAnimationPlaying = false;

            var viewModel = DataContext as LoginViewModel;

            viewModel.ClearError();
        }
    }
}
