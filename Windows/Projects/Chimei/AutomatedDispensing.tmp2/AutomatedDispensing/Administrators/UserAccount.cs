﻿using QualityTechnologyDepartment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Administrators
{
    partial class Administrator
    {
        public event EventHandler LoggedIn;

        public event EventHandler LoginCanceled;

        public bool Login(string employeeId, out UserAccount userAccount)
        {
            userAccount = GetUserAccount(employeeId);

            if (userAccount == null)
                return false;

            Environment.LoggedInUser = userAccount;
            LoggedIn?.Invoke(this, EventArgs.Empty);
            return true;
        }

        public void CancelLogin()
        {
            LoginCanceled?.Invoke(this, EventArgs.Empty);
        }

        private UserAccount GetUserAccount(string employeeId)
        {
            if (employeeId == null)
                throw new ArgumentNullException("employeeId");

            switch (employeeId)
            {
                case "00000":
                    return new UserAccount()
                    {
                        EmployeeId = "00000",
                        EmployeeName = "兆強科技",
                        Gender = Gender.Male,
                        Permission = AccountPermission.SupplierEngineer
                    };
                case "90122":
                    return new UserAccount()
                    {
                        EmployeeId = "90122",
                        EmployeeName = "陳秋楓",
                        Gender = Gender.Female,
                        Permission = AccountPermission.Manager
                    };
                case "26520":
                    return new UserAccount()
                    {
                        EmployeeId = "26520",
                        EmployeeName = "邱馨賢",
                        Gender = Gender.Female,
                        Permission = AccountPermission.Manager
                    };
                default:
                    return null;
            }
        }
    }
}
