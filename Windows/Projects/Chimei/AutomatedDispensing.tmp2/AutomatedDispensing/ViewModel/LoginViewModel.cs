﻿using AutomatedDispensing.Administrators;
using QualityTechnologyDepartment;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Animation;
using Unity;
using Widget.WPF;

namespace AutomatedDispensing.ViewModel
{
    class LoginViewModel : ViewModelBase
    {
        private readonly Administrator _administrator;
        private bool _hasError;

        public ObservableCollection<string> EmployeeId { get; } = new ObservableCollection<string>();

        public bool IsEmployeeIdEmpty { get { return EmployeeId.Count == 0; } }

        public RelayCommand<string> InputCommand { get; }

        public RelayCommand RemoveCommand { get; }

        public RelayCommand CancelCommand { get; }

        public LoginViewModel()
        {
            _administrator = Environment.UnityContainer.Resolve<Administrator>();

            InputCommand = new RelayCommand<string>(Input, _ => { return !_hasError; });
            RemoveCommand = new RelayCommand(Remove, () => { return !(_hasError | IsEmployeeIdEmpty); });
            CancelCommand = new RelayCommand(Cancel, () => { return !_hasError && IsEmployeeIdEmpty; });
        }

        private void Input(string numeric)
        {
            EmployeeId.Add(numeric);
            if (EmployeeId.Count == 1)
            {
                RaisePropertyChanged(nameof(IsEmployeeIdEmpty));
                RemoveCommand.RaiseCanExecuteChanged();
                CancelCommand.RaiseCanExecuteChanged();
            }
        }

        private void Remove()
        {
            EmployeeId.RemoveAt(EmployeeId.Count - 1);
            if (EmployeeId.Count == 0)
            {
                RaisePropertyChanged(nameof(IsEmployeeIdEmpty));
                RemoveCommand.RaiseCanExecuteChanged();
                CancelCommand.RaiseCanExecuteChanged();
            }
        }

        private void Cancel()
        {
            _administrator.CancelLogin();
        }

        public bool Login()
        {
            if (_hasError | IsEmployeeIdEmpty)
                return false;

            UserAccount user;

            StringBuilder sb = new StringBuilder();
            foreach (var numeric in EmployeeId)
            {
                sb.Append(numeric);
            }

            if (!_administrator.Login(sb.ToString(), out user))
            {
                _hasError = true;
                InputCommand.RaiseCanExecuteChanged();
                RemoveCommand.RaiseCanExecuteChanged();
                CancelCommand.RaiseCanExecuteChanged();
                return false;
            }

            Clear();
            return true;
        }

        public void ClearError()
        {
            _hasError = false;
            InputCommand.RaiseCanExecuteChanged();
            Clear();
        }

        private void Clear()
        {
            EmployeeId.Clear();
            RaisePropertyChanged(nameof(IsEmployeeIdEmpty));
            RemoveCommand.RaiseCanExecuteChanged();
            CancelCommand.RaiseCanExecuteChanged();
        }
    }
}
