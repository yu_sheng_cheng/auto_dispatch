﻿using AutomatedDispensing.Administrators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Unity;
using Widget.WPF;

namespace AutomatedDispensing.ViewModel
{
    class MainViewModel : ViewModelBase
    {
        private bool _isLoggedIn;
        private bool _onLoginPage = true;
        private bool _onDesktop = false;

        public LoginViewModel LoginViewModel { get; } = new LoginViewModel();

        public bool OnLoginPage
        {
            get { return _onLoginPage; }
            set
            {
                if (value != _onLoginPage)
                {
                    _onLoginPage = value;
                    RaisePropertyChanged(nameof(OnLoginPage));
                }
            }
        }

        public bool OnDesktop
        {
            get { return _onDesktop; }
            set
            {
                if (value != _onDesktop)
                {
                    _onDesktop = true;
                    RaisePropertyChanged(nameof(OnDesktop));
                }
            }
        }

        public MainViewModel()
        {
            Administrator administrator = Environment.UnityContainer.Resolve<Administrator>();

            administrator.LoggedIn += Administrator_LoggedIn;
            administrator.LoginCanceled += Administrator_LoginCanceled;
        }

        public void Close()
        {
            Administrator administrator = Environment.UnityContainer.Resolve<Administrator>();

            administrator.LoggedIn -= Administrator_LoggedIn;
            administrator.LoginCanceled -= Administrator_LoginCanceled;
        }

        private void Administrator_LoggedIn(object sender, EventArgs e)
        {
            if (!_isLoggedIn)
            {
                _isLoggedIn = true;
            }

            OnDesktop = true;
            OnLoginPage = false;
        }

        private void Administrator_LoginCanceled(object sender, EventArgs e)
        {
            if (!_isLoggedIn)
            {
                Application.Current.MainWindow.Close();
                return;
            }
        }
    }
}
