﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Widget.WPF.Keypads;

namespace AutomatedDispensing.View.Dialogs
{
    /// <summary>
    /// XYMotionPositionArrayNumberSelectorDialog.xaml 的互動邏輯
    /// </summary>
    public partial class XYMotionPositionArrayNumberSelectorDialog : DialogBase
    {
        public XYMotionPositionArrayNumberSelectorDialog()
        {
            InitializeComponent();
        }

        private void TextBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            TextBox textbox = sender as TextBox;

            NumericKeypad keypad = new NumericKeypad
            {
                Owner = this
            };

            if (keypad.ShowDialog() == true)
            {
                string result = keypad.Result;

                if (byte.TryParse(result, out byte value))
                    textbox.Text = result;
            }
        }

        private void GoButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
