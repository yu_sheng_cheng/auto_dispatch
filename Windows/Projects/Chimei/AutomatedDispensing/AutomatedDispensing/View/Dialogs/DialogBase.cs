﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using Widget;

namespace AutomatedDispensing.View.Dialogs
{
    public class DialogBase: MetroWindow
    {
        public DialogBase()
        {
            ShowTitleBar = false;
            ShowMinButton = false;
            ShowMaxRestoreButton = false;
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            ResizeMode = ResizeMode.NoResize;
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);

            HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);
            source.AddHook(new HwndSourceHook(WndProc));
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case Win32.WM_SYSCOMMAND:
                    switch (wParam.ToInt32() & 0xFFF0)
                    {
                        case Win32.SC_MOVE:
                        case Win32.SC_RESTORE:
                            handled = true;
                            break;
                    }
                    break;
            }
            return IntPtr.Zero;
        }
    }
}
