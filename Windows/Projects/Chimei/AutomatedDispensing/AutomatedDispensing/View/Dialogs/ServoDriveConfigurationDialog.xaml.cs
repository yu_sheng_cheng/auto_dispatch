﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Widget.WPF.Keypads;

namespace AutomatedDispensing.View.Dialogs
{
    /// <summary>
    /// ServoDriveConfigurationDialog.xaml 的互動邏輯
    /// </summary>
    public partial class ServoDriveConfigurationDialog : DialogBase
    {
        public ServoDriveConfigurationDialog()
        {
            InitializeComponent();
        }

        private void TextBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            TextBox textbox = sender as TextBox;

            NumericKeypad keypad = new NumericKeypad
            {
                Owner = this
            };

            if (keypad.ShowDialog() == true)
            {
                string result = keypad.Result;
                int value;

                if (int.TryParse(result, out value))
                    textbox.Text = result;
            }
        }
    }
}
