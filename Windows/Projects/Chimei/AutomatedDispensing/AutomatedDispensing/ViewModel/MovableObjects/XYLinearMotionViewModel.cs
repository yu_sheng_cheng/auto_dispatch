﻿using AutomatedDispensing.Controller;
using AutomatedDispensing.Model.Interfaces;
using AutomatedDispensing.View.Dialogs;
using Automation.Core.Messages.ManualControl.Movable;
using Automation.Core.Registers;
using Automation.Profiles.ServoDrive;
using SwissKnife;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;
using Unity;
using Widget.WPF;

namespace AutomatedDispensing.ViewModel.MovableObjects
{
    class XYLinearMotionViewModel : ViewModelBase, IPhysicalRegisterObserver
    {
        private readonly string _xAxisName;
        private readonly string _yAxisName;

        private readonly ServoDriveProfile _profile;
        private readonly XYMotionPositionViewModel[] _positions;
        private readonly AutomatedDispensingController _controller;

        private readonly double _magnification;

        private int _xAxisJogSpeed;
        private int _yAxisJogSpeed;
        private bool _isJogOperation;
        private bool _hadXAxisReturnedToHomePosition;
        private bool _hadYAxisReturnedToHomePosition;
        private int _xAxisCurrentFeedValue;
        private int _yAxisCurrentFeedValue;
        private XYMotionPositionViewModel _onPosition;

        public string Name { get; }

        public byte XAxisId { get; }

        public byte YAxisId { get; }

        public bool HadXAxisReturnedToHomePosition
        {
            get { return _hadXAxisReturnedToHomePosition; }
            set
            {
                if (value != _hadXAxisReturnedToHomePosition)
                {
                    _hadXAxisReturnedToHomePosition = value;

                    RaisePropertyChanged(nameof(HadXAxisReturnedToHomePosition));
                }
            }
        }

        public bool HadYAxisReturnedToHomePosition
        {
            get { return _hadYAxisReturnedToHomePosition; }
            set
            {
                if (value != _hadYAxisReturnedToHomePosition)
                {
                    _hadYAxisReturnedToHomePosition = value;

                    RaisePropertyChanged(nameof(HadYAxisReturnedToHomePosition));
                }
            }
        }

        public bool IsJogOperation
        {
            get { return _isJogOperation; }
            set
            {
                if (value != _isJogOperation)
                {
                    _isJogOperation = value;

                    RaisePropertyChanged(nameof(IsJogOperation));
                }
            }
        }

        public int XAxisJogSpeed
        {
            get { return _xAxisJogSpeed; }
            set
            {
                if (value <= 0)
                    return;

                if (value > _profile.Parameters.JogSpeedLimit)
                    value = _profile.Parameters.JogSpeedLimit;

                _xAxisJogSpeed = value;
            }
        }

        public int YAxisJogSpeed
        {
            get { return _yAxisJogSpeed; }
            set
            {
                if (value <= 0)
                    return;

                if (value > _profile.Parameters.JogSpeedLimit)
                    value = _profile.Parameters.JogSpeedLimit;

                _yAxisJogSpeed = value;
            }
        }

        public double XAxisCurrentFeedValue { get; private set; }

        public double YAxisCurrentFeedValue { get; private set; }

        public int AccelerationTime
        {
            get { return _profile.Parameters.AccelerationTime; }
            set
            {
                if (value <= 0)
                    return;

                var parameters = _profile.Parameters;

                if (value != parameters.AccelerationTime)
                {
                    parameters.AccelerationTime = value;
                    parameters.DecelerationTime = value;

                    UpdateParameters();
                }
            }
        }

        public int PositioningSpeed
        {
            get { return _profile.Parameters.PositioningSpeed; }
            set
            {
                if (value <= 0)
                    return;

                var parameters = _profile.Parameters;

                if (value != parameters.PositioningSpeed)
                {
                    parameters.SpeedLimit = value;
                    parameters.PositioningSpeed = value;

                    UpdateParameters();
                }
            }
        }

        public IEnumerable<XYMotionPositionViewModel> Positions { get { return _positions; } }

        public ICommand ReturnToHomePositionCommand { get; }

        public ICommand StopAxisCommand { get; }

        public ICommand StartXAxisJogReverseCommand { get; }

        public ICommand StartXAxisJogForwardCommand { get; }

        public ICommand StopXAxisJogCommand { get; }

        public ICommand StartYAxisJogReverseCommand { get; }

        public ICommand StartYAxisJogForwardCommand { get; }

        public ICommand StopYAxisJogCommand { get; }

        public ICommand GoOrUpdatePositioningValueCommand { get; }

        public ICommand ConfigureCommand { get; }

        /// <summary>
        /// XY軸線性設備
        /// </summary>
        /// <param name="name"></param>
        /// <param name="xAxisId"></param>
        /// <param name="yAxisId"></param>
        /// <param name="decimals"></param> 數值是小數後第幾位
        public XYLinearMotionViewModel(string name, byte xAxisId, byte yAxisId, byte decimals)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            if (xAxisId == 0 | yAxisId == 0)
                throw new ArgumentException("The 'axisId' argument must be greater than zero.");

            if (decimals == 0)
                throw new ArgumentException("The 'decimals' argument cannot be zero.", "decimals");

            _xAxisName = $"ARM{xAxisId.ToString()} X {name}";
            _yAxisName = $"ARM{yAxisId.ToString()} Y {name}";

            ServoDriveProfile xAxisProfile, yAxisProfile;

            using (var reader = new StreamReader(Path.Combine(Environment.EquipmentProfileRootFolder, $"{_xAxisName}.xml"), Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(ServoDriveProfile));

                xAxisProfile = (ServoDriveProfile)serializer.Deserialize(reader);
                xAxisProfile.CheckAfterDeserialization();
                _xAxisJogSpeed = xAxisProfile.Parameters.JogSpeed;
            }

            using (var reader = new StreamReader(Path.Combine(Environment.EquipmentProfileRootFolder, $"{_yAxisName}.xml"), Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(ServoDriveProfile));

                yAxisProfile = (ServoDriveProfile)serializer.Deserialize(reader);
                yAxisProfile.CheckAfterDeserialization();
                _yAxisJogSpeed = yAxisProfile.Parameters.JogSpeed;
            }

            ServoDrivePosition[] xAxisPositions = xAxisProfile.Positions;

            _positions = new XYMotionPositionViewModel[xAxisPositions.Length];

            var positionIni = Environment.PositionIni;
            int i = 0;

            foreach (var xAxisPos in xAxisPositions)
            {
                if (!yAxisProfile.TryGetPosition(xAxisPos.Name, out ServoDrivePosition yAxisPos))
                    throw new NotFoundException($"'{xAxisPos.Name}' position does not exist in { _yAxisName }.xml.");

                string positionDisplayName = positionIni.GetValue(_xAxisName, xAxisPos.Id.ToString());

                _positions[i++] = new XYMotionPositionViewModel(name, positionDisplayName, xAxisPos, xAxisPos.Value, yAxisPos.Value, decimals);
            }

            Array.Sort(_positions, (x, y) => { return x.PositionId - y.PositionId; });

            _profile = xAxisProfile;

            _controller = Environment.UnityContainer.Resolve<AutomatedDispensingController>();

            _magnification = Math.Pow(10, decimals);

            Name = name;
            XAxisId = xAxisId;
            YAxisId = yAxisId;

            ReturnToHomePositionCommand = new RelayCommand(ReturnToHomePosition);
            StopAxisCommand = new RelayCommand(StopAxis);
            StartXAxisJogReverseCommand = new RelayCommand(StartXAxisJogReverse);
            StartXAxisJogForwardCommand = new RelayCommand(StartXAxisJogForward);
            StopXAxisJogCommand = new RelayCommand(StopXAxisJog);
            StartYAxisJogReverseCommand = new RelayCommand(StartYAxisJogReverse);
            StartYAxisJogForwardCommand = new RelayCommand(StartYAxisJogForward);
            StopYAxisJogCommand = new RelayCommand(StopYAxisJog);
            GoOrUpdatePositioningValueCommand = new RelayCommand<XYMotionPositionViewModel>(GoOrUpdatePositioningValue);
            ConfigureCommand = new RelayCommand<Window>(Configure);

            Environment.PhysicalRegisterWatcher.AddObserver(this, true);
        }

        private void ReturnToHomePosition()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(Name);

            message.ReturnToHomePosition();

            _controller.PostMessage(message);
        }

        private void StopAxis()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(Name);

            message.StopAxis();

            _controller.PostMessage(message);
        }

        private void StartXAxisJogReverse()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(_xAxisName);

            message.StartJog(-_xAxisJogSpeed);

            _controller.PostMessage(message);
        }

        private void StartXAxisJogForward()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(_xAxisName);

            message.StartJog(_xAxisJogSpeed);

            _controller.PostMessage(message);
        }

        private void StopXAxisJog()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(_xAxisName);

            message.StopJog();

            _controller.PostMessage(message);
        }

        private void StartYAxisJogReverse()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(_yAxisName);

            message.StartJog(-_yAxisJogSpeed);

            _controller.PostMessage(message);
        }

        private void StartYAxisJogForward()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(_yAxisName);

            message.StartJog(_yAxisJogSpeed);

            _controller.PostMessage(message);
        }

        private void StopYAxisJog()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(_yAxisName);

            message.StopJog();

            _controller.PostMessage(message);
        }

        private void GoOrUpdatePositioningValue(XYMotionPositionViewModel positionViewModel)
        {
            if (_isJogOperation)
            {
                int x = _xAxisCurrentFeedValue;
                int y = _yAxisCurrentFeedValue;

                XYMotionPositioningValueDialog dlg = new XYMotionPositioningValueDialog
                {
                    PositionName = positionViewModel.PositionName,
                    X = x,
                    Y = y,
                    Owner = Application.Current.MainWindow
                };

                if (dlg.ShowDialog() != true)
                    return;

                positionViewModel.SetPointText(dlg.X, dlg.Y);

                ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(Name);

                message.ConfigurePositions(new ServoPoint(positionViewModel.PositionName, x, y));

                _controller.PostMessage(message);
            }
            else
            {
                positionViewModel.Go(_controller);
            }
        }

        private void Configure(Window owner)
        {
            ServoDriveConfigurationDialog dlg = new ServoDriveConfigurationDialog
            {
                Owner = owner,
                DataContext = this
            };

            dlg.ShowDialog();
        }

        private void UpdateParameters()
        {
            var message = new ServoDriveManualControlMessage(Name);

            message.ConfigureParameters(_profile.Parameters);

            _controller.PostMessage(message);
        }

        public void RefreshPhysicalRegister(IORegister io, MotionControlRegister motionControl)
        {
            ushort statusX = motionControl.GetStatus(XAxisId);
            ushort statusY = motionControl.GetStatus(YAxisId);

            HadXAxisReturnedToHomePosition = BitOperation.GetBit(statusX, MotionControlStatusBitOrdinal.ReturnedHome);
            HadYAxisReturnedToHomePosition = BitOperation.GetBit(statusY, MotionControlStatusBitOrdinal.ReturnedHome);

            int xAxisCurrentFeedValue = motionControl.GetCurrentFeedValue(XAxisId);
            int yAxisCurrentFeedValue = motionControl.GetCurrentFeedValue(YAxisId);

            if (xAxisCurrentFeedValue != _xAxisCurrentFeedValue)
            {
                _xAxisCurrentFeedValue = xAxisCurrentFeedValue;
                XAxisCurrentFeedValue = xAxisCurrentFeedValue / _magnification;
                RaisePropertyChanged(nameof(XAxisCurrentFeedValue));
            }

            if (yAxisCurrentFeedValue != _yAxisCurrentFeedValue)
            {
                _yAxisCurrentFeedValue = yAxisCurrentFeedValue;
                YAxisCurrentFeedValue = yAxisCurrentFeedValue / _magnification;
                RaisePropertyChanged(nameof(YAxisCurrentFeedValue));
            }

            ushort xAxisCurrentPositionId = (ushort)(motionControl.GetCurrentPositionId(XAxisId) & 0xFFFF);
            ushort yAxisCurrentPositionId = (ushort)(motionControl.GetCurrentPositionId(YAxisId) & 0xFFFF);

            if (xAxisCurrentPositionId == 0 | yAxisCurrentPositionId == 0 | xAxisCurrentPositionId != yAxisCurrentPositionId)
            {
                if (_onPosition != null)
                {
                    _onPosition.IsOn = false;
                    _onPosition = null;
                }
            }
            else
            {
                ushort id = xAxisCurrentPositionId;

                if (_onPosition != null)
                {
                    if (id == _onPosition.PositionId)
                        return;

                    _onPosition.IsOn = false;
                }

                int min = 0;
                int max = _positions.Length - 1;

                while (min <= max)
                {
                    int mid = (min + max) / 2;

                    XYMotionPositionViewModel position = _positions[mid];
                    ushort positionId = position.PositionId;

                    if (id == positionId)
                    {
                        position.IsOn = true;
                        _onPosition = position;
                        return;
                    }

                    if (id < positionId)
                    {
                        max = mid - 1;
                    }
                    else
                    {
                        min = mid + 1;
                    }
                }

                throw new BugException("Should not go in here.");
            }
        }
    }
}
