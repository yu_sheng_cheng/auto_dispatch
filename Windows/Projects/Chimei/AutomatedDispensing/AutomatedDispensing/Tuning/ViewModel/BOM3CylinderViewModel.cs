﻿using AutomatedDispensing.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Tuning.ViewModel
{
    class BOM3CylinderViewModel
    {
        private readonly CylinderViewModel[] _cylinderViewModels;

        public IEnumerable<CylinderViewModel> CylinderViewModels { get { return _cylinderViewModels; } }

        public BOM3CylinderViewModel()
        {
            _cylinderViewModels = new CylinderViewModel[]
            {
                Environment.GetCylinderViewModel("限位推瓶汽缸"),
                Environment.GetCylinderViewModel("擋瓶汽缸2"),
                Environment.GetCylinderViewModel("擋瓶汽缸1"),
                Environment.GetCylinderViewModel("推樣品瓶汽缸"),
                Environment.GetCylinderViewModel("進料旋轉汽缸"),
                Environment.GetCylinderViewModel("進料上下汽缸"),
                Environment.GetCylinderViewModel("進料夾爪汽缸"),
                Environment.GetCylinderViewModel("倒廢液汽缸"),
                Environment.GetCylinderViewModel("倒廢液夾爪汽缸"),
                Environment.GetCylinderViewModel("推瓶回收區汽缸"),
                Environment.GetCylinderViewModel("推瓶待處理區汽缸"),
                Environment.GetCylinderViewModel("推瓶NG區汽缸"),
                Environment.GetCylinderViewModel("上下移載汽缸"),
                Environment.GetCylinderViewModel("移載夾爪汽缸")
            };
        }
    }
}
