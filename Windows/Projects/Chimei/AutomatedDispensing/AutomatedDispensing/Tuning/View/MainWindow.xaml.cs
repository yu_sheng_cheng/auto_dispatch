﻿using AutomatedDispensing.Controller;
using AutomatedDispensing.Controller.Modules.AutoDistribution;
using AutomatedDispensing.Controller.Modules.AutomatedDispensing;
using AutomatedDispensing.Controller.Modules.BOM2.Ml1Dispatch;
using AutomatedDispensing.Controller.Modules.BOM2.Pipette1mL;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AutomatedDispensing.Controller.Modules.AutomatedDispensing.parallel.conveyor;
using AutomatedDispensing.Controller.Modules.BOM2.DiscardWasteLiquid;
using AutomatedDispensing.Controller.Modules.context;
using Unity;
using Widget;

namespace AutomatedDispensing.Tuning.View
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private readonly AutomatedDispensingController _controller;

        public MainWindow()
        {
            InitializeComponent();

            _controller = Environment.UnityContainer.Resolve<AutomatedDispensingController>();
        }

        private void MetroWindow_SourceInitialized(object sender, EventArgs e)
        {
            //HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);
            //source.AddHook(new HwndSourceHook(WndProc));
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case Win32.WM_SYSCOMMAND:
                    switch (wParam.ToInt32() & 0xFFF0)
                    {
                        case Win32.SC_MOVE:
                        case Win32.SC_RESTORE:
                            handled = true;
                            break;
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        private void AutomatedDispensing_Click(object sender, RoutedEventArgs e)
        {
            var autoRunContext = new AutoRunContext();
            var message = new AutomatedDispensingOneCycle(autoRunContext);
            var conveyorDirector = new ConveyorDirector(autoRunContext);
            _controller.PostMessage(message);
        }

        private void SampleBottleCoverGripperCylinder_Click(object sender, RoutedEventArgs e)
        {
            var message = new Automation.Core.Messages.ManualControl.Movable.CylinderManualControlMessage("樣品瓶蓋子夾爪汽缸");

            message.Go("打開");

            _controller.PostMessage(message);
        }

        private void SampleBottleUncoverUpDownCylinder_Click(object sender, RoutedEventArgs e)
        {
            var message = new Automation.Core.Messages.ManualControl.Movable.CylinderManualControlMessage("樣品瓶開蓋上下汽缸");

            message.Go("下降");

            _controller.PostMessage(message);
        }

        private void SampleBottleUncoverUpCylinder_Click(object sender, RoutedEventArgs e)
        {
            var message = new Automation.Core.Messages.ManualControl.Movable.CylinderManualControlMessage("樣品瓶開蓋上下汽缸");

            message.Go("上升");

            _controller.PostMessage(message);
        }

        private void SampleBottleBodyGripperCylinder_Click(object sender, RoutedEventArgs e)
        {
            var message = new Automation.Core.Messages.ManualControl.Movable.CylinderManualControlMessage("樣品瓶身體夾爪汽缸");

            message.Go("打開");

            _controller.PostMessage(message);
        }

        private void OnemlTipTest_Click(object sender, RoutedEventArgs e)
        {
          //  var message = new Dispatch1MOneCycle();
            var message = new Pipette1mlOneCycle();
            _controller.PostMessage(message);

        }

        private void AutoDistribution_Click(object sender, RoutedEventArgs e)
        {
            var message = new AutoDistributionOneCycle();

            _controller.PostMessage(message);
        }

        private void 藥劑瓶開蓋上下汽缸下降_Click(object sender, RoutedEventArgs e)
        {
            var message = new Automation.Core.Messages.ManualControl.Movable.CylinderManualControlMessage("藥劑瓶開蓋上下汽缸");

            message.Go("下降");

            _controller.PostMessage(message);
        }


        private void 藥劑瓶開蓋上下汽缸上升_Click(object sender, RoutedEventArgs e)
        {
            var message = new Automation.Core.Messages.ManualControl.Movable.CylinderManualControlMessage("藥劑瓶開蓋上下汽缸");

            message.Go("上升");

            _controller.PostMessage(message);
        }

        private void 藥劑瓶開蓋身體夾爪汽缸打開_Click(object sender, RoutedEventArgs e)
        {
            var message = new Automation.Core.Messages.ManualControl.Movable.CylinderManualControlMessage("藥劑瓶開蓋身體夾爪汽缸");

            message.Go("打開");

            _controller.PostMessage(message);
        }

        private void 藥劑瓶蓋子夾爪汽缸打開_Click(object sender, RoutedEventArgs e)
        {
            var message = new Automation.Core.Messages.ManualControl.Movable.CylinderManualControlMessage("藥劑瓶蓋子夾爪汽缸");

            message.Go("打開");

            _controller.PostMessage(message);
        }

        private void 開蓋60mL左右移載汽缸往左_Click(object sender, RoutedEventArgs e)
        {
            var message = new Automation.Core.Messages.ManualControl.Movable.CylinderManualControlMessage("60ml開蓋左右移載汽缸");

            message.Go("往左");

                 _controller.PostMessage(message);
        }

        private void 開蓋60mL上下移載汽缸上升_Click(object sender, RoutedEventArgs e)
        {
            var message = new Automation.Core.Messages.ManualControl.Movable.CylinderManualControlMessage("60ml開蓋上下移載汽缸");

            message.Go("上升");

            _controller.PostMessage(message);
        }

        private void 開蓋60mL上下移載汽缸下降_Click(object sender, RoutedEventArgs e)
        {
            var message = new Automation.Core.Messages.ManualControl.Movable.CylinderManualControlMessage("60ml開蓋上下移載汽缸");

            message.Go("下降");

            _controller.PostMessage(message);
        }

        private void 開蓋60mL上夾爪汽缸打開_Click(object sender, RoutedEventArgs e)
        {
            var message = new Automation.Core.Messages.ManualControl.Movable.CylinderManualControlMessage("60ml開蓋上夾爪汽缸");

            message.Go("打開");

            _controller.PostMessage(message);
        }

        private void 開蓋60mL下夾爪汽缸打開_Click(object sender, RoutedEventArgs e)
        {
            var message = new Automation.Core.Messages.ManualControl.Movable.CylinderManualControlMessage("60ml開蓋下夾爪汽缸");

            message.Go("打開");

            _controller.PostMessage(message);
        }

        private void OnDiscardLiquid_Click(object sender, RoutedEventArgs e)
        {
            var oneCycle = new TestDiscardLiquidOneCycle();
            _controller.PostMessage(oneCycle);
        }

    

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}