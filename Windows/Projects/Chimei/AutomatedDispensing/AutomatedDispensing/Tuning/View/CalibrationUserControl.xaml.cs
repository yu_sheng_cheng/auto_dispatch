﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AutomatedDispensing.Tuning.ViewModel;

namespace AutomatedDispensing.Tuning.View
{
    /// <summary>
    /// 台車一控制
    /// Interaction logic for CalibrationUserControl.xaml
    /// </summary>
    public partial class CalibrationUserControl : UserControl
    {
        private DispensingGantryModuleViewModel _dispensingGantryModuleViewModel;
        private InkjetPrinterModuleViewModel _inkjetPrinterModuleViewModel;
        private BOM1CylinderViewModel _bom1CylinderViewModel;
        private BOM2CylinderViewModel _bom2CylinderViewModel;
        private BOM3CylinderViewModel _bom3CylinderViewModel;

        public CalibrationUserControl()
        {
            InitializeComponent();
        }

        private void Car1Button_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new Car1Window {Owner = GetCurrentMainWindow()};
            dlg.ShowDialog();
        }

        private void Car2Button_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new Car2Window
            {
                Owner = GetCurrentMainWindow()
            };
            dlg.ShowDialog();
        }

        private static Window GetCurrentMainWindow()
        {
            return Application.Current.MainWindow;
        }

        private void DispensingGantryModuleButton_Click(object sender, RoutedEventArgs e)
        {
            DispensingGantryModuleWindow dlg = new DispensingGantryModuleWindow
            {
                Owner = GetCurrentMainWindow()
            };

            if (_dispensingGantryModuleViewModel == null)
                _dispensingGantryModuleViewModel = new DispensingGantryModuleViewModel();

            dlg.DataContext = _dispensingGantryModuleViewModel;
            dlg.ShowDialog();
        }

        private void TipModuleButton_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new TipWindow {Owner = GetCurrentMainWindow()};
            dlg.ShowDialog();
        }

        private void ChemicalAgentAreaButton_Click(object sender, RoutedEventArgs e)
        {
            ChemicalAgentAreaWindow dlg = new ChemicalAgentAreaWindow {Owner = GetCurrentMainWindow()};
            dlg.ShowDialog();
        }

        private void FeedingPickupGantryModuleButton_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new FeedingPickupGantryModuleWindow {Owner = GetCurrentMainWindow()};
            dlg.ShowDialog();
        }

        private void InkjetPrinterModuleButton_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new InkjetPrinterModuleWindow
            {
                Owner = GetCurrentMainWindow()
            };

            if (_inkjetPrinterModuleViewModel == null)
                _inkjetPrinterModuleViewModel = new InkjetPrinterModuleViewModel();

            _inkjetPrinterModuleViewModel.Load();

            dlg.DataContext = _inkjetPrinterModuleViewModel;
            dlg.ShowDialog();

            _inkjetPrinterModuleViewModel.Unload();
        }

        private void BOM1CylinderButton_Click(object sender, RoutedEventArgs e)
        {

            var dlg = new BOM1CylinderWindow
            {
                Owner = GetCurrentMainWindow()
            };

            if (_bom1CylinderViewModel == null)
                _bom1CylinderViewModel = new BOM1CylinderViewModel();

            dlg.DataContext = _bom1CylinderViewModel;
            dlg.ShowDialog();
        }

        private void BOM2CylinderButton_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new BOM2CylinderWindow
            {
                Owner = GetCurrentMainWindow()
            };

            if (_bom2CylinderViewModel == null)
                _bom2CylinderViewModel = new BOM2CylinderViewModel();

            dlg.DataContext = _bom2CylinderViewModel;
            dlg.ShowDialog();
        }

        private void BOM3CylinderButton_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new BOM3CylinderWindow
            {
                Owner = GetCurrentMainWindow()
            };

            if (_bom3CylinderViewModel == null)
                _bom3CylinderViewModel = new BOM3CylinderViewModel();

            dlg.DataContext = _bom3CylinderViewModel;
            dlg.ShowDialog();
        }

        private void LoadCellButton_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new LoadCellWindow {Owner = GetCurrentMainWindow()};
            dlg.ShowDialog();
        }

        private void BottlePickupTransferButton_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new BottlePickupTransferWindow {Owner = GetCurrentMainWindow()};
            dlg.ShowDialog();
        }
    }
}