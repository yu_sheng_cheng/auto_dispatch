﻿using AutomatedDispensing.Tuning.ViewModel;
using SwissKnife;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace AutomatedDispensing.Tuning.View
{
    [ValueConversion(typeof(InkjetPattern), typeof(bool))]
    class InkjetPatternConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            InkjetPattern? checkvalue = value as InkjetPattern?;

            if (checkvalue == null || parameter == null)
                return false;

            InkjetPattern targetvalue;

            if (!EnumUtils.TryParse(parameter.ToString(), out targetvalue))
                return false;

            return targetvalue == checkvalue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || parameter == null)
                return null;

            if ((bool)value)
            {
                InkjetPattern targetvalue;

                if (EnumUtils.TryParse(parameter.ToString(), out targetvalue))
                    return targetvalue;
            }

            return null;
        }
    }
}
