﻿using AutomatedDispensing.ViewModel;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Widget;

namespace AutomatedDispensing.Tuning.View
{
    /// <summary>
    /// BottlePickupTransferWindow.xaml 的互動邏輯
    /// </summary>
    public partial class BottlePickupTransferWindow : MetroWindow
    {
        private readonly CylinderViewModel[] _cylinderViewModels;


        public IEnumerable<CylinderViewModel> CylinderViewModels { get { return _cylinderViewModels; } }


        public BottlePickupTransferWindow()
        {
            InitializeComponent();

            _cylinderViewModels = new CylinderViewModel[]
            {
                Environment.GetCylinderViewModel("推樣品瓶汽缸"),
                Environment.GetCylinderViewModel("進料旋轉汽缸"),
                Environment.GetCylinderViewModel("進料上下汽缸"),
                Environment.GetCylinderViewModel("進料夾爪汽缸"),
                Environment.GetCylinderViewModel("倒廢液汽缸"),
                Environment.GetCylinderViewModel("倒廢液夾爪汽缸"),
                Environment.GetCylinderViewModel("推瓶回收區汽缸"),
                Environment.GetCylinderViewModel("推瓶待處理區汽缸"),
                Environment.GetCylinderViewModel("推瓶NG區汽缸"),
                Environment.GetCylinderViewModel("上下移載汽缸"),
                Environment.GetCylinderViewModel("移載夾爪汽缸"),
            };

            DataContext = this;
        }

        private void MetroWindow_SourceInitialized(object sender, EventArgs e)
        {
            HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);
            source.AddHook(new HwndSourceHook(WndProc));
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case Win32.WM_SYSCOMMAND:
                    switch (wParam.ToInt32() & 0xFFF0)
                    {
                        case Win32.SC_MOVE:
                        case Win32.SC_RESTORE:
                            handled = true;
                            break;
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            BottlePickupTransferControl.DataContext = Environment.BottlePickupTransferViewModel;
        }
    }
}
