﻿using AutomatedDispensing.Model.Interfaces;
using Automation.Core.Registers;
using System;
using System.Windows;

namespace AutomatedDispensing.Tuning.View
{
    /// <summary>
    /// LoadCellWindow.xaml 的互動邏輯, 荷重元
    /// </summary>
    public partial class LoadCellWindow : Window, IPhysicalRegisterObserver
    {
        public LoadCellWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Environment.PhysicalRegisterWatcher.AddObserver(this, true);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Environment.PhysicalRegisterWatcher.AddObserver(this, true);
        }

        public void RefreshPhysicalRegister(IORegister register, MotionControlRegister motionControl)
        {
            TextBlock1.Text = register.GetAnalogInput(6640).ToString(); //藥劑瓶手動入料
            TextBlock2.Text = register.GetAnalogInput(6641).ToString(); //藥劑瓶自動入料
            TextBlock3.Text = register.GetAnalogInput(6642).ToString(); //樣品瓶
            TextBlock4.Text = register.GetAnalogInput(6643).ToString();
        }
    }
}
