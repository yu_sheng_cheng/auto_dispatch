﻿using Automation.Core.Registers;
using Automation.Objects.Movable.ServoDrive;
using SwissKnife;
using SwissKnife.Conversion;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller
{
    public partial class AutomatedDispensingController
    {
        private readonly ArrayBuilder<byte> D6000 = new ArrayBuilder<byte>();
        private readonly ushort[] _ushortArray = new ushort[18];
        private readonly int[] _intArray = new int[180];
        private readonly OneWriteSeqLock _registerSeqLock = new OneWriteSeqLock();
        private bool _firstRefresh = true;
        private bool _plcDisconnected;
        private readonly OneWriteSeqLock _ioRegisterSeqLock = new OneWriteSeqLock();


        private IORegister CreateIORegister()
        {
            IOMemory contactIOMemory = new IOMemory(IOSignalType.Digital,
                new Tuple<ushort, ushort>[] { new Tuple<ushort, ushort>(0x100, 256), new Tuple<ushort, ushort>(0x300, 32) }, 6000);

            IOMemory coilIOMemory = new IOMemory(IOSignalType.Digital,
                new Tuple<ushort, ushort>[] { new Tuple<ushort, ushort>(0x100, 160), new Tuple<ushort, ushort>(0x300, 32) }, 6020);

            IOMemory analogInputIOMemory = new IOMemory(IOSignalType.Analog, 0x0, 6640, 8);

            return new IORegister(contactIOMemory, coilIOMemory, analogInputIOMemory, null);
        }

        private MotionControlRegister CreateMotionControlRegister()
        {
            return new MotionControlRegister((ushort)ARMs.Length);
        }

        protected override async Task<bool> RefreshAsync()
        {
            OperationResult result = await _melsec.ReadUInt8Async("D6000", 960 * 2, D6000).ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                if (!_plcDisconnected)
                {
                    _plcDisconnected = true;
                    Log.w(LogKeyword, $"{_melsec.Type.Name} Read: {result}");
                }
                return false;
            }

            _plcDisconnected = false;

            byte[] d6000 = D6000.Buffer;

            IByteConverter byteConverter = _melsec.ByteConverter;

            var ioRegister = IORegister;
            var motionControlRegister = MotionControlRegister;

            byteConverter.ToUInt16(d6000, 0, 18 * 2, ioRegister.ContactRegisters, 0); // D6000 - D6017 (實體X)

            ioRegister.NotContact(new BitCell(6000, 5));

            byteConverter.ToUInt16(d6000, 100 * 2, 18 * 2, _ushortArray, 0); // D6100 - D6117 (軸控IO讀)
            byteConverter.ToInt32(d6000, 699 * 2, 18 * 4 * 4, _intArray, 0); // D6699 - D6842 (軸控DWORD讀)

            int i = 0, j = 0;

            foreach (var arm in ARMs)
            {
                var ioMemory = arm.IOMemory;

                ushort value = _ushortArray[i];

                ioMemory.HadReturnedToHomePosition = BitOperation.GetBit(value, 0);
                ioMemory.IsPositioningCompleted = BitOperation.GetBit(value, 1);
                ioMemory.IsBusy = BitOperation.GetBit(value, 2);
                ioMemory.HasErrors = BitOperation.GetBit(value, 3);
                ioMemory.IsReturningToHomePosition = BitOperation.GetBit(value, 4);

                ioMemory.CurrentFeedValue = _intArray[j++];
                ioMemory.ErrorCode = unchecked((ushort)_intArray[j++]);
                ioMemory.CurrentTorque = _intArray[j++];
                ++j;

                ushort status = 0;

                if (arm.IsΘ | ioMemory.HadReturnedToHomePosition)
                    status |= 1 << MotionControlStatusBitOrdinal.ReturnedHome;
                if (ioMemory.IsBusy)
                    status |= 1 << MotionControlStatusBitOrdinal.Busy;

                ushort axisId = unchecked((ushort)(i + 1));

                motionControlRegister.SetStatus(axisId, status);
                motionControlRegister.SetCurrentFeedValue(axisId, ioMemory.CurrentFeedValue);
                motionControlRegister.SetCurrentTorque(axisId, ioMemory.CurrentTorque);

                arm.TryGetCurrentPosition(out string positionName, out uint positionId);
                motionControlRegister.SetCurrentPositionId(axisId, positionId);

                ++i;
            }

            foreach (var arm in XYARMs)
            {
                arm.TryGetCurrentPosition(out string positionName, out uint positionId);

                motionControlRegister.SetCurrentPositionId(arm.AxisX.Id, positionId);
                motionControlRegister.SetCurrentPositionId(arm.AxisY.Id, positionId);
            }

            byteConverter.ToInt32(d6000, 640 * 2, 4 * 4, _intArray, 0); // D6640 - D6647 (4個荷重元)

            ushort[] analogInputs = ioRegister.AnalogInputRegisters;

            for (i = 0; i < 4; ++i)
            {
                analogInputs[i * 2] = unchecked((ushort)_intArray[i]);
            }

            ushort d6120 = byteConverter.ToUInt16(d6000, 120 * 2);

            bool reboot = !BitOperation.GetBit(d6120, 7);

            if (_firstRefresh | reboot)
            {
                _firstRefresh = false;

                byteConverter.ToUInt16(d6000, 20 * 2, 12 * 2, ioRegister.CoilRegisters, 0); // D6020 - D6031 (實體Y)

                byteConverter.ToUInt16(d6000, 140 * 2, 18 * 2, _ushortArray, 0); // D6140 - D6157 (軸控IO寫)

                i = 0;

                foreach (var arm in ARMs)
                {
                    ushort value = _ushortArray[i++];

                    ServoDriveCommand command = ServoDriveCommand.None;

                    if (BitOperation.GetBit(value, 0))
                        command |= ServoDriveCommand.JogForward;
                    if (BitOperation.GetBit(value, 1))
                        command |= ServoDriveCommand.JogReverse;
                    if (BitOperation.GetBit(value, 2))
                        command |= ServoDriveCommand.Go;
                    if (BitOperation.GetBit(value, 3))
                        command |= ServoDriveCommand.AxisToStop;
                    if (BitOperation.GetBit(value, 4))
                        command |= ServoDriveCommand.ServoOff;
                    if (BitOperation.GetBit(value, 5))
                        command |= ServoDriveCommand.ReturnToHomePosition;
                    if (BitOperation.GetBit(value, 6))
                        command |= ServoDriveCommand.Reset;

                    arm.IOMemory.Command = command;
                }

                if (reboot)
                    OnEquipmentReboot();
            }

            _registerSeqLock.WriteSeqLock();
            ioRegister.CopyTo(_cacheIORegister);
            motionControlRegister.CopyTo(_cacheMotionControlRegister);
            _registerSeqLock.WriteSeqUnlock();
            return true;
        }

        protected override async Task<bool> UpdateAsync()
        {
            byte[] d6000 = D6000.Buffer;

            IByteConverter byteConverter = _melsec.ByteConverter;

            var ioRegister = IORegister;
            var motionControlRegister = MotionControlRegister;

            byteConverter.GetBytes(ioRegister.CoilRegisters, d6000, 20 * 2); // D6020 - D6031 (實體Y)

            int i = 0, j = 0;

            foreach (var arm in ARMs)
            {
                var ioMemory = arm.IOMemory;

                ushort value = 0;

                ServoDriveCommand command = ioMemory.Command;

                if ((command & ServoDriveCommand.JogForward) == ServoDriveCommand.JogForward)
                    value = unchecked((ushort)BitOperation.SetBit(value, 0, true));
                if ((command & ServoDriveCommand.JogReverse) == ServoDriveCommand.JogReverse)
                    value = unchecked((ushort)BitOperation.SetBit(value, 1, true));
                if ((command & ServoDriveCommand.Go) == ServoDriveCommand.Go)
                    value = unchecked((ushort)BitOperation.SetBit(value, 2, true));
                if ((command & ServoDriveCommand.AxisToStop) == ServoDriveCommand.AxisToStop)
                    value = unchecked((ushort)BitOperation.SetBit(value, 3, true));
                if ((command & ServoDriveCommand.ServoOff) == ServoDriveCommand.ServoOff)
                    value = unchecked((ushort)BitOperation.SetBit(value, 4, true));
                if ((command & ServoDriveCommand.ReturnToHomePosition) == ServoDriveCommand.ReturnToHomePosition)
                    value = unchecked((ushort)BitOperation.SetBit(value, 5, true));
                if ((command & ServoDriveCommand.Reset) == ServoDriveCommand.Reset)
                    value = unchecked((ushort)BitOperation.SetBit(value, 6, true));

                _ushortArray[i++] = value;

                _intArray[j++] = ioMemory.PositioningValue;
                _intArray[j++] = ioMemory.PositioningSpeed;
                _intArray[j++] = ioMemory.AccelerationTime;
                _intArray[j++] = ioMemory.SpeedLimit;
                _intArray[j++] = ioMemory.JogSpeed;
                j += 5;
            }

            int armCount = ARMs.Length;

            byteConverter.GetBytes(_ushortArray, 0, armCount, d6000, 140 * 2);   // D6140 - D6157 (軸控IO寫)
            byteConverter.GetBytes(_intArray, 0, armCount * 10, d6000, 200 * 2); // D6200 - D6559 (軸控DWORD寫)

            ArrayBuilder<byte> buffer = new ArrayBuilder<byte>();

            buffer.Length = 24;

            Array.Copy(d6000, 40, buffer.Buffer, 0, 24);

            OperationResult result = await _melsec.WriteAsync("D6020", buffer).ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                if (!_plcDisconnected)
                {
                    _plcDisconnected = true;
                    Log.w(LogKeyword, $"{_melsec.Type.Name} Write: {result}");
                }
                return false;
            }

            buffer.Length = 2;

            Array.Copy(d6000, 240, buffer.Buffer, 0, 2);

            result = await _melsec.WriteAsync("D6120", buffer).ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                if (!_plcDisconnected)
                {
                    _plcDisconnected = true;
                    Log.w(LogKeyword, $"{_melsec.Type.Name} Write: {result}");
                }
                return false;
            }

            

            buffer.Length = 720;

            Array.Copy(d6000, 400, buffer.Buffer, 0, 720);

            result = await _melsec.WriteAsync("D6200", buffer).ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                if (!_plcDisconnected)
                {
                    _plcDisconnected = true;
                    Log.w(LogKeyword, $"{_melsec.Type.Name} Write: {result}");
                }
                return false;
            }

            buffer.Length = 36;

            Array.Copy(d6000, 280, buffer.Buffer, 0, 36);

            result = await _melsec.WriteAsync("D6140", buffer).ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                if (!_plcDisconnected)
                {
                    _plcDisconnected = true;
                    Log.w(LogKeyword, $"{_melsec.Type.Name} Write: {result}");
                }
                return false;
            }

            /*OperationResult result = await _melsec.WriteAsync("D6000", D6000).ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                if (!_plcDisconnected)
                {
                    _plcDisconnected = true;
                    Log.w(LogKeyword, $"{_melsec.Type.Name} Write: {result}");
                }
                return false;
            }*/

            _plcDisconnected = false;
            return true;
        }

        protected override async Task<OperationResult<bool>> CheckIfEquipmentHasRebootAsync()
        {
            OperationResult<ushort> result = await _melsec.ReadUInt16Async("D6120").ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                Log.i(LogKeyword, $"{_melsec.Type.Name} Read: {result}");
                return OperationResultFactory.CreateFailedResult<bool>(result);
            }

            return OperationResultFactory.CreateSuccessfulResult(!BitOperation.GetBit(result.Value, 7));
        }

        protected override void OnBootCompleted()
        {
            base.OnBootCompleted();

            byte[] d6000 = D6000.Buffer;

            IByteConverter byteConverter = _melsec.ByteConverter;

            ushort d6120 = byteConverter.ToUInt16(d6000, 120 * 2);

            d6120 = unchecked((ushort)BitOperation.SetBit(d6120, 7, true));

            byteConverter.GetBytes(d6120, d6000, 120 * 2);
        }

        public void ReadRegister(IORegister ioRegister, MotionControlRegister motionControlRegister)
        {
            uint seq;

            do
            {
                seq = _registerSeqLock.ReadSeqBegin();

                _cacheIORegister.CopyTo(ioRegister);
                _cacheMotionControlRegister.CopyTo(motionControlRegister);

            } while (_registerSeqLock.ReadSeqRetry(seq));
        }

        public void ReadIORegister(IORegister register)
        {
            uint seq;

            do
            {
                seq = _ioRegisterSeqLock.ReadSeqBegin();

                _cacheIORegister.CopyTo(register);

            } while (_ioRegisterSeqLock.ReadSeqRetry(seq));
        }
    }
}
