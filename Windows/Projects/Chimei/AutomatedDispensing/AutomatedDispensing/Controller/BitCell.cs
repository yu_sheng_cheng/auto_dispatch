﻿using Automation.Core.Registers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller
{
    public partial class AutomatedDispensingController
    {
        /// <summary>
        /// 輸送帶馬達線
        /// </summary>
        public static readonly BitCell FeedingMotorOutput = new BitCell(6029, 0xA);
        /// <summary>
        /// 判斷有物體在是否在擋瓶汽缸1前
        /// </summary>
        public static readonly BitCell FeedingWaitBottlePosition1Input = new BitCell(6012, 0);
        /// <summary>
        /// 偵測入料等瓶位置2: 有瓶
        /// </summary>
        public static readonly BitCell FeedingWaitBottlePosition2Input = new BitCell(6012, 1);

        public static readonly BitCell 臨時蓋真空檢知Input = new BitCell(6006, 0xE);
        public static readonly BitCell 吸臨時蓋真空Output = new BitCell(6026, 0xE);

        public static readonly BitCell HollowMotorVacuumGaugesInput = new BitCell(6000, 8);
        public static readonly BitCell HollowMotorSuctionOutput = new BitCell(6021, 0xC);
        public static readonly BitCell KeyenceIVGOkInput = new BitCell(6016, 9);
    }
}
