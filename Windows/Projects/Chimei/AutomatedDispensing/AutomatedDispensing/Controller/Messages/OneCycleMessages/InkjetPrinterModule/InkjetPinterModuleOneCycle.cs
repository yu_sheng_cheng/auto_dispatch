﻿using AutomatedDispensing.Controller.Peripheral;
using System;

namespace AutomatedDispensing.Controller.Messages.OneCycleMessages.InkjetPrinterModule
{
    sealed partial class InkjetPrinterModuleOneCycle 
    {
        private Action<string> _barcodeReceived;
        private string _barcode = string.Empty;

        public InkjetPrinterDocument Document { get; }

        public bool HasBottle { get; set; }

        public string Barcode
        {
            get { return _barcode; }
            set
            {
                _barcode = value ?? string.Empty;

                if (!string.IsNullOrEmpty(value))
                    _barcodeReceived?.Invoke(_barcode);
            }
        }

        public InkjetPrinterModuleOneCycle(InkjetPrinterDocument document, Action<string> barcodeReceived = null)
        {
            Document = document;
            _barcodeReceived = barcodeReceived;
        }
    }
}
