﻿using AutomatedDispensing.Controller.MovableObjects;
using Automation.Core.Registers;
using Automation.Objects.Movable.Cylinders;
using Automation.Objects.Movable.ServoDrive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller
{
    public partial class AutomatedDispensingController
    {
        public ServoDriveControl[] ARMs { get; } = new ServoDriveControl[18];

        public XYLinearMotion[] XYARMs { get; } = new XYLinearMotion[3];

        public XYLinearMotion EmptyBottleTransferARM { get; private set; }

        public XYLinearMotion PrescriptionTransferARM { get; private set; }

        public XYLinearMotion BottlePickupTransferARM { get; private set; }

        private void Build()
        {
            ARMs[0] = new ServoDriveControl(this, "ARM1 X 空瓶移載手臂", 1);
            ARMs[1] = new ServoDriveControl(this, "ARM2 Y 空瓶移載手臂", 2);
            ARMs[2] = new ServoDriveControl(this, "ARM3 X 送料手臂", 3);
            ARMs[3] = new ServoDriveControl(this, "ARM4 Z 1.5ml夾取手臂", 4);
            ARMs[4] = new ServoDriveControl(this, "ARM5 X 配藥移載手臂", 5);
            ARMs[5] = new ServoDriveControl(this, "ARM6 Y 配藥移載手臂", 6);
            ARMs[6] = new ServoDriveControl(this, "ARM7 X 藥劑瓶移載", 7);
            ARMs[7] = new ServoDriveControl(this, "ARM8 Θ 藥劑瓶開蓋手臂", 8);
            ARMs[8] = new ServoDriveControl(this, "ARM9 Θ 樣品瓶開蓋手臂", 9);
            ARMs[9] = new ServoDriveControl(this, "ARM10 Θ 60ml開蓋手臂", 10);
            ARMs[10] = new ServoDriveControl(this, "ARM11 Z1 1ml取TIP手臂", 11);
            ARMs[11] = new ServoDriveControl(this, "ARM12 Z2 10ml取TIP手臂", 12);
            ARMs[12] = new ServoDriveControl(this, "ARM13 Y 藥劑瓶夾取手臂", 13);
            ARMs[13] = new ServoDriveControl(this, "ARM14 Z1 1ml移液手臂", 14);
            ARMs[14] = new ServoDriveControl(this, "ARM15 Z2 10ml移液手臂", 15);
            ARMs[15] = new ServoDriveControl(this, "ARM16 X 取瓶移載手臂", 16);
            ARMs[16] = new ServoDriveControl(this, "ARM17 Y 取瓶移載手臂", 17);
            ARMs[17] = new ServoDriveControl(this, "ARM18 X 送料手臂", 18);

            foreach (var arm in ARMs)
            {
                AddMovableObject(arm);
            }

            XYARMs[0] = new XYLinearMotion("空瓶移載手臂", ARMs[0], ARMs[1]);
            XYARMs[1] = new XYLinearMotion("配藥移載手臂", ARMs[4], ARMs[5]);
            XYARMs[2] = new XYLinearMotion("取瓶移載手臂", ARMs[15], ARMs[16]);

            EmptyBottleTransferARM = XYARMs[0];
            PrescriptionTransferARM = XYARMs[1];
            BottlePickupTransferARM = XYARMs[2];

            AddServoSimultaneousAction(EmptyBottleTransferARM);
            AddServoSimultaneousAction(PrescriptionTransferARM);
            AddServoSimultaneousAction(BottlePickupTransferARM);

            AddMovableObject(new DualPositionSolenoidValve(this, "60ml倒液汽缸",
                new BitCell(6003, 4), new BitCell(6003, 5), new BitCell(6023, 4), new BitCell(6023, 5), "正面", "反面"));
            AddMovableObject(new DualPositionSolenoidValve(this, "60ml倒液夾瓶汽缸",
                new BitCell(6003, 7), new BitCell(6003, 6), new BitCell(6023, 7), new BitCell(6023, 6), "打開", "夾持"));
            AddMovableObject(new DualPositionSolenoidValve(this, "60ml吹氣上下汽缸",
                new BitCell(6007, 6), new BitCell(6007, 7), new BitCell(6021, 0xA), new BitCell(6021, 0xB), "上升", "下降"));
            AddMovableObject(new DualPositionSolenoidValve(this, "60ml吹氣汽缸",
                new BitCell(6003, 0xF), new BitCell(6003, 0xE), new BitCell(6023, 0xF), new BitCell(6023, 0xE), "後退", "前進"));
            AddMovableObject(new DualPositionSolenoidValve(this, "1.5ml倒液汽缸",
                new BitCell(6003, 8), new BitCell(6003, 9), new BitCell(6023, 8), new BitCell(6023, 9), "正面", "反面"));
            AddMovableObject(new DualPositionSolenoidValve(this, "1.5ml倒液夾瓶汽缸",
                new BitCell(6003, 0xB), new BitCell(6003, 0xA), new BitCell(6023, 0xB), new BitCell(6023, 0xA), "打開", "夾持"));
            AddMovableObject(new DualPositionSolenoidValve(this, "1.5ml吹氣上下汽缸",
                new BitCell(6007, 4), new BitCell(6007, 5), new BitCell(6021, 8), new BitCell(6021, 9), "上升", "下降"));
            AddMovableObject(new DualPositionSolenoidValve(this, "1.5ml吹氣汽缸",
                new BitCell(6003, 0xD), new BitCell(6003, 0xC), new BitCell(6023, 0xD), new BitCell(6023, 0xC), "後退", "前進"));
            AddMovableObject(new DualPositionSolenoidValve(this, "噴碼前後移載汽缸",
                new BitCell(6002, 3), new BitCell(6002, 2), new BitCell(6022, 3), new BitCell(6022, 2), "後退", "前進"));
            AddMovableObject(new DualPositionSolenoidValve(this, "噴碼左右移載汽缸",
                new BitCell(6002, 4), new BitCell(6002, 5), new BitCell(6022, 4), new BitCell(6022, 5), "往左", "往右"));
            AddMovableObject(new DualPositionSolenoidValve(this, "噴碼上下移載汽缸",
                new BitCell(6002, 7), new BitCell(6002, 6), new BitCell(6022, 7), new BitCell(6022, 6), "下降", "上升"));
            AddMovableObject(new DualPositionSolenoidValve(this, "噴碼翻轉汽缸",
                new BitCell(6002, 9), new BitCell(6002, 8), new BitCell(6022, 9), new BitCell(6022, 8), "垂直", "水平"));
            AddMovableObject(new DualPositionSolenoidValve(this, "噴碼夾爪汽缸",
                new BitCell(6002, 0xB), new BitCell(6002, 0xA), new BitCell(6022, 0xB), new BitCell(6022, 0xA), "打開", "夾持"));
            AddMovableObject(new DualPositionSolenoidValve(this, "空噴汽缸",
                new BitCell(6002, 0xD), new BitCell(6002, 0xC), new BitCell(6022, 0xD), new BitCell(6022, 0xC), "退回", "伸出"));
            AddMovableObject(new DualPositionSolenoidValve(this, "1.5ml抱瓶汽缸",
                new BitCell(6002, 0xF), new BitCell(6002, 0xE), new BitCell(6022, 0xF), new BitCell(6022, 0xE), "退回", "伸出"));
            AddMovableObject(new DualPositionSolenoidValve(this, "1.5ml夾瓶汽缸",
                new BitCell(6002, 1), new BitCell(6002, 0), new BitCell(6022, 1), new BitCell(6022, 0), "打開", "夾持"));
            AddMovableObject(new DualPositionSolenoidValve(this, "60ml移載夾爪上下汽缸",
                new BitCell(6003, 0), new BitCell(6003, 1), new BitCell(6023, 0), new BitCell(6023, 1), "上升", "下降"));
            AddMovableObject(new DualPositionSolenoidValve(this, "60ml移載夾爪汽缸",
                new BitCell(6003, 3), new BitCell(6003, 2), new BitCell(6023, 3), new BitCell(6023, 2), "打開", "夾持"));

            AddMovableObject(new DualPositionSolenoidValve(this, "藥劑瓶開蓋上下汽缸",
                new BitCell(6009, 0xF), new BitCell(6009, 0xE), new BitCell(6025, 0xF), new BitCell(6025, 0xE), "上升", "下降"));
            AddMovableObject(new DualPositionSolenoidValve(this, "藥劑瓶開蓋身體夾爪汽缸",
                new BitCell(6010, 3), new BitCell(6010, 2), new BitCell(6026, 3), new BitCell(6026, 2), "打開", "夾持"));
            AddMovableObject(new NonSensorDualPositionSolenoidValve(this, "藥劑瓶蓋子夾爪汽缸", new BitCell(6026, 1), new BitCell(6026, 0), "打開", "夾持"));
            AddMovableObject(new DualPositionSolenoidValve(this, "樣品瓶開蓋上下汽缸",
                new BitCell(6010, 4), new BitCell(6010, 5), new BitCell(6026, 4), new BitCell(6026, 5), "上升", "下降"));
            AddMovableObject(new DualPositionSolenoidValve(this, "樣品瓶身體夾爪汽缸",
                new BitCell(6010, 9), new BitCell(6010, 8), new BitCell(6026, 9), new BitCell(6026, 8), "打開", "夾持"));
            AddMovableObject(new NonSensorDualPositionSolenoidValve(this, "樣品瓶蓋子夾爪汽缸", new BitCell(6026, 7), new BitCell(6026, 6), "打開", "夾持"));
            AddMovableObject(new DualPositionSolenoidValve(this, "60ml中繼站汽缸",
                new BitCell(6008, 0), new BitCell(6008, 1), new BitCell(6024, 0), new BitCell(6024, 1), "前進", "後退"));
            AddMovableObject(new DualPositionSolenoidValve(this, "藥劑瓶上下汽缸",
                new BitCell(6010, 0xA), new BitCell(6010, 0xB), new BitCell(6027, 0), new BitCell(6027, 1), "上升", "下降"));
            AddMovableObject(new DualPositionSolenoidValve(this, "藥劑瓶夾爪汽缸",
                new BitCell(6010, 0xD), new BitCell(6010, 0xC), new BitCell(6027, 3), new BitCell(6027, 2), "打開", "夾持"));
            AddMovableObject(new DualPositionSolenoidValve(this, "微量天秤遮風罩汽缸",
                new BitCell(6008, 0xE), new BitCell(6008, 0xF), new BitCell(6024, 0xE), new BitCell(6024, 0xF), "關閉", "打開"));
            AddMovableObject(new DualPositionSolenoidValve(this, "混合液汽缸",
                new BitCell(6008, 4), new BitCell(6008, 5), new BitCell(6024, 4), new BitCell(6024, 5), "正面", "反面"));
            AddMovableObject(new DualPositionSolenoidValve(this, "混合液夾爪汽缸",
                new BitCell(6008, 3), new BitCell(6008, 2), new BitCell(6024, 3), new BitCell(6024, 2), "打開", "夾持"));
            AddMovableObject(new DualPositionSolenoidValve(this, "60ml開蓋左右移載汽缸",
                new BitCell(6008, 0xC), new BitCell(6008, 0xD), new BitCell(6024, 0xC), new BitCell(6024, 0xD), "往左", "往右"));
            AddMovableObject(new DualPositionSolenoidValve(this, "60ml開蓋上下移載汽缸",
                new BitCell(6008, 0xA), new BitCell(6008, 0xB), new BitCell(6024, 0xA), new BitCell(6024, 0xB), "上升", "下降"));
            AddMovableObject(new DualPositionSolenoidValve(this, "60ml開蓋上夾爪汽缸",
                new BitCell(6008, 7), new BitCell(6008, 6), new BitCell(6024, 7), new BitCell(6024, 6), "打開", "夾持"));
            AddMovableObject(new NonSensorDualPositionSolenoidValve(this, "60ml開蓋下夾爪汽缸", new BitCell(6024, 9), new BitCell(6024, 8), "打開", "夾持"));
            AddMovableObject(new DualPositionSolenoidValve(this, "吸蓋上下汽缸",
                new BitCell(6009, 0xC), new BitCell(6009, 0xD), new BitCell(6025, 0xC), new BitCell(6025, 0xD), "上升", "下降"));
            AddMovableObject(new DualPositionSolenoidValve(this, "10ml夾爪本體汽缸",
                new BitCell(6009, 9), new BitCell(6009, 8), new BitCell(6025, 9), new BitCell(6025, 8), "打開", "夾持"));
            AddMovableObject(new DualPositionSolenoidValve(this, "10ml夾軸芯汽缸",
                new BitCell(6009, 0xB), new BitCell(6009, 0xA), new BitCell(6025, 0xB), new BitCell(6025, 0xA), "打開", "夾持"));
            AddMovableObject(new DualPositionSolenoidValve(this, "10ml接漏液汽缸",
                new BitCell(6009, 7), new BitCell(6009, 6), new BitCell(6025, 7), new BitCell(6025, 6), "縮回", "伸出"));
            AddMovableObject(new DualPositionSolenoidValve(this, "1ml接漏液汽缸",
                new BitCell(6009, 5), new BitCell(6009, 4), new BitCell(6025, 5), new BitCell(6025, 4), "縮回", "伸出"));
            AddMovableObject(new DualPositionSolenoidValve(this, "60ml上下移載汽缸",
                new BitCell(6009, 0), new BitCell(6009, 1), new BitCell(6025, 0), new BitCell(6025, 1), "上升", "下降"));
            AddMovableObject(new DualPositionSolenoidValve(this, "60ml配藥移載夾爪汽缸",
                new BitCell(6009, 3), new BitCell(6009, 2), new BitCell(6025, 3), new BitCell(6025, 2), "打開", "夾持"));

            //============================
            // BOM3
            //============================

            //輸送帶的推推汽缸
            AddMovableObject(new DualPositionSolenoidValve(this, "限位推瓶汽缸",
                new BitCell(6014, 0xC), new BitCell(6014, 0xD), new BitCell(6029, 0xE), "限位", "推瓶"));
            AddMovableObject(new DualPositionSolenoidValve(this, "擋瓶汽缸2",
                new BitCell(6014, 0xB), new BitCell(6014, 0xA), new BitCell(6029, 9), new BitCell(6029, 8), "退回", "伸出"));
            AddMovableObject(new DualPositionSolenoidValve(this, "擋瓶汽缸1",
                new BitCell(6014, 9), new BitCell(6014, 8), new BitCell(6029, 7), new BitCell(6029, 6), "退回", "伸出"));
            AddMovableObject(new DualPositionSolenoidValve(this, "推樣品瓶汽缸",
                new BitCell(6014, 7), new BitCell(6014, 6), new BitCell(6029, 5), new BitCell(6029, 4), "縮回", "推瓶"));
            AddMovableObject(new DualPositionSolenoidValve(this, "進料旋轉汽缸",
                new BitCell(6014, 0), new BitCell(6014, 1), new BitCell(6028, 0xE), new BitCell(6028, 0xF), "秤重", "掃碼"));
            AddMovableObject(new DualPositionSolenoidValve(this, "進料上下汽缸",
                new BitCell(6014, 2), new BitCell(6014, 3), new BitCell(6029, 0), new BitCell(6029, 1), "上升", "下降"));
            AddMovableObject(new DualPositionSolenoidValve(this, "進料夾爪汽缸",
                new BitCell(6014, 5), new BitCell(6014, 4), new BitCell(6029, 3), new BitCell(6029, 2), "打開", "夾持"));
            AddMovableObject(new DualPositionSolenoidValve(this, "倒廢液汽缸",
                new BitCell(6013, 0xE), new BitCell(6013, 0xF), new BitCell(6028, 0xC), new BitCell(6028, 0xD), "正面", "反面"));
            AddMovableObject(new DualPositionSolenoidValve(this, "倒廢液夾爪汽缸",
                new BitCell(6013, 0xD), new BitCell(6013, 0xC), new BitCell(6028, 0xB), new BitCell(6028, 0xA), "打開", "夾持"));
            AddMovableObject(new DualPositionSolenoidValve(this, "推瓶回收區汽缸",
                new BitCell(6013, 7), new BitCell(6013, 6), new BitCell(6028, 5), new BitCell(6028, 4), "縮回", "推瓶"));
            AddMovableObject(new DualPositionSolenoidValve(this, "推瓶待處理區汽缸",
                new BitCell(6013, 3), new BitCell(6013, 2), new BitCell(6028, 1), new BitCell(6028, 0), "縮回", "推瓶"));
            AddMovableObject(new DualPositionSolenoidValve(this, "推瓶NG區汽缸",
                new BitCell(6013, 5), new BitCell(6013, 4), new BitCell(6028, 3), new BitCell(6028, 2), "縮回", "推瓶"));
            AddMovableObject(new DualPositionSolenoidValve(this, "上下移載汽缸",
                new BitCell(6013, 8), new BitCell(6013, 9), new BitCell(6028, 6), new BitCell(6028, 7), "上升", "下降"));
            AddMovableObject(new DualPositionSolenoidValve(this, "移載夾爪汽缸",
                new BitCell(6013, 0xB), new BitCell(6013, 0xA), new BitCell(6028, 9), new BitCell(6028, 8), "打開", "夾持"));

            AddMovableObject(new HollowMotor());
            AddMovableObject(new UncoverMotor());

            ConstructPeripheral();
        }

        private void Demolish()
        {

        }

        private void ConstructPeripheral()
        {

        }
    }
}
