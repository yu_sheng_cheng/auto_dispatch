﻿using Automation.Core.Controller;
using Automation.Core.Registers;
using Automation.Objects.Movable.StepperMotors.Festo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.MovableObjects
{
    class UncoverMotor : FestoIOStepperMotor
    {
        public UncoverMotor() : base("開蓋馬達", new BitCell(6000, 7), new BitCell(6000, 4), new BitCell(6000, 5),
            new BitCell(6020, 6), new BitCell(6020, 1), new BitCell(6020, 4), new BitCell(6020, 5)) { }

        protected override short CheckIfScriptIsRunning(SupervisoryController controller)
        {
            IORegister register = controller.IORegister;

            if (register.GetCoil(new BitCell(6020, 2)))
                return 1;

            if (register.GetCoil(new BitCell(6020, 3)))
                return 2;

            return 0;
        }

        protected override short GetScriptId(string name)
        {
            if (name == "開蓋")
                return 1;

            if (name == "關蓋")
                return 2;

            return -1;
        }

        protected override void Run(SupervisoryController controller, short id)
        {
            IORegister register = controller.IORegister;

            switch (id)
            {
                case 0:
                    register.SetCoil(new BitCell(6020, 2), false);
                    register.SetCoil(new BitCell(6020, 3), false);
                    break;
                case 1:
                    register.SetCoil(new BitCell(6020, 2), true);
                    register.SetCoil(new BitCell(6020, 3), false);
                    break;
                case 2:
                    register.SetCoil(new BitCell(6020, 2), false);
                    register.SetCoil(new BitCell(6020, 3), true);
                    break;
                default:
                    throw new NotImplementedException();
            }
        }

        protected override void Stop(SupervisoryController controller)
        {
            IORegister register = controller.IORegister;

            register.SetCoil(new BitCell(6020, 2), false);
            register.SetCoil(new BitCell(6020, 3), false);
        }
    }
}
