﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action18 : DecompositionAction
        {
            private readonly InputInspection _vacuumGaugesInputInspection;

            public Action18(ContinuousActionDirector director) : base(director)
            {
                _vacuumGaugesInputInspection = new InputInspection("中空馬達中空檢知", 500, 30,
                    (controller) => { return controller.IORegister.GetContact(AutomatedDispensingController.HollowMotorVacuumGaugesInput); });
            }

            public override bool Go(SupervisoryController controller)
            {
                controller.IORegister.SetCoil(AutomatedDispensingController.HollowMotorSuctionOutput, true);
                AddInputInspection(_vacuumGaugesInputInspection, OnHasBottle);
                return true;
            }

            private void OnHasBottle(SupervisoryController controller, INamedObject inputInspection, CompletionResult result)
            {
                Console.WriteLine("中空馬達中空檢知: {0}", result == CompletionResult.Success ? "有瓶" : "無瓶");

                SignalAndWait(controller, "中空馬達中空檢知", result, (controllerArg) => Go(controllerArg, "噴碼夾爪汽缸", "打開", OnInkjetGripperCylinderOpenCompleted));
            }

            private void OnInkjetGripperCylinderOpenCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼夾爪汽缸 <打開> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "噴碼前後移載汽缸", "後退", OnInkjetTransferCylinderBackwardCompleted));
            }

            private void OnInkjetTransferCylinderBackwardCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼前後移載汽缸 <後退> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action19(Director);
            }
        }
    }
}
