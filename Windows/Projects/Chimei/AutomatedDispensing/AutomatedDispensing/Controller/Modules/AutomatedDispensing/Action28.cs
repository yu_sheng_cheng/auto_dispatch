﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action28 : DecompositionAction
        {
            public Action28(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "ARM4 Z 1.5ml夾取手臂", "1.5ml放蓋", ARM4_Z_小玻璃瓶夾取手臂已到放蓋位置);
            }

            private void ARM4_Z_小玻璃瓶夾取手臂已到放蓋位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM4 Z 1.5ml夾取手臂 <1.5ml放蓋> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "1.5ml夾瓶汽缸", "打開", 小玻璃瓶夾瓶汽缸已打開));
            }

            private void 小玻璃瓶夾瓶汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("1.5ml夾瓶汽缸 <打開> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "ARM4 Z 1.5ml夾取手臂", "Standby", ARM4_Z_小玻璃瓶夾取手臂已到Standby位置));
            }

            private void ARM4_Z_小玻璃瓶夾取手臂已到Standby位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM4 Z 1.5ml夾取手臂 <Standby> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "空瓶移載手臂", "Standby", 空瓶移載手臂已到Standby位置));
            }

            private void 空瓶移載手臂已到Standby位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("空瓶移載手臂 <Standby> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action40(Director);
            }
        }
    }
}
