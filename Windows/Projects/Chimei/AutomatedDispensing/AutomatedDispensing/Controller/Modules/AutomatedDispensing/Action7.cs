﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action7 : DecompositionAction
        {
            public Action7(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "進料上下汽缸", "下降", OnFeedCylinderDownCompleted);
            }

            private void OnFeedCylinderDownCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("進料上下汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "進料夾爪汽缸", "打開", OnFeedGripperCylinderOpenCompleted));
            }

            private void OnFeedGripperCylinderOpenCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("進料夾爪汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "進料上下汽缸", "上升", OnFeedCylinderUpCompleted));
            }

            private void OnFeedCylinderUpCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("進料上下汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "進料旋轉汽缸", "秤重", OnFeedSpinCylinderWeighingAreaArrivaled));
            }

            private void OnFeedSpinCylinderWeighingAreaArrivaled(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("進料旋轉汽缸 <秤重> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action8(Director);
            }
        }
    }
}
