﻿using Automation.Core.FSM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomatedDispensing.Controller.Modules.context;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    sealed partial class AutomatedDispensingDirector : ContinuousActionDirector, IIsAutoRun
    {
        private AutoRunContext _autoRunContext;
        public AutomatedDispensingDirector() : base("自動分注")
        {

        }

        public void SetGetAutoRunContext(AutoRunContext a)
        {
            _autoRunContext = a;
        }

        protected override DecompositionAction CreateFirstAction()
        {
            return new Action1(this);
        }

        public AutoRunContext GetAutoRunContext()
        {
            return this._autoRunContext;
        }
    }
}
