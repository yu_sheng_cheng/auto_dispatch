﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action14 : DecompositionAction
        {
            private int _originalAngle;
            private bool _uncoverCompleted;

            public Action14(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController ctrl)
            {
                AutomatedDispensingController controller = ctrl as AutomatedDispensingController;

                _originalAngle = controller.ARMs[8].IOMemory.CurrentFeedValue;

                return Go(controller, "ARM9 Θ 樣品瓶開蓋手臂", "JOGF", OnSampleBottleUncoverARMStopped);
            }

            public override DecompositionAction Poll(SupervisoryController ctrl)
            {
                AutomatedDispensingController controller = ctrl as AutomatedDispensingController;

                if (!_uncoverCompleted)
                {
                    int angle = controller.ARMs[8].IOMemory.CurrentFeedValue;

                    if (angle > (_originalAngle + 430)) //1080
                    {
                        _uncoverCompleted = true;
                        controller.ARMs[8].StopJog(ctrl);
                    }
                }

                return base.Poll(controller);
            }

            private void OnSampleBottleUncoverARMStopped(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM9 Θ 樣品瓶開蓋手臂 <Stopped> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "樣品瓶開蓋上下汽缸", "上升", OnSampleBottleUncoverCylinderUpCompleted));
            }

            private void OnSampleBottleUncoverCylinderUpCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("樣品瓶開蓋上下汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "樣品瓶身體夾爪汽缸", "打開", OnSampleBottleBodyGripperCylinderOpenCompleted));
            }

            private void OnSampleBottleBodyGripperCylinderOpenCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("樣品瓶身體夾爪汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "ARM18 X 送料手臂", "分注", OnFeedingTransferDispensingAreaArrived));
            }

            private void OnFeedingTransferDispensingAreaArrived(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM18 X 送料手臂 <分注> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action15(Director);
            }
        }
    }
}
