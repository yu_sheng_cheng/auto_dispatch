﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Enums;
using Automation.Helper;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing.parallel.conveyor
{
    internal partial class ConveyorDirector
    {
        private class FinalActionImpl : ConveyorAction, IFinalAction
        {
            private readonly OnceTimer _myTimer = new OnceTimer();

            public FinalActionImpl(ConveyorDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                //輸送帶馬達線stop
                controller.IORegister.SetCoil(AutomatedDispensingController.FeedingMotorOutput, false);
                Log("關閉入料馬達(try)");
                AddTimer(controller, _myTimer, 100, OnFeedingMotorStopped);
                return true;
            }

            private void OnFeedingMotorStopped(SupervisoryController controller)
            {
                Log(@"入料馬達 <停止>");
                SignalAndWait(controller, "入料馬達", CompletionResult.Success);
            }


            protected override DecompositionAction CreateConveyorAction()
            {
                return null;
            }
        }
    }

    internal interface IFinalAction
    {
    }
}