﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action8 : DecompositionAction
        {
            public Action8(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "ARM18 X 送料手臂", "放樣品瓶", OnFeedingTransferSampleBottleAreaArrived))
                    return false;

                Go(controller, "上下移載汽缸", "上升", OnTransferCylinderUpCompleted);
                return true;
            }

            private void OnFeedingTransferSampleBottleAreaArrived(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM18 X 送料手臂 <放樣品瓶> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void OnTransferCylinderUpCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("上下移載汽缸 <上升> : {0}", result.ToString());

                if (result == CompletionResult.Success)
                    Go(controller, "取瓶移載手臂", "掃碼", OnBottlePickupTransferBarcodeScaningAreaArrived);

                SignalAndWait(controller, movableObject.Name, result);
            }

            private void OnBottlePickupTransferBarcodeScaningAreaArrived(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("取瓶移載手臂 <掃碼> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action9(Director);
            }
        }
    }
}
