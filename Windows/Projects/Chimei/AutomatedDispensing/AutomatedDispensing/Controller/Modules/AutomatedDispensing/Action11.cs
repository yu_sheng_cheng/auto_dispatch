﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action11 : DecompositionAction
        {
            public Action11(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "取瓶移載手臂", "Standby", OnBottlePickupTransferStandbyPositionArrived))
                    return false;

                Go(controller, "ARM18 X 送料手臂", "開蓋", OnFeedingTransferUncoverAreaArrived);
                return true;
            }

            private void OnBottlePickupTransferStandbyPositionArrived(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("取瓶移載手臂 <Standby> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            private void OnFeedingTransferUncoverAreaArrived(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM18 X 送料手臂 <開蓋> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action12(Director);
            }
        }
    }
}
