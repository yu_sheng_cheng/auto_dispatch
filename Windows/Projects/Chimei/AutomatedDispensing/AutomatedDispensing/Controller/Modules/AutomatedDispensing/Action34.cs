﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action34 : DecompositionAction
        {
            public Action34(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "ARM3 X 送料手臂", "1.5m放料移載 [1]", ARM3_X_送料手臂已到放料位置))
                    return false;

                Go(controller, "空瓶移載手臂", "1.5ml放蓋 [1]", 空瓶移載手臂已到放蓋位置);
                return true;
            }

            private void ARM3_X_送料手臂已到放料位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM3 X 送料手臂 <1.5m放料移載 [1]> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void 空瓶移載手臂已到放蓋位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("空瓶移載手臂 <1.5ml放蓋 [1]> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action35(Director);
            }
        }
    }
}
