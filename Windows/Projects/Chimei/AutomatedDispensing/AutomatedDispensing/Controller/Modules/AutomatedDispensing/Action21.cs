﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action21 : DecompositionAction
        {
            public Action21(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "噴碼前後移載汽缸", "前進", OnInkjetTransferCylinderForwardCompleted);
            }

            private void OnInkjetTransferCylinderForwardCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼前後移載汽缸 <前進> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "噴碼夾爪汽缸", "夾持", OnInkjetGripperCylinderHoldCompleted));
            }

            private void OnInkjetGripperCylinderHoldCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼夾爪汽缸 <夾持> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action22(Director);
            }
        }
    }
}
