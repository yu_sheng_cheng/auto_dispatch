﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action10 : DecompositionAction
        {
            public Action10(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "上下移載汽缸", "下降", OnTransferCylinderDownCompleted);
            }

            private void OnTransferCylinderDownCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("上下移載汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "移載夾爪汽缸", "打開", OnTransferGripperCylinderOpenCompleted));
            }

            private void OnTransferGripperCylinderOpenCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("移載夾爪汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "上下移載汽缸", "上升", OnTransferCylinderUpCompleted));
            }

            private void OnTransferCylinderUpCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("上下移載汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action11(Director);
            }
        }
    }
}
