﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action23 : DecompositionAction
        {
            public Action23(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "噴碼上下移載汽缸", "下降", OnInkjetTransferCylinderDownCompleted))
                    return false;

                Go(controller, "噴碼左右移載汽缸", "往左", OnInkjetTransferCylinderLeftCompleted);
                return true;
            }

            private void OnInkjetTransferCylinderDownCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼上下移載汽缸 <下降> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            private void OnInkjetTransferCylinderLeftCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼左右移載汽缸 <往左> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action24(Director);
            }
        }
    }
}
