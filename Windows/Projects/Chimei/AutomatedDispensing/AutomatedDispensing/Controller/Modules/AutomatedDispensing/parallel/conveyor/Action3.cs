﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing.parallel.conveyor
{
    internal partial class ConveyorDirector
    {
        sealed class Action3 : ConveyorAction
        {
            private readonly InputInspection _feedingWaitBottleInputInspection;

            private readonly OnceTimer _myTimer = new OnceTimer();

            private bool _hasButton = false;

            internal Action3(ConveyorDirector c) : base(c)
            {
                // 入料等瓶位置2 => 在出發前的小洞洞
                _feedingWaitBottleInputInspection = new InputInspection(
                    "入料等瓶位置2", 3000, 30,
                    (controller) => controller.IORegister.GetContact(AutomatedDispensingController
                        .FeedingWaitBottlePosition2Input));
            }

            public override bool Go(SupervisoryController controller)
            {
                try
                {
                    Log("擋瓶汽缸2, 退回 (try)");
                    if (Go(controller, "擋瓶汽缸2", "退回", On入料等瓶位置1Back))
                    {
                        return true;
                    }
                    else
                    {
                        Log("入料等瓶位置2, 退回 (Go failed)");
                        return false;
                    }
                }
                catch (Exception e)
                {
                    Log("failed", e.StackTrace);
                    throw;
                }
            }

            private void On入料等瓶位置1Back(SupervisoryController controller,
                INamedObject movableObject, CompletionResult result)
            {
                try
                {
                    LogAction(result, movableObject, "退回");
                    SignalAndWait(controller, movableObject.Name, result);
                    //因為之後會有汽缸伸出的動作, 所以在汽缸縮回成功在做,避免下指令library因為指令沒有回傳,將新的指令拋棄
                    AddInputInspection(_feedingWaitBottleInputInspection, OnBottleArrived);
                }
                catch (Exception e)
                {
                    Log("failed", e);
                    throw;
                }
            }

            private void OnBottleArrived(SupervisoryController controller, INamedObject inputInspection,
                CompletionResult result)
            {
                try
                {
                    Log("入料等瓶位置2: {0}", result == CompletionResult.Success ? "有瓶" : "無瓶");
                    //入料準備區域有卡瓶子
                    if (result == CompletionResult.Success)
                    {
                        _hasButton = true;
                        Log(Go(controller, "擋瓶汽缸2", "伸出", On擋瓶汽缸2伸出)
                            ? "擋瓶汽缸2<伸出>(try)"
                            : "擋瓶汽缸2<伸出>(unableTry)");
                    }
                    else
                    {
                        _hasButton = false;

                        var isDone = false;

                        //限位推瓶汽缸在設計上是一種可以被覆蓋的指令的設備
                        //但是如果限位推瓶汽缸(推瓶)的Go被覆蓋
                        //會因為barrier已經被加一,沒有減去,因為他被覆改使得無法呼叫完成callback
                        //如此情況導致程序沒法繼續走,所以要判斷callback受否有完成(isDone)
                        //如果沒有在主動移除一個barrier
                        Go(controller, "限位推瓶汽缸", "推瓶",
                            (a, b, c) =>
                            {
                                isDone = true;
                                On推瓶推瓶汽缸Callback(a, b, c);
                            });
                        AddTimer(controller, _myTimer, 500,
                            (a) =>
                            {
                                if (!isDone)
                                {
                                    Log("主動移除限位推瓶汽缸(推瓶)的Participants");
                                    RemoveParticipants(1);
                                }
                                Start限位(a);
                            });
                        CloseConveyerIfAutoRunCompleted(controller);
                    }

                    SignalAndWait(controller, inputInspection.Name, CompletionResult.Success);
                }
                catch (Exception e)
                {
                    Log("failed", e);
                    throw;
                }
            }

            private void Start限位(SupervisoryController controller)
            {
                var device = "限位推瓶汽缸";
                Go(controller, device, "限位", On限位推瓶汽缸Callback);
                SignalAndWait(controller, device, CompletionResult.Success);
            }

            private void On擋瓶汽缸2伸出(SupervisoryController controller, INamedObject inputInspection,
                CompletionResult arg3)
            {
                LogAction(arg3, inputInspection, "伸出");
                Log(Go(controller, "擋瓶汽缸1", "退回", On擋瓶汽缸1退回)
                    ? "擋瓶汽缸1<退回>(try)"
                    : "擋瓶汽缸1<退回>(unableTry)");
                SignalAndWait(controller, inputInspection.Name, arg3);
            }

            private void On擋瓶汽缸1退回(SupervisoryController controller, INamedObject inputInspection,
                CompletionResult arg3)
            {
                LogAction(arg3, inputInspection, "退回");
                SignalAndWait(controller, inputInspection.Name, arg3);
            }

            private void On推瓶推瓶汽缸Callback(SupervisoryController controller,
                INamedObject movableObject, CompletionResult result)
            {
                LogAction(result, movableObject, "推瓶");
                SignalAndWait(controller, movableObject.Name, CompletionResult.Success);
            }


            private void On限位推瓶汽缸Callback(SupervisoryController controller,
                INamedObject movableObject, CompletionResult result)
            {
                LogAction(result, movableObject, "限位");
                SignalAndWait(controller, movableObject.Name, result);
            }


            private void OnFeedingMotorStopped(SupervisoryController controller)
            {
                Log("入料馬達 <停止>");
                SignalAndWait(controller, "入料馬達", CompletionResult.Success);
            }

            protected override DecompositionAction CreateConveyorAction()
            {
                if (_hasButton)
                {
                    return new Action31(ConveyorDirector);
                }
                else
                {
                    return new Action3(ConveyorDirector);
                }
            }
        }
    }
}