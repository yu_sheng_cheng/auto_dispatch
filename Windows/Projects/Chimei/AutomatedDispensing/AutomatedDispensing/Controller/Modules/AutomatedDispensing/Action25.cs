﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action25 : DecompositionAction
        {
            public Action25(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "噴碼翻轉汽缸", "垂直", OnInkjetFlipCylinderVCompleted))
                    return false;

                Go(controller, "空瓶移載手臂", "1.5ml抱瓶", OnEmptyBottleTransferAreaArrived);
                Go(controller, "ARM3 X 送料手臂", "1.5m放料移載 [1]", OnARM3AreaArrived);
                return true;
            }

            private void OnInkjetFlipCylinderVCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼翻轉汽缸 <垂直> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            private void OnEmptyBottleTransferAreaArrived(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("空瓶移載手臂 <1.5ml抱瓶> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            private void OnARM3AreaArrived(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM3 X 送料手臂 <1.5m放料移載 [1]> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action26(Director);
            }
        }
    }
}
