﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action22 : DecompositionAction
        {
            private readonly InputInspection _vacuumGaugesInputInspection;

            public Action22(ContinuousActionDirector director) : base(director)
            {
                _vacuumGaugesInputInspection = new InputInspection("中空馬達破瓶真空", 500, 30,
                    (controller) => { return !controller.IORegister.GetContact(AutomatedDispensingController.HollowMotorVacuumGaugesInput); });
            }

            public override bool Go(SupervisoryController controller)
            {
                controller.IORegister.SetCoil(AutomatedDispensingController.HollowMotorSuctionOutput, false);
                AddInputInspection(_vacuumGaugesInputInspection, OnCompleted);
                return true;
            }

            private void OnCompleted(SupervisoryController controller, INamedObject inputInspection, CompletionResult result)
            {
                Console.WriteLine("中空馬達破瓶真空: {0}", result == CompletionResult.Success ? "成功" : "失敗");

                SignalAndWait(controller, "中空馬達破瓶真空", result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action23(Director);
            }
        }
    }
}
