﻿using AutomatedDispensing.Controller.MovableObjects;
using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;
using Automation.Objects.Movable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action19 : DecompositionAction
        {
            private readonly InputInspection _keyenceIVGInputInspection;

            public Action19(ContinuousActionDirector director) : base(director)
            {
                _keyenceIVGInputInspection = new InputInspection("Keyence IVG檢知", 30000, 0,
                    (controller) => { return controller.IORegister.GetContact(AutomatedDispensingController.KeyenceIVGOkInput); });
            }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "中空馬達", "Jog慢速", OnHollowMotorStopped))
                    return false;

                AddInputInspection(_keyenceIVGInputInspection, OnPositioningCompleted);
                return true;
            }

            private void OnPositioningCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("Keyence IVG檢知: {0}", result == CompletionResult.Success ? "定位成功" : "定位失敗");

                controller.TryGetMovableObject("中空馬達", out IMovableObject obj);
                (obj as HollowMotor).StopJog(controller);

                SignalAndWait(controller, movableObject.Name, result);
            }

            private void OnHollowMotorStopped(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("中空馬達 <停止> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action20(Director);
            }
        }

    }
}
