﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action33 : DecompositionAction
        {
            public Action33(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "上下移載汽缸", "下降", 上下移載汽缸下降完成);
            }

            private void 上下移載汽缸下降完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("上下移載汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "移載夾爪汽缸", "打開", 移載夾爪汽缸已打開));
            }

            private void 移載夾爪汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("移載夾爪汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "上下移載汽缸", "上升", 上下移載汽缸上升完成));
            }

            private void 上下移載汽缸上升完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("上下移載汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "取瓶移載手臂", "Standby", 取瓶移載手臂已到待機位置));
            }

            private void 取瓶移載手臂已到待機位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("取瓶移載手臂 <Standby> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action34(Director);
            }
        }
    }
}
