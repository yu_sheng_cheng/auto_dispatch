﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action29 : DecompositionAction
        {
            public Action29(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "ARM18 X 送料手臂", "開蓋", ARM18_X_送料手臂已到開蓋位置);
            }

            private void ARM18_X_送料手臂已到開蓋位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM18 X 送料手臂 <開蓋> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "樣品瓶身體夾爪汽缸", "夾持", 樣品瓶身體夾爪汽缸已夾持));
            }

            private void 樣品瓶身體夾爪汽缸已夾持(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("樣品瓶身體夾爪汽缸 <夾持> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "樣品瓶開蓋上下汽缸", "下降", 樣品瓶開蓋上下汽缸下降完成));
            }

            private void 樣品瓶開蓋上下汽缸下降完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("樣品瓶開蓋上下汽缸 <下降> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action30(Director);
            }
        }
    }
}
