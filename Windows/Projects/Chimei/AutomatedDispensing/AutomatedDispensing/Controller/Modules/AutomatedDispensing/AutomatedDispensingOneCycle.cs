﻿using Automation.Core.Controller;
using Automation.Core.Messages.ManualControl.OneCycle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomatedDispensing.Controller.Modules.context;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    class AutomatedDispensingOneCycle : OneCycleManualControlMessage
    {
        private readonly AutomatedDispensingDirector _director = new AutomatedDispensingDirector();

        public override bool IsCompleted { get { return _director.IsCompleted; } }

        public AutomatedDispensingOneCycle(AutoRunContext autoRunContext) : base("自動分注動作一次")
        {
            _director.SetGetAutoRunContext(autoRunContext);
        }

        public AutomatedDispensingOneCycle() : base("自動分注動作一次")
        {

        }

        public override bool Go(SupervisoryController controller)
        {
            if (!_director.Go(controller))
            {
                Complete(_director.Result);
                return false;
            }
            return true;
        }

        public override void Poll(SupervisoryController controller)
        {
            _director.Poll(controller);

            if (_director.IsCompleted)
                Complete(_director.Result);
        }
    }
}
