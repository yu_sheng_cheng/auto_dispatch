﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action4 : DecompositionAction
        {
            private readonly InputInspection _feedingWaitBottleInputInspection;
            private readonly OnceTimer _feedingMotorTimer = new OnceTimer();

            public Action4(ContinuousActionDirector director) : base(director)
            {
                _feedingWaitBottleInputInspection = new InputInspection("入料等瓶位置1", 3000, 30,
                    (controller) => { return controller.IORegister.GetContact(AutomatedDispensingController.FeedingWaitBottlePosition1Input); });
            }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "擋瓶汽缸2", "伸出", OnBottleBlockCylinder2ReachOutCompleted);
            }

            private void OnBottleBlockCylinder2ReachOutCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("擋瓶汽缸2 <伸出> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result,
                    (controllerArg) =>
                    {
                        if (Go(controllerArg, "擋瓶汽缸1", "退回", OnBottleBlockCylinder1RetractCompleted))
                        {
                            AddInputInspection(_feedingWaitBottleInputInspection, OnBottleArrivaled);
                        }
                    });
            }

            private void OnBottleBlockCylinder1RetractCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("擋瓶汽缸1 <退回> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void OnBottleArrivaled(SupervisoryController controller, INamedObject inputInspection, CompletionResult result)
            {
                Console.WriteLine("入料等瓶位置1: {0}", result == CompletionResult.Success ? "有瓶" : "無瓶");

                controller.IORegister.SetCoil(AutomatedDispensingController.FeedingMotorOutput, false);
                AddTimer(controller, _feedingMotorTimer, 100, OnFeedingMotorStopped);

                SignalAndWait(controller, inputInspection.Name, result);
            }

            private void OnFeedingMotorStopped(SupervisoryController controller)
            {
                Console.WriteLine("入料馬達 <停止>");
                SignalAndWait(controller, "入料馬達", CompletionResult.Success);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action5(Director);
            }
        }
    }
}
