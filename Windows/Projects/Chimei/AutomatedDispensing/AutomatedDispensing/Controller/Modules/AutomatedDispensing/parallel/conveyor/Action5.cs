﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomatedDispensing.Controller.Modules.BOM2.Ml1Dispatch;
using Automation.Core.Controller;
using Automation.Core.FSM;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing.parallel.conveyor
{
    internal partial class ConveyorDirector
    {
        class Action5 : ConveyorAction
        {
            private bool _hasBottle;

            public Action5(ConveyorDirector director, bool hasBottle) : base(director)
            {
                _hasBottle = hasBottle;
            }

            public override bool Go(SupervisoryController controller)
            {
                AddParticipants(1);
                return true;
            }


            public override DecompositionAction Poll(SupervisoryController controller)
            {
                if (CheckAutoRunCompleted())
                {
                    RemoveParticipants(1);
                }
                else
                {
                    if (_hasBottle)
                    {
                        if (!controller.IORegister.GetContact(AutomatedDispensingController
                            .FeedingWaitBottlePosition1Input))
                        {
                            Log("入料等瓶位置1: 無瓶");
                            _hasBottle = false;
                            RemoveParticipants(1);
                        }
                    }
                    else
                    {
                        _hasBottle = true;
                        Log("入料等瓶位置1: 有瓶");
                    }
                }
                return base.Poll(controller);
            }

            /// <summary>
            ///  入料等瓶位置2: 無瓶時候, 要送另外一個新瓶子到入料區
            /// </summary>
            /// <returns></returns>
            protected override DecompositionAction CreateConveyorAction()
            {
                return new Action2(this.ConveyorDirector);
            }
        }
    }
}