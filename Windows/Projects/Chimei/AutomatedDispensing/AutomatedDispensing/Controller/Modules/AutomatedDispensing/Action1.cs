﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomatedDispensing.Controller.Modules.context;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action1 : DecompositionAction
        {
            private readonly OnceTimer _feedingMotorTimer = new OnceTimer();

            public Action1(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "擋瓶汽缸1", "伸出", OnBottleBlockCylinder1ReachOutCompleted))
                    return false;

                controller.IORegister.SetCoil(AutomatedDispensingController.FeedingMotorOutput, false);
                AddTimer(controller, _feedingMotorTimer, 100, OnFeedingMotorStopped);
                return true;
            }

            private void OnFeedingMotorStopped(SupervisoryController controller)
            {
                Console.WriteLine("入料馬達 <停止>");
                SignalAndWait(controller, "入料馬達", CompletionResult.Success);
            }

            private void OnBottleBlockCylinder1ReachOutCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("擋瓶汽缸1 <伸出> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action2(Director);
            }
        }
    }
}
