﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing.parallel.conveyor
{
    internal partial class ConveyorDirector
    {
        private sealed class Action2 : ConveyorAction
        {
            private readonly OnceTimer _feedingMotorTimer = new OnceTimer();

            public Action2(ConveyorDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "擋瓶汽缸2", "伸出", OnBottleBlockCylinder2RetractCompleted))
                    return false;

                controller.IORegister.SetCoil(AutomatedDispensingController.FeedingMotorOutput, true);
                AddTimer(controller, _feedingMotorTimer, 100, OnFeedingMotorStarted);
                return true;
            }

            private void OnFeedingMotorStarted(SupervisoryController controller)
            {
                Log("入料馬達 <運轉>");
                CloseConveyerIfAutoRunCompleted(controller);
                SignalAndWait(controller, "入料馬達", CompletionResult.Success);

            }

            private void OnBottleBlockCylinder2RetractCompleted(SupervisoryController controller,
                INamedObject movableObject, CompletionResult result)
            {
                Log("擋瓶汽缸2 <伸出> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateConveyorAction()
            {
                return new Action3(this.ConveyorDirector);
            }
        }
    }
}