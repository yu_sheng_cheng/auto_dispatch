﻿using System;
using AutomatedDispensing.Controller.Modules.context;
using Automation.Core.FSM;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing.parallel.conveyor
{
    /**
     * 當one/n cycle持續進行時, 輸送帶會把瓶裝水,持續往前送, 因此,他和one/n cycle是併行執行的
     */
    internal partial class ConveyorDirector : ContinuousActionDirector, IIsAutoRun
    {
        public readonly AutoRunContext AutoRunContext;

        public Func<DecompositionAction> FinalAction { get; set; }

        public ConveyorDirector(AutoRunContext a) : base("輸送帶持續分配Director")
        {
            this.AutoRunContext = a;
            this.FinalAction = () => new FinalActionImpl(this);
        }

        protected override DecompositionAction CreateFirstAction()
        {
            return new Action1(this);
        }

        public AutoRunContext GetAutoRunContext()
        {
            return AutoRunContext;
        }
    }
}