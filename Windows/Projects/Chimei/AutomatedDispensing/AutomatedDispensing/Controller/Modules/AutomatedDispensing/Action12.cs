﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action12 : DecompositionAction
        {
            public Action12(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "樣品瓶身體夾爪汽缸", "夾持", OnSampleBottleBodyGripperCylinderHoldCompleted))
                    return false;

                Go(controller, "樣品瓶蓋子夾爪汽缸", "打開", OnSampleBottleCoverGripperCylinderOpenCompleted);
                return true;
            }

            private void OnSampleBottleBodyGripperCylinderHoldCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("樣品瓶身體夾爪汽缸 <夾持> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void OnSampleBottleCoverGripperCylinderOpenCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("樣品瓶蓋子夾爪汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }


            protected override DecompositionAction CreateNextAction()
            {
                return new Action13(Director);
            }
        }
    }
}
