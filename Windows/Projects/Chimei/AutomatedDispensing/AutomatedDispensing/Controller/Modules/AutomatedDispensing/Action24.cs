﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action24 : DecompositionAction
        {
            public Action24(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "噴碼前後移載汽缸", "後退", OnInkjetTransferCylinderBackwardCompleted);
            }

            private void OnInkjetTransferCylinderBackwardCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼前後移載汽缸 <後退> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "噴碼翻轉汽缸", "水平", OnInkjetFlipCylinderHCompleted));
            }

            private void OnInkjetFlipCylinderHCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼翻轉汽缸 <水平> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "ARM4 Z 1.5ml夾取手臂", "噴碼夾持", OnARM4InkjetHoldCompleted));
            }

            private void OnARM4InkjetHoldCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM4 Z 1.5ml夾取手臂 <噴碼夾持> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "1.5ml夾瓶汽缸", "夾持", On15mlBottleGripperHoldCompleted));
            }

            private void On15mlBottleGripperHoldCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("1.5ml夾瓶汽缸 <夾持> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "噴碼夾爪汽缸", "打開", OnInkjetGripperOpenCompleted));
            }

            private void OnInkjetGripperOpenCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼夾爪汽缸 <打開> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "ARM4 Z 1.5ml夾取手臂", "Standby", OnARM4StandbyCompleted));
            }

            private void OnARM4StandbyCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM4 Z 1.5ml夾取手臂 <Standby> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action25(Director);
            }
        }
    }
}
