﻿using AutomatedDispensing.Controller.MovableObjects;
using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;
using Automation.Objects.Movable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action20 : DecompositionAction
        {
            private readonly OnceTimer _timer = new OnceTimer();

            public Action20(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "中空馬達", "定位", OnHollowMotorPositioningCompleted);
            }

            private void OnHollowMotorPositioningCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("中空馬達 <定位> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) =>
                {
                    if (Go(controllerArg, "中空馬達", "Jog快速", OnHollowMotorStopped))
                    {
                        AddTimer(controller, _timer, 800, OnPrintCompleted);
                    }
                });
            }

            private void OnPrintCompleted(SupervisoryController controller)
            {
                controller.TryGetMovableObject("中空馬達", out IMovableObject obj);
                (obj as HollowMotor).StopJog(controller);

                SignalAndWait(controller, "噴碼機", CompletionResult.Success);
            }

            private void OnHollowMotorStopped(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("中空馬達 <停止> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action21(Director);
            }
        }
    }
}
