﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomatedDispensing.Controller.Modules.AutomatedDispensing.parallel.conveyor;
using AutomatedDispensing.Controller.Modules.BOM2.Ml1Dispatch;
using AutomatedDispensing.Controller.Modules.context;
using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing.parallel
{
    internal abstract class ConveyorAction : DecompositionAction
    {
        protected ConveyorAction(ConveyorDirector director) : base(director)
        {
            Log("開始=> " + this.GetType().Name);
        }

        protected ConveyorDirector ConveyorDirector => (ConveyorDirector) Director;

        protected AutoRunContext AutoRunContext => ConveyorDirector.AutoRunContext;

        protected void Log(string c)
        {
            Console.WriteLine(@"[輸送帶Director] " + @"(" + this.GetType().Name + @") " + c);
        }

        protected void Log(string format, params object[] c)
        {
            Console.WriteLine(@"[輸送帶Director] " + @"(" + this.GetType().Name + @") " + format + @" detail: {0}", c);
        }

        protected bool CheckAutoRunCompleted()
        {
            if (AutoRunContext.AutoRunCompleted)
            {
                Log(@"AutoRun已經完成,關閉輸送帶director");
                return true;
            }

            return false;
        }


        protected void CloseConveyerIfAutoRunCompleted(SupervisoryController controller)
        {
            if (AutoRunContext.AutoRunCompleted)
            {
                Log("立即關閉輸送帶");
                controller.IORegister.SetCoil(AutomatedDispensingController.FeedingMotorOutput, false);
            }
        }

        protected override DecompositionAction CreateNextAction()
        {
            try
            {
                if (!(this is IFinalAction))
                {
                    if (CheckAutoRunCompleted())
                    {
                        var c = this.ConveyorDirector.FinalAction;
                        return c?.Invoke();
                    }
                }

                return CreateConveyorAction();
            }
            catch (Exception e)
            {
                Log("failed and stop", e);
                return null;
            }
        }

        protected abstract DecompositionAction CreateConveyorAction();

        protected void LogAction(CompletionResult objectName, INamedObject movableObjectName, string action)
        {
            if (objectName == CompletionResult.Success)
            {
                Log(movableObjectName.Name + "執行[" + action + "](ok)");
            }
            else
            {
                Log(movableObjectName.Name + "執行[" + action + "](failed)");
            }
        }
    }
}