﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action15 : DecompositionAction
        {
            public Action15(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "空瓶移載手臂", "1.5ml供料Tray盤 [1]", OnEmptyBottleTransferXXXAreaArrived);
            }

            private void OnEmptyBottleTransferXXXAreaArrived(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("空瓶移載手臂 <1.5ml供料Tray盤 [1]> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "1.5ml夾瓶汽缸", "打開", On15mlBottleGripperOpenCompleted));
            }

            private void On15mlBottleGripperOpenCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("1.5ml夾瓶汽缸 <打開> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "ARM4 Z 1.5ml夾取手臂", "Tray盤(有蓋)", OnARM4XXXCompleted));
            }

            private void OnARM4XXXCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM4 Z 1.5ml夾取手臂 <Tray盤(有蓋)> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "1.5ml夾瓶汽缸", "夾持", On15mlBottleGripperHoldCompleted));
            }

            private void On15mlBottleGripperHoldCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("1.5ml夾瓶汽缸 <夾持> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "ARM4 Z 1.5ml夾取手臂", "Standby", OnARM4StandbyCompleted));
            }

            private void OnARM4StandbyCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM4 Z 1.5ml夾取手臂 <Standby> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "空瓶移載手臂", "噴碼夾持", OnEmptyBottleTransferYYYAreaArrived));
            }

            private void OnEmptyBottleTransferYYYAreaArrived(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("空瓶移載手臂 <噴碼夾持> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "噴碼翻轉汽缸", "水平", OnInkjetFlipCylinderHCompleted));
            }

            private void OnInkjetFlipCylinderHCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼翻轉汽缸 <水平> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "噴碼夾爪汽缸", "打開", OnInkjetGripperOpenCompleted));
            }

            private void OnInkjetGripperOpenCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼夾爪汽缸 <打開> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "ARM4 Z 1.5ml夾取手臂", "噴碼夾持", OnARM4InkjetHoldCompleted));
            }

            private void OnARM4InkjetHoldCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM4 Z 1.5ml夾取手臂 <噴碼夾持> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "噴碼夾爪汽缸", "夾持", OnInkjetGripperHoldCompleted));
            }

            private void OnInkjetGripperHoldCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼夾爪汽缸 <夾持> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "1.5ml夾瓶汽缸", "打開", On15mlBottleGripperOpenCompleted2));
            }

            private void On15mlBottleGripperOpenCompleted2(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("1.5ml夾瓶汽缸 <打開> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "ARM4 Z 1.5ml夾取手臂", "Standby", OnARM4StandbyCompleted2));
            }

            private void OnARM4StandbyCompleted2(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM4 Z 1.5ml夾取手臂 <Standby> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "噴碼翻轉汽缸", "垂直", OnInkjetFlipCylinderVCompleted));
            }

            private void OnInkjetFlipCylinderVCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼翻轉汽缸 <垂直> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action16(Director);
            }
        }
    }
}
