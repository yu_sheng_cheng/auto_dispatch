﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action27 : DecompositionAction
        {
            public Action27(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "空瓶移載手臂", "1.5ml放蓋 [1]", 空瓶移載手臂已到放蓋位置))
                    return false;

                Go(controller, "ARM3 X 送料手臂", "分注", ARM3_X_送料手臂已到分注位置);
                return true;
            }

            private void 空瓶移載手臂已到放蓋位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("空瓶移載手臂 <1.5ml放蓋 [1]> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            private void ARM3_X_送料手臂已到分注位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM3 X 送料手臂 <分注> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action28(Director);
            }
        }
    }
}
