﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action13 : DecompositionAction
        {
            public Action13(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "樣品瓶開蓋上下汽缸", "下降", OnSampleBottleUncoverCylinderDownCompleted);
            }

            private void OnSampleBottleUncoverCylinderDownCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("樣品瓶開蓋上下汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "樣品瓶蓋子夾爪汽缸", "夾持", OnSampleBottleCoverGripperCylinderHoldCompleted));
            }

            private void OnSampleBottleCoverGripperCylinderHoldCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("樣品瓶蓋子夾爪汽缸 <夾持> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action14(Director);
            }
        }
    }
}
