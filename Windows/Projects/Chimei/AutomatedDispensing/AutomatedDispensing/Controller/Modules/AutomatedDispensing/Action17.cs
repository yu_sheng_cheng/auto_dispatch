﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action17 : DecompositionAction
        {
            public Action17(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "噴碼上下移載汽缸", "上升", OnInkjetTransferCylinderUpCompleted))
                    return false;

                Go(controller, "噴碼左右移載汽缸", "往右", OnInkjetTransferCylinderRightCompleted);
                return true;
            }

            private void OnInkjetTransferCylinderUpCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼上下移載汽缸 <上升> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            private void OnInkjetTransferCylinderRightCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("噴碼左右移載汽缸 <往右> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action18(Director);
            }
        }
    }
}
