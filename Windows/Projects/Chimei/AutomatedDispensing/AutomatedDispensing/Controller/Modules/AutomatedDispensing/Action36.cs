﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action36 : DecompositionAction
        {
            public Action36(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "ARM4 Z 1.5ml夾取手臂", "Standby", ARM4_Z_小玻璃瓶夾取手臂已到Standby位置);
            }

            private void ARM4_Z_小玻璃瓶夾取手臂已到Standby位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM4 Z 1.5ml夾取手臂 <Standby> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) =>
                {
                    Go(controllerArg, "1.5ml抱瓶汽缸", "退回", 小玻璃瓶抱瓶汽缸已退回);
                    Go(controllerArg, "空瓶移載手臂", "1.5ml收料Tray盤 [1]", 空瓶移載手臂已到收料Tray盤);
                });
            }

            private void 小玻璃瓶抱瓶汽缸已退回(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("1.5ml抱瓶汽缸 <退回> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void 空瓶移載手臂已到收料Tray盤(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("空瓶移載手臂 <1.5ml收料Tray盤 [1]> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action37(Director);
            }
        }
    }
}
