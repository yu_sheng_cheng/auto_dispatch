﻿using System;
using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing.parallel.conveyor
{
    internal partial class ConveyorDirector
    {
        private class Action1 : ConveyorAction
        {
            private readonly OnceTimer _myTimer = new OnceTimer();

            public Action1(ConveyorDirector d) : base(d)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                if (AutoRunContext.ConveyorOwnByOnceCycle)
                {
                    AddTimer(controller, _myTimer, 1000, KeepWaiting);
                    return true;
                }

                if (!Go(controller, "擋瓶汽缸1", "伸出", OnBottleBlockCylinder1ReachOutCompleted))
                {
                    Log("擋瓶汽缸1(伸出) Go失敗");
                    return false;
                }

                //輸送帶馬達線stop
                controller.IORegister.SetCoil(AutomatedDispensingController.FeedingMotorOutput, false);
                Log("關閉輸送帶(try)");
                AddTimer(controller, _myTimer, 100, OnFeedingMotorStopped);
                return true;
            }

            private void OnFeedingMotorStopped(SupervisoryController controller)
            {
                Log(@"入料馬達 <停止>");
                CloseConveyerIfAutoRunCompleted(controller);
                SignalAndWait(controller, "入料馬達", CompletionResult.Success);
            }

            public override DecompositionAction Poll(SupervisoryController controller)
            {
                //Log("-----------------------");
                return base.Poll(controller);
            }

            private void KeepWaiting(SupervisoryController obj)
            {
                RemoveParticipants(1);
                //不做任何事準備進入step1 or 2
            }

            private void OnBottleBlockCylinder1ReachOutCompleted(SupervisoryController controller,
                INamedObject movableObject, CompletionResult result)
            {
                Log(@"擋瓶汽缸1 <伸出> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateConveyorAction()
            {
                if (AutoRunContext.ConveyorOwnByOnceCycle)
                {
                    return new Action1(this.ConveyorDirector);
                }
                else
                {
                    return new Action2(this.ConveyorDirector);
                }
            }
        }
    }
}