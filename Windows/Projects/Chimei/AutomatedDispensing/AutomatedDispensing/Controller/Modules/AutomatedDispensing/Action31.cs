﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action31 : DecompositionAction
        {
            public Action31(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "ARM18 X 送料手臂", "放樣品瓶", ARM18_X_送料手臂已到放樣品瓶位置))
                    return false;

                Go(controller, "取瓶移載手臂", "放瓶", 取瓶移載手臂已到放瓶位置);
                return true;
            }

            private void ARM18_X_送料手臂已到放樣品瓶位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM18 X 送料手臂 <放樣品瓶> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void 取瓶移載手臂已到放瓶位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("取瓶移載手臂 <放瓶> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action32(Director);
            }
        }
    }
}
