﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action26 : DecompositionAction
        {
            public Action26(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "1.5ml抱瓶汽缸", "伸出", On15mlBottleHolderCylinderReachOutCompleted);
            }

            private void On15mlBottleHolderCylinderReachOutCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("1.5ml抱瓶汽缸 <伸出> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "ARM4 Z 1.5ml夾取手臂", "1.5ml抱瓶(有蓋)", OnARM4Completed));
            }

            private void OnARM4Completed(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM4 Z 1.5ml夾取手臂 <1.5ml抱瓶(有蓋)> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "1.5ml抱瓶汽缸", "退回", On15mlBottleHolderCylinderRetractCompleted));
            }

            private void On15mlBottleHolderCylinderRetractCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("1.5ml抱瓶汽缸 <退回> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "開蓋馬達", "開蓋", 小玻璃瓶開蓋完成));
            }

            private void 小玻璃瓶開蓋完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("開蓋馬達 <開蓋> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "ARM4 Z 1.5ml夾取手臂", "Standby", ARM4_Z_小玻璃瓶夾取手臂已到Standby位置));
            }

            private void ARM4_Z_小玻璃瓶夾取手臂已到Standby位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM4 Z 1.5ml夾取手臂 <Standby> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action27(Director);
            }
        }
    }
}
