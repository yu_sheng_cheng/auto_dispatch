﻿using AutomatedDispensing.Controller.Modules.BOM2.Ml1Dispatch;
using Automation.Core.Controller;
using Automation.Core.FSM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action40 : DecompositionAction
        {
            private readonly Dispatch1MDirector _dispatch1MDirector = new Dispatch1MDirector("1ml Tip");

            public Action40(ContinuousActionDirector director) : base(director)
            {

            }

            public override bool Go(SupervisoryController controller)
            {
                if (!_dispatch1MDirector.Go(controller))
                    return false;

                AddParticipants(1);
                return true;
            }

            public override DecompositionAction Poll(SupervisoryController controller)
            {
                _dispatch1MDirector.Poll(controller);

                if (_dispatch1MDirector.IsCompleted)
                {
                    if (!_dispatch1MDirector.IsSuccess)
                    {
                        Director.Result.ObjectName = _dispatch1MDirector.Result.ObjectName;
                        Director.Result.CompletionResult = _dispatch1MDirector.Result.CompletionResult;
                    }

                    RemoveParticipants(1);
                }

                return base.Poll(controller);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action29(Director);
            }
        }
    }
}
