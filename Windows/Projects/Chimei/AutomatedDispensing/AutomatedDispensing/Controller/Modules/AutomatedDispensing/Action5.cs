﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action5 : DecompositionAction
        {
            public Action5(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "擋瓶汽缸2", "退回", OnBottleBlockCylinder2RetractCompleted))
                    return false;

                Go(controller, "推樣品瓶汽缸", "推瓶", OnSampleBottlePushCylinderPushCompleted);
                return true;
            }

            private void OnBottleBlockCylinder2RetractCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("擋瓶汽缸2 <退回> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void OnSampleBottlePushCylinderPushCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("推樣品瓶汽缸 <推瓶> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action6(Director);
            }
        }
    }
}
