﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing.parallel.conveyor
{
    internal partial class ConveyorDirector
    {
        private sealed class Action31 : ConveyorAction
        {
            private bool _hasBottle = false;

            public Action31(ConveyorDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                AddParticipants(1);
                return true;
            }

            /// <summary>
            /// 持續偵測入料區是否已經有瓶子在其間
            /// </summary>
            /// <param Name="controller"></param>
            /// <returns></returns>
            public override DecompositionAction Poll(SupervisoryController controller)
            {
                if (CheckAutoRunCompleted())
                {
                    RemoveParticipants(1);
                }
                else
                {
                    if (!_hasBottle)
                    {
                        if (controller.IORegister.GetContact(AutomatedDispensingController
                            .FeedingWaitBottlePosition1Input))
                        {
                            Log("入料等瓶位置1: 有瓶");
                            _hasBottle = true;
                            RemoveParticipants(1);
                        }
                    }
                    else
                    {
                        _hasBottle = false;
                        Log("目前等瓶裝置是無瓶子的");
                    }
                }

                return base.Poll(controller);
            }

            /// <summary>
            /// 入料區有瓶子是最終目標, 但是一直保持有瓶子在其中, 所以開始持續偵測有瓶子載入料區的狀態模式
            /// </summary>
            /// <returns></returns>
            protected override DecompositionAction CreateConveyorAction()
            {
                if (_hasBottle)
                {
                    return new Action32(this.ConveyorDirector, _hasBottle);
                }
                else
                {
                    return new Action3(this.ConveyorDirector);
                }
            }
        }


        private sealed class Action32 : ConveyorAction
        {
            private readonly bool _hasBottle = false;

            private readonly OnceTimer _myTimer = new OnceTimer();


            public Action32(ConveyorDirector director, bool hasBottle) : base(director)
            {
                this._hasBottle = hasBottle;
            }

            public override bool Go(SupervisoryController controller)
            {

                var c = Go(controller, "擋瓶汽缸1", "伸出", OnBottleBlockCylinder1ReachOutCompleted);
                    controller.IORegister.SetCoil(AutomatedDispensingController.FeedingMotorOutput, false);
                AddTimer(controller, _myTimer, 100, OnFeedingMotorStopped);
                return c;
            }


            private void OnBottleBlockCylinder1ReachOutCompleted(SupervisoryController controller,
                INamedObject movableObject, CompletionResult result)
            {
                LogAction(result, movableObject, "伸出");
                SignalAndWait(controller, movableObject.Name, result);
                CloseConveyerIfAutoRunCompleted(controller);
            }

        
            private void OnFeedingMotorStopped(SupervisoryController controller)
            {
                Log("入料馬達 <停止>");
                SignalAndWait(controller, "入料馬達", CompletionResult.Success);
            }

            /// <summary>
            /// 推瓶汽缸前面有瓶子是最終目標, 但是一直保持有瓶子在其中, 所以開始持續偵測 推瓶汽缸前區的狀態模式
            /// </summary>
            /// <returns></returns>
            protected override DecompositionAction CreateConveyorAction()
            {
                return new Action5(this.ConveyorDirector, _hasBottle);
            }
        }
    }
}