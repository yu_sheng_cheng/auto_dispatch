﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutomatedDispensing
{
    partial class AutomatedDispensingDirector
    {
        private sealed class Action30 : DecompositionAction
        {
            private int _originalAngle;
            private bool _coverCompleted;

            public Action30(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController ctrl)
            {
                AutomatedDispensingController controller = ctrl as AutomatedDispensingController;

                _originalAngle = controller.ARMs[8].IOMemory.CurrentFeedValue;

                Console.WriteLine("OriginalAngle: {0}", _originalAngle);

                return Go(controller, "ARM9 Θ 樣品瓶開蓋手臂", "JOGR", ARM9_Θ_樣品瓶開蓋手臂已停止);
            }

            public override DecompositionAction Poll(SupervisoryController ctrl)
            {
                AutomatedDispensingController controller = ctrl as AutomatedDispensingController;

                if (!_coverCompleted)
                {
                    int angle = controller.ARMs[8].IOMemory.CurrentFeedValue;

                    if (angle < (_originalAngle - 360))
                    {
                        int torque = controller.ARMs[8].IOMemory.CurrentTorque;

                        if (torque > 1600)
                        {
                            Console.WriteLine("Torque: {0}", torque);

                            _coverCompleted = true;
                            controller.ARMs[8].StopJog(ctrl);
                        }
                    }

                    
                }

                return base.Poll(controller);
            }

            private void ARM9_Θ_樣品瓶開蓋手臂已停止(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM9 Θ 樣品瓶開蓋手臂 <Stopped> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "樣品瓶蓋子夾爪汽缸", "打開", 樣品瓶蓋子夾爪汽缸已打開));
            }

            private void 樣品瓶蓋子夾爪汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("樣品瓶蓋子夾爪汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) =>
                {
                    Go(controllerArg, "樣品瓶開蓋上下汽缸", "上升", 樣品瓶開蓋上下汽缸上升完成);
                    Go(controllerArg, "樣品瓶身體夾爪汽缸", "打開", 樣品瓶身體夾爪汽缸已打開);
                });
            }

            private void 樣品瓶開蓋上下汽缸上升完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("樣品瓶開蓋上下汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void 樣品瓶身體夾爪汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("樣品瓶身體夾爪汽缸已打開 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action31(Director);
            }
        }
    }
}
