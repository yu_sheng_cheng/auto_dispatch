﻿using Automation.Core.FSM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    sealed partial class AutoDistributionDirector : ContinuousActionDirector
    {
        public AutoDistributionDirector() : base("自動配藥")
        {

        }

        protected override DecompositionAction CreateFirstAction()
        {
            return new Action1(this);
        }
    }
}
