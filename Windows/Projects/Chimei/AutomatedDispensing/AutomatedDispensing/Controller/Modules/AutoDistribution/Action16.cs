﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    partial class AutoDistributionDirector
    {
        private sealed class Action16 : DecompositionAction
        {
            public Action16(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "配藥移載手臂", "10ml藥劑取液", 配藥移載手臂已到10mL藥劑取液位);
            }

            private void 配藥移載手臂已到10mL藥劑取液位(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("配藥移載手臂 <10ml藥劑取液> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "10ml接漏液汽缸", "伸出", 接10mL漏液汽缸已伸出));
            }

            private void 接10mL漏液汽缸已伸出(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("10ml接漏液汽缸 <伸出> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "配藥移載手臂", "臨時蓋天秤放蓋", 配藥移載手臂已到臨時蓋天秤放蓋位));
            }

            private void 配藥移載手臂已到臨時蓋天秤放蓋位(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("配藥移載手臂 <臨時蓋天秤放蓋> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "微量天秤遮風罩汽缸", "打開", 微量天秤遮風罩汽缸已打開));
            }

            private void 微量天秤遮風罩汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("微量天秤遮風罩汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "吸蓋上下汽缸", "下降", 吸蓋上下汽缸已下降));
            }

            private void 吸蓋上下汽缸已下降(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("吸蓋上下汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "吸蓋上下汽缸", "上升", 吸蓋上下汽缸已上升));
            }

            private void 吸蓋上下汽缸已上升(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("吸蓋上下汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "配藥移載手臂", "10ml天秤滴液", 配藥移載手臂已到10ml天秤滴液位));
            }

            private void 配藥移載手臂已到10ml天秤滴液位(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("配藥移載手臂 <10ml天秤滴液> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "10ml接漏液汽缸", "縮回", 接10mL漏液汽缸已縮回));
            }

            private void 接10mL漏液汽缸已縮回(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("10ml接漏液汽缸 <縮回> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action17(Director);
            }
        }
    }
}
