﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    partial class AutoDistributionDirector
    {
        private sealed class Action3 : DecompositionAction
        {
            public Action3(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "60ml移載夾爪上下汽缸", "下降", 移載60mL夾爪上下汽缸下降完成);
            }

            private void 移載60mL夾爪上下汽缸下降完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml移載夾爪上下汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "60ml移載夾爪汽缸", "打開", 移載60mL夾爪汽缸已打開));
            }

            private void 移載60mL夾爪汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml移載夾爪汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "60ml移載夾爪上下汽缸", "上升", 移載60mL夾爪上下汽缸上升完成));
            }

            private void 移載60mL夾爪上下汽缸上升完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml移載夾爪上下汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) =>
                {
                    if (!Go(controller_, "空瓶移載手臂", "Standby", 空瓶移載手臂已到Standby位置))
                        return;

                    Go(controller, "ARM3 X 送料手臂", "分注", ARM3_X_送料手臂已到分注位置);
                });
            }

            private void 空瓶移載手臂已到Standby位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("空瓶移載手臂 <Standby> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void ARM3_X_送料手臂已到分注位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM3 X 送料手臂 <分注> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action4(Director);
            }
        }
    }
}
