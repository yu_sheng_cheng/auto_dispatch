﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    partial class AutoDistributionDirector
    {
        private sealed class Action1 : DecompositionAction
        {
            public Action1(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "空瓶移載手臂", "60ml供料Tray盤 [1]", 空瓶移載手臂已到60ml供料Tray盤))
                    return false;

                Go(controller, "60ml移載夾爪汽缸", "打開", 移載60mL夾爪汽缸已打開);
                Go(controller, "ARM3 X 送料手臂", "Standby", ARM3_X_送料手臂已到Standby位置);
                return true;
            }

            private void 空瓶移載手臂已到60ml供料Tray盤(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("空瓶移載手臂 <60ml供料Tray盤 [1]> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void 移載60mL夾爪汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml移載夾爪汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void ARM3_X_送料手臂已到Standby位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM3 X 送料手臂 <Standby> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action2(Director);
            }
        }
    }
}
