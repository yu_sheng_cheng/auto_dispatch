﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    partial class AutoDistributionDirector
    {
        private sealed class Action9 : DecompositionAction
        {
            public Action9(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {            
                return Go(controller, "藥劑瓶開蓋身體夾爪汽缸", "夾持", 藥劑瓶開蓋身體夾爪汽缸已夾持);
            }

            private void 藥劑瓶開蓋身體夾爪汽缸已夾持(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("藥劑瓶開蓋身體夾爪汽缸 <夾持> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "藥劑瓶開蓋上下汽缸", "下降", 藥劑瓶開蓋上下汽缸已下降完成));
            }

            private void 藥劑瓶開蓋上下汽缸已下降完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("藥劑瓶開蓋上下汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "藥劑瓶蓋子夾爪汽缸", "夾持", 藥劑瓶蓋子夾爪汽缸已夾持));
            }

            private void 藥劑瓶蓋子夾爪汽缸已夾持(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("藥劑瓶蓋子夾爪汽缸 <夾持> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action10(Director);
            }
        }
    }
}
