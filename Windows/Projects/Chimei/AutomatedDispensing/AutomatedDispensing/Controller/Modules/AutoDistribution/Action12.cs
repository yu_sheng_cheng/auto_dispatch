﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    partial class AutoDistributionDirector
    {
        private sealed class Action12 : DecompositionAction
        {
            public Action12(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "60ml上下移載汽缸", "下降", 上下60mL移載汽缸已下降完成);
            }

            private void 上下60mL移載汽缸已下降完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml上下移載汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "60ml開蓋下夾爪汽缸", "夾持", 開蓋60ml下夾爪汽缸已夾持));
            }

            private void 開蓋60ml下夾爪汽缸已夾持(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml開蓋下夾爪汽缸 <夾持> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "60ml配藥移載夾爪汽缸", "打開", 配藥移載60mL夾爪汽缸已打開));
            }

            private void 配藥移載60mL夾爪汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml配藥移載夾爪汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "60ml上下移載汽缸", "上升", 上下60mL移載汽缸已上升完成));
            }

            private void 上下60mL移載汽缸已上升完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml上下移載汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "配藥移載手臂", "混合液夾瓶", 配藥移載手臂已到混合液夾瓶位置));
            }

            private void 配藥移載手臂已到混合液夾瓶位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("配藥移載手臂 <混合液夾瓶> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "60ml開蓋左右移載汽缸", "往右", 開蓋60ml左右移載汽缸已到右邊));
            }

            private void 開蓋60ml左右移載汽缸已到右邊(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml開蓋左右移載汽缸 <往右> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "60ml開蓋上夾爪汽缸", "打開", 開蓋60ml上夾爪汽缸已打開));
            }

            private void 開蓋60ml上夾爪汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml開蓋上夾爪汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "60ml開蓋上下移載汽缸", "下降", 開蓋60ml上下移載汽缸已下降完成));
            }

            private void 開蓋60ml上下移載汽缸已下降完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml開蓋上下移載汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "60ml開蓋上夾爪汽缸", "夾持", 開蓋60ml上夾爪汽缸已夾持));
            }

            private void 開蓋60ml上夾爪汽缸已夾持(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml開蓋上夾爪汽缸 <夾持> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action13(Director);
            }
        }
    }
}
