﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    partial class AutoDistributionDirector
    {
        private sealed class Action17 : DecompositionAction
        {
            public Action17(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "10ml接漏液汽缸", "伸出", 接10mL漏液汽缸已伸出);
            }

            private void 接10mL漏液汽缸已伸出(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("10ml接漏液汽缸 <伸出> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "配藥移載手臂", "臨時蓋天秤放蓋", 配藥移載手臂已到臨時蓋天秤放蓋位));
            }

            private void 配藥移載手臂已到臨時蓋天秤放蓋位(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("配藥移載手臂 <臨時蓋天秤放蓋> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "吸蓋上下汽缸", "下降", 吸蓋上下汽缸已下降));
            }

            private void 吸蓋上下汽缸已下降(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("吸蓋上下汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "吸蓋上下汽缸", "上升", 吸蓋上下汽缸已上升));
            }

            private void 吸蓋上下汽缸已上升(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("吸蓋上下汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "微量天秤遮風罩汽缸", "關閉", 微量天秤遮風罩汽缸已關閉));
            }

            private void 微量天秤遮風罩汽缸已關閉(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("微量天秤遮風罩汽缸 <關閉> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "配藥移載手臂", "10ml Tip Tray盤 [26]", 配藥移載手臂已到10mLTipTray盤));
            }

            private void 配藥移載手臂已到10mLTipTray盤(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("配藥移載手臂 <10ml Tip Tray盤 [26]> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "ARM15 Z2 10ml移液手臂", "取Tip", ARM15_Z2_10mL移液手臂已可取Tip));
            }

            private void ARM15_Z2_10mL移液手臂已可取Tip(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM15 Z2 10ml移液手臂 <取Tip> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "ARM12 Z2 10ml取TIP手臂", "取Tip", ARM12_Z2_10mL取TIP手臂已可取Tip));
            }

            private void ARM12_Z2_10mL取TIP手臂已可取Tip(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM12 Z2 10ml取TIP手臂 <取Tip> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) =>
                {
                    if (!Go(controller_, "10ml夾爪本體汽缸", "打開", 夾爪10mL本體汽缸已打開))
                        return;

                    Go(controller_, "10ml夾軸芯汽缸", "打開", 夾10mL軸芯汽缸已打開);
                });
            }

            private void 夾爪10mL本體汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("10ml夾爪本體汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void 夾10mL軸芯汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("10ml夾軸芯汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action18(Director);
            }
        }
    }
}
