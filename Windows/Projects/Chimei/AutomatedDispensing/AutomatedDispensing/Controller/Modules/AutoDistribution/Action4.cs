﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    partial class AutoDistributionDirector
    {
        private sealed class Action4 : DecompositionAction
        {
            public Action4(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "ARM7 X 藥劑瓶移載", "移載 [1]", ARM7_X_藥劑瓶移載已到移載位置))
                    return false;

                Go(controller, "ARM13 Y 藥劑瓶夾取手臂", "取瓶 [1]", ARM13_Y_藥劑瓶夾取手臂已到取瓶位置);
                return true;
            }

            private void ARM7_X_藥劑瓶移載已到移載位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM7 X 藥劑瓶移載 <移載 [1]> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void ARM13_Y_藥劑瓶夾取手臂已到取瓶位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM13 Y 藥劑瓶夾取手臂 <取瓶 [1]> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action5(Director);
            }
        }
    }
}
