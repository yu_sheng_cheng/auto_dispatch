﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    partial class AutoDistributionDirector
    {
        private sealed class Action15 : DecompositionAction
        {
            private readonly InputInspection _臨時蓋InputInspection;

            public Action15(ContinuousActionDirector director) : base(director)
            {
                _臨時蓋InputInspection = new InputInspection("臨時蓋真空檢知", 500, 30,
                    (controller) => { return !controller.IORegister.GetContact(AutomatedDispensingController.臨時蓋真空檢知Input); });
            }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "吸蓋上下汽缸", "下降", 吸蓋上下汽缸已下降完成);
            }

            private void 吸蓋上下汽缸已下降完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("吸蓋上下汽缸 <下降> : {0}", result.ToString());

                SignalAndWait(controller, movableObject.Name, result, (controller_) =>
                {
                    controller_.IORegister.SetCoil(AutomatedDispensingController.吸臨時蓋真空Output, false);
                    AddInputInspection(_臨時蓋InputInspection, 臨時蓋真空檢知完成);
                });
            }

            private void 臨時蓋真空檢知完成(SupervisoryController controller, INamedObject inputInspection, CompletionResult result)
            {
                Console.WriteLine("臨時蓋真空檢知: {0}", result == CompletionResult.Success ? "無蓋" : "有蓋");
                SignalAndWait(controller, "臨時蓋真空檢知", result, (controller_) => Go(controller_, "吸蓋上下汽缸", "上升", 吸蓋上下汽缸已上升完成));
            }

            private void 吸蓋上下汽缸已上升完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("吸蓋上下汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "微量天秤遮風罩汽缸", "關閉", 微量天秤遮風罩汽缸已關閉));
            }

            private void 微量天秤遮風罩汽缸已關閉(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("微量天秤遮風罩汽缸 <關閉> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "配藥移載手臂", "10ml Tip Tray盤 [1]", 配藥移載手臂已到10mLTipTray盤));
            }

            private void 配藥移載手臂已到10mLTipTray盤(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("配藥移載手臂 <10ml Tip Tray盤 [1]> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "10ml夾爪本體汽缸", "打開", 夾爪10mL本體汽缸已打開));
            }

            private void 夾爪10mL本體汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("10ml夾爪本體汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "ARM12 Z2 10ml取TIP手臂", "取Tip", ARM12_Z2_10mL取TIP手臂已可取Tip));
            }

            private void ARM12_Z2_10mL取TIP手臂已可取Tip(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM12 Z2 10ml取TIP手臂 <取Tip> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "10ml夾爪本體汽缸", "夾持", 夾爪10mL本體汽缸已夾持));
            }

            private void 夾爪10mL本體汽缸已夾持(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("10ml夾爪本體汽缸 <夾持> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "10ml夾軸芯汽缸", "打開", 夾10mL軸芯汽缸已打開));
            }

            private void 夾10mL軸芯汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("10ml夾軸芯汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "ARM15 Z2 10ml移液手臂", "取Tip", ARM15_Z2_10mL移液手臂已可取Tip));
            }

            private void ARM15_Z2_10mL移液手臂已可取Tip(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM15 Z2 10ml移液手臂 <取Tip> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "10ml夾軸芯汽缸", "夾持", 夾10mL軸芯汽缸已夾持));
            }

            private void 夾10mL軸芯汽缸已夾持(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("10ml夾軸芯汽缸 <夾持> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) =>
                {
                    if (!Go(controller_, "ARM15 Z2 10ml移液手臂", "Standby", ARM15_Z2_10mL移液手臂已到Standby位置))
                        return;

                    Go(controller_, "ARM12 Z2 10ml取TIP手臂", "Standby", ARM12_Z2_10mL取TIP手臂已到Standby位置);
                });
            }

            private void ARM15_Z2_10mL移液手臂已到Standby位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM15 Z2 10ml移液手臂 <Standby> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void ARM12_Z2_10mL取TIP手臂已到Standby位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM12 Z2 10ml取TIP手臂 <Standby> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action16(Director);
            }
        }
    }
}
