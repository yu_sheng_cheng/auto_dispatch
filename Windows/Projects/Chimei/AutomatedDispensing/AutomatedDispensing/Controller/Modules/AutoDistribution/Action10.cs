﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    partial class AutoDistributionDirector
    {
        private sealed class Action10 : DecompositionAction
        {
            private int _originalAngle;
            private bool _uncoverCompleted;

            public Action10(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController ctrl)
            {
                AutomatedDispensingController controller = ctrl as AutomatedDispensingController;

                _originalAngle = controller.ARMs[7].IOMemory.CurrentFeedValue;

                return Go(controller, "ARM8 Θ 藥劑瓶開蓋手臂", "JOGF", ARM8_Θ_藥劑瓶開蓋手臂JOG已停止);
            }

            public override DecompositionAction Poll(SupervisoryController ctrl)
            {
                AutomatedDispensingController controller = ctrl as AutomatedDispensingController;

                if (!_uncoverCompleted)
                {
                    int angle = controller.ARMs[7].IOMemory.CurrentFeedValue;

                    if (angle > (_originalAngle + 720))
                    {
                        _uncoverCompleted = true;
                        controller.ARMs[7].StopJog(ctrl);
                    }
                }

                return base.Poll(controller);
            }

            private void ARM8_Θ_藥劑瓶開蓋手臂JOG已停止(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM8 Θ 藥劑瓶開蓋手臂 <Stopped> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "藥劑瓶開蓋上下汽缸", "上升", 藥劑瓶開蓋上下汽缸已上升完成));
            }

            private void 藥劑瓶開蓋上下汽缸已上升完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("藥劑瓶開蓋上下汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "藥劑瓶開蓋身體夾爪汽缸", "打開", 藥劑瓶開蓋身體夾爪汽缸已打開));
            }

            private void 藥劑瓶開蓋身體夾爪汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("藥劑瓶開蓋身體夾爪汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "ARM18 X 送料手臂", "分注", ARM18_X_送料手臂已到分注位置));
            }

            private void ARM18_X_送料手臂已到分注位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM18 X 送料手臂 <分注> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action11(Director);
            }
        }
    }
}
