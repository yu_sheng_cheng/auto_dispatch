﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    partial class AutoDistributionDirector
    {
        private sealed class Action7 : DecompositionAction
        {
            public Action7(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "藥劑瓶上下汽缸", "下降", 藥劑瓶上下汽缸已下降完成);
            }

            private void 藥劑瓶上下汽缸已下降完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("藥劑瓶上下汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "藥劑瓶夾爪汽缸", "夾持", 藥劑瓶夾爪汽缸已夾持));
            }

            private void 藥劑瓶夾爪汽缸已夾持(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("藥劑瓶夾爪汽缸 <夾持> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "藥劑瓶上下汽缸", "上升", 藥劑瓶上下汽缸已上升完成));
            }

            private void 藥劑瓶上下汽缸已上升完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("藥劑瓶上下汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) =>
                {
                    if (!Go(controller_, "ARM13 Y 藥劑瓶夾取手臂", "移載", ARM13_Y_藥劑瓶夾取手臂已到移載位置))
                        return;

                    Go(controller_, "ARM18 X 送料手臂", "放藥劑瓶", ARM18_X_送料手臂已到放藥劑瓶位置);
                });
            }

            private void ARM13_Y_藥劑瓶夾取手臂已到移載位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM13 Y 藥劑瓶夾取手臂 <移載> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void ARM18_X_送料手臂已到放藥劑瓶位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM18 X 送料手臂 <放藥劑瓶> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action8(Director);
            }
        }
    }
}
