﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    partial class AutoDistributionDirector
    {
        private sealed class Action14 : DecompositionAction
        {
            private readonly InputInspection _臨時蓋InputInspection;

            public Action14(ContinuousActionDirector director) : base(director)
            {
                _臨時蓋InputInspection = new InputInspection("臨時蓋真空檢知", 500, 30,
                    (controller) => { return controller.IORegister.GetContact(AutomatedDispensingController.臨時蓋真空檢知Input); });
            }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "60ml上下移載汽缸", "下降", 上下60mL移載汽缸已下降完成);
            }

            private void 上下60mL移載汽缸已下降完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml上下移載汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "60ml配藥移載夾爪汽缸", "打開", 配藥移載60mL夾爪汽缸已打開));
            }

            private void 配藥移載60mL夾爪汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml配藥移載夾爪汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "60ml上下移載汽缸", "上升", 上下60mL移載汽缸已上升完成));
            }

            private void 上下60mL移載汽缸已上升完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml上下移載汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "配藥移載手臂", "臨時蓋取蓋 [1]", 配藥移載手臂已到臨時蓋取蓋位));
            }

            private void 配藥移載手臂已到臨時蓋取蓋位(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("配藥移載手臂 <臨時蓋取蓋 [1]> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "吸蓋上下汽缸", "下降", 吸蓋上下汽缸已下降完成));
            }

            private void 吸蓋上下汽缸已下降完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("吸蓋上下汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) =>
                {
                    controller_.IORegister.SetCoil(AutomatedDispensingController.吸臨時蓋真空Output, true);
                    AddInputInspection(_臨時蓋InputInspection, 臨時蓋真空檢知完成);
                });
            }

            private void 臨時蓋真空檢知完成(SupervisoryController controller, INamedObject inputInspection, CompletionResult result)
            {
                Console.WriteLine("臨時蓋真空檢知: {0}", result == CompletionResult.Success ? "有蓋" : "無蓋");

                SignalAndWait(controller, "臨時蓋真空檢知", result, (controller_) => Go(controller_, "吸蓋上下汽缸", "上升", 吸蓋上下汽缸已上升完成));
            }

            private void 吸蓋上下汽缸已上升完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("吸蓋上下汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "配藥移載手臂", "臨時蓋天秤放蓋", 配藥移載手臂已到臨時蓋天秤放蓋位));
            }

            private void 配藥移載手臂已到臨時蓋天秤放蓋位(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("配藥移載手臂 <臨時蓋天秤放蓋> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action15(Director);
            }
        }
    }
}
