﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    partial class AutoDistributionDirector
    {
        private sealed class Action18 : DecompositionAction
        {
            public Action18(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "ARM15 Z2 10ml移液手臂", "Standby", ARM15_Z2_10mL移液手臂已到Standby位置))
                    return false;

                Go(controller, "ARM12 Z2 10ml取TIP手臂", "Standby", ARM12_Z2_10mL取TIP手臂已到Standby位置);
                return true;
            }

            private void ARM15_Z2_10mL移液手臂已到Standby位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM15 Z2 10ml移液手臂 <Standby> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void ARM12_Z2_10mL取TIP手臂已到Standby位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM12 Z2 10ml取TIP手臂 <Standby> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }
        }
    }
}
