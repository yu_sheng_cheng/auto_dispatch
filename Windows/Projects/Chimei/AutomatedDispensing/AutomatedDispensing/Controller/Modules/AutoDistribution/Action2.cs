﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    partial class AutoDistributionDirector
    {
        private sealed class Action2 : DecompositionAction
        {
            public Action2(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "60ml移載夾爪上下汽缸", "下降", 移載60mL夾爪上下汽缸下降完成);
            }

            private void 移載60mL夾爪上下汽缸下降完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml移載夾爪上下汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "60ml移載夾爪汽缸", "夾持", 移載60mL夾爪汽缸已夾持));
            }

            private void 移載60mL夾爪汽缸已夾持(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml移載夾爪汽缸 <夾持> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "60ml移載夾爪上下汽缸", "上升", 移載60mL夾爪上下汽缸上升完成));
            }

            private void 移載60mL夾爪上下汽缸上升完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml移載夾爪上下汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "空瓶移載手臂", "60ml放料1", 空瓶移載手臂已到60ml放料1位置));
            }

            private void 空瓶移載手臂已到60ml放料1位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("空瓶移載手臂 <60ml放料1> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action3(Director);
            }
        }
    }
}
