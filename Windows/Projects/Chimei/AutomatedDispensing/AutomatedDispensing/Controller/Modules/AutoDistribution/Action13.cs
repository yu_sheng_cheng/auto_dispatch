﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    partial class AutoDistributionDirector
    {
        private sealed class Action13 : DecompositionAction
        {
            private int _originalAngle;
            private bool _uncoverCompleted;

            public Action13(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController ctrl)
            {
                AutomatedDispensingController controller = ctrl as AutomatedDispensingController;

                _originalAngle = controller.ARMs[9].IOMemory.CurrentFeedValue;

                return Go(controller, "ARM10 Θ 60ml開蓋手臂", "JOGF", ARM10_Θ_60mL開蓋手臂JOG已停止);
            }

            public override DecompositionAction Poll(SupervisoryController ctrl)
            {
                AutomatedDispensingController controller = ctrl as AutomatedDispensingController;

                if (!_uncoverCompleted)
                {
                    int angle = controller.ARMs[9].IOMemory.CurrentFeedValue;

                    if (angle < (_originalAngle + 540))
                    {
                        _uncoverCompleted = true;
                        controller.ARMs[9].StopJog(ctrl);
                    }
                }

                return base.Poll(controller);
            }

            private void ARM10_Θ_60mL開蓋手臂JOG已停止(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM10 Θ 60ml開蓋手臂 <Stopped> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "60ml開蓋上下移載汽缸", "上升", 開蓋60mL上下移載汽缸已上升完成));
            }

            private void 開蓋60mL上下移載汽缸已上升完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml開蓋上下移載汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "60ml開蓋左右移載汽缸", "往左", 開蓋60ml左右移載汽缸已到左邊));
            }

            private void 開蓋60ml左右移載汽缸已到左邊(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml開蓋左右移載汽缸 <往左> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "配藥移載手臂", "60ml開蓋", 配藥移載手臂已到60ml開蓋位置));
            }

            private void 配藥移載手臂已到60ml開蓋位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("配藥移載手臂 <60ml開蓋> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "60ml上下移載汽缸", "下降", 上下60mL移載汽缸已下降完成));
            }

            private void 上下60mL移載汽缸已下降完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml上下移載汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "60ml配藥移載夾爪汽缸", "夾持", 配藥移載60mL夾爪汽缸已夾持));
            }

            private void 配藥移載60mL夾爪汽缸已夾持(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml配藥移載夾爪汽缸 <夾持> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "60ml開蓋下夾爪汽缸", "打開", 開蓋60ml下夾爪汽缸已打開));
            }

            private void 開蓋60ml下夾爪汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml開蓋下夾爪汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) => Go(controllerArg, "60ml上下移載汽缸", "上升", 上下60mL移載汽缸已上升完成));
            }

            private void 上下60mL移載汽缸已上升完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("60ml上下移載汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) =>
                {
                    if (!Go(controller_, "配藥移載手臂", "60ml秤重", 配藥移載手臂已到60ml秤重位置))
                        return;

                    Go(controller_, "微量天秤遮風罩汽缸", "打開", 微量天秤遮風罩汽缸已打開);
                });
            }

            private void 配藥移載手臂已到60ml秤重位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("配藥移載手臂 <60ml秤重> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void 微量天秤遮風罩汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("微量天秤遮風罩汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action14(Director);
            }
        }
    }
}
