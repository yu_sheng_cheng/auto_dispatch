﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.AutoDistribution
{
    partial class AutoDistributionDirector
    {
        private sealed class Action8 : DecompositionAction
        {
            public Action8(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "藥劑瓶上下汽缸", "下降", 藥劑瓶上下汽缸已下降完成);
            }

            private void 藥劑瓶上下汽缸已下降完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("藥劑瓶上下汽缸 <下降> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "藥劑瓶夾爪汽缸", "打開", 藥劑瓶夾爪汽缸已打開));
            }

            private void 藥劑瓶夾爪汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("藥劑瓶夾爪汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "藥劑瓶上下汽缸", "上升", 藥劑瓶上下汽缸已上升完成));
            }

            private void 藥劑瓶上下汽缸已上升完成(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("藥劑瓶上下汽缸 <上升> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) =>
                {
                    if (!Go(controller_, "ARM13 Y 藥劑瓶夾取手臂", "Standby", ARM13_Y_藥劑瓶夾取手臂已到Standby位置))
                        return;

                    Go(controller_, "ARM18 X 送料手臂", "開蓋", ARM18_X_送料手臂已到開蓋位置);
                    Go(controller_, "藥劑瓶蓋子夾爪汽缸", "打開", 藥劑瓶蓋子夾爪汽缸已打開);
                });
            }

            private void ARM13_Y_藥劑瓶夾取手臂已到Standby位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM13 Y 藥劑瓶夾取手臂 <Standby> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void ARM18_X_送料手臂已到開蓋位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM18 X 送料手臂 <開蓋> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void 藥劑瓶蓋子夾爪汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("藥劑瓶蓋子夾爪汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action9(Director);
            }
        }
    }
}
