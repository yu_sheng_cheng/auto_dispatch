﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.context
{
    public class AutoRunContext
    {
        /// <summary>
        /// 有一灌藥水被處理中
        /// </summary>
        bool HasAPotionBeProcessing { get; set; }

        /// <summary>
        /// 輸送帶被one cycle流程控制
        /// </summary>
        public bool ConveyorOwnByOnceCycle { get; set; }

        /// <summary>
        /// AutoRun主流程是否走了一遭
        /// </summary>
        public bool AutoRunCompleted { get; set; }

        public AutoRunContext()
        {
            AutoRunCompleted = false;
            ConveyorOwnByOnceCycle = false;
            HasAPotionBeProcessing = false;
        }
    }

    public interface IIsAutoRun
    {
        AutoRunContext GetAutoRunContext();
    }
}