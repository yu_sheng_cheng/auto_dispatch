﻿using System;
using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;

namespace AutomatedDispensing.Controller.Modules.BOM2.Ml1Dispatch
{
    public partial class Dispatch1MDirector
    {
        internal class Action5 : DecompositionAction
        {
            private readonly OnceTimer _feedingMotorTimer = new OnceTimer();

            private volatile bool _totalSuccess = true;

            public Action5(ContinuousActionDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                TryLog(Arm5, @"移動到1ml天秤滴液");
                Go(controller, Arm5, "1ml天秤滴液", _onArm51ml天秤滴液);

                TryLog(Arm6, @"移動到1ml天秤滴液");

                Go(controller, Arm6, "1ml天秤滴液", _onArm61ml天秤滴液);
                return true;
            }

            private void _onArm51ml天秤滴液(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                if (IsSuccessAction(arg3))
                {
                    OkLog(Arm5 , @"移動到1ml天秤滴液(ok)");
                }
                else
                {
                    FailedLog(Arm5 , @"移動到1ml天秤滴液(failed)");
                    _totalSuccess = false;
                }

                SignalAndWait(arg1, Arm5, arg3);
            }

            private void _onArm61ml天秤滴液(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                if (IsSuccessAction(arg3))
                {
                    OkLog(Arm6 , @"移動到1ml天秤滴液(ok)");
                }
                else
                {
                    FailedLog(Arm6 , @"移動到1ml天秤滴液(failed)");
                    _totalSuccess = false;
                }

                SignalAndWait(arg1, Arm5, arg3);
            }

            protected override DecompositionAction CreateNextAction()
            {
                if (_totalSuccess)
                {
                    return new Action6(Director);
                }
                else
                {
                    return new RecoverCaptor(Director);
                }
            }
        }
    }
}