﻿using System;
using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;

namespace AutomatedDispensing.Controller.Modules.BOM2.Ml1Dispatch
{
    public partial class Dispatch1MDirector
    {
        internal class Action4 : DecompositionAction
        {
            private readonly OnceTimer _feedingMotorTimer = new OnceTimer();

            private volatile bool _totalSuccess = true;

            private const string TargetLocationName = "1ml樣品取液";

            public Action4(ContinuousActionDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                TryLog(Arm5, @"移動到" + TargetLocationName + @"(try)");
                Go(controller, Arm5, TargetLocationName, _onArm51ml台車取液1);
                //                Go(controller, Arm5, "1ml台車取液2", _onArm51ml台車取液2);
                TryLog(Arm6, @"移動" + TargetLocationName + @"(try)");
                Go(controller, Arm6, TargetLocationName, _onArm61ml台車取液1);
                return true;
            }

            private void _onArm51ml台車取液1(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                if (IsSuccessAction(arg3))
                {
                    OkLog(Arm5, @"移動到" + TargetLocationName + @"(ok)");
                }
                else
                {
                    FailedLog(Arm5, @"移動" + TargetLocationName + @"(failed)");
                    _totalSuccess = false;
                }

                SignalAndWait(arg1, Arm5, arg3);
            }

            private void _onArm61ml台車取液1(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                if (IsSuccessAction(arg3))
                {
                    OkLog(Arm6, @"移動到" + TargetLocationName + @"(ok)");
                }
                else
                {
                    FailedLog(Arm6, @"移動到" + TargetLocationName + @"(failed)");
                    _totalSuccess = false;
                }

                SignalAndWait(arg1, Arm5, arg3);
            }

            protected override DecompositionAction CreateNextAction()
            {
                if (_totalSuccess)
                {
                    return new Action5(Director);
                }
                else
                {
                    return new RecoverCaptor(Director);
                }
            }
        }
    }
}