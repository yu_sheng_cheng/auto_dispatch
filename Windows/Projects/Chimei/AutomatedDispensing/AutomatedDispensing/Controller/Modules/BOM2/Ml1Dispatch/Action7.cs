﻿using System;
using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;

namespace AutomatedDispensing.Controller.Modules.BOM2.Ml1Dispatch
{
    public partial class Dispatch1MDirector
    {
        internal class Action7 : DecompositionAction
        {
            private readonly OnceTimer _feedingMotorTimer = new OnceTimer();

            private volatile bool _totalSuccess = true;

            private const string Action = "丟Tip";

            private const string Arm14Action = "丟棄Tip";


            public Action7(ContinuousActionDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                TryLog(Arm11, Action);
                return Go(controller, Arm11, Action, _OnArm11_丟Tip);
            }

            private void _OnArm11_丟Tip(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                if (IsSuccessAction(arg3))
                {
                    OkLog(Arm11, Arm14Action);
                    Go(arg1, Arm14, Arm14Action, _OnArm14丟Tip);
                    SignalAndWait(arg1, Arm11, arg3);
                }
                else
                {
                    FailedLog(Arm11, Arm14Action);
                    _totalSuccess = false;
                }
                
            }

            private void _OnArm14丟Tip(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                if (IsSuccessAction(arg3))
                {
                    OkLog(Arm14, Arm14Action);
                    SignalAndWait(arg1, Arm14, arg3);
                }
                else
                {
                    FailedLog(Arm14, Arm14Action);
                    _totalSuccess = false;
                }
            }

            protected override DecompositionAction CreateNextAction()
            {
                if (_totalSuccess)
                {
                    return new RecoverCaptor(Director);
                }
                else
                {
                    return new RecoverCaptor(Director);
                }
            }
        }
    }
}