﻿using System;
using System.Diagnostics;
using AutomatedDispensing.Controller.Modules.AutomatedDispensing.parallel.conveyor;
using AutomatedDispensing.Controller.Modules.context;
using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;

namespace AutomatedDispensing.Controller.Modules.BOM2.DiscardWasteLiquid
{
    static class Arm18X送料手臂
    {
        public static string Name = "ARM18 X 送料手臂";
        public static string 待機 = "Standby";
        public static string 放樣品瓶 = "放樣品瓶";
        public static string 放藥劑瓶 = "放藥劑瓶";
        public static string 開蓋 = "開蓋";
        public static string 分注 = "分注";
    }

    static class 樣品瓶開蓋上下汽缸
    {
        public static string Name = "樣品瓶開蓋上下汽缸";
        public static string 上升 = "上升";
        public static string 下降 = "下降";
    }

    static class 樣品瓶身體夾爪汽缸
    {
        public static string Name = "樣品瓶身體夾爪汽缸";

        public static string 打開 = "打開";
        public static string 夾持 = "夾持";
    }


    static class 樣品瓶蓋子夾爪汽缸
    {
        public static string Name = "樣品瓶蓋子夾爪汽缸";
        public static string 打開 = "打開";
        public static string 夾持 = "夾持";
    }

    static class ARM16X取瓶移載手臂
    {
        public static string Name = "ARM16 X 取瓶移載手臂";
        public static string 待機 = "Standby";
        public static string 掃碼 = "掃碼";
        public static string 倒廢液 = "倒廢液";
        public static string 回收區 = "回收區 [1]";
        public static string 待處理區 = "待處理區";
        public static string NG區 = "NG區";
        public static string 放瓶 = "放瓶";
    }

    static class ARM17X取瓶移載手臂
    {
        public static string Name = "ARM17 Y 取瓶移載手臂";
        public static string 待機 = "Standby";
        public static string 掃碼 = "掃碼";
        public static string 倒廢液 = "倒廢液";
        public static string 回收區 = "回收區 [1]";
        public static string 待處理區 = "待處理區";
        public static string NG區 = "NG區";
        public static string 放瓶 = "放瓶";
    }


    static class 上下移載汽缸
    {
        public static string Name = "上下移載汽缸";
        public static string 上升 = "上升";
        public static string 下降 = "下降";
    }


    static class 移載夾爪汽缸
    {
        public static string Name = "移載夾爪汽缸";
        public static string 打開 = "打開";
        public static string 夾持 = "夾持";
    }

    static class 倒廢液夾爪汽缸
    {
        public static string Name = "倒廢液夾爪汽缸";
        public static string 打開 = "打開";
        public static string 夾持 = "夾持";
    }

    static class 倒廢液汽缸
    {
        public static string Name = "倒廢液汽缸";
        public static string 正 = "正面";
        public static string 反 = "反面";
    }

    public class DisCardLiquidContext
    {
        /// <summary>
        /// 停止了
        /// </summary>
        public bool Stop;

        /// <summary>
        /// 需要被處理的
        /// </summary>
        public int NumOfBottom = int.MaxValue;
    }

    public partial class DiscardLiquidDirector : ContinuousActionDirector
    {
        public DisCardLiquidContext DisCardLiquidContext { get; set; }

        public Func<DiscardLiquidDirector, bool> OnComplete { get; set; }

        const string ARM9 = "ARM9 Θ 樣品瓶開蓋手臂";

        public DiscardLiquidDirector(string name) : base(name)
        {
        }

        protected override DecompositionAction CreateFirstAction()
        {
            return new Action1(this, false);
        }

        internal class Action1Of1 : DiscardLiquidActor
        {
            public Action1Of1(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "擋瓶汽缸2", "退回", OnBottleBlockCylinder2RetractCompleted))
                    return false;
                Go(controller, 樣品瓶蓋子夾爪汽缸.Name, ARM17X取瓶移載手臂.待機, OnReach樣品瓶蓋子夾爪汽缸_打開);
                Go(controller, ARM16X取瓶移載手臂.Name, ARM16X取瓶移載手臂.待機, OnReach樣品瓶蓋子夾爪汽缸_待機);
                Go(controller, "推樣品瓶汽缸", "推瓶", OnSampleBottlePushCylinderPushCompleted);
                return true;
            }

            private void OnReach樣品瓶蓋子夾爪汽缸_待機(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM16X取瓶移載手臂.待機);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnReach樣品瓶蓋子夾爪汽缸_打開(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM17X取瓶移載手臂.待機);
                SignalAndWait(arg1, arg2.Name, arg3);
            }


            private void OnBottleBlockCylinder2RetractCompleted(SupervisoryController controller,
                INamedObject movableObject, CompletionResult result)
            {
                LogAction(result, movableObject, "退回");
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void OnSampleBottlePushCylinderPushCompleted(SupervisoryController controller,
                INamedObject movableObject, CompletionResult result)
            {
                LogAction(result, movableObject, "推瓶");
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action2Of1(DiscardLiquidDirector);
            }
        }

        internal class Action1 : DiscardLiquidActor
        {
            private bool _hasBottle;

            public Action1(DiscardLiquidDirector director, bool hasBottle) : base(director)
            {
                _hasBottle = hasBottle;
            }

            public override bool Go(SupervisoryController controller)
            {
                AddParticipants(1);
                return true;
            }

            public override DecompositionAction Poll(SupervisoryController controller)
            {
                if (!_hasBottle)
                {
                    if (!controller.IORegister.GetContact(AutomatedDispensingController
                        .FeedingWaitBottlePosition1Input))
                    {
                        //Log("入料等瓶位置1: 無瓶");
                        _hasBottle = false;
                    }
                    else
                    {
                        _hasBottle = true;
                        //Log("入料等瓶位置1: 有瓶");
                        RemoveParticipants(1);
                    }
                }

                return base.Poll(controller);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action1Of1(DiscardLiquidDirector);
            }
        }


        internal class Action2Of1 : DiscardLiquidActor
        {
            public Action2Of1(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "推樣品瓶汽缸", "縮回", OnSampleBottlePushCylinderRetractCompleted);
            }

            private void OnSampleBottlePushCylinderRetractCompleted(SupervisoryController controller,
                INamedObject movableObject, CompletionResult result)
            {
                LogAction(result, movableObject, "縮回");
                SignalAndWait(controller, movableObject.Name, result,
                    (controllerArg) => Go(controllerArg, "進料旋轉汽缸", "秤重", OnFeedSpinCylinderWeighingAreaArrivaled));
            }

            private void OnFeedSpinCylinderWeighingAreaArrivaled(SupervisoryController controller,
                INamedObject movableObject, CompletionResult result)
            {
                LogAction(result, movableObject, "秤重");
                SignalAndWait(controller, movableObject.Name, result,
                    (controllerArg) => Go(controller, "進料夾爪汽缸", "打開", OnFeedGripperCylinderOpenCompleted));
            }

            private void OnFeedGripperCylinderOpenCompleted(SupervisoryController controller,
                INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, "打開");
                SignalAndWait(controller, movableObject.Name, result,
                    (controllerArg) => Go(controllerArg, "進料上下汽缸", "下降", OnFeedCylinderDownCompleted));
            }

            private void OnFeedCylinderDownCompleted(SupervisoryController controller, INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, "下降");
                SignalAndWait(controller, movableObject.Name, result,
                    (controllerArg) => Go(controllerArg, "進料夾爪汽缸", "夾持",
                        OnFeedGripperCylinderHoldCompleted));
            }

            private void OnFeedGripperCylinderHoldCompleted(SupervisoryController controller,
                INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, "夾持");
                SignalAndWait(controller, movableObject.Name, result,
                    (controllerArg) => Go(controllerArg, "進料上下汽缸", "上升",
                        OnFeedCylinderUpCompleted));
            }

            private void OnFeedCylinderUpCompleted(SupervisoryController controller, INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, "上升");
                SignalAndWait(controller, movableObject.Name, result,
                    (controllerArg) => Go(controllerArg, "進料旋轉汽缸", "掃碼",
                        OnFeedSpinCylinderBarcodeScaningAreaArrivaled));
            }

            private void OnFeedSpinCylinderBarcodeScaningAreaArrivaled(SupervisoryController controller,
                INamedObject movableObject, CompletionResult result)
            {
                LogAction(result, movableObject, "掃碼");
                SignalAndWait(controller, movableObject.Name, result);
            }


            protected override DecompositionAction GoToNext()
            {
                return new Action3Of1(DiscardLiquidDirector);
            }
        }


        internal sealed class Action3Of1 : DiscardLiquidActor
        {
            public Action3Of1(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "進料上下汽缸", "下降", OnFeedCylinderDownCompleted);
            }

            private void OnFeedCylinderDownCompleted(SupervisoryController controller, INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, "下降");
                SignalAndWait(controller, movableObject.Name, result,
                    (controllerArg) => Go(controllerArg, "進料夾爪汽缸", "打開",
                        OnFeedGripperCylinderOpenCompleted));
            }

            private void OnFeedGripperCylinderOpenCompleted(SupervisoryController controller,
                INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, "打開");
                SignalAndWait(controller, movableObject.Name, result,
                    (controllerArg) => Go(controllerArg, "進料上下汽缸", "上升",
                        OnFeedCylinderUpCompleted));
            }

            private void OnFeedCylinderUpCompleted(SupervisoryController controller, INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, "上升打開");
                SignalAndWait(controller, movableObject.Name, result,
                    (controllerArg) => Go(controllerArg, "進料旋轉汽缸", "秤重",
                        OnFeedSpinCylinderWeighingAreaArrivaled));
            }

            private void OnFeedSpinCylinderWeighingAreaArrivaled(SupervisoryController controller,
                INamedObject movableObject, CompletionResult result)
            {
                LogAction(result, movableObject, "秤重");
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action4Of1(DiscardLiquidDirector);
            }
        }

        internal class Action4Of1 : DiscardLiquidActor
        {
            public Action4Of1(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
//                Go(controller, 樣品瓶蓋子夾爪汽缸.Name, 樣品瓶蓋子夾爪汽缸.打開, OnReach樣品瓶蓋子夾爪汽缸_打開);
                Go(controller, ARM16X取瓶移載手臂.Name, ARM16X取瓶移載手臂.掃碼, OnReachARM16X取瓶移載手臂_掃碼);
                return Go(controller, ARM17X取瓶移載手臂.Name, ARM17X取瓶移載手臂.掃碼, OnReachARM17X取瓶移載手臂_掃碼);
            }

            // private void OnReach樣品瓶蓋子夾爪汽缸_打開(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            // {
            //     LogAction(arg3, arg2, 樣品瓶蓋子夾爪汽缸.打開);
            //     SignalAndWait(arg1, arg2.Name, arg3);
            // }

            private void OnReachARM17X取瓶移載手臂_掃碼(SupervisoryController arg1, INamedObject arg2,
                CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM16X取瓶移載手臂.掃碼);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnReachARM16X取瓶移載手臂_掃碼(SupervisoryController arg1, INamedObject arg2,
                CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM17X取瓶移載手臂.掃碼);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action5Of1(DiscardLiquidDirector);
            }
        }


        internal class Action5Of1 : DiscardLiquidActor
        {
            public Action5Of1(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                Go(controller, Arm18X送料手臂.Name, Arm18X送料手臂.放樣品瓶, OnA);
                return Go(controller, 上下移載汽缸.Name, 上下移載汽缸.下降, On上下移載汽缸下降完成);
            }

            private void OnA(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, Arm18X送料手臂.放樣品瓶);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void On上下移載汽缸下降完成(SupervisoryController controller, INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, 上下移載汽缸.下降);
                Go(controller, 移載夾爪汽缸.Name, 移載夾爪汽缸.打開, On移載夾爪汽缸已打開);
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void On移載夾爪汽缸已打開(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 移載夾爪汽缸.打開);
                Go(arg1, 移載夾爪汽缸.Name, 移載夾爪汽缸.夾持, On移載夾爪汽缸夾持);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void On移載夾爪汽缸夾持(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 移載夾爪汽缸.夾持);
                Go(arg1, 上下移載汽缸.Name, 上下移載汽缸.上升, On上下移載汽缸上升完成);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void On上下移載汽缸上升完成(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 上下移載汽缸.上升);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action8Of1(DiscardLiquidDirector);
            }
        }


        // internal class Action8Of1 : DiscardLiquidActor
        // {
        //     public Action8Of1(DiscardLiquidDirector director) : base(director)
        //     {
        //     }
        //
        //     public override bool Go(SupervisoryController controller)
        //     {
        //         Go(controller, ARM16X取瓶移載手臂.Name, ARM16X取瓶移載手臂.掃碼, OnReachARM16X取瓶移載手臂_掃碼);
        //         return Go(controller, ARM17X取瓶移載手臂.Name, ARM17X取瓶移載手臂.掃碼, OnReachARM17X取瓶移載手臂_掃碼);
        //     }
        //
        //     private void OnReachARM17X取瓶移載手臂_掃碼(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
        //     {
        //         LogAction(arg3, arg2, ARM16X取瓶移載手臂.掃碼);
        //         SignalAndWait(arg1, arg2.Name, arg3);
        //     }
        //
        //     private void OnReachARM16X取瓶移載手臂_掃碼(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
        //     {
        //         LogAction(arg3, arg2, ARM17X取瓶移載手臂.掃碼);
        //         SignalAndWait(arg1, arg2.Name, arg3);
        //     }
        //
        //     protected override DecompositionAction GoToNext()
        //     {
        //         return new Action9Of1(DiscardLiquidDirector);
        //     }
        // }


        internal class Action8Of1 : DiscardLiquidActor
        {
            public Action8Of1(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                Go(controller, ARM16X取瓶移載手臂.Name, ARM16X取瓶移載手臂.放瓶, OnReachARM16X取瓶移載手臂_放瓶);
                return Go(controller, ARM17X取瓶移載手臂.Name, ARM17X取瓶移載手臂.放瓶, OnReachARM17X取瓶移載手臂_放瓶);
            }

            private void OnReachARM17X取瓶移載手臂_放瓶(SupervisoryController arg1, INamedObject arg2,
                CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM16X取瓶移載手臂.放瓶);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnReachARM16X取瓶移載手臂_放瓶(SupervisoryController arg1, INamedObject arg2,
                CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM17X取瓶移載手臂.放瓶);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action9Of1(DiscardLiquidDirector);
            }
        }


        internal class Action9Of1 : DiscardLiquidActor
        {
            public Action9Of1(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, 上下移載汽缸.Name, 上下移載汽缸.下降, On上下移載汽缸下降完成);
            }

            private void On上下移載汽缸下降完成(SupervisoryController controller, INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, 上下移載汽缸.下降);
                Go(controller, 移載夾爪汽缸.Name, 移載夾爪汽缸.打開, On移載夾爪汽缸已打開);
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void On移載夾爪汽缸已打開(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 移載夾爪汽缸.打開);
                Go(arg1, 上下移載汽缸.Name, 上下移載汽缸.上升, On上下移載汽缸上升完成);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void On上下移載汽缸上升完成(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 上下移載汽缸.上升);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                return new ActionNOf1(DiscardLiquidDirector);
            }
        }


        /// <summary>
        /// 移動到開蓋區, 並且上下抓住瓶子
        /// </summary>
        internal class ActionNOf1 : DiscardLiquidActor
        {
            public ActionNOf1(DiscardLiquidDirector director) : base(director)
            {
            }


            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, Arm18X送料手臂.Name, Arm18X送料手臂.開蓋, OnReach開蓋);
            }

            private void OnReach樣品瓶蓋子夾爪汽缸_打開(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 樣品瓶蓋子夾爪汽缸.打開);
                Go(arg1, 樣品瓶身體夾爪汽缸.Name, 樣品瓶身體夾爪汽缸.夾持, OnReach樣品瓶身體夾爪汽缸_夾持);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnReach開蓋(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, Arm18X送料手臂.開蓋);
                Go(arg1, 樣品瓶蓋子夾爪汽缸.Name, 樣品瓶蓋子夾爪汽缸.打開, OnReach樣品瓶蓋子夾爪汽缸_打開);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnReach樣品瓶身體夾爪汽缸_夾持(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 樣品瓶身體夾爪汽缸.夾持);
                Go(arg1, 樣品瓶開蓋上下汽缸.Name, 樣品瓶開蓋上下汽缸.下降, OnReach樣品瓶開蓋上下汽缸_下);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnReach樣品瓶蓋子夾爪汽缸_夾持(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 樣品瓶蓋子夾爪汽缸.夾持);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnReach樣品瓶開蓋上下汽缸_下(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 樣品瓶開蓋上下汽缸.下降);
                Go(arg1, 樣品瓶蓋子夾爪汽缸.Name, 樣品瓶蓋子夾爪汽缸.夾持, OnReach樣品瓶蓋子夾爪汽缸_夾持);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action2(DiscardLiquidDirector);
            }
        }


        /// <summary>
        /// 
        ///執行開蓋並且把開蓋後的瓶子透過推車送往"放樣品瓶"區塊
        /// </summary>
        internal sealed class Action2 : DiscardLiquidActor
        {
            private int _originalAngle;
            private bool _uncoverCompleted;
            private readonly OnceTimer _feedingMotorTimer = new OnceTimer();

            public Action2(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController ctrl)
            {
                var controller = ctrl as AutomatedDispensingController;
                Debug.Assert(controller != null);
                _originalAngle = controller.ARMs[8].IOMemory.CurrentFeedValue;
                return Go(controller, ARM9, "JOGF", OnARM9_Θ_樣品瓶開蓋手臂已停止);
            }

            public override DecompositionAction Poll(SupervisoryController ctrl)
            {
                var controller = ctrl as AutomatedDispensingController;

                if (!_uncoverCompleted)
                {
                    int angle = controller.ARMs[8].IOMemory.CurrentFeedValue;

                    // 430是一個完美角度
                    if (angle > (_originalAngle + 430))
                    {
                        _uncoverCompleted = true;
                        controller.ARMs[8].StopJog(ctrl);
                    }
                }

                return base.Poll(controller);
            }

            private void OnARM9_Θ_樣品瓶開蓋手臂已停止(SupervisoryController controller, INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, "樣品瓶開蓋手臂 <Stopped>");
                // const int delay = 1000;
                // Log("delay " + delay + "ms (start)");
                Go(controller, 樣品瓶開蓋上下汽缸.Name, 樣品瓶開蓋上下汽缸.上升
                    , OnSampleBottleUncoverCylinderUpCompleted);
//                AddTimer(controller, _feedingMotorTimer, delay, OnDelay);
                SignalAndWait(controller, movableObject.Name, result);
            }

            //
            // private void OnDelay(SupervisoryController obj)
            // {
            //     Log("delay (end)");
            //     Go(obj, 樣品瓶蓋子夾爪汽缸.Name, 樣品瓶蓋子夾爪汽缸.打開, OnOpen);
            //     RemoveParticipants(1);
            // }
            //
            // private void OnOpen(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            // {
            //     Go(arg1, 樣品瓶蓋子夾爪汽缸.Name, 樣品瓶蓋子夾爪汽缸.夾持, OnClose);
            //     SignalAndWait(arg1, arg2.Name, arg3);
            // }
            //
            // private void OnClose(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            // {
            //     Go(arg1, 樣品瓶開蓋上下汽缸.Name, 樣品瓶開蓋上下汽缸.上升, OnSampleBottleUncoverCylinderUpCompleted);
            //     SignalAndWait(arg1, arg2.Name, arg3);
            // }
            //

            private void OnSampleBottleUncoverCylinderUpCompleted(SupervisoryController controller,
                INamedObject movableObject, CompletionResult result)
            {
                LogAction(result, movableObject, 樣品瓶開蓋上下汽缸.上升);
                Go(controller, 樣品瓶身體夾爪汽缸.Name, 樣品瓶身體夾爪汽缸.打開, On樣品瓶身體夾爪汽缸_打開);
                //            Go(controller, 樣品瓶蓋子夾爪汽缸.Name, 樣品瓶蓋子夾爪汽缸.打開, OnReach樣品瓶蓋子夾爪汽缸_打開);
                SignalAndWait(controller, movableObject.Name, result);
            }

            // private void OnReach樣品瓶蓋子夾爪汽缸_打開(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            // {
            //     LogAction(arg3, arg2, 樣品瓶蓋子夾爪汽缸.打開);
            //     SignalAndWait(arg1, arg2.Name, arg3);
            // }

            private void On樣品瓶身體夾爪汽缸_打開(SupervisoryController controller,
                INamedObject movableObject, CompletionResult result)
            {
                LogAction(result, movableObject, 樣品瓶身體夾爪汽缸.打開);
                Go(controller, Arm18X送料手臂.Name, Arm18X送料手臂.放樣品瓶, OnArm18Move);
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void OnArm18Move(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, Arm18X送料手臂.放樣品瓶);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action3(DiscardLiquidDirector);
            }
        }


        internal abstract class DiscardLiquidActor : DecompositionAction
        {
            public DiscardLiquidDirector DiscardLiquidDirector => (DiscardLiquidDirector) Director;

            protected DiscardLiquidActor(DiscardLiquidDirector director) : base(director)
            {
                Log("開始 => " + this.GetType().Name);
            }

            protected new bool Go(SupervisoryController controller, string movableObjectName, string position,
                Action<SupervisoryController, INamedObject, CompletionResult> completed)
            {
                Log(movableObjectName + "執行[" + position + "](try)");
                return base.Go(controller, movableObjectName, position, completed);
            }

            protected ConveyorDirector ConveyorDirector => (ConveyorDirector) Director;

            protected AutoRunContext AutoRunContext => ConveyorDirector.AutoRunContext;

            protected void Log(string c)
            {
                Console.WriteLine(DirectorName + @"(" + this.GetType().Name + @") " + c);
            }

            private static string DirectorName => @"[倒廢液] ";

            protected void Log(string format, params object[] c)
            {
                Console.WriteLine(
                    @"[" + DirectorName + @"] " + @"(" + this.GetType().Name + @") " + format + @" detail: {0}", c);
            }

            protected override DecompositionAction CreateNextAction()
            {
                try
                {
                    return GoToNext();
                }
                catch (Exception e)
                {
                    Log("failed and stop", e);
                    return null;
                }
            }

            protected abstract DecompositionAction GoToNext();

            protected void LogAction(CompletionResult objectName, INamedObject movableObjectName, string action)
            {
                if (objectName == CompletionResult.Success)
                {
                    Log(movableObjectName.Name + "執行[" + action + "](ok)");
                }
                else
                {
                    Log(movableObjectName.Name + "執行[" + action + "](failed)");
                }
            }

            protected void LogTry(CompletionResult objectName, INamedObject movableObjectName, string action)
            {
                if (objectName == CompletionResult.Success)
                {
                    Log(movableObjectName.Name + "執行[" + action + "](ok)");
                }
                else
                {
                    Log(movableObjectName.Name + "執行[" + action + "](failed)");
                }
            }
        }

        /// <summary>
        /// 上方手臂打算move to 放瓶區, 下一步打算取瓶子
        /// </summary>
        internal class Action3 : DiscardLiquidActor
        {
            public Action3(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                Go(controller, ARM16X取瓶移載手臂.Name, ARM16X取瓶移載手臂.放瓶, OnMove2放瓶);
                return Go(controller, ARM17X取瓶移載手臂.Name, ARM17X取瓶移載手臂.放瓶, OnArm172放瓶);
            }

            private void OnArm172放瓶(SupervisoryController arg1, INamedObject arg2,
                CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM16X取瓶移載手臂.放瓶);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnMove2放瓶(SupervisoryController arg1, INamedObject arg2,
                CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM17X取瓶移載手臂.放瓶);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action4(DiscardLiquidDirector);
            }
        }

        /// <summary>
        ///
        /// 把東西由放瓶區移動到放瓶區上方
        /// </summary>
        internal class Action4 : DiscardLiquidActor
        {
            public Action4(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, 移載夾爪汽缸.Name, 移載夾爪汽缸.打開, OnPrepare夾爪);
            }

            private void OnPrepare夾爪(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 移載夾爪汽缸.打開);
                Go(arg1, 上下移載汽缸.Name, 上下移載汽缸.下降, On上下移載汽缸下降完成);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void On上下移載汽缸下降完成(SupervisoryController controller, INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, 上下移載汽缸.下降);
                Go(controller, 移載夾爪汽缸.Name, 移載夾爪汽缸.夾持, On移載夾爪汽缸夾持);
                SignalAndWait(controller, movableObject.Name, result);
            }


            private void On移載夾爪汽缸夾持(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 移載夾爪汽缸.夾持);
                Go(arg1, 上下移載汽缸.Name, 上下移載汽缸.上升, On上下移載汽缸上升完成);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void On上下移載汽缸上升完成(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 上下移載汽缸.上升);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action5(DiscardLiquidDirector);
            }
        }


        /// <summary>
        /// 廢液夾爪預先打開, 取瓶子上方移往廢液上方
        /// </summary>
        internal class Action5 : DiscardLiquidActor
        {
            public Action5(DiscardLiquidDirector director) : base(director)
            {
            }


            public override bool Go(SupervisoryController controller)
            {
                Go(controller, 倒廢液夾爪汽缸.Name, 倒廢液夾爪汽缸.打開, On倒廢液夾爪汽缸_打開);
                Go(controller, ARM16X取瓶移載手臂.Name, ARM16X取瓶移載手臂.倒廢液, OnReachARM16X取瓶移載手臂_倒廢液);
                return Go(controller, ARM17X取瓶移載手臂.Name, ARM17X取瓶移載手臂.倒廢液, OnReachARM17X取瓶移載手臂_倒廢液);
            }


            private void OnReachARM17X取瓶移載手臂_倒廢液(SupervisoryController arg1, INamedObject arg2,
                CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM16X取瓶移載手臂.倒廢液);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnReachARM16X取瓶移載手臂_倒廢液(SupervisoryController arg1, INamedObject arg2,
                CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM17X取瓶移載手臂.倒廢液);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void On倒廢液夾爪汽缸_打開(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 倒廢液夾爪汽缸.打開);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action6(DiscardLiquidDirector);
            }
        }

        /// <summary>
        /// 把瓶子從移載夾爪上放入到廢液汽缸中
        /// </summary>
        internal class Action6 : DiscardLiquidActor
        {
            public Action6(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, 倒廢液夾爪汽缸.Name, 倒廢液夾爪汽缸.打開, OnA);
            }

            private void OnA(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 倒廢液夾爪汽缸.打開);
                Go(arg1, 上下移載汽缸.Name, 上下移載汽缸.下降, OnB);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnB(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                LogAction(result, movableObject, 上下移載汽缸.下降);
                Go(controller, 倒廢液夾爪汽缸.Name, 倒廢液夾爪汽缸.夾持, OnC);
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void OnC(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 倒廢液夾爪汽缸.夾持);
                Go(arg1, 移載夾爪汽缸.Name, 移載夾爪汽缸.打開, OnD);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnD(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 移載夾爪汽缸.打開);
                Go(arg1, 上下移載汽缸.Name, 上下移載汽缸.上升, On上下移載汽缸上升完成);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void On上下移載汽缸上升完成(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 上下移載汽缸.上升);
                SignalAndWait(arg1, arg2.Name, arg3);
            }


            protected override DecompositionAction GoToNext()
            {
                return new Action7(DiscardLiquidDirector);
            }
        }

        /// <summary>
        ///
        /// 翻轉到出廢水
        /// </summary>
        internal class Action7 : DiscardLiquidActor
        {
            private readonly OnceTimer _feedingMotorTimer = new OnceTimer();

            private const int 倒廢液停留時間Ms = 5000;

            public Action7(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, 倒廢液汽缸.Name, 倒廢液汽缸.反, On倒廢液汽缸_反);
            }

            private void On倒廢液汽缸_反(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 倒廢液汽缸.反);
                Log(倒廢液汽缸.Name + "停留" + 倒廢液停留時間Ms + "ms (開始)");
                AddTimer(arg1, _feedingMotorTimer, 倒廢液停留時間Ms,
                    controller =>
                    {
                        Go(controller, 倒廢液汽缸.Name, 倒廢液汽缸.正, On倒廢液汽缸_正);
                        RemoveParticipants(1);
                    }
                );
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void On倒廢液汽缸_正(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                Log(倒廢液汽缸.Name + "停留" + 倒廢液停留時間Ms + "ms (end)");
                SignalAndWait(arg1, arg2.Name, arg3);
            }


            protected override DecompositionAction GoToNext()
            {
                return new Action8(DiscardLiquidDirector);
            }

            /// <summary>
            ///
            /// 瓶子從倒廢水朝內往上移動
            /// </summary>
            internal class Action8 : DiscardLiquidActor
            {
                public Action8(DiscardLiquidDirector director) : base(director)
                {
                }

                public override bool Go(SupervisoryController controller)
                {
                    return Go(controller, 上下移載汽缸.Name, 上下移載汽缸.下降, OnB);
                }

                private void OnB(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
                {
                    LogAction(arg3, arg2, 上下移載汽缸.下降);
                    Go(arg1, 移載夾爪汽缸.Name, 移載夾爪汽缸.夾持, OnC);
                    SignalAndWait(arg1, arg2.Name, arg3);
                }

                private void OnC(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
                {
                    LogAction(arg3, arg2, 移載夾爪汽缸.夾持);
                    Go(arg1, 倒廢液夾爪汽缸.Name, 倒廢液夾爪汽缸.打開, OnD);
                    SignalAndWait(arg1, arg2.Name, arg3);
                }

                private void OnD(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
                {
                    LogAction(arg3, arg2, 倒廢液夾爪汽缸.打開);
                    Go(arg1, 上下移載汽缸.Name, 上下移載汽缸.上升, OnE);
                    SignalAndWait(arg1, arg2.Name, arg3);
                }

                private void OnE(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
                {
                    SignalAndWait(arg1, arg2.Name, arg3);
                }

                protected override DecompositionAction GoToNext()
                {
                    return new Action9(DiscardLiquidDirector);
                }
            }
        }

        /// <summary>
        /// 將倒廢棄水區上方的夾瓶子移往放瓶區
        /// </summary>
        internal class Action9 : DiscardLiquidActor
        {
            public Action9(DiscardLiquidDirector discardLiquidDirector) : base(discardLiquidDirector)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                Go(controller, ARM16X取瓶移載手臂.Name, ARM16X取瓶移載手臂.放瓶, OnArm162放瓶);
                return Go(controller, ARM17X取瓶移載手臂.Name, ARM17X取瓶移載手臂.放瓶, OnArm172放瓶);
            }


            private void OnArm172放瓶(SupervisoryController arg1, INamedObject arg2,
                CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM16X取瓶移載手臂.放瓶);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnArm162放瓶(SupervisoryController arg1, INamedObject arg2,
                CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM17X取瓶移載手臂.放瓶);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action10(DiscardLiquidDirector);
            }
        }

        /// <summary>
        /// 從台車廢棄區上方放到台車上
        /// </summary>
        internal class Action10 : DiscardLiquidActor
        {
            public Action10(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, 上下移載汽缸.Name, 上下移載汽缸.下降, On上下移載汽缸下降完成);
            }

            private void On上下移載汽缸下降完成(SupervisoryController controller, INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, 上下移載汽缸.下降);
                Go(controller, 移載夾爪汽缸.Name, 移載夾爪汽缸.打開, On移載夾爪汽缸已打開);
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void On移載夾爪汽缸已打開(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 移載夾爪汽缸.打開);
                Go(arg1, 上下移載汽缸.Name, 上下移載汽缸.上升, On上下移載汽缸上升完成);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void On上下移載汽缸上升完成(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 上下移載汽缸.上升);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action11(DiscardLiquidDirector);
            }
        }

        /// <summary>
        ///往鎖開闔蓋區送, 並取準備鎖瓶子的動作, 下一個actor準備鎖住瓶子
        /// </summary>
        internal class Action11 : DiscardLiquidActor
        {
            public Action11(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, Arm18X送料手臂.Name, Arm18X送料手臂.開蓋, OnReach開蓋);
            }

            private void OnReach開蓋(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, Arm18X送料手臂.開蓋);
                Go(arg1, 樣品瓶身體夾爪汽缸.Name, 樣品瓶身體夾爪汽缸.夾持, OnReach樣品瓶身體夾爪汽缸_夾持);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnReach樣品瓶身體夾爪汽缸_夾持(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 樣品瓶身體夾爪汽缸.夾持);
                Go(arg1, 樣品瓶開蓋上下汽缸.Name, 樣品瓶開蓋上下汽缸.下降, OnReach樣品瓶開蓋上下汽缸_下);
                SignalAndWait(arg1, arg2.Name, arg3);
            }


            private void OnReach樣品瓶開蓋上下汽缸_下(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action12(DiscardLiquidDirector);
            }
        }


        /// <summary>
        /// 執行鎖瓶子, 以及夾爪上升
        /// </summary>
        private sealed class Action12 : DiscardLiquidActor
        {
            private int _originalAngle;
            private bool _coverCompleted;

            public Action12(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController ctrl)
            {
                AutomatedDispensingController controller = ctrl as AutomatedDispensingController;

                _originalAngle = controller.ARMs[8].IOMemory.CurrentFeedValue;

                Console.WriteLine("OriginalAngle: {0}", _originalAngle);

                return Go(controller, ARM9, "JOGR", ARM9_Θ_樣品瓶開蓋手臂已停止);
            }

            public override DecompositionAction Poll(SupervisoryController ctrl)
            {
                AutomatedDispensingController controller = ctrl as AutomatedDispensingController;

                if (!_coverCompleted)
                {
                    int angle = controller.ARMs[8].IOMemory.CurrentFeedValue;

                    if (angle < (_originalAngle - 360))
                    {
                        int torque = controller.ARMs[8].IOMemory.CurrentTorque;

                        if (torque > 1600)
                        {
                            Console.WriteLine("Torque: {0}", torque);

                            _coverCompleted = true;
                            controller.ARMs[8].StopJog(ctrl);
                        }
                    }
                }

                return base.Poll(controller);
            }

            private void ARM9_Θ_樣品瓶開蓋手臂已停止(SupervisoryController controller, INamedObject movableObject,
                CompletionResult result)
            {
                Log("ARM9 Θ 樣品瓶開蓋手臂 <Stopped> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result,
                    (controllerArg) => Go(controllerArg, 樣品瓶蓋子夾爪汽缸.Name,
                        樣品瓶蓋子夾爪汽缸.打開, 樣品瓶蓋子夾爪汽缸已打開));
            }

            private void 樣品瓶蓋子夾爪汽缸已打開(SupervisoryController controller, INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, 樣品瓶蓋子夾爪汽缸.打開);
                SignalAndWait(controller, movableObject.Name, result, (controllerArg) =>
                {
                    Go(controllerArg, 樣品瓶開蓋上下汽缸.Name, 樣品瓶開蓋上下汽缸.上升, 樣品瓶開蓋上下汽缸上升完成);
                    Go(controllerArg, 樣品瓶身體夾爪汽缸.Name, 樣品瓶身體夾爪汽缸.打開, 樣品瓶身體夾爪汽缸已打開);
                });
            }

            private void 樣品瓶開蓋上下汽缸上升完成(SupervisoryController controller, INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, 樣品瓶開蓋上下汽缸.上升);
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void 樣品瓶身體夾爪汽缸已打開(SupervisoryController controller, INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, 樣品瓶身體夾爪汽缸.打開);
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action13(DiscardLiquidDirector);
            }
        }

        /// <summary>
        /// 台車和移載手臂在放瓶區匯合
        /// </summary>
        private sealed class Action13 : DiscardLiquidActor
        {
            public Action13(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                var a = Go(controller, Arm18X送料手臂.Name, Arm18X送料手臂.放樣品瓶,
                    OnArm18Move
                );
                var b = Go(controller, ARM16X取瓶移載手臂.Name, ARM16X取瓶移載手臂.放瓶, OnArm162放瓶);
                var c = Go(controller, ARM17X取瓶移載手臂.Name, ARM17X取瓶移載手臂.放瓶, OnArm172放瓶);
                return a && b && c;
            }

            private void OnArm172放瓶(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM16X取瓶移載手臂.放瓶);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnArm162放瓶(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM17X取瓶移載手臂.放瓶);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnArm18Move(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, Arm18X送料手臂.放樣品瓶);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action14(DiscardLiquidDirector);
            }
        }


        /// <summary>
        /// 在放瓶區發生了, 移載手臂從台車上取瓶子至上方
        /// </summary>
        internal class Action14 : DiscardLiquidActor
        {
            public Action14(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, 移載夾爪汽缸.Name, 移載夾爪汽缸.打開, On移載夾爪汽缸已打開);
            }

            private void OnA(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, Arm18X送料手臂.放樣品瓶);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void On上下移載汽缸下降完成(SupervisoryController controller, INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, 上下移載汽缸.下降);
                Go(controller, 移載夾爪汽缸.Name, 移載夾爪汽缸.夾持, On移載夾爪汽缸夾持);
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void On移載夾爪汽缸已打開(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 移載夾爪汽缸.打開);
                Go(arg1, 上下移載汽缸.Name, 上下移載汽缸.下降, On上下移載汽缸下降完成);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void On移載夾爪汽缸夾持(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 移載夾爪汽缸.夾持);
                Go(arg1, 上下移載汽缸.Name, 上下移載汽缸.上升, On上下移載汽缸上升完成);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void On上下移載汽缸上升完成(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 上下移載汽缸.上升);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                return new Action15(DiscardLiquidDirector);
            }
        }

        /// <summary>
        /// 移載手臂從放瓶前往回收區
        /// </summary>
        private sealed class Action15 : DiscardLiquidActor
        {
            public Action15(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                var b = Go(controller, ARM16X取瓶移載手臂.Name, ARM16X取瓶移載手臂.回收區, OnArm16Move);
                var c = Go(controller, ARM17X取瓶移載手臂.Name, ARM17X取瓶移載手臂.回收區, OnArm17Move);
                return b && c;
            }

            private void OnArm17Move(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM16X取瓶移載手臂.回收區);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnArm16Move(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, ARM17X取瓶移載手臂.回收區);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                return new ActionFinal(DiscardLiquidDirector);
            }
        }

        internal class ActionFinal : DiscardLiquidActor
        {
            public ActionFinal(DiscardLiquidDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, 上下移載汽缸.Name, 上下移載汽缸.下降, On上下移載汽缸下降完成);
            }

            private void OnA(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, Arm18X送料手臂.放樣品瓶);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void On上下移載汽缸下降完成(SupervisoryController controller, INamedObject movableObject,
                CompletionResult result)
            {
                LogAction(result, movableObject, 上下移載汽缸.下降);
                Go(controller, 移載夾爪汽缸.Name, 移載夾爪汽缸.打開, On移載夾爪汽缸已打開);
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void On移載夾爪汽缸已打開(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 移載夾爪汽缸.打開);
                Go(arg1, 上下移載汽缸.Name, 上下移載汽缸.上升, OnUp);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            private void OnUp(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                LogAction(arg3, arg2, 上下移載汽缸.上升);
                SignalAndWait(arg1, arg2.Name, arg3);
            }

            protected override DecompositionAction GoToNext()
            {
                if (DiscardLiquidDirector.DisCardLiquidContext == null)
                {
                    return FinalMessage();
                }

                DiscardLiquidDirector.DisCardLiquidContext.NumOfBottom -= 1;
                if (DiscardLiquidDirector.DisCardLiquidContext.Stop)
                {
                    return FinalMessage();
                }
                else
                {
                    if (DiscardLiquidDirector.DisCardLiquidContext.NumOfBottom > 0)
                    {
                        Log("下一個處理廢棄物準備該使,剩餘:" + DiscardLiquidDirector.DisCardLiquidContext.NumOfBottom);
                        return new Action1(DiscardLiquidDirector, false);
                    }
                    else
                    {
                        return FinalMessage();
                    }
                }
            }

            private DecompositionAction FinalMessage()
            {
                Log("倒廢液程序結束");
                DiscardLiquidDirector?.OnComplete(DiscardLiquidDirector);
                return null;
            }
        }
    }
}