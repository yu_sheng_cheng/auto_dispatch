﻿using System;
using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;

namespace AutomatedDispensing.Controller.Modules.BOM2.Ml1Dispatch
{
    public partial class Dispatch1MDirector
    {
        internal class Action3 : DecompositionAction
        {
            private readonly OnceTimer _feedingMotorTimer = new OnceTimer();

            private volatile bool _totalSuccess = true;

            private const string Action取拋棄式Tip = "取拋棄式Tip";

            public Action3(ContinuousActionDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                TryLog(Arm11 , Action取拋棄式Tip);
                return Go(controller, Arm11, Action取拋棄式Tip, _OnArm11_取拋棄式Tip);
            }


            private void _OnArm11_取拋棄式Tip(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                if (IsSuccessAction(arg3))
                {
                    OkLog(Arm11, Action取拋棄式Tip);
                    TryLog(Arm14, Action取拋棄式Tip);
                    Go(arg1, Arm14, Action取拋棄式Tip, _OnArm14取拋棄式Tip);
                    SignalAndWait(arg1, Arm11, arg3);
                }
                else
                {
                    FailedLog(Arm11, Action取拋棄式Tip);
                    _totalSuccess = false;
                }
            }

            private void _OnArm14取拋棄式Tip(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                if (IsSuccessAction(arg3))
                {
                    OkLog(Arm14, Action取拋棄式Tip);
                    TryLog(Arm11, StandByAction);
                    Go(arg1, Arm11, StandByAction, _OnArm11StandBy);
                    SignalAndWait(arg1, Arm11, arg3);
                }
                else
                {
                    FailedLog(Arm14, Action取拋棄式Tip);
                    _totalSuccess = false;
                }
            }

            private  void _OnArm11StandBy(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                if (IsSuccessAction(arg3))
                {
                    OkLog(Arm11, StandByAction);
                    SignalAndWait(arg1, Arm11, arg3);
                }
                else
                {
                    FailedLog(Arm11, StandByAction);
                }
            }

            protected override DecompositionAction CreateNextAction()
            {
                if (_totalSuccess)
                {
                    return new Action4(Director);
                }
                else
                {
                    return new RecoverCaptor(Director);
                }
            }
        }
    }
}