﻿using Automation.Core.Controller;
using Automation.Core.Messages.ManualControl.OneCycle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.BOM2.Pipette1mL
{
    class Pipette1mlOneCycle : OneCycleManualControlMessage
    {
        private readonly Pipette1mlDirector _director = new Pipette1mlDirector();

        public override bool IsCompleted { get { return _director.IsCompleted; } }

        public Pipette1mlOneCycle() : base("1mL Pipette動作一次") { }

        public override bool Go(SupervisoryController controller)
        {
            if (!_director.Go(controller))
            {
                Complete(_director.Result);
                return false;
            }
            return true;
        }

        public override void Poll(SupervisoryController controller)
        {
            _director.Poll(controller);

            if (_director.IsCompleted)
                Complete(_director.Result);
        }
    }
}
