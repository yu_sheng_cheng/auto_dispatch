﻿using System;
using AutomatedDispensing.Controller.Modules.AutomatedDispensing.parallel.conveyor;
using AutomatedDispensing.Controller.Modules.context;
using Automation.Core.Controller;
using Automation.Core.Messages.ManualControl.OneCycle;

namespace AutomatedDispensing.Controller.Modules.BOM2.DiscardWasteLiquid
{
    internal class TestDiscardLiquidOneCycle : OneCycleManualControlMessage
    {
        private readonly DiscardLiquidDirector _discardLiquidDirector;
        private readonly AutoRunContext _autoRunContext;
        private readonly ConveyorDirector _conveyorDirector;


        private void Log(string s)
        {
            Console.WriteLine(this.GetType().Name + @" : " + s);
        }

        private void Log(string s, params object[] e)
        {
            Console.WriteLine(this.GetType().Name + @" : " + s + @"{0}", e);
        }

        public override bool IsCompleted => _discardLiquidDirector.IsCompleted;

        public TestDiscardLiquidOneCycle() : base("test丟廢液體動作一次")
        {
            _autoRunContext = new AutoRunContext();


            _discardLiquidDirector = new DiscardLiquidDirector("test丟廢液體")
            {
                DisCardLiquidContext = new DisCardLiquidContext {NumOfBottom = 1},
                OnComplete = director =>
                {
                    _autoRunContext.AutoRunCompleted = true;
                    return true;
                }
            };
            _conveyorDirector = new ConveyorDirector(_autoRunContext);
        }

        public override bool Go(SupervisoryController controller)
        {
            if (!_discardLiquidDirector.Go(controller))
            {
                Complete(_discardLiquidDirector.Result);
                Log("輸送帶執行失敗");
                _autoRunContext.AutoRunCompleted = true;
                return false;
            }


            if (!_conveyorDirector.Go(controller))
            {
                Complete(_conveyorDirector.Result);
                Log("輸送帶執行失敗");
                return false;
            }

            return true;
        }

        public override void Poll(SupervisoryController controller)
        {
            void PollDiscardD()
            {
                _discardLiquidDirector.Poll(controller);
                if (_discardLiquidDirector.IsCompleted)
                {
                    _autoRunContext.AutoRunCompleted = true;
                    Log("conveyor 結束");
                }
            }

            void PollConveyorD()
            {
                _conveyorDirector.Poll(controller);
                if (_conveyorDirector.IsCompleted)
                {
                    Complete(_conveyorDirector.Result);
                    Log("結束");
                }
            }

            try
            {
                PollDiscardD();
                PollConveyorD();
            }
            catch (Exception e)
            {
                Log("director crash", e);
            }
        }
    }
}