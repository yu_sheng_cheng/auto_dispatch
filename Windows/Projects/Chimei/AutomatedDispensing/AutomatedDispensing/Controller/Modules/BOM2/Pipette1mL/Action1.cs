﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.BOM2.Pipette1mL
{
    partial class Pipette1mlDirector
    {
        private sealed class Action1 : DecompositionAction
        {
            public Action1(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "配藥移載手臂", "1ml反覆用Tip Tray盤 [1]", 配藥移載手臂已到1mL反覆用TipTray盤);
            }

            private void 配藥移載手臂已到1mL反覆用TipTray盤(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("配藥移載手臂 <1ml反覆用Tip Tray盤 [1]> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "ARM11 Z1 1ml取TIP手臂", "取反覆用Tip", ARM11_Z1_1mL取TIP手臂已經可以取反覆用Tip));
            }

            private void ARM11_Z1_1mL取TIP手臂已經可以取反覆用Tip(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM11 Z1 1ml取TIP手臂 <取反覆用Tip> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "ARM14 Z1 1ml移液手臂", "取反覆用Tip", ARM14_Z1_1mL移液手臂已取到反覆用Tip));
            }

            private void ARM14_Z1_1mL移液手臂已取到反覆用Tip(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM14 Z1 1ml移液手臂 <取反覆用Tip> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "ARM11 Z1 1ml取TIP手臂", "Standby", ARM11_Z1_1mL取TIP手臂已經在Standby位置));
            }

            private void ARM11_Z1_1mL取TIP手臂已經在Standby位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM11 Z1 1ml取TIP手臂 <Standby> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "配藥移載手臂", "1ml藥劑取液", 配藥移載手臂已到1mL藥劑取液));
            }

            private void 配藥移載手臂已到1mL藥劑取液(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("配藥移載手臂 <1ml藥劑取液> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "1ml接漏液汽缸", "伸出", 接1mL漏液汽缸已伸出));
            }

            private void 接1mL漏液汽缸已伸出(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("1ml接漏液汽缸 <伸出> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "配藥移載手臂", "1ml天秤滴液", 配藥移載手臂已到1mL天秤滴液));
            }

            private void 配藥移載手臂已到1mL天秤滴液(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("配藥移載手臂 <1ml天秤滴液> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "微量天秤遮風罩汽缸", "打開", 微量天秤遮風罩汽缸已打開));
            }

            private void 微量天秤遮風罩汽缸已打開(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("微量天秤遮風罩汽缸 <打開> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action2(Director);
            }
        }
    }
}
