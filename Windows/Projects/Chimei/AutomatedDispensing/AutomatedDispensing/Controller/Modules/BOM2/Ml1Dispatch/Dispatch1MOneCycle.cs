﻿using System;
using Automation.Core.Controller;
using Automation.Core.Messages.ManualControl.OneCycle;
using AutomatedDispensing.Controller.Modules.AutomatedDispensing.parallel.conveyor;
using AutomatedDispensing.Controller.Modules.context;

namespace AutomatedDispensing.Controller.Modules.BOM2.Ml1Dispatch
{
    class Dispatch1MOneCycle : OneCycleManualControlMessage
    {
        //private readonly DiscardLiquidDirector _director;
        private readonly ConveyorDirector _conveyorDirector;
        private readonly AutoRunContext _autoRunContext;


        private void Log(string s)
        {
            Console.WriteLine(this.GetType().Name + @" : " + s);
        }

        private void Log(string s, params object[] e)
        {
            Console.WriteLine(this.GetType().Name + @" : " + s + @"{0}", e);
        }

        public override bool IsCompleted => _conveyorDirector.IsCompleted;

        public Dispatch1MOneCycle() : base("1mlTip動作一次")
        {
          //  _director = new DiscardLiquidDirector();
            _autoRunContext = new AutoRunContext();
            _conveyorDirector = new ConveyorDirector(_autoRunContext);
        }

        public override bool Go(SupervisoryController controller)
        {
            // if (!_director.Go(controller))
            // {
            //     Complete(_director.Result);PostMessage
            //     Log("OnCycle失敗");
            //     return false;
            // }；

            if (!_conveyorDirector.Go(controller))
            {
                Complete(_conveyorDirector.Result);
                Log("輸送帶執行失敗");
                return false;
            }

            return true;
        }

        public override void Poll(SupervisoryController controller)
        {
            void PollConveyor()
            {
                _conveyorDirector.Poll(controller);
                if (_conveyorDirector.IsCompleted)
                {
                    Complete(_conveyorDirector.Result);
                }
            }

            // void PollMainDirector()
            // {
            //     _director.Poll(controller);
            //     if (_director.IsCompleted)
            //     {
            //         Complete(_director.Result);
            //         _autoRunContext.AutoRunCompleted = true;
            //     }
            // }

            // PollMainDirector();
            try
            {
                PollConveyor();
            }
            catch (Exception e)
            {
                Log("director crash", e);
            }
        }
    }
}