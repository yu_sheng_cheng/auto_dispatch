﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.BOM2.Pipette1mL
{
    partial class Pipette1mlDirector
    {
        private sealed class Action3 : DecompositionAction
        {
            public Action3(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "配藥移載手臂", "1ml反覆用Tip Tray盤 [1]", 配藥移載手臂已到1mL反覆用TipTray盤);
            }

            private void 配藥移載手臂已到1mL反覆用TipTray盤(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("配藥移載手臂 <1ml反覆用Tip Tray盤 [1]> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "1ml接漏液汽缸", "縮回", 接1mL漏液汽缸已縮回));
            }

            private void 接1mL漏液汽缸已縮回(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("1ml接漏液汽缸 <縮回> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "ARM11 Z1 1ml取TIP手臂", "放反覆用Tip", ARM11_Z1_1mL取TIP手臂已經可以放反覆用Tip));
            }

            private void ARM11_Z1_1mL取TIP手臂已經可以放反覆用Tip(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM11 Z1 1ml取TIP手臂 <放反覆用Tip> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => Go(controller_, "ARM14 Z1 1ml移液手臂", "丟棄Tip", ARM14_Z1_1mL移液手臂已放開Tip));
            }

            private void ARM14_Z1_1mL移液手臂已放開Tip(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM14 Z1 1ml移液手臂 <丟棄Tip> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) => {
                    if (!Go(controller_, "ARM14 Z1 1ml移液手臂", "Standby", ARM14_Z1_1mL移液手臂已在Standby位置))
                        return;

                    Go(controller_, "ARM11 Z1 1ml取TIP手臂", "Standby", ARM11_Z1_1mL取TIP手臂已在Standby位置);
                });
            }

            private void ARM14_Z1_1mL移液手臂已在Standby位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM14 Z1 1ml移液手臂 <Standby> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void ARM11_Z1_1mL取TIP手臂已在Standby位置(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("ARM11 Z1 1ml取TIP手臂 <Standby> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action4(Director);
            }
        }
    }
}
