﻿using Automation.Core.FSM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.BOM2.Pipette1mL
{
    sealed partial class Pipette1mlDirector: ContinuousActionDirector
    {
        public Pipette1mlDirector() : base("1mLPipette測試")
        {

        }

        protected override DecompositionAction CreateFirstAction()
        {
            return new Action1(this);
        }
    }
}
