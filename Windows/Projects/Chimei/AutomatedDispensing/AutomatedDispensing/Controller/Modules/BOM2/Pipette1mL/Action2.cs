﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.BOM2.Pipette1mL
{
    partial class Pipette1mlDirector
    {
        private sealed class Action2 : DecompositionAction
        {
            public Action2(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "1ml接漏液汽缸", "縮回", 接1mL漏液汽缸已縮回);
            }

            private void 接1mL漏液汽缸已縮回(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("1ml接漏液汽缸 <縮回> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result, (controller_) =>
                {
                    if (!Go(controller_, "1ml接漏液汽缸", "伸出", 接1mL漏液汽缸已伸出))
                        return;

                    Go(controller_, "微量天秤遮風罩汽缸", "關閉", 微量天秤遮風罩汽缸已關閉);
                });
            }

            private void 接1mL漏液汽缸已伸出(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("1ml接漏液汽缸 <伸出> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            private void 微量天秤遮風罩汽缸已關閉(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("微量天秤遮風罩汽缸 <關閉> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action3(Director);
            }
        }
    }
}
