﻿using System;
using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;

namespace AutomatedDispensing.Controller.Modules.BOM2.Ml1Dispatch
{
    public partial class Dispatch1MDirector
    {
        internal class RecoverCaptor : DecompositionAction
        {
            private readonly OnceTimer _feedingMotorTimer = new OnceTimer();


            public RecoverCaptor(ContinuousActionDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                Console.WriteLine(@"回復原本位置:" + Arm5 + @"," + Arm6 + @"," + Arm14 + @"," + Arm14 + @" has done standby");
                Go(controller, Arm11, "Standby", _OnArm11_待機);
                Go(controller, Arm5, "Standby", _OnArm5待機);
                Go(controller, Arm14, "Standby", _OnArm14待機);
                Go(controller, Arm6, "Standby", _OnArm6待機);
                Console.WriteLine(@"回復原本位置:" + Arm5 + @"," + Arm6 + @"," + Arm14 + @"," + Arm14 + @" has done standby");
                return true;
            }

            private void _OnArm6待機(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                SignalAndWait(arg1, Arm6, CompletionResult.Success);
            }

            private void _OnArm11_待機(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                SignalAndWait(arg1, Arm11, CompletionResult.Success);
            }


            private void _OnArm14待機(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                SignalAndWait(arg1, Arm14, CompletionResult.Success);
            }

            private void _OnArm5待機(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                SignalAndWait(arg1, Arm5, CompletionResult.Success);
            }
        }
    }
}