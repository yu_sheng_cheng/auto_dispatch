﻿using System;
using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;
using ControlzEx.Standard;

namespace AutomatedDispensing.Controller.Modules.BOM2.Ml1Dispatch
{
    public partial class Dispatch1MDirector
    {
        internal class Action2 : DecompositionAction
        {
            private readonly OnceTimer _feedingMotorTimer = new OnceTimer();

            private volatile bool _totalSuccess = true;

            public Action2(ContinuousActionDirector director) : base(director)
            {
            }


            public override bool Go(SupervisoryController controller)
            {
                //                Go(controller, Arm11, "取反覆用Tip", _OnArm11_取反覆用Tip);

                Console.WriteLine(Arm5+ @"移動到1ml拋棄式Tip Tray盤(try)");
                Go(controller, Arm5, Action, _onArm51ml拋棄式Tip_Tray盤);
                Console.WriteLine(Arm6+ @"移動到1ml拋棄式Tip Tray盤(try)");
                Go(controller, Arm6, Action, _onArm61ml拋棄式Tip_Tray盤);
                return true;
            }

            private static string Action = "1ml拋棄式Tip Tray盤 [1]";

            private void _onArm51ml拋棄式Tip_Tray盤(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                if (IsSuccessAction(arg3))
                {
                    OkLog(Arm5, Action);
                }
                else
                {
                    FailedLog(Arm5, Action);
                    _totalSuccess = false;
                }

                SignalAndWait(arg1, Arm5, arg3);
            }

            private void _onArm61ml拋棄式Tip_Tray盤(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                if (IsSuccessAction(arg3))
                {
                    OkLog(Arm6, Action);
                }
                else
                {
                    FailedLog(Arm6, Action);
                    _totalSuccess = false;
                }

                SignalAndWait(arg1, Arm5, arg3);
            }


            protected override DecompositionAction CreateNextAction()
            {
                if (_totalSuccess)
                {
                    return new Action3(Director);
                }
                else
                {
                    return new RecoverCaptor(Director);
                }
            }
        }
    }
}