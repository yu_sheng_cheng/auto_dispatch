﻿using System;
using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;

namespace AutomatedDispensing.Controller.Modules.BOM2.Ml1Dispatch
{
    public partial class Dispatch1MDirector
    {
        internal class Action6 : DecompositionAction
        {
            private readonly OnceTimer _feedingMotorTimer = new OnceTimer();

            private volatile bool _totalSuccess = true;

            private const string TargetLocationName = "丟棄1ml Tip";

            public Action6(ContinuousActionDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {

                TryLog(Arm5, TargetLocationName);
                Go(controller, Arm5, TargetLocationName, _onArm5MoveCallback);
                //                Go(controller, Arm5, "1ml台車取液2", _onArm51ml台車取液2);
                TryLog(Arm6, TargetLocationName);
                Go(controller, Arm6, TargetLocationName, _onArm6MoveCallback);
                return true;
            }

            private void _onArm5MoveCallback(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                if (IsSuccessAction(arg3))
                {
                    OkLog(Arm5, TargetLocationName);
               
                }
                else
                {
                    FailedLog(Arm5, TargetLocationName);
                    _totalSuccess = false;
                }

                SignalAndWait(arg1, Arm5, arg3);
            }

            private void _onArm6MoveCallback(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                if (IsSuccessAction(arg3))
                {
                    OkLog(Arm6, TargetLocationName);
                }
                else
                {
                    FailedLog(Arm6, TargetLocationName);
                    _totalSuccess = false;
                }

                SignalAndWait(arg1, Arm5, arg3);
            }

            protected override DecompositionAction CreateNextAction()
            {
                if (_totalSuccess)
                {
                    return new Action7(Director);
                }
                else
                {
                    return new RecoverCaptor(Director);
                }
            }
        }
    }
}