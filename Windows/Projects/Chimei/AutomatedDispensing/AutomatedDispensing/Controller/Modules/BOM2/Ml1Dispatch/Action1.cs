﻿using System;
using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;
using ControlzEx.Standard;

namespace AutomatedDispensing.Controller.Modules.BOM2.Ml1Dispatch
{
    public partial class Dispatch1MDirector : ContinuousActionDirector
    {
        private const string Arm5 = "ARM5 X 配藥移載手臂";

        private const string Arm6 = "ARM6 Y 配藥移載手臂";

        private const string Arm14 = "ARM14 Z1 1ml移液手臂";

        private const string Arm11 = "ARM11 Z1 1ml取TIP手臂";

        private const string StandByAction = "Standby";

        public Dispatch1MDirector() : base("1mlTip測試") { }

        private static bool IsSuccessAction(CompletionResult c)
        {
            return c == CompletionResult.Success;
        }

        internal class Action1 : DecompositionAction
        {
            private readonly OnceTimer _feedingMotorTimer = new OnceTimer();


            public Action1(ContinuousActionDirector director) : base(director)
            {
            }

            public override bool Go(SupervisoryController controller)
            {
                Console.WriteLine(Arm5 + @"," + Arm6 + @"," + Arm14 + @"," + Arm14 + @" has done standby");
                Go(controller, Arm11, "Standby", _OnArm11_待機);
                Go(controller, Arm5, "Standby", _OnArm5待機);
                Go(controller, Arm14, "Standby", _OnArm14待機);
                Go(controller, Arm6, "Standby", _OnArm6待機);
                Console.WriteLine(Arm5 + @"," + Arm6 + @"," + Arm14 + @"," + Arm14 + @" has done standby");
                return true;
            }

            private void _OnArm6待機(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                SignalAndWait(arg1, Arm6, CompletionResult.Success);
            }

            private void _OnArm11_待機(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                SignalAndWait(arg1, Arm11, CompletionResult.Success);
            }


            private void _OnArm14待機(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                SignalAndWait(arg1, Arm14, CompletionResult.Success);
            }

            private void _OnArm5待機(SupervisoryController arg1, INamedObject arg2, CompletionResult arg3)
            {
                SignalAndWait(arg1, Arm5, CompletionResult.Success);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action2(Director);
            }
        }

        protected override DecompositionAction CreateFirstAction()
        {
            return new Action1(this);
        }

        private static void TryLog(string deviceName, string action)
        {
            Console.WriteLine(deviceName + @"執行" + action + @"(try)");
        }

        private static void FailedLog(string deviceName, string action)
        {
            Console.WriteLine(deviceName + @"執行" + action + @"(failed)");
        }

        private static void OkLog(string deviceName, string action)
        {
            Console.WriteLine(deviceName + @"執行" + action + @"(ok)");
        }

        public Dispatch1MDirector(string name) : base(name)
        {
        }
    }
}