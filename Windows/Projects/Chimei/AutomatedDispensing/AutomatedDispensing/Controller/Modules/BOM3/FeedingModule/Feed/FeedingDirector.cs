﻿using Automation.Core.FSM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.BOM3.FeedingModule.Feed
{
    sealed partial class FeedingDirector : ContinuousActionDirector
    {
        public FeedingDirector() : base("入料模組<入料>")
        {

        }

        protected override DecompositionAction CreateFirstAction()
        {
            return new Action1(this);
        }
    }
}
