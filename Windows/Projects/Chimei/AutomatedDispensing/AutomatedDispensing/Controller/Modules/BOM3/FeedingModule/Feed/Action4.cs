﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.BOM3.FeedingModule.Feed
{
    partial class FeedingDirector
    {
        private sealed class Action4 : DecompositionAction
        {
            public Action4(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "擋瓶汽缸2", "伸出", OnBottleBlockCylinder2ReachOutCompleted);
            }

            private void OnBottleBlockCylinder2ReachOutCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("擋瓶汽缸2 <伸出> : {0}", result.ToString());
                //SignalAndWait(controller, movableObject.Name, result, (con) => Go(con, "擋瓶汽缸1", "退回", OnBottleBlockCylinder1RetractCompleted));
            }
        }
    }
}
