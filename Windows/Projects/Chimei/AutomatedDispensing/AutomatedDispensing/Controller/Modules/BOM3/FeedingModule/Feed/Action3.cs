﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.BOM3.FeedingModule.Feed
{
    partial class FeedingDirector
    {
        private sealed class Action3 : DecompositionAction
        {
            private bool hasBottle = false;

            public Action3(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                AddParticipants(1);
                return true;
            }

            public override DecompositionAction Poll(SupervisoryController controller)
            {
                if (!hasBottle)
                {
                    if (controller.IORegister.GetContact(AutomatedDispensingController.FeedingWaitBottlePosition2Input))
                    {
                        Console.WriteLine("入料等瓶位置2: 有瓶");

                        hasBottle = true;

                        RemoveParticipants(1);
                    }
                }

                return base.Poll(controller);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action4(Director);
            }
        }
    }
}
