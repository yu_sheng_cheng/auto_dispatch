﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using Automation.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Modules.BOM3.FeedingModule.Feed
{
    partial class FeedingDirector
    {
        private sealed class Action2 : DecompositionAction
        {
            private readonly OnceTimer _feedingMotorTimer = new OnceTimer();

            public Action2(ContinuousActionDirector director) : base(director) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "擋瓶汽缸2", "退回", OnBottleBlockCylinder2RetractCompleted))
                    return false;

                controller.IORegister.SetCoil(AutomatedDispensingController.FeedingMotorOutput, true);
                AddTimer(controller, _feedingMotorTimer, 100, OnFeedingMotorStarted);
                return true;
            }

            private void OnFeedingMotorStarted(SupervisoryController controller)
            {
                Console.WriteLine("入料馬達 <運轉>");
                SignalAndWait(controller, "入料馬達", CompletionResult.Success);
            }

            private void OnBottleBlockCylinder2RetractCompleted(SupervisoryController controller, INamedObject movableObject, CompletionResult result)
            {
                Console.WriteLine("擋瓶汽缸2 <退回> : {0}", result.ToString());
                SignalAndWait(controller, movableObject.Name, result);
            }

            protected override DecompositionAction CreateNextAction()
            {
                return new Action3(Director);
            }
        }
    }
}
