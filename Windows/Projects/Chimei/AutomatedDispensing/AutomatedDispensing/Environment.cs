﻿using AutomatedDispensing.Model;
using SwissKnife;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomatedDispensing.ViewModel;
using AutomatedDispensing.ViewModel.MovableObjects;
using SwissKnife.Exceptions;
using Unity;

namespace AutomatedDispensing
{
    static class Environment
    {
        public static IUnityContainer UnityContainer { get; set; }

        public static string EquipmentProfileRootFolder { get; set; }

        public static string StringResourceRootFolder { get; set; }

        public static IniParser PositionIni { get; set; }

        public static PhysicalRegisterWatcher PhysicalRegisterWatcher { get; set; }


        public static XYLinearMotionViewModel EmptyBottleTransferViewModel { get; set; }

        public static XYLinearMotionViewModel DispensingTransferViewModel { get; set; }

        public static XYLinearMotionViewModel BottlePickupTransferViewModel { get; set; }

        private static readonly IDictionary<string, CylinderViewModel> CylinderViewModels = new Dictionary<string, CylinderViewModel>(StringComparer.OrdinalIgnoreCase);
        private static readonly IDictionary<string, ServoDriveViewModel> ServoDriveViewModels = new Dictionary<string, ServoDriveViewModel>(StringComparer.OrdinalIgnoreCase);

        public static void AddCylinderViewModel(CylinderViewModel viewModel)
        {
            CylinderViewModels.Add(viewModel.Name, viewModel);
        }

        public static CylinderViewModel GetCylinderViewModel(string name)
        {
            if (!CylinderViewModels.TryGetValue(name, out CylinderViewModel viewModel))
                throw new NotFoundException($"Cylinder ViewModel '{name}' was not found.");

            return viewModel;
        }

        public static void AddServoDriveViewModel(ServoDriveViewModel viewModel)
        {
            ServoDriveViewModels.Add(viewModel.Name, viewModel);
        }

        public static ServoDriveViewModel GetServoDriveViewModel(string name)
        {
            if (!ServoDriveViewModels.TryGetValue(name, out ServoDriveViewModel viewModel))
                throw new NotFoundException($"ServoDrive ViewModel '{name}' was not found.");

            return viewModel;
        }
    }
}
