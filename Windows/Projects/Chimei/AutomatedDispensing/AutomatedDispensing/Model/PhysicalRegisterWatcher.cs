﻿using AutomatedDispensing.Controller;
using AutomatedDispensing.Model.Interfaces;
using Automation.Core.Registers;
using System;
using System.Collections.Generic;
using System.Windows.Threading;
using Unity;

namespace AutomatedDispensing.Model
{
    class PhysicalRegisterWatcher
    {
        private readonly AutomatedDispensingController _controller;
        private readonly IORegister _io;
        private readonly MotionControlRegister _motionControl;
        private readonly DispatcherTimer _timer;
        private readonly List<IPhysicalRegisterObserver> _persistentObservers = new List<IPhysicalRegisterObserver>();
        private readonly List<IPhysicalRegisterObserver> _observers = new List<IPhysicalRegisterObserver>();

        public PhysicalRegisterWatcher()
        {
            _controller = Environment.UnityContainer.Resolve<AutomatedDispensingController>();

            _io = _controller.IORegister.Clone() as IORegister;
            _motionControl = _controller.MotionControlRegister.Clone() as MotionControlRegister;

            _timer = new DispatcherTimer();
            _timer.Tick += new EventHandler(DispatcherTimer_Tick);
            _timer.Interval = TimeSpan.FromMilliseconds(50);
            _timer.Start();
        }

        public void AddObserver(IPhysicalRegisterObserver observer, bool persistent)
        {
            if (observer == null)
                throw new ArgumentNullException("observer");

            if (persistent)
                _persistentObservers.Add(observer);
            else
                _observers.Add(observer);
        }

        public void RemoveObserver(IPhysicalRegisterObserver observer)
        {
            if (observer == null)
                throw new ArgumentNullException("observer");

            if (!_observers.Remove(observer))
                _persistentObservers.Remove(observer);
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            _controller.ReadRegister(_io, _motionControl);

            foreach (var observer in _persistentObservers)
            {
                observer.RefreshPhysicalRegister(_io, _motionControl);
            }

            foreach (var observer in _observers)
            {
                observer.RefreshPhysicalRegister(_io, _motionControl);
            }
        }
    }
}
