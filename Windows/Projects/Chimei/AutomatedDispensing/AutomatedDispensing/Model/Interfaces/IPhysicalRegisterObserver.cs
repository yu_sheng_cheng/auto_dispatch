﻿using Automation.Core.Registers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Model.Interfaces
{
    interface IPhysicalRegisterObserver
    {
        void RefreshPhysicalRegister(IORegister io, MotionControlRegister motionControl);
    }
}
