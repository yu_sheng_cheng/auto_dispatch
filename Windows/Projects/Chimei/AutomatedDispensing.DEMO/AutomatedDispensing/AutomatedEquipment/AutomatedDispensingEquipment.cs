﻿using Automation.Core;
using Automation.Core.Register;
using Automation.Objects.Movable.Cylinders;
using Automation.Objects.Movable.ServoDrive;
using PLC.Mitsubishi;
using SwissKnife;
using SwissKnife.Conversion;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.AutomatedEquipment
{
    class AutomatedDispensingEquipment : SupervisoryController
    {
        private readonly MelsecMCNet _melsec;
        private readonly ArrayBuilder<byte> D6000 = new ArrayBuilder<byte>();
        private readonly ServoDriveControl[] _arms;
        private readonly ushort[] _ushortArray = new ushort[18];
        private readonly int[] _intArray = new int[180];
        private bool _firstRefresh = true;

        protected override AsynchronousTimeoutSignal WDTExpiredSignal { get { return _melsec.TimeoutSignal; } }

        public AutomatedDispensingEquipment()
        {
            LogKeyword = "AutomatedDispensingEquipment";
            ScanCycleTime = 15;
            TimeForBoot = 5000;
            WDTTimeout = 1000;

            IOMemory contactIOMemory = new IOMemory(IOSignalType.Digital,
                new Tuple<ushort, ushort>[] { new Tuple<ushort, ushort>(0x100, 256), new Tuple<ushort, ushort>(0x300, 32) }, 6000);

            IOMemory coilIOMemory = new IOMemory(IOSignalType.Digital,
                new Tuple<ushort, ushort>[] { new Tuple<ushort, ushort>(0x100, 160), new Tuple<ushort, ushort>(0x300, 32) }, 6020);

            IOMemory analogInputIOMemory = new IOMemory(IOSignalType.Analog, 0x0, 6640, 4);

            EquipmentRegister = new AutomatedEquipmentRegister(contactIOMemory, coilIOMemory, analogInputIOMemory, null, null);

            AddMovableObject(
                new DualPositionSolenoidValve("BOM3", string.Empty, "擋瓶氣缸1", new BitCell(6014, 9), new BitCell(6014, 8), new BitCell(6029, 7), new BitCell(6029, 6), "縮回", "擋瓶"));
            AddMovableObject(
                new DualPositionSolenoidValve("BOM3", string.Empty, "擋瓶氣缸2", new BitCell(6014, 11), new BitCell(6014, 10), new BitCell(6029, 9), new BitCell(6029, 8), "縮回", "擋瓶"));
            AddMovableObject(
                new DualPositionSolenoidValve("BOM3", string.Empty, "推樣品瓶氣缸",
                new BitCell(6014, 7), new BitCell(6014, 6), new BitCell(6029, 5), new BitCell(6029, 4), "縮回", "推出"));
            AddMovableObject(
                new DualPositionSolenoidValve("BOM3", string.Empty, "進料旋轉氣缸",
                new BitCell(6014, 1), new BitCell(6014, 0), new BitCell(6028, 15), new BitCell(6028, 14), "回原點", "旋轉"));
            AddMovableObject(
                new DualPositionSolenoidValve("BOM3", string.Empty, "進料氣缸",
                new BitCell(6014, 2), new BitCell(6014, 3), new BitCell(6029, 0), new BitCell(6029, 1), "上", "下"));
            AddMovableObject(
                new DualPositionSolenoidValve("BOM3", string.Empty, "進料夾爪氣缸",
                new BitCell(6014, 5), new BitCell(6014, 4), new BitCell(6029, 3), new BitCell(6029, 2), "開", "夾"));
            AddMovableObject(
                new DualPositionSolenoidValve("BOM3", string.Empty, "移載氣缸",
                new BitCell(6013, 8), new BitCell(6013, 9), new BitCell(6028, 6), new BitCell(6028, 7), "上", "下"));
            AddMovableObject(
                new DualPositionSolenoidValve("BOM3", string.Empty, "移載夾爪氣缸",
                new BitCell(6013, 11), new BitCell(6013, 10), new BitCell(6028, 9), new BitCell(6028, 8), "開", "夾"));

            _arms = new ServoDriveControl[]
            {
                new ServoDriveControl("BOM", string.Empty, "ARM1 X 空瓶移載手臂", 1),
                new ServoDriveControl("BOM", string.Empty, "ARM2 Y 空瓶移載手臂", 2),
                new ServoDriveControl("BOM", string.Empty, "ARM3 X 送料手臂", 3),
                new ServoDriveControl("BOM", string.Empty, "ARM4 Z 1.5ml夾取手臂", 4),
                new ServoDriveControl("BOM", string.Empty, "ARM5 X 配藥移載手臂", 5),
                new ServoDriveControl("BOM", string.Empty, "ARM6 Y 配藥移載手臂", 6),
                new ServoDriveControl("BOM", string.Empty, "ARM7 X 藥劑瓶移載", 7),
                new ServoDriveControl("BOM", string.Empty, "ARM8 Θ 藥劑瓶開蓋手臂", 8),
                new ServoDriveControl("BOM", string.Empty, "ARM9 Θ 樣品瓶開蓋手臂", 9),
                new ServoDriveControl("BOM", string.Empty, "ARM10 Θ 60ml開蓋手臂", 10),
                new ServoDriveControl("BOM", string.Empty, "ARM11 Z1 1ml取TIP手臂", 11),
                new ServoDriveControl("BOM", string.Empty, "ARM12 Z2 10ml取TIP手臂", 12),
                new ServoDriveControl("BOM", string.Empty, "ARM13 Y 藥劑瓶夾取手臂", 13),
                new ServoDriveControl("BOM", string.Empty, "ARM14 Z1 1ml移液手臂", 14),
                new ServoDriveControl("BOM", string.Empty, "ARM15 Z2 10ml移液手臂", 15),
                new ServoDriveControl("BOM", string.Empty, "ARM16 X 取瓶移載手臂", 16),
                new ServoDriveControl("BOM", string.Empty, "ARM17 Y 取瓶移載手臂", 17),
                new ServoDriveControl("BOM", string.Empty, "ARM18 X 送料手臂", 18)
            };

            foreach (var arm in _arms)
            {
                AddMovableObject(arm);
            }

            AddManualControlMessageHandler(new XYLinearMotion("BOM", string.Empty, "空瓶移載手臂", _arms[0], _arms[1]));
            AddManualControlMessageHandler(new XYLinearMotion("BOM", string.Empty, "配藥移載手臂", _arms[4], _arms[5]));
            AddManualControlMessageHandler(new XYLinearMotion("BOM", string.Empty, "取瓶移載手臂", _arms[15], _arms[16]));

            IPAddress ipAddress = IPAddress.Parse(ConfigurationManager.AppSettings["PLC.IPAddress"]);
            int port = int.Parse(ConfigurationManager.AppSettings["PLC.Port"]);

            Log.i($"PLC Ethernet Setting: {ipAddress}:{port}");

            _melsec = new MelsecMCNet("Q", "Q03UDV");
            _melsec.InternetTransport.RemoteEndPoint = new IPEndPoint(ipAddress, port);
        }

        protected override void CleanUp()
        {
            base.CleanUp();
            _melsec.Dispose();
        }

        protected override async Task<OperationResult<bool>> CheckIfEquipmentHasRebootAsync()
        {
            OperationResult<ushort> result = await _melsec.ReadUInt16Async("D6120").ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                Log.i(LogKeyword, $"{_melsec.Type.Name} Read: {result}");
                return OperationResultFactory.CreateFailedResult<bool>(result);
            }

            return OperationResultFactory.CreateSuccessfulResult(!BitOperation.GetBit(result.Value, 7));
        }

        protected override async Task<bool> RefreshAsync()
        {
            OperationResult result = await _melsec.ReadUInt8Async("D6000", 960 * 2, D6000).ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                Log.i(LogKeyword, $"{_melsec.Type.Name} Read: {result}");
                return false;
            }

            byte[] d6000 = D6000.Buffer;

            IByteConverter byteConverter = _melsec.ByteConverter;

            var register = EquipmentRegister;

            byteConverter.ToUInt16(d6000, 0, 18 * 2, register.ContactRegisters, 0); // D6000 - D6017

            byteConverter.ToUInt16(d6000, 100 * 2, 18 * 2, _ushortArray, 0); // D6100 - D6117
            byteConverter.ToInt32(d6000, 699 * 2, 18 * 4 * 4, _intArray, 0); // D6699 - D6842

            int i = 0, j = 0;
            foreach (var arm in _arms)
            {
                var ioMemory = arm.IOMemory;
                ushort value = _ushortArray[i++];
                int i32Value = _intArray[i];

                ioMemory.IsReturnToHomeCompleted = BitOperation.GetBit(value, 0);
                ioMemory.IsPositioningCompleted = BitOperation.GetBit(value, 1);
                ioMemory.IsBusy = BitOperation.GetBit(value, 2);
                ioMemory.HasErrors = BitOperation.GetBit(value, 3);
                ioMemory.IsReturningToHome = BitOperation.GetBit(value, 4);

                ioMemory.CurrentFeedValue = _intArray[j++];
                ioMemory.ErrorCode = unchecked((ushort)_intArray[j++]);
                ioMemory.CurrentTorque = _intArray[j++];
                ++j;
            }

            byteConverter.ToInt32(d6000, 640 * 2, 4 * 4, _intArray, 0); // D6640 - D6647

            ushort[] analogInputs = register.AnalogInputRegisters;

            for (i = 0; i < 4; ++i)
            {
                analogInputs[i] = unchecked((ushort)_intArray[0]);
            }

            ushort d6120 = byteConverter.ToUInt16(d6000, 120 * 2);

            bool reboot = !BitOperation.GetBit(d6120, 7);

            if (_firstRefresh | reboot)
            {
                _firstRefresh = false;

                byteConverter.ToUInt16(d6000, 20 * 2, 12 * 2, register.CoilRegisters, 0); // D6020 - D6031

                byteConverter.ToUInt16(d6000, 140 * 2, 18 * 2, _ushortArray, 0); // D6140 - D6157

                i = 0;
                foreach (var arm in _arms)
                {
                    var ioMemory = arm.IOMemory;
                    ushort value = _ushortArray[i++];

                    ServoDriveCommand command = ServoDriveCommand.None;

                    if (BitOperation.GetBit(value, 0))
                        command |= ServoDriveCommand.JogForward;
                    if (BitOperation.GetBit(value, 1))
                        command |= ServoDriveCommand.JogReverse;
                    if (BitOperation.GetBit(value, 2))
                        command |= ServoDriveCommand.Go;
                    if (BitOperation.GetBit(value, 3))
                        command |= ServoDriveCommand.AxisToStop;
                    if (BitOperation.GetBit(value, 4))
                        command |= ServoDriveCommand.ServoOff;
                    if (BitOperation.GetBit(value, 5))
                        command |= ServoDriveCommand.ReturnToHome;
                    if (BitOperation.GetBit(value, 6))
                        command |= ServoDriveCommand.Reset;

                    ioMemory.Command = command;
                }

                if (reboot)
                    OnEquipmentReboot();
            }

            return true;
        }

        protected override void OnBootCompleted()
        {
            base.OnBootCompleted();

            byte[] d6000 = D6000.Buffer;

            IByteConverter byteConverter = _melsec.ByteConverter;

            ushort d6120 = byteConverter.ToUInt16(d6000, 120 * 2);

            d6120 = unchecked((ushort)BitOperation.SetBit(d6120, 7, true));

            byteConverter.GetBytes(d6120, d6000, 120 * 2);

            EquipmentState.NeedsUpdate = true;
        }

        protected override async Task<bool> UpdateAsync()
        {
            byte[] d6000 = D6000.Buffer;

            IByteConverter byteConverter = _melsec.ByteConverter;

            var register = EquipmentRegister;

            byteConverter.GetBytes(register.CoilRegisters, d6000, 20 * 2);

            int i = 0, j = 0;
            foreach (var arm in _arms)
            {
                var ioMemory = arm.IOMemory;
                ushort value = 0;

                ServoDriveCommand command = ioMemory.Command;

                if ((command & ServoDriveCommand.JogForward) == ServoDriveCommand.JogForward)
                    value = unchecked((ushort)BitOperation.SetBit(value, 0, true));
                if ((command & ServoDriveCommand.JogReverse) == ServoDriveCommand.JogReverse)
                    value = unchecked((ushort)BitOperation.SetBit(value, 1, true));
                if ((command & ServoDriveCommand.Go) == ServoDriveCommand.Go)
                    value = unchecked((ushort)BitOperation.SetBit(value, 2, true));
                if ((command & ServoDriveCommand.AxisToStop) == ServoDriveCommand.AxisToStop)
                    value = unchecked((ushort)BitOperation.SetBit(value, 3, true));
                if ((command & ServoDriveCommand.ServoOff) == ServoDriveCommand.ServoOff)
                    value = unchecked((ushort)BitOperation.SetBit(value, 4, true));
                if ((command & ServoDriveCommand.ReturnToHome) == ServoDriveCommand.ReturnToHome)
                    value = unchecked((ushort)BitOperation.SetBit(value, 5, true));
                if ((command & ServoDriveCommand.Reset) == ServoDriveCommand.Reset)
                    value = unchecked((ushort)BitOperation.SetBit(value, 6, true));

                _ushortArray[i++] = value;

                _intArray[j++] = ioMemory.PositioningValue;
                _intArray[j++] = ioMemory.PositioningSpeed;
                _intArray[j++] = ioMemory.AccelerationTime;
                _intArray[j++] = ioMemory.SpeedLimit;
                _intArray[j++] = ioMemory.JogSpeed;
                j += 5;
            }

            int armCount = _arms.Length;

            byteConverter.GetBytes(_ushortArray, 0, armCount, d6000, 140 * 2);
            byteConverter.GetBytes(_intArray, 0, armCount * 10, d6000, 200 * 2);

            OperationResult result = await _melsec.WriteAsync("D6000", D6000).ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                Log.i(LogKeyword, $"{_melsec.Type.Name} Write: {result}");
                return false;
            }
            return true;
        }
    }
}
