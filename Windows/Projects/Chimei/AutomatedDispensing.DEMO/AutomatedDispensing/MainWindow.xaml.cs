﻿using AutomatedDispensing.AutomatedEquipment;
using Automation.Core.Messages.ManualControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Unity;

namespace AutomatedDispensing
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly AutomatedDispensingEquipment _equipment;

        public MainWindow()
        {
            InitializeComponent();

            _equipment = Environment.UnityContainer.Resolve<AutomatedDispensingEquipment>();

            XJogRButton.AddHandler(Button.MouseLeftButtonDownEvent, new MouseButtonEventHandler(Button19_MouseDown), true);
            XJogRButton.AddHandler(Button.MouseLeftButtonUpEvent, new MouseButtonEventHandler(ButtonX_MouseUp), true);
            XJogRButton.AddHandler(Button.MouseDownEvent, new MouseButtonEventHandler(Button19_MouseDown), true);
            XJogRButton.AddHandler(Button.MouseUpEvent, new MouseButtonEventHandler(ButtonX_MouseUp), true);

            XJogFButton.AddHandler(Button.MouseLeftButtonDownEvent, new MouseButtonEventHandler(Button20_MouseDown), true);
            XJogFButton.AddHandler(Button.MouseLeftButtonUpEvent, new MouseButtonEventHandler(ButtonX_MouseUp), true);
            XJogFButton.AddHandler(Button.MouseDownEvent, new MouseButtonEventHandler(Button20_MouseDown), true);
            XJogFButton.AddHandler(Button.MouseUpEvent, new MouseButtonEventHandler(ButtonX_MouseUp), true);

            YJogRButton.AddHandler(Button.MouseLeftButtonDownEvent, new MouseButtonEventHandler(Button21_MouseDown), true);
            YJogRButton.AddHandler(Button.MouseLeftButtonUpEvent, new MouseButtonEventHandler(ButtonY_MouseUp), true);
            YJogRButton.AddHandler(Button.MouseDownEvent, new MouseButtonEventHandler(Button21_MouseDown), true);
            YJogRButton.AddHandler(Button.MouseUpEvent, new MouseButtonEventHandler(ButtonY_MouseUp), true);


            YJogFButton.AddHandler(Button.MouseLeftButtonDownEvent, new MouseButtonEventHandler(Button22_MouseDown), true);
            YJogFButton.AddHandler(Button.MouseLeftButtonUpEvent, new MouseButtonEventHandler(ButtonY_MouseUp), true);
            YJogFButton.AddHandler(Button.MouseDownEvent, new MouseButtonEventHandler(Button22_MouseDown), true);
            YJogFButton.AddHandler(Button.MouseUpEvent, new MouseButtonEventHandler(ButtonY_MouseUp), true);
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "擋瓶氣缸1";
            message.Go("縮回");
            _equipment.PostMessage(message);
        }

        private void Button2_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "擋瓶氣缸1";
            message.Go("擋瓶");
            _equipment.PostMessage(message);
        }

        private void Button3_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "擋瓶氣缸2";
            message.Go("縮回");
            _equipment.PostMessage(message);
        }

        private void Button4_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "擋瓶氣缸2";
            message.Go("擋瓶");
            _equipment.PostMessage(message);
        }

        private void Button5_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "推樣品瓶氣缸";
            message.Go("縮回");
            _equipment.PostMessage(message);
        }

        private void Button6_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "推樣品瓶氣缸";
            message.Go("推出");
            _equipment.PostMessage(message);
        }

        private void Button7_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "進料旋轉氣缸";
            message.Go("回原點");
            _equipment.PostMessage(message);
        }

        private void Button8_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "進料旋轉氣缸";
            message.Go("旋轉");
            _equipment.PostMessage(message);
        }

        private void Button9_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "進料氣缸";
            message.Go("上");
            _equipment.PostMessage(message);
        }

        private void Button10_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "進料氣缸";
            message.Go("下");
            _equipment.PostMessage(message);
        }

        private void Button11_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "進料夾爪氣缸";
            message.Go("開");
            _equipment.PostMessage(message);
        }

        private void Button12_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "進料夾爪氣缸";
            message.Go("夾");
            _equipment.PostMessage(message);
        }

        private void Button13_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "移載氣缸";
            message.Go("上");
            _equipment.PostMessage(message);
        }

        private void Button14_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "移載氣缸";
            message.Go("下");
            _equipment.PostMessage(message);
        }

        private void Button15_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "移載夾爪氣缸";
            message.Go("開");
            _equipment.PostMessage(message);
        }

        private void Button16_Click(object sender, RoutedEventArgs e)
        {
            CylinderManualControlMessage message = new CylinderManualControlMessage();
            message.Name = "移載夾爪氣缸";
            message.Go("夾");
            _equipment.PostMessage(message);
        }

        private void Button17_Click(object sender, RoutedEventArgs e)
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage();
            message.Name = "取瓶移載手臂";
            message.GoHome();
            _equipment.PostMessage(message);
        }

        private void Button18_Click(object sender, RoutedEventArgs e)
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage();
            message.Name = "取瓶移載手臂";
            message.Go("掃碼區");
            _equipment.PostMessage(message);
        }

        private void Button19_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage();
            message.Name = "ARM16 X 取瓶移載手臂";
            message.StartJog(-1000);
            _equipment.PostMessage(message);
        }

        private void Button20_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage();
            message.Name = "ARM16 X 取瓶移載手臂";
            message.StartJog(1000);
            _equipment.PostMessage(message);
        }

        private void ButtonX_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage();
            message.Name = "ARM16 X 取瓶移載手臂";
            message.StopJog();
            _equipment.PostMessage(message);
        }

        private void Button21_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage();
            message.Name = "ARM17 Y 取瓶移載手臂";
            message.StartJog(-1000);
            _equipment.PostMessage(message);
        }

        private void Button22_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage();
            message.Name = "ARM17 Y 取瓶移載手臂";
            message.StartJog(1000);
            _equipment.PostMessage(message);
        }

        private void ButtonY_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage();
            message.Name = "ARM17 Y 取瓶移載手臂";
            message.StopJog();
            _equipment.PostMessage(message);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage();
            message.Name = "ARM16 X 取瓶移載手臂";
            message.Reset();
            _equipment.PostMessage(message);

            message = new ServoDriveManualControlMessage();
            message.Name = "ARM17 Y 取瓶移載手臂";
            message.Reset();
            _equipment.PostMessage(message);
        }
    }
}
