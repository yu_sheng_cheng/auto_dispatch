﻿using AutomatedDispensingController.AutomatedMachine;
using AutomatedDispensingController.Net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensingController.ViewModel
{
    class MainViewModel : IDisposable
    {
        private readonly AutomatedDispensingMachine _machine;
        private readonly HttpServer _server;

        public MainViewModel()
        {
            _machine = new AutomatedDispensingMachine();

            try
            {
                _machine.Start();

                _server = new HttpServer
                {
                    Controller = _machine
                };
                _server.Start();
            }
            catch
            {
                if (_server != null)
                    _server.Dispose();
                _machine.Dispose();
                throw;
            }
        }

        public void Dispose()
        {
            _server.Dispose();
            _machine.Dispose();
        }
    }
}
