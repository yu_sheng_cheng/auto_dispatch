﻿using Automation.Core;
using Automation.Core.Register;
using PLC.Mitsubishi;
using SwissKnife;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensingController.AutomatedMachine
{
    class AutomatedDispensingMachine : SupervisoryController
    {
        private readonly MelsecMCNet _melsec;
        private readonly ArrayBuilder<byte> D6000 = new ArrayBuilder<byte>();

        protected override AsynchronousTimeoutSignal WDTExpiredSignal { get { return _melsec.TimeoutSignal; } }

        public AutomatedDispensingMachine()
        {
            AutomatedMachineRegister machineRegister = new AutomatedMachineRegister(
                new IOMemory(IORegisterType.Bit, new Tuple<ushort, ushort>[] { new Tuple<ushort, ushort>(0x100, 160), new Tuple<ushort, ushort>(0x300, 32) }, 6020),
                new IOMemory(IORegisterType.Bit, new Tuple<ushort, ushort>[] { new Tuple<ushort, ushort>(0x100, 256), new Tuple<ushort, ushort>(0x300, 32) }, 6020),
                null, null);

            MachineRegister = machineRegister;

            LogKeyword = "AutomatedDispensingMachine";
            ScanCycleTime = 15;

            IPAddress ipAddress = IPAddress.Parse(ConfigurationManager.AppSettings["PLC.IPAddress"]);
            int port = int.Parse(ConfigurationManager.AppSettings["PLC.Port"]);

            Log.i($"PLC Ethernet Setting: {ipAddress}:{port}");

            _melsec = new MelsecMCNet("Q", "Q03UDV");
            _melsec.InternetTransport.RemoteEndPoint = new IPEndPoint(ipAddress, port);
        }

        protected override void CleanUp()
        {
            base.CleanUp();
            _melsec.Dispose();
        }

        protected override async Task<bool> Refresh()
        {
            OperationResult result = await _melsec.ReadUInt8Async("D6000", 64, D6000).ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                Log.i(LogKeyword, $"{_melsec.Type.Name} Read: {result}");
                return false;
            }

            byte[] d6000 = D6000.Buffer;

            AutomatedMachineRegister machineRegister = MachineRegister;

            ushort[] contactRegisters = machineRegister.ContactRegisters;
            ushort[] coilRegisters = machineRegister.CoilRegisters;

            _melsec.ByteConverter.ToUInt16(d6000, 0, 36, contactRegisters, 0); // D6000-D6017
            _melsec.ByteConverter.ToUInt16(d6000, 40, 24, coilRegisters, 0); // D6020-D6031
            return true;
        }
    }
}
