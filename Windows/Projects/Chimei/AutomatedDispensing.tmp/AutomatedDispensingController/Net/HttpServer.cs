﻿using AutomatedDispensingController.Net.RESTfulAPIs;
using Automation.Net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensingController.Net
{
    sealed class HttpServer : HttpServer<AutomatedDispensingRESTfulAPI>
    {
        internal const string MachineName = "automated-dispensing";

        public HttpServer() : base(MachineName, int.Parse(ConfigurationManager.AppSettings["HttpServer.Port"]), 100)
        {
            LogKeyword = "AutomatedDispensingHttpServer";
        }
    }
}
