﻿using Automation.Net;
using Automation.Net.RESTfulAPIs;
using Net.Http;
using QualityTechnologyDepartment;
using SwissKnife;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensingController.Net.RESTfulAPIs
{
    partial class AutomatedDispensingRESTfulAPI
    {
        [DataContract]
        private sealed class LoginArgs : HttpActionArgs
        {
            [DataMember(IsRequired = true)]
            public string EmployeeId { get; set; }

            public override void CheckAfterDeserialization()
            {
                if (EmployeeId == null)
                    throw new SerializationException("The 'EmployeeId' property of the LoginArgs cannot be null.");
            }
        }

        public override OperationResult<string, IRemoteLoginUser> Login(IHttpContext context, string sessionId)
        {
            LoginArgs args = HttpActionArgs.DeserializeJson<LoginArgs>(context.Request.Body);
            if (args == null)
                return OperationResultFactory.CreateFailedResult<string, IRemoteLoginUser>(GetType().FullName, -1, Automation.SR.InvalidRequestParameter);

            UserAccount userAccount;

            if (!Administrator.Login(args.EmployeeId, out userAccount))
                return OperationResultFactory.CreateFailedResult<string, IRemoteLoginUser>(GetType().FullName, -1, Automation.SR.EnteredEmployeeNumberDoesNotExist);

            string data = $"{{\"EmployeeId\":\"{userAccount.EmployeeId}\",\"EmployeeName\":\"{userAccount.EmployeeName}\",\"Gender\":\"{userAccount.Gender.ToString()}\"," +
                $"\"Permission\":\"{userAccount.Permission.ToString()}\",\"SessionId\":\"{sessionId}\"}}";

            return OperationResultFactory.CreateSuccessfulResult<string, IRemoteLoginUser>(data, userAccount);
        }
    }
}
