﻿using Automation.Net;
using Automation.Net.RESTfulAPIs;
using Net.Http;
using Net.Http.WebSockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using SwissKnife.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace AutomatedDispensingController.Net.RESTfulAPIs
{
    sealed partial class AutomatedDispensingRESTfulAPI : RESTfulAPI
    {
        private const string _logKeyword = "AutomatedDispensingRESTfulAPI";

        private readonly ILog _log;
        private readonly WebSocketMessageCreationConverter _webSocketMessageCreationConverter = new WebSocketMessageCreationConverter();

        public AutomatedDispensingRESTfulAPI()
        {
            _log = Environment.UnityContainer.Resolve<ILog>();
        }

        public override bool DispatchGET<T>(HttpServer<T> server, IHttpContext context, string[] pathSegments, IRemoteLoginUser user)
        {
            return false;
        }

        public override bool DispatchPOST<T>(HttpServer<T> server, IHttpContext context, string[] pathSegments, IRemoteLoginUser user)
        {
            return false;
        }

        public override WebSocketMessage ParseWebSocketMessage(string json)
        {
            try
            {
                WebSocketMessage message = JsonConvert.DeserializeObject<WebSocketMessage>(json, _webSocketMessageCreationConverter);
                message.CheckAfterDeserialization();
                return message;
            }
            catch (Exception e)
            {
                _log.d(_logKeyword, $"ParseWebSocketMessage:{System.Environment.NewLine}{e}");
                throw new WebSocketMessageParsingException(e is SerializationException ? WebSocketCloseStatus.InvalidPayloadData : WebSocketCloseStatus.InvalidMessageType);
            }
        }

        private class WebSocketMessageCreationConverter : CustomCreationConverter<WebSocketMessage>
        {
            public override WebSocketMessage Create(Type objectType)
            {
                throw new NotImplementedException();
            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                JObject jObject = JObject.Load(reader);

                JToken methodhToken = jObject["Method"];
                JToken pathToken = jObject["Path"];

                if (methodhToken == null | pathToken == null)
                    throw new SerializationException("'Method' and 'Path' must be set.");

                string method = methodhToken.ToString();
                string path = pathToken.ToString();

                string[] hierarchies = HttpRequest.ExtractSegmentsFromPath(path);

                if (hierarchies.Length < 2 || hierarchies[0] != HttpServer.MachineName)
                    throw new SerializationException($"Function [{method}:{path}] is not supported");

                throw new SerializationException($"Function [{method}:{path}] is not supported");
            }
        }
    }
}
