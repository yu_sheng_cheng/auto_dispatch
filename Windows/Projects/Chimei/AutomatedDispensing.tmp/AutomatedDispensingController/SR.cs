﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AutomatedDispensingController
{
    public static class SR
    {
        private static StringResource _sr;

        static SR()
        {
            _sr = StringResource.Load();
        }
    }

    [XmlRoot(ElementName = "AutomatedDispensingController", Namespace = "AutomatedDispensingController.Cultures.StringResource.xaml", IsNullable = false)]
    public class StringResource
    {
        internal static StringResource Load()
        {
            return Load(string.Empty);
        }

        internal static StringResource Load(string name)
        {
            var assembly = Assembly.GetExecutingAssembly();

            if (!string.IsNullOrWhiteSpace(name))
                name = $"_{name}";

            using (var stream = assembly.GetManifestResourceStream($"{assembly.GetName().Name.Replace(".", "._")}.Cultures.StringResource{name}.xml"))
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    var serializer = new XmlSerializer(typeof(StringResource));
                    return (StringResource)serializer.Deserialize(reader);
                }
            }
        }
    }
}
