﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensingView.ViewModel
{
    class LoginViewModel
    {
        public ObservableCollection<char> EmployeeId { get; } = new ObservableCollection<char>();

        public LoginViewModel()
        {
            EmployeeId.Add('1');
            EmployeeId.Add('2');
            EmployeeId.Add('3');
            EmployeeId.Add('4');
            EmployeeId.Add('1');
            EmployeeId.Add('2');
            EmployeeId.Add('3');
            EmployeeId.Add('4');
        }
    }
}
