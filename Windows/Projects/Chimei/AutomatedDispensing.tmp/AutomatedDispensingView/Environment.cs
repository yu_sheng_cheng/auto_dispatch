﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace AutomatedDispensingView
{
    static class Environment
    {
        public static IUnityContainer UnityContainer { get; set; }
    }
}
