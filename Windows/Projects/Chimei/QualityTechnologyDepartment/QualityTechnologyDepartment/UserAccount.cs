﻿using Automation.Net.RESTfulAPIs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QualityTechnologyDepartment
{
    public enum Gender
    {
        Male,
        Female
    }

    public enum AccountPermission
    {
        SupplierEngineer,
        MaintenanceMan,
        Manager,
        ResearchFellow,
        Operator
    }

    public class UserAccount : IRemoteLoginUser
    {
        public string EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public Gender Gender { get; set; }

        public AccountPermission Permission { get; set; }
    }
}
