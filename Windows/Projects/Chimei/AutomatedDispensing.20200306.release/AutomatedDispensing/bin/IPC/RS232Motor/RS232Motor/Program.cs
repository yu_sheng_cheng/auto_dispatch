﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RS232Motor
{
    class Program
    {
        static void Main(string[] args)
        {
            SerialPort port = new SerialPort();
            port.PortName = "COM2";
            port.BaudRate = 9600;
            port.Parity = Parity.None;
            port.StopBits = StopBits.One;
            port.Open();

            /*string line2 = port.ReadLine();
            Console.WriteLine("{0}", line2);
            Console.WriteLine("------------");
            line2 = port.ReadLine();
            Console.WriteLine("{0}", line2);
            Console.WriteLine("------------");
            line2 = port.ReadLine();
            Console.WriteLine("{0}", line2);
            Console.WriteLine("------------");
            line2 = port.ReadLine();
            Console.WriteLine("{0}", line2);
            Console.WriteLine("------------");
            line2 = port.ReadLine();
            Console.WriteLine("{0}", line2);
            Console.WriteLine("------------");
            line2 = port.ReadLine();
            Console.WriteLine("{0}", line2);
            Console.WriteLine("------------");
            line2 = port.ReadLine();
            Console.WriteLine("{0}", line2);
            Console.WriteLine("------------");
            return;*/

           //  port.WriteLine("JGR"); //正轉 (逆時針)
            // port.WriteLine("JGF"); //反轉
              port.WriteLine("JG0"); //停止

            //port.WriteLine("?PN14");
            //port.WriteLine("PN14=10");
            string line = port.ReadLine();
            Console.WriteLine("{0}", line);
            Console.WriteLine("------------");
            line = port.ReadLine();
            Console.WriteLine("{0}", line);
            Console.WriteLine("------------");
            line = port.ReadLine();
            Console.WriteLine("{0}", line);
            Console.WriteLine("------------");
        }
    }
}
