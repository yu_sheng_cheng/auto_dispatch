﻿using AutomatedDispensing.Controller;
using AutomatedDispensing.Model.Interfaces;
using AutomatedDispensing.View.Dialogs;
using Automation.Core.Messages.ManualControl;
using Automation.Core.Registers;
using Automation.Profiles.Cylinder;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;
using Unity;
using Widget.WPF;

namespace AutomatedDispensing.ViewModel
{
    public class CylinderViewModel : ViewModelBase, IIOObserver
    {
        private readonly AutomatedDispensingController _controller;
        private readonly CylinderProfile _profile;

        private readonly BitCell _position1Input;
        private readonly BitCell _position1Output;
        private readonly BitCell _position2Input;
        private readonly BitCell _position2Output;

        private bool _isPosition1InputOn;
        private bool _isPosition1OutputOn;
        private bool _isPosition2InputOn;
        private bool _isPosition2OutputOn;

        public string Name { get; }

        public bool IsDoubleCoil { get; } = true;

        public string Position1InputName { get; }

        public bool IsPosition1InputOn
        {
            get { return _isPosition1InputOn; }
            set
            {
                if (value != _isPosition1InputOn)
                {
                    _isPosition1InputOn = value;

                    RaisePropertyChanged(nameof(IsPosition1InputOn));
                }
            }
        }

        public string Position1OutputName { get; }

        public bool IsPosition1OutpuOn
        {
            get { return _isPosition1OutputOn; }
            set
            {
                if (value != _isPosition1OutputOn)
                {
                    _isPosition1OutputOn = value;

                    RaisePropertyChanged(nameof(IsPosition1OutpuOn));
                }
            }
        }

        public string Acting1Name { get; }

        public string Position2InputName { get; }

        public bool IsPosition2InputOn
        {
            get { return _isPosition2InputOn; }
            set
            {
                if (value != _isPosition2InputOn)
                {
                    _isPosition2InputOn = value;

                    RaisePropertyChanged(nameof(IsPosition2InputOn));
                }
            }
        }

        public string Position2OutputName { get; }

        public bool IsPosition2OutpuOn
        {
            get { return _isPosition2OutputOn; }
            set
            {
                if (value != _isPosition2OutputOn)
                {
                    _isPosition2OutputOn = value;

                    RaisePropertyChanged(nameof(IsPosition2OutpuOn));
                }
            }
        }

        public string Acting2Name { get; }

        public ushort PositioningTimeout
        {
            get { return _profile.Configuration.PositioningTimeout; }
            set
            {
                if (value == 0)
                    return;

                var configuration = _profile.Configuration;

                if (value != configuration.PositioningTimeout)
                {
                    configuration.PositioningTimeout = value;

                    UpdateConfiguration();
                }
            }
        }

        public ushort Position1DoneHoldTime
        {
            get { return _profile.Configuration.Position1DoneHoldTime; }
            set
            {
                if (value == 0)
                    return;

                var configuration = _profile.Configuration;

                if (value != configuration.Position1DoneHoldTime)
                {
                    configuration.Position1DoneHoldTime = value;

                    UpdateConfiguration();
                }
            }
        }

        public ushort Position2DoneHoldTime
        {
            get { return _profile.Configuration.Position2DoneHoldTime; }
            set
            {
                if (value == 0)
                    return;

                var configuration = _profile.Configuration;

                if (value != configuration.Position2DoneHoldTime)
                {
                    configuration.Position2DoneHoldTime = value;

                    UpdateConfiguration();
                }
            }
        }

        public ICommand GoCommand { get; }

        public ICommand ConfigureCommand { get; }

        public CylinderViewModel(string name, BitCell position1Input, BitCell position2Input, BitCell positionOutput, string position1InputName, string position1OutputName,
            string acting1Name, string position2InputName, string position2OutputName, string acting2Name) : this(name, position1Input, positionOutput, position2Input, positionOutput,
                position1InputName, position1OutputName, acting1Name, position2InputName, position2OutputName, acting2Name)
        {
            IsDoubleCoil = false;
        }

        public CylinderViewModel(string name, BitCell position1Input, BitCell position1Output, BitCell position2Input, BitCell position2Output,
            string position1InputName, string position1OutputName, string acting1Name, string position2InputName, string position2OutputName, string acting2Name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            if (string.IsNullOrEmpty(position1InputName))
                throw new ArgumentException("String cannot be empty or null.", "position1InputName");

            if (string.IsNullOrEmpty(position1OutputName))
                throw new ArgumentException("String cannot be empty or null.", "position1OutputName");

            if (string.IsNullOrEmpty(acting1Name))
                throw new ArgumentException("String cannot be empty or null.", "acting1Name");

            if (string.IsNullOrEmpty(position2InputName))
                throw new ArgumentException("String cannot be empty or null.", "position2InputName");

            if (string.IsNullOrEmpty(position2OutputName))
                throw new ArgumentException("String cannot be empty or null.", "position2OutputName");

            if (string.IsNullOrEmpty(acting2Name))
                throw new ArgumentException("String cannot be empty or null.", "acting2Name");

            using (var reader = new StreamReader(Path.Combine(Environment.EquipmentProfileRootFolder, $"{name}.xml"), Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(CylinderProfile));

                _profile = (CylinderProfile)serializer.Deserialize(reader);
                _profile.CheckAfterDeserialization();
            }

            _controller = Environment.UnityContainer.Resolve<AutomatedDispensingController>();

            _position1Input = position1Input;
            _position1Output = position1Output;
            _position2Input = position2Input;
            _position2Output = position2Output;

            Name = name;
            Position1InputName = position1InputName;
            Position1OutputName = position1OutputName;
            Acting1Name = acting1Name;
            Position2InputName = position2InputName;
            Position2OutputName = position2OutputName;
            Acting2Name = acting2Name;

            GoCommand = new RelayCommand<string>(Go);
            ConfigureCommand = new RelayCommand<Window>(Configure);

            Environment.IOWatcher.PersistentObservers.Add(this);
        }

        private void Go(string position)
        {
            var message = new CylinderManualControlMessage(Name);

            message.Go(position);

            _controller.PostMessage(message);
        }

        private void Configure(Window owner)
        {
            CylinderConfigurationWindow dlg = new CylinderConfigurationWindow();

            dlg.Owner = owner;
            dlg.DataContext = this;
            dlg.ShowDialog();
        }

        private void UpdateConfiguration()
        {
            var message = new CylinderManualControlMessage(Name);

            message.UpdateConfiguration(_profile.Configuration);

            _controller.PostMessage(message);
        }

        public void RefreshIO(IORegister register)
        {
            IsPosition1InputOn = register.GetContact(_position1Input);
            IsPosition1OutpuOn = register.GetCoil(_position1Output);
            IsPosition2InputOn = register.GetContact(_position2Input);
            IsPosition2OutpuOn = register.GetCoil(_position2Output);
        }
    }
}
