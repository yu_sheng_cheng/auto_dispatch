﻿using AutomatedDispensing.Controller;
using AutomatedDispensing.Model.Interfaces;
using AutomatedDispensing.View.Dialogs;
using Automation.Core.Messages.ManualControl;
using Automation.Core.Registers;
using Automation.Profiles.ServoDrive;
using SwissKnife;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;
using Unity;
using Widget.WPF;

namespace AutomatedDispensing.ViewModel
{
    class XYLinearMotionViewModel : ViewModelBase, IIOObserver
    {
        private readonly string _xAxisName;
        private readonly string _yAxisName;
        private readonly byte _xAxisIndex;
        private readonly byte _yAxisIndex;

        private readonly ServoDriveProfile _profile;
        private readonly ServoDrivePointViewModel[] _points;
        private readonly AutomatedDispensingController _controller;

        private int _xAxisJogSpeed;
        private int _yAxisJogSpeed;
        private bool _isJogOperation;
        private bool _hadAxisXReturnedToHomePosition;
        private bool _hadAxisYReturnedToHomePosition;
        private int _xAxisCurrentFeedValue;
        private int _yAxisCurrentFeedValue;
        private ServoDrivePointViewModel _onPoint;

        public string Name { get; }

        public string AxisXId { get; }

        public string AxisYId { get; }

        public bool HadAxisXReturnedToHomePosition
        {
            get { return _hadAxisXReturnedToHomePosition; }
            set
            {
                if (value != _hadAxisXReturnedToHomePosition)
                {
                    _hadAxisXReturnedToHomePosition = value;

                    RaisePropertyChanged(nameof(HadAxisXReturnedToHomePosition));
                }
            }
        }

        public bool HadAxisYReturnedToHomePosition
        {
            get { return _hadAxisYReturnedToHomePosition; }
            set
            {
                if (value != _hadAxisYReturnedToHomePosition)
                {
                    _hadAxisYReturnedToHomePosition = value;

                    RaisePropertyChanged(nameof(HadAxisYReturnedToHomePosition));
                }
            }
        }

        public bool IsJogOperation
        {
            get { return _isJogOperation; }
            set
            {
                if (value != _isJogOperation)
                {
                    _isJogOperation = value;

                    RaisePropertyChanged(nameof(IsJogOperation));
                }
            }
        }

        public int AxisXJogSpeed
        {
            get { return _xAxisJogSpeed; }
            set
            {
                if (value <= 0)
                    return;

                if (value > _profile.Parameters.JogSpeedLimit)
                    value = _profile.Parameters.JogSpeedLimit;

                _xAxisJogSpeed = value;
            }
        }

        public int AxisYJogSpeed
        {
            get { return _yAxisJogSpeed; }
            set
            {
                if (value <= 0)
                    return;

                if (value > _profile.Parameters.JogSpeedLimit)
                    value = _profile.Parameters.JogSpeedLimit;

                _yAxisJogSpeed = value;
            }
        }

        public double AxisXCurrentFeedValue { get; private set; }

        public double AxisYCurrentFeedValue { get; private set; }

        public int AccelerationTime
        {
            get { return _profile.Parameters.AccelerationTime; }
            set
            {
                if (value <= 0)
                    return;

                var parameters = _profile.Parameters;

                if (value != parameters.AccelerationTime)
                {
                    parameters.AccelerationTime = value;
                    parameters.DecelerationTime = value;

                    UpdateParameters();
                }
            }
        }

        public int PositioningSpeed
        {
            get { return _profile.Parameters.PositioningSpeed; }
            set
            {
                if (value <= 0)
                    return;

                var parameters = _profile.Parameters;

                if (value != parameters.PositioningSpeed)
                {
                    parameters.SpeedLimit = value;
                    parameters.PositioningSpeed = value;

                    UpdateParameters();
                }
            }
        }

        public IEnumerable<ServoDrivePointViewModel> Points { get { return _points; } }

        public ICommand ReturnToHomePositionCommand { get; }

        public ICommand StopAxisCommand { get; }

        public ICommand StartAxisXJogReverseCommand { get; }

        public ICommand StartAxisXJogForwardCommand { get; }

        public ICommand StopAxisXJogCommand { get; }

        public ICommand StartAxisYJogReverseCommand { get; }

        public ICommand StartAxisYJogForwardCommand { get; }

        public ICommand StopAxisYJogCommand { get; }

        public ICommand GoOrUpdatePositioningValueCommand { get; }

        public ICommand ConfigureCommand { get; }

        public XYLinearMotionViewModel(string name, byte xAxisId, byte yAxisId)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            if (xAxisId == 0 | yAxisId == 0)
                throw new ArgumentException("The 'axisId' argument must be greater than zero.");

            _xAxisName = $"ARM{xAxisId.ToString()} X {name}";
            _yAxisName = $"ARM{yAxisId.ToString()} Y {name}";

            _xAxisIndex = (byte)(xAxisId - 1);
            _yAxisIndex = (byte)(yAxisId - 1);

            ServoDriveProfile xAxisProfile, yAxisProfile;

            using (var reader = new StreamReader(Path.Combine(Environment.EquipmentProfileRootFolder, $"{_xAxisName}.xml"), Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(ServoDriveProfile));

                xAxisProfile = (ServoDriveProfile)serializer.Deserialize(reader);
                xAxisProfile.CheckAfterDeserialization();
                _xAxisJogSpeed = xAxisProfile.Parameters.JogSpeed;
            }

            using (var reader = new StreamReader(Path.Combine(Environment.EquipmentProfileRootFolder, $"{_yAxisName}.xml"), Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(ServoDriveProfile));

                yAxisProfile = (ServoDriveProfile)serializer.Deserialize(reader);
                yAxisProfile.CheckAfterDeserialization();
                _yAxisJogSpeed = yAxisProfile.Parameters.JogSpeed;
            }

            ServoDrivePosition[] xAxisPositions = xAxisProfile.Positions;

            _points = new ServoDrivePointViewModel[xAxisPositions.Length];

            int i = 0;
            foreach (var xAxisPos in xAxisPositions)
            {
                yAxisProfile.TryGetPosition(xAxisPos.Name, out ServoDrivePosition yAxisPos);

                _points[i++] = new ServoDrivePointViewModel(name, _xAxisName, xAxisPos, xAxisPos.Value, yAxisPos.Value);
            }

            Array.Sort(_points, ((x, y) => { return x.PositionId - y.PositionId; }));

            _profile = xAxisProfile;

            _controller = Environment.UnityContainer.Resolve<AutomatedDispensingController>();

            Name = name;
            AxisXId = $"ARM{xAxisId.ToString()}";
            AxisYId = $"ARM{yAxisId.ToString()}";

            ReturnToHomePositionCommand = new RelayCommand(ReturnToHomePosition);
            StopAxisCommand = new RelayCommand(StopAxis);
            StartAxisXJogReverseCommand = new RelayCommand(StartAxisXJogReverse);
            StartAxisXJogForwardCommand = new RelayCommand(StartAxisXJogForward);
            StopAxisXJogCommand = new RelayCommand(StopAxisXJog);
            StartAxisYJogReverseCommand = new RelayCommand(StartAxisYJogReverse);
            StartAxisYJogForwardCommand = new RelayCommand(StartAxisYJogForward);
            StopAxisYJogCommand = new RelayCommand(StopAxisYJog);
            GoOrUpdatePositioningValueCommand = new RelayCommand<ServoDrivePointViewModel>(GoOrUpdatePositioningValue);
            ConfigureCommand = new RelayCommand<Window>(Configure);

            Environment.IOWatcher.PersistentObservers.Add(this);
        }

        private void ReturnToHomePosition()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(Name);

            message.ReturnToHomePosition();

            _controller.PostMessage(message);
        }

        private void StopAxis()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(Name);

            message.StopAxis();

            _controller.PostMessage(message);
        }

        private void StartAxisXJogReverse()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(_xAxisName);

            message.StartJog(-_xAxisJogSpeed);

            _controller.PostMessage(message);
        }

        private void StartAxisXJogForward()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(_xAxisName);

            message.StartJog(_xAxisJogSpeed);

            _controller.PostMessage(message);
        }

        private void StopAxisXJog()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(_xAxisName);

            message.StopJog();

            _controller.PostMessage(message);
        }

        private void StartAxisYJogReverse()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(_yAxisName);

            message.StartJog(-_yAxisJogSpeed);

            _controller.PostMessage(message);
        }

        private void StartAxisYJogForward()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(_yAxisName);

            message.StartJog(_yAxisJogSpeed);

            _controller.PostMessage(message);
        }

        private void StopAxisYJog()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(_yAxisName);

            message.StopJog();

            _controller.PostMessage(message);
        }

        private void GoOrUpdatePositioningValue(ServoDrivePointViewModel pointViewModel)
        {
            if (_isJogOperation)
            {
                int x = _xAxisCurrentFeedValue;
                int y = _yAxisCurrentFeedValue;

                PointDialog dlg = new PointDialog();
                dlg.X = x.ToString();
                dlg.Y = y.ToString();
                dlg.Owner = Application.Current.MainWindow;

                if (dlg.ShowDialog() != true)
                    return;

                if (!int.TryParse(dlg.X, out x) || !int.TryParse(dlg.Y, out y))
                    return;

                pointViewModel.SetPointText(x, y);

                ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(Name);

                message.ConfigurePositions(new ServoPoint(pointViewModel.PositionName, x, y));

                _controller.PostMessage(message);
            }
            else
            {
                pointViewModel.Go(_controller);
            }
        }

        private void Configure(Window owner)
        {
            ServoDriveConfigurationWindow dlg = new ServoDriveConfigurationWindow();

            dlg.Owner = owner;
            dlg.DataContext = this;
            dlg.ShowDialog();
        }

        private void UpdateParameters()
        {
            var message = new ServoDriveManualControlMessage(Name);

            message.ConfigureParameters(_profile.Parameters);

            _controller.PostMessage(message);
        }

        public void RefreshIO(IORegister register)
        {
            ushort[] statusRegister = register.MotionControlStatusRegister;

            ushort statusX = statusRegister[_xAxisIndex];
            ushort statusY = statusRegister[_yAxisIndex];

            HadAxisXReturnedToHomePosition = BitOperation.GetBit(statusX, MotionControlStatusBit.ReturnedHome);
            HadAxisYReturnedToHomePosition = BitOperation.GetBit(statusY, MotionControlStatusBit.ReturnedHome);

            int xAxisCurrentFeedValue = register.MotionControlCurrentFeedValueRegister[_xAxisIndex];
            int yAxisCurrentFeedValue = register.MotionControlCurrentFeedValueRegister[_yAxisIndex];

            if (xAxisCurrentFeedValue != _xAxisCurrentFeedValue)
            {
                _xAxisCurrentFeedValue = xAxisCurrentFeedValue;
                AxisXCurrentFeedValue = xAxisCurrentFeedValue / 100.0;
                RaisePropertyChanged(nameof(AxisXCurrentFeedValue));
            }

            if (yAxisCurrentFeedValue != _yAxisCurrentFeedValue)
            {
                _yAxisCurrentFeedValue = yAxisCurrentFeedValue;
                AxisYCurrentFeedValue = yAxisCurrentFeedValue / 100.0;
                RaisePropertyChanged(nameof(AxisYCurrentFeedValue));
            }

            ushort xAxisCurrentPositionId = (ushort)(register.MotionControlCurrentPositionIdRegister[_xAxisIndex] & 0xFFFF);
            ushort yAxisCurrentPositionId = (ushort)(register.MotionControlCurrentPositionIdRegister[_yAxisIndex] & 0xFFFF);

            if (xAxisCurrentPositionId == 0 | yAxisCurrentPositionId == 0 | xAxisCurrentPositionId != yAxisCurrentPositionId)
            {
                if (_onPoint != null)
                {
                    _onPoint.IsOn = false;
                    _onPoint = null;
                }
            }
            else
            {
                ushort id = xAxisCurrentPositionId;

                if (_onPoint != null)
                {
                    if (id == _onPoint.PositionId)
                        return;

                    _onPoint.IsOn = false;
                }

                int min = 0;
                int max = _points.Length - 1;

                while (min <= max)
                {
                    int mid = (min + max) / 2;

                    ServoDrivePointViewModel point = _points[mid];
                    ushort pid = point.PositionId;

                    if (id == pid)
                    {
                        point.IsOn = true;
                        _onPoint = point;
                        return;
                    }

                    if (id < pid)
                    {
                        max = mid - 1;
                    }
                    else
                    {
                        min = mid + 1;
                    }
                }

                throw new BugException("Should not go in here.");
            }
        }
    }
}
