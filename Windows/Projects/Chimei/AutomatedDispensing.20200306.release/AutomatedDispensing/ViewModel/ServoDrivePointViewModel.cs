﻿using AutomatedDispensing.View.Dialogs;
using Automation.Core.Controller;
using Automation.Core.Messages.ManualControl;
using Automation.Profiles.ServoDrive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Widget.WPF;

namespace AutomatedDispensing.ViewModel
{
    class ServoDrivePointViewModel : ViewModelBase
    {
        private readonly string _xyMotionName;
        private readonly ServoDrivePosition _position;

        private byte _row = 1;
        private byte _column = 1;

        private bool _isOn;

        public string PositionName { get { return _position.Name; } }

        public ushort PositionId { get { return _position.Id; } }

        public string DisplayName { get; }

        public string PointText { get; private set; }

        public bool IsOn
        {
            get { return _isOn; }
            set
            {
                if (value != _isOn)
                {
                    _isOn = value;
                    RaisePropertyChanged(nameof(IsOn));
                }
            }
        }

        public byte Rows { get { return _position.Rows; } }

        public byte Row
        {
            get { return _row; }
            set
            {
                if (value == 0 | value > _position.Rows)
                    return;

                _row = value;
            }
        }

        public byte Columns { get { return _position.Columns; } }

        public byte Column
        {
            get { return _column; }
            set
            {
                if (value == 0 | value > _position.Columns)
                    return;

                _column = value;
            }
        }

        public ServoDrivePointViewModel(string xyMotionName, string servoDriveName, ServoDrivePosition position, int x, int y)
        {
            _xyMotionName = xyMotionName;
            _position = position;

            DisplayName = Environment.PositionIni.GetValue(servoDriveName, position.Id.ToString());
            SetPointText(x, y);
        }

        public void SetPointText(int x, int y)
        {
            PointText = $"x<{(x / 100.0).ToString("#0.00")}> y<{(y / 100.0).ToString("#0.00")}>";
            RaisePropertyChanged(nameof(PointText));
        }

        public void Go(SupervisoryController controller)
        {
            string positionName = _position.Name;

            if (_position.IsArray)
            {
                ArrayNumberSelectorWindow dlg = new ArrayNumberSelectorWindow();
                dlg.Owner = Application.Current.MainWindow;
                dlg.DataContext = this;
                if (dlg.ShowDialog() != true)
                    return;

                positionName = $"{positionName} [{((_row - 1) * Columns + _column).ToString()}]";
            }

            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(_xyMotionName);

            message.Go(positionName);

            controller.PostMessage(message);
        }
    }
}
