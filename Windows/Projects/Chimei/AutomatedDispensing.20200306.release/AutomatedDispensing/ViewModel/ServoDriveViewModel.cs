﻿using AutomatedDispensing.Controller;
using AutomatedDispensing.Model.Interfaces;
using AutomatedDispensing.View.Dialogs;
using Automation.Core.Messages.ManualControl;
using Automation.Core.Registers;
using Automation.Profiles.ServoDrive;
using SwissKnife;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Xml.Serialization;
using Unity;
using Widget.WPF;
using Widget.WPF.Keypads;

namespace AutomatedDispensing.ViewModel
{
    class ServoDriveViewModel : ViewModelBase, IIOObserver
    {
        private readonly byte _axisIndex;
        private readonly ServoDriveProfile _profile;
        private readonly ServoDrivePositionViewModel[] _positions;
        private readonly AutomatedDispensingController _controller;

        private int _jogSpeed;
        private bool _isJogOperation;
        private bool _hadReturnedToHomePosition;
        private int _currentFeedValue;
        private double _currentTorque;
        private ServoDrivePositionViewModel _onPosition;

        public string Name { get; }

        public string AxisId { get; }

        public bool IsΘ { get { return _profile.IsΘ; } }

        public bool HadReturnedToHomePosition
        {
            get { return _hadReturnedToHomePosition; }
            set
            {
                if (value != _hadReturnedToHomePosition)
                {
                    _hadReturnedToHomePosition = value;

                    RaisePropertyChanged(nameof(HadReturnedToHomePosition));
                }
            }
        }

        public bool IsJogOperation
        {
            get { return _isJogOperation; }
            set
            {
                if (IsΘ)
                    return;

                if (value != _isJogOperation)
                {
                    _isJogOperation = value;

                    RaisePropertyChanged(nameof(IsJogOperation));
                }
            }
        }

        public int JogSpeed
        {
            get { return _jogSpeed; }
            set
            {
                if (value <= 0)
                    return;

                if (value > _profile.Parameters.JogSpeedLimit)
                    value = _profile.Parameters.JogSpeedLimit;

                if (value != _jogSpeed)
                {
                    _jogSpeed = value;

                    if (_profile.IsΘ)
                    {
                        _profile.Parameters.JogSpeed = value;
                        UpdateParameters();
                    }
                }
            }
        }

        public double CurrentFeedValue { get; private set; }

        public double CurrentTorque
        {
            get { return _currentTorque; }
            set
            {
                if (value != _currentTorque)
                {
                    _currentTorque = value;

                    RaisePropertyChanged(nameof(CurrentTorque));
                }
            }
        }

        public int AccelerationTime
        {
            get { return _profile.Parameters.AccelerationTime; }
            set
            {
                if (IsΘ | value <= 0)
                    return;

                var parameters = _profile.Parameters;

                if (value != parameters.AccelerationTime)
                {
                    parameters.AccelerationTime = value;
                    parameters.DecelerationTime = value;

                    UpdateParameters();
                }
            }
        }

        public int PositioningSpeed
        {
            get { return _profile.Parameters.PositioningSpeed; }
            set
            {
                if (IsΘ | value <= 0)
                    return;

                var parameters = _profile.Parameters;

                if (value != parameters.PositioningSpeed)
                {
                    parameters.SpeedLimit = value;
                    parameters.PositioningSpeed = value;

                    UpdateParameters();
                }
            }
        }

        public IEnumerable<ServoDrivePositionViewModel> Positions { get { return _positions; } }

        public ICommand ReturnToHomePositionCommand { get; }

        public ICommand StopAxisCommand { get; }

        public ICommand StartJogReverseCommand { get; }

        public ICommand StartJogForwardCommand { get; }

        public ICommand StopJogCommand { get; }

        public ICommand GoOrUpdatePositioningValueCommand { get; }

        public ICommand ConfigureCommand { get; }

        public ServoDriveViewModel(string name, byte axisId)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            if (axisId == 0)
                throw new ArgumentException("The 'axisId' argument must be greater than zero.", "axisId");

            _axisIndex = (byte)(axisId - 1);

            using (var reader = new StreamReader(Path.Combine(Environment.EquipmentProfileRootFolder, $"{name}.xml"), Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(ServoDriveProfile));

                _profile = (ServoDriveProfile)serializer.Deserialize(reader);
                _profile.CheckAfterDeserialization();
                _jogSpeed = _profile.Parameters.JogSpeed;
            }

            ServoDrivePosition[] axisPositions = _profile.Positions;

            _positions = new ServoDrivePositionViewModel[axisPositions.Length];

            int i = 0;
            foreach (var pos in axisPositions)
            {
                _positions[i++] = new ServoDrivePositionViewModel(name, pos);
            }

            Array.Sort(_positions, ((x, y) => { return x.PositionId - y.PositionId; }));

            _controller = Environment.UnityContainer.Resolve<AutomatedDispensingController>();

            Name = name;
            AxisId = $"ARM{axisId.ToString()}";

            ReturnToHomePositionCommand = new RelayCommand(ReturnToHomePosition);
            StopAxisCommand = new RelayCommand(StopAxis);
            StartJogReverseCommand = new RelayCommand(StartJogReverse);
            StartJogForwardCommand = new RelayCommand(StartJogForward);
            StopJogCommand = new RelayCommand(StopJog);
            GoOrUpdatePositioningValueCommand = new RelayCommand<ServoDrivePositionViewModel>(GoOrUpdatePositioningValue);
            ConfigureCommand = new RelayCommand<Window>(Configure);

            if (IsΘ)
                IsJogOperation = true;

            Environment.IOWatcher.PersistentObservers.Add(this);
        }

        private void ReturnToHomePosition()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(Name);

            message.ReturnToHomePosition();

            _controller.PostMessage(message);
        }

        private void StopAxis()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(Name);

            message.StopAxis();

            _controller.PostMessage(message);
        }

        private void StartJogReverse()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(Name);

            message.StartJog(-_jogSpeed);

            _controller.PostMessage(message);
        }

        private void StartJogForward()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(Name);

            message.StartJog(_jogSpeed);

            _controller.PostMessage(message);
        }

        private void StopJog()
        {
            ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(Name);

            message.StopJog();

            _controller.PostMessage(message);
        }

        private void GoOrUpdatePositioningValue(ServoDrivePositionViewModel positionViewModel)
        {
            if (_isJogOperation)
            {
                if (IsΘ)
                    return;

                NumericKeypad keypad = new NumericKeypad();
                keypad.Owner = Application.Current.MainWindow;
                if (keypad.ShowDialog() != true)
                    return;

                string result = keypad.Result;
                int value;

                if (!int.TryParse(result, out value))
                    return;


                positionViewModel.SetPositionText(value);

                ServoDriveManualControlMessage message = new ServoDriveManualControlMessage(Name);

                message.ConfigurePositions(new ServoPosition(positionViewModel.PositionName, value));

                _controller.PostMessage(message);
            }
            else
            {
                positionViewModel.Go(_controller);
            }
        }

        private void Configure(Window owner)
        {
            if (IsΘ)
                return;

            ServoDriveConfigurationWindow dlg = new ServoDriveConfigurationWindow();

            dlg.Owner = owner;
            dlg.DataContext = this;
            dlg.ShowDialog();
        }

        private void UpdateParameters()
        {
            var message = new ServoDriveManualControlMessage(Name);

            message.ConfigureParameters(_profile.Parameters);

            _controller.PostMessage(message);
        }

        public void RefreshIO(IORegister register)
        {
            ushort status = register.MotionControlStatusRegister[_axisIndex];

            HadReturnedToHomePosition = BitOperation.GetBit(status, MotionControlStatusBit.ReturnedHome);

            if (IsΘ)
                CurrentTorque = register.MotionControlCurrentTorqueRegister[_axisIndex] / 10.0;
            else
            {
                int currentFeedValue = register.MotionControlCurrentFeedValueRegister[_axisIndex];

                if (currentFeedValue != _currentFeedValue)
                {
                    _currentFeedValue = currentFeedValue;
                    CurrentFeedValue = _currentFeedValue / 100.0;
                    RaisePropertyChanged(nameof(CurrentFeedValue));
                }

                ushort currentPositionId = (ushort)(register.MotionControlCurrentPositionIdRegister[_axisIndex] & 0xFFFF);

                if (currentPositionId == 0)
                {
                    if (_onPosition != null)
                    {
                        _onPosition.IsOn = false;
                        _onPosition = null;
                    }
                }
                else
                {
                    if (_onPosition != null)
                    {
                        if (currentPositionId == _onPosition.PositionId)
                            return;

                        _onPosition.IsOn = false;
                    }

                    int min = 0;
                    int max = _positions.Length - 1;

                    while (min <= max)
                    {
                        int mid = (min + max) / 2;

                        ServoDrivePositionViewModel position = _positions[mid];
                        ushort pid = position.PositionId;

                        if (currentPositionId == pid)
                        {
                            position.IsOn = true;
                            _onPosition = position;
                            return;
                        }

                        if (currentPositionId < pid)
                        {
                            max = mid - 1;
                        }
                        else
                        {
                            min = mid + 1;
                        }
                    }

                    throw new BugException("Should not go in here.");
                }
            }
        }
    }
}
