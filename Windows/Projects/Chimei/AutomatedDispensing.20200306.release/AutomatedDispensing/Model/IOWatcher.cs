﻿using AutomatedDispensing.Controller;
using AutomatedDispensing.Model.Interfaces;
using Automation.Core.Registers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using Unity;

namespace AutomatedDispensing.Model
{
    class IOWatcher
    {
        private readonly AutomatedDispensingController _controller;
        private readonly IORegister _ioRegister;
        private readonly DispatcherTimer _timer;

        public List<IIOObserver> PersistentObservers { get; } = new List<IIOObserver>();

        public List<IIOObserver> Observers { get; } = new List<IIOObserver>();

        public IOWatcher()
        {
            _controller = Environment.UnityContainer.Resolve<AutomatedDispensingController>();

            _ioRegister = _controller.IORegister.Clone() as IORegister;

            _timer = new DispatcherTimer();
            _timer.Tick += new EventHandler(DispatcherTimer_Tick);
            _timer.Interval = TimeSpan.FromMilliseconds(50);
            _timer.Start();
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            _controller.ReadIORegister(_ioRegister);

            foreach (var observer in PersistentObservers)
            {
                observer.RefreshIO(_ioRegister);
            }

            foreach (var observer in Observers)
            {
                observer.RefreshIO(_ioRegister);
            }
        }
    }
}
