﻿using AutomatedDispensing.Model;
using AutomatedDispensing.ViewModel;
using SwissKnife;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;

namespace AutomatedDispensing
{
    static class Environment
    {
        private static readonly IDictionary<string, CylinderViewModel> _cylinderViewModels = new Dictionary<string, CylinderViewModel>(StringComparer.OrdinalIgnoreCase);
        private static readonly IDictionary<string, ServoDriveViewModel> _servoDriveViewModels = new Dictionary<string, ServoDriveViewModel>(StringComparer.OrdinalIgnoreCase);

        public static IUnityContainer UnityContainer { get; set; }

        public static string EquipmentProfileRootFolder { get; set; }

        public static string StringResourceRootFolder { get; set; }

        public static IniParser PositionIni { get; set; }

        public static IOWatcher IOWatcher { get; set; }

        public static XYLinearMotionViewModel EmptyBottleTransferViewModel { get; set; }

        public static XYLinearMotionViewModel DispensingTransferViewModel { get; set; }

        public static XYLinearMotionViewModel BottlePickupTransferViewModel { get; set; }

        public static void AddCylinderViewModel(CylinderViewModel viewModel)
        {
            _cylinderViewModels.Add(viewModel.Name, viewModel);
        }

        public static CylinderViewModel GetCylinderViewModel(string name)
        {
            if (!_cylinderViewModels.TryGetValue(name, out CylinderViewModel viewModel))
                throw new NotFoundException($"Cylinder ViewModel '{name}' was not found.");

            return viewModel;
        }

        public static void AddServoDriveViewModel(ServoDriveViewModel viewModel)
        {
            _servoDriveViewModels.Add(viewModel.Name, viewModel);
        }

        public static ServoDriveViewModel GetServoDriveViewModel(string name)
        {
            if (!_servoDriveViewModels.TryGetValue(name, out ServoDriveViewModel viewModel))
                throw new NotFoundException($"ServoDrive ViewModel '{name}' was not found.");

            return viewModel;
        }
    }
}
