﻿using AutomatedDispensing.Controller;
using AutomatedDispensing.Controller.Messages.OneCycleMessages.InkjetPrinterModule;
using AutomatedDispensing.Controller.Peripheral;
using AutomatedDispensing.Model.Interfaces;
using AutomatedDispensing.ViewModel;
using Automation.Core.Messages;
using Automation.Core.Messages.ManualControl;
using Automation.Core.Registers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Unity;
using Widget.WPF;

namespace AutomatedDispensing.Tuning.ViewModel
{
    enum InkjetPattern : byte
    {
        OnSiteSample
    }

    class InkjetPrinterModuleViewModel : ViewModelBase, IIOObserver
    {
        private readonly Dispatcher _dispatcher;
        private readonly AutomatedDispensingController _controller;
        private readonly CylinderViewModel[] _cylinderViewModels;

        private InkjetPattern _inkjetPattern = InkjetPattern.OnSiteSample;
        private string _lotNumber = string.Empty;
        private string _plantUnit = string.Empty;
        private string _samplingPoint = string.Empty;
        private string _mic = string.Empty;

        private string _barcode = string.Empty;
        private bool _isHollowMotorSuctionOn;
        private bool _isScript1Running;
        private bool _isScript2Running;
        private bool _isScript3Running;

        public IEnumerable<CylinderViewModel> CylinderViewModels { get { return _cylinderViewModels; } }

        public bool IsHollowMotorSuctionOn
        {
            get { return _isHollowMotorSuctionOn; }
            set
            {
                if (value != _isHollowMotorSuctionOn)
                {
                    _controller.PostMessage(new IOMessage(AutomatedDispensingController.HollowMotorSuctionOutput, value));

                    _isHollowMotorSuctionOn = value;
                    RaisePropertyChanged(nameof(IsHollowMotorSuctionOn));
                }
            }
        }

        public bool IsScript1Running
        {
            get { return _isScript1Running; }
            set
            {
                if (value != _isScript1Running)
                {
                    _isScript1Running = value;
                    RaisePropertyChanged(nameof(IsScript1Running));
                }
            }
        }

        public bool IsScript2Running
        {
            get { return _isScript2Running; }
            set
            {
                if (value != _isScript2Running)
                {
                    _isScript2Running = value;
                    RaisePropertyChanged(nameof(IsScript2Running));
                }
            }
        }

        public bool IsScript3Running
        {
            get { return _isScript3Running; }
            set
            {
                if (value != _isScript3Running)
                {
                    _isScript3Running = value;
                    RaisePropertyChanged(nameof(IsScript3Running));
                }
            }
        }

        public string Barcode
        {
            get { return _barcode; }
            set
            {
                value = value ?? string.Empty;

                if (value != _barcode)
                {
                    _barcode = value;
                    RaisePropertyChanged(nameof(Barcode));
                }
            }
        }

        public InkjetPattern InkjetPattern
        {
            get { return _inkjetPattern; }
            set
            {
                if (value != _inkjetPattern)
                {
                    _inkjetPattern = value;
                    RaisePropertyChanged(nameof(InkjetPattern));
                }
            }
        }

        public string LotNumber
        {
            get { return _lotNumber; }
            set
            {
                if (value != _lotNumber)
                {
                    _lotNumber = value;
                    RaisePropertyChanged(nameof(LotNumber));
                }
            }
        }

        public string PlantUnit
        {
            get { return _plantUnit; }
            set
            {
                if (value != _plantUnit)
                {
                    _plantUnit = value;
                    RaisePropertyChanged(nameof(PlantUnit));
                }
            }
        }

        public string SamplingPoint
        {
            get { return _samplingPoint; }
            set
            {
                if (value != _samplingPoint)
                {
                    _samplingPoint = value;
                    RaisePropertyChanged(nameof(SamplingPoint));
                }
            }
        }

        public string MIC
        {
            get { return _mic; }
            set
            {
                if (value != _mic)
                {
                    _mic = value;
                    RaisePropertyChanged(nameof(MIC));
                }
            }
        }

        public ICommand RunHollowMotorScriptCommand { get; }

        public ICommand StopHollowMotorCommand { get; }

        public ICommand ScanBarcodeCommand { get; }

        public ICommand UseInkjetPatternTemplateCommand { get; }

        public ICommand RunOneCycleCommand { get; }

        public InkjetPrinterModuleViewModel()
        {
            _dispatcher = Dispatcher.CurrentDispatcher;

            _cylinderViewModels = new CylinderViewModel[]
            {
                Environment.GetCylinderViewModel("噴碼前後移載汽缸"),
                Environment.GetCylinderViewModel("噴碼左右移載汽缸"),
                Environment.GetCylinderViewModel("噴碼上下移載汽缸"),
                Environment.GetCylinderViewModel("噴碼翻轉汽缸"),
                Environment.GetCylinderViewModel("噴碼夾爪汽缸")
            };

            _controller = Environment.UnityContainer.Resolve<AutomatedDispensingController>();

            RunHollowMotorScriptCommand = new RelayCommand<string>(RunHollowMotorScript);
            StopHollowMotorCommand = new RelayCommand(StopHollowMotor);
            ScanBarcodeCommand = new RelayCommand(ScanBarcode);
            UseInkjetPatternTemplateCommand = new RelayCommand(UseInkjetPatternTemplate);
            RunOneCycleCommand = new RelayCommand(RunOneCycle);
        }

        public void Load()
        {
            Environment.IOWatcher.Observers.Add(this);
        }

        public void Unload()
        {
            Environment.IOWatcher.Observers.Remove(this);
        }

        private void RunHollowMotorScript(string name)
        {
            _controller.PostMessage(new IOStepperMotorManualControlMessage("中空馬達", name));
        }

        private void StopHollowMotor()
        {
            _controller.PostMessage(new IOStepperMotorManualControlMessage("中空馬達"));
        }

        private void ScanBarcode()
        {
            Barcode = string.Empty;

            BarcodeScannerManualControlMessage message = new BarcodeScannerManualControlMessage("BCR-1",
                (msg) =>
                {
                    _dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        (ThreadStart)delegate ()
                        {
                            BarcodeScannerManualControlMessage barcodeScanner = msg as BarcodeScannerManualControlMessage;

                            Barcode = barcodeScanner.Barcode;
                        });
                });

            _controller.PostMessage(message);
        }

        private void UseInkjetPatternTemplate()
        {
            LotNumber = "890004204724";
            PlantUnit = "8001BP";
            SamplingPoint = "D201P";
            MIC = "TS000002";
        }

        private void RunOneCycle()
        {
            Barcode = string.Empty;

            InkjetPrinterDocument document = new InkjetPrinterDocument(_lotNumber, _plantUnit, _samplingPoint, _mic);

            var message = new InkjetPrinterModuleOneCycle(document, OnBarcodeReceived);

            _controller.PostMessage(message);
        }

        private void OnBarcodeReceived(string barcode)
        {
            _dispatcher.BeginInvoke(DispatcherPriority.Normal,
                (ThreadStart)delegate ()
                {
                    Barcode = barcode;
                });
        }

        public void RefreshIO(IORegister register)
        {
            bool on = register.GetCoil(AutomatedDispensingController.HollowMotorSuctionOutput);

            if (on != _isHollowMotorSuctionOn)
            {
                _isHollowMotorSuctionOn = on;
                RaisePropertyChanged(nameof(IsHollowMotorSuctionOn));
            }

            IsScript1Running = register.GetCoil(new BitCell(6030, 0xD));
            IsScript2Running = register.GetCoil(new BitCell(6030, 0xE));
            IsScript3Running = register.GetCoil(new BitCell(6030, 0xF));
        }
    }
}
