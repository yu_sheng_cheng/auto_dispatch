﻿using AutomatedDispensing.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Tuning.ViewModel
{
    class BOM1CylinderViewModel
    {
        private readonly CylinderViewModel[] _cylinderViewModels;

        public IEnumerable<CylinderViewModel> CylinderViewModels { get { return _cylinderViewModels; } }

        public BOM1CylinderViewModel()
        {
            _cylinderViewModels = new CylinderViewModel[]
            {
                Environment.GetCylinderViewModel("60ml倒液汽缸"),
                Environment.GetCylinderViewModel("60ml倒液夾瓶汽缸"),
                Environment.GetCylinderViewModel("60ml吹氣上下汽缸"),
                Environment.GetCylinderViewModel("60ml吹氣汽缸"),
                Environment.GetCylinderViewModel("1.5ml倒液汽缸"),
                Environment.GetCylinderViewModel("1.5ml倒液夾瓶汽缸"),
                Environment.GetCylinderViewModel("1.5ml吹氣上下汽缸"),
                Environment.GetCylinderViewModel("1.5ml吹氣汽缸"),
                Environment.GetCylinderViewModel("噴碼前後移載汽缸"),
                Environment.GetCylinderViewModel("噴碼左右移載汽缸"),
                Environment.GetCylinderViewModel("噴碼上下移載汽缸"),
                Environment.GetCylinderViewModel("噴碼翻轉汽缸"),
                Environment.GetCylinderViewModel("噴碼夾爪汽缸"),
                Environment.GetCylinderViewModel("空噴汽缸"),
                Environment.GetCylinderViewModel("1.5ml抱瓶汽缸"),
                Environment.GetCylinderViewModel("1.5ml夾瓶汽缸"),
                Environment.GetCylinderViewModel("60ml移載夾爪上下汽缸"),
                Environment.GetCylinderViewModel("60ml移載夾爪汽缸")
            };
        }
    }
}
