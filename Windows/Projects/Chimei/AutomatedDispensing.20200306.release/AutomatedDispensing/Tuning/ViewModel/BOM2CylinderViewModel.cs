﻿using AutomatedDispensing.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Tuning.ViewModel
{
    class BOM2CylinderViewModel
    {
        private readonly CylinderViewModel[] _cylinderViewModels;

        public IEnumerable<CylinderViewModel> CylinderViewModels { get { return _cylinderViewModels; } }

        public BOM2CylinderViewModel()
        {
            _cylinderViewModels = new CylinderViewModel[]
            {
                Environment.GetCylinderViewModel("藥劑瓶開蓋上下汽缸"),
                Environment.GetCylinderViewModel("藥劑瓶開蓋身體夾爪汽缸"),
                Environment.GetCylinderViewModel("樣品瓶開蓋上下汽缸"),
                Environment.GetCylinderViewModel("樣品瓶身體夾爪汽缸"),
                Environment.GetCylinderViewModel("60ml中繼站汽缸"),
                Environment.GetCylinderViewModel("藥劑瓶上下汽缸"),
                Environment.GetCylinderViewModel("藥劑瓶夾爪汽缸"),
                Environment.GetCylinderViewModel("微量天秤遮風罩汽缸"),
                Environment.GetCylinderViewModel("混合液汽缸"),
                Environment.GetCylinderViewModel("混合液夾爪汽缸"),
                Environment.GetCylinderViewModel("60ml開蓋左右移載汽缸"),
                Environment.GetCylinderViewModel("60ml開蓋上下移載汽缸"),
                Environment.GetCylinderViewModel("60ml開蓋上夾爪汽缸"),
                Environment.GetCylinderViewModel("吸蓋上下汽缸"),
                Environment.GetCylinderViewModel("10ml夾爪本體汽缸"),
                Environment.GetCylinderViewModel("10ml夾軸芯汽缸"),
                Environment.GetCylinderViewModel("10ml接漏液汽缸"),
                Environment.GetCylinderViewModel("1ml接漏液汽缸"),
                Environment.GetCylinderViewModel("60ml上下移載汽缸"),
                Environment.GetCylinderViewModel("60ml配藥移載夾爪汽缸")
            };
        }
    }
}
