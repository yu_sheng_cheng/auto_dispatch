﻿using AutomatedDispensing.Controller;
using AutomatedDispensing.Model.Interfaces;
using AutomatedDispensing.ViewModel;
using Automation.Core.Messages;
using Automation.Core.Registers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity;
using Widget.WPF;

namespace AutomatedDispensing.Tuning.ViewModel
{
    class DispensingGantryModuleViewModel : ViewModelBase, IIOObserver
    {
        private readonly AutomatedDispensingController _controller;
        private readonly CylinderViewModel[] _cylinderViewModelsA;
        private readonly CylinderViewModel[] _cylinderViewModelsB;

        private bool _isSuctionOn;

        public XYLinearMotionViewModel DispensingTransfer { get; }

        public ServoDriveViewModel ARM3 { get; }

        public ServoDriveViewModel ARM11 { get; }

        public ServoDriveViewModel ARM12 { get; }

        public IEnumerable<CylinderViewModel> CylinderViewModelsA { get { return _cylinderViewModelsA; } }

        public IEnumerable<CylinderViewModel> CylinderViewModelsB { get { return _cylinderViewModelsB; } }

        public bool IsSuctionOn
        {
            get { return _isSuctionOn; }
            set
            {
                if (value != _isSuctionOn)
                {
                    _controller.PostMessage(new IOMessage(new Automation.Core.Registers.BitCell(6026, 0xE), value));

                    _isSuctionOn = value;
                    RaisePropertyChanged(nameof(IsSuctionOn));
                }
            }
        }

        public DispensingGantryModuleViewModel()
        {
            DispensingTransfer = Environment.DispensingTransferViewModel;
            ARM11 = Environment.GetServoDriveViewModel("ARM11 Z1 1ml取TIP手臂");
            ARM12 = Environment.GetServoDriveViewModel("ARM12 Z2 10ml取TIP手臂");

            _cylinderViewModelsA = new CylinderViewModel[]
            {
                Environment.GetCylinderViewModel("混合液夾爪汽缸"),
                Environment.GetCylinderViewModel("60ml上下移載汽缸"),
                Environment.GetCylinderViewModel("60ml配藥移載夾爪汽缸")
            };

            _cylinderViewModelsB = new CylinderViewModel[]
            {
                Environment.GetCylinderViewModel("吸蓋上下汽缸"),
                Environment.GetCylinderViewModel("10ml夾爪本體汽缸"),
                Environment.GetCylinderViewModel("微量天秤遮風罩汽缸")
            };

            _controller = Environment.UnityContainer.Resolve<AutomatedDispensingController>();
        }

        public void RefreshIO(IORegister register)
        {
            bool on = register.GetCoil(new Automation.Core.Registers.BitCell(6026, 0xE));

            if (on != _isSuctionOn)
            {
                _isSuctionOn = on;
                RaisePropertyChanged(nameof(IsSuctionOn));
            }
        }
    }
}
