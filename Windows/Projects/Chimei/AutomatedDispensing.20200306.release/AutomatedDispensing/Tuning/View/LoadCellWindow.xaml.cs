﻿using AutomatedDispensing.Model.Interfaces;
using Automation.Core.Registers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AutomatedDispensing.Tuning.View
{
    /// <summary>
    /// LoadCellWindow.xaml 的互動邏輯
    /// </summary>
    public partial class LoadCellWindow : Window, IIOObserver
    {
        public LoadCellWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Environment.IOWatcher.Observers.Add(this);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Environment.IOWatcher.Observers.Remove(this);
        }

        public void RefreshIO(IORegister register)
        {
            TextBlock1.Text = register.GetAnalogInput(6640).ToString(); //藥劑瓶手動入料
            TextBlock2.Text = register.GetAnalogInput(6641).ToString(); //藥劑瓶自動入料
            TextBlock3.Text = register.GetAnalogInput(6642).ToString(); //樣品瓶
            TextBlock4.Text = register.GetAnalogInput(6643).ToString();
        }
    }
}
