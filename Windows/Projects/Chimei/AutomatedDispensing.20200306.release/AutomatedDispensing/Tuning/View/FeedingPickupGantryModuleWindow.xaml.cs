﻿using AutomatedDispensing.ViewModel;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Widget;

namespace AutomatedDispensing.Tuning.View
{
    /// <summary>
    /// FeedingPickupGantryWindow.xaml 的互動邏輯
    /// </summary>
    public partial class FeedingPickupGantryModuleWindow : MetroWindow
    {
        private readonly CylinderViewModel[] _cylinderViewModels;
        private readonly CylinderViewModel[] _cylinderViewModels2;

        public IEnumerable<CylinderViewModel> CylinderViewModels { get { return _cylinderViewModels; } }

        public IEnumerable<CylinderViewModel> CylinderViewModels2 { get { return _cylinderViewModels2; } }

        public FeedingPickupGantryModuleWindow()
        {
            InitializeComponent();

            _cylinderViewModels = new CylinderViewModel[]
            {
                Environment.GetCylinderViewModel("60ml倒液夾瓶汽缸"),
                Environment.GetCylinderViewModel("1.5ml倒液夾瓶汽缸"),
                Environment.GetCylinderViewModel("噴碼翻轉汽缸")
            };
            _cylinderViewModels2 = new CylinderViewModel[]
            {
                Environment.GetCylinderViewModel("噴碼夾爪汽缸"),
                Environment.GetCylinderViewModel("60ml移載夾爪上下汽缸"),
                Environment.GetCylinderViewModel("60ml移載夾爪汽缸")
            };

            DataContext = this;
        }

        private void MetroWindow_SourceInitialized(object sender, EventArgs e)
        {
            HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);
            source.AddHook(new HwndSourceHook(WndProc));
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case Win32.WM_SYSCOMMAND:
                    switch (wParam.ToInt32() & 0xFFF0)
                    {
                        case Win32.SC_MOVE:
                        case Win32.SC_RESTORE:
                            handled = true;
                            break;
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            FeedingPickupGantryControl.DataContext = Environment.EmptyBottleTransferViewModel;
            ARM3Control.DataContext = Environment.GetServoDriveViewModel("ARM3 X 送料手臂");
            ARM4Control.DataContext = Environment.GetServoDriveViewModel("ARM4 Z 1.5ml夾取手臂");
        }
    }
}
