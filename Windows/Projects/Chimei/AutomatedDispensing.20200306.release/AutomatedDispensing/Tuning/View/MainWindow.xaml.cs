﻿using AutomatedDispensing.Tuning.ViewModel;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Widget;

namespace AutomatedDispensing.Tuning.View
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private DispensingGantryModuleViewModel _dispensingGantryModuleViewModel;
        private InkjetPrinterModuleViewModel _inkjetPrinterModuleViewModel;
        private BOM1CylinderViewModel _bom1CylinderViewModel;
        private BOM2CylinderViewModel _bom2CylinderViewModel;
        private BOM3CylinderViewModel _bom3CylinderViewModel;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MetroWindow_SourceInitialized(object sender, EventArgs e)
        {
            HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);
            //source.AddHook(new HwndSourceHook(WndProc));
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case Win32.WM_SYSCOMMAND:
                    switch (wParam.ToInt32() & 0xFFF0)
                    {
                        case Win32.SC_MOVE:
                        case Win32.SC_RESTORE:
                            handled = true;
                            break;
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        private void DispensingGantryModuleButton_Click(object sender, RoutedEventArgs e)
        {
            DispensingGantryModuleWindow dlg = new DispensingGantryModuleWindow
            {
                Owner = this
            };

            if (_dispensingGantryModuleViewModel == null)
                _dispensingGantryModuleViewModel = new DispensingGantryModuleViewModel();

            dlg.DataContext = _dispensingGantryModuleViewModel;
            dlg.ShowDialog();
        }

        private void InkjetPrinterModuleButton_Click(object sender, RoutedEventArgs e)
        {
            InkjetPrinterModuleWindow dlg = new InkjetPrinterModuleWindow
            {
                Owner = this
            };

            if (_inkjetPrinterModuleViewModel == null)
                _inkjetPrinterModuleViewModel = new InkjetPrinterModuleViewModel();

            _inkjetPrinterModuleViewModel.Load();

            dlg.DataContext = _inkjetPrinterModuleViewModel;
            dlg.ShowDialog();

            _inkjetPrinterModuleViewModel.Unload();
        }

        private void BOM1CylinderButton_Click(object sender, RoutedEventArgs e)
        {
            BOM1CylinderWindow dlg = new BOM1CylinderWindow
            {
                Owner = this
            };

            if (_bom1CylinderViewModel == null)
                _bom1CylinderViewModel = new BOM1CylinderViewModel();

            dlg.DataContext = _bom1CylinderViewModel;
            dlg.ShowDialog();
        }

        private void BOM2CylinderButton_Click(object sender, RoutedEventArgs e)
        {
            BOM2CylinderWindow dlg = new BOM2CylinderWindow
            {
                Owner = this
            };

            if (_bom2CylinderViewModel == null)
                _bom2CylinderViewModel = new BOM2CylinderViewModel();

            dlg.DataContext = _bom2CylinderViewModel;
            dlg.ShowDialog();
        }

        private void BOM3CylinderButton_Click(object sender, RoutedEventArgs e)
        {
            BOM3CylinderWindow dlg = new BOM3CylinderWindow
            {
                Owner = this
            };

            if (_bom3CylinderViewModel == null)
                _bom3CylinderViewModel = new BOM3CylinderViewModel();

            dlg.DataContext = _bom3CylinderViewModel;
            dlg.ShowDialog();
        }

        private void Car2Button_Click(object sender, RoutedEventArgs e)
        {
            Car2Window dlg = new Car2Window();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void TipModuleButton_Click(object sender, RoutedEventArgs e)
        {
            TipWindow dlg = new TipWindow();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void FeedingPickupGantryModuleButton_Click(object sender, RoutedEventArgs e)
        {
            FeedingPickupGantryModuleWindow dlg = new FeedingPickupGantryModuleWindow();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void LoadCellButton_Click(object sender, RoutedEventArgs e)
        {
            LoadCellWindow dlg = new LoadCellWindow();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void ChemicalAgentAreaButton_Click(object sender, RoutedEventArgs e)
        {
            ChemicalAgentAreaWindow dlg = new ChemicalAgentAreaWindow();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void BottlePickupTransferButton_Click(object sender, RoutedEventArgs e)
        {
            BottlePickupTransferWindow dlg = new BottlePickupTransferWindow();
            dlg.Owner = this;
            dlg.ShowDialog();
        }
    }
}
