﻿using Automation.Core;
using Automation.Core.Controller;
using Automation.Core.Registers;
using PLC.Mitsubishi;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller
{
    public sealed partial class AutomatedDispensingController : SupervisoryController
    {
        private readonly IORegister _cacheIORegister;
        private readonly MelsecMCNet _melsec;

        protected override AsynchronousTimeoutSignal WatchdogExpiredSignal { get { return _melsec.TimeoutSignal; } }

        public AutomatedDispensingController() :
            base(new SupervisoryControllerConfiguration(Environment.EquipmentProfileRootFolder, Path.Combine(Environment.StringResourceRootFolder, "AlarmComment.xml")))
        {
            LogKeyword = "AutomatedDispensingController";
            ScanCycleTime = 15;
            TimeForBoot = 5000;
            TimeForAbnormalAirPressure = 30000;
            TimeForRecoveredAirPressure = 20000;
            WatchdogTimeout = 1000;

            IORegister = BuildIORegister();
            _cacheIORegister = (IORegister)IORegister.Clone();

            Build();

            IPAddress ipAddress = IPAddress.Parse(ConfigurationManager.AppSettings["PLC.IPAddress"]);
            int port = int.Parse(ConfigurationManager.AppSettings["PLC.Port"]);

            Log.i($"PLC Ethernet Setting: {ipAddress}:{port}");

            _melsec = new MelsecMCNet("Q", "Q03UDV");
            _melsec.InternetTransport.RemoteEndPoint = new IPEndPoint(ipAddress, port);
        }

        protected override void CleanUp()
        {
            base.CleanUp();

            Demolish();
            _melsec.Dispose();
        }

        protected override void Initialize()
        {

        }

        protected override void OnOperationModeChanged(OperationMode oldMode, OperationMode newMode)
        {

        }

        protected override void RefreshControllerState()
        {

        }
    }
}
