﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller
{
    public partial class AutomatedDispensingController
    {
        public const int HollowMotorSuctionTimeout = 500;
        public const int HollowMotorSuctionDoneHoldTime = 30;

        public const int KeyenceIVGTimeout = 30000;
    }
}
