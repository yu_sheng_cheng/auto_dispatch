﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.Core.Messages;

namespace AutomatedDispensing.Controller
{
    public partial class AutomatedDispensingController
    {
        protected override void OnIncomingMessage(Message message)
        {
            if (message.Class == (byte)MessageClass.SupervisoryController)
            {
                base.OnIncomingMessage(message);
                return;
            }
        }
    }
}
