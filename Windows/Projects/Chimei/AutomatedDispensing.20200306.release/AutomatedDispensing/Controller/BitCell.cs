﻿using Automation.Core.Registers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller
{
    public partial class AutomatedDispensingController
    {
        public static readonly BitCell HollowMotorVacuumGaugesInput = new BitCell(6000, 8);

        public static readonly BitCell HollowMotorSuctionOutput = new BitCell(6021, 0xC);

        public static readonly BitCell KeyenceIVGOkInput = new BitCell(6016, 9);
    }
}
