﻿using AutomatedDispensing.Controller.Peripheral;
using Automation.Core.Controller;
using Automation.Core.Messages.ManualControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Messages.OneCycleMessages.InkjetPrinterModule
{
    sealed partial class InkjetPrinterModuleOneCycle : OneCycleManualControlMessage
    {
        private Action<string> _barcodeReceived;
        private string _barcode = string.Empty;

        public InkjetPrinterDocument Document { get; }

        public bool HasBottle { get; set; }

        public string Barcode
        {
            get { return _barcode; }
            set
            {
                _barcode = value ?? string.Empty;

                if (!string.IsNullOrEmpty(value))
                    _barcodeReceived?.Invoke(_barcode);
            }
        }

        public InkjetPrinterModuleOneCycle(InkjetPrinterDocument document, Action<string> barcodeReceived = null) : base("InkjetPrinterModuleOneCycle")
        {
            Document = document;
            _barcodeReceived = barcodeReceived;
        }

        protected override OneCycleState CreateFirstState()
        {
            return new State1(this);
        }
    }
}
