﻿using Automation.Core.Controller;
using Automation.Core.Messages.ManualControl;
using Automation.Objects.Movable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Messages.OneCycleMessages.InkjetPrinterModule
{
    partial class InkjetPrinterModuleOneCycle
    {
        private sealed class State2 : OneCycleState
        {
            public State2(OneCycleManualControlMessage message) : base(message) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "噴碼夾爪汽缸", "夾持", OnInkjetGripperCylinderHoldCompleted);
            }

            private void OnInkjetGripperCylinderHoldCompleted(IMovableObject obj, MovingResult result)
            {
                if (Cancelled)
                    return;

                Complete(obj, result);
            }

            protected override OneCycleState CreateNextState()
            {
                return new State3(Message);
            }
        }
    }
}
