﻿using AutomatedDispensing.Controller.MovableObjects;
using Automation;
using Automation.Core;
using Automation.Core.Controller;
using Automation.Core.Messages.ManualControl;
using Automation.Objects.Movable;
using Automation.Peripheral.BarcodeScanners;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Messages.OneCycleMessages.InkjetPrinterModule
{
    partial class InkjetPrinterModuleOneCycle
    {
        private sealed class State11 : OneCycleState
        {
            private string _objectName;

            public State11(OneCycleManualControlMessage message) : base(message) { }

            public override bool Go(SupervisoryController controller)
            {
                /*AutomatedDispensingController controller = ctrl as AutomatedDispensingController;

                BarcodeScanner scanner = controller.BarcodeScanners[0];

                if (scanner.IsBusy)
                {
                    Message.ObjectName = scanner.Name;
                    Message.AcceptResult = ManualControlAcceptResult.Busy;
                    return false;
                }

                if (!Go(controller, "中空馬達", "Jog慢速", OnHollowMotorStopped))
                    return false;

                controller.AddIORequest(scanner, OnBarcodeScannerScanDone);

                Refcount = 2;
                return true;*/

                return Go(controller, "中空馬達", "定位", OnHollowMotorPositioningCompleted);
            }

            private void OnHollowMotorPositioningCompleted(IMovableObject obj, MovingResult result)
            {
                if (Cancelled)
                    return;

                if (result != MovingResult.Completed)
                {
                    Complete(obj, result);
                    return;
                }

                AutomatedDispensingController controller = Message.Controller as AutomatedDispensingController;

                BarcodeScanner scanner = controller.BarcodeScanners[0];

                if (scanner.IsBusy)
                {
                    Message.ObjectName = scanner.Name;
                    Message.AcceptResult = ManualControlAcceptResult.Busy;
                    Complete(false);
                    return;
                }

                //if (!Go(controller, "中空馬達", "Jog慢速", OnHollowMotorStopped))
                if (!Go(controller, "中空馬達", "定位", OnHollowMotorPositioningCompleted2))
                {
                    Complete(false);
                    return;
                }

                controller.AddIORequest(scanner, OnBarcodeScannerScanDone);

                Refcount = 2;
            }

            private void OnHollowMotorPositioningCompleted2(IMovableObject obj, MovingResult result)
            {
                if (Cancelled)
                    return;

                if (result != MovingResult.Completed)
                {
                    if (_objectName == null)
                    {
                        _objectName = obj.Name;
                        Message.ProcessResult = result.ToManualControlProcessResult();
                    }
                }

                PutRefcount(_objectName);
            }

            private void OnBarcodeScannerScanDone(IOWorker worker)
            {
                if (Cancelled)
                    return;

                BarcodeScanner scanner = worker as BarcodeScanner;

                var result = scanner.Result;

                if (!result.IsSuccess)
                {
                    //if (result.ErrorCode != (int)AutomationError.Timeout)
                   // {
                        _objectName = scanner.Name;
                        Message.ProcessResult = ManualControlProcessResult.PeripheralError;
                   // }
                }
                else
                    (Message as InkjetPrinterModuleOneCycle).Barcode = scanner.Barcode;

                var controller = Message.Controller;

                controller.TryGetMovableObject("中空馬達", out IMovableObject obj);
                (obj as HollowMotor).StopJog(controller);

                PutRefcount(_objectName);
            }

            private void OnHollowMotorStopped(IMovableObject obj, MovingResult result)
            {
                if (Cancelled)
                    return;

                if (result != MovingResult.Completed)
                {
                    if (_objectName == null)
                    {
                        _objectName = obj.Name;
                        Message.ProcessResult = result.ToManualControlProcessResult();
                    }
                }

                PutRefcount(_objectName);
            }

            protected override OneCycleState CreateNextState()
            {
                return new State12(Message);
            }
        }
    }
}
