﻿using AutomatedDispensing.Controller.MovableObjects;
using Automation.Core.Controller;
using Automation.Core.Messages.ManualControl;
using Automation.Objects.Movable;
using Automation.Peripheral.InkjetPrinters.Domino;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Messages.OneCycleMessages.InkjetPrinterModule
{
    partial class InkjetPrinterModuleOneCycle
    {
        private sealed class State10 : OneCycleState
        {
            private string _objectName;

            public State10(OneCycleManualControlMessage message) : base(message) { }

            public override bool Go(SupervisoryController ctrl)
            {
                AutomatedDispensingController controller = ctrl as AutomatedDispensingController;

                DominoInkjetPrinter printer = controller.InkjetPrinter;

                if (printer.IsBusy)
                {
                    Message.ObjectName = printer.Name;
                    Message.AcceptResult = ManualControlAcceptResult.Busy;
                    return false;
                }

                InkjetPrinterModuleOneCycle message = Message as InkjetPrinterModuleOneCycle;

                bool hasBottle = controller.IORegister.GetContact(AutomatedDispensingController.HollowMotorVacuumGaugesInput);

                if (!message.HasBottle | !hasBottle)
                {
                    message.HasBottle = false;
                    printer.Disable(controller, OnInkjetPrinterDisabled);
                    Refcount = 1;
                    return true;
                }

                if (!Go(controller, "中空馬達", "Jog快速", OnHollowMotorStopped))
                    return false;

                printer.Print(controller, OnPrintCompleted);
                return true;
            }

            private void OnPrintCompleted(DominoInkjetPrinter printer)
            {
                if (Cancelled)
                    return;

                var controller = Message.Controller;

                printer.Disable(controller, OnInkjetPrinterDisabled);

                controller.TryGetMovableObject("中空馬達", out IMovableObject obj);
                (obj as HollowMotor).StopJog(controller);

                Refcount = 2; // printer, hollow motor
            }

            private void OnInkjetPrinterDisabled(DominoInkjetPrinter printer, bool success)
            {
                if (Cancelled)
                    return;

                if (!success)
                {
                    if (_objectName == null)
                    {
                        _objectName = printer.Name;
                        Message.ProcessResult = ManualControlProcessResult.AlarmOccurred;
                    }
                }

                PutRefcount(_objectName);
            }

            private void OnHollowMotorStopped(IMovableObject obj, MovingResult result)
            {
                if (Cancelled)
                    return;

                if (result != MovingResult.Completed)
                {
                    if (_objectName == null)
                    {
                        _objectName = obj.Name;
                        Message.ProcessResult = result.ToManualControlProcessResult();
                    }
                }

                PutRefcount(_objectName);
            }

            protected override OneCycleState CreateNextState()
            {
                return new State11(Message);
            }
        }
    }
}
