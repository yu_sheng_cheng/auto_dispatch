﻿using AutomatedDispensing.Controller.MovableObjects;
using Automation.Core.Controller;
using Automation.Core.Messages.ManualControl;
using Automation.Helper;
using Automation.Objects.Movable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Messages.OneCycleMessages.InkjetPrinterModule
{
    partial class InkjetPrinterModuleOneCycle
    {
        private sealed class State8 : OneCycleState
        {
            private const byte Low = 0;
            private const byte High = 1;

            private Signal _signal = new Signal();
            private Timer _timer = new Timer();
            private bool _recognitionDone;

            public State8(OneCycleManualControlMessage message) : base(message) { }

            public override bool Go(SupervisoryController controller)
            {
                _signal.Output(High);
                return Go(controller, "中空馬達", "Jog慢速", OnHollowMotorStopped);
            }

            private void OnHollowMotorStopped(IMovableObject obj, MovingResult result)
            {
                if (Cancelled)
                    return;

                _signal.Output(Low);
                Complete(obj, result);
            }

            public override OneCycleState Poll(SupervisoryController controller)
            {
                if (!_recognitionDone)
                {
                    if (_signal.Tick(High))
                    {
                        if (controller.IORegister.GetContact(AutomatedDispensingController.KeyenceIVGOkInput))
                            _recognitionDone = true;
                        else
                        {
                            if (!_timer.IsEnabled)
                                _timer.Start(controller.GetExpirationJiffies(AutomatedDispensingController.KeyenceIVGTimeout));
                            else if (_timer.CheckTimeout(controller.Jiffies))
                                _recognitionDone = true;
                        }

                        if (_recognitionDone)
                        {
                            controller.TryGetMovableObject("中空馬達", out IMovableObject obj);
                            (obj as HollowMotor).StopJog(controller);

                            _timer.Stop();
                        }
                    }
                }

                return base.Poll(controller);
            }

            protected override OneCycleState CreateNextState()
            {
                return new State9(Message);
            }
        }
    }
}
