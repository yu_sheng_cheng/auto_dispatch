﻿using Automation.Core.Controller;
using Automation.Core.Messages.ManualControl;
using Automation.Objects.Movable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Messages.OneCycleMessages.InkjetPrinterModule
{
    partial class InkjetPrinterModuleOneCycle
    {
        private sealed class State1 : OneCycleState
        {
            public State1(OneCycleManualControlMessage message) : base(message) { }

            public override bool Go(SupervisoryController controller)
            {
                return Go(controller, "噴碼翻轉汽缸", "垂直", OnInkjetFlipCylinderCompleted);
            }

            private void OnInkjetFlipCylinderCompleted(IMovableObject obj, MovingResult result)
            {
                if (Cancelled)
                    return;

                Complete(obj, result);
            }

            protected override OneCycleState CreateNextState()
            {
                return new State2(Message);
            }
        }
    }
}
