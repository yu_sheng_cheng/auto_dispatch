﻿using Automation.Core.Controller;
using Automation.Core.Messages.ManualControl;
using Automation.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Messages.OneCycleMessages.InkjetPrinterModule
{
    partial class InkjetPrinterModuleOneCycle
    {
        private sealed class State14 : OneCycleState
        {
            private Timer _timer = new Timer();
            private Timer _doneHoldTimer = new Timer();

            public State14(OneCycleManualControlMessage message) : base(message) { }

            public override bool Go(SupervisoryController controller)
            {
                controller.IORegister.SetCoil(AutomatedDispensingController.HollowMotorSuctionOutput, false);
                return true;
            }

            public override OneCycleState Poll(SupervisoryController controller)
            {
                if (!_timer.IsEnabled)
                    _timer.Start(controller.GetExpirationJiffies(AutomatedDispensingController.HollowMotorSuctionTimeout));

                bool hasBottle = controller.IORegister.GetContact(AutomatedDispensingController.HollowMotorVacuumGaugesInput);

                if (hasBottle)
                {
                    _doneHoldTimer.Stop();

                    if (_timer.CheckTimeout(controller.Jiffies))
                    {
                        _timer.Stop();
                        Complete("中空馬達破瓶真空", ManualControlProcessResult.Timeout);
                    }
                }
                else
                {
                    if (!_doneHoldTimer.IsEnabled)
                        _doneHoldTimer.Start(controller.GetExpirationJiffies(AutomatedDispensingController.HollowMotorSuctionDoneHoldTime));
                    else if (_doneHoldTimer.CheckTimeout(controller.Jiffies))
                    {
                        _doneHoldTimer.Stop();
                        _timer.Stop();

                        Complete();
                    }
                }

                return base.Poll(controller);
            }

            protected override OneCycleState CreateNextState()
            {
                return new State15(Message);
            }
        }
    }
}
