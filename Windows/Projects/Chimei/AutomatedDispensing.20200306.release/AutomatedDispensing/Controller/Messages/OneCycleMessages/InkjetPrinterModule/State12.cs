﻿using Automation.Core.Messages.ManualControl;
using Automation.Objects.Movable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Messages.OneCycleMessages.InkjetPrinterModule
{
    partial class InkjetPrinterModuleOneCycle
    {
        private sealed class State12 : OneCycleState
        {
            public State12(OneCycleManualControlMessage message) : base(message) { }

            public override bool Go(Automation.Core.Controller.SupervisoryController controller)
            {
                return Go(controller, "噴碼前後移載汽缸", "前進", OnInkjetTransferCylinderForwardCompleted);
            }

            private void OnInkjetTransferCylinderForwardCompleted(IMovableObject obj, MovingResult result)
            {
                if (Cancelled)
                    return;

                Complete(obj, result);
            }

            protected override OneCycleState CreateNextState()
            {
                return new State13(Message);
            }
        }
    }
}
