﻿using Automation.Core.Controller;
using Automation.Core.Messages.ManualControl;
using Automation.Objects.Movable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Messages.OneCycleMessages.InkjetPrinterModule
{
    partial class InkjetPrinterModuleOneCycle
    {
        private sealed class State4 : OneCycleState
        {
            private string _objectName;

            public State4(OneCycleManualControlMessage message) : base(message) { }

            public override bool Go(SupervisoryController controller)
            {
                if (!Go(controller, "噴碼上下移載汽缸", "上升", OnInkjetTransferCylinderUpOrRightCompleted))
                    return false;

                Refcount = 2;

                if (!Go(controller, "噴碼左右移載汽缸", "往右", OnInkjetTransferCylinderUpOrRightCompleted))
                {
                    _objectName = "噴碼左右移載汽缸";
                    Refcount = 1;
                }

                return true;
            }

            private void OnInkjetTransferCylinderUpOrRightCompleted(IMovableObject obj, MovingResult result)
            {
                if (Cancelled)
                    return;

                if (result != MovingResult.Completed)
                {
                    if (_objectName == null)
                    {
                        _objectName = obj.Name;
                        Message.ProcessResult = result.ToManualControlProcessResult();
                    }
                }

                PutRefcount(_objectName);
            }

            protected override OneCycleState CreateNextState()
            {
                return new State5(Message);
            }
        }
    }
}
