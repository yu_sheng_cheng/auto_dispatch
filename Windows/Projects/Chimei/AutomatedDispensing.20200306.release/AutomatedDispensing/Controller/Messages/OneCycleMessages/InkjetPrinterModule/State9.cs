﻿using AutomatedDispensing.Controller.MovableObjects;
using AutomatedDispensing.Controller.Peripheral;
using Automation.Core.Controller;
using Automation.Core.Messages.ManualControl;
using Automation.Objects.Movable;
using Automation.Peripheral.InkjetPrinters.Domino;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.Messages.OneCycleMessages.InkjetPrinterModule
{
    partial class InkjetPrinterModuleOneCycle
    {
        private sealed class State9 : OneCycleState
        {
            private string _objectName;

            public State9(OneCycleManualControlMessage message) : base(message) { }

            public override bool Go(SupervisoryController ctrl)
            {
                AutomatedDispensingController controller = ctrl as AutomatedDispensingController;

                DominoInkjetPrinter printer = controller.InkjetPrinter;

                if (printer.IsBusy)
                {
                    Message.ObjectName = printer.Name;
                    Message.AcceptResult = ManualControlAcceptResult.Busy;
                    return false;
                }

                if (!Go(controller, "中空馬達", "定位", OnHollowMotorPositioningCompleted))
                    return false;

                InkjetPrinterModuleOneCycle message = Message as InkjetPrinterModuleOneCycle;
                InkjetPrinterDocument document = message.Document;

                printer.PrepareToPrint(controller, document.ProjectName, document.VariableTexts, OnInkjetPrinterReady);
                Refcount = 2;
                return true;
            }

            private void OnHollowMotorPositioningCompleted(IMovableObject obj, MovingResult result)
            {
                if (Cancelled)
                    return;

                if (result != MovingResult.Completed)
                {
                    if (_objectName == null)
                    {
                        _objectName = obj.Name;
                        Message.ProcessResult = result.ToManualControlProcessResult();
                    }
                }

                PutRefcount(_objectName);
            }

            private void OnInkjetPrinterReady(DominoInkjetPrinter printer, bool success)
            {
                if (Cancelled)
                    return;

                if (!success)
                {
                    if (_objectName == null)
                    {
                        _objectName = printer.Name;
                        Message.ProcessResult = ManualControlProcessResult.AlarmOccurred;
                    }
                }

                PutRefcount(_objectName);
            }

            protected override OneCycleState CreateNextState()
            {
                return new State10(Message);
            }
        }
    }
}
