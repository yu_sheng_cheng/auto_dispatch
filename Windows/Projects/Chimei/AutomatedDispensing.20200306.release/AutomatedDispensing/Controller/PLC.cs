﻿using Automation.Core.Registers;
using Automation.Objects.Movable.ServoDrive;
using SwissKnife;
using SwissKnife.Conversion;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller
{
    public partial class AutomatedDispensingController
    {
        private readonly ArrayBuilder<byte> D6000 = new ArrayBuilder<byte>();
        private readonly ushort[] _ushortArray = new ushort[18];
        private readonly int[] _intArray = new int[180];
        private readonly OneWriteSeqLock _ioRegisterSeqLock = new OneWriteSeqLock();
        private bool _firstRefresh = true;
        private bool _plcDisconnected;

        private IORegister BuildIORegister()
        {
            IOMemory contactIOMemory = new IOMemory(IOSignalType.Digital,
                new Tuple<ushort, ushort>[] { new Tuple<ushort, ushort>(0x100, 256), new Tuple<ushort, ushort>(0x300, 32) }, 6000);

            IOMemory coilIOMemory = new IOMemory(IOSignalType.Digital,
                new Tuple<ushort, ushort>[] { new Tuple<ushort, ushort>(0x100, 160), new Tuple<ushort, ushort>(0x300, 32) }, 6020);

            IOMemory analogInputIOMemory = new IOMemory(IOSignalType.Analog, 0x0, 6640, 8);

            return new IORegister(contactIOMemory, coilIOMemory, analogInputIOMemory, null, (ushort)ARMs.Length);
        }

        protected override async Task<bool> RefreshAsync()
        {
            OperationResult result = await _melsec.ReadUInt8Async("D6000", 960 * 2, D6000).ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                if (!_plcDisconnected)
                {
                    _plcDisconnected = true;
                    Log.w(LogKeyword, $"{_melsec.Type.Name} Read: {result}");
                }
                return false;
            }

            _plcDisconnected = false;

            byte[] d6000 = D6000.Buffer;

            IByteConverter byteConverter = _melsec.ByteConverter;

            var register = IORegister;

            byteConverter.ToUInt16(d6000, 0, 18 * 2, register.ContactRegisters, 0); // D6000 - D6017 (實體X)

            byteConverter.ToUInt16(d6000, 100 * 2, 18 * 2, _ushortArray, 0); // D6100 - D6117 (軸控IO讀)
            byteConverter.ToInt32(d6000, 699 * 2, 18 * 4 * 4, _intArray, 0); // D6699 - D6842 (軸控DWORD讀)

            int i = 0, j = 0;

            foreach (var arm in ARMs)
            {
                var ioMemory = arm.IOMemory;

                ushort value = _ushortArray[i];

                ioMemory.HadReturnedToHomePosition = BitOperation.GetBit(value, 0);
                ioMemory.IsPositioningCompleted = BitOperation.GetBit(value, 1);
                ioMemory.IsBusy = BitOperation.GetBit(value, 2);
                ioMemory.HasErrors = BitOperation.GetBit(value, 3);
                ioMemory.IsReturningToHomePosition = BitOperation.GetBit(value, 4);

                ioMemory.CurrentFeedValue = _intArray[j++];
                ioMemory.ErrorCode = unchecked((ushort)_intArray[j++]);
                ioMemory.CurrentTorque = _intArray[j++];
                ++j;

                ushort status = 0;

                if (arm.IsΘ | ioMemory.HadReturnedToHomePosition)
                    status |= 1 << MotionControlStatusBit.ReturnedHome;
                if (ioMemory.IsBusy)
                    status |= 1 << MotionControlStatusBit.Busy;

                register.MotionControlStatusRegister[i] = status;
                register.MotionControlCurrentFeedValueRegister[i] = ioMemory.CurrentFeedValue;
                register.MotionControlCurrentTorqueRegister[i] = ioMemory.CurrentTorque;

                arm.TryGetCurrentPosition(out string positionName, out uint positionId);
                register.MotionControlCurrentPositionIdRegister[i] = positionId;
                ++i;
            }

            foreach (var arm in XYARMs)
            {
                arm.TryGetCurrentPosition(out string positionName, out uint positionId);

                register.MotionControlCurrentPositionIdRegister[arm.AxisX.Id - 1] = positionId;
                register.MotionControlCurrentPositionIdRegister[arm.AxisY.Id - 1] = positionId;
            }

            byteConverter.ToInt32(d6000, 640 * 2, 4 * 4, _intArray, 0); // D6640 - D6647 (4個荷重元)

            ushort[] analogInputs = register.AnalogInputRegisters;

            for (i = 0; i < 4; ++i)
            {
                analogInputs[i] = unchecked((ushort)_intArray[i]);
            }

            ushort d6120 = byteConverter.ToUInt16(d6000, 120 * 2);

            bool reboot = !BitOperation.GetBit(d6120, 7);

            if (_firstRefresh | reboot)
            {
                _firstRefresh = false;

                byteConverter.ToUInt16(d6000, 20 * 2, 12 * 2, register.CoilRegisters, 0); // D6020 - D6031 (實體Y)

                byteConverter.ToUInt16(d6000, 140 * 2, 18 * 2, _ushortArray, 0); // D6140 - D6157 (軸控IO寫)

                i = 0;

                foreach (var arm in ARMs)
                {
                    var ioMemory = arm.IOMemory;
                    ushort value = _ushortArray[i++];

                    ServoDriveCommand command = ServoDriveCommand.None;

                    if (BitOperation.GetBit(value, 0))
                        command |= ServoDriveCommand.JogForward;
                    if (BitOperation.GetBit(value, 1))
                        command |= ServoDriveCommand.JogReverse;
                    if (BitOperation.GetBit(value, 2))
                        command |= ServoDriveCommand.Go;
                    if (BitOperation.GetBit(value, 3))
                        command |= ServoDriveCommand.AxisToStop;
                    if (BitOperation.GetBit(value, 4))
                        command |= ServoDriveCommand.ServoOff;
                    if (BitOperation.GetBit(value, 5))
                        command |= ServoDriveCommand.ReturnToHomePosition;
                    if (BitOperation.GetBit(value, 6))
                        command |= ServoDriveCommand.Reset;

                    ioMemory.Command = command;
                }

                if (reboot)
                    OnEquipmentReboot();
            }

            _ioRegisterSeqLock.WriteSeqLock();
            register.CopyTo(_cacheIORegister);
            _ioRegisterSeqLock.WriteSeqUnlock();
            return true;
        }

        protected override async Task<bool> UpdateAsync()
        {
            byte[] d6000 = D6000.Buffer;

            IByteConverter byteConverter = _melsec.ByteConverter;

            var register = IORegister;

            byteConverter.GetBytes(register.CoilRegisters, d6000, 20 * 2); // D6020 - D6031 (實體Y)

            int i = 0, j = 0;

            foreach (var arm in ARMs)
            {
                var ioMemory = arm.IOMemory;

                ushort value = 0;

                ServoDriveCommand command = ioMemory.Command;

                if ((command & ServoDriveCommand.JogForward) == ServoDriveCommand.JogForward)
                    value = unchecked((ushort)BitOperation.SetBit(value, 0, true));
                if ((command & ServoDriveCommand.JogReverse) == ServoDriveCommand.JogReverse)
                    value = unchecked((ushort)BitOperation.SetBit(value, 1, true));
                if ((command & ServoDriveCommand.Go) == ServoDriveCommand.Go)
                    value = unchecked((ushort)BitOperation.SetBit(value, 2, true));
                if ((command & ServoDriveCommand.AxisToStop) == ServoDriveCommand.AxisToStop)
                    value = unchecked((ushort)BitOperation.SetBit(value, 3, true));
                if ((command & ServoDriveCommand.ServoOff) == ServoDriveCommand.ServoOff)
                    value = unchecked((ushort)BitOperation.SetBit(value, 4, true));
                if ((command & ServoDriveCommand.ReturnToHomePosition) == ServoDriveCommand.ReturnToHomePosition)
                    value = unchecked((ushort)BitOperation.SetBit(value, 5, true));
                if ((command & ServoDriveCommand.Reset) == ServoDriveCommand.Reset)
                    value = unchecked((ushort)BitOperation.SetBit(value, 6, true));

                _ushortArray[i++] = value;

                _intArray[j++] = ioMemory.PositioningValue;
                _intArray[j++] = ioMemory.PositioningSpeed;
                _intArray[j++] = ioMemory.AccelerationTime;
                _intArray[j++] = ioMemory.SpeedLimit;
                _intArray[j++] = ioMemory.JogSpeed;
                j += 5;
            }

            int armCount = ARMs.Length;

            byteConverter.GetBytes(_ushortArray, 0, armCount, d6000, 140 * 2);   // D6140 - D6157 (軸控IO寫)
            byteConverter.GetBytes(_intArray, 0, armCount * 10, d6000, 200 * 2); // D6200 - D6559 (軸控DWORD寫)

            OperationResult result = await _melsec.WriteAsync("D6000", D6000).ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                if (!_plcDisconnected)
                {
                    _plcDisconnected = true;
                    Log.w(LogKeyword, $"{_melsec.Type.Name} Write: {result}");
                }
                return false;
            }

            _plcDisconnected = false;
            return true;
        }

        protected override async Task<OperationResult<bool>> CheckIfEquipmentHasRebootAsync()
        {
            OperationResult<ushort> result = await _melsec.ReadUInt16Async("D6120").ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                Log.i(LogKeyword, $"{_melsec.Type.Name} Read: {result}");
                return OperationResultFactory.CreateFailedResult<bool>(result);
            }

            return OperationResultFactory.CreateSuccessfulResult(!BitOperation.GetBit(result.Value, 7));
        }

        protected override void OnBootCompleted()
        {
            base.OnBootCompleted();

            byte[] d6000 = D6000.Buffer;

            IByteConverter byteConverter = _melsec.ByteConverter;

            ushort d6120 = byteConverter.ToUInt16(d6000, 120 * 2);

            d6120 = unchecked((ushort)BitOperation.SetBit(d6120, 7, true));

            byteConverter.GetBytes(d6120, d6000, 120 * 2);
        }

        public void ReadIORegister(IORegister register)
        {
            uint seq;

            do
            {
                seq = _ioRegisterSeqLock.ReadSeqBegin();

                _cacheIORegister.CopyTo(register);

            } while (_ioRegisterSeqLock.ReadSeqRetry(seq));
        }
    }
}
