﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller
{
    public partial class AutomatedDispensingController
    {
        protected override bool CanBeAcceptedToUnlockSafetyDoorDuringAutoRun()
        {
            return false;
        }
    }
}
