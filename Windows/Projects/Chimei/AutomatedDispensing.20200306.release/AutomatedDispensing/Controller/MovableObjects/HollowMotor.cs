﻿using Automation.Core.Controller;
using Automation.Core.Registers;
using Automation.Helper;
using Automation.Objects.Movable;
using Automation.Objects.Movable.StepperMotors;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomatedDispensing.Controller.MovableObjects
{
    class HollowMotor : IOStepperMotor
    {
        public HollowMotor() : base("中空馬達", new BitCell(6016, 0xE), new BitCell(6016, 0xD), new BitCell(6031, 0)) { }

        protected override short CheckIfScriptIsRunning(SupervisoryController controller, out bool isJog)
        {
            IORegister register = controller.IORegister;

            isJog = false;

            if (register.GetCoil(new BitCell(6030, 0xD)))
            {
                isJog = true;
                return 1;
            }

            if (register.GetCoil(new BitCell(6030, 0xE)))
            {
                isJog = true;
                return 2;
            }

            if (register.GetCoil(new BitCell(6030, 0xF)))
                return 3;

            return -1;
        }

        protected override short GetScriptId(string name)
        {
            if (name == "Jog快速")
                return 1;

            if (name == "Jog慢速")
                return 2;

            if (name == "定位")
                return 3;

            return -1;
        }

        protected override bool Run(SupervisoryController controller, short id)
        {
            IORegister register = controller.IORegister;

            bool isJog = false;

            switch (id)
            {
                case 1:
                    register.SetCoil(new BitCell(6030, 0xD), true);
                    isJog = true;
                    break;
                case 2:
                    register.SetCoil(new BitCell(6030, 0xE), true);
                    isJog = true;
                    break;
                case 3:
                    register.SetCoil(new BitCell(6030, 0xF), true);
                    break;
                default:
                    throw new NotImplementedException();
            }

            return isJog;
        }

        protected override void Stop(SupervisoryController controller, short id)
        {
            IORegister register = controller.IORegister;

            register.SetCoil(new BitCell(6030, 0xD), false);
            register.SetCoil(new BitCell(6030, 0xE), false);
            register.SetCoil(new BitCell(6030, 0xF), false);
        }
    }
}
