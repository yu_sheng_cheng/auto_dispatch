﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Widget;
using Widget.WPF.Keypads;

namespace AutomatedDispensing.View.Dialogs
{
    /// <summary>
    /// PointDialog.xaml 的互動邏輯
    /// </summary>
    public partial class PointDialog : MetroWindow
    {
        public string X
        {
            get { return XTextBox.Text; }
            set { XTextBox.Text = value; }
        }

        public string Y
        {
            get { return YTextBox.Text; }
            set { YTextBox.Text = value; }
        }

        public PointDialog()
        {
            InitializeComponent();
        }

        private void MetroWindow_SourceInitialized(object sender, EventArgs e)
        {
            HwndSource source = HwndSource.FromHwnd(new WindowInteropHelper(this).Handle);
            source.AddHook(new HwndSourceHook(WndProc));
        }

        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case Win32.WM_SYSCOMMAND:
                    switch (wParam.ToInt32() & 0xFFF0)
                    {
                        case Win32.SC_MOVE:
                        case Win32.SC_RESTORE:
                            handled = true;
                            break;
                    }
                    break;
            }
            return IntPtr.Zero;
        }

        private void TextBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            TextBox textbox = sender as TextBox;

            NumericKeypad keypad = new NumericKeypad();
            keypad.Owner = this;
            if (keypad.ShowDialog() == true)
            {
                string result = keypad.Result;
                int value;

                if (int.TryParse(result, out value))
                    textbox.Text = result;
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
