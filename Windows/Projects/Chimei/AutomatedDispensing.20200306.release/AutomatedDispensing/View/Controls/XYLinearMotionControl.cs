﻿using AutomatedDispensing.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Widget.WPF.Keypads;

namespace AutomatedDispensing.View.Controls
{
    class XYLinearMotionControl : Control
    {
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            var textBox = GetTemplateChild("PART_AxisXJogSpeedTextBox") as TextBox;
            if (textBox != null)
                textBox.PreviewMouseDown += NumericTextBox_PreviewMouseDown;

            var button = GetTemplateChild("PART_AxisXJogReverseButton") as Button;
            if (button != null)
            {
                button.PreviewMouseDown += AxisXJogReverseButton_PreviewMouseDown;
                button.PreviewMouseUp += AxisXJogButton_PreviewMouseUp;
            }

            button = GetTemplateChild("PART_AxisXJogForwardButton") as Button;
            if (button != null)
            {
                button.PreviewMouseDown += AxisXJogForwardButton_PreviewMouseDown;
                button.PreviewMouseUp += AxisXJogButton_PreviewMouseUp;
            }

            textBox = GetTemplateChild("PART_AxisYJogSpeedTextBox") as TextBox;
            if (textBox != null)
                textBox.PreviewMouseDown += NumericTextBox_PreviewMouseDown;

            button = GetTemplateChild("PART_AxisYJogReverseButton") as Button;
            if (button != null)
            {
                button.PreviewMouseDown += AxisYJogReverseButton_PreviewMouseDown;
                button.PreviewMouseUp += AxisYJogButton_PreviewMouseUp;
            }

            button = GetTemplateChild("PART_AxisYJogForwardButton") as Button;
            if (button != null)
            {
                button.PreviewMouseDown += AxisYJogForwardButton_PreviewMouseDown;
                button.PreviewMouseUp += AxisYJogButton_PreviewMouseUp;
            }
        }

        private void AxisXJogReverseButton_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            XYLinearMotionViewModel vm = DataContext as XYLinearMotionViewModel;

            vm.StartAxisXJogReverseCommand.Execute(null);
        }

        private void AxisXJogForwardButton_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            XYLinearMotionViewModel vm = DataContext as XYLinearMotionViewModel;

            vm.StartAxisXJogForwardCommand.Execute(null);
        }

        private void AxisXJogButton_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            XYLinearMotionViewModel vm = DataContext as XYLinearMotionViewModel;

            vm.StopAxisXJogCommand.Execute(null);
        }

        private void AxisYJogReverseButton_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            XYLinearMotionViewModel vm = DataContext as XYLinearMotionViewModel;

            vm.StartAxisYJogReverseCommand.Execute(null);
        }

        private void AxisYJogForwardButton_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            XYLinearMotionViewModel vm = DataContext as XYLinearMotionViewModel;

            vm.StartAxisYJogForwardCommand.Execute(null);
        }

        private void AxisYJogButton_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            XYLinearMotionViewModel vm = DataContext as XYLinearMotionViewModel;

            vm.StopAxisYJogCommand.Execute(null);
        }

        private void NumericTextBox_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TextBox textbox = sender as TextBox;

            NumericKeypad keypad = new NumericKeypad();
            keypad.Owner = Window.GetWindow(textbox);
            if (keypad.ShowDialog() == true)
            {
                string result = keypad.Result;
                int value;

                if (int.TryParse(result, out value))
                    textbox.Text = result;
            }
        }
    }
}
