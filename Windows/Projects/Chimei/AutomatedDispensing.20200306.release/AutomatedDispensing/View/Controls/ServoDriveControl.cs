﻿using AutomatedDispensing.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Widget.WPF.Keypads;

namespace AutomatedDispensing.View.Controls
{
    class ServoDriveControl : Control
    {
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            var textBox = GetTemplateChild("PART_JogSpeedTextBox") as TextBox;
            if (textBox != null)
                textBox.PreviewMouseDown += NumericTextBox_PreviewMouseDown;

            var button = GetTemplateChild("PART_JogReverseButton") as Button;
            if (button != null)
            {
                button.PreviewMouseDown += JogReverseButton_PreviewMouseDown;
                button.PreviewMouseUp += JogButton_PreviewMouseUp;
            }

            button = GetTemplateChild("PART_JogForwardButton") as Button;
            if (button != null)
            {
                button.PreviewMouseDown += JogForwardButton_PreviewMouseDown;
                button.PreviewMouseUp += JogButton_PreviewMouseUp;
            }
        }

        private void JogReverseButton_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ServoDriveViewModel vm = DataContext as ServoDriveViewModel;

            vm.StartJogReverseCommand.Execute(null);
        }

        private void JogForwardButton_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ServoDriveViewModel vm = DataContext as ServoDriveViewModel;

            vm.StartJogForwardCommand.Execute(null);
        }

        private void JogButton_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ServoDriveViewModel vm = DataContext as ServoDriveViewModel;

            vm.StopJogCommand.Execute(null);
        }

        private void NumericTextBox_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            TextBox textbox = sender as TextBox;

            NumericKeypad keypad = new NumericKeypad();
            keypad.Owner = Window.GetWindow(textbox);
            if (keypad.ShowDialog() == true)
            {
                string result = keypad.Result;
                int value;

                if (int.TryParse(result, out value))
                    textbox.Text = result;
            }
        }
    }
}
