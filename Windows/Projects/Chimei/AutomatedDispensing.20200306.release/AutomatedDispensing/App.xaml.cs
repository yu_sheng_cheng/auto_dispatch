﻿using AutomatedDispensing.Controller;
using AutomatedDispensing.Model;
using AutomatedDispensing.ViewModel;
using Automation;
using Automation.Core.Registers;
using Industry4._0;
using SwissKnife;
using SwissKnife.Log;
using SwissKnife.Log.AppLogger;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace AutomatedDispensing
{
    /// <summary>
    /// App.xaml 的互動邏輯
    /// </summary>
    public partial class App : Application
    {
        private Mutex _mutex;

        private void App_Startup(object sender, StartupEventArgs e)
        {
            bool createdNew;

            _mutex = new Mutex(true, "DEB6FED8-F1DD-4E8D-B946-0A68EFF80E09", out createdNew);

            if (!createdNew)
            {
                Current.Shutdown();
                return;
            }

            IUnityContainer container = new UnityContainer();
            container.RegisterType<ILog, Logger>(new ContainerControlledLifetimeManager(), new InjectionConstructor(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Applogger.xml")));
            container.RegisterType<AutomatedDispensingController>(new ContainerControlledLifetimeManager());

            ILog log = container.Resolve<ILog>();
            log.i("AutomatedDispensing starting ...");

            Industry4Dot0Environment.Log = log;
            AutomationEnvironment.Log = log;

            Environment.UnityContainer = container;
            Environment.EquipmentProfileRootFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Profiles");
            Environment.StringResourceRootFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "StringResources");

            Exit += App_Exit;

            DispatcherUnhandledException += (o, ea) => Backtrace("Dispatcher", ea.Exception);
            AppDomain.CurrentDomain.UnhandledException += (o, ea) =>
            {
                Exception exception = ea.ExceptionObject as Exception;
                if (exception == null)
                    Backtrace("AppDomain", new Exception("Unknown error. Exception object is null"));
                else
                    Backtrace("AppDomain", exception);
            };
            TaskScheduler.UnobservedTaskException += (o, ea) =>
            {
                ea.SetObserved();
                Environment.UnityContainer.Resolve<ILog>().w("TaskScheduler", $"UnobservedTaskException occurs", ea.Exception);
            };

            var controller = container.Resolve<AutomatedDispensingController>();
            controller.IsTuning = true;
            controller.AlarmMessageIni = new IniParser(Path.Combine(Environment.StringResourceRootFolder, "AlarmMessage.ini"));

            Environment.PositionIni = new IniParser(Path.Combine(Environment.StringResourceRootFolder, "Position.ini"));
            Environment.IOWatcher = new IOWatcher();

            Build();

            controller.Start();

            new Tuning.View.MainWindow().Show();
        }

        private void App_Exit(object sender, ExitEventArgs e)
        {
            IUnityContainer container = Environment.UnityContainer;

            container.Resolve<ILog>().i("AutomatedDispensing shutting down");
            container.Dispose();
        }

        private void Backtrace(string source, Exception ex)
        {
            string directory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ErrorReport");

            Directory.CreateDirectory(directory);

            string file = Path.Combine(directory, string.Format("{0}-{1:yyyy-MM-dd_HH-mm-ss}.txt", source, DateTime.Now));

            using (StreamWriter sw = new StreamWriter(file, false, Encoding.UTF8))
            {
                sw.Write(ex.ToString());
            }
        }

        private void Build()
        {
            Environment.AddServoDriveViewModel(new ServoDriveViewModel("ARM3 X 送料手臂", 3));
            Environment.AddServoDriveViewModel(new ServoDriveViewModel("ARM4 Z 1.5ml夾取手臂", 4));
            Environment.AddServoDriveViewModel(new ServoDriveViewModel("ARM7 X 藥劑瓶移載", 7));
            Environment.AddServoDriveViewModel(new ServoDriveViewModel("ARM8 Θ 藥劑瓶開蓋手臂", 8));
            Environment.AddServoDriveViewModel(new ServoDriveViewModel("ARM9 Θ 樣品瓶開蓋手臂", 9));
            Environment.AddServoDriveViewModel(new ServoDriveViewModel("ARM10 Θ 60ml開蓋手臂", 10));
            Environment.AddServoDriveViewModel(new ServoDriveViewModel("ARM11 Z1 1ml取TIP手臂", 11));
            Environment.AddServoDriveViewModel(new ServoDriveViewModel("ARM12 Z2 10ml取TIP手臂", 12));
            Environment.AddServoDriveViewModel(new ServoDriveViewModel("ARM13 Y 藥劑瓶夾取手臂", 13));
            Environment.AddServoDriveViewModel(new ServoDriveViewModel("ARM14 Z1 1ml移液手臂", 14));
            Environment.AddServoDriveViewModel(new ServoDriveViewModel("ARM15 Z2 10ml移液手臂", 15));
            Environment.AddServoDriveViewModel(new ServoDriveViewModel("ARM18 X 送料手臂", 18));

            Environment.EmptyBottleTransferViewModel = new XYLinearMotionViewModel("空瓶移載手臂", 1, 2);
            Environment.DispensingTransferViewModel = new XYLinearMotionViewModel("配藥移載手臂", 5, 6);
            Environment.BottlePickupTransferViewModel = new XYLinearMotionViewModel("取瓶移載手臂", 16, 17);

            Environment.AddCylinderViewModel(new CylinderViewModel("60ml倒液汽缸", new BitCell(6003, 4), new BitCell(6023, 4), new BitCell(6003, 5), new BitCell(6023, 5),
                "X134", "Y134", "正面", "X135", "Y135", "反面"));
            Environment.AddCylinderViewModel(new CylinderViewModel("60ml倒液夾瓶汽缸", new BitCell(6003, 7), new BitCell(6023, 7), new BitCell(6003, 6), new BitCell(6023, 6),
                "X133", "Y133", "打開", "X132", "Y132", "夾持"));
            Environment.AddCylinderViewModel(new CylinderViewModel("60ml吹氣上下汽缸", new BitCell(6007, 6), new BitCell(6021, 0xA), new BitCell(6007, 7), new BitCell(6021, 0xB),
                "X176", "Y11A", "上升", "X177", "Y11B", "下降"));
            Environment.AddCylinderViewModel(new CylinderViewModel("60ml吹氣汽缸", new BitCell(6003, 0xF), new BitCell(6023, 0xF), new BitCell(6003, 0xE), new BitCell(6023, 0xE),
                "X13F", "Y13F", "後退", "X13E", "Y13E", "前進"));
            Environment.AddCylinderViewModel(new CylinderViewModel("1.5ml倒液汽缸", new BitCell(6003, 8), new BitCell(6023, 8), new BitCell(6003, 9), new BitCell(6023, 9),
                "X138", "Y138", "正面", "X139", "Y139", "反面"));
            Environment.AddCylinderViewModel(new CylinderViewModel("1.5ml倒液夾瓶汽缸", new BitCell(6003, 0xB), new BitCell(6023, 0xB), new BitCell(6003, 0xA), new BitCell(6023, 0xA),
                "X13B", "Y13B", "打開", "X13A", "Y13A", "夾持"));
            Environment.AddCylinderViewModel(new CylinderViewModel("1.5ml吹氣上下汽缸", new BitCell(6007, 4), new BitCell(6021, 8), new BitCell(6007, 5), new BitCell(6021, 9),
                "X174", "Y118", "上升", "X175", "Y119", "下降"));
            Environment.AddCylinderViewModel(new CylinderViewModel("1.5ml吹氣汽缸", new BitCell(6003, 0xD), new BitCell(6023, 0xD), new BitCell(6003, 0xC), new BitCell(6023, 0xC),
                "X13D", "Y13D", "後退", "X13C", "Y13C", "前進"));
            Environment.AddCylinderViewModel(new CylinderViewModel("噴碼前後移載汽缸", new BitCell(6002, 3), new BitCell(6022, 3), new BitCell(6002, 2), new BitCell(6022, 2),
                "X123", "Y123", "後退", "X122", "Y122", "前進"));
            Environment.AddCylinderViewModel(new CylinderViewModel("噴碼左右移載汽缸", new BitCell(6002, 4), new BitCell(6022, 4), new BitCell(6002, 5), new BitCell(6022, 5),
                "X124", "Y124", "往左", "X125", "Y125", "往右"));
            Environment.AddCylinderViewModel(new CylinderViewModel("噴碼上下移載汽缸", new BitCell(6002, 7), new BitCell(6022, 7), new BitCell(6002, 6), new BitCell(6022, 6),
                "X127", "Y127", "下降", "X126", "Y126", "上升"));
            Environment.AddCylinderViewModel(new CylinderViewModel("噴碼翻轉汽缸", new BitCell(6002, 9), new BitCell(6022, 9), new BitCell(6002, 8), new BitCell(6022, 8),
                "X129", "Y129", "垂直", "X128", "Y128", "水平"));
            Environment.AddCylinderViewModel(new CylinderViewModel("噴碼夾爪汽缸", new BitCell(6002, 0xB), new BitCell(6022, 0xB), new BitCell(6002, 0xA), new BitCell(6022, 0xA),
                "X12B", "Y12B", "打開", "X12A", "Y12A", "夾持"));
            Environment.AddCylinderViewModel(new CylinderViewModel("空噴汽缸", new BitCell(6002, 0xD), new BitCell(6022, 0xD), new BitCell(6002, 0xC), new BitCell(6022, 0xC),
                "X12D", "Y12D", "退回", "X12C", "Y12C", "伸出"));
            Environment.AddCylinderViewModel(new CylinderViewModel("1.5ml抱瓶汽缸", new BitCell(6002, 0xF), new BitCell(6022, 0xF), new BitCell(6002, 0xE), new BitCell(6022, 0xE),
                "X12F", "Y12F", "退回", "X12E", "Y12E", "伸出"));
            Environment.AddCylinderViewModel(new CylinderViewModel("1.5ml夾瓶汽缸", new BitCell(6002, 1), new BitCell(6022, 1), new BitCell(6002, 0), new BitCell(6022, 0),
                "X121", "Y121", "打開", "X120", "Y120", "夾持"));
            Environment.AddCylinderViewModel(new CylinderViewModel("60ml移載夾爪上下汽缸", new BitCell(6003, 0), new BitCell(6023, 0), new BitCell(6003, 1), new BitCell(6023, 1),
                "X130", "Y130", "下降", "X131", "Y131", "上升"));
            Environment.AddCylinderViewModel(new CylinderViewModel("60ml移載夾爪汽缸", new BitCell(6003, 3), new BitCell(6023, 3), new BitCell(6003, 2), new BitCell(6023, 2),
                "X133", "Y133", "打開", "X132", "Y132", "夾持"));

            Environment.AddCylinderViewModel(new CylinderViewModel("藥劑瓶開蓋上下汽缸", new BitCell(6009, 0xF), new BitCell(6025, 0xF), new BitCell(6009, 0xE), new BitCell(6025, 0xE),
                "X19F", "Y15F", "上升", "X19E", "Y15E", "下降"));
            Environment.AddCylinderViewModel(new CylinderViewModel("藥劑瓶開蓋身體夾爪汽缸", new BitCell(6010, 3), new BitCell(6026, 3), new BitCell(6010, 2), new BitCell(6026, 2),
                "X1A3", "Y163", "打開", "X1A2", "Y162", "夾持"));
            Environment.AddCylinderViewModel(new CylinderViewModel("樣品瓶開蓋上下汽缸", new BitCell(6010, 4), new BitCell(6026, 4), new BitCell(6010, 5), new BitCell(6026, 5),
                "X1A4", "Y164", "上升", "X1A5", "Y165", "下降"));
            Environment.AddCylinderViewModel(new CylinderViewModel("樣品瓶身體夾爪汽缸", new BitCell(6010, 9), new BitCell(6026, 9), new BitCell(6010, 8), new BitCell(6026, 8),
                "X1A9", "Y169", "打開", "X1A8", "Y168", "夾持"));
            Environment.AddCylinderViewModel(new CylinderViewModel("60ml中繼站汽缸", new BitCell(6008, 0), new BitCell(6024, 0), new BitCell(6008, 1), new BitCell(6024, 1),
                "X180", "Y140", "前進", "X181", "Y141", "後退"));
            Environment.AddCylinderViewModel(new CylinderViewModel("藥劑瓶上下汽缸", new BitCell(6010, 0xA), new BitCell(6027, 0), new BitCell(6010, 0xB), new BitCell(6027, 1),
                "X1AA", "Y170", "上升", "X1AB", "Y171", "下降"));
            Environment.AddCylinderViewModel(new CylinderViewModel("藥劑瓶夾爪汽缸", new BitCell(6010, 0xD), new BitCell(6027, 3), new BitCell(6010, 0xC), new BitCell(6027, 2),
                "X1AD", "Y173", "打開", "X1AC", "Y172", "夾持"));
            Environment.AddCylinderViewModel(new CylinderViewModel("微量天秤遮風罩汽缸", new BitCell(6008, 0xE), new BitCell(6024, 0xE), new BitCell(6008, 0xF), new BitCell(6024, 0xF),
                "X18E", "Y14E", "關閉", "X18F", "Y14F", "打開"));
            Environment.AddCylinderViewModel(new CylinderViewModel("混合液汽缸", new BitCell(6008, 4), new BitCell(6024, 4), new BitCell(6008, 5), new BitCell(6024, 5),
                "X184", "Y144", "正面", "X185", "Y145", "反面"));
            Environment.AddCylinderViewModel(new CylinderViewModel("混合液夾爪汽缸", new BitCell(6008, 3), new BitCell(6024, 3), new BitCell(6008, 2), new BitCell(6024, 2),
                "X183", "Y143", "打開", "X184", "Y144", "夾持"));
            Environment.AddCylinderViewModel(new CylinderViewModel("60ml開蓋左右移載汽缸", new BitCell(6008, 0xC), new BitCell(6024, 0xC), new BitCell(6008, 0xD), new BitCell(6024, 0xD),
                "X18C", "Y14C", "往左", "X18D", "Y14D", "往右"));
            Environment.AddCylinderViewModel(new CylinderViewModel("60ml開蓋上下移載汽缸", new BitCell(6008, 0xA), new BitCell(6024, 0xA), new BitCell(6008, 0xB), new BitCell(6024, 0xB),
                "X18A", "Y14A", "上升", "X18B", "Y14B", "下降"));
            Environment.AddCylinderViewModel(new CylinderViewModel("60ml開蓋上夾爪汽缸", new BitCell(6008, 7), new BitCell(6024, 7), new BitCell(6008, 6), new BitCell(6024, 6),
                "X187", "Y147", "打開", "X186", "Y146", "夾持"));
            Environment.AddCylinderViewModel(new CylinderViewModel("吸蓋上下汽缸", new BitCell(6009, 0xC), new BitCell(6025, 0xC), new BitCell(6009, 0xD), new BitCell(6025, 0xD),
                "X19C", "Y15C", "上升", "X19D", "Y15D", "下降"));
            Environment.AddCylinderViewModel(new CylinderViewModel("10ml夾爪本體汽缸", new BitCell(6009, 9), new BitCell(6025, 9), new BitCell(6009, 8), new BitCell(6025, 8),
                "X199", "Y159", "打開", "X198", "Y158", "夾持"));
            Environment.AddCylinderViewModel(new CylinderViewModel("10ml夾軸芯汽缸", new BitCell(6009, 0xB), new BitCell(6025, 0xB), new BitCell(6009, 0xA), new BitCell(6025, 0xA),
                "X19B", "Y15B", "打開", "X19A", "Y15A", "夾持"));
            Environment.AddCylinderViewModel(new CylinderViewModel("10ml接漏液汽缸", new BitCell(6009, 7), new BitCell(6025, 7), new BitCell(6009, 6), new BitCell(6025, 6),
                "X197", "Y157", "縮回", "X196", "Y156", "伸出"));
            Environment.AddCylinderViewModel(new CylinderViewModel("1ml接漏液汽缸", new BitCell(6009, 5), new BitCell(6025, 5), new BitCell(6009, 4), new BitCell(6025, 4),
                "X195", "Y155", "縮回", "X194", "Y154", "伸出"));
            Environment.AddCylinderViewModel(new CylinderViewModel("60ml上下移載汽缸", new BitCell(6009, 0), new BitCell(6025, 0), new BitCell(6009, 1), new BitCell(6025, 1),
                "X190", "Y150", "上升", "X191", "Y151", "下降"));
            Environment.AddCylinderViewModel(new CylinderViewModel("60ml配藥移載夾爪汽缸", new BitCell(6009, 3), new BitCell(6025, 3), new BitCell(6009, 2), new BitCell(6025, 2),
                "X193", "Y153", "打開", "X192", "Y152", "夾持"));

            Environment.AddCylinderViewModel(new CylinderViewModel("限位推瓶汽缸", new BitCell(6014, 0xC), new BitCell(6014, 0xD), new BitCell(6029, 0xE),
                "X1EC", "Y19E", "限位", "X1ED", "Y19E", "推瓶"));
            Environment.AddCylinderViewModel(new CylinderViewModel("擋瓶汽缸2", new BitCell(6014, 0xB), new BitCell(6029, 9), new BitCell(6014, 0xA), new BitCell(6029, 8),
            "X1EB", "Y199", "退回", "X1EA", "Y198", "伸出"));
            Environment.AddCylinderViewModel(new CylinderViewModel("擋瓶汽缸1", new BitCell(6014, 9), new BitCell(6029, 7), new BitCell(6014, 8), new BitCell(6029, 6),
            "X1E9", "Y197", "退回", "X1E8", "Y196", "伸出"));
            Environment.AddCylinderViewModel(new CylinderViewModel("推樣品瓶汽缸", new BitCell(6014, 7), new BitCell(6029, 5), new BitCell(6014, 6), new BitCell(6029, 4),
            "X1E7", "Y195", "縮回", "X1E6", "Y194", "推瓶"));
            Environment.AddCylinderViewModel(new CylinderViewModel("進料旋轉汽缸", new BitCell(6014, 0), new BitCell(6028, 0xE), new BitCell(6014, 1), new BitCell(6028, 0xF),
            "X1E0", "Y18E", "秤重", "X1E1", "Y18F", "掃碼"));
            Environment.AddCylinderViewModel(new CylinderViewModel("進料上下汽缸", new BitCell(6014, 2), new BitCell(6029, 0), new BitCell(6014, 3), new BitCell(6029, 1),
            "X1E2", "Y190", "上升", "X1E3", "Y191", "下降"));
            Environment.AddCylinderViewModel(new CylinderViewModel("進料夾爪汽缸", new BitCell(6014, 5), new BitCell(6029, 3), new BitCell(6014, 4), new BitCell(6029, 2),
            "X1E5", "Y193", "打開", "X1E4", "Y192", "夾持"));
            Environment.AddCylinderViewModel(new CylinderViewModel("倒廢液汽缸", new BitCell(6013, 0xE), new BitCell(6028, 0xC), new BitCell(6013, 0xF), new BitCell(6028, 0xD),
            "X1DE", "Y18C", "正面", "X1DF", "Y18D", "反面"));
            Environment.AddCylinderViewModel(new CylinderViewModel("倒廢液夾爪汽缸", new BitCell(6013, 0xD), new BitCell(6028, 0xB), new BitCell(6013, 0xC), new BitCell(6028, 0xA),
            "X1DD", "Y18B", "打開", "X1DC", "Y18A", "夾持"));
            Environment.AddCylinderViewModel(new CylinderViewModel("推瓶回收區汽缸", new BitCell(6013, 7), new BitCell(6028, 5), new BitCell(6013, 6), new BitCell(6028, 4),
            "X1D7", "Y185", "縮回", "X1D6", "Y184", "推瓶"));
            Environment.AddCylinderViewModel(new CylinderViewModel("推瓶待處理區汽缸", new BitCell(6013, 3), new BitCell(6028, 1), new BitCell(6013, 2), new BitCell(6028, 0),
            "X1D3", "Y181", "縮回", "X1D2", "Y180", "推瓶"));
            Environment.AddCylinderViewModel(new CylinderViewModel("推瓶NG區汽缸", new BitCell(6013, 5), new BitCell(6028, 3), new BitCell(6013, 4), new BitCell(6028, 2),
            "X1D5", "Y183", "縮回", "X1D4", "Y182", "推瓶"));
            Environment.AddCylinderViewModel(new CylinderViewModel("上下移載汽缸", new BitCell(6013, 8), new BitCell(6028, 6), new BitCell(6013, 9), new BitCell(6028, 7),
            "X1D8", "Y186", "上升", "X1D9", "Y187", "下降"));
            Environment.AddCylinderViewModel(new CylinderViewModel("移載夾爪汽缸", new BitCell(6013, 0xB), new BitCell(6028, 9), new BitCell(6013, 0xA), new BitCell(6028, 8),
            "X1DB", "Y189", "打開", "X1DA", "Y188", "夾持"));
        }
    }
}
