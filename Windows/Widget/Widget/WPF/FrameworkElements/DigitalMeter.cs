﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Widget.WPF.FrameworkElements
{
    public class DigitalMeterValueChangedEventArgs : RoutedEventArgs
    {
        public decimal OldValue { get; }

        public decimal NewValue { get; }

        public DigitalMeterValueChangedEventArgs(RoutedEvent routedEvent, decimal oldValue, decimal newValue) : base(routedEvent)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }
    }

    public class DigitalMeter : Control
    {
        public DigitalMeter()
        {
            DefaultStyleKey = typeof(DigitalMeter);
        }

        public delegate void ValueChangedEventHandler(object sender, DigitalMeterValueChangedEventArgs e);

        public static readonly RoutedEvent ValueChangedEvent = EventManager.RegisterRoutedEvent("ValueChanged", RoutingStrategy.Bubble,
            typeof(ValueChangedEventHandler), typeof(DigitalMeter));

        public event ValueChangedEventHandler ValueChanged
        {
            add { AddHandler(ValueChangedEvent, value); }
            remove { RemoveHandler(ValueChangedEvent, value); }
        }

        public int Precision
        {
            get { return (int)GetValue(PrecisionProperty); }
            set { SetValue(PrecisionProperty, value); }
        }

        public static readonly DependencyProperty PrecisionProperty =
            DependencyProperty.Register("Precision", typeof(int), typeof(DigitalMeter), new PropertyMetadata(5, new PropertyChangedCallback(SetValueText)));

        public int ScalingFactor
        {
            get { return (int)GetValue(ScalingFactorProperty); }
            set { SetValue(ScalingFactorProperty, value); }
        }

        public static readonly DependencyProperty ScalingFactorProperty =
            DependencyProperty.Register("ScalingFactor", typeof(int), typeof(DigitalMeter), new PropertyMetadata(0, new PropertyChangedCallback(SetValueText)));

        public decimal Value
        {
            get { return (decimal)GetValue(ValueProperty); }
            set
            {
                decimal oldValue = Value;
                SetValue(ValueProperty, value);

                if (oldValue != value)
                    OnValueChanged(oldValue, value);
            }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(decimal), typeof(DigitalMeter), new PropertyMetadata(0M, new PropertyChangedCallback(SetValueText)));

        public string ValueText
        {
            get { return (string)GetValue(ValueTextProperty); }
            set { SetValue(ValueTextProperty, value); }
        }

        public static readonly DependencyProperty ValueTextProperty =
            DependencyProperty.Register("ValueText", typeof(string), typeof(DigitalMeter), new PropertyMetadata("00000"));

        public string MeasurementUnit
        {
            get { return (string)GetValue(MeasurementUnitProperty); }
            set { SetValue(MeasurementUnitProperty, value); }
        }

        public static readonly DependencyProperty MeasurementUnitProperty =
            DependencyProperty.Register("MeasurementUnit", typeof(string), typeof(DigitalMeter), new UIPropertyMetadata(""));

        private static void SetValueText(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            DigitalMeter dm = (DigitalMeter)d;
            dm.ValueText = FormatDecimalValue(dm.Value, dm.Precision, dm.ScalingFactor);
        }

        public static string FormatDecimalValue(decimal value, int precision, int scalingFactor)
        {
            string valueText = "";

            bool negative = value < 0;

            if (negative)
                value *= -1;

            if (scalingFactor == 0)
            {
                valueText = Math.Round(value, 0).ToString().PadLeft(precision, '0');
            }
            else
            {
                decimal integralValue = Decimal.Truncate(value);

                decimal fractionalValue = Math.Round(value - integralValue, scalingFactor);
                string fractionalValueText = fractionalValue.ToString();
                if (fractionalValueText.IndexOf('.') > 0)
                    fractionalValueText = fractionalValueText.Remove(0, 2);
                valueText = integralValue.ToString().PadLeft(precision - scalingFactor, '0');
                valueText = $"{valueText}.{fractionalValueText.PadRight(scalingFactor, '0')}";
            }

            if ((scalingFactor == 0 && valueText.Length > precision) || (scalingFactor > 0 && valueText.Length > precision + 1))
                valueText = string.Empty.PadLeft(precision - scalingFactor, '#') + "." + string.Empty.PadLeft(scalingFactor, '#');

            if (negative)
                valueText = $"-{valueText}";

            return valueText;
        }

        protected void OnValueChanged(decimal oldValue, decimal newValue)
        {
            DigitalMeterValueChangedEventArgs e = new DigitalMeterValueChangedEventArgs(DigitalMeter.ValueChangedEvent, oldValue, newValue);
            RaiseEvent(e);
        }
    }
}
