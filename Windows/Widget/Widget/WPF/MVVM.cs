﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Widget.WPF
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> _action;
        private readonly Func<T, bool> _canExecute;

        public event EventHandler CanExecuteChanged;

        public RelayCommand(Action<T> action) : this(action, null)
        {

        }

        public RelayCommand(Action<T> action, Func<T, bool> canExecute)
        {
            _action = action ?? throw new ArgumentNullException("action");
            _canExecute = canExecute ?? (anything => true);
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute((T)parameter);
        }

        public void Execute(object parameter)
        {
            _action((T)parameter);
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    public class RelayCommand : RelayCommand<object>
    {
        private static Action<object> SafeConvert(Action action)
        {
            if (action == null)
                throw new ArgumentNullException("action");
            return unused => action();
        }

        private static Func<object, bool> SafeConvert(Func<bool> canExecute)
        {
            if (canExecute == null)
                return null;
            return unused => canExecute();
        }

        public RelayCommand(Action action) : this(action, null)
        {

        }

        public RelayCommand(Action action, Func<bool> canExecute) : base(SafeConvert(action), SafeConvert(canExecute))
        {

        }
    }
}
