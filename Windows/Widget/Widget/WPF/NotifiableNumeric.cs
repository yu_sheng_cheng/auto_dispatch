﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Widget.WPF
{
    public class NotifiableUshort : ViewModelBase
    {
        private ushort _value;

        public ushort Value
        {
            get { return _value; }
            set
            {
                if (value != _value)
                {
                    _value = value;
                    RaisePropertyChanged(nameof(Value));
                }
            }
        }
    }
}
