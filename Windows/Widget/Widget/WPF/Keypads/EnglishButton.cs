﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Widget.WPF.Keypads
{
    public class EnglishButton : Button
    {
        public string UpperCase
        {
            get { return (string)GetValue(UpperCaseProperty); }
            set { SetValue(UpperCaseProperty, value); }
        }

        public static readonly DependencyProperty UpperCaseProperty = DependencyProperty.Register("UpperCase", typeof(string), typeof(EnglishButton), new PropertyMetadata(""));

        public string LowerCase
        {
            get { return (string)GetValue(LowerCaseProperty); }
            set { SetValue(LowerCaseProperty, value); }
        }

        public static readonly DependencyProperty LowerCaseProperty = DependencyProperty.Register("LowerCase", typeof(string), typeof(EnglishButton), new PropertyMetadata(""));
    }
}
