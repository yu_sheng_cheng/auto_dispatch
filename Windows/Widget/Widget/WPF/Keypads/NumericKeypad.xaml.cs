﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Widget.WPF.Keypads
{
    /// <summary>
    /// NumericKeypad.xaml 的互動邏輯
    /// </summary>
    public partial class NumericKeypad : Window, INotifyPropertyChanged
    {
        private bool _isFloatingPoint;
        private string _result = string.Empty;

        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsFloatingPoint
        {
            get { return _isFloatingPoint; }
            set
            {
                if (value != _isFloatingPoint)
                {
                    _isFloatingPoint = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(IsFloatingPoint)));
                }
            }
        }

        public string Result
        {
            get { return _result; }
            private set
            {
                _result = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Result)));
            }
        }

        public NumericKeypad()
        {
            InitializeComponent();

            DataContext = this;
        }

        public void StylusUpButton_Click(object sender, StylusEventArgs e)
        {
            e.Handled = true;

            Button button = sender as Button;

            Input(button);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;

            Input(button);
        }

        private void Input(Button button)
        {
            switch (button.CommandParameter.ToString())
            {
                case "ESC":
                    Close();
                    break;
                case "RETURN":
                    DialogResult = true;
                    break;
                case "BACK":
                    if (_result.Length > 0)
                        Result = _result.Remove(_result.Length - 1);
                    break;
                default:
                    Result = _result + button.Content.ToString();
                    break;
            }
        }
    }
}
