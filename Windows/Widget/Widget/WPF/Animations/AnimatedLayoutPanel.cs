﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Widget.WPF.Animations
{
    public abstract class AnimatedLayoutPanel : LayoutPanel, IArrangeAnimator
    {
        private DispatcherTimer _animationTimer;

        public static readonly DependencyProperty IsAnimatedProperty = DependencyProperty.Register("IsAnimated",
            typeof(bool), typeof(AnimatedLayoutPanel), new PropertyMetadata(true));

        public bool IsAnimated
        {
            get { return (bool)GetValue(IsAnimatedProperty); }
            set { SetValue(IsAnimatedProperty, value); }
        }

        protected virtual double ElapsedSeconds { get { return 0; } }

        public AnimatedLayoutPanel()
        {
            _animationTimer = AnimationBase.CreateAnimationTimer(this, TimeSpan.FromSeconds(0.05));
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            finalSize = base.ArrangeOverride(finalSize);

            if (IsAnimated)
                AnimationBase.Arrange(ElapsedSeconds, this, Children, this);

            return finalSize;
        }

        public abstract Rect Arrange(double elapsedTime, Point desiredPosition, Size desiredSize, Point currentPosition, Size currentSize);
    }
}
