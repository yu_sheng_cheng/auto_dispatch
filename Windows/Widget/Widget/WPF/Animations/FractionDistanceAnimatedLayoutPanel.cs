﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Widget.WPF.Animations
{
    public class FractionDistanceAnimatedLayoutPanel : AnimatedLayoutPanel
    {
        public static readonly DependencyProperty FractionProperty = DependencyProperty.Register("Fraction",
            typeof(double), typeof(FractionDistanceAnimatedLayoutPanel), new PropertyMetadata(0.25));

        public double Fraction
        {
            get { return (double)GetValue(FractionProperty); }
            set { SetValue(FractionProperty, value); }
        }

        public override Rect Arrange(double elapsedTime, Point desiredPosition, Size desiredSize, Point currentPosition, Size currentSize)
        {
            double deltaX = (desiredPosition.X - currentPosition.X) * Fraction;
            double deltaY = (desiredPosition.Y - currentPosition.Y) * Fraction;
            double deltaW = (desiredSize.Width - currentSize.Width) * Fraction;
            double deltaH = (desiredSize.Height - currentSize.Height) * Fraction;

            return new Rect(currentPosition.X + deltaX, currentPosition.Y + deltaY, currentSize.Width + deltaW, currentSize.Height + deltaH);
        }
    }
}
