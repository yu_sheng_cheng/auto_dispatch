﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Animation;
using Widget.WPF.FrameworkElements;

namespace Widget.WPF.Animations
{
    public class CounterAnimation : StringAnimationBase
    {
        public string From
        {
            get { return (string)GetValue(FromProperty); }
            set { SetValue(FromProperty, value); }
        }

        public static readonly DependencyProperty FromProperty =
            DependencyProperty.Register("From", typeof(string), typeof(CounterAnimation), new UIPropertyMetadata("0"));

        public string To
        {
            get { return (string)GetValue(ToProperty); }
            set { SetValue(ToProperty, value); }
        }

        public static readonly DependencyProperty ToProperty =
            DependencyProperty.Register("To", typeof(string), typeof(CounterAnimation), new UIPropertyMetadata("0"));

        protected override string GetCurrentValueCore(string defaultOriginValue, string defaultDestinationValue, AnimationClock animationClock)
        {
            if (To.Contains("#"))
                return To;

            TimeSpan? current = animationClock.CurrentTime;
            int precision = To.Length;
            int scalingFactor = 0;
            if (To.IndexOf('.') > 0)
            {
                precision--;
                scalingFactor = precision - To.IndexOf('.');
            }

            decimal from = 0;

            if (!string.IsNullOrEmpty(From))
            {
                if (!From.ToString().Contains("#"))
                    from = Convert.ToDecimal(From);
                else
                {
                    string max = "".PadLeft(precision, '9');
                    if (scalingFactor > 0)
                        max = max.Insert(precision - scalingFactor, ".");
                    from = Convert.ToDecimal(max);
                }
            }

            decimal to = Convert.ToDecimal(To);
            decimal increase = 0;
            if (Duration.HasTimeSpan && current.Value.Ticks > 0)
            {
                decimal factor = (decimal)current.Value.Ticks / (decimal)Duration.TimeSpan.Ticks;
                increase = (to - from) * factor;
            }

            from += increase;

            return DigitalMeter.FormatDecimalValue(from, precision, scalingFactor);
        }

        protected override Freezable CreateInstanceCore()
        {
            return new CounterAnimation();
        }
    }
}
