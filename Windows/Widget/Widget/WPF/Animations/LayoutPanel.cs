﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Widget.WPF.Animations
{
    public class LayoutPanel : Panel
    {
        private class ProxyUIElement : FrameworkElement
        {
            internal UIElement Element { get; }

            public ProxyUIElement(UIElement element)
            {
                Element = element;
            }

            protected override Size MeasureOverride(Size constraint)
            {
                Element.Measure(constraint);
                return Element.DesiredSize;
            }
        }

        private static IList<DependencyPropertyDescriptor> _attachedProperties;

        private Panel _panel;

        private static readonly DependencyPropertyKey ProxyUIElementPropertyKey =
            DependencyProperty.RegisterAttachedReadOnly("Visual", typeof(UIElement), typeof(LayoutPanel), new PropertyMetadata(null));

        private static readonly DependencyProperty ProxyUIElementProperty = ProxyUIElementPropertyKey.DependencyProperty;

        public static readonly DependencyProperty PanelProperty =
            DependencyProperty.Register("Panel", typeof(ItemsPanelTemplate), typeof(LayoutPanel),
            new FrameworkPropertyMetadata(new ItemsPanelTemplate(new FrameworkElementFactory(typeof(StackPanel))),
                FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.AffectsArrange, PanelChanged));

        public ItemsPanelTemplate Panel
        {
            get
            {
                return (ItemsPanelTemplate)GetValue(PanelProperty);
            }
            set
            {
                SetValue(PanelProperty, value);
            }
        }

        public LayoutPanel()
        {
            _panel = LoadPanelTemplate(Panel);
            InitializeChildren();
        }

        private static void PanelChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            LayoutPanel layoutPanel = sender as LayoutPanel;

            if (layoutPanel != null)
            {
                var newPanel = LoadPanelTemplate((ItemsPanelTemplate)args.NewValue);
                layoutPanel._panel = newPanel;
                layoutPanel.InitializeChildren();
            }
        }

        private static Panel LoadPanelTemplate(ItemsPanelTemplate panelTemplate)
        {
            var panel = (Panel)panelTemplate.LoadContent();
            panel.IsItemsHost = false; //set by ItemPanelTemplate, we need to reset it
            return panel;
        }

        private void InitializeChildren()
        {
            foreach (UIElement child in Children)
            {
                _panel.Children.Add(CreateProxyUIElement(child));
            }
        }

        private ProxyUIElement CreateProxyUIElement(UIElement child)
        {
            var proxyElement = new ProxyUIElement(child);

            SetProxyUIElement(child, proxyElement);

            var props = GetAllAttachedProperties();
            foreach (var d in props)
            {
                d.SetValue(proxyElement, d.GetValue(child));
            }
            return proxyElement;
        }

        private static UIElement GetProxyUIElement(DependencyObject obj)
        {
            return (UIElement)obj.GetValue(ProxyUIElementProperty);
        }

        private static void SetProxyUIElement(DependencyObject obj, UIElement element)
        {
            obj.SetValue(ProxyUIElementPropertyKey, element);
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            _panel.Measure(availableSize);
            return _panel.RenderSize;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            _panel.Arrange(new Rect(finalSize));
            foreach (ProxyUIElement child in _panel.Children)
            {
                Point point = child.TranslatePoint(new Point(), _panel);
                child.Element.Arrange(new Rect(point, child.RenderSize));
            }
            return _panel.RenderSize;
        }

        protected override void OnVisualChildrenChanged(DependencyObject visualAdded, DependencyObject visualRemoved)
        {
            base.OnVisualChildrenChanged(visualAdded, visualRemoved);

            if (visualAdded != null)
            {
                var props = GetAllAttachedProperties();
                foreach (var d in props)
                {
                    d.AddValueChanged(visualAdded, AttachedPropertyValueChanged);
                }

                var child = (UIElement)visualAdded;
                int index = Children.IndexOf(child);
                var proxyElement = CreateProxyUIElement(child);
                _panel.Children.Insert(index, proxyElement);
            }

            if (visualRemoved != null)
            {
                var props = GetAllAttachedProperties();
                foreach (var d in props)
                {
                    d.RemoveValueChanged(visualRemoved, AttachedPropertyValueChanged);
                }
                var proxyElement = GetProxyUIElement(visualRemoved);
                _panel.Children.Remove(proxyElement);
                SetProxyUIElement(visualRemoved, null);
            }
        }

        private void AttachedPropertyValueChanged(object sender, EventArgs args)
        {
            var element = (UIElement)sender;
            var proxyElement = GetProxyUIElement(element);
            if (proxyElement != null)
            {
                var props = GetAllAttachedProperties();
                foreach (var d in props)
                {
                    d.SetValue(proxyElement, d.GetValue(element));
                }
            }
        }

        private static IEnumerable<DependencyPropertyDescriptor> GetAllAttachedProperties()
        {
            if (_attachedProperties != null)
                return _attachedProperties;

            _attachedProperties = new List<DependencyPropertyDescriptor>();

            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                try
                {
                    foreach (Type t in assembly.GetTypes())
                    {
                        foreach (FieldInfo fi in t.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Static | BindingFlags.Public))
                        {
                            if (fi.FieldType == typeof(DependencyProperty))
                            {
                                DependencyProperty dp = (DependencyProperty)fi.GetValue(null);
                                try
                                {
                                    DependencyPropertyDescriptor dpd = DependencyPropertyDescriptor.FromProperty(dp, t);

                                    if (dpd != null && dpd.IsAttached && !dpd.IsReadOnly)
                                    {
                                        _attachedProperties.Add(dpd);
                                    }
                                }
                                catch (ArgumentException)
                                {
                                    // there was a problem obtaining the
                                    // DependencyPropertyDescriptor?
                                }
                            }
                        }
                    }
                }
                catch
                {

                }
            }

            return _attachedProperties;
        }
    }
}
