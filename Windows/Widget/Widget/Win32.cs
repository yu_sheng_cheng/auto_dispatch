﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Widget
{
    public static class Win32
    {
        public const int WM_SYSCOMMAND = 0x0112;
        public const int WM_NCLBUTTONDBLCLK = 0x00A3;

        public const int SC_MOVE = 0xF010;
        public const int SC_RESTORE = 0xF120;
    }
}
