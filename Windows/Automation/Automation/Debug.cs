﻿using SwissKnife.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation
{
    internal class Debug : ILog
    {
        public void d(string message)
        {
#if DEBUG
            Log("DEBUG", null, message, null);
#endif
        }

        public void d(string keyword, string message)
        {
#if DEBUG
            Log("DEBUG", keyword, message, null);
#endif
        }

        public void i(string message)
        {
#if DEBUG
            Log("INFO", null, message, null);
#endif
        }

        public void i(string keyword, string message)
        {
#if DEBUG
            Log("INFO", keyword, message, null);
#endif
        }

        public void w(string message)
        {
#if DEBUG
            Log("WARNING", null, message, null);
#endif
        }

        public void w(string keyword, string message)
        {
#if DEBUG
            Log("WARNING", keyword, message, null);
#endif
        }

        public void w(string message, Exception ex)
        {
#if DEBUG
            Log("WARNING", null, message, ex);
#endif
        }

        public void w(string keyword, string message, Exception ex)
        {
#if DEBUG
            Log("WARNING", keyword, message, ex);
#endif
        }

        public void e(string message)
        {
#if DEBUG
            Log("ERROR", null, message, null);
#endif
        }

        public void e(string keyword, string message)
        {
#if DEBUG
            Log("ERROR", keyword, message, null);
#endif
        }

        public void e(string message, Exception ex)
        {
#if DEBUG
            Log("ERROR", null, message, ex);
#endif
        }

        public void e(string keyword, string message, Exception ex)
        {
#if DEBUG
            Log("ERROR", keyword, message, ex);
#endif
        }

        public void f(string message)
        {
#if DEBUG
            Log("FATAL", null, message, null);
#endif
        }

        public void f(string keyword, string message)
        {
#if DEBUG
            Log("FATAL", keyword, message, null);
#endif
        }

        public void f(string message, Exception ex)
        {
#if DEBUG
            Log("FATAL", null, message, ex);
#endif
        }

        public void f(string keyword, string message, Exception ex)
        {
#if DEBUG
            Log("FATAL", keyword, message, ex);
#endif
        }

        private void Log(string level, string keyword, string message, Exception exception)
        {
#if DEBUG
            if (message == null)
                message = string.Empty;

            message = exception == null ? message : $"{message}{Environment.NewLine}{exception.ToString()}";

            if (string.IsNullOrWhiteSpace(keyword))
                Console.WriteLine("<{0}> {1}", level, message);
            else
                Console.WriteLine("[{0}] <{1}> {2}", keyword, level, message);
#endif
        }
    }
}
