﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Helper
{
    public struct Barrier
    {
        private byte _participantCount;

        public bool Done { get { return _participantCount == 0; } }

        public void AddParticipants(byte participantCount)
        {
            int totalCount = _participantCount + participantCount;

            if (totalCount > byte.MaxValue)
                throw new ArgumentOutOfRangeException("participantCount",
                    "Adding participantCount participants would result in the number of participants exceeding the maximum number allowed.");

            _participantCount = unchecked((byte)totalCount);
        }

        public bool RemoveParticipants(byte participantCount)
        {
            if (participantCount == 0)
                throw new ArgumentException("The participantCount argument must be a positive value.", "participantCount");

            if (participantCount > _participantCount)
                throw new ArgumentOutOfRangeException("participantCount",
                    "The participantCount argument must be less than or equal the number of participants.");

            _participantCount -= participantCount;

            return _participantCount == 0;
        }

        public bool SignalAndWait()
        {
            if (_participantCount == 0)
                throw new InvalidOperationException("The barrier has no registered participants.");

            return --_participantCount == 0;
        }
    }
}
