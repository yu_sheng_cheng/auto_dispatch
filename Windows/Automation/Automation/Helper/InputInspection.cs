﻿using Automation.Core;
using Automation.Core.Alarm;
using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Enums;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Helper
{
    public class InputInspection : INamedObject, IPollable, ICompletionNotification
    {
        private readonly Completion _completion;
        private readonly ushort _inspectionTimeout;
        private readonly ushort _confirmationHoldTime;

        [ContractPublicPropertyName("IsEnabled")]
        private bool _started;

        private readonly Func<SupervisoryController, bool> _checkInput;

        private Timer _inspectionTimer = new Timer();
        private Timer _confirmationHoldTimer = new Timer();

        internal LinkedListNode<InputInspection> Node { get; }

        public string Name { get; }

        public bool IsEnabled { get { return _started; } }

        public bool IsCompleted { get; private set; }

        public CompletionResult Result { get; private set; }

        public Action<SupervisoryController, INamedObject, CompletionResult> Completed
        {
            get { return _completion.Completed; }
            set { _completion.Completed = value; }
        }

        public Func<SupervisoryController, AlarmReport> GetAlarmReport { get; }

        public InputInspection(string name, ushort inspectionTimeout, ushort confirmationHoldTime, Func<SupervisoryController, bool> checkInput,
            Func<SupervisoryController, AlarmReport> getAlarmReport = null)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            if (inspectionTimeout == 0)
                throw new ArgumentException("The 'inspectionTimeout' argument must be greater than zero.", "inspectionTimeout");

            _checkInput = checkInput ?? throw new ArgumentNullException("checkInput");
            GetAlarmReport = getAlarmReport;

            Node = new LinkedListNode<InputInspection>(this);
            Name = name;

            _inspectionTimeout = inspectionTimeout;
            _confirmationHoldTime = confirmationHoldTime;

            _completion = new Completion(this);
        }

        public void Start()
        {
            if (_started)
                throw new InvalidOperationException("Already started.");

            if (Node.List != null)
                throw new BugException("This InputInspection is already in progress.");

            _started = true;
            IsCompleted = false;
            Result = CompletionResult.Success;
        }

        public void Poll(SupervisoryController controller)
        {
#if DEBUG
            if (!_started)
                throw new InvalidOperationException("This InputInspection was not started.");
#endif
            if (!_inspectionTimer.IsEnabled)
                _inspectionTimer.Start(controller.GetExpirationJiffies(_inspectionTimeout));

            bool condition = _checkInput(controller);

            if (!condition)
            {
                _confirmationHoldTimer.Stop();

                if (_inspectionTimer.CheckTimeout(controller.Jiffies))
                    Complete(controller, CompletionResult.Timeout);

                return;
            }

            if (!_confirmationHoldTimer.IsEnabled)
                _confirmationHoldTimer.Start(controller.GetExpirationJiffies(_confirmationHoldTime));
            else if (_confirmationHoldTime == 0 | _confirmationHoldTimer.CheckTimeout(controller.Jiffies))
                Complete(controller);
        }

        private void Complete(SupervisoryController controller, CompletionResult result = CompletionResult.Success)
        {
            _started = false;
            _inspectionTimer.Stop();
            _confirmationHoldTimer.Stop();

            IsCompleted = true;
            Result = result;

            _completion.Complete(controller, result);
        }
    }
}
