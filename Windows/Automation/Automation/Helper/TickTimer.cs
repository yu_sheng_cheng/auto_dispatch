﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Helper
{
    public struct TickTimer
    {
        [ContractPublicPropertyName("IsEnabled")]
        private bool _started;
        private byte _dueTicks;
        private byte _ticks;

        public bool IsEnabled { get { return _started; } }

        public void Start(byte dueTicks = 2)
        {
            if (dueTicks == 0)
                throw new ArgumentException("The 'dueTicks' argument must be greater than zero.", "dueTicks");

            _dueTicks = dueTicks;

            if (!_started)
            {
                _started = true;
                _ticks = 0;
            }
        }

        public void Stop()
        {
            _started = false;
        }

        public bool Tick()
        {
            if (_started)
            {
                if (_ticks >= _dueTicks)
                    return true;

                ++_ticks;
            }
            return false;
        }
    }
}
