﻿using SwissKnife;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Helper
{
    public struct Timer
    {
        [ContractPublicPropertyName("IsEnabled")]
        private bool _started;
        private uint _expires;

        public bool IsEnabled { get { return _started; } }

        public void Restart(uint expires)
        {
            Stop();
            Start(expires);
        }

        public void Start(uint expires)
        {
            if (_started)
                throw new InvalidOperationException("Already started.");

            _started = true;
            _expires = expires;
        }

        public void Stop()
        {
            _started = false;
        }

        public bool CheckTimeout(uint jiffies)
        {
            return _started ? TimeComparison.IsAfter(jiffies, _expires) : false;
        }
    }
}
