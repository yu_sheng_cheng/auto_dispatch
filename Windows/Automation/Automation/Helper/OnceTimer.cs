﻿using Automation.Core.Controller;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Helper
{
    public class OnceTimer
    {
        private Timer _timer = new Timer();

        internal LinkedListNode<OnceTimer> Node { get; }

        public Action<SupervisoryController> Expired { get; set; }

        public OnceTimer()
        {
            Node = new LinkedListNode<OnceTimer>(this);
        }

        public void Start(SupervisoryController controller, int milliseconds, Action<SupervisoryController> expired)
        {
            if (Node.List != null)
                throw new BugException("This OnceTimer already started.");

            if (milliseconds <= 0)
                throw new ArgumentOutOfRangeException("milliseconds");

            Expired = expired ?? throw new ArgumentNullException("expired");

            _timer.Start(controller.GetExpirationJiffies(milliseconds));
        }

        public bool CheckTimeout(SupervisoryController controller)
        {
            if (_timer.CheckTimeout(controller.Jiffies))
            {
                _timer.Stop();

                var expired = Expired;
                Expired = null;
                expired?.Invoke(controller);
                return true;
            }
            return false;
        }
    }
}
