﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Helper
{
    public struct Pulse
    {
        private const byte Low = 0;
        private const byte High = 1;

        private ushort _highWidth;
        private ushort _lowWidth;
        private ushort _ticks;

        public bool IsEnabled { get; private set; }

        public byte Level
        {
            get { return (!IsEnabled | _ticks >= _highWidth) ? Low : High; }
        }

        public void Output(uint highWidth, uint lowWidth = 2)
        {
            if (highWidth == 0)
                throw new ArgumentException("The 'highWidth' argument must be greater than zero.", "highWidth");

            if (lowWidth == 0)
                throw new ArgumentException("The 'lowWidth' argument must be greater than zero.", "lowWidth");

            if (IsEnabled)
                throw new InvalidOperationException("Already output.");

            _highWidth = highWidth <= 30000 ? (ushort)highWidth : (ushort)30000;
            _lowWidth = lowWidth <= 30000 ? (ushort)lowWidth : (ushort)30000;
            IsEnabled = true;
        }

        public void Disable()
        {
            _ticks = 0;
            IsEnabled = false;
        }

        public bool Tick()
        {
            if (!IsEnabled)
                return false;

            if (++_ticks == _highWidth)
                return true;

            if (_ticks == (_lowWidth + 2))
                Disable();

            return false;
        }
    }
}
