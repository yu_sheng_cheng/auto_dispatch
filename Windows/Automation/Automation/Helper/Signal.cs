﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Helper
{
    public struct Signal
    {
        private const byte Low = 0;
        private const byte High = 1;

        private byte _countdown;

        public byte Level { get; private set; }

        public void Output(byte level, bool edge = false)
        {
            if (edge | level != Level)
            {
                Level = level;
                _countdown = 2;
            }
        }

        public bool Tick(byte level)
        {
            return !(level != Level | !Tick());
        }

        public bool Tick()
        {
            if (_countdown == 0)
                return true;

            --_countdown;
            return false;
        }

        public bool IsStable()
        {
            return _countdown == 0;
        }

        public bool IsStable(byte level)
        {
            return !(level != Level | _countdown > 0);
        }

        public void Reset()
        {
            Level = Low;
            _countdown = 0;
        }
    }
}
