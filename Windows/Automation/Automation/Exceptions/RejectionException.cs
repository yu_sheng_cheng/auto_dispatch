﻿using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Exceptions
{
    [Serializable()]
    public class RejectionException : Exception
    {
        public RejectionReason Reason { get; }

        public RejectionException(RejectionReason reason) : this(reason, null, null) { }

        public RejectionException(RejectionReason reason, string message) : this(reason, message, null) { }

        public RejectionException(RejectionReason reason, string message, Exception inner) : base(message ?? reason.GetMessage(), inner)
        {
            Reason = reason;
        }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client. 
        protected RejectionException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
