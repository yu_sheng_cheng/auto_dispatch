﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation
{
    public enum AutomationError : short
    {
        Error = -1,
        Success = 0,
        Timeout
    }
}
