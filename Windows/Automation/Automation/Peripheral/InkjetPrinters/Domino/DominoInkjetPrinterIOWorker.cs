﻿using Automation.Core;
using Automation.Core.Controller;
using Automation.Core.IOWorkers;
using Automation.Enums;
using Automation.Exceptions;
using Net.AsyncSocket.Tcp;
using SwissKnife;
using SwissKnife.Exceptions;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Automation.Peripheral.InkjetPrinters.Domino
{
    public sealed class DominoInkjetPrinterIOWorker : NetworkIOWorker<string, string>
    {
        private const int WatchdogTimeout = 5000;

        private readonly byte[] _reply = new byte[1460];
        private readonly List<string> _variableTexts = new List<string>();
        private string _projectName = string.Empty;

        public DominoInkjetPrinter Printer { get; }

        public InkjetPrinterCommand Command { get; set; }

        public Severity Severity { get; private set; }

        public ushort StatusId { get; private set; }

        public string Message { get; private set; } = string.Empty;

        public string ProjectName
        {
            get { return _projectName; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException("String cannot be empty or null.", "ProjectName");

                _projectName = value;
            }
        }

        internal DominoInkjetPrinterIOWorker(DominoInkjetPrinter printer, SupervisoryController controller, IPEndPoint remoteEndPoint) : base(printer.Name, controller, remoteEndPoint)
        {
            Printer = printer;
            Timeout = WatchdogTimeout;
        }

        public override void Dispose()
        {
            DisposeAsync().Wait();
        }

        public async Task DisposeAsync()
        {
            TimeoutSignal.Reset();

            ThreadPool.QueueUserWorkItem(async (_) =>
            {
                await Task.Delay(WatchdogTimeout).ConfigureAwait(false);
                TimeoutSignal.Raise();
            });

            await SendThenReceiveAsync("MARK \"STOP\"").ConfigureAwait(false);

            base.Dispose();
        }

        public void PrepareToPrint(string projectName, IEnumerable<KeyValuePair<string, string>> variableTexts)
        {
            if (IsBusy)
                throw new RejectionException(RejectionReason.Busy);

            if (string.IsNullOrEmpty(projectName))
                throw new ArgumentException("String cannot be empty or null.", "projectName");

            Command = InkjetPrinterCommand.PrepareToPrint;

            ProjectName = projectName;

            if (variableTexts != null)
            {
                foreach (var pair in variableTexts)
                {
                    AddVariableText(pair.Key, pair.Value);
                }
            }
        }

        private void AddVariableText(string obj, string text)
        {
            if (string.IsNullOrEmpty(obj))
                throw new ArgumentException("String cannot be empty or null.", "obj");

            if (text == null)
                throw new ArgumentNullException("text");

            _variableTexts.Add($"SETTEXT \"{obj}\" \"{text}\"");
        }

        protected override async Task<OperationResult> Execute()
        {
            OperationResult<string> result;

            switch (Command)
            {
                case InkjetPrinterCommand.GetStatus:
                    result = CheckResult(await SendThenReceiveAsync("GETSTATUS").ConfigureAwait(false));
                    if (!result.IsSuccess)
                        return result;

                    string response = result.Value;

                    var match = Regex.Match(response, "^RESULT GETSTATUS (?<SEVERITY>[0-9]+) (?<STATUS_ID>[0-9]+) \"(?<TEXT>.+)\"", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
                    var groups = match.Groups;

                    Severity = (Severity)byte.Parse(groups["SEVERITY"].Value);
                    StatusId = ushort.Parse(groups["STATUS_ID"].Value);
                    Message = groups["TEXT"].Value;

                    return result;

                case InkjetPrinterCommand.PrepareToPrint:
                    try
                    {
                        result = CheckResult(await SendThenReceiveAsync("MARK \"STOP\"").ConfigureAwait(false));
                        if (!result.IsSuccess)
                            return result;

                        result = CheckResult(await SendThenReceiveAsync($"LOADPROJECT \"{_projectName}\"").ConfigureAwait(false));
                        if (!result.IsSuccess)
                            return result;

                        foreach (var vtext in _variableTexts)
                        {
                            result = CheckResult(await SendThenReceiveAsync(vtext).ConfigureAwait(false));
                            if (!result.IsSuccess)
                                return result;
                        }

                        return CheckResult(await SendThenReceiveAsync("MARK \"START\"").ConfigureAwait(false));
                    }
                    finally
                    {
                        _variableTexts.Clear();
                    }

                case InkjetPrinterCommand.Print:
                    throw new BugException("Should not go in here.");

                case InkjetPrinterCommand.Disable:
                    return CheckResult(await SendThenReceiveAsync("MARK \"STOP\"").ConfigureAwait(false));

                default:
                    throw new NotImplementedException();
            }
        }

        private OperationResult<string> CheckResult(OperationResult<string> result)
        {
            if (!result.IsSuccess)
                return result;

            string response = result.Value;

            if (IsError(response, out int error))
                return OperationResultFactory.CreateFailedResult<string>(GetType().FullName, (int)AutomationError.Error, error, SR.InkjetPrinterProtocolError);

            return result;
        }

        private bool IsError(string response, out int error)
        {
            if (response.StartsWith("ERROR"))
            {
                Log.w(LogKeyword, $"An error occurred ({response})");
                error = int.Parse(response.Substring(5));
                return true;
            }

            error = 0;
            return false;
        }

        protected override async Task<OperationResult<string>> SendThenReceiveAsync(AsyncTcpClient session, string commandString)
        {
            byte[] command = Encoding.UTF8.GetBytes($"{commandString}\r\n");

            string errorMessage = SR.SendFailure;

            try
            {
                await session.SendAsync(command, 0, command.Length).ConfigureAwait(false);

                errorMessage = SR.ReceiveFailure;

                int bytesRead = await session.ReceiveAsync(_reply, 0, _reply.Length).ConfigureAwait(false);

                return OperationResultFactory.CreateSuccessfulResult(Encoding.UTF8.GetString(_reply, 0, bytesRead).TrimEnd('\r', '\n'), SR.Success);
            }
            catch (Exception e)
            {
                if (!(e is ObjectDisposedException))
                    Log.e(LogKeyword, "SendThenReceiveAsync:", e);

                return OperationResultFactory.CreateFailedResult<string>(GetType().FullName, errorMessage, e);
            }
        }

        public override void Complete()
        {
            base.Complete();
        }
    }
}
