﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Peripheral.InkjetPrinters.Domino
{
    public enum Severity : byte
    {
        Information = 0,
        Warning,
        TemporaryFault,
        CriticalFault,
        NeedsToBeResetByHardware
    }
}
