﻿using Automation.Core.Alarm;
using Automation.Core.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Peripheral.InkjetPrinters.Domino
{
    public sealed class DominoInkjetPrinter : IAlarmSource
    {
        public bool AlarmOccurred => throw new NotImplementedException();

        public ushort ErrorCode => throw new NotImplementedException();

        public string Name => throw new NotImplementedException();

        public AlarmReport GetAlarmReport(SupervisoryController controller)
        {
            throw new NotImplementedException();
        }

        public void Poll(SupervisoryController controller)
        {
            throw new NotImplementedException();
        }

        public void Reset(SupervisoryController controller)
        {
            throw new NotImplementedException();
        }
    }
}
