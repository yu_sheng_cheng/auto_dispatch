﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automation.Peripheral.InkjetPrinters.Domino
{
    [XmlRoot(ElementName = "DominoInkjetPrinter", Namespace = "Automation.Peripheral.InkjetPrinters.Domino.DominoInkjetPrinterProfile.xml", IsNullable = false)]
    public sealed class DominoInkjetPrinterProfile
    {
        [XmlAttribute] public string Name { get; set; }

        public DominoInkjetPrinterConfiguration Configuration { get; set; }

        public void CheckAfterDeserialization()
        {
            if (string.IsNullOrEmpty(Name))
                throw new FormatException("The 'Name' attribute cannot be empty or null.");

            if (Configuration == null)
                Configuration = new DominoInkjetPrinterConfiguration();
        }
    }

    public sealed class DominoInkjetPrinterConfiguration
    {
        private ushort _printingOneTime = 500;

        public ushort PrintingOneTime
        {
            get { return _printingOneTime; }
            set
            {
                if (value == 0)
                    throw new ArgumentException("The PrintingOneTime property cannot be zero.", "PrintingOneTime");

                _printingOneTime = value;
            }
        }

        public void CopyTo(DominoInkjetPrinterConfiguration destination)
        {
            if (destination == null)
                throw new ArgumentNullException("destination");

            destination._printingOneTime = _printingOneTime;
        }
    }
}
