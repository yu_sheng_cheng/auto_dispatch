﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Peripheral.InkjetPrinters
{
    public enum InkjetPrinterCommand : byte
    {
        GetStatus,
        PrepareToPrint,
        Print,
        Disable
    }
}
