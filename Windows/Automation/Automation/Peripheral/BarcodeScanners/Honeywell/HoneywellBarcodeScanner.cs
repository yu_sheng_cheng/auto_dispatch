﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Peripheral.BarcodeScanners.Honeywell
{
    public static class HoneywellBarcodeScanner
    {
        public static readonly byte[] StartScanCommand = new byte[] { 0x16, 0x54, 0x0D };
        public static readonly byte[] StopScanCommand = new byte[] { 0x16, 0x55, 0x0D };
    }
}
