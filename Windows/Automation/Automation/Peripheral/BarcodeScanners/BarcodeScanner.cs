﻿using Automation.Core.Alarm;
using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Core.IOWorkers;
using Automation.Core.Messages.ManualControl;
using Automation.Core.Messages.ManualControl.Peripheral;
using Automation.Enums;
using Automation.Profiles;
using IO;
using SwissKnife;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automation.Peripheral.BarcodeScanners
{
    public sealed class BarcodeScanner : IOWorker, IAlarmSource, IManualControlMessageHandler
    {
        private readonly AsyncSerialPort _port;
        private readonly byte[] _startScanCommand;
        private readonly byte[] _stopScanCommand;
        private bool _isOpen;

        public string NewLine
        {
            get { return _port.NewLine; }
            set { _port.NewLine = value; }
        }

        public CompletionResult ScanResult { get; private set; }

        public string Barcode { get; private set; }

        public bool AlarmOccurred { get; private set; }

        public ushort ErrorCode { get { return AlarmOccurred ? (ushort)0x9487 : (ushort)0; } }

        public BarcodeScanner(string name, SupervisoryController controller, byte[] startScanCommand, byte[] stopScanCommand) : base(name, controller, false)
        {
            if (startScanCommand == null || startScanCommand.Length == 0)
                throw new ArgumentException("Array cannot be empty or null.", "startScanCommand");

            if (stopScanCommand == null || stopScanCommand.Length == 0)
                throw new ArgumentException("Array cannot be empty or null.", "stopScanCommand");

            SerialPortProfile profile;

            using (var reader = new StreamReader(Path.Combine(controller.ProfileRootFolder, $"{name}.xml"), Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(SerialPortProfile));

                profile = (SerialPortProfile)serializer.Deserialize(reader);
                if (profile.Name != name)
                    throw new FormatException($"The 'Name' attribute must be equal to {name}");

                profile.CheckAfterDeserialization();
            }

            _port = new AsyncSerialPort(profile.PortName, profile.BaudRate, profile.Parity, profile.DataBits);
            Timeout = profile.ReadTimeout;

            _startScanCommand = startScanCommand;
            _stopScanCommand = stopScanCommand;

            controller.AddMiscObject(this);
        }

        public override void Dispose()
        {
            _port.Dispose();
        }

        protected override async Task<OperationResult> Execute()
        {
            try
            {
                if (!_isOpen)
                {
                    _port.Open();
                    _isOpen = true;
                }

                try
                {
                    int timeout = Timeout;

                    _port.AsyncTimeout = timeout > 0 ? timeout : 10000;

                    _port.Write(_startScanCommand, 0, _startScanCommand.Length);

                    Barcode = await _port.ReadLineAsync().ConfigureAwait(false);

                    return OperationResultFactory.CreateSuccessfulResult(SR.Success);
                }
                catch (TimeoutException)
                {
                    _port.Write(_stopScanCommand, 0, _stopScanCommand.Length);

                    await Task.Delay(100).ConfigureAwait(false);

                    return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)AutomationError.Timeout, SR.Timeout);
                }
                finally
                {
                    _port.DiscardInBuffer();
                }
            }
            catch (Exception e)
            {
                if (_isOpen)
                {
                    _port.Close();
                    _isOpen = false;
                }

                Log.e(LogKeyword, "Execute:", e);

                return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)AutomationError.Error, e.Message);
            }
        }

        public override void Complete()
        {
            ScanResult = CompletionResult.Success;

            if (!Result.IsSuccess)
            {
                Barcode = string.Empty;

                if (Result.ErrorCode == (int)AutomationError.Error)
                {
                    ScanResult = CompletionResult.AlarmOccurred;
                    AlarmOccurred = true;
                }
            }

            base.Complete();
        }

        public void Poll(SupervisoryController controller)
        {

        }

        public void Reset(SupervisoryController controller)
        {
            AlarmOccurred = false;
        }

        public AlarmReport GetAlarmReport(SupervisoryController controller)
        {
            if (!AlarmOccurred)
                throw new InvalidOperationException("'GetAlarmReport' method only can be called when alarming.");

            return controller.CreateAlarmReport(Name, "Error");
        }

        public void OnIncomingMessage(SupervisoryController controller, ManualControlMessage msg)
        {
            BarcodeScannerManualControlMessage message = msg as BarcodeScannerManualControlMessage;

            if (message == null)
            {
                message.Result = ManualControlResult.InvalidArgument;
                message.Complete(false);
                return;
            }

            if (IsBusy)
            {
                message.Result = ManualControlResult.Busy;
                message.Complete(false);
                return;
            }

            controller.AddIORequest(this, message.OnScanCompleted);
        }
    }
}
