﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automation.Profiles.Cylinder
{
    [XmlRoot(ElementName = "Cylinder", Namespace = "Automation.Profiles.Cylinder.CylinderProfile.xml", IsNullable = false)]
    public sealed class CylinderProfile
    {
        [XmlAttribute] public string Name { get; set; }

        [XmlAttribute] public bool HasSensor { get; set; } = true;

        public CylinderConfiguration Configuration { get; set; }

        public void CheckAfterDeserialization()
        {
            if (string.IsNullOrEmpty(Name))
                throw new FormatException("The 'Name' attribute cannot be empty or null.");

            if (Configuration == null)
                throw new FormatException("The 'Configuration' property was not set.");
        }
    }
}
