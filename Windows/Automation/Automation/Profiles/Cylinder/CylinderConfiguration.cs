﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Profiles.Cylinder
{
    public sealed class CylinderConfiguration
    {
        private const ushort DefaultPositioningTimeout = 5000;
        private const ushort DefaultPosition1DoneHoldTime = 100;
        private const ushort DefaultPosition2DoneHoldTime = 100;

        private const ushort DefaultPosition1Timeout = 1000;
        private const ushort DefaultPosition2Timeout = 1000;

        private ushort _positioningTimeout = DefaultPositioningTimeout;
        private ushort _position1DoneHoldTime = DefaultPosition1DoneHoldTime;
        private ushort _position2DoneHoldTime = DefaultPosition2DoneHoldTime;

        private ushort _position1Timeout = DefaultPosition1Timeout;
        private ushort _position2Timeout = DefaultPosition2Timeout;

        public ushort PositioningTimeout
        {
            get { return _positioningTimeout; }
            set
            {
                _positioningTimeout = value != 0 ? value : DefaultPositioningTimeout;
            }
        }

        public ushort Position1DoneHoldTime
        {
            get { return _position1DoneHoldTime; }
            set
            {
                _position1DoneHoldTime = value != 0 ? value : DefaultPosition1DoneHoldTime;
            }
        }

        public ushort Position2DoneHoldTime
        {
            get { return _position2DoneHoldTime; }
            set
            {
                _position2DoneHoldTime = value != 0 ? value : DefaultPosition2DoneHoldTime;
            }
        }

        public ushort Position1Timeout
        {
            get { return _position1Timeout; }
            set
            {
                _position1Timeout = value != 0 ? value : DefaultPosition1Timeout;
            }
        }

        public ushort Position2Timeout
        {
            get { return _position2Timeout; }
            set
            {
                _position2Timeout = value != 0 ? value : DefaultPosition2Timeout;
            }
        }

        public void CopyTo(CylinderConfiguration destination)
        {
            if (destination == null)
                throw new ArgumentNullException("destination");

            destination._positioningTimeout = _positioningTimeout;
            destination._position1DoneHoldTime = _position1DoneHoldTime;
            destination._position2DoneHoldTime = _position2DoneHoldTime;

            destination._position1Timeout = _position1Timeout;
            destination._position2Timeout = _position2Timeout;
        }
    }
}
