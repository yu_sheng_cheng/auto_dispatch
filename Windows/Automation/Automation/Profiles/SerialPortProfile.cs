﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automation.Profiles
{
    [XmlRoot(ElementName = "SerialPort", Namespace = "Automation.Profiles.SerialPortProfile.xml", IsNullable = false)]
    public class SerialPortProfile
    {
        [XmlAttribute] public string Name { get; set; }

        public string PortName { get; set; }

        public int BaudRate { get; set; }

        public int DataBits { get; set; }

        public Parity Parity { get; set; }

        public ushort ReadTimeout { get; set; }

        public virtual void CheckAfterDeserialization()
        {
            if (string.IsNullOrEmpty(Name))
                throw new FormatException("The 'Name' attribute cannot be empty or null.");

            if (string.IsNullOrEmpty(PortName))
                throw new FormatException("The 'PortName' property cannot be empty or null.");

            if (BaudRate <= 0)
                throw new FormatException("The 'BaudRate' property must be greater than zero.");

            if (DataBits != 7 && DataBits != 8)
                throw new FormatException("The 'DataBits' property must to be equal to 7 or 8.");

            if (ReadTimeout == 0)
                throw new FormatException("The 'ReadTimeout' property must be greater than zero.");
        }
    }
}
