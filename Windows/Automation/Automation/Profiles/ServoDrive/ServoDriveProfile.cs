﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automation.Profiles.ServoDrive
{
    [XmlRoot(ElementName = "ServoDrive", Namespace = "Automation.Profiles.ServoDrive.ServoDriveProfile.xml", IsNullable = false)]
    public sealed class ServoDriveProfile
    {
        private Dictionary<string, ServoDrivePosition> _dictionary;

        [XmlAttribute] public string Name { get; set; }
        [XmlAttribute] public string Id { get; set; }
        [XmlAttribute] public bool IsΘ { get; set; }

        public ServoDriveParameters Parameters { get; set; }

        [XmlArrayItem(ElementName = "Position")] public ServoDrivePosition[] Positions { get; set; }

        public void CheckAfterDeserialization()
        {
            if (string.IsNullOrEmpty(Name))
                throw new FormatException("The 'Name' attribute cannot be empty or null.");

            if (Id == null)
                Id = string.Empty;

            if (Parameters == null)
                throw new FormatException("The 'Parameters' property was not set.");

            if (Positions == null)
                Positions = new ServoDrivePosition[0];
            else
            {
                foreach (var position in Positions)
                {
                    position.CheckAfterDeserialization();
                }

                if (Positions.Length >= 7)
                {
                    _dictionary = new Dictionary<string, ServoDrivePosition>(StringComparer.Ordinal);

                    foreach (var position in Positions)
                    {
                        _dictionary.Add(position.Name, position);
                    }
                }
            }
        }

        public bool TryGetPosition(string name, out ServoDrivePosition position)
        {
            if (_dictionary == null)
            {
                if (name == null)
                    throw new ArgumentNullException("name");

                foreach (var pos in Positions)
                {
                    if (name == pos.Name)
                    {
                        position = pos;
                        return true;
                    }
                }

                position = null;
                return false;
            }
            else
                return _dictionary.TryGetValue(name, out position);
        }

        public bool TryUpdatePosition(string name, int value)
        {
            if (_dictionary == null)
            {
                if (name == null)
                    throw new ArgumentNullException("name");

                int count = Positions.Length;

                for (int i = 0; i < count; ++i)
                {
                    if (name == Positions[i].Name)
                    {
                        Positions[i].Value = value;
                        return true;
                    }
                }
            }
            else
            {
                if (_dictionary.ContainsKey(name))
                {
                    _dictionary[name].Value = value;
                    return true;
                }
            }

            return false;
        }
    }
}
