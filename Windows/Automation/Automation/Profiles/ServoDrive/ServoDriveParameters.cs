﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Profiles.ServoDrive
{
    public sealed class ServoDriveParameters
    {
        private int _accelerationTime = 1;
        private int _decelerationTime = 1;
        private int _speedLimit = 1;
        private int _torqueLimit = 1;
        private int _jogSpeedLimit = 1;
        private int _jogSpeed = 1;
        private int _positioningSpeed = 1;

        public int AccelerationTime
        {
            get { return _accelerationTime; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("AccelerationTime");

                _accelerationTime = value;
            }
        }

        public int DecelerationTime
        {
            get { return _decelerationTime; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("DecelerationTime");

                _decelerationTime = value;
            }
        }

        public int SpeedLimit
        {
            get { return _speedLimit; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("SpeedLimit");

                _speedLimit = value;
            }
        }

        public int TorqueLimit
        {
            get { return _torqueLimit; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("TorqueLimit");

                _torqueLimit = value;
            }
        }

        public int JogSpeedLimit
        {
            get { return _jogSpeedLimit; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("JogSpeedLimit");

                _jogSpeedLimit = value;
            }
        }

        public int JogSpeed
        {
            get { return _jogSpeed; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("JogSpeed");

                _jogSpeed = value;
            }
        }

        public int PositioningSpeed
        {
            get { return _positioningSpeed; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("PositioningSpeed");

                _positioningSpeed = value;
            }
        }

        public void CopyTo(ServoDriveParameters destination)
        {
            if (destination == null)
                throw new ArgumentNullException("destination");

            destination._accelerationTime = _accelerationTime;
            destination._decelerationTime = _decelerationTime;
            destination._speedLimit = _speedLimit;
            destination._torqueLimit = _torqueLimit;
            destination._jogSpeedLimit = _jogSpeedLimit;
            destination._jogSpeed = _jogSpeed;
            destination._positioningSpeed = _positioningSpeed;
        }
    }
}
