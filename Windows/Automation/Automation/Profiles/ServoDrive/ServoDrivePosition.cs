﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automation.Profiles.ServoDrive
{
    public sealed class ServoDrivePosition
    {
        [XmlAttribute] public string Name { get; set; }

        [XmlAttribute] public ushort Id { get; set; }

        [XmlAttribute] public bool IsArray { get; set; }

        [XmlAttribute] public bool Horizontal { get; set; }

        [XmlAttribute] public byte Rows { get; set; }

        [XmlAttribute] public byte Columns { get; set; }

        [XmlAttribute] public int Interval { get; set; }

        public int Value { get; set; }

        public void CheckAfterDeserialization()
        {
            if (string.IsNullOrEmpty(Name))
                throw new FormatException("The 'Name' attribute cannot be empty or null.");

            switch (Id)
            {
                case 0:
                    throw new FormatException("The 'Id' attribute must be greater than zero.");
                case 1:
                    if (Name != "Home")
                        throw new FormatException("The 'Name' attribute must be equal to 'Home' when the 'Id' attribute is equal to one.");
                    break;
                case 2:
                    if (Name != "Standby")
                        throw new FormatException("The 'Name' attribute must be equal to 'Standby' when the 'Id' attribute is equal to two.");
                    break;
            }

            if (IsArray)
            {
                if (Rows == 0)
                    throw new FormatException("The 'Rows' property must be greater than zero.");

                if (Columns == 0)
                    throw new FormatException("The 'Columns' property must be greater than zero.");

                if (Interval == 0)
                {
                    if ((Horizontal && Columns != 1) | (!Horizontal && Rows != 1))
                        throw new FormatException("The 'Interval' property cannot be zero.");
                }
            }
        }

        public bool IsValidArrayNumber(ushort number)
        {
            if (!IsArray | number == 0 | number > Rows * Columns | Id + number > ushort.MaxValue + 1)
                return false;

            return true;
        }
    }
}
