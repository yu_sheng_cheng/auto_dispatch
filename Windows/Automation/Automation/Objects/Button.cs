﻿using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Core.Registers;
using Automation.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects
{
    public class Button : INamedObject, IPollable
    {
        private readonly BitCell _input;

        private readonly ushort _pressedSignalHoldTime;

        private readonly Action _pressed;

        private Timer _timer = new Timer();
        private bool _released = true;

        public string Name { get; }

        public Button(string name, BitCell input, ushort pressedSignalHoldTime, Action pressed)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            Name = name;
            _input = input;
            _pressedSignalHoldTime = pressedSignalHoldTime;
            _pressed = pressed ?? throw new ArgumentNullException("pressed");
        }

        public void Poll(SupervisoryController controller)
        {
            IORegister register = controller.IORegister;

            bool pressed = register.GetContact(_input);

            if (pressed)
            {
                if (!_released)
                    return;

                if (_pressedSignalHoldTime > 0)
                {
                    if (!_timer.IsEnabled)
                    {
                        _timer.Start(unchecked(controller.Jiffies + controller.GetJiffiesFromMilliseconds(_pressedSignalHoldTime)));
                        return;
                    }

                    if (_timer.CheckTimeout(controller.Jiffies))
                    {
                        _released = false;
                        _pressed.Invoke();
                    }
                }
                else
                {
                    _released = false;
                    _pressed.Invoke();
                }
            }
            else
            {
                _released = true;
                _timer.Stop();
            }
        }
    }
}
