﻿using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Core.Registers;
using Automation.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects
{
    public class SafetyDoor : INamedObject, IPollable
    {
        private readonly BitCell _closedInput;
        private readonly BitCell _lockOutput;

        private readonly ushort _closedSignalHoldTime;

        private bool _closed;
        private bool _stable;
        private Timer _timer = new Timer();

        public string Name { get; }

        public bool CanBeUnlockedDuringAutoRun { get; }

        public bool IgnoreUnlockAllCommand { get; }

        public bool IsLocked => _closed && _stable;

        public SafetyDoor(string name, BitCell closedInput, BitCell lockOutput, ushort closedSignalHoldTime, bool canBeUnlockedDuringAutoRun, bool ignoreUnlockAllCommand)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            if (closedSignalHoldTime == 0)
                throw new ArgumentException("The 'closedSignalHoldTime' argument must be greater than zero.", "closedSignalHoldTime");

            Name = name;

            _closedInput = closedInput;
            _lockOutput = lockOutput;

            _closedSignalHoldTime = closedSignalHoldTime;

            CanBeUnlockedDuringAutoRun = canBeUnlockedDuringAutoRun;
            IgnoreUnlockAllCommand = ignoreUnlockAllCommand;
        }

        public void Poll(SupervisoryController controller)
        {
            IORegister register = controller.IORegister;

            bool closed = register.GetContact(_closedInput);

            bool changed = closed ^ _closed;

            if (changed)
            {
                _closed = closed;

                _stable = false;
                _timer.Stop();
            }
            else
            {
                if (!_stable)
                {
                    if (!_timer.IsEnabled)
                    {
                        _timer.Start(controller.GetExpirationJiffies(_closedSignalHoldTime));
                        return;
                    }

                    if (_timer.CheckTimeout(controller.Jiffies))
                    {
                        _stable = true;
                        _timer.Stop();
                    }
                }
            }
        }

        public void Lock(SupervisoryController controller)
        {
            controller.IORegister.SetCoil(_lockOutput, true);
        }

        public void Unlock(SupervisoryController controller)
        {
            controller.IORegister.SetCoil(_lockOutput, false);
            controller.EquipmentState.AreAllSafetyDoorsLocked = controller.IsTuning;
            _stable = false;
            _timer.Stop();
        }
    }
}
