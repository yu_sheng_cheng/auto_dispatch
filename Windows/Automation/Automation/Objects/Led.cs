﻿using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Core.Registers;
using Automation.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects
{
    public class Led : INamedObject, IPollable
    {
        private readonly BitCell _output;

        private ushort _onTime;
        private ushort _offTime;
        private Timer _timer = new Timer();

        public string Name { get; }

        public Led(string name, BitCell output)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            Name = name;
            _output = output;
        }

        public void Poll(SupervisoryController controller)
        {
            if (_timer.CheckTimeout(controller.Jiffies))
            {
                IORegister register = controller.IORegister;

                bool on = !register.GetCoil(_output);

                _timer.Restart(controller.GetExpirationJiffies(on ? _onTime : _offTime));
                register.SetCoil(_output, on);
            }
        }

        public void Turn(SupervisoryController controller, bool on)
        {
            _timer.Stop();
            controller.IORegister.SetCoil(_output, on);
        }

        public void Blink(SupervisoryController controller, ushort onTime, ushort offTime)
        {
            if (onTime == 0)
                throw new ArgumentException("The onTime argument must be greater than zero.", "onTime");

            if (offTime == 0)
                throw new ArgumentException("The offTime argument must be greater than zero.", "offTime");

            _onTime = onTime;
            _offTime = offTime;
            _timer.Restart(controller.GetExpirationJiffies(onTime));

            controller.IORegister.SetCoil(_output, true);
        }
    }
}
