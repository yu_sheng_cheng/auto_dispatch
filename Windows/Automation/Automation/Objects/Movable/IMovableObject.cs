﻿using Automation.Core.Alarm;
using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects.Movable
{
    public interface IMovableObject : IAlarmSource, ICompletionNotification, IManualControlMessageHandler, IEventObserver
    {
        MovableObjectType Type { get; }

        bool IsEnabled { get; }

        bool HadReturnedToHomePosition { get; }

        bool IsStationary { get; }

        bool IsBusy { get; }

        Action<SupervisoryController> StopCommand { get; }

        Func<SupervisoryController, string, ushort> MistakeProofing { get; set; }

        void OnPositiveAirPressureSourceChanged(SupervisoryController controller, bool abnormal);

        void ReturnToHomePosition(SupervisoryController controller);

        RejectionReason CanExecuteGo(SupervisoryController controller);

        void Go(SupervisoryController controller, string position, Action<SupervisoryController, INamedObject, CompletionResult> completed = null);

        bool IsOn(string position);

        bool IsOnUnexpectedPosition();

        bool CheckIfReadyToAutoRun(SupervisoryController controller);
    }
}
