﻿using Automation.Core;
using Automation.Core.Alarm;
using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Core.Messages.ManualControl;
using Automation.Core.Messages.ManualControl.Movable;
using Automation.Core.Registers;
using Automation.Enums;
using Automation.Exceptions;
using Automation.Helper;
using Automation.Profiles.Cylinder;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Automation.Objects.Movable.Cylinders
{
    public class NonSensorDualPositionSolenoidValve : IMovableObject
    {
        private const byte Low = 0;
        private const byte High = 1;

        private const byte Unknown = 0;
        private const byte Position1 = 1;
        private const byte Position2 = 2;

        private readonly BitCell _position1Output;
        private readonly BitCell _position2Output;

        private readonly string _acting1Name;
        private readonly string _acting2Name;

        private readonly CylinderProfile _profile;

        private readonly Completion _completion;

        private bool _isReady;

        private Timer _positioningTimer = new Timer();
        private Signal _goSignal = new Signal();

        private byte _targetPosition;
        private byte _currentPosition;

        public MovableObjectType Type { get { return MovableObjectType.Cylinder; } }

        public string Name { get; }

        public Action<SupervisoryController> StopCommand { get { return null; } }

        public Func<SupervisoryController, string, ushort> MistakeProofing
        {
            get
            {
                return null;
            }
            set
            {

            }
        }

        public bool IsEnabled => true;

        public bool HadReturnedToHomePosition => true;

        public bool IsStationary { get; private set; }

        public bool IsBusy { get; private set; }

        public bool AlarmOccurred { get; private set; }

        public ushort ErrorCode => AlarmOccurred ? (ushort)CylinderError.PositiveAirPressureAbnormal : (ushort)0;

        public Action<SupervisoryController, INamedObject, CompletionResult> Completed
        {
            get { return _completion.Completed; }
            set
            {
                if (value != null)
                    throw new ArgumentException("Setting for null to be cancelled only.");

                _completion.Completed = null;
            }
        }

        public NonSensorDualPositionSolenoidValve(SupervisoryController controller, string name, BitCell position1Output, BitCell position2Output, string acting1Name, string acting2Name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            if (string.IsNullOrEmpty(acting1Name))
                throw new ArgumentException("String cannot be empty or null.", "acting1Name");

            if (string.IsNullOrEmpty(acting2Name))
                throw new ArgumentException("String cannot be empty or null.", "acting2Name");

            if (acting1Name == acting2Name)
                throw new ArgumentException("Acting names cannot be the same.");

            using (var reader = new StreamReader(Path.Combine(controller.ProfileRootFolder, $"{name}.xml"), Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(CylinderProfile));

                _profile = (CylinderProfile)serializer.Deserialize(reader);
                if (_profile.Name != name)
                    throw new FormatException($"The 'Name' attribute must be equal to {name}");

                _profile.CheckAfterDeserialization();
            }

            Name = name;

            _completion = new Completion(this);

            _position1Output = position1Output;
            _position2Output = position2Output;
            _acting1Name = acting1Name;
            _acting2Name = acting2Name;

            _goSignal.Output(High);
        }

        public void OnReboot(SupervisoryController controller)
        {
            _goSignal.Reset();

            _targetPosition = Unknown;
            _currentPosition = Unknown;

            if (controller.EquipmentState.HasPositiveAirPressureAbnormal)
            {
                _isReady = true;

                _positioningTimer.Stop();

                if (IsBusy)
                {
                    IsStationary = true;
                    IsBusy = false;
                    Alarm();
                }
            }
            else
            {
                _isReady = false;

                IsStationary = false;
                IsBusy = false;
                AlarmOccurred = false;
            }

            _completion.Complete(controller, CompletionResult.Reboot);
        }

        public void OnEmergencyStop(SupervisoryController controller)
        {

        }

        public void OnPositiveAirPressureSourceChanged(SupervisoryController controller, bool abnormal)
        {
            if (abnormal)
            {
                if (IsBusy)
                    return;

                IsStationary = true;
                Alarm();
            }
        }

        public void Poll(SupervisoryController controller)
        {
            if (!_isReady)
            {
                if (_goSignal.Level == High)
                {
                    IORegister register = controller.IORegister;

                    register.SetCoil(_position1Output, false);
                    register.SetCoil(_position2Output, false);

                    _goSignal.Output(Low);
                    return;
                }

                if (!_goSignal.Tick())
                    return;

                if (!_positioningTimer.IsEnabled)
                {
                    int timeout = Math.Max(_profile.Configuration.Position1Timeout, _profile.Configuration.Position2Timeout);

                    _positioningTimer.Start(controller.GetExpirationJiffies(timeout));
                    return;
                }

                if (!_positioningTimer.CheckTimeout(controller.Jiffies))
                    return;

                _positioningTimer.Stop();
                IsStationary = true;
                _isReady = true;
                return;
            }

            _goSignal.Tick();

            if (IsBusy)
                CheckProgress(controller);
        }

        private void CheckProgress(SupervisoryController controller)
        {
            if (_currentPosition == _targetPosition | AlarmOccurred)
            {
                CompletionResult result = _currentPosition == _targetPosition ? CompletionResult.Success : CompletionResult.AlarmOccurred;

                _targetPosition = Unknown;
                IsStationary = true;
                IsBusy = false;
                _completion.Complete(controller, result);
                return;
            }

            IsStationary = false;

            if (_goSignal.Level == Low)
            {
                if (_goSignal.IsStable(Low))
                {
                    controller.IORegister.SetCoil(_targetPosition == Position2 ? _position2Output : _position1Output, true);
                    _goSignal.Output(High);
                }
                return;
            }

            if (_positioningTimer.IsEnabled)
            {
                if (_positioningTimer.CheckTimeout(controller.Jiffies))
                {
                    controller.IORegister.SetCoil(_targetPosition == Position2 ? _position2Output : _position1Output, false);
                    _goSignal.Output(Low);
                    _positioningTimer.Stop();

                    _currentPosition = _targetPosition;
                    _targetPosition = Unknown;

                    IsStationary = true;
                    IsBusy = false;

                    CompletionResult result = CompletionResult.Success;

                    if (controller.EquipmentState.HasPositiveAirPressureAbnormal)
                    {
                        result = CompletionResult.AlarmOccurred;
                        Alarm();
                    }

                    _completion.Complete(controller, result);
                }
            }
            else if (_goSignal.IsStable(High))
            {
                int timeout = _targetPosition == Position2 ? _profile.Configuration.Position2Timeout : _profile.Configuration.Position1Timeout;

                _positioningTimer.Start(controller.GetExpirationJiffies(timeout));
            }
        }

        private void Alarm()
        {
            _currentPosition = Unknown;
            AlarmOccurred = true;
        }

        public void ReturnToHomePosition(SupervisoryController controller)
        {

        }

        public RejectionReason CanExecuteGo(SupervisoryController controller)
        {
            return !IsBusy ? RejectionReason.Allowed : RejectionReason.Busy;
        }

        public void Go(SupervisoryController controller, string position, Action<SupervisoryController, INamedObject, CompletionResult> completed = null)
        {
            if (!TryGetPosition(position, out byte targetPosition))
                throw new NotFoundException($"'{position}' position does not exist.");

            RejectionReason rejection = CanExecuteGo(controller);

            if (rejection != RejectionReason.Allowed)
                throw new RejectionException(rejection);

            _targetPosition = targetPosition;
            IsBusy = true;

            if (completed != null)
                _completion.Completed = completed;

            controller.EquipmentState.KeepActive();

            if (_currentPosition == targetPosition | AlarmOccurred)
                return;

            if (_goSignal.IsStable(Low))
            {
                controller.IORegister.SetCoil(targetPosition == Position2 ? _position2Output : _position1Output, true);

                _goSignal.Output(High);
            }
        }

        public void Reset(SupervisoryController controller)
        {
            if (IsBusy | !AlarmOccurred | controller.EquipmentState.HasPositiveAirPressureAbnormal)
                return;

            AlarmOccurred = false;
        }

        public void OnIncomingMessage(SupervisoryController controller, ManualControlMessage msg)
        {
            CylinderManualControlMessage message = msg as CylinderManualControlMessage;

            if (message == null)
            {
                message.Result = ManualControlResult.InvalidArgument;
                message.Complete(false);
                return;
            }

            var command = message.Command;

            if (command == CylinderManualControlCommand.Go)
            {
                var equipmentState = controller.EquipmentState;

                if (!equipmentState.AreAllSafetyDoorsLocked)
                {
                    message.Result = ManualControlResult.SafetyDoorNotLocked;
                    message.Complete(false);
                    return;
                }

                string positionName = message.Position;

                if (!TryGetPosition(positionName, out byte targetPosition))
                {
                    message.Result = ManualControlResult.InvalidArgument;
                    message.Complete(false);
                    return;
                }

                try
                {
                    Go(controller, positionName);
                    _completion.Completed = message.OnCompleted;
                    equipmentState.CanUnlockSafetyDoor = false;
                }
                catch (RejectionException e)
                {
                    message.Result = e.Reason.ToManualControlResult();
                    message.Complete(false);
                }

                return;
            }

            switch (message.Command)
            {
                case CylinderManualControlCommand.InformIfAvailable:
                    message.Result = ManualControlResult.CommandNotSupported;
                    message.Complete(false);
                    return;
                case CylinderManualControlCommand.QueryConfiguration:
                    _profile.Configuration.CopyTo(message.Configuration);
                    break;
                case CylinderManualControlCommand.UpdateConfiguration:
                    message.Configuration.CopyTo(_profile.Configuration);

                    XmlSerializer serializer = new XmlSerializer(typeof(CylinderProfile));

                    using (var fs = new FileStream(Path.Combine(controller.ProfileRootFolder, $"{Name}.xml"), FileMode.Create))
                    {
                        using (var writer = new XmlTextWriter(fs, Encoding.UTF8))
                        {
                            writer.Formatting = Formatting.Indented;
                            serializer.Serialize(writer, _profile);
                        }
                    }
                    break;
            }

            message.Result = ManualControlResult.Success;
            message.Complete();
        }

        public bool IsOn(string position)
        {
            return true;
        }

        public bool IsOnUnexpectedPosition()
        {
            return false;
        }

        private bool TryGetPosition(string name, out byte position)
        {
            return DualPositionSolenoidValveUtils.TryGetPosition(name, _acting1Name, _acting2Name, out position);
        }

        public bool CheckIfReadyToAutoRun(SupervisoryController controller)
        {
            return !(IsBusy | AlarmOccurred);
        }

        public AlarmReport GetAlarmReport(SupervisoryController controller)
        {
            if (!AlarmOccurred)
                throw new InvalidOperationException("'GetAlarmReport' method only can be called when alarming.");

            return controller.CreateAlarmReport(Name, CylinderError.PositiveAirPressureAbnormal.ToString());
        }
    }
}
