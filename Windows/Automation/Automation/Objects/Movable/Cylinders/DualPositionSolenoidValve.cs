﻿using Automation.Core;
using Automation.Core.Alarm;
using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Core.Messages.ManualControl;
using Automation.Core.Messages.ManualControl.Movable;
using Automation.Core.Registers;
using Automation.Enums;
using Automation.Exceptions;
using Automation.Helper;
using Automation.Profiles.Cylinder;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Automation.Objects.Movable.Cylinders
{
    public class DualPositionSolenoidValve : IMovableObject
    {
        private const byte Low = 0;
        private const byte High = 1;

        private const byte Unknown = 0;
        private const byte Position1 = 1;
        private const byte Position2 = 2;

        private const ushort DoubleCoilHighHoldTime = 1000;

        private readonly BitCell _position1Input;
        private readonly BitCell _position2Input;

        // single coil
        private readonly BitCell _positionOutput;
        // double coil
        private readonly BitCell _position1Output;
        private readonly BitCell _position2Output;

        private readonly string _acting1Name;
        private readonly string _acting2Name;

        private readonly bool _singleCoil;

        private readonly CylinderProfile _profile;

        private readonly Completion _completion;

        private bool _isReady;

        private Timer _bootTimer = new Timer();
        private Timer _positioningTimer = new Timer();
        private Timer _positionDoneHoldTimer = new Timer();
        private Timer _goSignalTimer = new Timer();

        private Signal _goSignal = new Signal();
        private bool _goSignalFallingEdgeTriggered;

        private bool _needsConfirmCurrentPosition = true;

        private byte _targetPosition;
        private byte _currentPosition;

        public MovableObjectType Type { get { return MovableObjectType.Cylinder; } }

        public string Name { get; }

        public Action<SupervisoryController> StopCommand { get { return null; } }

        public Func<SupervisoryController, string, ushort> MistakeProofing { get; set; }

        public bool IsEnabled { get { return true; } }

        public bool HadReturnedToHomePosition { get { return true; } }

        public bool IsStationary { get; private set; }

        public bool IsBusy { get; private set; }

        public bool AlarmOccurred { get; private set; }

        public ushort ErrorCode { get; private set; }

        public Action<SupervisoryController, INamedObject, CompletionResult> Completed
        {
            get { return _completion.Completed; }
            set
            {
                if (value != null)
                    throw new ArgumentException("Setting for null to be cancelled only.");

                _completion.Completed = null;
            }
        }

        public DualPositionSolenoidValve(SupervisoryController controller,
            string name, BitCell position1Input, BitCell position2Input, BitCell positionOutput, string acting1Name, string acting2Name) :
            this(controller, name, position1Input, position2Input, positionOutput, positionOutput, acting1Name, acting2Name)
        {
            _positionOutput = positionOutput;
            _singleCoil = true;
        }

        public DualPositionSolenoidValve(SupervisoryController controller, string name, BitCell position1Input, BitCell position2Input, BitCell position1Output, BitCell position2Output,
            string acting1Name, string acting2Name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            if (string.IsNullOrEmpty(acting1Name))
                throw new ArgumentException("String cannot be empty or null.", "acting1Name");

            if (string.IsNullOrEmpty(acting2Name))
                throw new ArgumentException("String cannot be empty or null.", "acting2Name");

            if (acting1Name == acting2Name)
                throw new ArgumentException("Acting names cannot be the same.");

            using (var reader = new StreamReader(Path.Combine(controller.ProfileRootFolder, $"{name}.xml"), Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(CylinderProfile));

                _profile = (CylinderProfile)serializer.Deserialize(reader);
                if (_profile.Name != name)
                    throw new FormatException($"The 'Name' attribute must be equal to {name}");

                _profile.CheckAfterDeserialization();
            }

            Name = name;

            _completion = new Completion(this);

            _position1Input = position1Input;
            _position2Input = position2Input;
            _position1Output = position1Output;
            _position2Output = position2Output;
            _acting1Name = acting1Name;
            _acting2Name = acting2Name;

            _singleCoil = false;

            _goSignal.Output(High);
        }

        public void OnReboot(SupervisoryController controller)
        {
            _positioningTimer.Stop();
            _positionDoneHoldTimer.Stop();
            _goSignalTimer.Stop();
            _goSignal.Reset();
            _goSignalFallingEdgeTriggered = false;

            _targetPosition = Unknown;
            _currentPosition = Unknown;

            _isReady = true;

            if (controller.EquipmentState.HasPositiveAirPressureAbnormal)
            {
                if (IsBusy)
                {
                    IsStationary = true;
                    IsBusy = false;

                    if (!AlarmOccurred)
                        Alarm(CylinderError.PositiveAirPressureAbnormal);
                }
            }
            else
            {
                _needsConfirmCurrentPosition = true;

                IsStationary = false;
                IsBusy = false;
                AlarmOccurred = false;
                ErrorCode = 0;

                if (!_bootTimer.IsEnabled)
                    _bootTimer.Start(controller.GetExpirationJiffies(_profile.Configuration.PositioningTimeout));
            }

            _completion.Complete(controller, CompletionResult.Reboot);
        }

        public void OnEmergencyStop(SupervisoryController controller)
        {

        }

        public void OnPositiveAirPressureSourceChanged(SupervisoryController controller, bool abnormal)
        {
            if (abnormal)
            {
                if (IsBusy)
                    return;

                if (_needsConfirmCurrentPosition)
                    CancelPositionConfirmation();

                if (!AlarmOccurred)
                    Alarm(CylinderError.PositiveAirPressureAbnormal);
            }
        }

        public void Poll(SupervisoryController controller)
        {
            IORegister register = controller.IORegister;

            if (!_isReady)
            {
                if (_goSignal.Level == High)
                {
                    if (_singleCoil)
                        register.SetCoil(_positionOutput, false);
                    else
                    {
                        register.SetCoil(_position1Output, false);
                        register.SetCoil(_position2Output, false);
                    }

                    _goSignal.Output(Low);
                    return;
                }

                if (!_goSignal.Tick())
                    return;

                if (!_positioningTimer.IsEnabled)
                {
                    _positioningTimer.Start(controller.GetExpirationJiffies(_profile.Configuration.PositioningTimeout));
                    return;
                }

                if (!_positioningTimer.CheckTimeout(controller.Jiffies))
                    return;

                _positioningTimer.Stop();
                _isReady = true;
            }

            bool onPosition1 = register.GetContact(_position1Input);
            bool onPosition2 = register.GetContact(_position2Input);

            _goSignal.Tick();

            if (IsBusy)
            {
                CheckProgress(controller, onPosition1, onPosition2);
                return;
            }

            if (_needsConfirmCurrentPosition)
            {
                CheckCurrentPosition(controller, onPosition1, onPosition2);
                return;
            }

            if (AlarmOccurred)
                return;

            bool sensor1Error = (_currentPosition == Position1) ^ onPosition1;

            if (onPosition1 ^ onPosition2)
            {
                if (sensor1Error)
                    Alarm(CylinderError.PositionSensorError);
            }
            else
                Alarm(sensor1Error ? CylinderError.PositionSensor1Error : CylinderError.PositionSensor2Error);
        }

        private void CheckProgress(SupervisoryController controller, bool onPosition1, bool onPosition2)
        {
            if (AlarmOccurred)
            {
                _targetPosition = Unknown;
                IsStationary = true;
                IsBusy = false;
                _completion.Complete(controller, CompletionResult.AlarmOccurred);
                return;
            }

            uint jiffies = controller.Jiffies;

            if (_singleCoil)
            {
                if (!_positioningTimer.IsEnabled && _goSignal.IsStable())
                    _positioningTimer.Start(controller.GetExpirationJiffies(_profile.Configuration.PositioningTimeout));
            }
            else
            {
                if (_goSignal.Level == Low)
                {
                    if (!_goSignalFallingEdgeTriggered)
                    {
                        if (_goSignal.IsStable(Low))
                        {
                            controller.IORegister.SetCoil(_targetPosition == Position2 ? _position2Output : _position1Output, true);
                            _goSignal.Output(High);
                        }
                        IsStationary = false;
                        return;
                    }
                }
                else
                {
                    if (_goSignalTimer.IsEnabled)
                    {
                        if (_goSignalTimer.CheckTimeout(jiffies))
                        {
                            controller.IORegister.SetCoil(_targetPosition == Position2 ? _position2Output : _position1Output, false);
                            _goSignal.Output(Low);
                            _goSignalTimer.Stop();
                            _goSignalFallingEdgeTriggered = true;
                        }
                    }
                    else if (_goSignal.IsStable(High))
                    {
                        _goSignalTimer.Start(controller.GetExpirationJiffies(DoubleCoilHighHoldTime));
                        _positioningTimer.Start(controller.GetExpirationJiffies(_profile.Configuration.PositioningTimeout));
                    }
                }
            }

            bool inProgress = true;
            CompletionResult result = CompletionResult.Success;

            if (onPosition1 ^ onPosition2)
            {
                _currentPosition = onPosition1 ? Position1 : Position2;

                if (_currentPosition != _targetPosition)
                    _positionDoneHoldTimer.Stop();
                else
                {
                    if (!_positionDoneHoldTimer.IsEnabled)
                        _positionDoneHoldTimer.Start(controller.GetExpirationJiffies(GetPositionDoneHoldTime(_currentPosition)));
                    else if (_positionDoneHoldTimer.CheckTimeout(jiffies))
                        inProgress = false;
                }
            }
            else
                _positionDoneHoldTimer.Stop();

            if (_positioningTimer.CheckTimeout(jiffies))
            {
                if (inProgress)
                {
                    inProgress = false;
                    result = CompletionResult.AlarmOccurred;
                    Alarm(_targetPosition == Position2 ? CylinderError.Position2CanNotReach : CylinderError.Position1CanNotReach);
                }
            }

            IsStationary = !inProgress;

            if (IsStationary)
            {
                if (!_singleCoil)
                {
                    if (_goSignal.Level == High)
                    {
                        controller.IORegister.SetCoil(_targetPosition == Position2 ? _position2Output : _position1Output, false);
                        _goSignal.Output(Low);
                        _goSignalTimer.Stop();
                    }
                    _goSignalFallingEdgeTriggered = false;
                }

                _positioningTimer.Stop();
                _positionDoneHoldTimer.Stop();
                _targetPosition = Unknown;
                IsBusy = false;

                if (controller.EquipmentState.HasPositiveAirPressureAbnormal)
                {
                    result = CompletionResult.AlarmOccurred;
                    Alarm(CylinderError.PositiveAirPressureAbnormal);
                }

                _completion.Complete(controller, result);

                if (!AlarmOccurred)
                    RefreshPosition();
            }
        }

        private void CheckCurrentPosition(SupervisoryController controller, bool onPosition1, bool onPosition2)
        {
            uint jiffies = controller.Jiffies;

            if (_bootTimer.IsEnabled)
            {
                if (!_bootTimer.CheckTimeout(jiffies))
                    return;

                _bootTimer.Stop();
            }

            if (!_positioningTimer.IsEnabled)
                _positioningTimer.Start(controller.GetExpirationJiffies(_profile.Configuration.PositioningTimeout));

            bool inProgress = true;
            bool timerExpired = false;

            if (onPosition1 ^ onPosition2)
            {
                byte currentPosition = onPosition1 ? Position1 : Position2;

                if (_positionDoneHoldTimer.IsEnabled)
                {
                    if (currentPosition != _currentPosition)
                    {
                        _currentPosition = currentPosition;
                        _positionDoneHoldTimer.Restart(controller.GetExpirationJiffies(GetPositionDoneHoldTime(currentPosition)));
                    }
                    else if (_positionDoneHoldTimer.CheckTimeout(jiffies))
                        inProgress = false;
                }
                else
                {
                    _currentPosition = currentPosition;
                    _positionDoneHoldTimer.Start(controller.GetExpirationJiffies(GetPositionDoneHoldTime(currentPosition)));
                }
            }
            else
                _positionDoneHoldTimer.Stop();

            if (_positioningTimer.CheckTimeout(jiffies))
            {
                if (inProgress)
                {
                    inProgress = false;
                    timerExpired = true;
                }
            }

            if (!inProgress)
            {
                _needsConfirmCurrentPosition = false;

                _positioningTimer.Stop();
                _positionDoneHoldTimer.Stop();

                IsStationary = true;

                if (timerExpired)
                    Alarm(CylinderError.PositioningTimeout);
                else
                {
                    AlarmOccurred = false;
                    ErrorCode = 0;
                    RefreshPosition();
                }
            }
        }

        private ushort GetPositionDoneHoldTime(byte position)
        {
            return position == Position2 ? _profile.Configuration.Position2DoneHoldTime : _profile.Configuration.Position1DoneHoldTime;
        }

        private void Alarm(CylinderError error)
        {
            _currentPosition = Unknown;
            AlarmOccurred = true;
            ErrorCode = (ushort)error;
        }

        private void RefreshPosition()
        {
#if DEBUG
            if (_currentPosition == Unknown)
                throw new BugException("Should not go in here.");
#endif
            OnPositionRefreshed(_currentPosition == Position2 ? _acting2Name : _acting1Name);
        }

        protected virtual void OnPositionRefreshed(string position)
        {

        }

        public void ReturnToHomePosition(SupervisoryController controller)
        {

        }

        public RejectionReason CanExecuteGo(SupervisoryController controller)
        {
            return (!IsBusy | _singleCoil) ? RejectionReason.Allowed : RejectionReason.Busy;
        }

        public void Go(SupervisoryController controller, string position, Action<SupervisoryController, INamedObject, CompletionResult> completed = null)
        {
            if (!TryGetPosition(position, out byte targetPosition))
                throw new NotFoundException($"'{position}' position does not exist.");

            RejectionReason rejection = CanExecuteGo(controller);

            if (rejection != RejectionReason.Allowed)
                throw new RejectionException(rejection);

            if (_needsConfirmCurrentPosition)
                CancelPositionConfirmation();

            _targetPosition = targetPosition;
            IsBusy = true;

            if (completed != null)
                _completion.Completed = completed;

            controller.EquipmentState.KeepActive();

            if (AlarmOccurred)
                return;

            IORegister register = controller.IORegister;

            if (_singleCoil)
            {
                byte level = targetPosition == Position2 ? High : Low;

                if (level == _goSignal.Level)
                    return;

                register.SetCoil(_positionOutput, level == High);

                _goSignal.Output(level);

                _positioningTimer.Stop();
                _positionDoneHoldTimer.Stop();
            }
            else if (_goSignal.IsStable(Low))
            {
                register.SetCoil(_targetPosition == Position2 ? _position2Output : _position1Output, true);

                _goSignal.Output(High);
            }
        }

        public void Reset(SupervisoryController controller)
        {
            if (IsBusy | !AlarmOccurred | controller.EquipmentState.HasPositiveAirPressureAbnormal)
                return;

            _needsConfirmCurrentPosition = true;
        }

        private void CancelPositionConfirmation()
        {
#if DEBUG
            if (!_needsConfirmCurrentPosition)
                throw new BugException("Should not go in here.");
#endif
            _needsConfirmCurrentPosition = false;

            _currentPosition = Unknown;

            _bootTimer.Stop();
            _positioningTimer.Stop();
            _positionDoneHoldTimer.Stop();

            IsStationary = true;
        }

        public void OnIncomingMessage(SupervisoryController controller, ManualControlMessage msg)
        {
            CylinderManualControlMessage message = msg as CylinderManualControlMessage;

            if (message == null)
            {
                message.Result = ManualControlResult.InvalidArgument;
                message.Complete(false);
                return;
            }

            var command = message.Command;

            if (command == CylinderManualControlCommand.Go)
            {
                var equipmentState = controller.EquipmentState;

                if (!equipmentState.AreAllSafetyDoorsLocked)
                {
                    message.Result = ManualControlResult.SafetyDoorNotLocked;
                    message.Complete(false);
                    return;
                }

                string positionName = message.Position;

                if (!TryGetPosition(positionName, out byte targetPosition))
                {
                    message.Result = ManualControlResult.InvalidArgument;
                    message.Complete(false);
                    return;
                }

                ushort error = MistakeProofing?.Invoke(controller, positionName) ?? 0;

                if (error != 0)
                {
                    message.Result = ManualControlResult.Prohibited;
                    message.ProhibitionCode = error;
                    message.Complete(false);
                    return;
                }

                try
                {
                    Go(controller, positionName);
                    _completion.Completed = message.OnCompleted;
                    equipmentState.CanUnlockSafetyDoor = false;
                }
                catch (RejectionException e)
                {
                    message.Result = e.Reason.ToManualControlResult();
                    message.Complete(false);
                }

                return;
            }

            switch (message.Command)
            {
                case CylinderManualControlCommand.InformIfAvailable:
                    InformIfAvailable(message.Available);
                    break;
                case CylinderManualControlCommand.QueryConfiguration:
                    _profile.Configuration.CopyTo(message.Configuration);
                    break;
                case CylinderManualControlCommand.UpdateConfiguration:
                    message.Configuration.CopyTo(_profile.Configuration);

                    XmlSerializer serializer = new XmlSerializer(typeof(CylinderProfile));

                    using (var fs = new FileStream(Path.Combine(controller.ProfileRootFolder, $"{Name}.xml"), FileMode.Create))
                    {
                        using (var writer = new XmlTextWriter(fs, Encoding.UTF8))
                        {
                            writer.Formatting = Formatting.Indented;
                            serializer.Serialize(writer, _profile);
                        }
                    }
                    break;
            }

            message.Result = ManualControlResult.Success;
            message.Complete();
        }

        protected virtual void InformIfAvailable(bool available)
        {

        }

        public bool IsOn(string position)
        {
            if (!TryGetPosition(position, out byte expectedPosition))
                throw new NotFoundException($"'{position}' position does not exist.");

            if (IsOnUnexpectedPosition())
                return false;

            return _currentPosition == expectedPosition;
        }

        public bool IsOnUnexpectedPosition()
        {
            return IsBusy | _needsConfirmCurrentPosition | _currentPosition == Unknown;
        }

        private bool TryGetPosition(string name, out byte position)
        {
            return DualPositionSolenoidValveUtils.TryGetPosition(name, _acting1Name, _acting2Name, out position);
        }

        public bool CheckIfReadyToAutoRun(SupervisoryController controller)
        {
            return !(IsBusy | AlarmOccurred);
        }

        public AlarmReport GetAlarmReport(SupervisoryController controller)
        {
            if (!AlarmOccurred)
                throw new InvalidOperationException("'GetAlarmReport' method only can be called when alarming.");

            CylinderError error = (CylinderError)ErrorCode;

            return controller.CreateAlarmReport(Name, error.ToString());
        }
    }
}
