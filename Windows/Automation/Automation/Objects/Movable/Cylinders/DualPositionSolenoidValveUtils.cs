﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects.Movable.Cylinders
{
    public static class DualPositionSolenoidValveUtils
    {
        private const byte Unknown = 0;
        private const byte Position1 = 1;
        private const byte Position2 = 2;

        public static bool TryGetPosition(string name, string acting1Name, string acting2Name, out byte position)
        {
            if (name == null)
                throw new ArgumentNullException("name");

            if (name == acting1Name || name == "Standby" || name == "Home")
            {
                position = Position1;
                return true;
            }

            if (name == acting2Name)
            {
                position = Position2;
                return true;
            }

            position = Unknown;
            return false;
        }
    }
}
