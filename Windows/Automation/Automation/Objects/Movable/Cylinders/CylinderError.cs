﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects.Movable.Cylinders
{
    public enum CylinderError : byte
    {
        Success = 0,
        PositiveAirPressureAbnormal,
        PositioningTimeout,
        Position1CanNotReach,
        Position2CanNotReach,
        PositionSensorError,
        PositionSensor1Error,
        PositionSensor2Error
    }
}
