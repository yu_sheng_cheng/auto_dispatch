﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects.Movable.StepperMotors.SerialPort
{
    public enum SerialPortStepperMotorCommand : byte
    {
        JogForward,
        JogReverse,
        StopJog,
    }
}
