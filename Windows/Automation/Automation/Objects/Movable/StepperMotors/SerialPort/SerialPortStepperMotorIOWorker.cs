﻿using Automation.Core.Controller;
using Automation.Core.IOWorkers;
using Automation.Profiles;
using IO;
using SwissKnife;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automation.Objects.Movable.StepperMotors.SerialPort
{
    public abstract class SerialPortStepperMotorIOWorker : IOWorker
    {
        private readonly AsyncSerialPort _port;

        public SerialPortStepperMotorCommand Command { get; set; }

        public SerialPortStepperMotorIOWorker(string name, SupervisoryController controller) : base(name, controller, false)
        {
            SerialPortProfile profile;

            using (var reader = new StreamReader(Path.Combine(controller.ProfileRootFolder, $"{name}.xml"), Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(SerialPortProfile));

                profile = (SerialPortProfile)serializer.Deserialize(reader);
                if (profile.Name != name)
                    throw new FormatException($"The 'Name' attribute must be equal to {name}");

                profile.CheckAfterDeserialization();
            }

            _port = new AsyncSerialPort(profile.PortName, profile.BaudRate, profile.Parity, profile.DataBits);
            Timeout = profile.ReadTimeout;
        }

        public override void Dispose()
        {
            base.Dispose();

            _port.Dispose();
        }

        protected override async Task<OperationResult> Execute()
        {
            bool isOpen = false;

            try
            {
                _port.Open();
                isOpen = true;

                int timeout = Timeout;

                _port.AsyncTimeout = timeout > 0 ? timeout : 1000;

                await Execute(_port, Command).ConfigureAwait(false);

                return OperationResultFactory.CreateSuccessfulResult(SR.Success);
            }
            catch (Exception e)
            {
                Log.e(LogKeyword, "Execute:", e);

                int error = e is TimeoutException ? (int)AutomationError.Timeout : (int)AutomationError.Error;

                return OperationResultFactory.CreateFailedResult(GetType().FullName, error, e.Message);
            }
            finally
            {
                if (isOpen)
                    _port.Close();
            }
        }

        protected abstract Task Execute(AsyncSerialPort port, SerialPortStepperMotorCommand command);
    }
}
