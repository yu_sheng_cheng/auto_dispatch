﻿using Automation.Core;
using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Core.Registers;
using Automation.Enums;
using Automation.Exceptions;
using Automation.Helper;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects.Movable.StepperMotors.SerialPort
{
#if false
    public class SerialPortStepperMotor : IMovableObject
    {
        private const byte Low = 0;
        private const byte High = 1;

        private const ushort StepperMotorError = 1487;
        private const ushort CommunicationError = 9487;

        private readonly BitCell _alarmInput;
        private readonly BitCell _powerOnOutput;

        private readonly SerialPortStepperMotorIOWorker _ioWorker;
        private readonly Completion _completion;

        private Pulse _powerOffPulse = new Pulse();
        private Signal _signal = new Signal();
        private bool _pendingStopCommand;
        private bool _isStopped;
        private bool _firstPolling = true;

        public MovableObjectType Type { get { return MovableObjectType.StepperMotor; } }

        public string Name { get; }

        public Action<SupervisoryController> StopCommand { get { return null; } }

        public Func<SupervisoryController, string, ushort> MistakeProofing { get; set; }

        public bool IsEnabled { get; private set; } = true;

        public bool HadReturnedToHomePosition { get { return true; } }

        public bool IsStationary { get; private set; }

        public bool IsBusy { get; private set; }

        public bool AlarmOccurred { get; private set; }

        public ushort ErrorCode { get; private set; }

        public Action<INamedObject, CompletionResult> Completed
        {
            get { return _completion.Completed; }
            set
            {
                if (value != null)
                    throw new ArgumentException("Setting for null to be cancelled only.");

                _completion.Completed = null;
            }
        }

        public SerialPortStepperMotor(string name, SerialPortStepperMotorIOWorker ioWorker, BitCell alarmInput, BitCell powerOnOutput)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            Name = name;

            _alarmInput = alarmInput;
            _powerOnOutput = powerOnOutput;

            _ioWorker = ioWorker ?? throw new ArgumentNullException("ioWorker");
            _completion = new Completion(this);

            _ioWorker.Completed = _ =>
            {
                var result = _ioWorker.Result;

                if (!result.IsSuccess)
                {
                    Alarm(CommunicationError);
                    return;
                }

                if (ioWorker.Command == SerialPortStepperMotorCommand.StopJog)
                {
#if DEBUG
                    if (_pendingStopCommand)
                        throw new BugException("Should not go in here.");
#endif
                    _isStopped = true;
                    return;
                }
            };
        }

        public void OnReboot(SupervisoryController controller)
        {
            _firstPolling = false;

            _signal.Output(Low);
            _pendingStopCommand = false;
            _isStopped = true;

            IsEnabled = true;
            IsStationary = true;
            IsBusy = false;

            AlarmOccurred = controller.IORegister.GetContact(_alarmInput);
            ErrorCode = AlarmOccurred ? StepperMotorError : (ushort)0;

            _powerOffPulse.Disable();
            _powerOffPulse.Output(1, controller.GetExpirationJiffies(1000));

            _completion.Complete(CompletionResult.Reboot);
        }

        public void OnEmergencyStop(SupervisoryController controller)
        {
            if (!IsEnabled | !IsBusy)
                return;

            IsEnabled = false;

            if (_signal.Level != Low)
            {
                StopJog(controller);
                _signal.Output(Low);
            }
        }

        public void OnPositiveAirPressureSourceChanged(SupervisoryController controller, bool abnormal)
        {

        }

        public void Poll(SupervisoryController controller)
        {
            var register = controller.IORegister;

            if (_firstPolling)
            {
                _firstPolling = false;

                register.SetCoil(_powerOnOutput, false);

                _powerOffPulse.Output(controller.GetExpirationJiffies(500), controller.GetExpirationJiffies(3000));
            }

            if (_powerOffPulse.IsEnabled)
            {
                IsStationary = _isStopped;

                if (_powerOffPulse.Tick())
                {
                    register.SetCoil(_powerOnOutput, true);
                    return;
                }

                if (!_powerOffPulse.IsEnabled)
                {
                    _isStopped = true;
                    IsStationary = true;
                    AlarmOccurred = false;
                    ErrorCode = 0;
                }
                return;
            }

            if (!AlarmOccurred)
            {
                if (register.GetContact(_alarmInput))
                    Alarm(StepperMotorError);
            }

            CompletionResult result = CompletionResult.Success;
            bool completed = false;

            if (IsBusy)
            {
                if (_signal.Level == Low)
                {
                    if (_isStopped)
                    {
                        IsBusy = false;

                        completed = true;

                        if (AlarmOccurred | !IsEnabled)
                            result = AlarmOccurred ? CompletionResult.AlarmOccurred : CompletionResult.EmergencyStop;
                    }
                }
                else if (AlarmOccurred)
                    _signal.Output(Low);
            }
            else if (_pendingStopCommand && !_ioWorker.IsBusy)
            {
                StopJog(controller);
                _signal.Output(Low);
            }

            if (!(IsEnabled | IsBusy))
                IsEnabled = true;

            IsStationary = _isStopped;

            if (completed)
                _completion.Complete(result);
        }

        private void Alarm(ushort error)
        {
            if (!AlarmOccurred)
            {
                _pendingStopCommand = false;
                _isStopped = true;

                AlarmOccurred = true;
                ErrorCode = error;
            }
        }

        public void ReturnToHomePosition(SupervisoryController controller)
        {

        }

        public RejectionReason CanExecuteGo(SupervisoryController controller)
        {
            bool busy = _ioWorker.IsBusy;

            if (busy | _powerOffPulse.IsEnabled)
                return busy ? RejectionReason.Busy : RejectionReason.NotReady;

            return RejectionReason.Allowed;
        }

        public void Go(SupervisoryController controller, string direction, Action<INamedObject, CompletionResult> completed = null)
        {
            bool? forward = null;

            if (direction == "JOGF")
                forward = true;
            else if (direction == "JOGR")
                forward = false;

            if (!forward.HasValue)
            {
                if (direction == null)
                    throw new ArgumentNullException("direction");

                throw new NotFoundException($"Unknown direction '{direction}'.");
            }

            RejectionReason rejection = CanExecuteGo(controller);

            if (rejection != RejectionReason.Allowed)
                throw new RejectionException(rejection);

            controller.EquipmentState.KeepActive();

            SerialPortStepperMotorCommand command = (bool)forward ? SerialPortStepperMotorCommand.JogForward : SerialPortStepperMotorCommand.JogReverse;

            if (AlarmOccurred)
                command = SerialPortStepperMotorCommand.NotWorking;
            else
            {
                _pendingStopCommand = false;
                _isStoppingOrStopped = false;
            }

            _ioWorker.Command = command;
            controller.AddIORequest(_ioWorker);

            _isBusy = true;

            if (completed != null)
                Completed = completed;
        }

        public void Stop(SupervisoryController controller)
        {
            if (!IsBusy | _signal.Level == Low | _isStopped)
                return;

            StopJog(controller);
            _signal.Output(Low);
        }

        private void StopJog(SupervisoryController controller)
        {
            if (_ioWorker.IsBusy)
            {
                _pendingStopCommand = true;
                return;
            }

            _ioWorker.Command = SerialPortStepperMotorCommand.StopJog;
            controller.AddIORequest(_ioWorker);
        }

        public void Reset(SupervisoryController controller)
        {
            if (IsBusy | !AlarmOccurred | _powerOffPulse.IsEnabled)
                return;

            //if (ErrorCode == StepperMotorErrorCode)
            //{
            //    controller.IORegister.SetCoil(_resetOutput, true);
            //}

            //controller.IORegister.SetCoil(_resetOutput, true);

            //_resetPulse.Disable();
            //_resetPulse.Output(controller.GetExpirationJiffies(500));
        }
    }
#endif
}
