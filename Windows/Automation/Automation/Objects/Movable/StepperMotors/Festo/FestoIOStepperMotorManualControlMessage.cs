﻿using Automation.Core.Messages.ManualControl.Movable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects.Movable.StepperMotors.Festo
{
    public sealed class FestoIOStepperMotorManualControlMessage : MovableManualControlMessage
    {
        public string ScriptName { get; }

        public FestoIOStepperMotorManualControlMessage(string stepperMotorName, string scriptName) : base(stepperMotorName)
        {
            if (string.IsNullOrEmpty(scriptName))
                throw new ArgumentException("String cannot be empty or null.", "scriptName");

            ScriptName = scriptName;
        }
    }
}
