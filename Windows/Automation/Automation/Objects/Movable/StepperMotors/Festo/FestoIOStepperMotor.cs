﻿using Automation.Core;
using Automation.Core.Alarm;
using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Core.Messages.ManualControl;
using Automation.Core.Registers;
using Automation.Enums;
using Automation.Exceptions;
using Automation.Helper;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects.Movable.StepperMotors.Festo
{
    public abstract class FestoIOStepperMotor : IMovableObject
    {
        private const byte Low = 0;
        private const byte High = 1;

        private const short Idle = -1;
        private const short Homing = 0;

        private readonly BitCell _readyInput;
        private readonly BitCell _busyInput;
        private readonly BitCell _alarmInput;
        private readonly BitCell _pauseOutput;
        private readonly BitCell _controlEnableOutput;
        private readonly BitCell _resetOutput;
        private readonly BitCell _startOutput;

        private readonly Completion _completion;

        private Pulse _resetPulse = new Pulse();
        private Signal _commandSignal = new Signal();
        private Signal _startSignal = new Signal();
        private Timer _timer = new Timer();
        private short _pendingScriptId = Idle;
        private short _runningScriptId = Idle;
        private bool _firstPolling = true;

        public MovableObjectType Type { get { return MovableObjectType.StepperMotor; } }

        public string Name { get; }

        public Action<SupervisoryController> StopCommand { get { return null; } }

        public Func<SupervisoryController, string, ushort> MistakeProofing { get; set; }

        public bool IsEnabled { get { return true; } }

        public bool HadReturnedToHomePosition { get { return true; } }

        public bool IsStationary { get; private set; }

        public bool IsBusy { get { return _runningScriptId != Idle; } }

        public bool AlarmOccurred { get; private set; }

        public ushort ErrorCode { get { return AlarmOccurred ? (ushort)0x9487 : (ushort)0; } }

        public Action<SupervisoryController, INamedObject, CompletionResult> Completed
        {
            get { return _completion.Completed; }
            set
            {
                if (value != null)
                    throw new ArgumentException("Setting for null to be cancelled only.");

                _completion.Completed = null;
            }
        }

        public FestoIOStepperMotor(string name, BitCell readyInput, BitCell busyInput, BitCell alarmInput,
            BitCell pauseOutput, BitCell controlEnableOutput, BitCell resetOutput, BitCell startOutput)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            Name = name;

            _readyInput = readyInput;
            _busyInput = busyInput;
            _alarmInput = alarmInput;
            _pauseOutput = pauseOutput;
            _controlEnableOutput = controlEnableOutput;
            _resetOutput = resetOutput;
            _startOutput = startOutput;

            _completion = new Completion(this);
        }

        public void OnReboot(SupervisoryController controller)
        {
            _firstPolling = false;

            _resetPulse.Disable();
            _commandSignal.Reset();
            _startSignal.Reset();
            _timer.Stop();

            _pendingScriptId = Idle;
            _runningScriptId = Idle;

            IsStationary = true;

            var register = controller.IORegister;

            register.SetCoil(_pauseOutput, true);
            register.SetCoil(_controlEnableOutput, true);

            AlarmOccurred = register.GetContact(_alarmInput);

            _completion.Complete(controller, CompletionResult.Reboot);
        }

        public void OnEmergencyStop(SupervisoryController controller)
        {

        }

        public void OnPositiveAirPressureSourceChanged(SupervisoryController controller, bool abnormal)
        {

        }

        public void Poll(SupervisoryController controller)
        {
            IORegister register = controller.IORegister;

            AlarmOccurred = register.GetContact(_alarmInput);

            if (_firstPolling)
            {
                _firstPolling = false;

                register.SetCoil(_pauseOutput, true);

                if (register.GetCoil(_resetOutput))
                    _resetPulse.Output(controller.GetExpirationJiffies(50));
                else if (!AlarmOccurred)
                    register.SetCoil(_controlEnableOutput, true);

                if (register.GetCoil(_startOutput))
                {
                    _startSignal.Output(High);
                    _runningScriptId = CheckIfScriptIsRunning(controller);
                }
            }

            bool busy = register.GetContact(_busyInput);

            CompletionResult result = CompletionResult.Success;
            bool completed = false;

            if (_commandSignal.Level == High)
            {
                _commandSignal.Tick();

                if (!_timer.IsEnabled)
                {
                    if (_commandSignal.IsStable(High))
                        _timer.Start(controller.GetExpirationJiffies(10));
                }
                else if (_timer.CheckTimeout(controller.Jiffies))
                {
                    _timer.Stop();
                    _commandSignal.Reset();
#if DEBUG
                    if (_runningScriptId == Idle)
                        throw new BugException("Should not go in here.");
#endif
                    register.SetCoil(_startOutput, true);
                    _startSignal.Output(High);
                }
            }
            else if (_runningScriptId != Idle)
            {
                _startSignal.Tick();

                if (_startSignal.Level == Low)
                {
                    if (!_timer.IsEnabled)
                    {
                        if (_startSignal.IsStable(Low))
                            _timer.Start(controller.GetExpirationJiffies(100));
                    }
                    else if (_timer.CheckTimeout(controller.Jiffies))
                    {
                        _timer.Stop();
                        _runningScriptId = Idle;

                        completed = true;

                        if (AlarmOccurred)
                        {
                            result = CompletionResult.AlarmOccurred;
                            _pendingScriptId = Idle;
                        }
                        else if (_pendingScriptId != Idle)
                        {
                            completed = false;

                            _runningScriptId = _pendingScriptId;
                            _pendingScriptId = Idle;

                            Run(controller, _runningScriptId);
                            _commandSignal.Output(High);
                        }
                    }
                }
                else if (_startSignal.IsStable(High))
                {
                    if (!_timer.IsEnabled)
                        _timer.Start(controller.GetExpirationJiffies(50));
                    else if (_timer.CheckTimeout(controller.Jiffies))
                    {
                        if (_runningScriptId == Homing | !busy)
                        {
                            _timer.Stop();

                            Stop(controller);

                            register.SetCoil(_startOutput, false);
                            _startSignal.Output(Low);
                        }
                    }
                }
            }

            if (_resetPulse.IsEnabled)
            {
                if (_resetPulse.Tick())
                    register.SetCoil(_resetOutput, false);

                if (!_resetPulse.IsEnabled)
                    register.SetCoil(_controlEnableOutput, true);
            }

            IsStationary = !(IsBusy | busy);

            if (completed)
                _completion.Complete(controller, result);
        }

        public void ReturnToHomePosition(SupervisoryController controller)
        {

        }

        public RejectionReason CanExecuteGo(SupervisoryController controller)
        {
            if (IsBusy | _resetPulse.IsEnabled | !controller.IORegister.GetContact(_readyInput))
                return IsBusy ? RejectionReason.Busy : RejectionReason.NotReady;

            return RejectionReason.Allowed;
        }

        public void Go(SupervisoryController controller, string scriptName, Action<SupervisoryController, INamedObject, CompletionResult> completed = null)
        {
            if (string.IsNullOrEmpty(scriptName))
                throw new ArgumentException("String cannot be empty or null.", "scriptName");

            short id = GetScriptId(scriptName);

            if (id <= 0)
                throw new NotFoundException($"'{scriptName}' script does not exist.");

            Go(controller, id, completed);
        }

        private void Go(SupervisoryController controller, short id, Action<SupervisoryController, INamedObject, CompletionResult> completed = null)
        {
            RejectionReason rejection = CanExecuteGo(controller);

            if (rejection != RejectionReason.Allowed)
                throw new RejectionException(rejection);

            controller.EquipmentState.KeepActive();

            Run(controller, Homing);
            _commandSignal.Output(High);

            _pendingScriptId = id;
            _runningScriptId = Homing;

            if (completed != null)
                _completion.Completed = completed;
        }

        public void Reset(SupervisoryController controller)
        {
            if (IsBusy | !AlarmOccurred | _resetPulse.Level == High)
                return;

            var register = controller.IORegister;

            register.SetCoil(_resetOutput, true);
            register.SetCoil(_controlEnableOutput, false);

            _resetPulse.Disable();
            _resetPulse.Output(controller.GetExpirationJiffies(50));
        }

        protected abstract short CheckIfScriptIsRunning(SupervisoryController controller);

        protected abstract short GetScriptId(string name);

        protected abstract void Run(SupervisoryController controller, short id);

        protected abstract void Stop(SupervisoryController controller);

        public void OnIncomingMessage(SupervisoryController controller, ManualControlMessage msg)
        {
            FestoIOStepperMotorManualControlMessage message = msg as FestoIOStepperMotorManualControlMessage;

            if (message == null)
            {
                message.Result = ManualControlResult.InvalidArgument;
                message.Complete(false);
                return;
            }

            var equipmentState = controller.EquipmentState;

            if (!equipmentState.AreAllSafetyDoorsLocked)
            {
                message.Result = ManualControlResult.SafetyDoorNotLocked;
                message.Complete(false);
                return;
            }

            string scriptName = message.ScriptName;

            short id = GetScriptId(scriptName);

            if (id <= 0)
            {
                message.Result = ManualControlResult.InvalidArgument;
                message.Complete(false);
                return;
            }

            ushort error = MistakeProofing?.Invoke(controller, scriptName) ?? 0;

            if (error != 0)
            {
                message.Result = ManualControlResult.Prohibited;
                message.ProhibitionCode = error;
                message.Complete(false);
                return;
            }

            var rejection = CanExecuteGo(controller);

            if (rejection != RejectionReason.Allowed)
            {
                message.Result = rejection.ToManualControlResult();
                message.Complete(false);
                return;
            }

            Go(controller, id, message.OnCompleted);
            equipmentState.CanUnlockSafetyDoor = false;
        }

        public bool IsOn(string position)
        {
            return true;
        }

        public bool IsOnUnexpectedPosition()
        {
            return false;
        }

        public bool CheckIfReadyToAutoRun(SupervisoryController controller)
        {
            return !(IsBusy | AlarmOccurred | _resetPulse.IsEnabled | !controller.IORegister.GetContact(_readyInput));
        }

        public AlarmReport GetAlarmReport(SupervisoryController controller)
        {
            if (!AlarmOccurred)
                throw new InvalidOperationException("'GetAlarmReport' method only can be called when alarming.");

            return controller.CreateAlarmReport(Name, "Error");
        }
    }
}
