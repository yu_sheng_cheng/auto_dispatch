﻿using Automation.Core.Controller;
using Automation.Objects.Movable.StepperMotors.SerialPort;
using IO;
using SwissKnife;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects.Movable.StepperMotors.CSIM
{
    public class CSIMStepperMotorIOWorker : SerialPortStepperMotorIOWorker
    {
        public CSIMStepperMotorIOWorker(string name, SupervisoryController controller) : base(name, controller) { }

        protected override async Task Execute(AsyncSerialPort port, SerialPortStepperMotorCommand command)
        {
            string commandText;

            switch (command)
            {
                case SerialPortStepperMotorCommand.JogForward:
                    commandText = "JGF";
                    break;
                case SerialPortStepperMotorCommand.JogReverse:
                    commandText = "JGR";
                    break;
                case SerialPortStepperMotorCommand.StopJog:
                    commandText = "JG0";
                    break;
                default:
                    throw new NotImplementedException();
            }

            while (true)
            {
                int retryCount = 0;
                string line;

                while (true)
                {
                    try
                    {
                        port.WriteLine(commandText);
                        line = await port.ReadLineAsync().ConfigureAwait(false);
                        break;
                    }
                    catch (TimeoutException)
                    {
                        if (++retryCount == 3)
                            throw;
                    }
                }

                if (line != commandText)
                {
                    try
                    {
                        int timeout = port.AsyncTimeout;

                        port.AsyncTimeout = 2000;

                        while (true)
                        {
                            await port.ReadLineAsync().ConfigureAwait(false);
                        }
                    }
                    catch (TimeoutException)
                    {

                    }
                    port.AsyncTimeout = Timeout;
                    continue;
                }

                await port.ReadLineAsync().ConfigureAwait(false);
                break;
            }
        }
    }
}
