﻿using Automation.Core;
using Automation.Core.Alarm;
using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Core.Messages.ManualControl;
using Automation.Core.Messages.ManualControl.Movable;
using Automation.Core.Registers;
using Automation.Enums;
using Automation.Exceptions;
using Automation.Helper;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects.Movable.StepperMotors
{
    public abstract class IOStepperMotor : IMovableObject
    {
        private const byte Low = 0;
        private const byte High = 1;

        private const short Idle = -1;

        private readonly BitCell _busyInput;
        private readonly BitCell _alarmInput;
        private readonly BitCell _resetOutput;

        private readonly Completion _completion;

        private Pulse _resetPulse = new Pulse();
        private Signal _signal = new Signal();
        private Timer _timer = new Timer();
        private short _runningScriptId = Idle;
        private bool _isJog;
        private bool _firstPolling = true;

        public MovableObjectType Type { get { return MovableObjectType.StepperMotor; } }

        public string Name { get; }

        public Action<SupervisoryController> StopCommand { get; }

        public Func<SupervisoryController, string, ushort> MistakeProofing { get; set; }

        public bool IsEnabled { get; private set; } = true;

        public bool HadReturnedToHomePosition { get { return true; } }

        public bool IsStationary { get; private set; }

        public bool IsBusy { get { return _runningScriptId != Idle; } }

        public bool AlarmOccurred { get; private set; }

        public ushort ErrorCode { get { return AlarmOccurred ? (ushort)0x9487 : (ushort)0; } }

        public Action<SupervisoryController, INamedObject, CompletionResult> Completed
        {
            get { return _completion.Completed; }
            set
            {
                if (value != null)
                    throw new ArgumentException("Setting for null to be cancelled only.");

                _completion.Completed = null;
            }
        }

        public IOStepperMotor(string name, BitCell busyInput, BitCell alarmInput, BitCell resetOutput)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            Name = name;

            _busyInput = busyInput;
            _alarmInput = alarmInput;
            _resetOutput = resetOutput;

            _completion = new Completion(this);

            StopCommand = StopJog;
        }

        public void OnReboot(SupervisoryController controller)
        {
            _firstPolling = false;

            _resetPulse.Disable();
            _signal.Reset();
            _timer.Stop();

            _runningScriptId = Idle;

            IsEnabled = true;
            IsStationary = true;
            AlarmOccurred = controller.IORegister.GetContact(_alarmInput);

            _completion.Complete(controller, CompletionResult.Reboot);
        }

        public void OnEmergencyStop(SupervisoryController controller)
        {
            if (!IsEnabled | !IsBusy)
                return;

            IsEnabled = false;

            if (_isJog && _signal.Level != Low)
            {
                Stop(controller, _runningScriptId);
                _signal.Output(Low);
            }
        }

        public void OnPositiveAirPressureSourceChanged(SupervisoryController controller, bool abnormal)
        {

        }

        public void Poll(SupervisoryController controller)
        {
            IORegister register = controller.IORegister;

            if (_firstPolling)
            {
                _firstPolling = false;

                if (register.GetCoil(_resetOutput))
                    _resetPulse.Output(controller.GetExpirationJiffies(500));

                _runningScriptId = CheckIfScriptIsRunning(controller, out bool isJog);
                if (_runningScriptId != Idle)
                {
                    _signal.Output(High);
                    _isJog = isJog;
                }
            }

            AlarmOccurred = register.GetContact(_alarmInput);

            bool busy = register.GetContact(_busyInput);

            CompletionResult result = CompletionResult.Success;
            bool completed = false;

            if (_runningScriptId != Idle)
            {
                _signal.Tick();

                if (_signal.Level == Low)
                {
                    if (!_timer.IsEnabled)
                    {
                        if (_signal.IsStable(Low))
                            _timer.Start(controller.GetExpirationJiffies(100));
                    }
                    else if (_timer.CheckTimeout(controller.Jiffies))
                    {
                        _timer.Stop();

                        _runningScriptId = Idle;

                        if (AlarmOccurred | !IsEnabled)
                            result = AlarmOccurred ? CompletionResult.AlarmOccurred : CompletionResult.EmergencyStop;

                        completed = true;
                    }
                }
                else
                {
                    if (_isJog)
                    {
                        if (AlarmOccurred)
                        {
                            Stop(controller, _runningScriptId);
                            _signal.Output(Low);
                        }
                    }
                    else
                    {
                        if (_signal.IsStable(High) && !busy)
                        {
                            if (!_timer.IsEnabled)
                                _timer.Start(controller.GetExpirationJiffies(100));
                            else if (_timer.CheckTimeout(controller.Jiffies))
                            {
                                _timer.Stop();

                                Stop(controller, _runningScriptId);
                                _signal.Output(Low);
                            }
                        }
                    }
                }
            }

            if (!(IsEnabled | IsBusy))
                IsEnabled = true;

            if (_resetPulse.Tick())
                register.SetCoil(_resetOutput, false);

            IsStationary = !(IsBusy | busy);

            if (completed)
                _completion.Complete(controller, result);
        }

        public void ReturnToHomePosition(SupervisoryController controller)
        {

        }

        public RejectionReason CanExecuteGo(SupervisoryController controller)
        {
            if (IsBusy | _resetPulse.IsEnabled | !IsEnabled)
                return IsBusy ? RejectionReason.Busy : RejectionReason.NotReady;

            return RejectionReason.Allowed;
        }

        public void Go(SupervisoryController controller, string scriptName, Action<SupervisoryController, INamedObject, CompletionResult> completed = null)
        {
            if (string.IsNullOrEmpty(scriptName))
                throw new ArgumentException("String cannot be empty or null.", "scriptName");

            short id = GetScriptId(scriptName);

            if (id < 0)
                throw new NotFoundException($"'{scriptName}' script does not exist.");

            Go(controller, id, completed);
        }

        private void Go(SupervisoryController controller, short id, Action<SupervisoryController, INamedObject, CompletionResult> completed = null)
        {
            RejectionReason rejection = CanExecuteGo(controller);

            if (rejection != RejectionReason.Allowed)
                throw new RejectionException(rejection);

            controller.EquipmentState.KeepActive();

            _isJog = Run(controller, id);
            _signal.Output(High);

            _runningScriptId = id;

            if (completed != null)
                _completion.Completed = completed;
        }

        public void StopJog(SupervisoryController controller)
        {
            if (!IsBusy | !_isJog | _signal.Level == Low)
                return;

            Stop(controller, _runningScriptId);
            _signal.Output(Low);
        }

        public void Reset(SupervisoryController controller)
        {
            if (IsBusy | !AlarmOccurred | _resetPulse.Level == High)
                return;

            controller.IORegister.SetCoil(_resetOutput, true);

            _resetPulse.Disable();
            _resetPulse.Output(controller.GetExpirationJiffies(500));
        }

        public void OnIncomingMessage(SupervisoryController controller, ManualControlMessage msg)
        {
            IOStepperMotorManualControlMessage message = msg as IOStepperMotorManualControlMessage;

            if (message == null)
            {
                message.Result = ManualControlResult.InvalidArgument;
                message.Complete(false);
                return;
            }

            if (message.IsRun)
            {
                var equipmentState = controller.EquipmentState;

                if (!equipmentState.AreAllSafetyDoorsLocked)
                {
                    message.Result = ManualControlResult.SafetyDoorNotLocked;
                    message.Complete(false);
                    return;
                }

                string scriptName = message.ScriptName;

                short id = GetScriptId(scriptName);

                if (id < 0)
                {
                    message.Result = ManualControlResult.InvalidArgument;
                    message.Complete(false);
                    return;
                }

                ushort error = MistakeProofing?.Invoke(controller, scriptName) ?? 0;

                if (error != 0)
                {
                    message.Result = ManualControlResult.Prohibited;
                    message.ProhibitionCode = error;
                    message.Complete(false);
                    return;
                }

                var rejection = CanExecuteGo(controller);

                if (rejection != RejectionReason.Allowed)
                {
                    message.Result = rejection.ToManualControlResult();
                    message.Complete(false);
                    return;
                }

                Go(controller, id, message.OnCompleted);
                equipmentState.CanUnlockSafetyDoor = false;
            }
            else
            {
                StopJog(controller);

                message.Result = ManualControlResult.Success;
                message.Complete();
            }
        }

        protected abstract short CheckIfScriptIsRunning(SupervisoryController controller, out bool isJog);

        protected abstract short GetScriptId(string name);

        protected abstract bool Run(SupervisoryController controller, short id);

        protected abstract void Stop(SupervisoryController controller, short id);

        public bool IsOn(string position)
        {
            return true;
        }

        public bool IsOnUnexpectedPosition()
        {
            return false;
        }

        public bool CheckIfReadyToAutoRun(SupervisoryController controller)
        {
            return !(IsBusy | AlarmOccurred | _resetPulse.IsEnabled | !IsEnabled);
        }

        public AlarmReport GetAlarmReport(SupervisoryController controller)
        {
            if (!AlarmOccurred)
                throw new InvalidOperationException("'GetAlarmReport' method only can be called when alarming.");

            return controller.CreateAlarmReport(Name, "Error");
        }
    }
}
