﻿using Automation.Core;
using Automation.Core.Alarm;
using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Core.Messages.ManualControl;
using Automation.Core.Messages.ManualControl.Movable;
using Automation.Enums;
using Automation.Exceptions;
using Automation.Helper;
using Automation.Profiles.ServoDrive;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Automation.Objects.Movable.ServoDrive
{
    public class ServoDriveControl : IMovableObject
    {
        private const byte Low = 0;
        private const byte High = 1;

        private static readonly Regex PositionArrayRegex = new Regex(@"^(?<NAME>.+) \[(?<NUMBER>[0-9]+)\]$",
            RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);

        private readonly ServoDriveProfile _profile;
        private readonly Dictionary<int, IEnumerable<ServoDrivePosition>> _positioningValueDictionary = new Dictionary<int, IEnumerable<ServoDrivePosition>>();

        private readonly Completion _completion;

        private Pulse _resetPulse = new Pulse();
        private TickTimer _servoOnTimer = new TickTimer();
        private Signal _axisStopSignal = new Signal();
        private Signal _motionSignal = new Signal();
        private TickTimer _jogStopTimer = new TickTimer();
        private bool _isJogStarted;
        private ServoDriveCommand _motionCommand;
        private ServoDriveCommand _pendingMotionCommand;
        private bool _hadReturnedToHomePosition;
        private bool _firstPolling = true;

        public MovableObjectType Type { get { return MovableObjectType.ServoDrive; } }

        public string Name { get; }

        public byte Id { get; }

        public bool IsΘ { get { return _profile.IsΘ; } }

        public ServoDriveIOMemory IOMemory { get; } = new ServoDriveIOMemory();

        public Action<SupervisoryController> StopCommand { get; }

        public Func<SupervisoryController, string, ushort> MistakeProofing { get; set; }

        public bool IsServoOn { get; private set; } = true;

        public bool IsAxisEnabled { get; private set; } = true;

        public bool IsEnabled { get { return IsServoOn && IsAxisEnabled; } }

        public bool HadReturnedToHomePosition
        {
            get { return IsΘ | _hadReturnedToHomePosition; }
        }

        public bool IsStationary { get; private set; }

        public bool IsBusy
        {
            get
            {
                return _motionCommand != ServoDriveCommand.None | _pendingMotionCommand != ServoDriveCommand.None | _isJogStarted;
            }
        }

        public bool AlarmOccurred { get { return IOMemory.HasErrors; } }

        public ushort ErrorCode { get { return IOMemory.ErrorCode; } }

        public Action<SupervisoryController, INamedObject, CompletionResult> Completed
        {
            get { return _completion.Completed; }
            set
            {
                if (value != null)
                    throw new ArgumentException("Setting for null to be cancelled only.");

                _completion.Completed = null;
            }
        }

        public ServoDriveControl(SupervisoryController controller, string name, byte id)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            using (var reader = new StreamReader(Path.Combine(controller.ProfileRootFolder, $"{name}.xml"), Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(ServoDriveProfile));

                _profile = (ServoDriveProfile)serializer.Deserialize(reader);
                if (_profile.Name != name)
                    throw new FormatException($"The 'Name' attribute must be equal to {name}");

                _profile.CheckAfterDeserialization();
            }

            foreach (var position in _profile.Positions)
            {
                int positioningValue = position.Value;

                if (position.IsArray)
                {
                    int n = position.Horizontal ? position.Columns : position.Rows;

                    int interval = position.Interval;

                    if (interval == 0)
                        n = 1;

                    for (int i = 0; i < n; ++i)
                    {
                        AddPositioningValueToDictionary(positioningValue + i * interval, position);
                    }
                }
                else
                    AddPositioningValueToDictionary(positioningValue, position);
            }

            IOMemory.UpdateParameters(_profile.Parameters);

            Name = name;
            Id = id;
            StopCommand = StopJog;
            _completion = new Completion(this);
        }

        public void OnReboot(SupervisoryController controller)
        {
            _resetPulse.Disable();
            _servoOnTimer.Stop();
            _axisStopSignal.Reset();
            _motionSignal.Reset();
            _jogStopTimer.Stop();
            _isJogStarted = false;
            _motionCommand = ServoDriveCommand.None;
            _pendingMotionCommand = ServoDriveCommand.None;

            _hadReturnedToHomePosition = IOMemory.HadReturnedToHomePosition;
            _firstPolling = false;

            IsServoOn = true;
            IsAxisEnabled = true;
            IsStationary = true;

            _completion.Complete(controller, CompletionResult.Reboot);
        }

        public void OnEmergencyStop(SupervisoryController controller)
        {
            StopJog(controller);
            StopAxis(controller);
        }

        public void OnPositiveAirPressureSourceChanged(SupervisoryController controller, bool abnormal)
        {

        }

        public void Poll(SupervisoryController controller)
        {
            if (_firstPolling)
            {
                _firstPolling = false;

                ServoDriveCommand command = IOMemory.Command;

                if ((command & ServoDriveCommand.Reset) == ServoDriveCommand.Reset)
                    _resetPulse.Output(controller.GetExpirationJiffies(500));

                if ((command & ServoDriveCommand.ServoOff) == ServoDriveCommand.ServoOff)
                    IsServoOn = false;

                if ((command & ServoDriveCommand.AxisToStop) == ServoDriveCommand.AxisToStop)
                {
                    IsAxisEnabled = false;
                    _axisStopSignal.Output(High);
                }

                if ((command & (ServoDriveCommand.ReturnToHomePosition | ServoDriveCommand.Go)) != 0)
                {
                    _motionCommand = command;
                    _motionSignal.Output(High);
                }

                if ((command & (ServoDriveCommand.JogForward | ServoDriveCommand.JogReverse)) != 0)
                    _isJogStarted = true;
            }

            if (_servoOnTimer.Tick())
            {
                IsServoOn = true;
                _servoOnTimer.Stop();
            }

            if (_axisStopSignal.Tick(Low))
                IsAxisEnabled = true;

            CompletionResult result = CompletionResult.Success;
            bool completed = false;

            if (_isJogStarted)
            {
                if (_jogStopTimer.Tick())
                {
                    if (!IOMemory.IsBusy)
                    {
                        _isJogStarted = false;
                        _jogStopTimer.Stop();

                        if (AlarmOccurred | !IsAxisEnabled)
                            result = AlarmOccurred ? CompletionResult.AlarmOccurred : CompletionResult.EmergencyStop;

                        completed = true;
                    }
                }
                else if (AlarmOccurred)
                    StopJog(controller);
            }

            if (_motionCommand != ServoDriveCommand.None)
            {
                if (_motionSignal.Tick() && !IOMemory.IsBusy)
                {
                    bool inProgress = true;

                    if (_motionCommand == ServoDriveCommand.ReturnToHomePosition)
                    {
                        if (IsΘ | !IOMemory.IsReturningToHomePosition)
                        {
                            inProgress = false;

                            if (!(IsΘ | IOMemory.HadReturnedToHomePosition))
                                result = AlarmOccurred ? CompletionResult.AlarmOccurred : CompletionResult.EmergencyStop;
                        }
                    }
                    else
                    {
                        if (IOMemory.IsPositioningCompleted | AlarmOccurred | _axisStopSignal.IsStable(High))
                        {
                            inProgress = false;

                            if (!IOMemory.IsPositioningCompleted)
                                result = AlarmOccurred ? CompletionResult.AlarmOccurred : CompletionResult.EmergencyStop;
                        }
                    }

                    if (!inProgress)
                    {
                        IOMemory.Command &= ~_motionCommand;

                        _motionCommand = ServoDriveCommand.None;
                        _motionSignal.Output(Low);

                        completed = true;
                    }
                }
            }
            else
                _motionSignal.Tick();

            if (!IsAxisEnabled && !IsBusy)
            {
                if (_axisStopSignal.Level == High)
                {
                    IOMemory.Command &= ~ServoDriveCommand.AxisToStop;
                    _axisStopSignal.Output(Low);
                }
            }

            if (_resetPulse.Tick())
                IOMemory.Command &= ~ServoDriveCommand.Reset;

            IsStationary = !(IsBusy | IOMemory.IsBusy);

            _hadReturnedToHomePosition = _motionCommand != ServoDriveCommand.ReturnToHomePosition ? IOMemory.HadReturnedToHomePosition : false;

            if (completed)
                _completion.Complete(controller, result);

            if (_pendingMotionCommand == ServoDriveCommand.None | !_motionSignal.IsStable(Low) | _resetPulse.IsEnabled)
                return;

#if DEBUG
            if (_motionCommand != ServoDriveCommand.None)
                throw new BugException("Should not go in here.");
#endif

            ServoDriveCommand pendingCommand = _pendingMotionCommand;
            _pendingMotionCommand = ServoDriveCommand.None;

            switch (pendingCommand)
            {
                case ServoDriveCommand.Go:
                case ServoDriveCommand.ReturnToHomePosition:
                    StartMotion(controller, pendingCommand);
                    break;
                default:
                    throw new BugException("Should not go in here.");
            }
        }

        public void ReturnToHomePosition(SupervisoryController controller)
        {
            StartMotion(controller, ServoDriveCommand.ReturnToHomePosition);
        }

        public RejectionReason CanExecuteGo(SupervisoryController controller)
        {
            if (IsBusy | _resetPulse.IsEnabled | !IsEnabled)
                return IsBusy ? RejectionReason.Busy : RejectionReason.NotReady;

            return RejectionReason.Allowed;
        }

        public void Go(SupervisoryController controller, string position, Action<SupervisoryController, INamedObject, CompletionResult> completed = null)
        {
            if (position == "JOGF")
            {
                StartJog(controller, true, completed);
                return;
            }

            if (position == "JOGR")
            {
                StartJog(controller, false, completed);
                return;
            }

            if (!TryGetPositioningValue(position, out int positioningValue))
                throw new NotFoundException($"'{position}' position does not exist.");

            Go(controller, positioningValue, completed);
        }

        public void Go(SupervisoryController controller, int positioningValue, Action<SupervisoryController, INamedObject, CompletionResult> completed = null)
        {
            StartMotion(controller, ServoDriveCommand.Go, completed);

            IOMemory.PositioningValue = positioningValue;
        }

        public void StartMotion(SupervisoryController controller, ServoDriveCommand command, Action<SupervisoryController, INamedObject, CompletionResult> completed = null)
        {
            CheckIfCanMove();

            controller.EquipmentState.KeepActive();

            if (completed != null)
                _completion.Completed = completed;

            if (!_motionSignal.IsStable(Low))
            {
                _pendingMotionCommand = command;
                return;
            }

            _motionCommand = command;
            _motionSignal.Output(High);

            IOMemory.Command |= command;
        }

        public void StartJog(SupervisoryController controller, bool forward, Action<SupervisoryController, INamedObject, CompletionResult> completed = null)
        {
            CheckIfCanMove();

            controller.EquipmentState.KeepActive();

            _isJogStarted = true;

            IOMemory.Command |= forward ? ServoDriveCommand.JogForward : ServoDriveCommand.JogReverse;

            if (completed != null)
                _completion.Completed = completed;
        }

        public void StopJog(SupervisoryController controller)
        {
            if (!_isJogStarted | _jogStopTimer.IsEnabled)
                return;

            _jogStopTimer.Start();

            IOMemory.Command &= ~(ServoDriveCommand.JogForward | ServoDriveCommand.JogReverse);
        }

        private void CheckIfCanMove()
        {
            RejectionReason rejection = CanExecuteGo(null);

            if (rejection != RejectionReason.Allowed)
                throw new RejectionException(rejection);
        }

        public void Reset(SupervisoryController controller)
        {
            if (IsBusy | !AlarmOccurred | _resetPulse.Level == High)
                return;

            _resetPulse.Disable();
            _resetPulse.Output(controller.GetExpirationJiffies(500));

            IOMemory.Command |= ServoDriveCommand.Reset;
        }

        public void StopAxis(SupervisoryController controller)
        {
            if (!IsAxisEnabled | !IsBusy)
                return;

            IsAxisEnabled = false;

            _axisStopSignal.Output(High);

            IOMemory.Command |= ServoDriveCommand.AxisToStop;
        }

        public void TurnServoOn(SupervisoryController controller)
        {
            if (IsServoOn | _servoOnTimer.IsEnabled)
                return;

            _servoOnTimer.Start();

            IOMemory.Command &= ~ServoDriveCommand.ServoOff;
        }

        public void TurnServoOff(SupervisoryController controller)
        {
            if (IsBusy | (IOMemory.Command & ServoDriveCommand.ServoOff) == ServoDriveCommand.ServoOff)
                return;

            IsServoOn = false;
            _servoOnTimer.Stop();

            IOMemory.Command |= ServoDriveCommand.ServoOff;
        }

        public void OnIncomingMessage(SupervisoryController controller, ManualControlMessage msg)
        {
            ServoDriveManualControlMessage message = msg as ServoDriveManualControlMessage;

            if (message == null)
            {
                message.Result = ManualControlResult.InvalidArgument;
                message.Complete(false);
                return;
            }

            var command = message.Command;

            if (IsQueryOrUpdateManualControlCommand(command))
            {
                if (command == ServoDriveManualControlCommand.QueryParameters)
                    ReadParameters(message.Parameters);
                else
                {
                    try
                    {
                        CheckIfCanConfigure();

                        if (command == ServoDriveManualControlCommand.UpdateParameters)
                            UpdateParameters(controller, message.Parameters);
                        else
                        {
                            ServoPosition position = message.ServoPosition;

                            UpdatePosition(controller, position.Name, position.Value);
                        }
                    }
                    catch (RejectionException e)
                    {
                        message.Result = e.Reason.ToManualControlResult();
                        message.Complete(false);
                        return;
                    }
                }

                message.Result = ManualControlResult.Success;
                message.Complete();
                return;
            }

            var equipmentState = controller.EquipmentState;

            int positioningValue = IOMemory.PositioningValue;

            bool isMoveManualControl = IsMoveManualControlCommand(command);
            bool isStartJog = command == ServoDriveManualControlCommand.JogForward | command == ServoDriveManualControlCommand.JogReverse;

            if (isMoveManualControl)
            {
                if (!equipmentState.AreAllSafetyDoorsLocked)
                {
                    message.Result = ManualControlResult.SafetyDoorNotLocked;
                    message.Complete(false);
                    return;
                }

                if (!isStartJog)
                {
                    ushort error = 0;

                    if (command == ServoDriveManualControlCommand.ReturnToHomePosition)
                        error = MistakeProofing?.Invoke(controller, "Home?") ?? 0;
                    else // if (command == ServoDriveManualControlCommand.Go)
                    {
                        string position = message.Position;

                        if (string.IsNullOrEmpty(position))
                            positioningValue = message.PositioningValue;
                        else
                        {
                            if (!TryGetPositioningValue(position, out positioningValue))
                            {
                                message.Result = ManualControlResult.InvalidArgument;
                                message.Complete(false);
                                return;
                            }

                            error = MistakeProofing?.Invoke(controller, position) ?? 0;
                        }
                    }

                    if (error != 0)
                    {
                        message.Result = ManualControlResult.Prohibited;
                        message.ProhibitionCode = error;
                        message.Complete(false);
                        return;
                    }
                }
            }

            ManualControlResult result = CanExecuteServoDriveCommand(command);

            if (result != ManualControlResult.Success)
            {
                message.Result = result;
                message.Complete(false);
                return;
            }

            ExecuteServoDriveCommand(controller, command);

            if (isMoveManualControl)
            {
                equipmentState.CanUnlockSafetyDoor = false;

                if (isStartJog)
                    IOMemory.JogSpeed = message.JogSpeed;
                else
                {
                    IOMemory.PositioningValue = positioningValue;
                    _completion.Completed = message.OnCompleted;
                    return;
                }
            }

            message.Result = ManualControlResult.Success;
            message.Complete();
        }

        public static bool IsQueryOrUpdateManualControlCommand(ServoDriveManualControlCommand command)
        {
            return command == ServoDriveManualControlCommand.QueryParameters |
                command == ServoDriveManualControlCommand.UpdateParameters | command == ServoDriveManualControlCommand.UpdatePosition;
        }

        private bool IsMoveManualControlCommand(ServoDriveManualControlCommand command)
        {
            return command == ServoDriveManualControlCommand.JogForward | command == ServoDriveManualControlCommand.JogReverse |
                command == ServoDriveManualControlCommand.ReturnToHomePosition | command == ServoDriveManualControlCommand.Go;
        }

        public void CheckIfCanConfigure()
        {
            if (IsBusy)
                throw new RejectionException(RejectionReason.Busy);
        }

        public void ReadParameters(ServoDriveParameters parameters)
        {
            if (parameters == null)
                throw new ArgumentNullException("parameters");

            _profile.Parameters.CopyTo(parameters);
        }

        public void UpdateParameters(SupervisoryController controller, ServoDriveParameters parameters, bool includeJogSpeed = true)
        {
            if (parameters == null)
                throw new ArgumentNullException("parameters");

            int jogSpeed = _profile.Parameters.JogSpeed;

            IOMemory.UpdateParameters(parameters);
            parameters.CopyTo(_profile.Parameters);

            if (!includeJogSpeed)
            {
                IOMemory.JogSpeed = jogSpeed;
                _profile.Parameters.JogSpeed = jogSpeed;
            }

            SaveProfile(controller);
        }

        public void UpdatePosition(SupervisoryController controller, string name, int value)
        {
            if (!_profile.TryGetPosition(name, out ServoDrivePosition position))
                return;

            if (position.IsArray)
            {
                int n = position.Horizontal ? position.Columns : position.Rows;

                int interval = position.Interval;

                if (interval == 0)
                    n = 1;

                int positioningValue = position.Value;

                for (int i = 0; i < n; ++i)
                {
                    int d = i * interval;

                    RemovePositioningValueToDictionary(positioningValue + d, position);
                    AddPositioningValueToDictionary(value + d, position);
                }
            }
            else
            {
                RemovePositioningValueToDictionary(position.Value, position);
                AddPositioningValueToDictionary(value, position);
            }

            position.Value = value;

            SaveProfile(controller);
        }

        private void AddPositioningValueToDictionary(int positioningValue, ServoDrivePosition position)
        {
            if (_positioningValueDictionary.ContainsKey(positioningValue))
            {
                IEnumerable<ServoDrivePosition> positions = _positioningValueDictionary[positioningValue];

                if (!(positions is List<ServoDrivePosition> list))
                {
                    list = new List<ServoDrivePosition>();

                    foreach (var pos in positions)
                    {
                        list.Add(pos);
                    }

                    _positioningValueDictionary[positioningValue] = list;
                }

                list.Add(position);
            }
            else
                _positioningValueDictionary.Add(positioningValue, new ServoDrivePosition[] { position });
        }

        private void RemovePositioningValueToDictionary(int positioningValue, ServoDrivePosition position)
        {
            IEnumerable<ServoDrivePosition> positions = _positioningValueDictionary[positioningValue];

            if (positions is List<ServoDrivePosition> list)
            {
                list.Remove(position);

                if (list.Count != 0)
                    return;
            }

            _positioningValueDictionary.Remove(positioningValue);
        }

        private void SaveProfile(SupervisoryController controller)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ServoDriveProfile));

            using (var fs = new FileStream(Path.Combine(controller.ProfileRootFolder, $"{Name}.xml"), FileMode.Create))
            {
                using (var writer = new XmlTextWriter(fs, Encoding.UTF8))
                {
                    writer.Formatting = Formatting.Indented;

                    serializer.Serialize(writer, _profile);
                }
            }
        }

        public ManualControlResult CanExecuteServoDriveCommand(ServoDriveManualControlCommand command)
        {
            switch (command)
            {
                case ServoDriveManualControlCommand.ServoOn:
                case ServoDriveManualControlCommand.ServoOff:
                case ServoDriveManualControlCommand.AxisToStop:
                case ServoDriveManualControlCommand.StopJog:
                    return ManualControlResult.Success;
                case ServoDriveManualControlCommand.QueryParameters:
                case ServoDriveManualControlCommand.UpdateParameters:
                case ServoDriveManualControlCommand.UpdatePosition:
                    return ManualControlResult.InvalidArgument;
            }

            RejectionReason rejection = CanExecuteGo(null);

            return rejection == RejectionReason.Allowed ? ManualControlResult.Success : rejection.ToManualControlResult();
        }

        public void ExecuteServoDriveCommand(SupervisoryController controller, ServoDriveManualControlCommand command)
        {
            switch (command)
            {
                case ServoDriveManualControlCommand.ServoOn:
                    TurnServoOn(controller);
                    break;
                case ServoDriveManualControlCommand.ServoOff:
                    TurnServoOff(controller);
                    break;
                case ServoDriveManualControlCommand.AxisToStop:
                    StopAxis(controller);
                    break;
                case ServoDriveManualControlCommand.JogForward:
                    StartJog(controller, true);
                    break;
                case ServoDriveManualControlCommand.JogReverse:
                    StartJog(controller, false);
                    break;
                case ServoDriveManualControlCommand.StopJog:
                    StopJog(controller);
                    break;
                case ServoDriveManualControlCommand.ReturnToHomePosition:
                    StartMotion(controller, ServoDriveCommand.ReturnToHomePosition);
                    break;
                case ServoDriveManualControlCommand.Go:
                    StartMotion(controller, ServoDriveCommand.Go);
                    break;
                case ServoDriveManualControlCommand.QueryParameters:
                case ServoDriveManualControlCommand.UpdateParameters:
                case ServoDriveManualControlCommand.UpdatePosition:
                    throw new InvalidOperationException("Servo drive command does not include query and update.");
            }
        }

        public bool IsOn(string positionName)
        {
            if (IsΘ)
                return true;

            if (!TryGetPositioningValue(positionName, out int positioningValue))
            {
                if (!_profile.TryGetPosition(positionName, out ServoDrivePosition position))
                    throw new NotFoundException($"'{positionName}' position does not exist.");

                if (!position.IsArray)
                    throw new BugException("Should not go in here.");

                foreach (var name in MatchPositions())
                {
                    var match = PositionArrayRegex.Match(name);

                    if (match.Success)
                    {
                        if (positionName == match.Groups["NAME"].Value)
                            return true;
                    }
                }

                return false;
            }

            return IOMemory.CurrentFeedValue == positioningValue;
        }

        public bool IsOnUnexpectedPosition()
        {
            if (IsΘ)
                return false;

            return !_positioningValueDictionary.ContainsKey(IOMemory.CurrentFeedValue);
        }

        public bool TryGetCurrentPosition(out string name, out uint id)
        {
            foreach (var positionName in MatchPositions())
            {
                if (!TryGetPositionId(positionName, out id))
                    throw new BugException("Should not go in here.");

                name = positionName;
                return true;
            }

            name = string.Empty;
            id = 0;
            return false;
        }

        public bool TryGetPositionId(string name, out uint id)
        {
            if (name == "Home")
            {
                id = 1;
                return true;
            }

            id = 0;

            if (!_profile.TryGetPosition(name, out ServoDrivePosition position))
            {
                var match = PositionArrayRegex.Match(name);

                if (!match.Success)
                    return false;

                var groups = match.Groups;

                if (!_profile.TryGetPosition(groups["NAME"].Value, out position))
                    return false;

                if (!ushort.TryParse(groups["NUMBER"].Value, out ushort number))
                    return false;

                if (!position.IsValidArrayNumber(number))
                    return false;

                id = unchecked((uint)(position.Id | ((number - 1) << 16)));
                return true;
            }

            id = position.Id;
            return true;
        }

        public bool TryGetPositioningValue(string name, out int value)
        {
            ServoDrivePosition position;

            value = 0;

            var match = PositionArrayRegex.Match(name);

            if (match.Success)
            {
                var groups = match.Groups;

                if (!_profile.TryGetPosition(groups["NAME"].Value, out position))
                    return false;

                if (!position.IsArray)
                    return false;

                if (!ushort.TryParse(groups["NUMBER"].Value, out ushort number))
                    return false;

                if (!position.IsValidArrayNumber(number))
                    return false;

                int cols = position.Columns;

                int n = position.Horizontal ? (number - 1) % cols : (number - 1) / cols;

                value = position.Value + n * position.Interval;
                return true;
            }

            if (!_profile.TryGetPosition(name, out position))
                return name == "Home";

            if (position.IsArray)
                return false;

            value = position.Value;
            return true;
        }

        public IEnumerable<string> MatchPositions()
        {
            return MatchPositions(IOMemory.CurrentFeedValue);
        }

        public IEnumerable<string> MatchPositions(int positioningValue)
        {
            if (_positioningValueDictionary.ContainsKey(positioningValue))
            {
                foreach (var position in _positioningValueDictionary[positioningValue])
                {
                    if (position.IsArray)
                    {
                        string name = position.Name;

                        int interval = position.Interval;

                        int offset = interval != 0 ? (positioningValue - position.Value) / interval : 0;

                        int stride = position.Columns;

                        if (position.Horizontal)
                        {
                            int rows = position.Rows;
                            int index = offset + 1;

                            for (int row = 0; row < rows; ++row)
                            {
                                yield return $"{name} [{index.ToString()}]";
                                index += stride;
                            }
                        }
                        else
                        {
                            int cols = position.Columns;
                            int index = offset * stride + 1;

                            for (int col = 0; col < cols; ++col)
                            {
                                yield return $"{name} [{index.ToString()}]";
                                index += 1;
                            }
                        }
                    }
                    else
                        yield return position.Name;
                }
            }
        }

        public bool CheckIfReadyToAutoRun(SupervisoryController controller)
        {
            return !(IsBusy | AlarmOccurred | _resetPulse.IsEnabled | !HadReturnedToHomePosition | !IsEnabled);
        }

        public AlarmReport GetAlarmReport(SupervisoryController controller)
        {
            if (!AlarmOccurred)
                throw new InvalidOperationException("'GetAlarmReport' method only can be called when alarming.");

            AlarmReport report = controller.CreateAlarmReport(Name, "Error");

            report.Message = $"{report.Message} (0x{ErrorCode.ToString("X2")})";

            return report;
        }
    }
}
