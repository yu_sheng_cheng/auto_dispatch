﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects.Movable.ServoDrive
{
    [Flags]
    public enum ServoDriveCommand : ushort
    {
        None = 0,
        Reset = 1,
        ServoOff = 2,
        AxisToStop = 4,
        ReturnToHomePosition = 8,
        JogForward = 16,
        JogReverse = 32,
        Go = 64
    }
}
