﻿using Automation.Core;
using Automation.Core.Alarm;
using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Core.Messages.ManualControl;
using Automation.Core.Messages.ManualControl.Movable;
using Automation.Enums;
using Automation.Exceptions;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects.Movable.ServoDrive
{
    public class XYMotion : IMovableObject
    {
        private readonly Action<SupervisoryController, INamedObject, CompletionResult> _xAxisCompleted;
        private readonly Action<SupervisoryController, INamedObject, CompletionResult> _yAxisCompleted;

        private readonly Completion _completion;

        private bool _isMotionStarted;
        private bool _xAxisInProgress;
        private bool _yAxisInProgress;
        private CompletionResult _xAxisMotionResult;
        private CompletionResult _yAxisMotionResult;

        public MovableObjectType Type { get { return MovableObjectType.ServoDrive; } }

        public string Name { get; }

        public ServoDriveControl AxisX { get; }

        public ServoDriveControl AxisY { get; }

        public bool IsEnabled { get { return AxisX.IsEnabled && AxisY.IsEnabled; } }

        public bool HadReturnedToHomePosition { get { return AxisX.HadReturnedToHomePosition && AxisY.HadReturnedToHomePosition; } }

        public bool IsStationary { get { return AxisX.IsStationary && AxisY.IsStationary; } }

        public bool IsBusy { get { return AxisX.IsBusy | AxisY.IsBusy; } }

        public Action<SupervisoryController> StopCommand => throw new NotImplementedException();

        public Func<SupervisoryController, string, ushort> MistakeProofing { get; set; }

        public bool AlarmOccurred { get { return AxisX.AlarmOccurred | AxisY.AlarmOccurred; } }

        public ushort ErrorCode { get { return AlarmOccurred ? (ushort)0x9487 : (ushort)0; } }

        public Action<SupervisoryController, INamedObject, CompletionResult> Completed
        {
            get { return _completion.Completed; }
            set
            {
                if (value != null)
                    throw new ArgumentException("Setting for null to be cancelled only.");

                _completion.Completed = null;
            }
        }

        public XYMotion(string name, ServoDriveControl xAxis, ServoDriveControl yAxis)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            Name = name;

            AxisX = xAxis ?? throw new ArgumentNullException("xAxis");
            AxisY = yAxis ?? throw new ArgumentNullException("yAxis");

            _completion = new Completion(this);

            _xAxisCompleted = ((controller, axis, result) =>
            {
                _xAxisInProgress = false;
                _xAxisMotionResult = result;

                if (!_yAxisInProgress)
                    Complete(controller);
            });

            _yAxisCompleted = ((controller, axis, result) =>
            {
                _yAxisInProgress = false;
                _yAxisMotionResult = result;

                if (!_xAxisInProgress)
                    Complete(controller);
            });
        }

        public void OnReboot(SupervisoryController controller)
        {
            throw new NotImplementedException();
        }

        public void OnEmergencyStop(SupervisoryController controller)
        {
            throw new NotImplementedException();
        }

        public void OnPositiveAirPressureSourceChanged(SupervisoryController controller, bool abnormal)
        {
            throw new NotImplementedException();
        }

        public void Poll(SupervisoryController controller)
        {
            throw new NotImplementedException();
        }

        public void ReturnToHomePosition(SupervisoryController controller)
        {
            StartMotion(controller, ServoDriveCommand.ReturnToHomePosition);
        }

        public RejectionReason CanExecuteGo(SupervisoryController controller)
        {
            RejectionReason reason = AxisX.CanExecuteGo(controller);

            if (reason != RejectionReason.Allowed)
                return reason;

            return AxisY.CanExecuteGo(controller);
        }

        public void Go(SupervisoryController controller, string position, Action<SupervisoryController, INamedObject, CompletionResult> completed = null)
        {
            if (!AxisX.TryGetPositioningValue(position, out int x))
                throw new NotFoundException($"X axis '{position}' position does not exist.");

            if (!AxisY.TryGetPositioningValue(position, out int y))
                throw new NotFoundException($"Y axis '{position}' position does not exist.");

            Go(controller, new Point(x, y), completed);
        }

        public void Go(SupervisoryController controller, Point point, Action<SupervisoryController, INamedObject, CompletionResult> completed = null)
        {
            StartMotion(controller, ServoDriveCommand.Go);

            AxisX.IOMemory.PositioningValue = point.X;
            AxisY.IOMemory.PositioningValue = point.Y;

            if (completed != null)
                _completion.Completed = completed;
        }

        private void StartMotion(SupervisoryController controller, ServoDriveCommand command)
        {
            if (_isMotionStarted)
                throw new RejectionException(RejectionReason.Busy, "Busy");

            AxisX.StartMotion(controller, command, _xAxisCompleted);
            AxisY.StartMotion(controller, command, _yAxisCompleted);

            _xAxisInProgress = true;
            _yAxisInProgress = true;
            _isMotionStarted = true;
        }

        private void Complete(SupervisoryController controller)
        {
            _isMotionStarted = false;

            CompletionResult result = CompletionResult.Success;

            if (_xAxisMotionResult != CompletionResult.Success | _yAxisMotionResult != CompletionResult.Success)
            {
                if (_xAxisMotionResult == CompletionResult.Reboot | _yAxisMotionResult == CompletionResult.Reboot)
                    result = CompletionResult.Reboot;
                else
                {
                    bool alarmOccurred = _xAxisMotionResult == CompletionResult.AlarmOccurred | _yAxisMotionResult == CompletionResult.AlarmOccurred;

                    result = alarmOccurred ? CompletionResult.AlarmOccurred : CompletionResult.EmergencyStop;
                }
            }

            _completion.Complete(controller, result);
        }

        public void Reset(SupervisoryController controller)
        {
            AxisX.Reset(controller);
            AxisY.Reset(controller);
        }

        public void Stop(SupervisoryController controller)
        {
            AxisX.StopAxis(controller);
            AxisY.StopAxis(controller);
        }

        public void TurnServoOn(SupervisoryController controller)
        {
            AxisX.TurnServoOn(controller);
            AxisY.TurnServoOn(controller);
        }

        public void TurnServoOff(SupervisoryController controller)
        {
            AxisX.TurnServoOff(controller);
            AxisY.TurnServoOff(controller);
        }

        public void OnIncomingMessage(SupervisoryController controller, ManualControlMessage msg)
        {
            ServoDriveManualControlMessage message = msg as ServoDriveManualControlMessage;

            if (message == null)
            {
                message.Result = ManualControlResult.InvalidArgument;
                message.Complete(false);
                return;
            }

            var command = message.Command;

            if (ServoDriveControl.IsQueryOrUpdateManualControlCommand(command))
            {
                if (command == ServoDriveManualControlCommand.QueryParameters)
                    AxisX.ReadParameters(message.Parameters);
                else
                {
                    try
                    {
                        AxisX.CheckIfCanConfigure();
                        AxisY.CheckIfCanConfigure();

                        if (command == ServoDriveManualControlCommand.UpdateParameters)
                        {
                            var parameters = message.Parameters;

                            AxisX.UpdateParameters(controller, parameters, false);
                            AxisY.UpdateParameters(controller, parameters, false);
                        }
                        else
                        {
                            ServoPoint point = message.ServoPoint;

                            string name = point.Name;

                            AxisX.UpdatePosition(controller, name, point.X);
                            AxisY.UpdatePosition(controller, name, point.Y);
                        }
                    }
                    catch (RejectionException e)
                    {
                        message.Result = e.Reason.ToManualControlResult();
                        message.Complete(false);
                        return;
                    }
                }

                message.Result = ManualControlResult.Success;
                message.Complete();
                return;
            }

            if (IsJogManualControlCommand(command))
            {
                message.Result = ManualControlResult.CommandNotSupported;
                message.Complete(false);
                return;
            }

            ManualControlResult result = AxisX.CanExecuteServoDriveCommand(command);
            if (result != ManualControlResult.Success)
            {
                message.Result = result;
                message.Complete(false);
                return;
            }

            result = AxisY.CanExecuteServoDriveCommand(command);
            if (result != ManualControlResult.Success)
            {
                message.Result = result;
                message.Complete(false);
                return;
            }

            if (command == ServoDriveManualControlCommand.ReturnToHomePosition | command == ServoDriveManualControlCommand.Go)
            {
                if (_isMotionStarted)
                    throw new BugException("Should not go in here.");

                var equipmentState = controller.EquipmentState;

                if (!equipmentState.AreAllSafetyDoorsLocked)
                {
                    message.Result = ManualControlResult.SafetyDoorNotLocked;
                    message.Complete(false);
                    return;
                }

                if (command == ServoDriveManualControlCommand.ReturnToHomePosition)
                {
                    ushort error = MistakeProofing?.Invoke(controller, "Home?") ?? 0;

                    if (error != 0)
                    {
                        message.Result = ManualControlResult.Prohibited;
                        message.ProhibitionCode = error;
                        message.Complete(false);
                        return;
                    }

                    ReturnToHomePosition(controller);
                }
                else
                {
                    string position = message.Position;
                    Point point;

                    if (string.IsNullOrEmpty(position))
                        point = message.Point;
                    else
                    {
                        if (!AxisX.TryGetPositioningValue(position, out int x) || !AxisY.TryGetPositioningValue(position, out int y))
                        {
                            message.Result = ManualControlResult.InvalidArgument;
                            message.Complete(false);
                            return;
                        }

                        ushort error = MistakeProofing?.Invoke(controller, position) ?? 0;

                        if (error != 0)
                        {
                            message.Result = ManualControlResult.Prohibited;
                            message.ProhibitionCode = error;
                            message.Complete(false);
                            return;
                        }

                        point = new Point(x, y);
                    }

                    Go(controller, point);
                }

                _completion.Completed = message.OnCompleted;
                equipmentState.CanUnlockSafetyDoor = false;
            }
            else
            {
                AxisX.ExecuteServoDriveCommand(controller, command);
                AxisY.ExecuteServoDriveCommand(controller, command);

                message.Result = ManualControlResult.Success;
                message.Complete();
            }
        }

        private bool IsJogManualControlCommand(ServoDriveManualControlCommand command)
        {
            return command == ServoDriveManualControlCommand.JogForward | command == ServoDriveManualControlCommand.JogReverse |
                command == ServoDriveManualControlCommand.StopJog;
        }

        public bool IsOn(string position)
        {
            return AxisX.IsOn(position) && AxisY.IsOn(position);
        }

        public bool IsOnUnexpectedPosition()
        {
            if (AxisX.IsOnUnexpectedPosition() | AxisY.IsOnUnexpectedPosition())
                return true;

            foreach (var position in AxisX.MatchPositions())
            {
                if (AxisY.IsOn(position))
                    return false;
            }
            return true;
        }

        public bool TryGetCurrentPosition(out string name, out uint id)
        {
            name = string.Empty;
            id = 0;

            if (AxisX.IsOnUnexpectedPosition() | AxisY.IsOnUnexpectedPosition())
                return false;

            foreach (var position in AxisX.MatchPositions())
            {
                if (AxisY.IsOn(position))
                {
                    name = position;
#if DEBUG
                    if (!AxisX.TryGetPositionId(position, out id))
                        throw new BugException("Should not go in here.");
#else
                    AxisX.TryGetPositionId(position, out id);
#endif
                    return true;
                }
            }

            return false;
        }

        public bool CheckIfReadyToAutoRun(SupervisoryController controller)
        {
            return AxisX.CheckIfReadyToAutoRun(controller) && AxisY.CheckIfReadyToAutoRun(controller);
        }

        public AlarmReport GetAlarmReport(SupervisoryController controller)
        {
            throw new NotImplementedException();
        }
    }
}
