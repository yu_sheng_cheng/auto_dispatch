﻿using Automation.Profiles.ServoDrive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects.Movable.ServoDrive
{
    public sealed class ServoDriveIOMemory
    {
        private int _accelerationTime;
        private int _decelerationTime;
        private int _speedLimit;
        private int _torqueLimit;
        private int _jogSpeed;
        private int _positioningSpeed;

        public int AccelerationTime
        {
            get { return _accelerationTime; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("AccelerationTime");

                _accelerationTime = value;
            }
        }

        public int DecelerationTime
        {
            get { return _decelerationTime; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("DecelerationTime");

                _decelerationTime = value;
            }
        }

        public int SpeedLimit
        {
            get { return _speedLimit; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("SpeedLimit");

                _speedLimit = value;
            }
        }

        public int TorqueLimit
        {
            get { return _torqueLimit; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("TorqueLimit");

                _torqueLimit = value;
            }
        }

        public int JogSpeed
        {
            get { return _jogSpeed; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("JogSpeed");

                _jogSpeed = value;
            }
        }

        public int PositioningSpeed
        {
            get { return _positioningSpeed; }
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("PositioningSpeed");

                _positioningSpeed = value;
            }
        }

        public int PositioningValue { get; set; }

        public ServoDriveCommand Command { get; set; }

        public bool HadReturnedToHomePosition { get; set; }

        public bool IsReturningToHomePosition { get; set; }

        public bool IsBusy { get; set; }

        public bool IsPositioningCompleted { get; set; }

        public bool HasErrors { get; set; }

        public ushort ErrorCode { get; set; }

        public int CurrentFeedValue { get; set; }

        public int CurrentTorque { get; set; }

        public void UpdateParameters(ServoDriveParameters parameters)
        {
            _accelerationTime = parameters.AccelerationTime;
            _decelerationTime = parameters.DecelerationTime;
            _speedLimit = parameters.SpeedLimit;
            _torqueLimit = parameters.TorqueLimit;
            _jogSpeed = parameters.JogSpeed;
            _positioningSpeed = parameters.PositioningSpeed;
        }
    }
}
