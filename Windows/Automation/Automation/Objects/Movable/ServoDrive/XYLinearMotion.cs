﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects.Movable.ServoDrive
{
    public class XYLinearMotion : XYMotion
    {
        public XYLinearMotion(string name, ServoDriveControl xAxis, ServoDriveControl yAxis) : base(name, xAxis, yAxis) { }
    }
}
