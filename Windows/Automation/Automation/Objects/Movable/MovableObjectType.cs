﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects.Movable
{
    public enum MovableObjectType : byte
    {
        Cylinder,
        ServoDrive,
        StepperMotor
    }
}
