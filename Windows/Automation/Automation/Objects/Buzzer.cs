﻿using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Core.Registers;
using Automation.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Objects
{
    public class Buzzer : INamedObject, IPollable
    {
        private readonly BitCell _output;

        private ushort _highTime;
        private ushort _lowTime;
        private Timer _timer = new Timer();

        public string Name { get; }

        public Buzzer(string name, BitCell output)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            Name = name;
            _output = output;
        }

        public void Poll(SupervisoryController controller)
        {
            if (_timer.CheckTimeout(controller.Jiffies))
            {
                IORegister register = controller.IORegister;

                bool high = !register.GetCoil(_output);

                _timer.Restart(controller.GetExpirationJiffies(high ? _highTime : _lowTime));
                register.SetCoil(_output, high);
            }
        }

        public void Play(SupervisoryController controller, ushort highTime, ushort lowTime)
        {
            if (highTime == 0)
                throw new ArgumentException("The highTime argument must be greater than zero.", "highTime");

            if (lowTime == 0)
                throw new ArgumentException("The lowTime argument must be greater than zero.", "lowTime");

            _highTime = highTime;
            _lowTime = lowTime;
            _timer.Restart(controller.GetExpirationJiffies(highTime));

            controller.IORegister.SetCoil(_output, true);
        }

        public void Stop(SupervisoryController controller)
        {
            _timer.Stop();
            controller.IORegister.SetCoil(_output, false);
        }
    }
}
