﻿using Automation.Core.Messages.ManualControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Enums
{
    public enum CompletionResult : byte
    {
        Success,
        AlarmOccurred,
        EmergencyStop,
        Reboot,
        ForbiddenOperating,
        Timeout
    }

    public static class CompletionResultExtensions
    {
        public static ManualControlResult ToManualControlResult(this CompletionResult result)
        {
            switch (result)
            {
                case CompletionResult.Success:
                    return ManualControlResult.Success;
                case CompletionResult.AlarmOccurred:
                    return ManualControlResult.AlarmOccurred;
                case CompletionResult.EmergencyStop:
                    return ManualControlResult.EmergencyStop;
                case CompletionResult.Reboot:
                    return ManualControlResult.Reboot;
                case CompletionResult.ForbiddenOperating:
                    return ManualControlResult.ForbiddenOperating;
                case CompletionResult.Timeout:
                    return ManualControlResult.Timeout;
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
