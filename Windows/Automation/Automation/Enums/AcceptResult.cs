﻿using Automation.Core.Messages.ManualControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Enums
{
    public enum AcceptResult : byte
    {
        Accepted,
        Prohibited,
        NotReady,
        Busy
    }

    public static class AcceptResultExtensions
    {
        public static ManualControlResult ToManualControlResult(this AcceptResult result)
        {
            switch (result)
            {
                case AcceptResult.Prohibited:
                    return ManualControlResult.Prohibited;
                case AcceptResult.NotReady:
                    return ManualControlResult.NotReady;
                case AcceptResult.Busy:
                    return ManualControlResult.Busy;
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
