﻿using Automation.Core.Messages.ManualControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Enums
{
    public enum RejectionReason : byte
    {
        Allowed,
        NotReady,
        Busy
    }

    public static class RejectionReasonExtensions
    {
        public static string GetMessage(this RejectionReason reason)
        {
            switch (reason)
            {
                case RejectionReason.Allowed:
                    return SR.Allowed;
                case RejectionReason.NotReady:
                    return SR.NotReady;
                case RejectionReason.Busy:
                    return SR.Busy;
                default:
                    throw new NotImplementedException();
            }
        }

        public static AcceptResult ToAcceptResult(this RejectionReason reason)
        {
            switch (reason)
            {
                case RejectionReason.NotReady:
                    return AcceptResult.NotReady;
                case RejectionReason.Busy:
                    return AcceptResult.Busy;
                default:
                    throw new NotImplementedException();
            }
        }

        public static ManualControlResult ToManualControlResult(this RejectionReason reason)
        {
            switch (reason)
            {
                case RejectionReason.NotReady:
                    return ManualControlResult.NotReady;
                case RejectionReason.Busy:
                    return ManualControlResult.Busy;
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
