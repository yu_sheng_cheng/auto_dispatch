﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automation
{
    public static class SR
    {
        private static StringResource _sr;

        public static string Success { get { return _sr.Success; } }
        public static string Accepted { get { return _sr.Accepted; } }
        public static string Allowed { get { return _sr.Allowed; } }
        public static string Timeout { get { return _sr.Timeout; } }
        public static string CommunicationTimeout { get { return _sr.CommunicationTimeout; } }
        public static string ConnectingSuccess { get { return _sr.ConnectingSuccess; } }
        public static string ConnectingFailed { get { return _sr.ConnectingFailed; } }
        public static string SendFailure { get { return _sr.SendFailure; } }
        public static string ReceiveFailure { get { return _sr.ReceiveFailure; } }
        public static string UnexpectedExceptionOccurred { get { return _sr.UnexpectedExceptionOccurred; } }
        public static string IncorrectRequestContent { get { return _sr.IncorrectRequestContent; } }
        public static string EnteredEmployeeNumberDoesNotExist { get { return _sr.EnteredEmployeeNumberDoesNotExist; } }
        public static string FunctionNotSupported { get { return _sr.FunctionNotSupported; } }
        public static string NotLoggedIn { get { return _sr.NotLoggedIn; } }
        public static string SessionAlreadyExpired { get { return _sr.SessionAlreadyExpired; } }
        public static string RequestMethodNotSupported { get { return _sr.RequestMethodNotSupported; } }
        public static string InvalidRequestParameter { get { return _sr.InvalidRequestParameter; } }
        public static string FileNotFound { get { return _sr.FileNotFound; } }
        public static string NotInManualMode { get { return _sr.NotInManualMode; } }
        public static string NotExist { get { return _sr.NotExist; } }
        public static string SafetyDoorNotLocked { get { return _sr.SafetyDoorNotLocked; } }
        public static string CommandNotSupported { get { return _sr.CommandNotSupported; } }
        public static string UnknownCommand { get { return _sr.UnknownCommand; } }
        public static string InvalidArgument { get { return _sr.InvalidArgument; } }
        public static string NotReady { get { return _sr.NotReady; } }
        public static string Busy { get { return _sr.Busy; } }
        public static string Prohibited { get { return _sr.Prohibited; } }
        public static string AlarmOccurred { get { return _sr.AlarmOccurred; } }
        public static string EmergencyStop { get { return _sr.EmergencyStop; } }
        public static string Reboot { get { return _sr.Reboot; } }
        public static string ForbiddenOperating { get { return _sr.ForbiddenOperating; } }
        public static string AlarmingStopping { get { return _sr.AlarmingStopping; } }
        public static string UnexpectedPosition { get { return _sr.UnexpectedPosition; } }
        public static string ServoOff { get { return _sr.ServoOff; } }
        public static string AxisStopped { get { return _sr.AxisStopped; } }
        public static string InkjetPrinterProtocolError { get { return _sr.InkjetPrinterProtocolError; } }

        static SR()
        {
            _sr = StringResource.Load();
        }

        public static void SetStringResource(StringResource sr)
        {
            _sr = sr;
        }
    }

    [XmlRoot(ElementName = "Automation", Namespace = "Automation.Cultures.StringResource.xaml", IsNullable = false)]
    public class StringResource
    {
        internal static StringResource Load()
        {
            return Load(string.Empty);
        }

        internal static StringResource Load(string name)
        {
            var assembly = Assembly.GetExecutingAssembly();

            if (!string.IsNullOrEmpty(name))
                name = $"_{name}";

            using (var stream = assembly.GetManifestResourceStream($"{assembly.GetName().Name.Replace(".", "._")}.Cultures.StringResource{name}.xml"))
            {
                using (var reader = new StreamReader(stream, Encoding.UTF8))
                {
                    var serializer = new XmlSerializer(typeof(StringResource));
                    return (StringResource)serializer.Deserialize(reader);
                }
            }
        }

        public string Success { get; set; }
        public string Accepted { get; set; }
        public string Allowed { get; set; }
        public string Timeout { get; set; }
        public string CommunicationTimeout { get; set; }
        public string ConnectingSuccess { get; set; }
        public string ConnectingFailed { get; set; }
        public string SendFailure { get; set; }
        public string ReceiveFailure { get; set; }
        public string UnexpectedExceptionOccurred { get; set; }
        public string IncorrectRequestContent { get; set; }
        public string EnteredEmployeeNumberDoesNotExist { get; set; }
        public string FunctionNotSupported { get; set; }
        public string NotLoggedIn { get; set; }
        public string SessionAlreadyExpired { get; set; }
        public string RequestMethodNotSupported { get; set; }
        public string InvalidRequestParameter { get; set; }
        public string FileNotFound { get; set; }
        public string NotInManualMode { get; set; }
        public string NotExist { get; set; }
        public string SafetyDoorNotLocked { get; set; }
        public string CommandNotSupported { get; set; }
        public string UnknownCommand { get; set; }
        public string InvalidArgument { get; set; }
        public string NotReady { get; set; }
        public string Busy { get; set; }
        public string Prohibited { get; set; }
        public string AlarmOccurred { get; set; }
        public string EmergencyStop { get; set; }
        public string Reboot { get; set; }
        public string ForbiddenOperating { get; set; }
        public string AlarmingStopping { get; set; }
        public string UnexpectedPosition { get; set; }
        public string ServoOff { get; set; }
        public string AxisStopped { get; set; }
        public string InkjetPrinterProtocolError { get; set; }
    }
}
