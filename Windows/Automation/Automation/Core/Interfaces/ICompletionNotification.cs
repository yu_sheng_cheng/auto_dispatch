﻿using Automation.Core.Controller;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Interfaces
{
    public interface ICompletionNotification
    {
        Action<SupervisoryController, INamedObject, CompletionResult> Completed { get; set; }
    }
}
