﻿using Automation.Core.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Interfaces
{
    public interface IEventObserver
    {
        void OnReboot(SupervisoryController controller);

        void OnEmergencyStop(SupervisoryController controller);
    }
}
