﻿using Automation.Core.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Interfaces
{
    public interface IAutomatedScript : IPollable
    {
        void Reinitialize(SupervisoryController controller);

        bool CheckBeforeAutoRunRestored(SupervisoryController controller);
    }
}
