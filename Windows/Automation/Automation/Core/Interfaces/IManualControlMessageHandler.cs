﻿using Automation.Core.Controller;
using Automation.Core.Messages.ManualControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Interfaces
{
    public interface IManualControlMessageHandler : INamedObject
    {
        void OnIncomingMessage(SupervisoryController controller, ManualControlMessage message);
    }
}
