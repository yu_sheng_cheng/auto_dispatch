﻿using Automation.Core.Controller;
using Automation.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core
{
    public abstract class AutomatedModule : INamedObject, IPollable
    {
        internal LinkedListNode<AutomatedModule> Node { get; }

        public string Name { get; }

        public abstract bool IsCompleted { get; }

        public abstract bool IsSuccess { get; }

        public AutomatedModule(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            Node = new LinkedListNode<AutomatedModule>(this);
            Name = name;
        }

        public abstract void Go(SupervisoryController controller);

        public abstract void Poll(SupervisoryController controller);

        public virtual void Reset(SupervisoryController controller)
        {

        }
    }
}
