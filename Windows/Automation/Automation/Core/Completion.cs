﻿using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core
{
    public sealed class Completion : ICompletionNotification
    {
        private readonly INamedObject _sender;

        public Action<SupervisoryController, INamedObject, CompletionResult> Completed { get; set; }

        public Completion(INamedObject sender)
        {
            _sender = sender ?? throw new ArgumentNullException("sender");
        }

        public void Complete(SupervisoryController controller, CompletionResult result = CompletionResult.Success)
        {
            var completed = Completed;
            Completed = null;
            completed?.Invoke(controller, _sender, result);
        }
    }
}
