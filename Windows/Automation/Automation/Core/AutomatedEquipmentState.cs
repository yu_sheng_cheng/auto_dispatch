﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core
{
    public sealed class AutomatedEquipmentState
    {
        public OperationMode OperationMode { get; internal set; } = OperationMode.Forbidden;

        public bool AreAllSafetyDoorsLocked { get; internal set; }

        public bool CanUnlockSafetyDoor { get; set; }

        public bool IsActive { get; internal set; }

        public bool IsAutoRunStarting { get; internal set; }

        public bool IsAutoRunSuspended { get; internal set; }

        public bool IsUpdateFailed { get; internal set; }

        public bool EmergencyStop { get; internal set; }

        public bool HasPositiveAirPressureAbnormal { get; internal set; }

        public void KeepActive()
        {
            if (OperationMode != OperationMode.AutoRun | !IsAutoRunStarting)
                return;

            IsActive = true;
        }

        public void NeedsSuspendAutoRun()
        {
            IsAutoRunSuspended = IsAutoRunStarting;
        }
    }
}
