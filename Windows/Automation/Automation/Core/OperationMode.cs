﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core
{
    [Flags]
    public enum OperationMode : byte
    {
        Forbidden = 0,
        Manual = 1,
        AutoRun = 2
    }
}
