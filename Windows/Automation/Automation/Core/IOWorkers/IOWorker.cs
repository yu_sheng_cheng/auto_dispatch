﻿using Automation.Core.Controller;
using Automation.Core.Interfaces;
using SwissKnife;
using SwissKnife.Exceptions;
using SwissKnife.Log;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automation.Core.IOWorkers
{
    public abstract class IOWorker : INamedObject, IRunnable, IDisposable
    {
        private const int IOCompleted = 0;
        private const int Running = 1;
        private const int WatchdogTimerExpired = 2;

        private readonly bool _isWatchdogEnabled;
        private int _timeout = System.Threading.Timeout.Infinite;

        private uint _expires;
        private bool _isWatchdogTimerEnabled;
        private bool _isRunning;
        private int _status;

        protected ILog Log { get; }

        public string LogKeyword { get; set; }

        public string Name { get; }

        public SupervisoryController Controller { get; }

        protected virtual AsynchronousTimeoutSignal TimeoutSignal { get { return null; } }

        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value > 0 ? value : System.Threading.Timeout.Infinite; }
        }

        public bool IsBusy { get; internal set; }

        public bool IsRunning { get { return _isRunning; } }

        public OperationResult Result { get; private set; }

        public object UserToken { get; set; }

        public Action<IOWorker> Completed { get; set; }

        public IOWorker(string name, SupervisoryController controller, bool isWatchdogEnabled = true)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            _isWatchdogEnabled = isWatchdogEnabled;

            Controller = controller ?? throw new ArgumentNullException("controller");
            Log = AutomationEnvironment.Log;
            LogKeyword = name;
            Name = name;
        }

        public virtual void Dispose()
        {

        }

        public async void Run()
        {
            if (Interlocked.Exchange(ref _status, Running) != IOCompleted)
                throw new BugException("Previous operation is still in progress.");

            if (_isWatchdogEnabled)
            {
                _isWatchdogTimerEnabled = false;

                int timeout = _timeout;

                if (timeout > 0)
                {
                    _expires = Controller.GetExpirationJiffies(timeout);
                    _isWatchdogTimerEnabled = true;

                    TimeoutSignal?.Reset();
                }
            }

            Result = null;
            Volatile.Write(ref _isRunning, true);

            try
            {
                OperationResult result = await Execute().ConfigureAwait(false);

                int status = Interlocked.CompareExchange(ref _status, IOCompleted, Running);
                if (status == WatchdogTimerExpired)
                {
                    Interlocked.Exchange(ref _status, IOCompleted);
                    result = OperationResultFactory.CreateFailedResult(GetType().FullName, (int)AutomationError.Timeout, SR.Timeout);
                }
                Result = result;
            }
            catch (Exception e)
            {
                Interlocked.Exchange(ref _status, IOCompleted);
                Log.e(LogKeyword, "Execute:", e);
                Result = OperationResultFactory.CreateFailedResult(GetType().FullName, SR.UnexpectedExceptionOccurred, e);
            }
            finally
            {
                _isWatchdogTimerEnabled = false;
                _isRunning = false;
                Controller.PostIOCompletion(this);
            }
        }

        protected abstract Task<OperationResult> Execute();

        public void CheckTimeout(uint jiffies)
        {
            if (_isWatchdogTimerEnabled && TimeComparison.IsAfter(jiffies, _expires))
            {
                if (Interlocked.CompareExchange(ref _status, WatchdogTimerExpired, Running) == Running)
                {
                    Log.e(LogKeyword, "Execute: Timeout");
                    OnTimeout();
                }
            }
        }

        protected virtual void OnTimeout()
        {
            AsynchronousTimeoutSignal timeoutSignal = TimeoutSignal;
            if (timeoutSignal != null)
                timeoutSignal.Raise();
        }

        public virtual void Complete()
        {
            IsBusy = false;

            Completed?.Invoke(this);
        }
    }
}
