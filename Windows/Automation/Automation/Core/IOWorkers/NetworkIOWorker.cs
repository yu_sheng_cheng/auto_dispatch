﻿using Automation.Core.Controller;
using Net.AsyncSocket.Tcp;
using SwissKnife;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automation.Core.IOWorkers
{
    public abstract class NetworkIOWorker<T1, T2> : IOWorker
    {
        private readonly IPEndPoint _remoteEndPoint;
        private readonly AsynchronousTimeoutSignal _timeoutSignal;
        private AsyncTcpClient _session;

        protected override AsynchronousTimeoutSignal TimeoutSignal => _timeoutSignal;

        public NetworkIOWorker(string name, SupervisoryController controller, IPEndPoint remoteEndPoint) : base(name, controller)
        {
            _remoteEndPoint = remoteEndPoint ?? throw new ArgumentNullException("remoteEndPoint");

            _timeoutSignal = new AsynchronousTimeoutSignal((o) =>
            {
                AsyncTcpClient session = o as AsyncTcpClient;
                ThreadPool.QueueUserWorkItem((_) => session.Dispose());
            });
        }

        public override void Dispose()
        {
            base.Dispose();

            _session?.Dispose();
        }

        protected async Task<OperationResult<T2>> SendThenReceiveAsync(T1 command)
        {
            if (_session == null)
            {
                OperationResult<AsyncTcpClient> connectResult = await CreateSessionAsync().ConfigureAwait(false);
                if (!connectResult.IsSuccess)
                    return OperationResultFactory.CreateFailedResult<T2>(connectResult);

                _session = connectResult.Value;
            }

            OperationResult<T2> replyResult;

            _timeoutSignal.Register(_session, out bool timeout);
            if (timeout)
            {
                _timeoutSignal.UnRegister(out timeout);
                Log.w(LogKeyword, "SendThenReceiveAsync: Communication timeout after getting connection");
                replyResult = OperationResultFactory.CreateFailedResult<T2>(GetType().FullName, (int)AutomationError.Timeout, SR.CommunicationTimeout);
            }
            else
            {
                replyResult = await SendThenReceiveAsync(_session, command);

                _timeoutSignal.UnRegister(out timeout);
                if (timeout)
                {
                    Log.w(LogKeyword, "SendThenReceiveAsync: Communication timeout");

                    if (replyResult.IsSuccess)
                        replyResult = OperationResultFactory.CreateFailedResult<T2>(GetType().FullName, (int)AutomationError.Timeout, SR.CommunicationTimeout);
                }
            }

            if (!replyResult.IsSuccess)
            {
                _session.Dispose();
                _session = null;
            }

            return replyResult;
        }

        protected abstract Task<OperationResult<T2>> SendThenReceiveAsync(AsyncTcpClient session, T1 command);

        private async Task<OperationResult<AsyncTcpClient>> CreateSessionAsync()
        {
            AsyncTcpClient client = null;

            try
            {
                client = new AsyncTcpClient(_remoteEndPoint);

                if (!await ConnectAsync(client))
                {
                    client.Dispose();
                    client = null;
                    return OperationResultFactory.CreateFailedResult<AsyncTcpClient>(GetType().FullName, (int)AutomationError.Timeout, SR.CommunicationTimeout);
                }

                return OperationResultFactory.CreateSuccessfulResult(client, SR.ConnectingSuccess);
            }
            catch (Exception e)
            {
                if (!(e is ObjectDisposedException))
                    Log.e(LogKeyword, "CreateSessionAsync:", e);

                if (client != null)
                    client.Dispose();

                return OperationResultFactory.CreateFailedResult<AsyncTcpClient>(GetType().FullName, SR.ConnectingFailed, e);
            }
        }

        private async Task<bool> ConnectAsync(AsyncTcpClient client)
        {
            _timeoutSignal.Register(client, out bool timeout);
            if (timeout)
            {
                _timeoutSignal.UnRegister(out timeout);
                return false;
            }

            try
            {
                await client.ConnectAsync().ConfigureAwait(false);

                _timeoutSignal.UnRegister(out timeout);
                return !timeout;
            }
            catch
            {
                _timeoutSignal.UnRegister(out timeout);

                if (timeout)
                {
                    Log.w(LogKeyword, "ConnectAsync: Timeout");
                }
                throw;
            }
        }
    }
}
