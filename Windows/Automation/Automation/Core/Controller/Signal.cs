﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Controller
{
    public partial class SupervisoryController
    {
        private readonly Dictionary<string, LinkedList<Action<SignalArgs>>> _signals = new Dictionary<string, LinkedList<Action<SignalArgs>>>(StringComparer.Ordinal);

        public void Connect(string signalName, Action<SignalArgs> slot)
        {
            if (slot == null)
                throw new ArgumentNullException("slot");

            if (!_signals.TryGetValue(signalName, out LinkedList<Action<SignalArgs>> slots))
            {
                slots = new LinkedList<Action<SignalArgs>>();
                _signals.Add(signalName, slots);
            }

            slots.AddLast(slot);
        }

        public void Disconnect(string signalName, Action<SignalArgs> slot)
        {
            if (_signals.TryGetValue(signalName, out LinkedList<Action<SignalArgs>> slots))
                slots.Remove(slot);
        }

        public void Emit(string signalName, SignalArgs args)
        {
            if (_signals.TryGetValue(signalName, out LinkedList<Action<SignalArgs>> slots))
            {
                var node = slots.First;

                while (node != null)
                {
                    var next = node.Next;

                    node.Value.Invoke(args);

                    node = next;
                }
            }
        }
    }
}
