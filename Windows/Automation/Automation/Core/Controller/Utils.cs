﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Controller
{
    public partial class SupervisoryController
    {
        public uint GetExpirationJiffies(int timeout)
        {
            return unchecked(Jiffies + GetJiffiesFromMilliseconds(timeout));
        }

        public uint GetJiffiesFromMilliseconds(int value)
        {
            int jiffies = value / _scanCycleTime;

            if (jiffies <= 0)
                jiffies = 1;

            return (uint)jiffies;
        }
    }
}
