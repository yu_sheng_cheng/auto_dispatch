﻿using Automation.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Controller
{
    public partial class SupervisoryController
    {
        private bool AreAllSafetyDoorsLocked()
        {
            foreach (var door in _safetyDoors)
            {
                door.Poll(this);

                bool locked = door.IsLocked | IsTuning;

                if (!locked)
                    return false;
            }
            return true;
        }

        private void LockAllSafetyDoors()
        {
            foreach (var door in _safetyDoors)
            {
                door.Lock(this);
            }
        }

        private void LockSafetyDoor(string name)
        {
            if (!TryGetSafetyDoor(name, out SafetyDoor door))
                return;

            door.Lock(this);
        }

        private bool UnlockAllSafetyDoors()
        {
            bool autoRun = EquipmentState.OperationMode == OperationMode.AutoRun;

            if (autoRun | !EquipmentState.CanUnlockSafetyDoor)
                return false;

            foreach (var door in _safetyDoors)
            {
                if (door.IgnoreUnlockAllCommand)
                    continue;

                door.Unlock(this);
            }
            return true;
        }

        private bool UnlockSafetyDoor(string name)
        {
            if (!TryGetSafetyDoor(name, out SafetyDoor door))
                return false;

            bool autoRun = EquipmentState.OperationMode == OperationMode.AutoRun;

            if (autoRun)
            {
                if (!EquipmentState.IsAutoRunStarting | EquipmentState.IsAutoRunSuspended)
                    return false;

                if (door.CanBeUnlockedDuringAutoRun)
                {
                    door.Unlock(this);
                    return true;
                }

                return CanBeAcceptedToUnlockSafetyDoorDuringAutoRun(name);
            }
            else
            {
                if (!EquipmentState.CanUnlockSafetyDoor)
                    return false;

                door.Unlock(this);
                return true;
            }
        }

        protected virtual bool CanBeAcceptedToUnlockSafetyDoorDuringAutoRun(string doorName)
        {
            return false;
        }
    }
}
