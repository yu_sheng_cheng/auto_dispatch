﻿using Automation.Core.Alarm;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Objects;
using Automation.Objects.Movable;
using Automation.Objects.Movable.ServoDrive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Controller
{
    public partial class SupervisoryController
    {
        private readonly List<SafetyDoor> _safetyDoors = new List<SafetyDoor>();
        private IDictionary<string, SafetyDoor> _safetyDoorDictionary;

        private readonly List<IMovableObject> _movableObjects = new List<IMovableObject>();
        private readonly IDictionary<string, IMovableObject> _movableObjectDictionary = new Dictionary<string, IMovableObject>(StringComparer.Ordinal);

        private readonly List<IAlarmSource> _additionalAlarmSource = new List<IAlarmSource>();
        private readonly List<IPollable> _pollableObjects = new List<IPollable>();
        private readonly List<IEventObserver> _eventObservers = new List<IEventObserver>();

        private readonly IDictionary<string, ContinuousActionDirector> _continuousActionDirectors = new Dictionary<string, ContinuousActionDirector>(StringComparer.Ordinal);

        protected IEnumerable<IMovableObject> MovableObjects { get { return _movableObjects; } }

        public void AddSafetyDoor(SafetyDoor safetyDoor)
        {
            ThrowIfStarted();

            if (safetyDoor == null)
                throw new ArgumentNullException("safetyDoor");

            _safetyDoors.Add(safetyDoor);

            if (_safetyDoors.Count <= 6)
                return;

            if (_safetyDoorDictionary == null)
                _safetyDoorDictionary = new Dictionary<string, SafetyDoor>(StringComparer.Ordinal);

            _safetyDoorDictionary.Add(safetyDoor.Name, safetyDoor);
        }

        public bool TryGetSafetyDoor(string name, out SafetyDoor safetyDoor)
        {
            if (_safetyDoorDictionary == null)
            {
                if (name == null)
                    throw new ArgumentNullException("name");

                foreach (var door in _safetyDoors)
                {
                    if (name == door.Name)
                    {
                        safetyDoor = door;
                        return true;
                    }
                }

                safetyDoor = null;
                return false;
            }
            else
                return _safetyDoorDictionary.TryGetValue(name, out safetyDoor);
        }

        public void AddMovableObject(IMovableObject movableObject)
        {
            ThrowIfStarted();

            AddManualControlMessageHandler(movableObject);

            _movableObjects.Add(movableObject);
            _movableObjectDictionary.Add(movableObject.Name, movableObject);

            _alarmWatcher.AddAlarmSource(movableObject);
            _eventObservers.Add(movableObject);
        }

        public void AddServoSimultaneousAction(IMovableObject movableObject)
        {
            ThrowIfStarted();

            AddManualControlMessageHandler(movableObject);

            _movableObjectDictionary.Add(movableObject.Name, movableObject);
        }

        public bool TryGetMovableObject(string name, out IMovableObject movableObject)
        {
            return _movableObjectDictionary.TryGetValue(name, out movableObject);
        }

        public void AddMiscObject(object obj)
        {
            ThrowIfStarted();

            if (obj == null)
                throw new ArgumentNullException("obj");

            if (obj is IManualControlMessageHandler handler)
                AddManualControlMessageHandler(handler);

            if (obj is IAlarmSource alarmSource)
            {
                _alarmWatcher.AddAlarmSource(alarmSource);
                _additionalAlarmSource.Add(alarmSource);
            }
            else if (obj is IPollable pollableObject)
                _pollableObjects.Add(pollableObject);

            if (obj is IEventObserver eventObserver)
                _eventObservers.Add(eventObserver);
        }

        public void AddContinuousActionDirector(ContinuousActionDirector director)
        {
            ThrowIfStarted();

            if (director == null)
                throw new ArgumentNullException("director");

            _continuousActionDirectors.Add(director.Name, director);
        }

        public bool TryGetContinuousActionDirector(string name, out ContinuousActionDirector director)
        {
            return _continuousActionDirectors.TryGetValue(name, out director);
        }
    }
}
