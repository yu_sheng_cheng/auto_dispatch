﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Controller
{
    public class SupervisoryControllerConfiguration
    {
        private static readonly string DefaultProfileRootFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "AutomatedEquipmentProfiles");
        private static readonly string DefaultAlarmCommentResourceFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "AlarmCommentResource.xml");

        private string _profileRootFolder;
        private string _alarmCommentResourceFile;

        public string ProfileRootFolder
        {
            get { return _profileRootFolder ?? DefaultProfileRootFolder; }
            set { _profileRootFolder = value; }
        }

        public string AlarmCommentResourceFile
        {
            get { return _alarmCommentResourceFile ?? DefaultAlarmCommentResourceFile; }
            set { _alarmCommentResourceFile = value; }
        }

        public SupervisoryControllerConfiguration() { }

        public SupervisoryControllerConfiguration(string profileRootFolder, string alarmCommentResourceFile)
        {
            _profileRootFolder = profileRootFolder;
            _alarmCommentResourceFile = alarmCommentResourceFile;
        }
    }
}
