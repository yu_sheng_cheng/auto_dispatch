﻿using Automation.Core.IOWorkers;
using SwissKnife.Exceptions;
using SwissKnife.Queues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Controller
{
    public partial class SupervisoryController
    {
        private readonly HashSet<IOWorker> _pendingIOWorkers = new HashSet<IOWorker>();
        private readonly HashSet<IOWorker> _runningIOWorkers = new HashSet<IOWorker>();
        private readonly List<IOWorker> _pendingToRunIOWorkers = new List<IOWorker>();
        private readonly OneReadManyWriteQueue<IOWorker> _ioCompletionQueue = new OneReadManyWriteQueue<IOWorker>();

        public void AddIORequest(IOWorker worker, Action<IOWorker> completed = null)
        {
            ThrowIfStopped();

            if (worker == null)
                throw new ArgumentNullException("worker");

            if (_pendingIOWorkers.Contains(worker) | _runningIOWorkers.Contains(worker))
                throw new InvalidOperationException("This IOWorker is already in progress.");

            if (_status == Disposed)
            {
                Log.d(LogKeyword, $"The supervisory controller already disposed to throw io request");
                return;
            }

            if (completed != null)
                worker.Completed = completed;

            worker.IsBusy = true;

            _pendingIOWorkers.Add(worker);

            AddTask(worker);

            EquipmentState.KeepActive();
        }

        public void PostIOCompletion(IOWorker completion)
        {
            if (completion == null)
                throw new ArgumentNullException("completion");

            _ioCompletionQueue.Enqueue(completion);
        }

        private void ProcessIO()
        {
            IOWorker worker;

            while ((worker = _ioCompletionQueue.Dequeue()) != null)
            {
#if DEBUG
                if (_pendingIOWorkers.Contains(worker))
                    _pendingIOWorkers.Remove(worker);
                else
                {
                    if (!_runningIOWorkers.Contains(worker))
                        throw new BugException("IOWorker was not found.");
                    _runningIOWorkers.Remove(worker);
                }
#else
                _pendingIOWorkers.Remove(worker);
                _runningIOWorkers.Remove(worker);
#endif
                if (!worker.Result.IsSuccess)
                    Log.i(LogKeyword, $"{worker.Name}: {worker.Result}");

                worker.Complete();
            }

            foreach (var work in _runningIOWorkers)
            {
                work.CheckTimeout(_lastScanningJiffies);
            }

            foreach (var work in _pendingIOWorkers)
            {
                if (work.IsRunning)
                    _pendingToRunIOWorkers.Add(work);
            }

            foreach (var work in _pendingToRunIOWorkers)
            {
                _pendingIOWorkers.Remove(work);
                _runningIOWorkers.Add(work);
            }
            _pendingToRunIOWorkers.Clear();
        }
    }
}
