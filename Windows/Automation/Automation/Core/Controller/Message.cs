﻿using Automation.Core.Interfaces;
using Automation.Core.Messages;
using Automation.Core.Messages.ManualControl;
using Automation.Core.Messages.ManualControl.OneCycle;
using SwissKnife.Queues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Controller
{
    public partial class SupervisoryController
    {
        private readonly IDictionary<string, IManualControlMessageHandler> _manualControlMessageHandlers = new Dictionary<string, IManualControlMessageHandler>(StringComparer.Ordinal);

        private readonly OneReadManyWriteQueue<Message> _messageQueue = new OneReadManyWriteQueue<Message>();
        private readonly List<Message> _lowPriorityMessages = new List<Message>();

        private OneCycleManualControlMessage _oneCycleManualControlMessage;

        public void AddManualControlMessageHandler(IManualControlMessageHandler handler)
        {
            ThrowIfStarted();

            if (handler == null)
                throw new ArgumentNullException("handler");

            string name = handler.Name;

            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("The 'Name' property of the IManualControlMessageHandler cannot be empty or null.", "handler");

            _manualControlMessageHandlers.Add(name, handler);
        }

        public void PostMessage(Message message)
        {
            ThrowIfNotStarted();

            if (message == null)
                throw new ArgumentNullException("message");

            _messageQueue.Enqueue(message);
        }

        private void ProcessMessage()
        {
            Message message;

            while ((message = _messageQueue.Dequeue()) != null)
            {
                message.Controller = this;

                if (message.Priority == MessagePriority.High)
                    OnIncomingMessage(message);
                else
                    _lowPriorityMessages.Add(message);
            }
        }

        private void ProcessLowPriorityMessage()
        {
            foreach (var message in _lowPriorityMessages)
            {
                OnIncomingMessage(message);
            }

            _lowPriorityMessages.Clear();
        }

        protected virtual void OnIncomingMessage(Message message)
        {
            if (message.Class != (byte)MessageClass.SupervisoryController)
                return;

            switch (message.Id)
            {
                case MessageId.KeepAlive:
                    message.Complete();
                    return;

                case MessageId.SafetyDoor:
                    OnIncomingSafetyDoorMessage(message);
                    return;

                case MessageId.ClearAlarm:
                    OnIncomingClearAlarmMessage(message);
                    return;

                case MessageId.ManualControl:
                    OnIncomingManualControlMessage(message);
                    return;

                case MessageId.IO:
                    OnIncomingIOMessage(message);
                    return;
            }
        }

        private void OnIncomingSafetyDoorMessage(Message message)
        {
            if (!(message is SafetyDoorMessage safetyDoorMessage))
                throw new ArgumentException("The message instance must derive from SafetyDoorMessage.", "message");

            switch (safetyDoorMessage.Command)
            {
                case SafetyDoorCommand.LockAll:
                    LockAllSafetyDoors();
                    message.Complete();
                    break;
                case SafetyDoorCommand.UnlockAll:
                    message.Complete(UnlockAllSafetyDoors());
                    break;
                case SafetyDoorCommand.Lock:
                    LockSafetyDoor(safetyDoorMessage.SafetyDoorName);
                    message.Complete();
                    break;
                case SafetyDoorCommand.Unlock:
                    message.Complete(UnlockSafetyDoor(safetyDoorMessage.SafetyDoorName));
                    break;
            }
        }

        private void OnIncomingClearAlarmMessage(Message message)
        {
            if (!(message is ClearAlarmMessage clearAlarmMessage))
                throw new ArgumentException("The message instance must derive from ClearAlarmMessage.", "message");

            bool manual = (EquipmentState.OperationMode & OperationMode.Manual) == OperationMode.Manual;

            if (!manual)
            {
                message.Complete(false);
                return;
            }

            if (clearAlarmMessage.IsClearAll)
                _alarmWatcher.Reset(this);
            else
            {
                string alarmSourceName = clearAlarmMessage.AlarmSourceName;

                if (string.IsNullOrEmpty(alarmSourceName))
                    _alarmWatcher.Reset(this, clearAlarmMessage.AlarmCode);
                else
                    _alarmWatcher.Reset(this, alarmSourceName);
            }
            message.Complete();
        }

        private void OnIncomingManualControlMessage(Message message)
        {
            if (!(message is ManualControlMessage manualControlMessage))
                throw new ArgumentException("The message instance must derive from ManualControlMessage.", "message");

            bool manual = (EquipmentState.OperationMode & OperationMode.Manual) == OperationMode.Manual;

            if (!manual)
            {
                manualControlMessage.Result = ManualControlResult.NotInManualMode;
                message.Complete(false);
                return;
            }

            if (message is OneCycleManualControlMessage oneCycleMessage)
            {
                if (!EquipmentState.AreAllSafetyDoorsLocked)
                {
                    manualControlMessage.Result = ManualControlResult.SafetyDoorNotLocked;
                    message.Complete(false);
                    return;
                }

                if (_oneCycleManualControlMessage != null)
                {
                    manualControlMessage.Result = ManualControlResult.OneCycleDisabled;
                    message.Complete(false);
                    return;
                }

                if (!oneCycleMessage.Go(this))
                    return;

                _oneCycleManualControlMessage = oneCycleMessage;
            }
            else
            {
                if (_oneCycleManualControlMessage != null)
                {
                    manualControlMessage.Result = ManualControlResult.SingleActionDisabled;
                    message.Complete(false);
                    return;
                }

                if (!_manualControlMessageHandlers.TryGetValue(manualControlMessage.Name, out IManualControlMessageHandler handler))
                {
                    manualControlMessage.Result = ManualControlResult.NotExist;
                    message.Complete(false);
                    return;
                }

                handler.OnIncomingMessage(this, manualControlMessage);
            }
        }

        private void OnIncomingIOMessage(Message message)
        {
            if (!(message is IOMessage ioMessage))
                throw new ArgumentException("The message instance must derive from IOMessage.", "message");

            bool manual = (EquipmentState.OperationMode & OperationMode.Manual) == OperationMode.Manual;

            if (!manual | IORegister == null)
            {
                message.Complete(false);
                return;
            }

            bool success = true;
            try
            {
                IORegister.SetCoil(ioMessage.BitCell, ioMessage.BitValue);
            }
            catch
            {
                success = false;
            }
            message.Complete(success);
        }

        public void PollOneCycleManualControlMessage()
        {
            if (_oneCycleManualControlMessage != null)
            {
                _oneCycleManualControlMessage.Poll(this);

                if (_oneCycleManualControlMessage.IsCompleted)
                    _oneCycleManualControlMessage = null;
            }
        }
    }
}
