﻿using Automation.Core.Alarm;
using Automation.Core.Interfaces;
using Automation.Core.Registers;
using Automation.Objects.Movable;
using SwissKnife;
using SwissKnife.Exceptions;
using SwissKnife.Log;
using SwissKnife.Threading;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automation.Core.Controller
{
    public abstract partial class SupervisoryController
    {
        private const int Stopped = 0;
        private const int Started = 1;
        private const int Disposed = 2;

        private const int DefaultTimeForBoot = 5000;
        private const int DefaultTimeForAbnormalAirPressure = 30000;
        private const int DefaultTimeForRecoveredAirPressure = 20000;

        private const int WatchdogMaxTimeout = 60000;
        private const int WatchdogMinTimeout = 100;
        private const int WatchdogDefaultTimeout = 10000;

        private readonly AlarmWatcher _alarmWatcher = new AlarmWatcher();

        private readonly LinkedList<IAutomatedScript> _automatedScripts = new LinkedList<IAutomatedScript>();
        private readonly LinkedList<AutomatedModule> _automatedModules = new LinkedList<AutomatedModule>();

        private readonly ConcurrentStack<List<IRunnable>> _taskQueuePool = new ConcurrentStack<List<IRunnable>>();

        private readonly Timer _timer;
        private readonly SemaphoreSlim _stoppedEvent = new SemaphoreSlim(0, 1);

        private int _status;

        private int _scanCycleTime = 30;
        private int _timeForBoot = DefaultTimeForBoot;
        private int _timeForAbnormalAirPressure = DefaultTimeForAbnormalAirPressure;
        private int _timeForRecoveredAirPressure = DefaultTimeForRecoveredAirPressure;
        private int _watchdogTimeout = WatchdogDefaultTimeout;
        private uint _watchdogTimeoutJiffies;
        private bool _isFirstScan = true;

        private bool _emergencyStop;
        private bool _hasPositiveAirPressureAbnormal;
        private Helper.Timer _airPressureTimer = new Helper.Timer();
        private Helper.Timer _bootTimer = new Helper.Timer();

        private List<IRunnable> _taskQueue;

        private readonly LightSpinLock _lock = new LightSpinLock();
        private uint _watchdogExpires;
        private uint _lastScanningJiffies;
        private bool _isScanning;
        private bool _timerDisabled;

        public ILog Log { get; }

        public string LogKeyword { get; set; }

        public string ProfileRootFolder { get; }

        public bool IsTuning { get; set; }

        public int ScanCycleTime
        {
            get { return _scanCycleTime; }
            protected set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("ScanCycleTime");

                _scanCycleTime = value;

                ThrowIfStarted();
            }
        }

        public int TimeForBoot
        {
            get { return _timeForBoot; }
            protected set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("TimeForBoot");

                _timeForBoot = value;

                ThrowIfStarted();
            }
        }

        public int TimeForAbnormalAirPressure
        {
            get { return _timeForAbnormalAirPressure; }
            protected set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("TimeForAbnormalAirPressure");

                _timeForAbnormalAirPressure = value;

                ThrowIfStarted();
            }
        }

        public int TimeForRecoveredAirPressure
        {
            get { return _timeForRecoveredAirPressure; }
            protected set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("TimeForRecoveredAirPressure");

                _timeForRecoveredAirPressure = value;

                ThrowIfStarted();
            }
        }

        public int WatchdogTimeout
        {
            get { return _watchdogTimeout; }
            protected set
            {
                if (value > WatchdogMaxTimeout)
                    _watchdogTimeout = WatchdogMaxTimeout;
                else if (value < WatchdogMinTimeout)
                    _watchdogTimeout = WatchdogMinTimeout;
                else
                    _watchdogTimeout = value;

                ThrowIfStarted();
            }
        }

        protected abstract AsynchronousTimeoutSignal WatchdogExpiredSignal { get; }

        public IAlarmWatcher AlarmWatcher => _alarmWatcher;

        public AutomatedEquipmentState EquipmentState { get; } = new AutomatedEquipmentState();

        public IORegister IORegister { get; protected set; }

        public MotionControlRegister MotionControlRegister { get; protected set; }

        protected IEnumerable<IAutomatedScript> AutomatedScripts { get { return _automatedScripts; } }

        public bool IsReady { get; private set; }

        protected bool EmergencyStop
        {
            get { return _emergencyStop; }
            set
            {
                ThrowIfStopped();

                if (value != _emergencyStop)
                {
                    _emergencyStop = value;

                    EquipmentState.EmergencyStop = value | EquipmentState.HasPositiveAirPressureAbnormal;

                    if (value)
                    {
                        StopEmergency();
                        Log.w(LogKeyword, "Emergency stop triggered");
                    }
                    else
                        Log.i(LogKeyword, "Emergency stop cancelled");
                }
            }
        }

        protected bool HasPositiveAirPressureAbnormal
        {
            get { return _hasPositiveAirPressureAbnormal; }
            set
            {
                ThrowIfStopped();

                if (value != _hasPositiveAirPressureAbnormal)
                {
                    if (value)
                    {
                        _hasPositiveAirPressureAbnormal = true;

                        Log.w(LogKeyword, "Abnormal positive air pressure");

                        _airPressureTimer.Restart(GetExpirationJiffies(_timeForAbnormalAirPressure));

                        EquipmentState.EmergencyStop = true;
                        EquipmentState.HasPositiveAirPressureAbnormal = true;

                        foreach (IMovableObject movableObject in _movableObjects)
                        {
                            movableObject.OnPositiveAirPressureSourceChanged(this, true);
                        }

                        StopEmergency();
                    }
                    else
                    {
                        if (_airPressureTimer.IsEnabled)
                            return;

                        _hasPositiveAirPressureAbnormal = false;

                        _airPressureTimer.Start(GetExpirationJiffies(_timeForRecoveredAirPressure));

                        Log.i(LogKeyword, "Positive air pressure source already recovered, startup timer");
                    }
                }
            }
        }

        public uint Jiffies { get; private set; }

        public SupervisoryController() : this(new SupervisoryControllerConfiguration()) { }

        public SupervisoryController(SupervisoryControllerConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException("configuration");

            LoadAlarmCommentResource(configuration.AlarmCommentResourceFile);

            Log = AutomationEnvironment.Log;
            ProfileRootFolder = configuration.ProfileRootFolder;

            _timer = new Timer(PollingTimer_Expired, null, Timeout.Infinite, Timeout.Infinite);
        }

        public void Dispose()
        {
            DisposeAsync().Wait();
        }

        public async Task DisposeAsync()
        {
            int status = Interlocked.Exchange(ref _status, Disposed);

            if (status == Disposed)
                return;

            if (status == Stopped)
                CleanUp();
            else
            {
                await _stoppedEvent.WaitAsync().ConfigureAwait(false);
                CleanUp();
            }
        }

        protected virtual void CleanUp()
        {
            using (var completionEvent = new ManualResetEvent(false))
            {
                if (_timer.Dispose(completionEvent))
                    completionEvent.WaitOne();
            }

            _stoppedEvent.Dispose();
            _alarmWatcher.Dispose();
        }

        public void Start()
        {
            int status = Interlocked.CompareExchange(ref _status, Started, Stopped);

            if (status == Disposed)
                throw new ObjectDisposedException(GetType().FullName);

            if (status == Started)
                return;

            _watchdogTimeoutJiffies = GetJiffiesFromMilliseconds(_watchdogTimeout);

            _alarmWatcher.Start();

            OnStarted();

            Thread.MemoryBarrier();

            _timer.Change(_scanCycleTime, _scanCycleTime);
        }

        protected virtual void OnStarted()
        {

        }

        private void Stop()
        {
            _timer.Change(Timeout.Infinite, Timeout.Infinite);

            _lock.Enter();
            _isScanning = false;
            _timerDisabled = true;
            _lock.Leave();

            OnStopped();

            _stoppedEvent.Release();
        }

        protected virtual void OnStopped()
        {

        }

        private async void PollingTimer_Expired(object state)
        {
            uint jiffies;

            AsynchronousTimeoutSignal watchdogExpiredSignal = WatchdogExpiredSignal;
            if (watchdogExpiredSignal == null)
                throw new BugException("The 'WatchdogExpiredSignal' property of the SupervisoryController cannot be null.");

            int lastScanningElapsedJiffies;

            _lock.Enter();
            try
            {
                if (_timerDisabled)
                    return;

                jiffies = unchecked(Jiffies + 1);
                Jiffies = jiffies;

                if (_isScanning)
                {
                    if (TimeComparison.IsAfter(jiffies, _watchdogExpires))
                    {
                        if (watchdogExpiredSignal.Raise())
                            Log.w(LogKeyword, "The scan must be completed before this timeout expired and launches a new cycle");
                    }
                    return;
                }
                else
                {
                    lastScanningElapsedJiffies = unchecked((int)(jiffies - _lastScanningJiffies));
                    _lastScanningJiffies = jiffies;
                    _isScanning = true;
                    _watchdogExpires = unchecked(jiffies + _watchdogTimeoutJiffies);
                    watchdogExpiredSignal.Reset();
                }
            }
            finally
            {
                _lock.Leave();
            }

            if (lastScanningElapsedJiffies > 1)
                Log.d(LogKeyword, $"The last scan to spend {lastScanningElapsedJiffies.ToString()} cycle time");

            await Scan().ConfigureAwait(false);

            if (_status == Disposed)
            {
                if (_pendingIOWorkers.Count == 0 && _runningIOWorkers.Count == 0)
                {
                    Stop();
                    return;
                }
            }

            Volatile.Write(ref _isScanning, false);
        }

        private async Task Scan()
        {
            ProcessIO();

            if (_status == Disposed)
                return;

            if (EquipmentState.IsUpdateFailed)
            {
                await RetryUpdateAsync();
                return;
            }

            if (!await RefreshAsync().ConfigureAwait(false))
            {
                if (_bootTimer.IsEnabled)
                {
                    _bootTimer.Restart(GetExpirationJiffies(_timeForBoot));

                    Log.i(LogKeyword, "The boot timer has already been restarted");
                }
                return;
            }

            if (_bootTimer.IsEnabled)
            {
                if (!CheckIfBootHasBeenCompleted())
                    return;
            }

            EquipmentState.AreAllSafetyDoorsLocked = AreAllSafetyDoorsLocked();

            bool needsSuspendAutoRun = false;

            foreach (var alarmSource in _additionalAlarmSource)
            {
                alarmSource.Poll(this);
                needsSuspendAutoRun |= alarmSource.AlarmOccurred;
            }

            bool hasMovingObjects = false;

            foreach (var movableObject in _movableObjects)
            {
                movableObject.Poll(this);

                hasMovingObjects |= !movableObject.IsStationary;
                needsSuspendAutoRun |= movableObject.AlarmOccurred;
            }

            if (needsSuspendAutoRun)
                EquipmentState.NeedsSuspendAutoRun();

            if (_isFirstScan)
            {
                _isFirstScan = false;

                foreach (var movableObject in _movableObjects)
                {
                    movableObject.StopCommand?.Invoke(this);
                }
            }

            foreach (var obj in _pollableObjects)
            {
                obj.Poll(this);
            }

            PollAutomatedModules();
            PollOneCycleManualControlMessage();

            bool isActive = hasMovingObjects | _automatedModules.Count != 0 | _oneCycleManualControlMessage != null;

            EquipmentState.CanUnlockSafetyDoor = !(isActive | _airPressureTimer.IsEnabled);
            EquipmentState.IsActive = _pendingIOWorkers.Count != 0 | _runningIOWorkers.Count != 0 | isActive;

            if (!EquipmentState.IsActive)
            {
                if (!IsReady)
                {
                    IsReady = true;

                    Log.i(LogKeyword, "The automated equipment ready");

                    Initialize();
                }

                if (EquipmentState.OperationMode == OperationMode.Forbidden)
                {
                    if (!EquipmentState.EmergencyStop)
                    {
                        EquipmentState.OperationMode = OperationMode.Manual;
                        OnOperationModeChanged(OperationMode.Forbidden, OperationMode.Manual);
                    }
                }
            }

            ProcessMessage();

            bool autoRun = !(EquipmentState.OperationMode != OperationMode.AutoRun | !EquipmentState.IsAutoRunStarting);

            if (autoRun)
                PollAutomatedScripts();

            ProcessLowPriorityMessage();

            if (EquipmentState.IsAutoRunSuspended && !EquipmentState.IsActive)
            {
                if (EquipmentState.OperationMode == OperationMode.AutoRun)
                {
                    var mode = OperationMode.AutoRun | OperationMode.Manual;

                    EquipmentState.OperationMode = mode;
                    OnOperationModeChanged(OperationMode.AutoRun, mode);
                }
            }

            if (_airPressureTimer.CheckTimeout(Jiffies))
                AirPressureTimer_Expired();

            _alarmWatcher.Poll(this);

            RefreshControllerState();

            List<IRunnable> tasks = _taskQueue;
            if (tasks != null)
            {
                ThreadPool.QueueUserWorkItem((_) =>
                {
                    foreach (var task in tasks)
                    {
                        task.Run();
                    }

                    tasks.Clear();
                    _taskQueuePool.Push(tasks);
                });
            }
            _taskQueue = null;

            EquipmentState.IsUpdateFailed = !await UpdateAsync().ConfigureAwait(false);
        }

        protected virtual void Initialize()
        {

        }

        protected virtual void OnEquipmentReboot()
        {
            if (_bootTimer.IsEnabled)
                return;

            _isFirstScan = false;

            _bootTimer.Start(GetExpirationJiffies(_timeForBoot));

            Log.i(LogKeyword, "The automated equipment restarted");
        }

        private bool CheckIfBootHasBeenCompleted()
        {
            if (!_bootTimer.CheckTimeout(Jiffies))
                return false;

            _bootTimer.Stop();

            Log.i(LogKeyword, "The automated equipment has completed a full startup");

            OnBootCompleted();
            return true;
        }

        protected virtual void OnBootCompleted()
        {
            foreach (var observer in _eventObservers)
            {
                observer.OnReboot(this);
            }

            ForbidOperation();
        }

        private void AirPressureTimer_Expired()
        {
            _airPressureTimer.Stop();

            if (!_hasPositiveAirPressureAbnormal)
            {
                EquipmentState.EmergencyStop = _emergencyStop;
                EquipmentState.HasPositiveAirPressureAbnormal = false;

                Log.i(LogKeyword, "Positive air pressure source already recovered");

                foreach (IMovableObject movableObject in _movableObjects)
                {
                    movableObject.OnPositiveAirPressureSourceChanged(this, false);
                }
            }
        }

        private void StopEmergency()
        {
            foreach (var observer in _eventObservers)
            {
                observer.OnEmergencyStop(this);
            }

            ForbidOperation();
        }

        private void ForbidOperation()
        {
            OperationMode currentMode = EquipmentState.OperationMode;

            if (currentMode != OperationMode.Forbidden)
            {
                EquipmentState.OperationMode = OperationMode.Forbidden;
                EquipmentState.IsAutoRunStarting = false;
                EquipmentState.IsAutoRunSuspended = false;

                foreach (var script in _automatedScripts)
                {
                    script.Reinitialize(this);
                }

                OnOperationModeChanged(currentMode, OperationMode.Forbidden);
            }
        }

        protected virtual void OnOperationModeChanged(OperationMode oldMode, OperationMode newMode)
        {

        }

        protected abstract Task<bool> RefreshAsync();

        private async Task RetryUpdateAsync()
        {
            var result = await CheckIfEquipmentHasRebootAsync().ConfigureAwait(false);
            if (!result.IsSuccess)
            {
                Log.i(LogKeyword, $"CheckIfEquipmentHasRebootAsync: {result}");
                return;
            }

            bool reboot = result.Value;

            if (reboot)
                EquipmentState.IsUpdateFailed = false;
            else
                EquipmentState.IsUpdateFailed = !await UpdateAsync().ConfigureAwait(false);
        }

        protected virtual Task<OperationResult<bool>> CheckIfEquipmentHasRebootAsync()
        {
            return Task.FromResult(OperationResultFactory.CreateSuccessfulResult(false));
        }

        protected virtual Task<bool> UpdateAsync()
        {
            return Task.FromResult(true);
        }

        protected virtual void RefreshControllerState()
        {

        }

        public void AddAutomatedScript(IAutomatedScript script)
        {
            if (script == null)
                throw new ArgumentNullException("script");

            _automatedScripts.AddLast(script);
        }

        public void RemoveAutomatedScript(IAutomatedScript script)
        {
            if (script == null)
                throw new ArgumentNullException("script");

            _automatedScripts.Remove(script);
        }

        public void RemoveAllAutomatedScripts()
        {
            _automatedScripts.Clear();
        }

        private void PollAutomatedScripts()
        {
            var node = _automatedScripts.First;

            while (node != null)
            {
                var next = node.Next;

                node.Value.Poll(this);

                node = next;
            }
        }

        public void RunAutomatedModule(AutomatedModule module)
        {
            ThrowIfStopped();

            if (!EquipmentState.IsAutoRunStarting)
                throw new BugException("It only can be added automated module after operating auto run.");

            if (module == null)
                throw new ArgumentNullException("module");

            LinkedListNode<AutomatedModule> node = module.Node;

            if (node.List != null)
                throw new BugException("This AutomatedModule is already in progress.");

            module.Go(this);
            _automatedModules.AddLast(node);
        }

        public void PollAutomatedModules()
        {
            var node = _automatedModules.First;

            while (node != null)
            {
                var next = node.Next;

                AutomatedModule module = node.Value;

                module.Poll(this);

                if (module.IsCompleted)
                    _automatedModules.Remove(node);

                node = next;
            }
        }

        public void AddTask(IRunnable task)
        {
            if (_taskQueue == null)
            {
                if (!_taskQueuePool.TryPop(out _taskQueue))
                    _taskQueue = new List<IRunnable>();
            }

            _taskQueue.Add(task);
        }

        private void ThrowIfStarted()
        {
#if DEBUG
            if (Interlocked.CompareExchange(ref _status, Stopped, Stopped) == Started)
                throw new InvalidOperationException("Supervisory controller has already been started.");
#endif
        }

        private void ThrowIfStopped()
        {
#if DEBUG
            if (Interlocked.CompareExchange(ref _status, Started, Started) == Stopped)
                throw new InvalidOperationException("Supervisory controller was not started.");
#endif
        }

        private void ThrowIfNotStarted()
        {
#if DEBUG
            int status = Interlocked.CompareExchange(ref _status, Started, Started);

            if (status == Disposed)
                throw new ObjectDisposedException(GetType().FullName);

            if (status == Stopped)
                throw new InvalidOperationException("Supervisory controller was not started.");
#endif
        }
    }
}
