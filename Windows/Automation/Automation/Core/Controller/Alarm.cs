﻿using Automation.Core.Alarm;
using Automation.Core.Alarm.Comment;
using SwissKnife;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automation.Core.Controller
{
    public partial class SupervisoryController
    {
        private readonly Dictionary<string, AlarmRegister> _alarmRegisters = new Dictionary<string, AlarmRegister>(StringComparer.Ordinal);
        private readonly IniParser _emptyIni = new IniParser();
        private IniParser _alarmMessageIni;

        public IniParser AlarmMessageIni
        {
            get { return _alarmMessageIni ?? _emptyIni; }
            set { _alarmMessageIni = value; }
        }

        private void LoadAlarmCommentResource(string resourceFile)
        {
            using (var reader = new StreamReader(resourceFile, Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(AlarmResource));
                var resource = (AlarmResource)serializer.Deserialize(reader);

                resource.CheckAfterDeserialization();

                foreach (var register in resource.Registers)
                {
                    _alarmRegisters.Add(register.Name, register);
                }
            }
        }

        public AlarmReport CreateAlarmReport(string name, string alarmName)
        {
            if (!_alarmRegisters.TryGetValue(name, out AlarmRegister register))
                throw new NotFoundException($"Alarm register '{name}' was not found.");

            if (!register.TryGetComment(alarmName, out AlarmComment comment))
                throw new NotFoundException($"Alarm comment '{alarmName}' ({name}) was not found.");

            string message = AlarmMessageIni.GetValue(name, comment.AlarmName);

            return new AlarmReport(name, comment.AlarmCode, register.Keyword, message);
        }
    }
}
