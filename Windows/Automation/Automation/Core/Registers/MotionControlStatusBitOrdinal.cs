﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Registers
{
    public static class MotionControlStatusBitOrdinal
    {
        public const byte ReturnedHome = 0;
        public const byte Busy = 1;
    }
}
