﻿using SwissKnife;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Registers
{
    public class Register<T> : ICloneable
    {
        public ushort BaseAddress { get; }

        public T[] Buffer { get; private set; }

        public Register(ushort baseAddress, ushort length)
        {
            if (length == 0)
                throw new ArgumentException("Register length cannot be zero.", "length");

            if (baseAddress + length > ushort.MaxValue + 1)
                throw new ArgumentException("The sum of the baseAddress and length arguments must be less than or equal to 65536.");

            BaseAddress = baseAddress;
            Buffer = new T[length];
        }

        public T GetValue(ushort address)
        {
            return Buffer[address - BaseAddress];
        }

        public void SetValue(ushort address, T value)
        {
            Buffer[address - BaseAddress] = value;
        }

        public void CopyTo(Register<T> destination)
        {
            if (destination == null)
                throw new ArgumentNullException("destination");

            Array.Copy(Buffer, destination.Buffer, Buffer.Length);
        }

        public object Clone()
        {
            Register<T> other = (Register<T>)MemberwiseClone();

            other.Buffer = new T[Buffer.Length];

            return other;
        }
    }

    public class UInt16Register : Register<ushort>
    {
        public UInt16Register(ushort baseAddress, ushort length) : base(baseAddress, length) { }

        public bool GetBit(ushort address, byte ordinal)
        {
            return BitOperation.GetBit(GetValue(address), ordinal);
        }

        public bool GetBit(BitCell bitCell)
        {
            return BitOperation.GetBit(GetValue(bitCell.Address), bitCell.Ordinal);
        }

        public void SetBit(ushort address, byte ordinal, bool value)
        {
            int word = BitOperation.SetBit(GetValue(address), ordinal, value);

            SetValue(address, unchecked((ushort)word));
        }

        public void SetBit(BitCell bitCell, bool value)
        {
            ushort address = bitCell.Address;

            int word = BitOperation.SetBit(GetValue(address), bitCell.Ordinal, value);

            SetValue(address, unchecked((ushort)word));
        }
    }

    public class Int32Register : Register<int>
    {
        public Int32Register(ushort baseAddress, ushort length) : base(baseAddress, length) { }

        public bool GetBit(ushort address, byte ordinal)
        {
            return BitOperation.GetBit(GetValue(address), ordinal);
        }

        public bool GetBit(BitCell bitCell)
        {
            return BitOperation.GetBit(GetValue(bitCell.Address), bitCell.Ordinal);
        }

        public void SetBit(ushort address, byte ordinal, bool value)
        {
            SetValue(address, BitOperation.SetBit(GetValue(address), ordinal, value));
        }

        public void SetBit(BitCell bitCell, bool value)
        {
            ushort address = bitCell.Address;

            SetValue(address, BitOperation.SetBit(GetValue(address), bitCell.Ordinal, value));
        }
    }

    public class UInt32Register : Register<uint>
    {
        public UInt32Register(ushort baseAddress, ushort length) : base(baseAddress, length) { }

        public bool GetBit(ushort address, byte ordinal)
        {
            return BitOperation.GetBit(GetValue(address), ordinal);
        }

        public bool GetBit(BitCell bitCell)
        {
            return BitOperation.GetBit(GetValue(bitCell.Address), bitCell.Ordinal);
        }

        public void SetBit(ushort address, byte ordinal, bool value)
        {
            SetValue(address, BitOperation.SetBit(GetValue(address), ordinal, value));
        }

        public void SetBit(BitCell bitCell, bool value)
        {
            ushort address = bitCell.Address;

            SetValue(address, BitOperation.SetBit(GetValue(address), bitCell.Ordinal, value));
        }
    }
}
