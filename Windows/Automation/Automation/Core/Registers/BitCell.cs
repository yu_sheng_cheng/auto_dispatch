﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Registers
{
    public struct BitCell
    {
        public ushort Address { get; }

        public byte Ordinal { get; }

        public BitCell(ushort address, byte ordinal)
        {
            Address = address;
            Ordinal = ordinal;
        }
    }
}
