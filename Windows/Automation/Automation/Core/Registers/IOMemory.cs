﻿using SwissKnife;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Registers
{
    public sealed class IOMemory : ICloneable
    {
        private readonly Tuple<ushort, ushort>[] _physicalBlocks;

        public IOSignalType Type { get; }

        public ushort VirtualStartAddress { get; }

        public ushort[] Registers { get; private set; }

        public IOMemory(IOSignalType type, ushort physicalStartAddress, ushort virtualStartAddress, ushort count) :
            this(type, new Tuple<ushort, ushort>[] { new Tuple<ushort, ushort>(physicalStartAddress, count) }, virtualStartAddress)
        { }

        public IOMemory(IOSignalType type, Tuple<ushort, ushort>[] physicalBlocks, ushort virtualStartAddress)
        {
            if (physicalBlocks == null)
                throw new ArgumentNullException("physicalBlocks");

            if (physicalBlocks.Length == 0)
                throw new ArgumentException("The physicalBlocks argument is a zero-length array.", "physicalBlocks");

            Type = type;
            _physicalBlocks = physicalBlocks.ToArray();

            Array.Sort(_physicalBlocks, (x, y) =>
            {
                return x.Item1 - y.Item1;
            });

            int length = 0;

            foreach (var block in _physicalBlocks)
            {
                ushort physicalAddress = block.Item1;
                ushort count = block.Item2;

                if (count == 0)
                    throw new ArgumentException("Physical block length cannot be zero.", "physicalBlocks");

                if (physicalAddress + count > ushort.MaxValue + 1)
                    throw new ArgumentException("The sum of the physicalAddress and count arguments must be less than or equal to 65536.");

                if (type == IOSignalType.Digital)
                {
                    if (!IsAlignedOn16Boundary(physicalAddress))
                        throw new ArgumentException("The physical start address must be aligned on 16 boundary.", "physicalBlocks");

                    if (!IsAlignedOn16Boundary(count))
                        throw new ArgumentException("The physical block length must be a multiple of 16.", "physicalBlocks");
                }

                length += count;
            }

            if (length > ushort.MaxValue)
                throw new ArgumentException("The physical register count must be less than 65536.");

            if (type == IOSignalType.Digital)
                length /= 16;

            if (virtualStartAddress + length > ushort.MaxValue + 1)
                throw new ArgumentException("The sum of the virtualStartAddress and length arguments must be less than or equal to 65536.");

            VirtualStartAddress = virtualStartAddress;
            Registers = new ushort[length];
        }

        private bool IsAlignedOn16Boundary(ushort value)
        {
            return (value & 15) == 0;
        }

        public bool GetBit(ushort virtualAddress, byte ordinal)
        {
            return BitOperation.GetBit(GetValue(virtualAddress), ordinal);
        }

        public bool GetBit(BitCell bitCell)
        {
            return BitOperation.GetBit(GetValue(bitCell.Address), bitCell.Ordinal);
        }

        public ushort GetValue(ushort virtualAddress)
        {
            return Registers[virtualAddress - VirtualStartAddress];
        }

        public void SetBit(ushort virtualAddress, byte ordinal, bool value)
        {
            int index = virtualAddress - VirtualStartAddress;

            int newValue = BitOperation.SetBit(Registers[index], ordinal, value);

            Registers[index] = unchecked((ushort)newValue);
        }

        public void SetBit(BitCell bitCell, bool value)
        {
            int index = bitCell.Address - VirtualStartAddress;

            int newValue = BitOperation.SetBit(Registers[index], bitCell.Ordinal, value);

            Registers[index] = unchecked((ushort)newValue);
        }

        public void SetValue(ushort virtualAddress, ushort value)
        {
            Registers[virtualAddress - VirtualStartAddress] = value;
        }

        public bool PhysToVirt(ushort physicalAddress, out ushort virtualAddress)
        {
            ushort virtualOffset = 0;

            virtualAddress = 0;

            byte strideOrderPerWord = Type == IOSignalType.Digital ? (byte)4 : (byte)0;

            foreach (var block in _physicalBlocks)
            {
                ushort address = block.Item1;
                ushort count = block.Item2;

                if (physicalAddress < address)
                    return false;

                if (physicalAddress < address + count)
                {
                    virtualAddress = (ushort)(VirtualStartAddress + virtualOffset + ((physicalAddress - address) >> strideOrderPerWord));
                    return true;
                }

                virtualOffset += (ushort)(count >> strideOrderPerWord);
            }
            return false;
        }

        public bool IsAccessRangeValid(ushort physicalAddress, ushort length)
        {
            foreach (var block in _physicalBlocks)
            {
                ushort address = block.Item1;
                ushort count = block.Item2;

                int end = address + count;

                if (physicalAddress < address | physicalAddress >= end | length > end - physicalAddress)
                    continue;

                return true;
            }
            return false;
        }

        public object Clone()
        {
            IOMemory other = (IOMemory)MemberwiseClone();

            other.Registers = new ushort[Registers.Length];

            return other;
        }
    }
}
