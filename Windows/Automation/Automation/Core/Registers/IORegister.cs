﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Registers
{
    public sealed class IORegister : ICloneable
    {
        private IOMemory _contactIOMemory;
        private IOMemory _coilIOMemory;
        private IOMemory _analogInputIOMemory;
        private IOMemory _analogOutputIOMemory;

        public ushort[] ContactRegisters { get { return _contactIOMemory?.Registers; } }

        public ushort[] CoilRegisters { get { return _coilIOMemory?.Registers; } }

        public ushort[] AnalogInputRegisters { get { return _analogInputIOMemory?.Registers; } }

        public ushort[] AnalogOutputRegisters { get { return _analogOutputIOMemory?.Registers; } }

        private IORegister() { }

        public IORegister(IOMemory contactIOMemory, IOMemory coilIOMemory, IOMemory analogInputIOMemory, IOMemory analogOutputIOMemory)
        {
            _contactIOMemory = contactIOMemory;
            _coilIOMemory = coilIOMemory;
            _analogInputIOMemory = analogInputIOMemory;
            _analogOutputIOMemory = analogOutputIOMemory;
        }

        public ushort GetContacts(ushort address)
        {
            return _contactIOMemory?.GetValue(address) ?? 0;
        }

        public bool GetContact(BitCell bitCell)
        {
            return _contactIOMemory?.GetBit(bitCell) ?? false;
        }

        public void NotContact(BitCell bitCell)
        {
            if (_contactIOMemory == null)
                return;

            _contactIOMemory.SetBit(bitCell, !_contactIOMemory.GetBit(bitCell));
        }

        public ushort GetCoils(ushort address)
        {
            return _coilIOMemory?.GetValue(address) ?? 0;
        }

        public bool GetCoil(BitCell bitCell)
        {
            return _coilIOMemory?.GetBit(bitCell) ?? false;
        }

        public void SetCoil(BitCell bitCell, bool value)
        {
            _coilIOMemory?.SetBit(bitCell, value);
        }

        public ushort GetAnalogInput(ushort address)
        {
            return _analogInputIOMemory?.GetValue(address) ?? 0;
        }

        public ushort GetAnalogOutput(ushort address)
        {
            return _analogOutputIOMemory?.GetValue(address) ?? 0;
        }

        public void SetAnalogOutput(ushort address, ushort value)
        {
            _analogOutputIOMemory?.SetValue(address, value);
        }

        public void CopyTo(IORegister destination)
        {
            if (destination == null)
                throw new ArgumentNullException("destination");

            if (_contactIOMemory != null)
            {
                ushort[] registers = _contactIOMemory.Registers;

                Array.Copy(registers, destination._contactIOMemory.Registers, registers.Length);
            }

            if (_coilIOMemory != null)
            {
                ushort[] registers = _coilIOMemory.Registers;

                Array.Copy(registers, destination._coilIOMemory.Registers, registers.Length);
            }

            if (_analogInputIOMemory != null)
            {
                ushort[] registers = _analogInputIOMemory.Registers;

                Array.Copy(registers, destination._analogInputIOMemory.Registers, registers.Length);
            }

            if (_analogOutputIOMemory != null)
            {
                ushort[] registers = _analogOutputIOMemory.Registers;

                Array.Copy(registers, destination._analogOutputIOMemory.Registers, registers.Length);
            }
        }

        public object Clone()
        {
            IORegister other = new IORegister();

            if (_contactIOMemory != null)
                other._contactIOMemory = (IOMemory)_contactIOMemory.Clone();

            if (_coilIOMemory != null)
                other._coilIOMemory = (IOMemory)_coilIOMemory.Clone();

            if (_analogInputIOMemory != null)
                other._analogInputIOMemory = (IOMemory)_analogInputIOMemory.Clone();

            if (_analogOutputIOMemory != null)
                other._analogOutputIOMemory = (IOMemory)_analogOutputIOMemory.Clone();

            return other;
        }
    }
}
