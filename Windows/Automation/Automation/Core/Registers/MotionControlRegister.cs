﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Registers
{
    public sealed class MotionControlRegister : ICloneable
    {
        private readonly ushort _motionControlCount;
        private UInt16Register _status;
        private Int32Register _currentFeedValue;
        private Int32Register _currentTorque;
        private UInt32Register _currentPositionId;

        public MotionControlRegister(ushort motionControlCount)
        {
            if (motionControlCount == 0)
                throw new ArgumentException("The 'motionControlCount' argument must be greater than zero.", "motionControlCount");

            _motionControlCount = motionControlCount;

            _status = new UInt16Register(1, motionControlCount);
            _currentFeedValue = new Int32Register(1, motionControlCount);
            _currentTorque = new Int32Register(1, motionControlCount);
            _currentPositionId = new UInt32Register(1, motionControlCount);
        }

        public ushort GetStatus(ushort axisId)
        {
            return _status.GetValue(axisId);
        }

        public void SetStatus(ushort axisId, ushort value)
        {
            _status.SetValue(axisId, value);
        }

        public int GetCurrentFeedValue(ushort axisId)
        {
            return _currentFeedValue.GetValue(axisId);
        }

        public void SetCurrentFeedValue(ushort axisId, int value)
        {
            _currentFeedValue.SetValue(axisId, value);
        }

        public int GetCurrentTorque(ushort axisId)
        {
            return _currentTorque.GetValue(axisId);
        }

        public void SetCurrentTorque(ushort axisId, int value)
        {
            _currentTorque.SetValue(axisId, value);
        }

        public uint GetCurrentPositionId(ushort axisId)
        {
            return _currentPositionId.GetValue(axisId);
        }

        public void SetCurrentPositionId(ushort axisId, uint value)
        {
            _currentPositionId.SetValue(axisId, value);
        }

        public void CopyTo(MotionControlRegister destination)
        {
            if (destination == null)
                throw new ArgumentNullException("destination");

            _status.CopyTo(destination._status);
            _currentFeedValue.CopyTo(destination._currentFeedValue);
            _currentTorque.CopyTo(destination._currentTorque);
            _currentPositionId.CopyTo(destination._currentPositionId);
        }

        public object Clone()
        {
            MotionControlRegister other = (MotionControlRegister)MemberwiseClone();

            other._status = (UInt16Register)_status.Clone();
            other._currentFeedValue = (Int32Register)_currentFeedValue.Clone();
            other._currentTorque = (Int32Register)_currentTorque.Clone();
            other._currentPositionId = (UInt32Register)_currentPositionId.Clone();

            return other;
        }
    }
}
