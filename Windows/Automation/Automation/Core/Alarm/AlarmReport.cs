﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Alarm
{
    [Serializable]
    public class AlarmReport
    {
        public string Name { get; }

        public DateTime Time { get; set; }

        public ushort AlarmCode { get; }

        public ushort LowLevelErrorCode { get; set; }

        public string Keyword { get; }

        public string Message { get; set; }

        public AlarmReport(string name, ushort code, string keyword, string message)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            if (string.IsNullOrEmpty(keyword))
                throw new ArgumentException("String cannot be empty or null.", "keyword");

            if (string.IsNullOrEmpty(message))
                throw new ArgumentException("String cannot be empty or null.", "message");

            Name = name;
            AlarmCode = code;
            Keyword = keyword;
            Message = message;
        }

        public override string ToString()
        {
            return $"{AlarmCode.ToString("X3")} <{Keyword}> {Message}";
        }
    }
}
