﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Alarm
{
    public interface IAlarmNotification
    {
        event EventHandler<AlarmChangedEventArgs> AlarmChanged;
    }
}
