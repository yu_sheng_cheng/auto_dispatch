﻿using Automation.Core.Controller;
using Automation.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Alarm
{
    public interface IAlarmSource : INamedObject, IPollable
    {
        bool AlarmOccurred { get; }

        ushort ErrorCode { get; }

        AlarmReport GetAlarmReport(SupervisoryController controller);

        void Reset(SupervisoryController controller);
    }
}
