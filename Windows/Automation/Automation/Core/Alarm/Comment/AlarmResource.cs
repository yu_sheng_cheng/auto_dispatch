﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automation.Core.Alarm.Comment
{
    [XmlRoot(ElementName = "AlarmResource", Namespace = "Automation.Core.Alarm.Comment.AlarmResource.xml", IsNullable = false)]
    public class AlarmResource
    {
        [XmlArrayItem(ElementName = "Register")] public AlarmRegister[] Registers { get; set; }

        public void CheckAfterDeserialization()
        {
            if (Registers == null)
                Registers = new AlarmRegister[0];
            else
            {
                foreach (var register in Registers)
                {
                    register.CheckAfterDeserialization();
                }
            }
        }
    }
}
