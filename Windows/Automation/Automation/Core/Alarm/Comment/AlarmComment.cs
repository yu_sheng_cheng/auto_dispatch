﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automation.Core.Alarm.Comment
{
    public class AlarmComment
    {
        [XmlAttribute] public string AlarmName { get; set; }
        [XmlAttribute] public ushort AlarmCode { get; set; }

        public void CheckAfterDeserialization()
        {
            if (string.IsNullOrEmpty(AlarmName))
                throw new FormatException("The 'AlarmName' attribute cannot be empty or null.");

            if (AlarmCode > 4095)
                throw new FormatException("The 'AlarmCode' attribute must be less than 4096.");
        }
    }
}
