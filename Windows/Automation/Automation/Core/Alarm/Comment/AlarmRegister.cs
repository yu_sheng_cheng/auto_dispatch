﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Automation.Core.Alarm.Comment
{
    public class AlarmRegister
    {
        private Dictionary<string, AlarmComment> _dictionary;

        [XmlAttribute] public string Name { get; set; }
        [XmlAttribute] public string Keyword { get; set; }
        [XmlArrayItem(ElementName = "Comment")] public AlarmComment[] Comments { get; set; }

        public void CheckAfterDeserialization()
        {
            if (string.IsNullOrEmpty(Name))
                throw new FormatException("The 'Name' attribute cannot be empty or null.");

            if (string.IsNullOrEmpty(Keyword))
                throw new FormatException("The 'Keyword' attribute cannot be empty or null.");

            if (Comments == null)
            {
                Comments = new AlarmComment[0];
                return;
            }

            foreach (var comment in Comments)
            {
                comment.CheckAfterDeserialization();
            }

            if (Comments.Length >= 7)
            {
                _dictionary = new Dictionary<string, AlarmComment>(StringComparer.Ordinal);

                foreach (var comment in Comments)
                {
                    _dictionary.Add(comment.AlarmName, comment);
                }
            }
        }

        public bool TryGetComment(string alarmName, out AlarmComment comment)
        {
            if (_dictionary == null)
            {
                if (alarmName == null)
                    throw new ArgumentNullException("alarmName");

                foreach (var c in Comments)
                {
                    if (alarmName == c.AlarmName)
                    {
                        comment = c;
                        return true;
                    }
                }

                comment = null;
                return false;
            }
            else
                return _dictionary.TryGetValue(alarmName, out comment);
        }
    }
}
