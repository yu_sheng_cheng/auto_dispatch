﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Alarm
{
    [Serializable]
    public class AlarmEntry
    {
        public DateTime OccurTime { get; }

        public DateTime RestoreTime { get; }

        public ushort Code { get; }

        public string Keyword { get; }

        public string Message { get; }

        public AlarmEntry(AlarmReport report, DateTime restoreTime)
        {
            OccurTime = report.Time;
            RestoreTime = restoreTime;
            Code = report.AlarmCode;
            Keyword = report.Keyword;
            Message = report.Message;
        }
    }
}
