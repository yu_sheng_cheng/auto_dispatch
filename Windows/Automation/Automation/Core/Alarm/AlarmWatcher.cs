﻿using Automation.Core.Controller;
using Automation.Core.Interfaces;
using SwissKnife.Exceptions;
using SwissKnife.Queues;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automation.Core.Alarm
{
    public interface IAlarmWatcher : IAlarmNotification, IEnumerable<ushort>
    {
        bool AlarmOccurred { get; }

        void AddAutoRunError(SupervisoryController controller, AlarmReport report);

        void ClearAutoRunError(ushort code);

        bool Exists(ushort code);
    }

    public sealed class AlarmWatcher : IAlarmWatcher, IPollable, IDisposable
    {
        private readonly IDictionary<string, IAlarmSource> _alarmSources = new Dictionary<string, IAlarmSource>(StringComparer.Ordinal);
        private readonly IDictionary<string, AlarmReport> _alarmsFromAlarmSource = new Dictionary<string, AlarmReport>(StringComparer.Ordinal);
        private readonly List<AlarmReport> _autoRunErrors = new List<AlarmReport>();
        private readonly List<AlarmReport> _newOccurredAutoRunErrors = new List<AlarmReport>();
        private readonly List<AlarmEntry> _clearedAutoRunErrors = new List<AlarmEntry>();
        private readonly BitArray _alarmBitArray = new BitArray(4096);
        private readonly OneReadOneWriteQueue<AlarmChangedEventArgs> _queue = new OneReadOneWriteQueue<AlarmChangedEventArgs>();
        private readonly SemaphoreSlim _enqueued = new SemaphoreSlim(0);
        private readonly CancellationTokenSource _cts = new CancellationTokenSource();
        private bool _started;

        public bool AlarmOccurred
        {
            get
            {
                return _alarmsFromAlarmSource.Count != 0 | _autoRunErrors.Count != 0 | _newOccurredAutoRunErrors.Count != 0 | _clearedAutoRunErrors.Count != 0;
            }
        }

        public event EventHandler<AlarmChangedEventArgs> AlarmChanged;

        public void Dispose()
        {
            _cts.Cancel();

            SpinWait spinwait = new SpinWait();
            while (Volatile.Read(ref _started))
            {
                spinwait.SpinOnce();
            }

            _cts.Dispose();
            _enqueued.Dispose();
        }

        public void AddAlarmSource(IAlarmSource alarmSource)
        {
            if (alarmSource == null)
                throw new ArgumentNullException("alarmSource");

            string name = alarmSource.Name;

            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("The 'Name' property of the IAlarmSource cannot be empty or null.");

            _alarmSources.Add(name, alarmSource);
        }

        public void Start()
        {
            _started = true;

            Task.Run(async () =>
            {
                try
                {
                    while (true)
                    {
                        await _enqueued.WaitAsync(_cts.Token).ConfigureAwait(false);

                        var e = _queue.Dequeue();

                        RaiseAlarmChangedEvent(e);
                    }
                }
                catch (OperationCanceledException)
                {

                }
                finally
                {
                    _started = false;
                }
            });
        }

        private void RaiseAlarmChangedEvent(AlarmChangedEventArgs e)
        {
            EventHandler<AlarmChangedEventArgs> temp = Volatile.Read(ref AlarmChanged);
            temp?.Invoke(this, e);
        }

        public void Reset(SupervisoryController controller)
        {
            foreach (var alarm in _alarmsFromAlarmSource)
            {
                string name = alarm.Key;

                _alarmSources[name].Reset(controller);
            }

            if (_autoRunErrors.Count != 0)
            {
                DateTime now = DateTime.Now;

                foreach (var report in _autoRunErrors)
                {
                    _clearedAutoRunErrors.Add(new AlarmEntry(report, now));
                    _alarmBitArray[report.AlarmCode] = false;
                }

                _autoRunErrors.Clear();
            }
        }

        public void Reset(SupervisoryController controller, string alarmSourceName)
        {
            if (_alarmsFromAlarmSource.TryGetValue(alarmSourceName, out AlarmReport report))
                _alarmSources[alarmSourceName].Reset(controller);
        }

        public void Reset(SupervisoryController controller, ushort code)
        {
            ClearAutoRunError(code);

            if (!_alarmBitArray[code])
                return;

            foreach (var alarm in _alarmsFromAlarmSource)
            {
                AlarmReport report = alarm.Value;

                if (code == report.AlarmCode)
                {
                    _alarmSources[report.Name].Reset(controller);
                    return;
                }
            }
        }

        public void AddAutoRunError(SupervisoryController controller, AlarmReport report)
        {
#if DEBUG
            if (_clearedAutoRunErrors.Count != 0)
                throw new BugException("The 'AddAutoRunError' method can't be called under the manual mode.");
#endif
            if (report == null)
                throw new ArgumentNullException("report");

            ushort code = report.AlarmCode;

            if (_alarmBitArray[code])
                return;

            report.Time = DateTime.Now;

            _newOccurredAutoRunErrors.Add(report);
            _alarmBitArray[code] = true;

            controller.EquipmentState.NeedsSuspendAutoRun();
        }

        public void ClearAutoRunError(ushort code)
        {
            if (!_alarmBitArray[code])
                return;

            int count = _autoRunErrors.Count;

            for (int i = 0; i < count; ++i)
            {
                AlarmReport report = _autoRunErrors[i];

                if (code == report.AlarmCode)
                {
                    _clearedAutoRunErrors.Add(new AlarmEntry(report, DateTime.Now));
                    _autoRunErrors.RemoveAt(i);
                    _alarmBitArray[code] = false;
                    return;
                }
            }
        }

        public void Poll(SupervisoryController controller)
        {
            List<AlarmReport> alarmReports = null;
            List<AlarmEntry> resolvedAlarms = null;
            DateTime now = DateTime.MinValue;

            if (_newOccurredAutoRunErrors.Count != 0)
            {
                _autoRunErrors.AddRange(_newOccurredAutoRunErrors);

                alarmReports = new List<AlarmReport>();
                alarmReports.AddRange(_newOccurredAutoRunErrors);
                _newOccurredAutoRunErrors.Clear();
            }

            if (_clearedAutoRunErrors.Count != 0)
            {
                resolvedAlarms = new List<AlarmEntry>();
                resolvedAlarms.AddRange(_clearedAutoRunErrors);
                _clearedAutoRunErrors.Clear();
            }

            foreach (var item in _alarmSources)
            {
                string name = item.Key;
                IAlarmSource alarmSource = item.Value;

                _alarmsFromAlarmSource.TryGetValue(name, out AlarmReport report);

                if (report != null)
                {
                    if (alarmSource.AlarmOccurred && alarmSource.ErrorCode == report.LowLevelErrorCode)
                        continue;

                    _alarmsFromAlarmSource.Remove(name);
                    _alarmBitArray[report.AlarmCode] = false;

                    if (now == DateTime.MinValue)
                        now = DateTime.Now;

                    if (resolvedAlarms == null)
                        resolvedAlarms = new List<AlarmEntry>();

                    resolvedAlarms.Add(new AlarmEntry(report, now));
                }

                if (alarmSource.AlarmOccurred)
                {
                    if (now == DateTime.MinValue)
                        now = DateTime.Now;

                    report = alarmSource.GetAlarmReport(controller);
                    report.Time = now;
                    report.LowLevelErrorCode = alarmSource.ErrorCode;

                    _alarmsFromAlarmSource.Add(name, report);
                    _alarmBitArray[report.AlarmCode] = true;

                    if (alarmReports == null)
                        alarmReports = new List<AlarmReport>();

                    alarmReports.Add(report);
                }
            }

            if (alarmReports != null | resolvedAlarms != null)
            {
                _queue.Enqueue(new AlarmChangedEventArgs(alarmReports, resolvedAlarms));
                _enqueued.Release();
            }
        }

        public IEnumerator<ushort> GetEnumerator()
        {
            foreach (var alarm in _alarmsFromAlarmSource)
            {
                yield return alarm.Value.AlarmCode;
            }

            foreach (var report in _autoRunErrors)
            {
                yield return report.AlarmCode;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool Exists(ushort code)
        {
            return _alarmBitArray[code];
        }
    }
}
