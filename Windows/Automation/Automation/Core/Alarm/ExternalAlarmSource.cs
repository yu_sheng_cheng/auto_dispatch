﻿using Automation.Core.Controller;
using Automation.Core.Registers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Alarm
{
    public sealed class ExternalAlarmSource : IAlarmSource
    {
        private readonly BitCell _input;
        private bool _tryClearAlarm;

        public string Name { get; }

        public bool AlarmOccurred { get; private set; }

        public ushort ErrorCode { get { return AlarmOccurred ? (ushort)0x9487 : (ushort)0; } }

        public ExternalAlarmSource(string name, BitCell input)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            Name = name;
            _input = input;
        }

        public void Poll(SupervisoryController controller)
        {
            if (AlarmOccurred && !_tryClearAlarm)
                return;

            AlarmOccurred = controller.IORegister.GetContact(_input);
            _tryClearAlarm = false;
        }

        public void Reset(SupervisoryController controller)
        {
            _tryClearAlarm = AlarmOccurred;
        }

        public AlarmReport GetAlarmReport(SupervisoryController controller)
        {
            if (!AlarmOccurred)
                throw new InvalidOperationException("'GetAlarmReport' method only can be called when alarming.");

            return controller.CreateAlarmReport(Name, "Error");
        }
    }
}
