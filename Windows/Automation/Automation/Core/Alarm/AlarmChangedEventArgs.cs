﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Alarm
{
    [Serializable]
    public class AlarmChangedEventArgs : EventArgs
    {
        public IEnumerable<AlarmReport> AlarmReports { get; }

        public IEnumerable<AlarmEntry> ResolvedAlarms { get; }

        public AlarmChangedEventArgs(IEnumerable<AlarmReport> alarmReports, IEnumerable<AlarmEntry> resolvedAlarms)
        {
            if (alarmReports == null && resolvedAlarms == null)
                throw new ArgumentException("'alarmReports' and 'resolvedAlarms' arguments cannot both be null.");

            AlarmReports = alarmReports ?? new AlarmReport[0];
            ResolvedAlarms = resolvedAlarms ?? new AlarmEntry[0];
        }
    }
}
