﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages
{
    public enum MessageClass : byte
    {
        SupervisoryController = 0,
        Other = 32
    }
}
