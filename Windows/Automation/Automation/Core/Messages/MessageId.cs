﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages
{
    internal static class MessageId
    {
        public const ushort KeepAlive = 0;
        public const ushort SafetyDoor = 1;
        public const ushort ClearAlarm = 2;
        public const ushort ManualControl = 3;
        public const ushort IO = 4;
    }
}
