﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages
{
    public sealed class ClearAlarmMessage : Message
    {
        public bool IsClearAll { get; }

        public string AlarmSourceName { get; } = string.Empty;

        public ushort AlarmCode { get; }

        public ClearAlarmMessage() : base((byte)MessageClass.SupervisoryController, MessageId.ClearAlarm)
        {
            IsClearAll = true;
        }

        public ClearAlarmMessage(string alarmSourceName) : base((byte)MessageClass.SupervisoryController, MessageId.ClearAlarm)
        {
            if (string.IsNullOrEmpty(alarmSourceName))
                throw new ArgumentException("String cannot be empty or null.", "alarmSourceName");

            AlarmSourceName = alarmSourceName;
        }

        public ClearAlarmMessage(ushort alarmCode) : base((byte)MessageClass.SupervisoryController, MessageId.ClearAlarm)
        {
            AlarmCode = alarmCode;
        }
    }
}
