﻿using Automation.Core.Registers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages
{
    public sealed class IOMessage : Message
    {
        public IOSignalType Type { get; }

        public BitCell BitCell { get; }

        public bool BitValue { get; }

        public IOMessage(BitCell bitCell, bool value) : base((byte)MessageClass.SupervisoryController, MessageId.IO)
        {
            Type = IOSignalType.Digital;

            BitCell = bitCell;
            BitValue = value;
        }
    }
}
