﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages
{
    public enum SafetyDoorCommand
    {
        LockAll,
        UnlockAll,
        Lock,
        Unlock
    }

    public sealed class SafetyDoorMessage : Message
    {
        public SafetyDoorCommand Command { get; }

        public string SafetyDoorName { get; }

        public SafetyDoorMessage(bool isLock) : base((byte)MessageClass.SupervisoryController, MessageId.SafetyDoor)
        {
            Command = isLock ? SafetyDoorCommand.LockAll : SafetyDoorCommand.UnlockAll;
        }

        public SafetyDoorMessage(bool isLock, string doorName) : base((byte)MessageClass.SupervisoryController, MessageId.SafetyDoor)
        {
            if (string.IsNullOrEmpty(doorName))
                throw new ArgumentException("String cannot be empty or null.", "doorName");

            SafetyDoorName = doorName;
            Command = isLock ? SafetyDoorCommand.Lock : SafetyDoorCommand.Unlock;
        }
    }
}
