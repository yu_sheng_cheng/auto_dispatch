﻿using Automation.Core.Controller;
using Automation.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages
{
    public class Message : IRunnable
    {
        public SupervisoryController Controller { get; internal set; }

        public MessagePriority Priority { get; }

        public byte Class { get; }

        public ushort Id { get; }

        public object Content { get; }

        public object Token { get; set; }

        public object Tag { get; set; }

        public Action<Message> Completed { get; set; }

        public bool IsSuccess { get; set; }

        public Message(byte messageClass, ushort id, MessagePriority priority = MessagePriority.Low) : this(messageClass, id, null, priority) { }

        public Message(byte messageClass, ushort id, object content, MessagePriority priority = MessagePriority.Low)
        {
            Priority = priority;
            Class = messageClass;
            Id = id;
            Content = content;
        }

        public void Complete(bool success = true)
        {
            IsSuccess = success;

            if (Completed != null)
                Controller.AddTask(this);

            Controller = null;
        }

        public void Run()
        {
            var completed = Completed;
            Completed = null;
            completed.Invoke(this);
        }
    }
}
