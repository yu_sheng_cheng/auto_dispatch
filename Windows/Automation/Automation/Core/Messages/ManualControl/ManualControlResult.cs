﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages.ManualControl
{
    public enum ManualControlResult : byte
    {
        Success,
        NotInManualMode,
        NotExist,
        SafetyDoorNotLocked,
        SingleActionDisabled,
        OneCycleDisabled,
        CommandNotSupported,
        UnknownCommand,
        InvalidArgument,
        NotReady,
        Busy,
        Prohibited,
        AlarmOccurred,
        EmergencyStop,
        Reboot,
        ForbiddenOperating,
        Timeout
    }

    public static class ManualControlResultExtensions
    {
        public static string GetMessage(this ManualControlResult result)
        {
            switch (result)
            {
                case ManualControlResult.Success:
                    return SR.Success;
                case ManualControlResult.NotInManualMode:
                    return SR.NotInManualMode;
                case ManualControlResult.NotExist:
                    return SR.NotExist;
                case ManualControlResult.SafetyDoorNotLocked:
                    return SR.SafetyDoorNotLocked;
                case ManualControlResult.CommandNotSupported:
                    return SR.CommandNotSupported;
                case ManualControlResult.UnknownCommand:
                    return SR.UnknownCommand;
                case ManualControlResult.InvalidArgument:
                    return SR.InvalidArgument;
                case ManualControlResult.NotReady:
                    return SR.NotReady;
                case ManualControlResult.Busy:
                    return SR.Busy;
                case ManualControlResult.Prohibited:
                    return SR.Prohibited;
                case ManualControlResult.AlarmOccurred:
                    return SR.AlarmOccurred;
                case ManualControlResult.EmergencyStop:
                    return SR.EmergencyStop;
                case ManualControlResult.Reboot:
                    return SR.Reboot;
                case ManualControlResult.ForbiddenOperating:
                    return SR.ForbiddenOperating;
                case ManualControlResult.Timeout:
                    return SR.Timeout;
                default:
                    return string.Empty;
            }
        }
    }
}
