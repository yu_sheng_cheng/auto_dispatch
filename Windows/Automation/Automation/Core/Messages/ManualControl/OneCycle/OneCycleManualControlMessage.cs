﻿using Automation.Core.Controller;
using Automation.Core.FSM;
using Automation.Core.Interfaces;
using Automation.Enums;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages.ManualControl.OneCycle
{
    public abstract class OneCycleManualControlMessage : ManualControlMessage, IPollable
    {
        public abstract bool IsCompleted { get; }

        public string ObjectName { get; set; } = string.Empty;

        protected OneCycleManualControlMessage(string actionName) : base(ManualControlType.OneCycle, actionName) { }

        /// <summary>
        /// 每次都會判斷director的action是否執行成功, 如果direct的action x回傳false, 其方法也該實作回傳false
        /// </summary>
        /// <param name="controller"></param>
        /// <returns>
        /// direct Y的action X回傳的回傳值, 當return 是false時, OneCycle將不會被繼續執行
        /// </returns>
        public abstract bool Go(SupervisoryController controller);

        public abstract void Poll(SupervisoryController controller);

        protected void Complete(ContinuousActionResult result)
        {
            if (result == null)
                throw new ArgumentNullException("result");

            bool success = result.IsSuccess;

            if (!success)
            {
                ObjectName = result.ObjectName;

                AcceptResult acceptResult = result.AcceptResult;

                if (acceptResult != AcceptResult.Accepted)
                {
                    Result = acceptResult.ToManualControlResult();

                    if (acceptResult == AcceptResult.Prohibited)
                        ProhibitionCode = result.ProhibitionCode;
                }
                else
                {
                    CompletionResult completionResult = result.CompletionResult;
#if DEBUG
                    if (completionResult == CompletionResult.Success)
                        throw new BugException("Should not go in here.");
#endif
                    Result = completionResult.ToManualControlResult();
                }
            }

            Complete(success);
        }
    }
}
