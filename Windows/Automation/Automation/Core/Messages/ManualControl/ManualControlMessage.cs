﻿using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages.ManualControl
{
    public abstract class ManualControlMessage : Message
    {
        public ManualControlType Type { get; }

        public string Name { get; }

        public ManualControlResult Result { get; set; } = ManualControlResult.Success;

        public ushort ProhibitionCode { get; set; }

        protected ManualControlMessage(ManualControlType type, string name) : base((byte)MessageClass.SupervisoryController, MessageId.ManualControl)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            Type = type;
            Name = name;
        }
    }
}
