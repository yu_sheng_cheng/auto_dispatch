﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages.ManualControl
{
    public enum ManualControlType : byte
    {
        MovableObject,
        Peripheral,
        OneCycle
    }
}
