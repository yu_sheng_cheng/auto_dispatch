﻿using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages.ManualControl.Movable
{
    public abstract class MovableManualControlMessage : ManualControlMessage
    {
        protected MovableManualControlMessage(string objectName) : base(ManualControlType.MovableObject, objectName) { }

        internal void OnCompleted(SupervisoryController controller, INamedObject namedObject, CompletionResult result)
        {
            bool success = result == CompletionResult.Success;

            Result = result.ToManualControlResult();

            Complete(success);
        }
    }
}
