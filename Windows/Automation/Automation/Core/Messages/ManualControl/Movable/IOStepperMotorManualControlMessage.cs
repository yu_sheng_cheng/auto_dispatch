﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages.ManualControl.Movable
{
    public sealed class IOStepperMotorManualControlMessage : MovableManualControlMessage
    {
        public bool IsRun { get; }

        public string ScriptName { get; }

        public IOStepperMotorManualControlMessage(string stepperMotorName) : base(stepperMotorName)
        {
            IsRun = false;
        }

        public IOStepperMotorManualControlMessage(string stepperMotorName, string scriptName) : base(stepperMotorName)
        {
            if (string.IsNullOrEmpty(scriptName))
                throw new ArgumentException("String cannot be empty or null.", "scriptName");

            IsRun = true;
            ScriptName = scriptName;
        }
    }
}
