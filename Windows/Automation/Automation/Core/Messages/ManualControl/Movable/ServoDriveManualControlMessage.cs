﻿using Automation.Objects.Movable;
using Automation.Profiles.ServoDrive;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages.ManualControl.Movable
{
    public enum ServoDriveManualControlCommand : byte
    {
        ServoOn,
        ServoOff,
        AxisToStop,
        JogForward,
        JogReverse,
        StopJog,
        ReturnToHomePosition,
        Go,
        QueryParameters,
        UpdateParameters,
        UpdatePosition,
    }

    public sealed class ServoDriveManualControlMessage : MovableManualControlMessage
    {
        public ServoDriveManualControlCommand Command { get; private set; }

        public int JogSpeed { get; private set; }

        public string Position { get; private set; }

        public int PositioningValue { get; private set; }

        public Point Point { get; private set; }

        public ServoDriveParameters Parameters { get; private set; }

        public ServoPosition ServoPosition { get; private set; }

        public ServoPoint ServoPoint { get; private set; }

        public ServoDriveManualControlMessage(string servoDriveName) : base(servoDriveName) { }

        public void TurnServoOn()
        {
            Command = ServoDriveManualControlCommand.ServoOn;
        }

        public void TurnServoOff()
        {
            Command = ServoDriveManualControlCommand.ServoOff;
        }

        public void StopAxis()
        {
            Command = ServoDriveManualControlCommand.AxisToStop;
        }

        public void StartJog(int speed)
        {
            if (speed == 0)
                throw new ArgumentException("The speed argument cannot be zero.", "speed");

            if (speed > 0)
            {
                JogSpeed = speed;
                Command = ServoDriveManualControlCommand.JogForward;
            }
            else
            {
                JogSpeed = -speed;
                Command = ServoDriveManualControlCommand.JogReverse;
            }
        }

        public void StopJog()
        {
            Command = ServoDriveManualControlCommand.StopJog;
        }

        public void ReturnToHomePosition()
        {
            Command = ServoDriveManualControlCommand.ReturnToHomePosition;
        }

        public void Go(string position)
        {
            if (string.IsNullOrEmpty(position))
                throw new ArgumentException("String cannot be empty or null.", "position");

            Position = position;
            Command = ServoDriveManualControlCommand.Go;
        }

        public void Go(int positioningValue)
        {
            Position = string.Empty;
            PositioningValue = positioningValue;
            Command = ServoDriveManualControlCommand.Go;
        }

        public void Go(Point point)
        {
            Position = string.Empty;
            Point = point;
            Command = ServoDriveManualControlCommand.Go;
        }

        public void ReadParameters(ServoDriveParameters parameters)
        {
            Parameters = parameters ?? throw new ArgumentNullException("parameters");
            Command = ServoDriveManualControlCommand.QueryParameters;
        }

        public void ConfigureParameters(ServoDriveParameters parameters)
        {
            Parameters = parameters ?? throw new ArgumentNullException("parameters");
            Command = ServoDriveManualControlCommand.UpdateParameters;
        }

        public void ConfigurePositions(ServoPosition position)
        {
            ServoPosition = position;
            Command = ServoDriveManualControlCommand.UpdatePosition;
        }

        public void ConfigurePositions(ServoPoint point)
        {
            ServoPoint = point;
            Command = ServoDriveManualControlCommand.UpdatePosition;
        }
    }

    public struct ServoPosition
    {
        public string Name { get; }

        public int Value { get; }

        public ServoPosition(string name, int value)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            Name = name;
            Value = value;
        }
    }

    public struct ServoPoint
    {
        public string Name { get; }

        public int X { get; }

        public int Y { get; }

        public ServoPoint(string name, int x, int y)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            Name = name;
            X = x;
            Y = y;
        }
    }
}
