﻿using Automation.Profiles.Cylinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages.ManualControl.Movable
{
    public enum CylinderManualControlCommand : byte
    {
        Go,
        InformIfAvailable,
        QueryConfiguration,
        UpdateConfiguration,
    }

    public sealed class CylinderManualControlMessage : MovableManualControlMessage
    {
        public CylinderManualControlCommand Command { get; private set; }

        public string Position { get; private set; }

        public bool Available { get; private set; }

        public CylinderConfiguration Configuration { get; private set; }

        public CylinderManualControlMessage(string cylinderName) : base(cylinderName) { }

        public void Go(string position)
        {
            if (string.IsNullOrEmpty(position))
                throw new ArgumentException("String cannot be empty or null.", "position");

            Position = position;
            Command = CylinderManualControlCommand.Go;
        }

        public void InformIfAvailable(bool available)
        {
            Available = available;
            Command = CylinderManualControlCommand.InformIfAvailable;
        }

        public void ReadConfiguration(CylinderConfiguration configuration)
        {
            Configuration = configuration ?? throw new ArgumentNullException("configuration");
            Command = CylinderManualControlCommand.QueryConfiguration;
        }

        public void UpdateConfiguration(CylinderConfiguration configuration)
        {
            Configuration = configuration ?? throw new ArgumentNullException("configuration");
            Command = CylinderManualControlCommand.UpdateConfiguration;
        }
    }
}
