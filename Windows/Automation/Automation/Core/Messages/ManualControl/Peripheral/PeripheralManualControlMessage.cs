﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages.ManualControl.Peripheral
{
    public abstract class PeripheralManualControlMessage : ManualControlMessage
    {
        protected PeripheralManualControlMessage(string objectName) : base(ManualControlType.Peripheral, objectName) { }
    }
}
