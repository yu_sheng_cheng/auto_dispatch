﻿using Automation.Core.IOWorkers;
using Automation.Peripheral.BarcodeScanners;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages.ManualControl.Peripheral
{
    public sealed class BarcodeScannerManualControlMessage : PeripheralManualControlMessage
    {
        private string _barcode = string.Empty;

        public string Barcode
        {
            get { return _barcode; }
            set
            {
                _barcode = value ?? throw new ArgumentNullException("Barcode");
            }
        }

        public BarcodeScannerManualControlMessage(string scannerName, Action<Message> completed) : base(scannerName)
        {
            Completed = completed ?? throw new ArgumentNullException("completed");
        }

        internal void OnScanCompleted(IOWorker worker)
        {
            BarcodeScanner scanner = worker as BarcodeScanner;

            var result = scanner.Result;

            if (!result.IsSuccess)
            {
                Result = result.ErrorCode == (int)AutomationError.Timeout ? ManualControlResult.Timeout : ManualControlResult.AlarmOccurred;
                Complete(false);
                return;
            }

            Result = ManualControlResult.Success;
            Barcode = scanner.Barcode;
            Complete();
        }
    }
}
