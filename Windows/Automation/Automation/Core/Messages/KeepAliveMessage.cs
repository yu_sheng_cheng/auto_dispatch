﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.Messages
{
    public sealed class KeepAliveMessage : Message
    {
        public KeepAliveMessage(Action<Message> completed) : base((byte)MessageClass.SupervisoryController, MessageId.KeepAlive)
        {
            Completed = completed ?? throw new ArgumentNullException("completed");
        }
    }
}
