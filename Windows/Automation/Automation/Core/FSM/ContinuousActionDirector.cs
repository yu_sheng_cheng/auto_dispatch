﻿using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Enums;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SwissKnife.Log.AppLogger;

namespace Automation.Core.FSM
{
    public abstract class ContinuousActionDirector : INamedObject, IPollable
    {
        private DecompositionAction _firstAction;
        private DecompositionAction _currentAction;

        public string Name { get; }

        public bool IsBusy { get; private set; }

        public bool IsCompleted { get; private set; }

        public ContinuousActionResult Result { get; } = new ContinuousActionResult();

        public bool IsSuccess
        {
            get { return Result.IsSuccess; }
        }

        public ContinuousActionDirector(string name)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("String cannot be empty or null.", "name");

            Name = name;
        }

        protected virtual void Reinitialize()
        {
            IsCompleted = false;
            Result.Reinitialize();
        }

        public bool Go(SupervisoryController controller)
        {
            if (IsBusy)
                throw new InvalidOperationException($"The {Name} continuous action is already in progress.");

            if (_firstAction == null)
                _firstAction = CreateFirstAction();
            else
            {
                Reinitialize();
                _firstAction.Reinitialize();
            }

            _currentAction = _firstAction;

            IsBusy = true;

            if (!_currentAction.Go(controller))
            {
                Log("current action Go failed");
                IsBusy = false;
                _currentAction = null;
                return false;
            }

            return true;
        }

        private void Log(string v)
        {
            Console.WriteLine("[{0}]: {1}", this.GetType().Name, v);
        }

        private void Log(string v, string c)
        {
            Console.WriteLine("[{0}]: {1} || {2}", this.GetType().Name, v, c);
        }

        protected abstract DecompositionAction CreateFirstAction();

        public void Poll(SupervisoryController controller)
        {
            bool autoRun = controller.EquipmentState.OperationMode == OperationMode.AutoRun;

            if (autoRun)
            {
#if DEBUG
                if (!CheckIfObjectExists(controller) && IsSuccess)
                    throw new BugException("Should not go in here.");
#else
                CheckIfObjectExists(controller);
#endif
            }

            if (_currentAction.Poll(controller) == null)
            {
                if (controller.EquipmentState.OperationMode == OperationMode.Forbidden)
                {
                    Result.ObjectName = Name;
                    Result.CompletionResult = CompletionResult.ForbiddenOperating;
                }

                bool completed = !IsSuccess || (_currentAction = _currentAction.GetNextAction()) == null || !_currentAction.Go(controller);

                if (completed)
                {
                    _currentAction = null;
                    IsBusy = false;
                    IsCompleted = true;
                    OnCompleted();
                }
            }
        }

        protected virtual bool CheckIfObjectExists(SupervisoryController controller)
        {
            return true;
        }

        protected virtual void OnCompleted()
        {
        }
    }
}