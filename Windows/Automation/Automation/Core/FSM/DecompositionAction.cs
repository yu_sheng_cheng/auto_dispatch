﻿using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Core.IOWorkers;
using Automation.Enums;
using Automation.Exceptions;
using Automation.Helper;
using Automation.Objects.Movable;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.FSM
{
    public abstract class DecompositionAction
    {
        private readonly LinkedList<InputInspection> _inputInspections = new LinkedList<InputInspection>();
        private readonly LinkedList<OnceTimer> _timers = new LinkedList<OnceTimer>();

        private DecompositionAction _nextAction;
        private Barrier _barrier = new Barrier();

        protected ContinuousActionDirector Director { get; }

        protected DecompositionAction(ContinuousActionDirector director)
        {
            Director = director ?? throw new ArgumentNullException("director");
        }

        public virtual void Reinitialize()
        {
        }

        public abstract bool Go(SupervisoryController controller);

        protected bool Go(SupervisoryController controller, string movableObjectName, string position,
            Action<SupervisoryController, INamedObject, CompletionResult> completed)
        {
            if (completed == null)
                throw new ArgumentNullException("completed");

            if (!controller.TryGetMovableObject(movableObjectName, out IMovableObject movableObject))
                throw new NotFoundException($"'{movableObjectName}' was not found.");

            bool manual = (controller.EquipmentState.OperationMode & OperationMode.Manual) == OperationMode.Manual;

            if (manual)
            {
                if (string.IsNullOrEmpty(position))
                    throw new ArgumentException("String cannot be empty or null.", "position");

                ushort error = movableObject.MistakeProofing?.Invoke(controller, position) ?? 0;

                if (error != 0)
                {
                    ContinuousActionResult result = Director.Result;

                    result.ObjectName = movableObjectName;
                    result.AcceptResult = AcceptResult.Prohibited;
                    result.ProhibitionCode = error;
                    return false;
                }

                RejectionReason rejection = movableObject.CanExecuteGo(controller);

                if (rejection != RejectionReason.Allowed)
                {
                    ContinuousActionResult result = Director.Result;

                    result.ObjectName = movableObjectName;
                    result.AcceptResult = rejection.ToAcceptResult();
                    return false;
                }
            }

            _barrier.AddParticipants(1);
            movableObject.Go(controller, position, completed);
            return true;
        }

        protected bool AddIORequest(IOWorker worker, Action<IOWorker> completed)
        {
            if (worker == null)
                throw new ArgumentNullException("worker");

            if (completed == null)
                throw new ArgumentNullException("completed");

            var controller = worker.Controller;

            bool manual = (controller.EquipmentState.OperationMode & OperationMode.Manual) == OperationMode.Manual;

            if (manual)
            {
                if (worker.IsBusy)
                {
                    ContinuousActionResult result = Director.Result;

                    result.ObjectName = worker.Name;
                    result.AcceptResult = AcceptResult.Busy;
                    return false;
                }
            }

            _barrier.AddParticipants(1);
            controller.AddIORequest(worker, completed);
            return true;
        }

        protected bool Act(SupervisoryController controller, ISingleAction singleAction,
            Action<SupervisoryController, INamedObject, CompletionResult> completed)
        {
            if (singleAction == null)
                throw new ArgumentNullException("singleAction");

            if (completed == null)
                throw new ArgumentNullException("completed");

            bool manual = (controller.EquipmentState.OperationMode & OperationMode.Manual) == OperationMode.Manual;

            if (manual)
            {
                if (!singleAction.CanAct)
                {
                    ContinuousActionResult result = Director.Result;

                    result.ObjectName = singleAction.Name;
                    result.AcceptResult = singleAction.IsBusy ? AcceptResult.Busy : AcceptResult.NotReady;
                    return false;
                }
            }

            _barrier.AddParticipants(1);
            singleAction.Act(controller, completed);
            return true;
        }

        protected void AddInputInspection(InputInspection inputInspection,
            Action<SupervisoryController, INamedObject, CompletionResult> completed)
        {
            if (inputInspection == null)
                throw new ArgumentNullException("inputInspection");

            inputInspection.Completed = completed ?? throw new ArgumentNullException("completed");
            inputInspection.Start();

            _inputInspections.AddLast(inputInspection.Node);
            _barrier.AddParticipants(1);
        }

        /// <summary>
        /// 延遲 <c>milliseconds</c>執行<c>expired</c>
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="timer"></param>
        /// <param name="milliseconds"></param>
        /// <param name="expired"></param>
        protected void AddTimer(SupervisoryController controller, OnceTimer timer, int milliseconds,
            Action<SupervisoryController> expired)
        {
            if (timer == null)
                throw new ArgumentNullException("timer");

            timer.Start(controller, milliseconds, expired);

            _timers.AddLast(timer.Node);
            _barrier.AddParticipants(1);
        }

        protected void AddParticipants(byte participantCount)
        {
            _barrier.AddParticipants(participantCount);
        }

        protected void RemoveParticipants(byte participantCount)
        {
            if (_barrier.RemoveParticipants(participantCount))
                Exit();
        }

        protected bool SignalAndWait(SupervisoryController controller, string participantName, CompletionResult result,
            Action<SupervisoryController> goOn = null)
        {
            if (string.IsNullOrEmpty(participantName))
                throw new ArgumentException("String cannot be empty or null.", "participantName");

            if (result != CompletionResult.Success)
            {
                Director.Result.ObjectName = participantName;
                Director.Result.CompletionResult = result;
            }

            if (_barrier.SignalAndWait())
            {
                if (Director.IsSuccess && goOn != null)
                {
                    goOn.Invoke(controller);

                    if (!_barrier.Done)
                        return false;
                }

                Exit();
                return true;
            }

            return false;
        }

        public virtual DecompositionAction Poll(SupervisoryController controller)
        {
            try
            {
                PollInputInspection(controller);
            }
            catch (Exception e)
            {
                Console.WriteLine("failed on poll input section: {0}", e);
                throw;
            }

            try
            {
                PollTimer(controller);
            }
            catch (Exception e)
            {
                Console.WriteLine("failed on poll timer: {0}", e);
                throw;
            }

            if (_barrier.Done)
                return null;

            return this;
        }

        private void PollInputInspection(SupervisoryController controller)
        {
            bool autoRun = controller.EquipmentState.OperationMode == OperationMode.AutoRun;

            var node = _inputInspections.First;

            while (node != null)
            {
                var next = node.Next;

                InputInspection inputInspection = node.Value;

                var completed = inputInspection.Completed;
                inputInspection.Completed = null;

                inputInspection.Poll(controller);

                if (inputInspection.IsCompleted)
                {
                    _inputInspections.Remove(node);

                    var result = inputInspection.Result;

                    if (result == CompletionResult.Timeout && autoRun)
                    {
                        var getAlarmReport = inputInspection.GetAlarmReport;

                        if (getAlarmReport != null)
                        {
                            controller.AlarmWatcher.AddAutoRunError(controller, getAlarmReport.Invoke(controller));
                            result = CompletionResult.AlarmOccurred;
                        }
                    }

                    completed.Invoke(controller, inputInspection, result);
                }
                else
                    inputInspection.Completed = completed;

                node = next;
            }
        }

        private void PollTimer(SupervisoryController controller)
        {
            var node = _timers.First;

            while (node != null)
            {
                var next = node.Next;

                OnceTimer timer = node.Value;

                var expired = timer.Expired;
                timer.Expired = null;

                if (timer.CheckTimeout(controller))
                {
                    _timers.Remove(node);
                    expired.Invoke(controller);
                }
                else
                    timer.Expired = expired;

                node = next;
            }
        }

        protected virtual void Exit()
        {
        }

        public DecompositionAction GetNextAction()
        {
            if (_nextAction == null)
                _nextAction = CreateNextAction();
            else
                _nextAction.Reinitialize();

            return _nextAction;
        }

        protected virtual DecompositionAction CreateNextAction()
        {
            return null;
        }
    }
}