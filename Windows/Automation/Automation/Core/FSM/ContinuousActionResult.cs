﻿using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.FSM
{
    public sealed class ContinuousActionResult
    {
        private string _objectName = string.Empty;
        private AcceptResult _acceptResult = AcceptResult.Accepted;
        private CompletionResult _completionResult = CompletionResult.Success;
        private ushort _prohibitionCode;

        public bool IsSuccess { get { return !(AcceptResult != AcceptResult.Accepted | CompletionResult != CompletionResult.Success); } }

        public string ObjectName
        {
            get { return _objectName; }
            set
            {
                if (string.IsNullOrEmpty(value))
                    throw new ArgumentException("String cannot be empty or null.", "ObjectName");

                if (string.IsNullOrEmpty(_objectName))
                    _objectName = value;
            }
        }

        public AcceptResult AcceptResult
        {
            get { return _acceptResult; }
            set
            {
                if (value == AcceptResult.Accepted)
                    throw new ArgumentException("The 'AcceptResult' property can't be equal to 'Accepted'.", "AcceptResult");

                if (_acceptResult != AcceptResult.Accepted | _completionResult != CompletionResult.Success)
                    return;

                _acceptResult = value;
            }
        }

        public CompletionResult CompletionResult
        {
            get { return _completionResult; }
            set
            {
                if (value == CompletionResult.Success)
                    throw new ArgumentException("The 'CompletionResult' property can't be equal to 'Success'.", "CompletionResult");

                if (_acceptResult != AcceptResult.Accepted | _completionResult != CompletionResult.Success)
                    return;

                _completionResult = value;
            }
        }

        public ushort ProhibitionCode
        {
            get { return _prohibitionCode; }
            set
            {
                if (_prohibitionCode == 0)
                    _prohibitionCode = value;
            }
        }

        public void Reinitialize()
        {
            _objectName = string.Empty;
            _acceptResult = AcceptResult.Accepted;
            _completionResult = CompletionResult.Success;
            _prohibitionCode = 0;
        }
    }
}
