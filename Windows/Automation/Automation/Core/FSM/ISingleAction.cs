﻿using Automation.Core.Controller;
using Automation.Core.Interfaces;
using Automation.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Core.FSM
{
    public interface ISingleAction : INamedObject
    {
        bool CanAct { get; }

        bool IsBusy { get; }

        void Act(SupervisoryController controller, Action<SupervisoryController, INamedObject, CompletionResult> completed);
    }
}
