﻿using SwissKnife.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation
{
    public static class AutomationEnvironment
    {
        private static readonly ILog _debug = new Debug();
        private static volatile ILog _log;

        public static ILog Log
        {
            get => _log ?? _debug;
            set => _log = value;
        }

        public static async Task SetCurrentLanguage(string name)
        {
            StringResource sr;

            switch (name)
            {
                case "zh-TW":
                    sr = StringResource.Load();
                    break;
                default:
                    return;
            }

            await Task.Delay(1).ConfigureAwait(false);
            SR.SetStringResource(sr);
        }
    }
}
