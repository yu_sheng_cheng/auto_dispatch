﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Net.Http
{
    public sealed class HttpRequest:IEnumerable<string>
    {
        private const string HttpRequestRegexPattern = @"^(?<METHOD>[^\s]+)\s(?<PATH>[^\s]+)\sHTTP\/1\.1\r\n" + // request line
                                                       @"((?<FIELD_NAME>[^:\r\n]+):(?([^\r\n])\s)*(?<FIELD_VALUE>[^\r\n]*)\r\n)+" + // headers
                                                       @"\r\n"; // newline

        private static readonly Regex HttpRequestRegex = new Regex(HttpRequestRegexPattern, RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);

        public static bool TryParse(byte[] bytes, out HttpRequest result)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");

            return TryParse(bytes, 0, bytes.Length, out result);
        }

        public static bool TryParse(byte[] bytes, int index, int count, out HttpRequest result)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");

            if (count < 0)
                throw new ArgumentOutOfRangeException("count");

            if (index < 0)
                throw new ArgumentOutOfRangeException("index");

            if (bytes.Length - index < count)
                throw new ArgumentException("Index and count must refer to a location within the buffer.");

            result = null;

            if (count < 4)
                return false;

            int start = index;
            int end = start + count - 4;

            while (start <= end)
            {
                if (bytes[start] != 0x0d || ((bytes[start] << 24) | (bytes[start + 1] << 16) | (bytes[start + 2] << 8) | bytes[start + 3]) != 0x0d0a0d0a) // \r\n\r\n
                {
                    ++start;
                    continue;
                }

                int headerLength = (start - index) + 4;

                var text = Encoding.UTF8.GetString(bytes, index, headerLength);

                Match match = HttpRequestRegex.Match(text);

                if (!match.Success)
                    throw new FormatException("Can't parse the http request headers.");

                var request = new HttpRequest
                {
                    HeaderLength = headerLength,
                    Method = match.Groups["METHOD"].Value,
                    Path = match.Groups["PATH"].Value
                };

                var names = match.Groups["FIELD_NAME"].Captures;
                var values = match.Groups["FIELD_VALUE"].Captures;
                int fields = names.Count;
                for (int i = 0; i < fields; ++i)
                {
                    var name = names[i].ToString();
                    var value = values[i].ToString();
                    request._headers[name] = value;
                }

                string fieldValue;

                if (request._headers.TryGetValue("Connection", out fieldValue))
                {
                    if (string.Compare(fieldValue, "Upgrade", StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        if (request._headers.TryGetValue("Upgrade", out fieldValue))
                            request.IsWebSocketRequest = string.Compare(fieldValue, "websocket", StringComparison.OrdinalIgnoreCase) == 0;
                    }
                    else if (string.Compare(fieldValue, "close", StringComparison.OrdinalIgnoreCase) == 0)
                        request.KeepAlive = false;
                }

                request.SubProtocols =
                    request._headers.TryGetValue("Sec-WebSocket-Protocol", out fieldValue) ? fieldValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries) : new string[0];

                if (request._headers.TryGetValue("Content-Length", out fieldValue))
                {
                    int contentLength;

                    if (!int.TryParse(fieldValue, out contentLength))
                        throw new FormatException("Can't parse Content-Length.");

                    if (contentLength < 0)
                        throw new FormatException("Content length cannot be less than 0.");

                    request.ContentLength = contentLength;

                    if (contentLength > 0 && count - headerLength >= contentLength)
                        request.ParseBody(bytes, index + headerLength, contentLength);
                }

                result = request;
                return true;
            }

            return false;
        }

        public static string[] ExtractSegmentsFromPath(string path)
        {
            if (path == null)
                throw new ArgumentNullException("path");

            int length = path.Length;

            if (length == 0)
                return new string[0];

            ArrayList pathSegments = new ArrayList();

            int last = length - 1;
            int slash = path[0] == '/' ? 0 : -1;

            while (slash < last)
            {
                int current = slash + 1;
                slash = path.IndexOf('/', current);
                if (slash == -1)
                    slash = length;
                pathSegments.Add(path.Substring(current, slash - current));
            }
            return (string[])pathSegments.ToArray(typeof(string));
        }

        private readonly IDictionary<string, string> _headers = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        public int HeaderLength { get; private set; }

        public string Method { get; private set; }

        public string Path { get; private set; }

        public bool KeepAlive { get; private set; } = true;

        public bool IsWebSocketRequest { get; private set; }

        public IEnumerable<string> SubProtocols { get; private set; }

        public int ContentLength { get; private set; }

        public bool HasEntityBody { get { return ContentLength != 0; } }

        public string Body { get; set; }

        public string this[string name]
        {
            get
            {
                _headers.TryGetValue(name, out string value);
                return value;
            }
        }

        public bool ContainsHeader(string name)
        {
            return _headers.ContainsKey(name);
        }

        public bool TryGetHeader(string name, out string value)
        {
            return _headers.TryGetValue(name, out value);
        }

        public IEnumerator<string> GetEnumerator()
        {
            foreach (var item in _headers)
            {
                yield return item.Key;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void ParseBody(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            ParseBody(data, 0, data.Length);
        }

        public void ParseBody(byte[] data, int index, int count)
        {
            if (!HasEntityBody)
                return;

            if (count < ContentLength)
                throw new ArgumentOutOfRangeException("count");

            Body = Encoding.UTF8.GetString(data, index, ContentLength);
        }
    }
}
