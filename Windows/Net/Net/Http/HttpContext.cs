﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Net.Http
{
    public interface IHttpContext
    {
        HttpRequest Request { get; }

        HttpResponse Response { get; }

        void SendResponse(byte[] data, Action<byte[], int> returnBuffer = null);

        void SendResponse(byte[] data, int offset, int count, Action<byte[], int> returnBuffer = null);
    }

    public sealed class HttpContext : IHttpContext
    {
        private readonly MicroHttpConnection _connection;
        private bool _hasSentResponse;
        private Action<byte[], int> _returnBuffer;
        internal HttpContext _next;

        public HttpRequest Request { get; }

        public HttpResponse Response { get; }

        public BufferedOutputMessage BufferedResponseData { get; private set; }

        public HttpContext(MicroHttpConnection connection, HttpRequest request)
        {
            _connection = connection ?? throw new ArgumentNullException("connection");
            Request = request ?? throw new ArgumentNullException("request");
            Response = new HttpResponse(request.KeepAlive);
        }

        public void SendResponse(byte[] data, Action<byte[], int> returnBuffer = null)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            SendResponse(data, 0, data.Length, returnBuffer);
        }

        public void SendResponse(byte[] data, int offset, int count, Action<byte[], int> returnBuffer = null)
        {
            if (_hasSentResponse)
                throw new InvalidOperationException("The response has already been sent.");

            BufferedResponseData = new BufferedOutputMessage(data, offset, count);
            _returnBuffer = returnBuffer;
            _hasSentResponse = true;

            _connection.SendResponseAsync(this);
        }

        public void ReturnBuffer()
        {
            if (!_hasSentResponse)
                throw new InvalidOperationException("The response not sent.");

            if (_returnBuffer != null)
            {
                try { _returnBuffer.Invoke(BufferedResponseData.Buffer, BufferedResponseData.Offset); } catch { }
            }
        }
    }
}
