﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Net.Http.WebSockets
{
    [Serializable()]
    public class WebSocketException : Exception
    {
        public WebSocketCloseStatus Code { get; }

        public WebSocketException(WebSocketCloseStatus code) : this(code, null, null) { }

        public WebSocketException(WebSocketCloseStatus code, string message) : this(code, message, null) { }

        public WebSocketException(WebSocketCloseStatus code, string message, Exception inner) : base(message ?? code.GetMessage(), inner)
        {
            Code = code;
        }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client. 
        protected WebSocketException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
