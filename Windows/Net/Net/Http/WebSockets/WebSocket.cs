﻿using SwissKnife.Conversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Net.Http.WebSockets
{
    public static class WebSocket
    {
        private static readonly IByteConverter _bigEndianConverter = new BigEndianConverter();

        public static bool IsDraftIetfHybi13Version(HttpRequest request)
        {
            return IsDraftIetfHybi13Version(GetVersion(request));
        }

        public static bool IsDraftIetfHybi13Version(string version)
        {
            switch (version)
            {
                case "7":
                case "8":
                case "13":
                    return true;
                default:
                    return false;
            }
        }

        public static string GetVersion(HttpRequest request)
        {
            string version;

            if (request.TryGetHeader("Sec-WebSocket-Version", out version))
                return version;

            if (request.TryGetHeader("Sec-WebSocket-Draft", out version))
                return version;

            if (request.ContainsHeader("Sec-WebSocket-Key1"))
                return "76";

            return "75";
        }

        public static bool BuildHandshakeResponse(HttpRequest request, out HttpResponse response)
        {
            string secWebSocketKey = request["Sec-WebSocket-Key"];
            if (secWebSocketKey == null)
            {
                response = null;
                return false;
            }

            response = new HttpResponse(101);

            response.AddHeader("Connection", "Upgrade");
            response.AddHeader("Upgrade", "websocket");

            byte[] sha1Hash = SHA1.Create().ComputeHash(Encoding.ASCII.GetBytes($"{secWebSocketKey}258EAFA5-E914-47DA-95CA-C5AB0DC85B11"));
            response.AddHeader("Sec-WebSocket-Accept", System.Convert.ToBase64String(sha1Hash));
            return true;
        }

        public static bool IsControlFrame(WebSocketOpCode opCode)
        {
            return opCode == WebSocketOpCode.ConnectionClose | opCode == WebSocketOpCode.Ping | opCode == WebSocketOpCode.Pong;
        }

        public static string GetMessage(this WebSocketCloseStatus code)
        {
            switch (code)
            {
                case WebSocketCloseStatus.GoingAway:
                    return "Indicates an endpoint is being removed. Either the server or client will become unavailable.";
                case WebSocketCloseStatus.ProtocolError:
                    return "The client or server is terminating the connection because of a protocol error.";
                case WebSocketCloseStatus.InvalidMessageType:
                    return "The client or server is terminating the connection because it cannot accept the data type it received.";
                case WebSocketCloseStatus.Empty:
                    return "No error specified.";
                case WebSocketCloseStatus.InvalidPayloadData:
                    return "The client or server is terminating the connection because it has received data inconsistent with the message type.";
                case WebSocketCloseStatus.PolicyViolation:
                    return "The connection will be closed because an endpoint has received a message that violates its policy.";
                case WebSocketCloseStatus.MessageTooBig:
                    return "The client or server is terminating the connection because it has received a message that is too big for it to process.";
                case WebSocketCloseStatus.MandatoryExtension:
                    return "The client is terminating the connection because it expected the server to negotiate an extension.";
                case WebSocketCloseStatus.InternalServerError:
                    return "The connection will be closed by the server because of an error on the server.";
                default:
                    return string.Empty;
            }
        }

        public static byte[] BuildHeader(WebSocketOpCode opCode, int payloadLength)
        {
            return BuildHeader(true, opCode, payloadLength);
        }

        public static byte[] BuildHeader(bool isFinalFragment, WebSocketOpCode opCode, int payloadLength)
        {
            if (!isFinalFragment && IsControlFrame(opCode))
                throw new ArgumentException("Control frames themselves must not be fragmented.");

            if (payloadLength < 0)
                throw new ArgumentOutOfRangeException("payloadLength");

            byte[] buffer;

            if (payloadLength > UInt16.MaxValue)
            {
                buffer = new byte[10];
                buffer[1] = 127;
                _bigEndianConverter.GetBytes((ulong)payloadLength, buffer, 2);
            }
            else if (payloadLength > 125)
            {
                buffer = new byte[4];
                buffer[1] = 126;
                _bigEndianConverter.GetBytes((ushort)payloadLength, buffer, 2);
            }
            else
            {
                buffer = new byte[2];
                buffer[1] = (byte)payloadLength;
            }

            buffer[0] = (byte)((isFinalFragment ? 0x80 : 0) | (byte)opCode);
            return buffer;
        }
    }
}
