﻿using Net.AsyncSocket;
using Net.AsyncSocket.Tcp;
using SwissKnife;
using SwissKnife.Conversion;
using SwissKnife.Exceptions;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Net.Http.WebSockets
{
    public interface IWebSocketConnection
    {
        bool IsClosed { get; }

        IPEndPoint RemoteEndPoint { get; }

        string SessionKey { get; }

        int ReceiveBufferLimit { get; set; }

        Func<byte[]> GetReceiveBuffer { get; set; }

        object UserToken { get; set; }

        Action<IWebSocketConnection> OnOpen { get; set; }

        Action<IWebSocketConnection> OnClose { get; set; }

        Action<IWebSocketConnection, byte[], int, int, bool, string> OnTextFrame { get; set; }

        Action<IWebSocketConnection, byte[], int, int, bool> OnBinaryFrame { get; set; }

        void SendTextFrame(string text);

        void SendTextFrame(bool isFinalFragment, string text);

        void SendBinaryFrame(byte[] data);

        void SendBinaryFrame(bool isFinalFragment, byte[] data);

        void SendBinaryFrame(byte[] data, int offset, int count);

        void SendBinaryFrame(bool isFinalFragment, byte[] data, int offset, int count);

        void SendFrame(byte[] frame, Action<byte[], int> returnBuffer = null);

        void SendFrame(byte[] frame, int offset, int count, Action<byte[], int> returnBuffer = null);

        void Close(WebSocketCloseStatus closeStatus = WebSocketCloseStatus.NormalClosure);
    }

    public class WebSocketConnection : IWebSocketConnection, IDisposable
    {
        private class ReceivedFrame
        {
            internal WebSocketOpCode _opCode;
            internal bool _isFragmentedMessage;
            internal readonly byte[] _maskingKey = new byte[4];
            internal byte _headerLength;
            internal int _payloadLength;
            internal int _payloadHead;
            internal byte[] _buffer;

            internal bool HeaderReceived
            {
                get { return _payloadLength != -1; }
                set
                {
                    if (!value)
                        _payloadLength = -1;
                }
            }

            internal int Length { get { return _headerLength + _payloadLength; } }
        }

        private class OutputFrame
        {
            private Action<byte[], int> _returnBuffer;
            internal OutputFrame _next;

            public WebSocketOpCode OpCode { get; private set; }

            public BufferedOutputMessage BufferedData { get; private set; }

            public OutputFrame(WebSocketOpCode opCode, byte[] buffer, int offset, int count, Action<byte[], int> returnBuffer = null)
            {
                _returnBuffer = returnBuffer;
                OpCode = opCode;
                BufferedData = new BufferedOutputMessage(buffer, offset, count);
            }

            public void ReturnBuffer()
            {
                if (_returnBuffer != null)
                {
                    try { _returnBuffer.Invoke(BufferedData.Buffer, BufferedData.Offset); } catch { }
                }
            }
        }

        private class Node
        {
            private OutputFrame _prev;
            [ContractPublicPropertyName("First")]
            internal OutputFrame _next;

            internal OutputFrame First { get { return _next; } }

            internal void Add(OutputFrame frame)
            {
                if (_prev == null)
                    _next = frame;
                else
                    _prev._next = frame;

                _prev = frame;
            }

            internal void Push(OutputFrame frame)
            {
                OutputFrame first = _next;

                if (first == null)
                    _prev = frame;

                _next = frame;
                frame._next = first;
            }

            internal OutputFrame Pop()
            {
                OutputFrame first = _next;

                if (first == null)
                    return null;

                _next = first._next;
                if (_next == null)
                    _prev = null;

                return first;
            }
        }

        private const int MinimumInternalReceiveBufferSize = 4 << 10;
        private const int MaximumInternalReceiveBufferSize = 1 << 30;
        private const int CacheSize = 16 << 10;

        private const byte WaitForNewMessage = 0;
        private const byte WaitForNewFragment = 1;

        private static readonly UTF8Encoding _utf8Encoding = new UTF8Encoding(false, true);

        private static string ParseTextPayloadData(byte[] data, int offset, int count)
        {
            try
            {
                return _utf8Encoding.GetString(data, offset, count);
            }
            catch (ArgumentException)
            {
                throw new WebSocketException(WebSocketCloseStatus.InvalidPayloadData);
            }
        }

        private readonly AsyncTcpServer _server;
        private readonly MicroHttpConnection _connection;
        private readonly IByteConverter _bigEndianConverter = new BigEndianConverter();
        private readonly ReceivedFrame _receivedFrame = new ReceivedFrame();
        private int _internalReceiveBufferLimit;
        private byte[] _receiveBuffer;
        private int _dataHead;
        private int _bytesRead;
        private byte[] _cache;
        private int _cacheHead;
        private int _bytesCached;    
        private byte _receiveState;
        private Node _outputFrames = new Node();
        private bool _hasSentCloseFrame;
        private bool _isSending;
        private bool _disposed;
        private readonly LightSpinLock _lock = new LightSpinLock();

        private Func<byte[]> _getReceiveBuffer;
        private Action<IWebSocketConnection> _onOpen = (_) => { };
        private Action<IWebSocketConnection> _onClose = (_) => { };
        private Action<IWebSocketConnection, byte[], int, int, bool, string> _onTextFrame = (_, __, ___, ____, _____, ______) => { };
        private Action<IWebSocketConnection, byte[], int, int, bool> _onBinaryFrame = (_, __, ___, ____, _____) => { };

        public bool IsClosed { get { return _disposed | _hasSentCloseFrame; } }

        public IPEndPoint RemoteEndPoint { get { return _connection.RemoteEndPoint; } }

        public string SessionKey { get { return _connection.SessionKey; } }

        public int ReceiveBufferLimit { get; set; } = 1 << 20;

        public Func<byte[]> GetReceiveBuffer
        {
            get { return _getReceiveBuffer; }
            set { _getReceiveBuffer = value; }
        }

        public object UserToken { get; set; }

        public Action<IWebSocketConnection> OnOpen
        {
            get { return _onOpen; }
            set
            {
                _onOpen = value ?? throw new ArgumentNullException("OnOpen");
            }
        }

        public Action<IWebSocketConnection> OnClose
        {
            get { return _onClose; }
            set
            {
                _onClose = value ?? throw new ArgumentNullException("OnClose");
            }
        }

        public Action<IWebSocketConnection, byte[], int, int, bool, string> OnTextFrame
        {
            get { return _onTextFrame; }
            set
            {
                _onTextFrame = value ?? throw new ArgumentNullException("OnTextFrame");
            }
        }

        public Action<IWebSocketConnection, byte[], int, int, bool> OnBinaryFrame
        {
            get { return _onBinaryFrame; }
            set
            {
                _onBinaryFrame = value ?? throw new ArgumentNullException("OnBinaryFrame");
            }
        }

        public WebSocketConnection(MicroHttpConnection connection)
        {
            _connection = connection;
            _server = _connection.Server;
        }

        public void Dispose()
        {
            Node outputFrames = _outputFrames;

            _lock.Enter();
            _outputFrames = null;
            _disposed = true;
            _lock.Leave();

            OutputFrame frame;

            while ((frame = outputFrames.Pop()) != null)
            {
                frame.ReturnBuffer();
            }

            try { _onClose(this); } catch { }
        }

        public async Task StartReceivingAsync()
        {
            Func<byte[]> getReceiveBuffer = Volatile.Read(ref _getReceiveBuffer);
            bool useInternalBuffer = getReceiveBuffer == null;

            try
            {
                if (useInternalBuffer)
                {
                    if (ReceiveBufferLimit >= MaximumInternalReceiveBufferSize)
                        _internalReceiveBufferLimit = MaximumInternalReceiveBufferSize;
                    else if (ReceiveBufferLimit <= MinimumInternalReceiveBufferSize)
                        _internalReceiveBufferLimit = MinimumInternalReceiveBufferSize;
                    else
                        _internalReceiveBufferLimit = (ReceiveBufferLimit + (MinimumInternalReceiveBufferSize - 1)) & ~(MinimumInternalReceiveBufferSize - 1);

                    int receiveBufferSize = MinimumInternalReceiveBufferSize;

                    _receiveBuffer = new byte[receiveBufferSize];

                    _connection.SetReceiveBuffer(_receiveBuffer, 0, receiveBufferSize);

                    while (true)
                    {
                        await ReceiveMessageAsync();
                        if (!ProcessReceivedFrame(_receivedFrame))
                            break;
                    }
                }
                else
                {
                    _cache = new byte[CacheSize];

                    while (true)
                    {
                        await ReceiveMessageAsync(getReceiveBuffer);
                        if (!ProcessReceivedFrame(_receivedFrame))
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                if (e is SocketException || e is ObjectDisposedException)
                {
                    if (e is SocketException)
                        _server.Log.d(_server.LogKeyword, $"WebSocketConnection.StartReceivingAsync:{Environment.NewLine}{e}");

                    _server.CloseSession(_connection);
                }
                else
                {
                    _server.Log.e(_server.LogKeyword, "WebSocketConnection.StartReceivingAsync:", e);

                    WebSocketCloseStatus closeStatus = WebSocketCloseStatus.InternalServerError;

                    WebSocketException webSocketException = e as WebSocketException;
                    if (webSocketException != null)
                        closeStatus = webSocketException.Code;

                    SendCloseFrame((ushort)closeStatus);
                }
            }
        }

        private async Task ReceiveMessageAsync()
        {
            int capacity = _receiveBuffer.Length - _dataHead;
            bool stickyPacket = _bytesRead != 0;
            bool messageReceived = false;

            while (true)
            {
                if (stickyPacket)
                    stickyPacket = false;
                else
                {
                    int size = Math.Min(capacity - _bytesRead, 16 << 10);

                    _bytesRead += await _connection.ReceiveAsync(_dataHead + _bytesRead, size).ConfigureAwait(false);
                }

                messageReceived = ParseReceivedMessage(_receiveBuffer, _dataHead, _bytesRead, _receivedFrame);

                if (!_receivedFrame.HeaderReceived)
                {
                    if (_bytesRead == capacity)
                    {
                        Buffer.BlockCopy(_receiveBuffer, _dataHead, _receiveBuffer, 0, _bytesRead);
                        _dataHead = 0;
                        capacity = _receiveBuffer.Length;
                    }
                    continue;
                }

                break;
            }

            int messageLength = _receivedFrame.Length;

            if (!messageReceived)
            {
                if (messageLength > _internalReceiveBufferLimit)
                    throw new WebSocketException(WebSocketCloseStatus.MessageTooBig);

                if (messageLength > _receiveBuffer.Length)
                {
                    capacity = messageLength + (16 << 10);
                    capacity = Math.Min((capacity + (MinimumInternalReceiveBufferSize - 1)) & ~(MinimumInternalReceiveBufferSize - 1), _internalReceiveBufferLimit);
                    GrowInternalReceiveBuffer(capacity);
                }
                else if (messageLength > capacity)
                {
                    Buffer.BlockCopy(_receiveBuffer, _dataHead, _receiveBuffer, 0, _bytesRead);
                    _dataHead = 0;
                    capacity = _receiveBuffer.Length;
                }

                int remainingBytes = messageLength - _bytesRead;

                while (remainingBytes > 0)
                {
                    int size = Math.Min(capacity - _bytesRead, remainingBytes + (16 << 10));

                    _bytesRead += await _connection.ReceiveAsync(_dataHead + _bytesRead, size).ConfigureAwait(false);

                    remainingBytes = messageLength - _bytesRead;
                }

                _receivedFrame._buffer = _receiveBuffer;
                _receivedFrame._payloadHead = _dataHead + _receivedFrame._headerLength;
                UnmaskPayloadData(_receivedFrame);
            }

            _bytesRead -= messageLength;
            _dataHead = _bytesRead == 0 ? 0 : _dataHead + messageLength;
            return;
        }

        private void GrowInternalReceiveBuffer(int size)
        {
            byte[] newBuffer = new byte[size];
            Buffer.BlockCopy(_receiveBuffer, _dataHead, newBuffer, 0, _bytesRead);
            _receiveBuffer = newBuffer;
            _dataHead = 0;
            _connection.SetReceiveBuffer(newBuffer, 0, size);
        }

        private async Task ReceiveMessageAsync(Func<byte[]> getReceiveBuffer)
        {
            byte[] userBuffer = getReceiveBuffer();

            if (userBuffer == null || userBuffer.Length < 64)
            {
                string log =
                    $"WebSocketConnection.ReceiveMessageAsync: {(userBuffer == null ? "getReceiveBuffer returned null" : "The receive buffer length must be greater than or equal to 64")}";
                _server.Log.e(_server.LogKeyword, log);
                throw new WebSocketException(WebSocketCloseStatus.InternalServerError);
            }

            int capacity = userBuffer.Length;
            int bytesRead = 0;

            _receivedFrame.HeaderReceived = false;

            if (_bytesCached != 0)
            {
                if (ParseReceivedMessage(_cache, _cacheHead, _bytesCached, _receivedFrame))
                {
                    int payloadLength = _receivedFrame._payloadLength;

                    if (payloadLength > capacity)
                        throw new WebSocketException(WebSocketCloseStatus.MessageTooBig);

                    Buffer.BlockCopy(_cache, _cacheHead + _receivedFrame._headerLength, userBuffer, 0, payloadLength);
                    _receivedFrame._buffer = userBuffer;
                    _receivedFrame._payloadHead = 0;
                    UnmaskPayloadData(_receivedFrame);

                    int messageLength = _receivedFrame.Length;
                    _bytesCached -= messageLength;
                    _cacheHead = _bytesCached == 0 ? 0 : _cacheHead + messageLength;
                    return;
                }

                if (_receivedFrame.HeaderReceived)
                {
                    if (_receivedFrame._payloadLength > capacity)
                        throw new WebSocketException(WebSocketCloseStatus.MessageTooBig);

                    int headerLength = _receivedFrame._headerLength;
                    bytesRead = _bytesCached - headerLength;
                    Buffer.BlockCopy(_cache, _cacheHead + headerLength, userBuffer, 0, bytesRead);

                    _cacheHead = 0;
                    _bytesCached = 0;
                }
            }

            bool isUserBufferPinned = false;

            try
            {
                if (capacity > AsyncSocketTransceiver.MAX_INTERNAL_BUFFER_SIZE)
                {
                    _connection.SetReceiveBuffer(userBuffer, 0, capacity);
                    isUserBufferPinned = true;
                }

                if (!_receivedFrame.HeaderReceived)
                {
                    byte[] buffer = isUserBufferPinned ? userBuffer : _cache;

                    if (_bytesCached > 0)
                    {
                        Buffer.BlockCopy(_cache, _cacheHead, buffer, 0, _bytesCached);
                        bytesRead = _bytesCached;
                        _cacheHead = 0;
                        _bytesCached = 0;
                    }

                    int bufferSize = Math.Min(CacheSize, buffer.Length);

                    while (true)
                    {
                        if (isUserBufferPinned)
                            bytesRead += await _connection.ReceiveAsync(bytesRead, bufferSize - bytesRead).ConfigureAwait(false);
                        else
                            bytesRead += await _connection.ReceiveAsync(_cache, bytesRead, CacheSize - bytesRead).ConfigureAwait(false);

                        bool messageReceived = ParseReceivedMessage(buffer, 0, bytesRead, _receivedFrame);

                        if (_receivedFrame.HeaderReceived | messageReceived)
                        {
                            if (isUserBufferPinned)
                                Buffer.BlockCopy(userBuffer, 0, _cache, 0, bytesRead);

                            if (messageReceived)
                            {
                                Buffer.BlockCopy(_cache, _receivedFrame._headerLength, userBuffer, 0, _receivedFrame._payloadLength);
                                _receivedFrame._buffer = userBuffer;
                                _receivedFrame._payloadHead = 0;
                                UnmaskPayloadData(_receivedFrame);

                                int messageLength = _receivedFrame.Length;

                                _bytesCached = bytesRead - messageLength;
                                if (_bytesCached > 0)
                                    _cacheHead = messageLength;
                                return;
                            }

                            if (_receivedFrame._payloadLength > capacity)
                                throw new WebSocketException(WebSocketCloseStatus.MessageTooBig);

                            int headerLength = _receivedFrame._headerLength;
                            bytesRead -= headerLength;
                            Buffer.BlockCopy(_cache, headerLength, userBuffer, 0, bytesRead);
                            break;
                        }
                        continue;
                    }
                }

                int payloadLength = _receivedFrame._payloadLength;
                int remainingBytes = payloadLength - bytesRead;

                while (remainingBytes > 0)
                {
                    int size = Math.Min(capacity - bytesRead, remainingBytes + CacheSize);

                    if (isUserBufferPinned)
                        bytesRead += await _connection.ReceiveAsync(bytesRead, size).ConfigureAwait(false);
                    else
                        bytesRead += await _connection.ReceiveAsync(userBuffer, bytesRead, size).ConfigureAwait(false);

                    remainingBytes = payloadLength - bytesRead;
                }

                _receivedFrame._buffer = userBuffer;
                _receivedFrame._payloadHead = 0;
                UnmaskPayloadData(_receivedFrame);

                _bytesCached = bytesRead - payloadLength;
                if (_bytesCached > 0)
                    Buffer.BlockCopy(userBuffer, payloadLength, _cache, 0, _bytesCached);
            }
            finally
            {
                if (isUserBufferPinned)
                    _connection.SetReceiveBuffer(null, 0, 0);
            }
        }

        private bool ParseReceivedMessage(byte[] buffer, int head, int count, ReceivedFrame frame)
        {
            frame.HeaderReceived = false;

            if (count < 2)
                return false;

            byte byte1 = buffer[head];
            byte byte2 = buffer[head + 1];

            bool isFinBitSet = (byte1 & 0x80) == 0x80;
            byte rsv123 = (byte)(byte1 & 0x70);
            WebSocketOpCode opCode;
            bool isMaskBitSet = (byte2 & 0x80) == 0x80;

            if (rsv123 != 0 | !Enum.IsDefined(typeof(WebSocketOpCode), (byte)(byte1 & 0x0F)) | !isMaskBitSet)
                throw new WebSocketException(WebSocketCloseStatus.ProtocolError);

            opCode = (WebSocketOpCode)(byte1 & 0x0F);

            if (_receiveState == WaitForNewMessage)
            {
                if (opCode == WebSocketOpCode.ContinuationFrame | (!isFinBitSet && WebSocket.IsControlFrame(opCode)))
                    throw new WebSocketException(WebSocketCloseStatus.ProtocolError);

                frame._opCode = opCode;
                frame._isFragmentedMessage = !isFinBitSet;
            }
            else // _receiveState == WaitForNewFragment
            {
                if (opCode != WebSocketOpCode.ContinuationFrame)
                    throw new WebSocketException(WebSocketCloseStatus.ProtocolError);
            }

            byte length = (byte)(byte2 & 0x7F);
            byte headerLength;
            int payloadLength;

            switch (length)
            {
                case 126:
                    if (count < 8) // 2 + 2 + 4 (Masking-key)
                        return false;
                    headerLength = 8;
                    payloadLength = _bigEndianConverter.ToUInt16(buffer, head + 2);
                    break;
                case 127:
                    if (count < 14) // 2 + 8 + 4 (Masking-key)
                        return false;

                    ulong value = _bigEndianConverter.ToUInt64(buffer, head + 2);
                    if (value > (int.MaxValue - 14))
                        throw new WebSocketException(WebSocketCloseStatus.MessageTooBig);

                    headerLength = 14;
                    payloadLength = unchecked((int)value);
                    break;
                default:
                    if (count < 6) // 2 + 4 (Masking-key)
                        return false;
                    headerLength = 6;
                    payloadLength = length;
                    break;
            }

            int payloadHead = head + headerLength;

            Buffer.BlockCopy(buffer, payloadHead - 4, frame._maskingKey, 0, 4);
            frame._headerLength = headerLength;
            frame._payloadLength = payloadLength;

            if (count < frame.Length)
                return false;

            frame._buffer = buffer;
            frame._payloadHead = payloadHead;       
            UnmaskPayloadData(frame);
            _receiveState = isFinBitSet ? WaitForNewMessage : WaitForNewFragment;
            return true;
        }

        private void UnmaskPayloadData(ReceivedFrame frame)
        {
            byte[] maskingKey = frame._maskingKey;
            byte[] buffer = frame._buffer;
            int head = frame._payloadHead;
            int length = frame._payloadLength;

            for (int i = 0; i < length; ++i)
            {
                byte originalOctet = buffer[head];
                buffer[head] = (byte)(originalOctet ^ maskingKey[i & 0x03]);
                head = unchecked(head + 1);
            }
        }

        private bool ProcessReceivedFrame(ReceivedFrame frame)
        {
            bool isFinalFragment = _receiveState == WaitForNewMessage;
            byte[] buffer = frame._buffer;
            int payloadHead = frame._payloadHead;
            int payloadLength = frame._payloadLength;

            try
            {
                _lock.Enter();
                if (_disposed | _hasSentCloseFrame)
                {
                    _lock.Leave();
                    return false;
                }
                _lock.Leave();

                switch (frame._opCode)
                {
                    case WebSocketOpCode.ContinuationFrame:
                        throw new BugException("Should not go in here.");
                    case WebSocketOpCode.TextFrame:
                        string text = ParseTextPayloadData(buffer, payloadHead, payloadLength);
                        _onTextFrame(this, buffer, payloadHead, payloadLength, isFinalFragment, text);
                        break;
                    case WebSocketOpCode.BinaryFrame:
                        _onBinaryFrame(this, buffer, payloadHead, payloadLength, isFinalFragment);
                        break;
                    case WebSocketOpCode.ConnectionClose:
                        if (payloadLength == 1 | payloadLength >= 126)
                            throw new WebSocketException(WebSocketCloseStatus.ProtocolError);

                        if (payloadLength == 0)
                        {
                            _server.Log.i(_server.LogKeyword, $"The websocket connection [{SessionKey}] receives a Close frame");
                            SendCloseFrame((ushort)WebSocketCloseStatus.Empty);
                        }
                        else
                        {
                            ushort statusCode = _bigEndianConverter.ToUInt16(buffer, payloadHead);

                            if (statusCode < 1000 | statusCode > 4999)
                                throw new WebSocketException(WebSocketCloseStatus.ProtocolError);

#if DEBUG
                            if (payloadLength > 2)
                            {
                                string reason = ParseTextPayloadData(buffer, payloadHead + 2, payloadLength - 2);
                                _server.Log.i(_server.LogKeyword, $"The websocket connection [{SessionKey}] receives a Close frame ({statusCode.ToString()}: {reason})");
                            }
                            else
#endif
                            _server.Log.i(_server.LogKeyword, $"The websocket connection [{SessionKey}] receives a Close frame ({statusCode.ToString()})");

                            SendCloseFrame(statusCode);
                        }
                        break;
                    case WebSocketOpCode.Ping:
                        if (payloadLength >= 126)
                            throw new WebSocketException(WebSocketCloseStatus.ProtocolError);
                        _server.Log.i(_server.LogKeyword, $"The websocket connection [{SessionKey}] receives a Ping frame");
                        SendPongFrame(buffer, payloadHead, payloadLength);
                        break;
                    case WebSocketOpCode.Pong:
                        if (payloadLength >= 126)
                            throw new WebSocketException(WebSocketCloseStatus.ProtocolError);
                        break;
                }

                return true;
            }
            finally
            {
                frame._buffer = null;
            }
        }

        public void SendTextFrame(string text)
        {
            SendTextFrame(true, text);
        }

        public void SendTextFrame(bool isFinalFragment, string text)
        {
            byte[] payload = Encoding.UTF8.GetBytes(text);

            SendFrame(isFinalFragment, WebSocketOpCode.TextFrame, payload, 0, payload.Length);
        }

        public void SendBinaryFrame(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            SendBinaryFrame(true, data, 0, data.Length);
        }

        public void SendBinaryFrame(bool isFinalFragment, byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            SendBinaryFrame(isFinalFragment, data, 0, data.Length);
        }

        public void SendBinaryFrame(byte[] data, int offset, int count)
        {
            SendBinaryFrame(true, data, offset, count);
        }

        public void SendBinaryFrame(bool isFinalFragment, byte[] data, int offset, int count)
        {
            if (data == null)
                throw new ArgumentNullException("data");
            if (offset < 0)
                throw new ArgumentOutOfRangeException("offset");
            if (count < 0)
                throw new ArgumentOutOfRangeException("count");
            if (data.Length - offset < count)
                throw new ArgumentException("Offset and count must refer to a location within the buffer.");

            SendFrame(isFinalFragment, WebSocketOpCode.BinaryFrame, data, offset, count);
        }

        private void SendFrame(bool isFinalFragment, WebSocketOpCode opCode, byte[] data, int offset, int count)
        {
            byte[] buffer;

            byte[] header = WebSocket.BuildHeader(isFinalFragment, opCode, count);

            if (count == 0)
                buffer = header;
            else
            {
                int headerLength = header.Length;

                buffer = new byte[headerLength + count];
                Buffer.BlockCopy(header, 0, buffer, 0, headerLength);
                Buffer.BlockCopy(data, offset, buffer, headerLength, count);
            }

            SendFrameAsync(new OutputFrame(opCode, buffer, 0, buffer.Length));
        }

        public void SendFrame(byte[] frame, Action<byte[], int> returnBuffer = null)
        {
            if (frame == null)
                throw new ArgumentNullException("frame");

            SendFrame(frame, 0, frame.Length, returnBuffer);
        }

        public void SendFrame(byte[] frame, int offset, int count, Action<byte[], int> returnBuffer = null)
        {
            if (frame == null)
                throw new ArgumentNullException("frame");
            if (count < 2)
                throw new ArgumentOutOfRangeException("count");

            byte byte1 = frame[0];

            if (!Enum.IsDefined(typeof(WebSocketOpCode), (byte)(byte1 & 0x0F)))
                throw new ArgumentException("The parsed WebSocket Opcode was invalid.");

            WebSocketOpCode opCode = (WebSocketOpCode)(byte1 & 0x0F);

            SendFrameAsync(new OutputFrame(opCode, frame, offset, count, returnBuffer));
        }

        public void Close(WebSocketCloseStatus closeStatus = WebSocketCloseStatus.NormalClosure)
        {
            SendCloseFrame((ushort)closeStatus);
        }

        private void SendCloseFrame(ushort closeStatus)
        {
            byte[] buffer = new byte[4];

            byte finBitFlag = 0x80;

            buffer[0] = (byte)(finBitFlag | (byte)WebSocketOpCode.ConnectionClose);
            buffer[1] = 2;
            buffer[2] = (byte)((closeStatus & 0xff00) >> 16);
            buffer[3] = (byte)(closeStatus & 0x00ff);

            SendFrameAsync(new OutputFrame(WebSocketOpCode.ConnectionClose, buffer, 0, buffer.Length));
        }

        private void SendPongFrame(byte[] data, int offset, int count)
        {
            byte[] buffer = new byte[2 + count];

            byte finBitFlag = 0x80;

            buffer[0] = (byte)(finBitFlag | (byte)WebSocketOpCode.Pong);
            buffer[1] = (byte)count;

            if (count > 0)
                Buffer.BlockCopy(data, offset, buffer, 2, count);

            SendFrameAsync(new OutputFrame(WebSocketOpCode.Pong, buffer, 0, buffer.Length));
        }

        private async void SendFrameAsync(OutputFrame frame)
        {
            bool closed = false;

            _lock.Enter();
            try
            {
                if (_disposed | _hasSentCloseFrame)
                    closed = true;
                else
                {
                    if (frame.OpCode == WebSocketOpCode.ConnectionClose)
                    {
                        _hasSentCloseFrame = true;
                        if (_isSending)
                        {
                            _outputFrames.Push(frame);
                            return;
                        }
                    }
                    else
                    {
                        if (_isSending)
                        {
                            _outputFrames.Add(frame);
                            return;
                        }
                    }
                    _isSending = true;
                }
            }
            finally
            {
                _lock.Leave();
            }

            if (closed)
            {
                frame.ReturnBuffer();
                return;
            }

            var nextFrame = await SendAsync(frame, false).ConfigureAwait(false);

            if (nextFrame != null)
                ThreadPool.QueueUserWorkItem(f => SendAsync(f as OutputFrame, true).Forget(), nextFrame);
        }

        private async Task<OutputFrame> SendAsync(OutputFrame frame, bool infinite)
        {
            try
            {
                while (true)
                {
                    BufferedOutputMessage data = frame.BufferedData;

                    await _connection.SendAsync(data.Buffer, data.Offset, data.Count).ConfigureAwait(false);

                    frame.ReturnBuffer();

                    if (frame.OpCode == WebSocketOpCode.ConnectionClose)
                    {
                        _server.CloseSession(_connection);
                        return null;
                    }

                    _lock.Enter();
                    try
                    {
                        if (_disposed)
                            return null;

                        frame = _outputFrames.Pop();
                        if (frame == null)
                        {
                            _isSending = false;
                            return null;
                        }
                    }
                    finally
                    {
                        _lock.Leave();
                    }

                    if (!infinite)
                        return frame;
                }
            }
            catch (Exception e)
            {
                if (e is SocketException)
                    _server.Log.d(_server.LogKeyword, $"WebSocketConnection.SendAsync:{Environment.NewLine}{e}");
                else if (!(e is ObjectDisposedException))
                    _server.Log.e(_server.LogKeyword, "WebSocketConnection.SendAsync:", e);

                frame.ReturnBuffer();
                _server.CloseSession(_connection);
                return null;
            }
        }
    }
}
