﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Http.WebSockets
{
    public enum WebSocketOpCode : byte
    {
        ContinuationFrame = 0x00,
        TextFrame = 0x01,
        BinaryFrame = 0x02,
        ConnectionClose = 0x08,
        Ping = 0x09,
        Pong = 0x0A
    }
}
