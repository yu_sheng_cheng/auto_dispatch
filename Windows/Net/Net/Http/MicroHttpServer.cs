﻿using Net.AsyncSocket.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Net.Http
{
    public class MicroHttpServer : AsyncTcpServer
    {
        private Action<IMicroHttpConnection> _configureConnection;

        public MicroHttpServer(int maxConnectionsAllowed) : base(maxConnectionsAllowed) { }

        public MicroHttpServer(int port, int maxConnectionsAllowed) : base(port, maxConnectionsAllowed) { }

        public MicroHttpServer(IPAddress listenedAddress, int listenedPort, int maxConnectionsAllowed) : base(listenedAddress, listenedPort, maxConnectionsAllowed) { }

        public MicroHttpServer(IPEndPoint listenedEndPoint, int maxConnectionsAllowed) : base(listenedEndPoint, maxConnectionsAllowed) { }

        public void Start(Action<IMicroHttpConnection> configure)
        {
            Start(ListenedEndPoint, configure);
        }

        public void Start(IPAddress listenedAddress, int listenedPort, Action<IMicroHttpConnection> configure)
        {
            Start(new IPEndPoint(listenedAddress, listenedPort), configure);
        }

        public void Start(IPEndPoint listenedEndPoint, Action<IMicroHttpConnection> configure)
        {
            if (configure == null)
                throw new ArgumentNullException("configure");

            Start(listenedEndPoint, () => _configureConnection = configure);
        }

        protected override AsyncTcpSession CreateAsyncTcpSessionInstance(Socket clientSocket)
        {
            return new MicroHttpConnection(this, clientSocket, SocketAwaitablePool);
        }

        protected override void OnClientConnected(ClientConnectedEventArgs e)
        {
            MicroHttpConnection connection = e.Session as MicroHttpConnection;

            _configureConnection(connection);

            ThreadPool.QueueUserWorkItem((_) => connection.StartReceivingAsync());
        }
    }
}
