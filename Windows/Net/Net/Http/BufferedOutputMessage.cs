﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Http
{
    public sealed class BufferedOutputMessage
    {
        public byte[] Buffer { get; }

        public int Offset { get; }

        public int Count { get; }

        public BufferedOutputMessage(byte[] buffer, int offset, int count)
        {
            Buffer = buffer ?? throw new ArgumentNullException("buffer");

            if (offset < 0)
                throw new ArgumentOutOfRangeException("offset");
            if (count <= 0)
                throw new ArgumentOutOfRangeException("count");
            if (buffer.Length - offset < count)
                throw new ArgumentException("Offset and count must refer to a location within the buffer.");

            Offset = offset;
            Count = count;
        }
    }
}
