﻿using Net.AsyncSocket;
using Net.AsyncSocket.Tcp;
using SwissKnife;
using SwissKnife.Exceptions;
using SwissKnife.Log;
using SwissKnife.ObjectPool;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Net.Http.HttpClients
{
    public class MicroHttpClient : AsyncTcpClient
    {
        private const int MinimumReceiveBufferSize = 4 << 10;
        private const int MaximumReceiveBufferSize = 1 << 20;
        private const int DefaultReceiveBufferSize = 8 << 10;

        private class Node
        {
            private HttpContext _prev;
            [ContractPublicPropertyName("First")]
            private HttpContext _next;

            internal HttpContext First { get { return _next; } }

            internal void Add(HttpContext context)
            {
                if (_prev == null)
                    _next = context;
                else
                    _prev._next = context;

                _prev = context;
            }

            internal HttpContext Pop()
            {
                HttpContext first = _next;

                if (first == null)
                    return null;

                _next = first._next;
                if (_next == null)
                    _prev = null;

                return first;
            }
        }

        private ILog _log = new Debug();
        private Node _pendingSendContexts = new Node();
        private Node _hasSentContexts = new Node();
        private int _receiveBufferLimit;
        private byte[] _receiveBuffer;
        private int _dataHead;
        private int _bytesRead;
        private bool _isSending;
        private bool _disposed;
        private readonly LightSpinLock _lock = new LightSpinLock();

        public ILog Log
        {
            get { return _log; }
            set
            {
                _log = value ?? new Debug();
            }
        }

        public string LogKeyword { get; set; } = "HttpClient";

        public int ReceiveBufferLimit { get; set; } = MaximumReceiveBufferSize;

        public MicroHttpClient(IPAddress remoteAddress, int remotePort, IObjectPool<SocketAwaitable> socketAwaitablePool = null) : base(remoteAddress, remotePort, socketAwaitablePool) { }

        public MicroHttpClient(IPEndPoint remoteEndPoint, IObjectPool<SocketAwaitable> socketAwaitablePool = null) : base(remoteEndPoint, socketAwaitablePool) { }

        protected override void CleanUp()
        {
            Node pendingSendContexts = _pendingSendContexts;

            _lock.Enter();
            _disposed = true;
            _pendingSendContexts = null;
            _hasSentContexts = null;
            _lock.Leave();

            HttpContext context;

            while ((context = pendingSendContexts.Pop()) != null)
            {
                context.ReturnBuffer();
            }

            base.CleanUp();
        }

        public new async Task ConnectAsync()
        {
            await base.ConnectAsync().ConfigureAwait(false);

            ThreadPool.QueueUserWorkItem((_) => StartReceivingAsync());
        }

        private async void StartReceivingAsync()
        {
            _receiveBufferLimit = ReceiveBufferLimit;
            Thread.MemoryBarrier();

            if (_receiveBufferLimit >= MaximumReceiveBufferSize)
                _receiveBufferLimit = MaximumReceiveBufferSize;
            else if (_receiveBufferLimit <= MinimumReceiveBufferSize)
                _receiveBufferLimit = MinimumReceiveBufferSize;
            else
                _receiveBufferLimit = (_receiveBufferLimit + (MinimumReceiveBufferSize - 1)) & ~(MinimumReceiveBufferSize - 1);

            int receiveBufferSize = DefaultReceiveBufferSize <= _receiveBufferLimit ? DefaultReceiveBufferSize : _receiveBufferLimit;

            _receiveBuffer = new byte[receiveBufferSize];

            try
            {
                SetReceiveBuffer(_receiveBuffer, 0, receiveBufferSize);

                while (true)
                {
                    HttpResponse response = await ReceiveHttpResponseAsync().ConfigureAwait(false);
                    HttpContext context;

                    _lock.Enter();
                    try
                    {
                        if (_disposed)
                            break;

                        context = _hasSentContexts.Pop();
                    }
                    finally
                    {
                        _lock.Leave();
                    }

                    if (context == null)
                        throw new BugException("Should not go in here.");

                    context.OnIncomingResponse(response);

                    if (!context.Request.KeepAlive | !response.KeepAlive)
                        break;

                    response = null;
                    context = null;
                }
            }
            catch (Exception e)
            {
                if (!(e is ObjectDisposedException))
                    _log.e(LogKeyword, "HttpClient.StartReceivingAsync:", e);

                Dispose();
            }
        }

        private async Task<HttpResponse> ReceiveHttpResponseAsync()
        {
            int capacity = _receiveBuffer.Length - _dataHead;
            bool stickyPacket = _bytesRead != 0;

            HttpResponse response;

            while (true)
            {
                if (stickyPacket)
                    stickyPacket = false;
                else
                {
                    int size = Math.Min(capacity - _bytesRead, 16 << 10);

                    _bytesRead += await ReceiveAsync(_dataHead + _bytesRead, size).ConfigureAwait(false);
                }

                if (!HttpResponse.TryParse(_receiveBuffer, _dataHead, _bytesRead, out response))
                {
                    if (_bytesRead == capacity)
                    {
                        if (_dataHead != 0)
                        {
                            Buffer.BlockCopy(_receiveBuffer, _dataHead, _receiveBuffer, 0, _bytesRead);
                            _dataHead = 0;
                            capacity = _receiveBuffer.Length;
                        }
                        else
                        {
                            if (capacity >= _receiveBufferLimit)
                                throw new OutOfLimitException($"Http response header too large to fit in buffer.");

                            capacity += MinimumReceiveBufferSize;
                            GrowBuffer(capacity);
                        }
                    }
                    continue;
                }

                break;
            }

            int bytesConsumed;

            if (response.HasEntityBody && response.Body == null)
            {
                int headerLength = response.HeaderLength;
                int contentLength = response.ContentLength;

                if (contentLength > (_receiveBufferLimit - headerLength))
                    throw new OutOfLimitException($"Http response body too large to fit in buffer.");

                int responseLength = headerLength + contentLength;

                _dataHead += headerLength;
                _bytesRead -= headerLength;

                if (responseLength > _receiveBuffer.Length)
                    GrowBuffer((responseLength + (MinimumReceiveBufferSize - 1)) & ~(MinimumReceiveBufferSize - 1));
                else if (contentLength > _receiveBuffer.Length - _dataHead)
                {
                    Buffer.BlockCopy(_receiveBuffer, _dataHead, _receiveBuffer, 0, _bytesRead);
                    _dataHead = 0;
                }

                capacity = _receiveBuffer.Length - _dataHead;
                int remainingBytes = contentLength - _bytesRead;

                while (remainingBytes > 0)
                {
                    int size = Math.Min(capacity - _bytesRead, remainingBytes + (16 << 10));

                    _bytesRead += await ReceiveAsync(_dataHead + _bytesRead, size).ConfigureAwait(false);

                    remainingBytes = contentLength - _bytesRead;
                }

                response.ParseBody(_receiveBuffer, _dataHead, contentLength);

                bytesConsumed = contentLength;
            }
            else
                bytesConsumed = response.HeaderLength + response.ContentLength;

            _bytesRead -= bytesConsumed;
            _dataHead = _bytesRead == 0 ? 0 : _dataHead + bytesConsumed;
            return response;
        }

        private void GrowBuffer(int size)
        {
            byte[] newBuffer = new byte[size];
            Buffer.BlockCopy(_receiveBuffer, _dataHead, newBuffer, 0, _bytesRead);
            _receiveBuffer = newBuffer;
            _dataHead = 0;
            SetReceiveBuffer(newBuffer, 0, size);
        }

        public async void SendRequestAsync(HttpContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            bool disposed = false;

            _lock.Enter();
            try
            {
                if (_disposed)
                    disposed = true;
                else
                {
                    if (_isSending)
                    {
                        _pendingSendContexts.Add(context);
                        return;
                    }

                    _isSending = true;
                    _hasSentContexts.Add(context);
                }
            }
            finally
            {
                _lock.Leave();
            }

            if (disposed)
            {
                context.ReturnBuffer();
                return;
            }

            var nextContext = await SendAsync(context, false).ConfigureAwait(false);

            if (nextContext != null)
                ThreadPool.QueueUserWorkItem(c => SendAsync(c as HttpContext, true).Forget(), nextContext);
        }

        private async Task<HttpContext> SendAsync(HttpContext context, bool infinite)
        {
            try
            {
                while (true)
                {
                    BufferedOutputMessage request = context.BufferedResponseData;

                    await SendAsync(request.Buffer, request.Offset, request.Count).ConfigureAwait(false);

                    context.ReturnBuffer();

                    _lock.Enter();
                    try
                    {
                        if (_disposed)
                            return null;

                        context = _pendingSendContexts.Pop();
                        if (context == null)
                        {
                            _isSending = false;
                            return null;
                        }

                        _hasSentContexts.Add(context);
                    }
                    finally
                    {
                        _lock.Leave();
                    }

                    if (!infinite)
                        return context;
                }
            }
            catch (Exception e)
            {
                if (!(e is ObjectDisposedException))
                    _log.e(LogKeyword, "HttpClient.SendAsync:", e);

                context.ReturnBuffer();
                Dispose();
                return null;
            }
        }
    }
}
