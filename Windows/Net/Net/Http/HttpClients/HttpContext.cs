﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Http.HttpClients
{
    public sealed class HttpContext
    {
        private readonly MicroHttpClient _client;
        private bool _hasSentRequest;
        private Action<HttpContext> _onIncomingResponse;
        private Action<byte[], int> _returnBuffer;
        internal HttpContext _next;

        public HttpRequest Request { get; } = new HttpRequest();

        public HttpResponse Response { get; private set; }

        internal BufferedOutputMessage BufferedResponseData { get; set; }

        public HttpContext(MicroHttpClient client)
        {
            _client = client ?? throw new ArgumentNullException("client");
        }
   
        public void SendRequest(byte[] data, Action<HttpContext> onIncomingResponse, Action<byte[], int> returnBuffer = null)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            SendRequest(data, 0, data.Length, onIncomingResponse, returnBuffer);
        }

        public void SendRequest(byte[] data, int offset, int count, Action<HttpContext> onIncomingResponse, Action<byte[], int> returnBuffer = null)
        {
            if (_hasSentRequest)
                throw new InvalidOperationException("The request has already been sent.");

            BufferedResponseData = new BufferedOutputMessage(data, offset, count);
            _onIncomingResponse = onIncomingResponse ?? throw new ArgumentNullException("onIncomingResponse");
            _returnBuffer = returnBuffer;
            _hasSentRequest = true;

            _client.SendRequestAsync(this);
        }

        internal void OnIncomingResponse(HttpResponse response)
        {
            if (!_hasSentRequest)
                throw new InvalidOperationException("The request not sent.");

            Response = response;
            try { _onIncomingResponse.Invoke(this); } catch { }
        }

        internal void ReturnBuffer()
        {
            if (!_hasSentRequest)
                throw new InvalidOperationException("The request not sent.");

            if (_returnBuffer != null)
            {
                try { _returnBuffer.Invoke(BufferedResponseData.Buffer, BufferedResponseData.Offset); } catch { }
            }
        }
    }
}
