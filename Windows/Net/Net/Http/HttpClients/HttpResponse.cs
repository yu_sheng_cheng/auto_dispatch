﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Net.Http.HttpClients
{
    public sealed class HttpResponse : IEnumerable<string>
    {
        private const string HttpResponseRegexPattern = @"^HTTP\/1\.1\s(?<STATUS_CODE>[1-9][0-9][0-9])\s(?<REASON_PHRASE>[^\r\n]+)\r\n" + // status line
                                                        @"((?<FIELD_NAME>[^:\r\n]+):(?([^\r\n])\s)*(?<FIELD_VALUE>[^\r\n]*)\r\n)+" + // headers
                                                        @"\r\n"; // newline

        private static readonly Regex HttpResponseRegex = new Regex(HttpResponseRegexPattern, RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled);

        public static bool TryParse(byte[] bytes, out HttpResponse result)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");

            return TryParse(bytes, 0, bytes.Length, out result);
        }

        public static bool TryParse(byte[] bytes, int index, int count, out HttpResponse result)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");

            if (count < 0)
                throw new ArgumentOutOfRangeException("count");

            if (index < 0)
                throw new ArgumentOutOfRangeException("index");

            if (bytes.Length - index < count)
                throw new ArgumentException("Index and count must refer to a location within the buffer.");

            result = null;

            if (count < 4)
                return false;

            int start = index;
            int end = start + count - 4;

            while (start <= end)
            {
                if (bytes[start] != 0x0d || ((bytes[start] << 24) | (bytes[start + 1] << 16) | (bytes[start + 2] << 8) | bytes[start + 3]) != 0x0d0a0d0a) // \r\n\r\n
                {
                    ++start;
                    continue;
                }

                int headerLength = (start - index) + 4;

                var text = Encoding.UTF8.GetString(bytes, index, headerLength);

                Match match = HttpResponseRegex.Match(text);

                if (!match.Success)
                    throw new FormatException("Can't parse the http response headers.");

                var response = new HttpResponse
                {
                    HeaderLength = headerLength,
                    StatusCode = ushort.Parse(match.Groups["STATUS_CODE"].Value),
                    ReasonPhrase = match.Groups["REASON_PHRASE"].Value
                };

                var names = match.Groups["FIELD_NAME"].Captures;
                var values = match.Groups["FIELD_VALUE"].Captures;
                int fields = names.Count;
                for (int i = 0; i < fields; ++i)
                {
                    var name = names[i].ToString();
                    var value = values[i].ToString();
                    response._headers[name] = value;
                }

                string fieldValue;

                if (response._headers.TryGetValue("Connection", out fieldValue))
                {
                    if (string.Compare(fieldValue, "close", StringComparison.OrdinalIgnoreCase) == 0)
                        response.KeepAlive = false;
                }

                if (response._headers.TryGetValue("Content-Length", out fieldValue))
                {
                    int contentLength;

                    if (!int.TryParse(fieldValue, out contentLength))
                        throw new FormatException("Can't parse Content-Length.");

                    if (contentLength < 0)
                        throw new FormatException("Content length cannot be less than 0.");

                    response.ContentLength = contentLength;

                    if (contentLength > 0)
                    {
                        if (response._headers.TryGetValue("Content-Type", out fieldValue))
                        {
                            fieldValue = fieldValue.Replace(" ", "");

                            string[] components = fieldValue.Split(new char[] { ';' });

                            if (components.Length == 0)
                                throw new FormatException("The 'Content-Type' header cannot be an empty string.");

                            response.ContentType = components[0];

                        }

                        if (count - headerLength >= contentLength)
                            response.ParseBody(bytes, index + headerLength, contentLength);
                    }
                }
                else if (response._headers.TryGetValue("Transfer-Encoding", out fieldValue))
                {
                    if (string.Compare(fieldValue, "chunked", StringComparison.OrdinalIgnoreCase) == 0)
                        response.SendChunked = true;

                    throw new NotImplementedException("The Transfer-Encoding option is not supported.");
                }

                result = response;
                return true;
            }

            return false;
        }

        private readonly IDictionary<string, string> _headers = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        public int HeaderLength { get; private set; }

        public ushort StatusCode { get; private set; }

        public string ReasonPhrase { get; private set; }

        public bool KeepAlive { get; private set; } = true;

        public string ContentType { get; private set; } = "text/plain";

        public int ContentLength { get; private set; }

        public bool SendChunked { get; private set; }

        public bool HasEntityBody { get { return ContentLength != 0 | SendChunked; } }

        public string Body { get; set; }

        public string this[string name]
        {
            get
            {
                _headers.TryGetValue(name, out string value);
                return value;
            }
        }

        public bool ContainsHeader(string name)
        {
            return _headers.ContainsKey(name);
        }

        public bool TryGetHeader(string name, out string value)
        {
            return _headers.TryGetValue(name, out value);
        }

        public IEnumerator<string> GetEnumerator()
        {
            foreach (var item in _headers)
            {
                yield return item.Key;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void ParseBody(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            ParseBody(data, 0, data.Length);
        }

        public void ParseBody(byte[] data, int index, int count)
        {
            if (!HasEntityBody)
                return;

            if (count < ContentLength)
                throw new ArgumentOutOfRangeException("count");

            Body = Encoding.UTF8.GetString(data, index, ContentLength);
        }
    }
}
