﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Http.HttpClients
{
    public sealed class HttpRequest
    {
        public static byte[] BuildMessage(HttpRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            request.ContentLength = 0;
            return Encoding.UTF8.GetBytes(request.ToString());
        }

        public static byte[] BuildMessage(HttpRequest request, byte[] content)
        {
            if (content == null || content.Length == 0)
                return BuildMessage(request);

            if (request == null)
                throw new ArgumentNullException("request");

            int contentLength = content.Length;

            request.ContentLength = contentLength;

            byte[] header = Encoding.UTF8.GetBytes(request.ToString());

            int headerLength = header.Length;

            byte[] buffer = new byte[headerLength + contentLength];
            Buffer.BlockCopy(header, 0, buffer, 0, headerLength);
            Buffer.BlockCopy(content, 0, buffer, headerLength, contentLength);
            return buffer;
        }

        private readonly StringBuilder _headers = new StringBuilder();
        private string _method = "GET";
        private string _path = "/";
        private string _contentType = "text/plain";
        private int _contentLength;

        public string Method
        {
            get { return _method; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("The method name cannot be an empty string.", "Method");

                _method = value;
            }
        }

        public string Path
        {
            get { return _path; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("The path cannot be an empty string.", "Path");

                _path = value;
            }
        }

        public bool KeepAlive { get; set; } = true;

        public string ContentType
        {
            get { return _contentType; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("ContentType");
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("The 'ContentType' property cannot be an empty string.", "ContentType");

                _contentType = value;
            }
        }

        public int ContentLength
        {
            get { return _contentLength; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("ContentLength");

                _contentLength = value;
            }
        }

        public void AddHeader(string name, string value)
        {
            if (name == null)
                throw new ArgumentNullException("name");
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("The header name cannot be an empty string.", "name");
            if (value == null)
                throw new ArgumentNullException("value");

            _headers.Append($"{name}: {value}\r\n");
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append($"{Method} {Path} HTTP/1.1\r\n");

            sb.Append($"Content-Length: {_contentLength.ToString()}\r\n");

            if (_contentLength != 0)
                sb.Append($"Content-Type: {ContentType}; charset={Encoding.UTF8.HeaderName}\r\n");

            if (_headers.Length > 0)
                sb.Append(_headers.ToString());

            sb.Append("\r\n");
            return sb.ToString();
        }
    }
}
