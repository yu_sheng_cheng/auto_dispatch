﻿using Net.AsyncSocket;
using Net.AsyncSocket.Tcp;
using Net.Http.WebSockets;
using SwissKnife;
using SwissKnife.Exceptions;
using SwissKnife.ObjectPool;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Net.Http
{
    public interface IMicroHttpConnection
    {
        IPEndPoint RemoteEndPoint { get; }

        int ReceiveBufferLimit { get; set; }

        Action<IHttpContext> OnRequest { get; set; }

        Action<IWebSocketConnection> ConfigureWebSocketConnection { get; set; }
    }

    public class MicroHttpConnection : AsyncTcpSession, IMicroHttpConnection
    {
        private const int MinimumReceiveBufferSize = 4 << 10;
        private const int MaximumReceiveBufferSize = 1 << 20;
        private const int DefaultReceiveBufferSize = 8 << 10;

        private class Node
        {
            private HttpContext _prev;
            [ContractPublicPropertyName("First")]
            private HttpContext _next;

            internal HttpContext First { get { return _next; } }

            internal void Add(HttpContext context)
            {
                if (_prev == null)
                    _next = context;
                else
                    _prev._next = context;

                _prev = context;
            }

            internal HttpContext Pop()
            {
                HttpContext first = _next;

                if (first == null)
                    return null;

                _next = first._next;
                if (_next == null)
                    _prev = null;

                return first;
            }
        }

        private Action<IHttpContext> _onRequest = (_) => { };
        private Action<IWebSocketConnection> _configureWebSocketConnection;

        private WebSocketConnection _webSocketConnection;
        private Node _httpContexts = new Node();
        private HashSet<HttpContext> _hasSentResponseContexts = new HashSet<HttpContext>();
        private int _receiveBufferLimit;
        private byte[] _receiveBuffer;
        private int _dataHead;
        private int _bytesRead;
        private bool _isSending;
        private bool _disposed;
        private readonly LightSpinLock _lock = new LightSpinLock();

        public int ReceiveBufferLimit { get; set; } = MaximumReceiveBufferSize;

        public Action<IHttpContext> OnRequest
        {
            get { return _onRequest; }
            set
            {
                _onRequest = value ?? throw new ArgumentNullException("OnRequest");
            }
        }

        public Action<IWebSocketConnection> ConfigureWebSocketConnection
        {
            get { return _configureWebSocketConnection; }
            set { _configureWebSocketConnection = value; }
        }

        public MicroHttpConnection(MicroHttpServer server, Socket socket, IObjectPool<SocketAwaitable> socketAwaitablePool = null) : base(server, socket, socketAwaitablePool)
        {

        }

        protected override void CleanUp()
        {
            HashSet<HttpContext> hasSentResponseContexts = _hasSentResponseContexts;
            WebSocketConnection webSocketConnection;

            _lock.Enter();
            _disposed = true;
            webSocketConnection = _webSocketConnection;
            _httpContexts = null;
            _hasSentResponseContexts = null;
            _lock.Leave();

            foreach (var context in hasSentResponseContexts)
            {
                context.ReturnBuffer();
            }

            if (webSocketConnection != null)
                webSocketConnection.Dispose();

            base.CleanUp();
        }

        public async void StartReceivingAsync()
        {
            if (ReceiveBufferLimit >= MaximumReceiveBufferSize)
                _receiveBufferLimit = MaximumReceiveBufferSize;
            else if (ReceiveBufferLimit <= MinimumReceiveBufferSize)
                _receiveBufferLimit = MinimumReceiveBufferSize;
            else
                _receiveBufferLimit = (ReceiveBufferLimit + (MinimumReceiveBufferSize - 1)) & ~(MinimumReceiveBufferSize - 1);

            int receiveBufferSize = DefaultReceiveBufferSize <= _receiveBufferLimit ? DefaultReceiveBufferSize : _receiveBufferLimit;

            _receiveBuffer = new byte[receiveBufferSize];

            try
            {
                SetReceiveBuffer(_receiveBuffer, 0, receiveBufferSize);

                bool firstRequest = true;

                while (true)
                {
                    HttpRequest request = await ReceiveHttpRequestAsync().ConfigureAwait(false);

                    if (request.IsWebSocketRequest)
                    {
                        if (!firstRequest | _bytesRead != 0)
                        {
                            if (!firstRequest)
                                Server.Log.d(Server.LogKeyword, "The necessary of the first request to be upgraded to WebSocket");
                            else
                                Server.Log.d(Server.LogKeyword, "It can't be resent the data before upgraded to WebSocket incompleted");
                            Server.CloseSession(this);
                            break;
                        }

                        Action<IWebSocketConnection> configureWebSocketConnection = Volatile.Read(ref _configureWebSocketConnection);
                        if (configureWebSocketConnection == null)
                        {
                            await SendFailureResponseAsync(new HttpResponse(510));
                            Server.CloseSession(this);
                            break;
                        }

                        SetReceiveBuffer(null, 0, 0);
                        _receiveBuffer = null;

                        if (!WebSocket.IsDraftIetfHybi13Version(request))
                        {
                            HttpResponse response = new HttpResponse(426);
                            response.AddHeader("Sec-WebSocket-Version", "13");
                            await SendFailureResponseAsync(response);
                            Server.CloseSession(this);
                            break;
                        }

                        HttpResponse handshakeResponse;

                        if (!WebSocket.BuildHandshakeResponse(request, out handshakeResponse))
                        {
                            await SendFailureResponseAsync(new HttpResponse(400));
                            Server.CloseSession(this);
                            break;
                        }

                        await SendResponseAsync(handshakeResponse);

                        WebSocketConnection webSocketConnection = new WebSocketConnection(this);
                        configureWebSocketConnection(webSocketConnection);

                        _lock.Enter();
                        try
                        {
                            if (_disposed)
                            {
                                webSocketConnection.Dispose();
                                break;
                            }

                            _webSocketConnection = webSocketConnection;
                        }
                        finally
                        {
                            _lock.Leave();
                        }

                        webSocketConnection.OnOpen(webSocketConnection);

                        await webSocketConnection.StartReceivingAsync();
                        break;
                    }
                    firstRequest = false;

                    HttpContext context = new HttpContext(this, request);

                    _lock.Enter();
                    try
                    {
                        if (_disposed)
                            break;

                        _httpContexts.Add(context);
                    }
                    finally
                    {
                        _lock.Leave();
                    }

                    _onRequest(context);

                    if (!request.KeepAlive | !context.Response.KeepAlive)
                        break;

                    request = null;
                    context = null;
                }
            }
            catch (Exception e)
            {
                if (e is SocketException)
                    Server.Log.d(Server.LogKeyword, $"MicroHttpConnection.StartReceivingAsync:{Environment.NewLine}{e}");
                else if (!(e is ObjectDisposedException))
                    Server.Log.e(Server.LogKeyword, "MicroHttpConnection.StartReceivingAsync:", e);

                Server.CloseSession(this);
            }
        }

        private async Task<HttpRequest> ReceiveHttpRequestAsync()
        {
            int capacity = _receiveBuffer.Length - _dataHead;
            bool stickyPacket = _bytesRead != 0;

            HttpRequest request;

            while (true)
            {
                if (stickyPacket)
                    stickyPacket = false;
                else
                {
                    int size = Math.Min(capacity - _bytesRead, 16 << 10);

                    _bytesRead += await ReceiveAsync(_dataHead + _bytesRead, size).ConfigureAwait(false);
                }

                if (!HttpRequest.TryParse(_receiveBuffer, _dataHead, _bytesRead, out request))
                {
                    if (_bytesRead == capacity)
                    {
                        if (_dataHead != 0)
                        {
                            Buffer.BlockCopy(_receiveBuffer, _dataHead, _receiveBuffer, 0, _bytesRead);
                            _dataHead = 0;
                            capacity = _receiveBuffer.Length;
                        }
                        else
                        {
                            if (capacity >= _receiveBufferLimit)
                                throw new OutOfLimitException($"Http request header too large to fit in buffer.");

                            capacity += MinimumReceiveBufferSize;
                            GrowBuffer(capacity);
                        }
                    }
                    continue;
                }

                break;
            }

            int bytesConsumed;

            if (request.HasEntityBody && request.Body == null)
            {
                int headerLength = request.HeaderLength;
                int contentLength = request.ContentLength;

                if (contentLength > (_receiveBufferLimit - headerLength))
                    throw new OutOfLimitException($"Http request body too large to fit in buffer.");

                int requestLength = headerLength + contentLength;

                _dataHead += headerLength;
                _bytesRead -= headerLength;

                if (requestLength > _receiveBuffer.Length)
                    GrowBuffer((requestLength + (MinimumReceiveBufferSize - 1)) & ~(MinimumReceiveBufferSize - 1));
                else if (contentLength > _receiveBuffer.Length - _dataHead)
                {
                    Buffer.BlockCopy(_receiveBuffer, _dataHead, _receiveBuffer, 0, _bytesRead);
                    _dataHead = 0;
                }

                capacity = _receiveBuffer.Length - _dataHead;
                int remainingBytes = contentLength - _bytesRead;

                while (remainingBytes > 0)
                {
                    int size = Math.Min(capacity - _bytesRead, remainingBytes + (16 << 10));

                    _bytesRead += await ReceiveAsync(_dataHead + _bytesRead, size).ConfigureAwait(false);

                    remainingBytes = contentLength - _bytesRead;
                }

                request.ParseBody(_receiveBuffer, _dataHead, contentLength);

                bytesConsumed = contentLength;
            }
            else
                bytesConsumed = request.HeaderLength + request.ContentLength;

            _bytesRead -= bytesConsumed;
            _dataHead = _bytesRead == 0 ? 0 : _dataHead + bytesConsumed;
            return request;
        }

        private void GrowBuffer(int size)
        {
            byte[] newBuffer = new byte[size];
            Buffer.BlockCopy(_receiveBuffer, _dataHead, newBuffer, 0, _bytesRead);
            _receiveBuffer = newBuffer;
            _dataHead = 0;
            SetReceiveBuffer(newBuffer, 0, size);
        }

        private Task SendFailureResponseAsync(HttpResponse response)
        {
            response.KeepAlive = false;
            return SendResponseAsync(response);
        }

        private async Task SendResponseAsync(HttpResponse response)
        {
            try
            {
                await SendAsync(HttpResponse.BuildMessage(response)).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                if (e is SocketException)
                    Server.Log.d(Server.LogKeyword, $"MicroHttpConnection.SendResponseAsync(HttpResponse):{Environment.NewLine}{e}");
                else if (!(e is ObjectDisposedException))
                    Server.Log.e(Server.LogKeyword, "MicroHttpConnection.SendResponseAsync(HttpResponse):", e);
            }
        }

        public async void SendResponseAsync(HttpContext context)
        {
            if (context == null)
                throw new ArgumentNullException("context");

            bool disposed = false;

            _lock.Enter();
            try
            {
                if (_disposed)
                    disposed = true;
                else
                {
                    if (_isSending | _httpContexts.First != context)
                    {
                        if (!_hasSentResponseContexts.Add(context))
                            throw new InvalidOperationException("The response has already been sent.");
                        return;
                    }

                    _httpContexts.Pop();
                    _isSending = true;
                }
            }
            finally
            {
                _lock.Leave();
            }

            if (disposed)
            {
                context.ReturnBuffer();
                return;
            }

            var nextContext = await SendAsync(context, false).ConfigureAwait(false);

            if (nextContext != null)
                ThreadPool.QueueUserWorkItem(c => SendAsync(c as HttpContext, true).Forget(), nextContext);
        }

        private async Task<HttpContext> SendAsync(HttpContext context, bool infinite)
        {
            try
            {
                while (true)
                {
                    BufferedOutputMessage response = context.BufferedResponseData;

                    await SendAsync(response.Buffer, response.Offset, response.Count).ConfigureAwait(false);

                    context.ReturnBuffer();

                    if (!context.Request.KeepAlive | !context.Response.KeepAlive)
                    {
                        Server.CloseSession(this);
                        return null;
                    }

                    _lock.Enter();
                    try
                    {
                        if (_disposed)
                            return null;

                        context = _httpContexts.First;
                        if (context == null || !_hasSentResponseContexts.Remove(context))
                        {
                            _isSending = false;
                            return null;
                        }

                        _httpContexts.Pop();
                    }
                    finally
                    {
                        _lock.Leave();
                    }

                    if (!infinite)
                        return context;
                }
            }
            catch (Exception e)
            {
                if (e is SocketException)
                    Server.Log.d(Server.LogKeyword, $"MicroHttpConnection.SendAsync:{Environment.NewLine}{e}");
                else if (!(e is ObjectDisposedException))
                    Server.Log.e(Server.LogKeyword, "MicroHttpConnection.SendAsync:", e);

                context.ReturnBuffer();
                Server.CloseSession(this);
                return null;
            }
        }
    }
}
