﻿#if false // not currently supported
#define SEND_CHUNKED 
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.Http
{
    public sealed class HttpResponse
    {
        public static bool IsSuccessStatusCode(int statusCode)
        {
            return statusCode >= 200 && statusCode <= 299;
        }

        public static string GetRFC2616Description(int statusCode)
        {
            switch (statusCode)
            {
                case 100: return "Continue";
                case 101: return "Switching Protocols";
                case 102: return "Processing";
                case 103: return "Early Hints";
                case 200: return "OK";
                case 201: return "Created";
                case 202: return "Accepted";
                case 203: return "Non-Authoritative Information";
                case 204: return "No Content";
                case 205: return "Reset Content";
                case 206: return "Partial Content";
                case 207: return "Multi-Status";
                case 208: return "Already Reported";
                case 226: return "IM Used";
                case 300: return "Multiple Choices";
                case 301: return "Moved Permanently";
                case 302: return "Found";
                case 303: return "See Other";
                case 304: return "Not Modified";
                case 305: return "Use Proxy";
                case 306: return "Switch Proxy";
                case 307: return "Temporary Redirect";
                case 308: return "Permanent Redirect";
                case 400: return "Bad Request";
                case 401: return "Unauthorized";
                case 402: return "Payment Required";
                case 403: return "Forbidden";
                case 404: return "Not Found";
                case 405: return "Method Not Allowed";
                case 406: return "Not Acceptable";
                case 407: return "Proxy Authentication Required";
                case 408: return "Request Timeout";
                case 409: return "Conflict";
                case 410: return "Gone";
                case 411: return "Length Required";
                case 412: return "Precondition Failed";
                case 413: return "Payload Too Large";
                case 414: return "URI Too Long";
                case 415: return "Unsupported Media Type";
                case 416: return "Requested range not satisfiable";
                case 417: return "Expectation Failed";
                case 418: return "I'm a teapot";
                case 421: return "Misdirected Request";
                case 422: return "Unprocessable Entity";
                case 423: return "Locked";
                case 424: return "Failed Dependency";
                case 426: return "Upgrade Required";
                case 428: return "Precondition Required";
                case 429: return "Too Many Requests";
                case 431: return "Request Header Fields Too Large";
                case 451: return "Unavailable For Legal Reasons";
                case 500: return "Internal Server Error";
                case 501: return "Not Implemented";
                case 502: return "Bad Gateway";
                case 503: return "Service Unavailable";
                case 504: return "Gateway Timeout";
                case 505: return "HTTP Version Not Supported";
                case 506: return "Variant Also Negotiates";
                case 507: return "Insufficient Storage";
                case 508: return "Loop Detected";
                case 510: return "Not Extended";
                case 511: return "Network Authentication Required";
                default: return string.Empty;
            }
        }

        public static byte[] BuildMessage(HttpResponse response)
        {
            if (response == null)
                throw new ArgumentNullException("response");

            response.ContentLength = 0;
#if SEND_CHUNKED
            response.SendChunked = false;
#endif
            return Encoding.UTF8.GetBytes(response.ToString());
        }

        public static byte[] BuildMessage(HttpResponse response, byte[] content)
        {
            if (content == null || content.Length == 0)
                return BuildMessage(response);

            if (response == null)
                throw new ArgumentNullException("response");

            if (response.SendChunked)
                throw new NotImplementedException();

            int contentLength = content.Length;

            response.ContentLength = contentLength;

            byte[] header = Encoding.UTF8.GetBytes(response.ToString());

            int headerLength = header.Length;

            byte[] buffer = new byte[headerLength + contentLength];
            Buffer.BlockCopy(header, 0, buffer, 0, headerLength);
            Buffer.BlockCopy(content, 0, buffer, headerLength, contentLength);
            return buffer;
        }

        private ushort _statusCode = 200;
        private string _reasonPhrase = string.Empty;
        private readonly StringBuilder _headers = new StringBuilder();
        private bool _keepAliveAllowed = true;
        private bool _keepAlive = true;
        private bool _isWebSocketHandshakeResponse;
        private string _contentType = "text/plain";
        private int _contentLength;
        
        public ushort StatusCode
        {
            get { return _statusCode; }
            set
            {
                if (value < 100 | value > 999)
                    throw new ArgumentOutOfRangeException("StatusCode");

                _statusCode = value;
            }
        }

        public string ReasonPhrase
        {
            get { return _reasonPhrase; }
            set
            {
                _reasonPhrase = value ?? throw new ArgumentNullException("ReasonPhrase");
            }
        }

        public bool KeepAlive
        {
            get { return _keepAliveAllowed ? _keepAlive : false; }
            set { _keepAlive = value; }
        }

        public string ContentType
        {
            get { return _contentType; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("ContentType");
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentException("The 'ContentType' property cannot be an empty string.", "ContentType");

                _contentType = value;
            }
        }

        public int ContentLength
        {
            get { return _contentLength; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("ContentLength");

                _contentLength = value;
            }
        }

        public bool SendChunked
        {
            get;
#if SEND_CHUNKED
            set;
#endif
        }

        public HttpResponse()
        {

        }

        public HttpResponse(ushort statusCode)
        {
            StatusCode = statusCode;
        }

        public HttpResponse(bool keepAliveAllowed)
        {
            _keepAliveAllowed = keepAliveAllowed;
        }

        public HttpResponse(ushort statusCode, bool keepAliveAllowed)
        {
            StatusCode = statusCode;
            _keepAliveAllowed = keepAliveAllowed;
        }

        public void AddHeader(string name, string value)
        {
            if (name == null)
                throw new ArgumentNullException("name");
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException("The header name cannot be an empty string.", "name");
            if (value == null)
                throw new ArgumentNullException("value");

            if (string.Compare(name, "Connection", StringComparison.OrdinalIgnoreCase) == 0)
            {
                if (string.Compare(value, "Upgrade", StringComparison.OrdinalIgnoreCase) == 0)
                    _isWebSocketHandshakeResponse = true;
                return;
            }

            _headers.Append($"{name}: {value}\r\n");
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            string reasonPhrase = string.IsNullOrWhiteSpace(_reasonPhrase) ? GetRFC2616Description(_statusCode) : _reasonPhrase;

            sb.Append($"HTTP/1.1 {_statusCode.ToString()} {reasonPhrase}\r\n");
            if (_isWebSocketHandshakeResponse)
                sb.Append($"Connection: Upgrade\r\n");
            else
                sb.Append($"Connection: {(KeepAlive ? "Keep-Alive" : "Close")}\r\n");
            sb.Append("Access-Control-Allow-Origin: *\r\n");
            if (_contentLength == 0 && !SendChunked)
                sb.Append("Content-Length: 0\r\n");
            else
            {
                if (_contentLength != 0)
                    sb.Append($"Content-Length: {_contentLength.ToString()}\r\n");
                else
                    sb.Append("Transfer-Encoding: chunked\r\n");
                sb.Append($"Content-Type: {ContentType}; charset={Encoding.UTF8.HeaderName}\r\n");
            }
            if (_headers.Length > 0)
                sb.Append(_headers.ToString());

            sb.Append("\r\n");
            return sb.ToString();
        }
    }
}
