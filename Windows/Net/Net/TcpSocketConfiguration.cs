﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Net
{
    public sealed class TcpSocketConfiguration
    {
        private int _receiveBufferSize = 8192;
        private int _receiveTimeout = Timeout.Infinite;
        private int _sendBufferSize = 8192;
        private int _sendTimeout = Timeout.Infinite;
        private int _keepAliveTime = 2 * 3600 * 1000; // 2hr
        private int _keepAliveInterval = 1000; // 1s
        private int _pendingConnectionBacklog = 100;

        public int ReceiveBufferSize
        {
            get { return _receiveBufferSize; }
            set
            {
                if (value > 0)
                    _receiveBufferSize = value;
            }
        }

        public int ReceiveTimeout
        {
            get { return _receiveTimeout; }
            set { _receiveTimeout = value >= -1 ? value : -1; }
        }

        public int SendBufferSize
        {
            get { return _sendBufferSize; }
            set
            {
                if (value > 0)
                    _sendBufferSize = value;
            }
        }

        public int SendTimeout
        {
            get { return _sendTimeout; }
            set { _sendTimeout = value >= -1 ? value : -1; }
        }

        public bool NoDelay { get; set; } // false if the Socket uses the Nagle algorithm; otherwise, true.

        public LingerOption LingerState { get; set; } = new LingerOption(false, 0); // Attempts to send pending data until the default IP protocol time-out expires.

        public bool IsKeepAliveEnabled { get; set; }

        public int KeepAliveTime
        {
            get { return _keepAliveTime; }
            set
            {
                if (value > 0)
                    _keepAliveTime = value;
            }
        }

        public int KeepAliveInterval
        {
            get { return _keepAliveInterval; }
            set
            {
                if (value > 0)
                    _keepAliveInterval = value;
            }
        }

        public bool ExclusiveAddressUse { get; set; }

        public bool ReuseAddress { get; set; }

        public bool AllowNatTraversal { get; set; }

        public int PendingConnectionBacklog
        {
            get { return _pendingConnectionBacklog; }
            set
            {
                if (value > 0)
                    _pendingConnectionBacklog = value;
            }
        }

        public static void ConfigureTcpServerSocket(Socket socket, TcpSocketConfiguration configuration)
        {
            ConfigureTcpSocketCommon(socket, configuration);
            socket.ExclusiveAddressUse = configuration.ExclusiveAddressUse;
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, configuration.ReuseAddress);
            socket.SetIPProtectionLevel(configuration.AllowNatTraversal ? IPProtectionLevel.Unrestricted : IPProtectionLevel.EdgeRestricted);
        }

        public static void ConfigureTcpClientSocket(Socket socket, TcpSocketConfiguration configuration)
        {
            ConfigureTcpSocketCommon(socket, configuration);
        }

        private static void ConfigureTcpSocketCommon(Socket socket, TcpSocketConfiguration configuration)
        {
            socket.ReceiveBufferSize = configuration.ReceiveBufferSize;
            socket.ReceiveTimeout = configuration.ReceiveTimeout;
            socket.SendBufferSize = configuration.SendBufferSize;
            socket.SendTimeout = configuration.SendTimeout;
            socket.NoDelay = configuration.NoDelay;
            socket.LingerState = configuration.LingerState;
            if (configuration.IsKeepAliveEnabled)
                socket.IOControl(IOControlCode.KeepAliveValues, NetUtils.KeepAlive(true, configuration._keepAliveTime, configuration._keepAliveInterval), null);
        }
    }
}
