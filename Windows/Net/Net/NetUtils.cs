﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Net
{
    public static partial class NetUtils
    {
        public static byte[] KeepAlive(bool enabled, int keepAliveTime, int keepAliveInterval)
        {
            byte[] buffer = new byte[12];
            int onOff = enabled ? 1 : 0;

            BitConverter.GetBytes(onOff).CopyTo(buffer, 0);
            BitConverter.GetBytes(keepAliveTime).CopyTo(buffer, 4);
            BitConverter.GetBytes(keepAliveInterval).CopyTo(buffer, 8);
            return buffer;
        }
    }
}
