﻿using SwissKnife;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Net.AsyncSocket
{
    public sealed class AsyncSocketTransceiver : IDisposable
    {
        private struct Segment
        {
            public byte[] Buffer;
            public int Offset;
            public int Count;

            public void Initialize(byte[] buffer, int offset, int count)
            {
                Buffer = buffer;
                Offset = offset;
                Count = count;
            }
        }

        private const int Free = 0;
        private const int InProgress = 1;
        private const int Disposed = 2;

        internal const int MAX_INTERNAL_BUFFER_SIZE = 24 << 10;

        private readonly SocketAwaitable _receiveAwaitable;
        private readonly SocketAwaitable _sendAwaitable;
        private Segment _receiveBufferOriginal;
        private Segment _sendBufferOriginal;
        private byte[] _receiveInternalBuffer;
        private byte[] _sendInternalBuffer;
        private bool _usePinnedReceiveBuffer;
        private bool _usePinnedSendBuffer;
        private int _receiveOperating;
        private int _sendOperating;
        private ManualResetEvent _receiveCompletionEvent;
        private ManualResetEvent _sendCompletionEvent;
        private bool _disposed;

        public AsyncSocketTransceiver(SocketAwaitable receiveAwaitable, SocketAwaitable sendAwaitable)
        {
            if (receiveAwaitable != null)
            {
                _receiveAwaitable = receiveAwaitable;
                SocketAsyncEventArgs saea = receiveAwaitable.SAEA;
                _receiveBufferOriginal.Initialize(saea.Buffer, saea.Offset, saea.Count);
                _receiveInternalBuffer = new byte[0];
            }

            if (sendAwaitable != null)
            {
                _sendAwaitable = sendAwaitable;
                SocketAsyncEventArgs saea = _sendAwaitable.SAEA;
                _sendBufferOriginal.Initialize(saea.Buffer, saea.Offset, saea.Count);
                _sendInternalBuffer = new byte[0];
            }
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _receiveCompletionEvent = new ManualResetEvent(false);
            _sendCompletionEvent = new ManualResetEvent(false);

            int receiveStatus = Interlocked.Exchange(ref _receiveOperating, Disposed);
            int sendStatus = Interlocked.Exchange(ref _sendOperating, Disposed);

            if (receiveStatus == InProgress)
                _receiveCompletionEvent.WaitOne();
            if (sendStatus == InProgress)
                _sendCompletionEvent.WaitOne();
         
            _receiveCompletionEvent.Dispose();
            _sendCompletionEvent.Dispose();

            if (_receiveAwaitable != null)
                _receiveAwaitable.SAEA.SetBuffer(_receiveBufferOriginal.Buffer, _receiveBufferOriginal.Offset, _receiveBufferOriginal.Count);

            if (_sendAwaitable != null)
                _sendAwaitable.SAEA.SetBuffer(_sendBufferOriginal.Buffer, _sendBufferOriginal.Offset, _sendBufferOriginal.Count);

            _disposed = true;
        }

        public void SetReceiveBuffer(byte[] buffer, int offset, int count)
        {
            if (_receiveAwaitable == null)
                throw new InvalidOperationException("This operation is not supported.");
            if (buffer != null && count <= 0)
                throw new ArgumentOutOfRangeException("count");

            StartOperation(ref _receiveOperating);
            try
            {
                _receiveAwaitable.SAEA.SetBuffer(buffer, offset, count);
                _usePinnedReceiveBuffer = buffer != null;
            }
            finally
            {
                CompleteOperation(ref _receiveOperating, ref _receiveCompletionEvent);
            }
        }

        public void SetSendBuffer(byte[] buffer, int offset, int count)
        {
            if (_sendAwaitable == null)
                throw new InvalidOperationException("This operation is not supported.");

            StartOperation(ref _sendOperating);
            try
            {
                _sendAwaitable.SAEA.SetBuffer(buffer, offset, count);
                _usePinnedSendBuffer = buffer != null;
            }
            finally
            {
                CompleteOperation(ref _sendOperating, ref _sendCompletionEvent);
            }
        }

        public async Task<int> ReceiveAsync(Socket socket)
        {
            if (_receiveAwaitable == null)
                throw new InvalidOperationException("This operation is not supported.");
            if (socket == null)
                throw new ArgumentNullException("socket");

            StartOperation(ref _receiveOperating);
            try
            {
                if (!_usePinnedReceiveBuffer)
                    throw new ArgumentException("This method cannot be called if the receive buffer is set to null.");

                return await ReceiveInternalAsync(socket, null, 0, 0).ConfigureAwait(false);
            }
            finally
            {
                CompleteOperation(ref _receiveOperating, ref _receiveCompletionEvent);
            }
        }

        public async Task<int> ReceiveAsync(Socket socket, int offset, int size)
        {
            if (_receiveAwaitable == null)
                throw new InvalidOperationException("This operation is not supported.");
            if (socket == null)
                throw new ArgumentNullException("socket");
            if (size <= 0)
                throw new ArgumentOutOfRangeException("size");

            StartOperation(ref _receiveOperating);
            try
            {
                if (!_usePinnedReceiveBuffer)
                    throw new ArgumentException("This method cannot be called if the receive buffer is set to null.");

                _receiveAwaitable.SAEA.SetBuffer(offset, size);
                return await ReceiveInternalAsync(socket, null, 0, 0).ConfigureAwait(false);
            }
            finally
            {
                CompleteOperation(ref _receiveOperating, ref _receiveCompletionEvent);
            }
        }

        public Task<int> ReceiveAsync(Socket socket, byte[] buffer)
        {
            if (buffer == null)
                throw new ArgumentNullException("buffer");

            return ReceiveAsync(socket, buffer, 0, buffer.Length);
        }

        public async Task<int> ReceiveAsync(Socket socket, byte[] buffer, int offset, int size)
        {
            if (_receiveAwaitable == null)
                throw new InvalidOperationException("This operation is not supported.");
            if (socket == null)
                throw new ArgumentNullException("socket");
            if (buffer == null)
                throw new ArgumentNullException("buffer");
            if (size <= 0)
                throw new ArgumentOutOfRangeException("size");
            if (offset < 0)
                throw new ArgumentOutOfRangeException("offset");
            if (size > buffer.Length - offset)
                throw new ArgumentException("The sum of the offset and size arguments must be less than the collection's Count.");

            StartOperation(ref _receiveOperating);
            try
            {
                if (_usePinnedReceiveBuffer)
                    throw new ArgumentException("This method cannot be called if the receive buffer is set to non-null.");

                return await ReceiveInternalAsync(socket, buffer, offset, size).ConfigureAwait(false);
            }
            finally
            {
                CompleteOperation(ref _receiveOperating, ref _receiveCompletionEvent);
            }
        }

        private async Task<int> ReceiveInternalAsync(Socket socket, byte[] buffer, int offset, int size)
        {
            bool usePinnedBuffer = buffer == null;
            bool needsResetToNull = false;
            bool needsCopy = false;

            try
            {
                if (!usePinnedBuffer)
                {
                    if (size > MAX_INTERNAL_BUFFER_SIZE)
                    {
                        _receiveAwaitable.SAEA.SetBuffer(buffer, offset, size);
                        needsResetToNull = true;
                    }
                    else
                    {
                        _receiveInternalBuffer = IncreaseBufferIfNeeded(_receiveInternalBuffer, size);
                        _receiveAwaitable.SAEA.SetBuffer(_receiveInternalBuffer, 0, size);
                        needsCopy = true;
                    }
                }

                await socket.ReceiveAsync(_receiveAwaitable);

                int bytesRead = _receiveAwaitable.SAEA.BytesTransferred;
                if (bytesRead == 0)
                    throw new SocketException((int)SocketError.ConnectionReset);

                if (needsCopy)
                    Buffer.BlockCopy(_receiveInternalBuffer, 0, buffer, offset, bytesRead);

                return bytesRead;
            }
            catch (SocketException e)
            {
                if (e.SocketErrorCode == SocketError.OperationAborted)
                    throw new ObjectDisposedException("Asynchronous Socket");
                throw;
            }
            finally
            {
                if (needsResetToNull)
                    _receiveAwaitable.SAEA.SetBuffer(null, 0, 0);
            }
        }

        public async Task SendAsync(Socket socket)
        {
            if (_sendAwaitable == null)
                throw new InvalidOperationException("This operation is not supported.");
            if (socket == null)
                throw new ArgumentNullException("socket");

            StartOperation(ref _sendOperating);
            try
            {
                if (!_usePinnedSendBuffer)
                    throw new ArgumentException("This method cannot be called if the send buffer is set to null.");

                await SendInternalAsync(socket, null, 0, 0).ConfigureAwait(false);
            }
            finally
            {
                CompleteOperation(ref _sendOperating, ref _sendCompletionEvent);
            }
        }

        public async Task SendAsync(Socket socket, int offset, int count)
        {
            if (_sendAwaitable == null)
                throw new InvalidOperationException("This operation is not supported.");
            if (socket == null)
                throw new ArgumentNullException("socket");

            StartOperation(ref _sendOperating);
            try
            {
                if (!_usePinnedSendBuffer)
                    throw new ArgumentException("This method cannot be called if the send buffer is set to null.");

                _sendAwaitable.SAEA.SetBuffer(offset, count);
                await SendInternalAsync(socket, null, 0, 0).ConfigureAwait(false);
            }
            finally
            {
                CompleteOperation(ref _sendOperating, ref _sendCompletionEvent);
            }
        }

        public Task SendAsync(Socket socket, byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            return SendAsync(socket, data, 0, data.Length);
        }

        public async Task SendAsync(Socket socket, byte[] data, int offset, int count)
        {
            if (_sendAwaitable == null)
                throw new InvalidOperationException("This operation is not supported.");
            if (socket == null)
                throw new ArgumentNullException("socket");
            if (data == null)
                throw new ArgumentNullException("data");
            if (count < 0)
                throw new ArgumentOutOfRangeException("count");
            if (offset < 0)
                throw new ArgumentOutOfRangeException("offset");
            if (count > data.Length - offset)
                throw new ArgumentException("The sum of the offset and count arguments must be less than or equal to the collection's Count.");

            StartOperation(ref _sendOperating);
            try
            {
                if (_usePinnedSendBuffer)
                    throw new ArgumentException("This method cannot be called if the send buffer is set to non-null.");

                await SendInternalAsync(socket, data, offset, count).ConfigureAwait(false);
            }
            finally
            {
                CompleteOperation(ref _sendOperating, ref _sendCompletionEvent);
            }
        }

        public async Task SendInternalAsync(Socket socket, byte[] data, int offset, int count)
        {
            bool usePinnedBuffer = data == null;
            bool needsResetToNull = false;

            try
            {
                if (!usePinnedBuffer)
                {
                    if (count > MAX_INTERNAL_BUFFER_SIZE)
                    {
                        _sendAwaitable.SAEA.SetBuffer(data, offset, count);
                        needsResetToNull = true;
                    }
                    else
                    {
                        _sendInternalBuffer = IncreaseBufferIfNeeded(_sendInternalBuffer, count);
                        Buffer.BlockCopy(data, offset, _sendInternalBuffer, 0, count);
                        _sendAwaitable.SAEA.SetBuffer(_sendInternalBuffer, 0, count);
                    }
                }

                await socket.SendAsync(_sendAwaitable);
            }
            catch (SocketException e)
            {
                if (e.SocketErrorCode == SocketError.OperationAborted)
                    throw new ObjectDisposedException("Asynchronous Socket");
                throw;
            }
            finally
            {
                if (needsResetToNull)
                    _sendAwaitable.SAEA.SetBuffer(null, 0, 0);
            }
        }

        private void StartOperation(ref int operating)
        {
            switch (Interlocked.CompareExchange(ref operating, InProgress, Free))
            {
                case InProgress:
                    throw new InvalidOperationException("An asynchronous socket operation is already in progress using this SocketAsyncEventArgs instance.");
                case Disposed:
                    throw new ObjectDisposedException(GetType().FullName);
            }
        }

        private void CompleteOperation(ref int operating, ref ManualResetEvent completionEvent)
        {
            if (Interlocked.CompareExchange(ref operating, Free, InProgress) == Disposed)
                completionEvent.Set();
        }

        private byte[] IncreaseBufferIfNeeded(byte[] buffer, int minLength)
        {
#if DEBUG
            if (minLength > MAX_INTERNAL_BUFFER_SIZE)
                throw new ArgumentException($"Argument minLength should be less than or equal to {MAX_INTERNAL_BUFFER_SIZE.ToString()}.", "minLength");
#endif
            if (buffer.Length < minLength)
            {
                int length = (minLength * 2 + 7) & 0x7;

                length = length > minLength ? Math.Min(length, MAX_INTERNAL_BUFFER_SIZE) : MAX_INTERNAL_BUFFER_SIZE;
                return new byte[length];
            }
            return buffer;
        }
    }
}
