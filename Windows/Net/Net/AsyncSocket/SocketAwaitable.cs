﻿using SwissKnife.ObjectPool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Net.AsyncSocket
{
    public class SocketAwaitable : RecyclableObject, INotifyCompletion
    {
        private static readonly Action SENTINEL = () => { };

        private Action _continuation;

        public SocketAsyncEventArgs SAEA { get; } = new SocketAsyncEventArgs();

        public bool IsCompleted { get; set; }

        public SocketAwaitable()
        {
            SAEA.Completed += SocketAsyncEventArgs_Completed;
        }

        public override void Dispose()
        {
            SAEA.Dispose();
        }

        public override void Recycle()
        {
            SAEA.AcceptSocket = null;
            SAEA.RemoteEndPoint = null;
        }

        public SocketAwaitable GetAwaiter()
        {
            return this;
        }

        void INotifyCompletion.OnCompleted(Action continuation)
        {
            if (_continuation == SENTINEL || Interlocked.CompareExchange(ref _continuation, continuation, null) == SENTINEL)
                Task.Run(continuation);
        }

        private void SocketAsyncEventArgs_Completed(object sender, SocketAsyncEventArgs e)
        {
            IsCompleted = true;
            (_continuation ?? Interlocked.CompareExchange(ref _continuation, SENTINEL, null))?.Invoke();
        }

        public void GetResult()
        {
            if (SAEA.SocketError != SocketError.Success)
                throw new SocketException((int)SAEA.SocketError);
        }

        public void Reset()
        {
            IsCompleted = false;
            _continuation = null;
        }
    }
}
