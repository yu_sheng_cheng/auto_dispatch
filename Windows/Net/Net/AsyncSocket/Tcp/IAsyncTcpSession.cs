﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Net.AsyncSocket.Tcp
{
    public interface IAsyncTcpSession : IAsyncSocketTransceiver
    {
        AsyncTcpServer Server { get; }

        string SessionKey { get; }

        IPEndPoint RemoteEndPoint { get; }

        object UserToken { get; set; }

        void Shutdown();
    }
}
