﻿using SwissKnife.ObjectPool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Net.AsyncSocket.Tcp
{
    public class AsyncTcpClient : IDisposable, IAsyncSocketTransceiver
    {
        private const int Disconnected = 0;
        private const int Connecting = 1;
        private const int Connected = 2;
        private const int Disposed = 3;

        private readonly SocketAwaitable _receiveAwaitable;
        private readonly SocketAwaitable _sendAwaitable;
        private readonly AsyncSocketTransceiver _socketTransceiver;
        private Socket _socket;
        private int _connectionStatus;
        private Socket _connectingSocket;

        public TcpSocketConfiguration Configuration { get; } = new TcpSocketConfiguration();

        public IPEndPoint RemoteEndPoint { get; }

        public AsyncTcpClient(IPAddress remoteAddress, int remotePort, IObjectPool<SocketAwaitable> socketAwaitablePool = null)
            : this(new IPEndPoint(remoteAddress, remotePort), socketAwaitablePool) { }

        public AsyncTcpClient(IPEndPoint remoteEndPoint, IObjectPool<SocketAwaitable> socketAwaitablePool = null)
        {
            RemoteEndPoint = remoteEndPoint ?? throw new ArgumentNullException("remoteEndPoint");

            try
            {
                if (socketAwaitablePool != null)
                {
                    _receiveAwaitable = socketAwaitablePool.Take();
                    _sendAwaitable = socketAwaitablePool.Take();
                }
                else
                {
                    _receiveAwaitable = new SocketAwaitable();
                    _sendAwaitable = new SocketAwaitable();
                }

                _socketTransceiver = new AsyncSocketTransceiver(_receiveAwaitable, _sendAwaitable);
            }
            catch
            {
                if (_sendAwaitable != null)
                    _sendAwaitable.Release();
                if (_receiveAwaitable != null)
                    _receiveAwaitable.Release();
                throw;
            }
        }

        public void Dispose()
        {
            int status = Interlocked.Exchange(ref _connectionStatus, Disposed);

            if (status == Disposed)
                return;

            if (status == Connecting)
            {
                Socket connectingSocket = Interlocked.CompareExchange(ref _connectingSocket, null, null);
                if (connectingSocket != null)
                    connectingSocket.Dispose();
                return;
            }

            CleanUp();
        }

        protected virtual void CleanUp()
        {
            if (_socket != null)
            {
                _socket.Dispose();
                _socket = null;
            }
            _socketTransceiver.Dispose();

            _sendAwaitable.Release();
            _receiveAwaitable.Release();
        }

        public async Task ConnectAsync()
        {
            switch (Interlocked.CompareExchange(ref _connectionStatus, Connecting, Disconnected))
            {
                case Connecting:
                    throw new InvalidOperationException("Previous connect operation is still in progress.");
                case Connected:
                    throw new InvalidOperationException("Already connected.");
                case Disposed:
                    throw new ObjectDisposedException(GetType().FullName);
            }

            Socket socket = null;

            try
            {
                socket = new Socket(RemoteEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                TcpSocketConfiguration.ConfigureTcpClientSocket(socket, Configuration);

                _sendAwaitable.SAEA.RemoteEndPoint = RemoteEndPoint;

                Interlocked.Exchange(ref _connectingSocket, socket);
                if (Interlocked.CompareExchange(ref _connectionStatus, Connecting, Connecting) == Disposed)
                {
                    _sendAwaitable.SAEA.RemoteEndPoint = null;
                    socket.Dispose();
                    CleanUp();
                    return;
                }

                await socket.ConnectAsync(_sendAwaitable);
                Volatile.Write(ref _socket, socket);

                if (Interlocked.CompareExchange(ref _connectionStatus, Connected, Connecting) == Disposed)
                    CleanUp();
            }
            catch
            {
                if (socket != null)
                {
                    _sendAwaitable.SAEA.RemoteEndPoint = null;
                    socket.Dispose();
                }
                if (Interlocked.CompareExchange(ref _connectionStatus, Disconnected, Connecting) == Disposed)
                    CleanUp();
                throw;
            }
            finally
            {
                Interlocked.Exchange(ref _connectingSocket, null);
            }
        }

        public void Shutdown()
        {
            // The correct way to shut down the connection (especially if you are in a full-duplex conversation) is to
            // call socket.Shutdown(SocketShutdown.Send) and give the remote party some time to close their send 
            // channel. This ensures that you receive any pending data instead of slamming the connection shut. 
            // ObjectDisposedException should never be part of the normal application flow.
            Socket socket = Volatile.Read(ref _socket);
            if (socket != null && socket.Connected)
                socket.Shutdown(SocketShutdown.Send);
        }

        public void SetReceiveBuffer(byte[] buffer, int offset, int count)
        {
            _socketTransceiver.SetReceiveBuffer(buffer, offset, count);
        }

        public void SetSendBuffer(byte[] buffer, int offset, int count)
        {
            _socketTransceiver.SetSendBuffer(buffer, offset, count);
        }

        public Task<int> ReceiveAsync()
        {
            Socket socket = GetSocket();
            return _socketTransceiver.ReceiveAsync(socket);
        }

        public Task<int> ReceiveAsync(int offset, int size)
        {
            Socket socket = GetSocket();
            return _socketTransceiver.ReceiveAsync(socket, offset, size);
        }

        public Task<int> ReceiveAsync(byte[] buffer)
        {
            if (buffer == null)
                throw new ArgumentNullException("buffer");

            Socket socket = GetSocket();
            return _socketTransceiver.ReceiveAsync(socket, buffer, 0, buffer.Length);
        }

        public Task<int> ReceiveAsync(byte[] buffer, int offset, int size)
        {
            Socket socket = GetSocket();
            return _socketTransceiver.ReceiveAsync(socket, buffer, offset, size);
        }

        public Task SendAsync()
        {
            Socket socket = GetSocket();
            return _socketTransceiver.SendAsync(socket);
        }

        public Task SendAsync(int offset, int count)
        {
            Socket socket = GetSocket();
            return _socketTransceiver.SendAsync(socket, offset, count);
        }

        public Task SendAsync(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            Socket socket = GetSocket();
            return _socketTransceiver.SendAsync(socket, data, 0, data.Length);
        }

        public Task SendAsync(byte[] data, int offset, int count)
        {
            Socket socket = GetSocket();
            return _socketTransceiver.SendAsync(socket, data, offset, count);
        }

        private Socket GetSocket()
        {
            Socket socket = Volatile.Read(ref _socket);

            if (socket == null)
            {
                switch (Interlocked.CompareExchange(ref _connectionStatus, Disconnected, Disconnected))
                {
                    case Disposed:
                        throw new ObjectDisposedException(GetType().FullName);
                    default:
                        throw new InvalidOperationException("The socket is not connected.");
                }
            }
            return socket;
        }
    }
}
