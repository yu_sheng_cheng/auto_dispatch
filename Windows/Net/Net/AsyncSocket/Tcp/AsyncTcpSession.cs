﻿using SwissKnife.ObjectPool;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Net.AsyncSocket.Tcp
{
    public class AsyncTcpSession : IAsyncTcpSession, IDisposable
    {
        private Socket _socket;
        private readonly SocketAwaitable _receiveAwaitable;
        private readonly SocketAwaitable _sendAwaitable;
        private readonly AsyncSocketTransceiver _socketTransceiver;
        private int _disposed;

        public AsyncTcpServer Server { get; }

        public string SessionKey { get; internal set; }

        public IPEndPoint RemoteEndPoint { get { return _socket.RemoteEndPoint as IPEndPoint; } }

        public object UserToken { get; set; }

        public AsyncTcpSession(AsyncTcpServer server, Socket socket, IObjectPool<SocketAwaitable> socketAwaitablePool = null)
        {
            Server = server ?? throw new ArgumentNullException("server");
            _socket = socket ?? throw new ArgumentNullException("socket");

            if (socket.RemoteEndPoint as IPEndPoint == null)
                throw new ArgumentException($"The supplied EndPoint of AddressFamily {socket.AddressFamily.ToString()} is not valid for this Socket.", "socket");

            try
            {
                if (socketAwaitablePool != null)
                {
                    _receiveAwaitable = socketAwaitablePool.Take();
                    _sendAwaitable = socketAwaitablePool.Take();
                }
                else
                {
                    _receiveAwaitable = new SocketAwaitable();
                    _sendAwaitable = new SocketAwaitable();
                }

                _socketTransceiver = new AsyncSocketTransceiver(_receiveAwaitable, _sendAwaitable);
            }
            catch
            {
                if (_sendAwaitable != null)
                    _sendAwaitable.Release();
                if (_receiveAwaitable != null)
                    _receiveAwaitable.Release();
                throw;
            }
        }

        public void Dispose()
        {
            if (Interlocked.Exchange(ref _disposed, 1) == 1)
                return;

            CleanUp();
        }

        protected virtual void CleanUp()
        {
            _socket.Dispose();
            _socket = null;
            _socketTransceiver.Dispose();
            _receiveAwaitable.Release();
            _sendAwaitable.Release();
        }

        public void Shutdown()
        {
            // The correct way to shut down the connection (especially if you are in a full-duplex conversation) is to
            // call socket.Shutdown(SocketShutdown.Send) and give the remote party some time to close their send 
            // channel. This ensures that you receive any pending data instead of slamming the connection shut. 
            // ObjectDisposedException should never be part of the normal application flow.
            Socket socket = Volatile.Read(ref _socket);
            if (socket != null && socket.Connected)
                socket.Shutdown(SocketShutdown.Send);
        }

        public void SetReceiveBuffer(byte[] buffer, int offset, int count)
        {
            _socketTransceiver.SetReceiveBuffer(buffer, offset, count);
        }

        public void SetSendBuffer(byte[] buffer, int offset, int count)
        {
            _socketTransceiver.SetSendBuffer(buffer, offset, count);
        }

        public Task<int> ReceiveAsync()
        {
            Socket socket = GetSocket();
            return _socketTransceiver.ReceiveAsync(socket);
        }

        public Task<int> ReceiveAsync(int offset, int size)
        {
            Socket socket = GetSocket();
            return _socketTransceiver.ReceiveAsync(socket, offset, size);
        }

        public Task<int> ReceiveAsync(byte[] buffer)
        {
            if (buffer == null)
                throw new ArgumentNullException("buffer");

            Socket socket = GetSocket();
            return _socketTransceiver.ReceiveAsync(socket, buffer, 0, buffer.Length);
        }

        public Task<int> ReceiveAsync(byte[] buffer, int offset, int size)
        {
            Socket socket = GetSocket();
            return _socketTransceiver.ReceiveAsync(socket, buffer, offset, size);
        }

        public Task SendAsync()
        {
            Socket socket = GetSocket();
            return _socketTransceiver.SendAsync(socket);
        }

        public Task SendAsync(int offset, int count)
        {
            Socket socket = GetSocket();
            return _socketTransceiver.SendAsync(socket, offset, count);
        }

        public Task SendAsync(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            Socket socket = GetSocket();
            return _socketTransceiver.SendAsync(socket, data, 0, data.Length);
        }

        public Task SendAsync(byte[] data, int offset, int count)
        {
            Socket socket = GetSocket();
            return _socketTransceiver.SendAsync(socket, data, offset, count);
        }

        private Socket GetSocket()
        {
            Socket socket = Volatile.Read(ref _socket);

            if (socket == null)
                throw new ObjectDisposedException(GetType().FullName);
            return socket;
        }
    }
}
