﻿using SwissKnife.Log;
using SwissKnife.ObjectPool;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Net.AsyncSocket.Tcp
{
    [Serializable]
    public class ClientConnectedEventArgs : EventArgs
    {
        [NonSerialized]
        private IAsyncTcpSession _session;

        public IAsyncTcpSession Session { get { return _session; } }

        public ClientConnectedEventArgs(IAsyncTcpSession session)
        {
            _session = session ?? throw new ArgumentNullException("session");
        }
    }

    public delegate void ClientConnectedEventHandler(object sender, ClientConnectedEventArgs e);

    public class AsyncTcpServer : IDisposable
    {
        private const int Stopped = 0;
        private const int Starting = 1;
        private const int Started = 2;
        private const int Stopping = 3;
        private const int Disposed = 4;

        private ILog _log = new Debug();
        private readonly ConcurrentDictionary<string, AsyncTcpSession> _sessions = new ConcurrentDictionary<string, AsyncTcpSession>();
        private Socket _listenedSocket;
        private readonly SocketAwaitable _acceptAwaitable;
        private bool _acceptTaskAlive;
        private int _numConnectedSockets;
        private EventWaitHandle _notifyWhenDisposed;
        private int _status;

        public ILog Log
        {
            get { return _log; }
            set
            {
                _log = value ?? new Debug();
            }
        }

        public string LogKeyword { get; set; } = "AsyncTcpServer";

        protected IObjectPool<SocketAwaitable> SocketAwaitablePool { get; }

        public TcpSocketConfiguration Configuration { get; } = new TcpSocketConfiguration();

        public int MaxConnectionsAllowed { get; }

        public int NumOfConnectedSockets
        {
            get
            {
                int numConnectedSockets = _numConnectedSockets;

                if (Interlocked.CompareExchange(ref _status, Started, Started) != Started)
                    return 0;

                if (numConnectedSockets > MaxConnectionsAllowed)
                    return MaxConnectionsAllowed;
                else if (numConnectedSockets < 0)
                    return 0;
                else
                    return numConnectedSockets;
            }
        }

        public IPEndPoint ListenedEndPoint { get; private set; }

        public event ClientConnectedEventHandler ClientConnected;

        public AsyncTcpServer(int maxConnectionsAllowed) : this(null, maxConnectionsAllowed) { }

        public AsyncTcpServer(int port, int maxConnectionsAllowed) : this(IPAddress.Any, port, maxConnectionsAllowed) { }

        public AsyncTcpServer(IPAddress listenedAddress, int listenedPort, int maxConnectionsAllowed) : this(new IPEndPoint(listenedAddress, listenedPort), maxConnectionsAllowed) { }

        public AsyncTcpServer(IPEndPoint listenedEndPoint, int maxConnectionsAllowed)
        {
            if (maxConnectionsAllowed < 1 | maxConnectionsAllowed >= int.MaxValue)
                throw new ArgumentOutOfRangeException("maxConnectionsAllowed");

            ListenedEndPoint = listenedEndPoint;
            MaxConnectionsAllowed = maxConnectionsAllowed;

            int initialCapacity = maxConnectionsAllowed;
            if (maxConnectionsAllowed > 8)
                initialCapacity = 8 + (maxConnectionsAllowed - 8) / 8;
            if (initialCapacity > 512)
                initialCapacity = 512;

            try
            {
                _acceptAwaitable = new SocketAwaitable();
                SocketAwaitablePool = new ConcurrentObjectPool<SocketAwaitable>(() => { return new SocketAwaitable(); }, initialCapacity * 4, initialCapacity * 2);
            }
            catch
            {
                if (_acceptAwaitable != null)
                    _acceptAwaitable.Dispose();
                throw;
            }
        }

        public void Dispose()
        {
            int status = Interlocked.Exchange(ref _status, Disposed);

            if (status == Starting | status == Stopping | status == Disposed)
                return;

            CleanUp();
        }

        public bool Dispose(EventWaitHandle notifyObject)
        {
            if (notifyObject == null)
                throw new ArgumentNullException("notifyObject");

            if (Interlocked.CompareExchange(ref _notifyWhenDisposed, notifyObject, null) != null)
                return false;

            int status = Interlocked.Exchange(ref _status, Disposed);

            if (status == Disposed)
                return false;

            if (status == Starting | status == Stopping)
                return true;

            CleanUp();
            return false;
        }

        private void CleanUpThenNotifyIfNeeded()
        {
            CleanUp();

            if (_notifyWhenDisposed != null)
                _notifyWhenDisposed.Set();
        }

        protected virtual void CleanUp()
        {
            StopInternal();
            ReleaseResourcesCreatedInConstructor();
        }

        protected void ReleaseResourcesCreatedInConstructor()
        {
            _acceptAwaitable.Dispose();
            SocketAwaitablePool.Dispose();
        }

        public void Start()
        {
            Start(ListenedEndPoint, null);
        }

        public void Start(IPAddress listenedAddress, int listenedPort)
        {
            Start(new IPEndPoint(listenedAddress, listenedPort), null);
        }

        public void Start(IPEndPoint listenedEndPoint)
        {
            Start(listenedEndPoint, null);
        }

        protected void Start(IPEndPoint listenedEndPoint, Action configure)
        {
            switch (Interlocked.CompareExchange(ref _status, Starting, Stopped))
            {
                case Starting:
                    throw new InvalidOperationException("Previous start operation is still in progress.");
                case Started:
                    throw new InvalidOperationException("Already started.");
                case Stopping:
                    throw new InvalidOperationException("Start cannot be called while stop operation is in progress.");
                case Disposed:
                    throw new ObjectDisposedException(GetType().FullName);
            }

            Socket socket = null;

            try
            {
                ListenedEndPoint = listenedEndPoint ?? throw new ArgumentNullException("listenedEndPoint");

                socket = new Socket(listenedEndPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                TcpSocketConfiguration.ConfigureTcpServerSocket(socket, Configuration);
                socket.Bind(listenedEndPoint);
                socket.Listen(Configuration.PendingConnectionBacklog);

                _listenedSocket = socket;

                configure?.Invoke();

                _numConnectedSockets = 0;
                _acceptTaskAlive = true;
                ThreadPool.QueueUserWorkItem(_ => AcceptTask());
                
                _log.i(LogKeyword, $"The asynchronous tcp server has already been started. ListenedEndPoint[{listenedEndPoint.ToString()}]");

                if (Interlocked.CompareExchange(ref _status, Started, Starting) == Disposed)
                    CleanUpThenNotifyIfNeeded();        
            }
            catch
            {
                if (socket != null)
                    socket.Dispose();
                if (Interlocked.CompareExchange(ref _status, Stopped, Starting) == Disposed)
                    CleanUpThenNotifyIfNeeded();
                throw;
            }
        }

        private async void AcceptTask()
        {
            Socket serverSocket = Volatile.Read(ref _listenedSocket);
            if (serverSocket == null)
            {
                _acceptTaskAlive = false;
                return;
            }

            try
            {
                while (true)
                {
                    await serverSocket.AcceptAsync(_acceptAwaitable);

                    Socket clientSocket = _acceptAwaitable.SAEA.AcceptSocket;

                    _log.i(LogKeyword, $"Client connection accepted. RemoteEndPoint[{clientSocket.RemoteEndPoint.ToString()}]");

                    bool limitExceeded = Interlocked.Increment(ref _numConnectedSockets) > MaxConnectionsAllowed;
                    if (limitExceeded || !CreateSession(clientSocket))
                    {
                        Interlocked.Decrement(ref _numConnectedSockets);
                        clientSocket.Dispose();
                        if (limitExceeded)
                            _log.w(LogKeyword, $"The connection limit was exceeded. The connection count is limited to {MaxConnectionsAllowed.ToString()}");
                    }

                    _acceptAwaitable.SAEA.AcceptSocket = null;
                }
            }
            catch (Exception e)
            {
                _acceptAwaitable.SAEA.AcceptSocket = null;
                if (Interlocked.CompareExchange(ref _listenedSocket, null, null) != null) // unexpected exception occurred
                    _log.e(LogKeyword, "AcceptTask:", e);
            }
            finally
            {
                _acceptTaskAlive = false;
            }
        }

        private bool CreateSession(Socket clientSocket)
        {
            AsyncTcpSession session = null;
            string sessionKey = null;

            try
            {
                session = CreateAsyncTcpSessionInstance(clientSocket);

                string tmp = Guid.NewGuid().ToString();
                while (!_sessions.TryAdd(tmp, session))
                {
                    tmp = Guid.NewGuid().ToString();
                }
                sessionKey = tmp;

                session.SessionKey = sessionKey;
                _log.i(LogKeyword, $"New session created. SessionKey[{sessionKey}], RemoteEndPoint[{session.RemoteEndPoint.ToString()}]");
                OnClientConnected(new ClientConnectedEventArgs(session));
                return true;
            }
            catch (Exception e)
            {
                _log.e(LogKeyword, "CreateSession:", e);

                if (sessionKey != null)
                    _sessions.TryRemove(sessionKey, out AsyncTcpSession unused);
                if (session != null)
                    session.Dispose();
                return false;
            }
        }

        protected virtual AsyncTcpSession CreateAsyncTcpSessionInstance(Socket clientSocket)
        {
            return new AsyncTcpSession(this, clientSocket, SocketAwaitablePool);
        }

        protected virtual void OnClientConnected(ClientConnectedEventArgs e)
        {
            ClientConnectedEventHandler temp = Volatile.Read(ref ClientConnected);
            try { temp?.Invoke(this, e); } catch { }
        }

        public void Stop()
        {
            switch (Interlocked.CompareExchange(ref _status, Stopping, Started))
            {
                case Stopped:
                    return;
                case Starting:
                    throw new InvalidOperationException("Previous start operation is still in progress.");
                case Stopping:
                    throw new InvalidOperationException("Previous stop operation is still in progress.");
                case Disposed:
                    throw new ObjectDisposedException(GetType().FullName);
            }

            StopInternal();

            if (Interlocked.CompareExchange(ref _status, Stopped, Stopping) == Disposed)
                CleanUpThenNotifyIfNeeded();
        }

        private void StopInternal()
        {
            Socket socket = Interlocked.Exchange(ref _listenedSocket, null);
            if (socket != null)
                socket.Dispose();

            SpinWait spinwait = new SpinWait();
            while (Volatile.Read(ref _acceptTaskAlive))
            {
                spinwait.SpinOnce();
            }

            foreach (var item in _sessions)
            {
                item.Value.Dispose();
            }
            _sessions.Clear();
        }

        public void CloseSession(IAsyncTcpSession s)
        {
            AsyncTcpSession session = s as AsyncTcpSession;
            if (session == null || session.Server != this)
                return;

            string sessionKey = session.SessionKey;
            if (string.IsNullOrEmpty(sessionKey))
                return;

            session.Dispose();

            if (_sessions.TryRemove(sessionKey, out AsyncTcpSession unused))
            {
                Interlocked.Decrement(ref _numConnectedSockets);
                _log.i(LogKeyword, $"The session was closed. SessionKey[{sessionKey}]");          
            }              
        }
    }
}
