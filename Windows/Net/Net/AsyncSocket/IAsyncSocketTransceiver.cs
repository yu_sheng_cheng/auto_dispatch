﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Net.AsyncSocket
{
    public interface IAsyncSocketTransceiver
    {
        void SetReceiveBuffer(byte[] buffer, int offset, int count);

        void SetSendBuffer(byte[] buffer, int offset, int count);

        Task<int> ReceiveAsync();
        Task<int> ReceiveAsync(int offset, int size);
        Task<int> ReceiveAsync(byte[] buffer);
        Task<int> ReceiveAsync(byte[] buffer, int offset, int size);

        Task SendAsync();
        Task SendAsync(int offset, int count);
        Task SendAsync(byte[] data);
        Task SendAsync(byte[] data, int offset, int count);
    }
}
