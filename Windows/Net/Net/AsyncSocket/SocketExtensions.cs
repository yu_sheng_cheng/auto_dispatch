﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Net.AsyncSocket
{
    public static class SocketExtensions
    {
        private static readonly Func<Socket, SocketAsyncEventArgs, bool> ACCEPT = (s, e) => s.AcceptAsync(e);
        private static readonly Func<Socket, SocketAsyncEventArgs, bool> CONNECT = (s, e) => s.ConnectAsync(e);
        private static readonly Func<Socket, SocketAsyncEventArgs, bool> RECEIVE = (s, e) => s.ReceiveAsync(e);
        private static readonly Func<Socket, SocketAsyncEventArgs, bool> SEND = (s, e) => s.SendAsync(e);

        public static SocketAwaitable AcceptAsync(this Socket socket, SocketAwaitable awaitable)
        {
            return OperateAsync(socket, awaitable, ACCEPT);
        }

        public static SocketAwaitable ConnectAsync(this Socket socket, SocketAwaitable awaitable)
        {
            return OperateAsync(socket, awaitable, CONNECT);
        }

        public static SocketAwaitable ReceiveAsync(this Socket socket, SocketAwaitable awaitable)
        {
            return OperateAsync(socket, awaitable, RECEIVE);
        }

        public static SocketAwaitable SendAsync(this Socket socket, SocketAwaitable awaitable)
        {
            return OperateAsync(socket, awaitable, SEND);
        }

        private static SocketAwaitable OperateAsync(Socket socket, SocketAwaitable awaitable, Func<Socket, SocketAsyncEventArgs, bool> operation)
        {
            if (awaitable == null)
                throw new ArgumentNullException("awaitable");

            awaitable.Reset();
            if (!operation.Invoke(socket, awaitable.SAEA))
                awaitable.IsCompleted = true;
            return awaitable;
        }
    }
}
