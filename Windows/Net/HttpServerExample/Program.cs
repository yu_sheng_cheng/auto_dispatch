﻿using Net;
using Net.Http;
using Net.Http.WebSockets;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HttpServerExample
{
    class Program
    {
        static byte[] HttpResponse400;
        static byte[] HttpResponse401;
        static byte[] HttpResponse404;

        static void Main(string[] args)
        {
            HttpResponse400 = PrebuildHttpResponse(400, "{\"Result\":false,\"ErrorCode\":400,\"Message\":\"請求內容有錯誤\"}");
            HttpResponse401 = PrebuildHttpResponse(401, "{\"Result\":false,\"ErrorCode\":401,\"Message\":\"輸入的員工編號不存在\"}");
            HttpResponse404 = PrebuildHttpResponse(404, "{\"Result\":false,\"ErrorCode\":404,\"Message\":\"不支援此功能\"}");

            MicroHttpServer server = new MicroHttpServer(8080, 1024);

            TcpSocketConfiguration configuration = server.Configuration;
            configuration.ReceiveBufferSize = 32 << 10;
            configuration.SendBufferSize = 32 << 10;
            configuration.IsKeepAliveEnabled = true;
            configuration.KeepAliveTime = 10000;
            configuration.KeepAliveInterval = 1000;
            configuration.PendingConnectionBacklog = 100;

            server.Start(connection =>
            {
                connection.OnRequest = (context) =>
                {
                    HttpRequest request = context.Request;

                    //Console.WriteLine("Method: {0}", request.Method);
                    //Console.WriteLine("Path: {0}", request.Path);
                    //Console.WriteLine("KeepAlive: {0}", request.KeepAlive.ToString());

                    if (request.Method == "GET")
                    {
                        Console.WriteLine("{0}", request.Path);

                        HttpResponse response = context.Response;
                        //response.StatusCode = 404;
                        response.ContentType = "text/html";
                        //response.KeepAlive = false;

                        context.SendResponse(HttpResponse.BuildMessage(response, Encoding.UTF8.GetBytes("<HTML><BODY> Hello world!</BODY></HTML>")));
                        //context.SendResponse(Encoding.UTF8.GetBytes(response.ToString()));
                    }
                    else if (request.Method == "POST")
                    {                 
                        Console.WriteLine("{0}", request.Path);
                        //string[] hierarchies = request.Path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                        string[] hierarchies = HttpRequest.ExtractSegmentsFromPath(request.Path);

                        if (hierarchies.Length < 2 || hierarchies[0] != "automated-dispensing")
                        {
                            context.SendResponse(HttpResponse404);
                            return;
                        }

                        switch (hierarchies[1])
                        {
                            case "login":
                                HandleLogin(context);
                                return;
                            default:
                                context.SendResponse(HttpResponse404);
                                return;
                        }
                    }
                };
                connection.ConfigureWebSocketConnection = (webSocketConnection) =>
                {
                    webSocketConnection.OnOpen = ws => Console.WriteLine($"WebSocket connection {{{ws.SessionKey}}} has been opened.");
                    webSocketConnection.OnClose = ws => Console.WriteLine($"WebSocket connection {{{ws.SessionKey}}} was closed.");
                    webSocketConnection.OnTextFrame = (_, __, ___, ____, _____, text) =>
                    {
                        webSocketConnection.SendTextFrame(text);
                    };
                };
            });

            Console.WriteLine("Press the enter key to exit.");
            Console.ReadLine();
            server.Dispose();
            Console.WriteLine("MicroHttpServer Bye...");
        }

        static byte[] PrebuildHttpResponse(ushort statusCode, string body)
        {
            HttpResponse response = new HttpResponse(statusCode);
            response.KeepAlive = false;
            return BuildHttpResponse(response, body);
        }

        static byte[] BuildHttpResponse(HttpResponse response, string body)
        {
            response.ContentType = "application/json";
            return HttpResponse.BuildMessage(response, Encoding.UTF8.GetBytes(body));
        }

        static void HandleLogin(IHttpContext context)
        {
            HttpRequest request = context.Request;
            string body = request.Body;

            if (body == null)
            {
                context.SendResponse(HttpResponse400);
                return;
            }

            LoginArgs args = DeserializeJson<LoginArgs>(body);
            if (args == null)
            {
                context.SendResponse(HttpResponse400);
                return;
            }

            string userData;

            switch (args.EmployeeId)
            {
                case "00000":
                    userData = "{\"Result\":true,\"EmployeeId\":\"00000\",\"EmployeeName\":\"兆強科技\",\"Gender\":\"Male\",\"Permission\":\"SupplierEngineer\"}";
                    break;
                case "90122":
                    userData = "{\"Result\":true,\"EmployeeId\":\"90122\",\"EmployeeName\":\"陳秋楓\",\"Gender\":\"Female\",\"Permission\":\"Manager\"}";
                    break;
                case "26520":
                    userData = "{\"Result\":true,\"EmployeeId\":\"26520\",\"EmployeeName\":\"邱馨賢\",\"Gender\":\"Female\",\"Permission\":\"Manager\"}";
                    break;
                default:
                    context.SendResponse(HttpResponse401);
                    return;
            }

            context.SendResponse(BuildHttpResponse(context.Response, userData));
        }

        static T DeserializeJson<T>(string json) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}", e);
                return null;
            }
        }
    }
}
