﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HttpServerExample
{
    [DataContract]
    class LoginArgs
    {
        [DataMember(IsRequired = true)]
        public string EmployeeId { get; set; }
    }
}
