﻿using Net.AsyncSocket.Tcp;
using Net.Http.HttpClients;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HttpServerTest
{
    class Program
    {
        static int counter;

        static void Main(string[] args)
        {
#if true
            Task.Run(async () =>
            {
#if false
                using (AsyncTcpClient client = new AsyncTcpClient(IPAddress.Parse("127.0.0.1"), 8080))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("POST /automated-dispensing/login HTTP/1.1\r\n");
                    sb.Append("Host: localhost:8080\r\n");
                    sb.Append("content-length: 23\r\n\r\n");
                    sb.Append("{\"EmployeeId\": \"00000\"}");

                    byte[] request = Encoding.UTF8.GetBytes(sb.ToString());

                    await client.ConnectAsync().ConfigureAwait(false);

                    Stopwatch watch = new Stopwatch();

                    byte[] buffer = new byte[1024];

                    long[] report = new long[100];

                    for (int i = 0; i < 100; i++)
                    {
                        watch.Reset();
                        watch.Start();

                        await client.SendAsync(request, 0, request.Length).ConfigureAwait(false);
                        int bytesRead = await client.ReceiveAsync(buffer, 0, buffer.Length).ConfigureAwait(false);

                        watch.Stop();

                        report[i] = watch.ElapsedMilliseconds;
                        //Thread.Sleep(100);
                        Console.WriteLine("Receive {0} bytes, ElapsedMilliseconds: {1}", bytesRead.ToString(), watch.ElapsedMilliseconds.ToString());
                        Console.WriteLine("{0}", Encoding.UTF8.GetString(buffer, 0, bytesRead));
                    }

                    foreach (var ms in report)
                    {
                        Console.WriteLine("ElapsedMilliseconds: {0}", ms.ToString());
                    }
                }
#else
                using (MicroHttpClient client = new MicroHttpClient(IPAddress.Parse("127.0.0.1"), 8080))
                {
                    await client.ConnectAsync().ConfigureAwait(false);

                    Stopwatch watch = new Stopwatch();
                    watch.Start();

                    for (int i = 0; i < 100; ++i)
                    {
                        HttpContext context = new HttpContext(client);
                        HttpRequest request = context.Request;

                        request.Method = "POST";
                        request.Path = "/automated-dispensing/login";

                        context.SendRequest(HttpRequest.BuildMessage(request, Encoding.UTF8.GetBytes("{\"EmployeeId\": \"00000\"}")),
                        (c) =>
                        {
                            HttpResponse response = c.Response;

                            /*StringBuilder sb = new StringBuilder();
                            sb.Append($"StatusCode: {response.StatusCode.ToString()} ");
                            sb.Append($"ReasonPhrase: {response.ReasonPhrase} ");
                            sb.Append($"KeepAlive: {response.KeepAlive.ToString()} ");
                            sb.Append($"ContentType: {response.ContentType} ");
                            if (response.Body != null)
                                sb.Append($"Body: {response.Body}");
                            Console.WriteLine("{0}", sb.ToString());*/

                            if (Interlocked.Increment(ref counter) == 100)
                            {
                                watch.Stop();
                                Console.WriteLine("ElapsedMilliseconds: {0}", watch.ElapsedMilliseconds.ToString());
                            }
                        });
                    }

                    Console.ReadLine();
                }
#endif
            });
#else
            HttpClient client = new HttpClient();

            Task.Run(async () =>
            {
                string[] employeeIds = new string[] { "00000", "90122", "26520", "9487" };

                foreach(var id in employeeIds)
                {
                    var stringContent = new StringContent($"{{\"EmployeeId\":\"{id}\"}}", Encoding.UTF8, "application/json");

                    HttpResponseMessage response = await client.PostAsync("http://localhost:8080/automated-dispensing/login", stringContent);

                    string responseBody = await response.Content.ReadAsStringAsync();

                    if (response.IsSuccessStatusCode)
                    {
                        User user = DeserializeJson<User>(responseBody);

                        Console.WriteLine("Result: {0}", user.Result.ToString());
                        Console.WriteLine("EmployeeId: {0}", user.EmployeeId);
                        Console.WriteLine("EmployeeName: {0}", user.EmployeeName);
                        Console.WriteLine("Gender: {0}", user.Gender.ToString());
                        Console.WriteLine("Permission: {0}", user.Permission.ToString());
                    }
                    else
                    {
                        ErrorMessage msg = DeserializeJson<ErrorMessage>(responseBody);

                        Console.WriteLine("Result: {0}", msg.Result.ToString());
                        Console.WriteLine("ErrorCode: {0}", msg.ErrorCode.ToString());
                        Console.WriteLine("Message: {0}", msg.Message);
                    }
                }
            });
#endif
            Console.ReadLine();
        }

        static T DeserializeJson<T>(string json) where T : class
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception e)
            {
                Console.WriteLine("{0}", e);
                return null;
            }
        }
    }

    public enum Gender
    {
        Male,
        Female
    }

    public enum Permission
    {
        SupplierEngineer,
        MaintenanceMan,
        Manager,
        ResearchFellow,
        Operator
    }

    [DataContract]
    class User
    {
        [DataMember(IsRequired = true)]
        public string Result { get; set; }

        [DataMember(IsRequired = true)]
        public string EmployeeId { get; set; }

        [DataMember(IsRequired = true)]
        public string EmployeeName { get; set; }

        [DataMember(IsRequired = true)]
        public Gender Gender { get; set; }

        [DataMember(IsRequired = true)]
        public Permission Permission { get; set; }
    }

    [DataContract]
    class ErrorMessage
    {
        [DataMember(IsRequired = true)]
        public string Result { get; set; }

        [DataMember(IsRequired = true)]
        public int ErrorCode { get; set; }

        [DataMember(IsRequired = true)]
        public string Message { get; set; }
    }
}
