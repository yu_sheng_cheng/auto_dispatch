﻿using Net;
using Net.AsyncSocket.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncEchoTcpServer
{
    class Program
    {
        static void Main(string[] args)
        {
            AsyncTcpServer server = new AsyncTcpServer(9487, 8192);

            TcpSocketConfiguration configuration = server.Configuration;
            configuration.ReceiveBufferSize = 64 << 10;
            configuration.SendBufferSize = 64 << 10;
            configuration.IsKeepAliveEnabled = true;
            configuration.KeepAliveTime = 10000;
            configuration.KeepAliveInterval = 1000;
            configuration.PendingConnectionBacklog = 10000;

            server.ClientConnected += AsyncTcpServer_ClientConnected;
            server.Start();

            Console.WriteLine("Press the enter key to exit.");
            Console.ReadLine();
            server.Dispose();
            Console.WriteLine("AsyncEchoTcpServer Bye...");
        }

        private static async void AsyncTcpServer_ClientConnected(object sender, ClientConnectedEventArgs e)
        {
            IAsyncTcpSession session = e.Session;

            Console.WriteLine("New client connected. SessionKey[{0}], RemoteEndPoint[{1}]", session.SessionKey, session.RemoteEndPoint);

            try
            {
                byte[] buffer = new byte[64 << 10];
#if true
                while (true)
                {
                    int bytesRead = await session.ReceiveAsync(buffer).ConfigureAwait(false);
                    Console.WriteLine("SessionKey[{0}] receive {1} bytes", session.SessionKey, bytesRead.ToString());
                    await session.SendAsync(buffer, 0, bytesRead).ConfigureAwait(false);
                    Console.WriteLine("SessionKey[{0}] send {1} bytes", session.SessionKey, bytesRead.ToString());
                }
#else
                session.SetReceiveBuffer(buffer, 0, buffer.Length);
                session.SetSendBuffer(buffer, 0, buffer.Length);

                while (true)
                {
                    int bytesRead = await session.ReceiveAsync().ConfigureAwait(false);
                    Console.WriteLine("SessionKey[{0}] receive {1} bytes", session.SessionKey, bytesRead.ToString());
                    await session.SendAsync(0, bytesRead).ConfigureAwait(false);
                    Console.WriteLine("SessionKey[{0}] send {1} bytes", session.SessionKey, bytesRead.ToString());
                }
#endif
            }
            catch (Exception ex)
            {
                session.Server.CloseSession(session);
                Console.WriteLine("SessionKey[{0}] Bye...{1}{2}", session.SessionKey, Environment.NewLine, ex);
            }
        }
    }
}
