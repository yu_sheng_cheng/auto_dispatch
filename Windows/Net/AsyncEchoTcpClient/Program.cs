﻿using Net;
using Net.AsyncSocket.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncEchoTcpClient
{
    class Program
    {
        private const int NumOfParallelConnections = 32;
        private static int CompletionCount;

        static void Main(string[] args)
        {
            Parallel.For(0, NumOfParallelConnections, async _ => await EchoAsync().ConfigureAwait(false));
            Console.ReadLine();
        }

        static async Task EchoAsync()
        {
            using (AsyncTcpClient client = new AsyncTcpClient(IPAddress.Parse("127.0.0.1"), 9487))
            {
                TcpSocketConfiguration configuration = client.Configuration;
                configuration.ReceiveBufferSize = 64 << 10;
                configuration.SendBufferSize = 64 << 10;
                configuration.IsKeepAliveEnabled = true;
                configuration.KeepAliveTime = 10000;
                configuration.KeepAliveInterval = 1000;

                await client.ConnectAsync().ConfigureAwait(false);
                Console.WriteLine("AsyncTcpClient has connected to server [{0}]", client.RemoteEndPoint);

                byte[] buffer = Enumerable.Repeat((byte)87, 64 << 10).ToArray();

#if true
                int bytesSent = 0;
                while (bytesSent < buffer.Length)
                {
                    int bytes = Math.Min(1024, buffer.Length - bytesSent);
                    await client.SendAsync(buffer, bytesSent, bytes).ConfigureAwait(false);
                    Console.WriteLine("Send {0} bytes", bytes.ToString());
                    int bytesRead = await client.ReceiveAsync(buffer, bytesSent, buffer.Length - bytesSent).ConfigureAwait(false);
                    Console.WriteLine("Receive {0} bytes", bytesRead.ToString());

                    bytesSent += bytes;
                }
#else
                int bytesSend = 128;
                while (bytesSend <= buffer.Length)
                {
                    await client.SendAsync(buffer, 0, bytesSend).ConfigureAwait(false);
                    Console.WriteLine("Send {0} bytes", bytesSend.ToString());
                    int bytesRead = await client.ReceiveAsync(buffer).ConfigureAwait(false);
                    Console.WriteLine("Receive {0} bytes", bytesRead.ToString());

                    bytesSend *= 2;
                }
#endif
                if (Interlocked.Increment(ref CompletionCount) == NumOfParallelConnections)
                    Console.WriteLine("{0} connection has been completed ", NumOfParallelConnections.ToString());
            }
        }
    }
}
