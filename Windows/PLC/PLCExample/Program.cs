﻿using PLC;
using PLC.Mitsubishi;
using SwissKnife;
using SwissKnife.Log.AppLogger;
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PLCExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                try
                {
                    Industry4._0.Industry4Dot0Environment.Log = new Logger(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "AppLogger.xml"));
                    PLCEnvironment.MakeInfoRootFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Makes");
#if true
                    ArrayBuilder<bool> boolArray = new ArrayBuilder<bool>();
                    ArrayBuilder<short> shortArray = new ArrayBuilder<short>();

                    using (MelsecMCNet melsec = new MelsecMCNet("Q", "Q03UDE"))
                    {
                        melsec.InternetTransport.RemoteEndPoint = new IPEndPoint(IPAddress.Parse("192.168.1.120"), 1025);

                        AsynchronousTimeoutSignal timeoutSignal = melsec.InternetTransport.TimeoutSignal;
                        Task.Run(async () => { await Task.Delay(10000); timeoutSignal.Raise(); }).Forget();

                        for (int i = 0; i < 1024; ++i)
                        {
                            boolArray[i] = true;
                            shortArray[i] = unchecked((short)(i + 1));
                        }

                        OperationResult writeResult = await melsec.WriteAsync("M0", boolArray);
                        if (!writeResult.IsSuccess)
                            Console.WriteLine("{0}", writeResult);

                        writeResult = await melsec.WriteAsync("D0", shortArray);
                        if (!writeResult.IsSuccess)
                            Console.WriteLine("{0}", writeResult);

                        boolArray.Clear();
                        shortArray.Clear();

                        OperationResult readResult = await melsec.ReadBoolAsync("M0", 1024, boolArray);
                        if (!readResult.IsSuccess)
                            Console.WriteLine("{0}", readResult);

                        readResult = await melsec.ReadInt16Async("D0", 1024, shortArray);
                        if (!readResult.IsSuccess)
                            Console.WriteLine("{0}", readResult);

                        int index = 0;
                        foreach (var b in boolArray)
                        {
                            Console.WriteLine("M{0}: {1}", index++.ToString(), b.ToString());
                        }

                        index = 0;
                        foreach (var s in shortArray)
                        {
                            Console.WriteLine("D{0}: {1}", index++.ToString(), s.ToString());
                        }
                    }                    
#else
                    using (MelsecMCNet melsec = new MelsecMCNet("Q", "Q03UDE"))
                    {
                        melsec.InternetTransport.RemoteEndPoint = new IPEndPoint(IPAddress.Parse("192.168.1.120"), 1025);

                        AsynchronousTimeoutSignal timeoutSignal = melsec.InternetTransport.TimeoutSignal;
                        Task.Run(async () => { await Task.Delay(10000); timeoutSignal.Raise(); }).Forget();

                        OperationResult<PLCDeviceTransportBlock> result = melsec.CreateDeviceTransportBlock("M0", 1024);
                        if (!result.IsSuccess)
                            Console.WriteLine("{0}", result);

                        PLCDeviceTransportBlock M0 = result.Value;

                        result = melsec.CreateDeviceTransportBlock("D0", 500);
                        if (!result.IsSuccess)
                            Console.WriteLine("{0}", result);

                        PLCDeviceTransportBlock D0 = result.Value;

                        for (int i = 0; i < 1024; i += 16)
                        {
                            M0.Write(unchecked((uint)i), (ushort)0xFFFF);
                        }
                        for (int i = 0; i < 500; ++i)
                        {
                            D0.Write(unchecked((uint)i), unchecked((short)(i + 1)));
                        }

                        OperationResult writeResult = await melsec.WriteMultipleBlocksAsync(new PLCDeviceTransportBlock[] { D0, M0 });
                        if (!writeResult.IsSuccess)
                            Console.WriteLine("{0}", writeResult);

                        M0.ClearBuffer();
                        D0.ClearBuffer();

                        OperationResult readResult = await melsec.ReadMultipleBlocksAsync(new PLCDeviceTransportBlock[] { D0, M0 });
                        if (!readResult.IsSuccess)
                            Console.WriteLine("{0}", readResult);

                        for (int i = 0; i < 1024; i += 16)
                        {
                            ushort value = M0.ReadUInt16(unchecked((uint)i));
                            Console.WriteLine("M{0}: {1}", i.ToString(), value.ToString());
                        }
                        for (int i = 0; i < 500; ++i)
                        {
                            short value = D0.ReadInt16(unchecked((uint)i));
                            Console.WriteLine("D{0}: {1}", i.ToString(), value.ToString());
                        }

                        OperationResult remoteOperation = await melsec.RemoteStop();
                        Console.WriteLine("RemoteStop: {0}", remoteOperation);

                        remoteOperation = await melsec.RemoteRun();
                        Console.WriteLine("RemoteRun: {0}", remoteOperation);
                    }
#endif
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0}", e.ToString());
                }
            });

            Console.ReadLine();
        }
    }
}
