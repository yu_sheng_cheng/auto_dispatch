﻿using SwissKnife;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PLC
{
    public static class PLCEnvironment
    {
        private static string _makeInfoRootFolder;
        private static readonly ConcurrentDictionary<string, PLCMake> _makes = new ConcurrentDictionary<string, PLCMake>();

        public static string MakeInfoRootFolder
        {
            get { return _makeInfoRootFolder ?? Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PLCMakes"); }
            set { _makeInfoRootFolder = value; }
        }

        public static PLCMake QueryPLCMakeInfo(string makeName)
        {
            PLCMake make;

            if (_makes.TryGetValue(makeName, out make))
                return make;

            ReloadPLCMake(makeName);
            _makes.TryGetValue(makeName, out make);
            return make;
        }

        public static void ReloadPLCMake(string makeName)
        {
            if (string.IsNullOrWhiteSpace(makeName))
                throw new ArgumentException("The makeName argument cannot be null or an empty string.", "makeName");

            PLCMake make;

            using (var reader = new StreamReader(Path.Combine(MakeInfoRootFolder, $"{makeName}.xml"), Encoding.UTF8))
            {
                var serializer = new XmlSerializer(typeof(PLCMake));
                make = (PLCMake)serializer.Deserialize(reader);
                if (makeName != make.Name)
                    throw new FormatException($"The Name attribute must be equal to {makeName}");
                make.CheckAfterDeserialization();
            }
         
            while (true)
            {
                PLCMake result = _makes.GetOrAdd(makeName, make);
                if (result == make)
                    break;

                Version curVer = Version.Parse(result.Version);
                Version newVer = Version.Parse(make.Version);
                if (curVer >= newVer || _makes.TryUpdate(makeName, make, result))
                    break;
            }
        }
    }
}
