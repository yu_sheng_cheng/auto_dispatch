﻿using SwissKnife;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PLC
{
    public sealed class PLCDeviceBlock
    {
        private Func<string, PLCDevice> TryParseDevice;

        public PLCDevice Device { get; private set; }

        public uint BaseAddress { get; set; }

        public ushort Length { get; set; }

        public PLCDeviceBlock(Func<string, PLCDevice> TryParseDeviceFunc)
        {
            TryParseDevice = TryParseDeviceFunc ?? throw new ArgumentNullException("TryParseDeviceFunc");
        }

        public bool TryParse(string address, ushort length)
        {
            if (address == null)
                return false;

            PLCDevice device = TryParseDevice(address);
            if (device == null)
                return false;

            uint baseAddress;

            try { baseAddress = Convert.ToUInt32(address.Substring(device.Symbol.Length), (int)device.Notation); }
            catch { return false; }

            if (length > (long)uint.MaxValue - baseAddress + 1)
                return false;

            Device = device;
            BaseAddress = baseAddress;
            Length = length;
            return true;
        }
    }
}
