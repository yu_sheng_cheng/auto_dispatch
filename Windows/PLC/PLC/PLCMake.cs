﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PLC
{
    [XmlRoot(ElementName = "Make", Namespace = "PLC.Make.xml", IsNullable = false)]
    public sealed class PLCMake
    {
        private Dictionary<string, PLCSeries> _dictionary;

        [XmlAttribute] public string Version { get; set; }
        [XmlAttribute] public string Name { get; set; }
        [XmlAttribute] public string DisplayName { get; set; }
        [XmlArrayItem(ElementName = "Series")] public PLCSeries[] Series { get; set; } = new PLCSeries[0];

        public void CheckAfterDeserialization()
        {
            System.Version.Parse(Version);
            if (string.IsNullOrWhiteSpace(Name))
                throw new FormatException("The 'Name' attribute cannot be empty or null.");
            if (string.IsNullOrWhiteSpace(DisplayName))
                throw new FormatException("The 'DisplayName' attribute cannot be empty or null.");

            foreach (var series in Series)
            {
                series.CheckAfterDeserialization();
            }

            if (Series.Length < 7)
                _dictionary = null;
            else
            {
                if (_dictionary != null)
                    _dictionary.Clear();
                else
                    _dictionary = new Dictionary<string, PLCSeries>(StringComparer.OrdinalIgnoreCase);

                foreach (var series in Series)
                {
                    if (!_dictionary.ContainsKey(series.Name))
                        _dictionary.Add(series.Name, series);
                }
            }
        }

        public PLCSeries GetSeries(string name)
        {
            if (name == null)
                throw new ArgumentNullException("name");

            if (_dictionary == null)
            {
                foreach (var series in Series)
                {
                    if (name == series.Name)
                        return series;
                }
            }
            else
            {
                PLCSeries series;
                if (_dictionary.TryGetValue(name, out series))
                    return series;
            }
            return null;
        }
    }

    public struct PLCSeriesExtendedProperty
    {
        [XmlAttribute] public string Name { get; set; }
        [XmlAttribute] public string Value { get; set; }
    }

    public sealed class PLCSeries
    {
        private Dictionary<string, PLCType> _dictionary;

        [XmlAttribute] public string Name { get; set; }
        [XmlAttribute] public string DisplayName { get; set; }
        public PLCSpec Spec { get; set; } = new PLCSpec();
        [XmlArrayItem(ElementName = "Property")] public PLCSeriesExtendedProperty[] ExtendedProperties { get; set; } = new PLCSeriesExtendedProperty[0];
        [XmlArrayItem(ElementName = "Type")] public PLCType[] Types { get; set; } = new PLCType[0];

        public void CheckAfterDeserialization()
        {
            if (string.IsNullOrWhiteSpace(Name))
                throw new FormatException("The 'Name' attribute cannot be empty or null.");
            if (string.IsNullOrWhiteSpace(DisplayName))
                throw new FormatException("The 'DisplayName' attribute cannot be empty or null.");

            Spec.CheckAfterDeserialization();

            foreach (var property in ExtendedProperties)
            {
                if (string.IsNullOrWhiteSpace(property.Name))
                    throw new FormatException("The 'Name' attribute cannot be empty or null.");
                if (property.Value == null)
                    throw new FormatException("The Value attribute was not set.");
            }

            foreach (var type in Types)
            {
                type.CheckAfterDeserialization(this);
            }

            if (Types.Length < 7)
                _dictionary = null;
            else
            {
                if (_dictionary != null)
                    _dictionary.Clear();
                else
                    _dictionary = new Dictionary<string, PLCType>(StringComparer.OrdinalIgnoreCase);

                foreach (var type in Types)
                {
                    if (!_dictionary.ContainsKey(type.Name))
                        _dictionary.Add(type.Name, type);
                }
            }
        }

        public PLCType GetType(string name)
        {
            if (name == null)
                throw new ArgumentNullException("name");

            if (_dictionary == null)
            {
                foreach (var type in Types)
                {
                    if (name == type.Name)
                        return type;
                }
            }
            else
            {
                PLCType type;
                if (_dictionary.TryGetValue(name, out type))
                    return type;
            }
            return null;
        }
    }

    public sealed class PLCType
    {
        [XmlAttribute] public string Name { get; set; }
        public PLCSpec Spec { get; set; }

        public void CheckAfterDeserialization(PLCSeries series)
        {
            if (string.IsNullOrWhiteSpace(Name))
                throw new FormatException("The 'Name' attribute cannot be empty or null.");

            if (Spec == null)
                Spec = series.Spec;
            else
                Spec.CheckAfterDeserialization();
        }
    }
}
