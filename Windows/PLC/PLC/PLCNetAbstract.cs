﻿using Industry4._0.IoT;
using Net.AsyncSocket.Tcp;
using PLC.Protocols;
using SwissKnife;
using SwissKnife.Conversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC
{
    public abstract class PLCNetAbstract<T1, T2> : PLCAbstract<T1, T2> where T1 : IByteConverter, new() where T2 : IPLCProtocol, new()
    {
        public IInternetTransport InternetTransport { get { return Transport as IInternetTransport; } }

        public PLCNetAbstract(string makeName, string seriesName, string typeName) : base(makeName, seriesName, typeName)
        {
            InternetTransport transport = new InternetTransport(PLCProtocol);
            transport.Connected = OnConnected;
            Transport = transport;
        }

        protected virtual Task<OperationResult> OnConnected(AsyncTcpClient session)
        {
            return Task.FromResult(OperationResultFactory.CreateSuccessfulResult());
        }
    }
}
