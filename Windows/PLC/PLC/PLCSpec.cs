﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PLC
{
    public struct PLCSpecExtendedProperty
    {
        [XmlAttribute] public string Name { get; set; }
        [XmlAttribute] public string Value { get; set; }
    }

    public sealed class PLCSpec
    {
        [XmlAttribute] public bool IsBitOperationSupported { get; set; } = false;

        [XmlAttribute] public int BatchBitAccessLimit { get; set; } = 0;

        [XmlAttribute] public int BatchWordAccessLimit { get; set; } = -1;

        [XmlAttribute] public bool IsMultipleBlocksAccessSupported { get; set; } = false;

        [XmlAttribute] public int WordReadLimitPerBlock { get; set; } = 0;

        [XmlAttribute] public int WordWriteLimitPerBlock { get; set; } = 0;

        [XmlArrayItem(ElementName = "Property")] public PLCSpecExtendedProperty[] ExtendedProperties { get; set; } = new PLCSpecExtendedProperty[0];

        public void CheckAfterDeserialization()
        {
            foreach (var property in ExtendedProperties)
            {
                if (string.IsNullOrWhiteSpace(property.Name))
                    throw new FormatException("The 'Name' attribute cannot be empty or null.");
                if (property.Value == null)
                    throw new FormatException("The Value attribute was not set.");
            }
        }
    }
}
