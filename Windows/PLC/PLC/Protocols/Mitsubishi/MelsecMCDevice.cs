﻿using SwissKnife;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Protocols.Mitsubishi
{
    public class MelsecMCDevice : PLCDevice
    {
        public ushort BinaryCode { get; }

        public string AsciiCode { get; }

        protected MelsecMCDevice(string symbol, ushort binaryCode, string asciiCode, CarryBase notation, bool bitType) : base(symbol, notation, bitType)
        {
            BinaryCode = binaryCode;
            AsciiCode = asciiCode;
        }

        public static readonly MelsecMCDevice X = new MelsecMCDevice("X", 0x9C, "X*", CarryBase.Hexadecimal, true);
        public static readonly MelsecMCDevice Y = new MelsecMCDevice("Y", 0x9D, "Y*", CarryBase.Hexadecimal, true);
        public static readonly MelsecMCDevice M = new MelsecMCDevice("M", 0x90, "M*", CarryBase.Decimal, true);
        public static readonly MelsecMCDevice D = new MelsecMCDevice("D", 0xA8, "D*", CarryBase.Decimal, false);
        public static readonly MelsecMCDevice W = new MelsecMCDevice("W", 0xB4, "W*", CarryBase.Hexadecimal, false);
        public static readonly MelsecMCDevice L = new MelsecMCDevice("L", 0x92, "L*", CarryBase.Decimal, true);
        public static readonly MelsecMCDevice F = new MelsecMCDevice("F", 0x93, "F*", CarryBase.Decimal, true);
        public static readonly MelsecMCDevice V = new MelsecMCDevice("V", 0x94, "V*", CarryBase.Decimal, true);
        public static readonly MelsecMCDevice B = new MelsecMCDevice("B", 0xA0, "B*", CarryBase.Hexadecimal, true);
        public static readonly MelsecMCDevice R = new MelsecMCDevice("R", 0xAF, "R*", CarryBase.Decimal, false);
        public static readonly MelsecMCDevice S = new MelsecMCDevice("S", 0x98, "S*", CarryBase.Decimal, true);
        public static readonly MelsecMCDevice Z = new MelsecMCDevice("Z", 0xCC, "Z*", CarryBase.Decimal, false);
        public static readonly MelsecMCDevice TN = new MelsecMCDevice("TN", 0xC2, "TN", CarryBase.Decimal, false);
        public static readonly MelsecMCDevice TS = new MelsecMCDevice("TS", 0xC1, "TS", CarryBase.Decimal, true);
        public static readonly MelsecMCDevice TC = new MelsecMCDevice("TC", 0xC0, "TC", CarryBase.Decimal, true);
        public static readonly MelsecMCDevice SS = new MelsecMCDevice("SS", 0xC7, "SS", CarryBase.Decimal, true);
        public static readonly MelsecMCDevice SC = new MelsecMCDevice("SC", 0xC6, "SC", CarryBase.Decimal, true);
        public static readonly MelsecMCDevice SN = new MelsecMCDevice("SN", 0xC8, "SN", CarryBase.Decimal, false);
        public static readonly MelsecMCDevice CN = new MelsecMCDevice("CN", 0xC5, "CN", CarryBase.Decimal, false);
        public static readonly MelsecMCDevice CS = new MelsecMCDevice("CS", 0xC4, "CS", CarryBase.Decimal, true);
        public static readonly MelsecMCDevice CC = new MelsecMCDevice("CC", 0xC3, "CC", CarryBase.Decimal, true);
        public static readonly MelsecMCDevice ZR = new MelsecMCDevice("ZR", 0xB0, "ZR", CarryBase.Hexadecimal, false);

        public static MelsecMCDevice TryParse(string address)
        {
            if (string.IsNullOrEmpty(address))
                return null;

            address = address.ToUpper(CultureInfo.InvariantCulture);

            switch (address[0])
            {
                case 'X': return X;
                case 'Y': return Y;
                case 'M': return M;
                case 'D': return D;
                case 'W': return W;
                case 'L': return L;
                case 'F': return F;
                case 'V': return V;
                case 'B': return B;
                case 'R': return R;
                case 'S':
                    if (address.Length > 1)
                    {
                        switch (address[1])
                        {
                            case 'S': return SS;
                            case 'C': return SC;
                            case 'N': return SN;
                        }
                    }
                    return S;
                case 'Z': return address.StartsWith("ZR") ? ZR : Z;
                case 'T':
                    if (address.Length > 1)
                    {
                        switch (address[1])
                        {
                            case 'N': return TN;
                            case 'S': return TS;
                            case 'C': return TC;
                        }
                    }
                    return null;
                case 'C':
                    if (address.Length > 1)
                    {
                        switch (address[1])
                        {
                            case 'N': return CN;
                            case 'S': return CS;
                            case 'C': return CC;
                        }
                    }
                    return null;
                default: return null;
            }
        }
    }
}
