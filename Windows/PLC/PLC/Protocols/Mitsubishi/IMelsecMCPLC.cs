﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Protocols.Mitsubishi
{
    public interface IMelsecMCPLC
    {
        byte NetworkNumber { get;}

        byte StationNumber { get; }
    }
}
