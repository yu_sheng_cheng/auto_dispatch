﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Industry4._0;
using PLC.Mitsubishi;
using SwissKnife;
using SwissKnife.Conversion;
using SwissKnife.Exceptions;

namespace PLC.Protocols.Mitsubishi
{
    public class MelsecMCBinaryProtocol : IPLCProtocol
    {
        private static readonly byte[] _emptyArray = new byte[0];

        private readonly IByteConverter _byteConverter = new LittleEndianConverter();
        private readonly ArrayBuilder<byte> _byteArray = new ArrayBuilder<byte>();
        private readonly List<PLCDeviceTransportBlock> _deviceBlocks = new List<PLCDeviceTransportBlock>();

        public bool IsEndsWith { get { return false; } }

        public string EndString => throw new NotImplementedException();

        public int HeaderLength { get { return 9; } }

        public int GetContentLengthFromHeader(ArrayBuilder<byte> receiveBuffer, ArrayBuilder<byte> sendBuffer)
        {
            return _byteConverter.ToUInt16(receiveBuffer.Buffer, 7);
        }

        public bool ValidateHeader(ArrayBuilder<byte> receiveBuffer, ArrayBuilder<byte> sendBuffer)
        {
            if (receiveBuffer[0] != 0xD0 | receiveBuffer[1] != 0x00)
                return false;

            for (int i = 2; i <= 6; ++i)
            {
                if (receiveBuffer[i] != sendBuffer[i])
                    return false;
            }

            return GetContentLengthFromHeader(receiveBuffer, sendBuffer) >= 2;
        }

        public bool Validate(ArrayBuilder<byte> receiveBuffer, ArrayBuilder<byte> sendBuffer)
        {
            return true;
        }

        public void BuildDeviceWordReadCommand(ArrayBuilder<byte> buffer, IPLC plc, PLCDeviceBlock block)
        {
            BuildDeviceAccessCommand(buffer, plc, block, false, new ArraySegment<byte>(_emptyArray));
        }

        public void BuildDeviceWordWriteCommand(ArrayBuilder<byte> buffer, IPLC plc, PLCDeviceBlock block, ArraySegment<byte> data)
        {
            BuildDeviceAccessCommand(buffer, plc, block, false, data);
        }

        public void BuildDeviceBitReadCommand(ArrayBuilder<byte> buffer, IPLC plc, PLCDeviceBlock block)
        {
            BuildDeviceAccessCommand(buffer, plc, block, true, new ArraySegment<byte>(_emptyArray));
        }

        public void BuildDeviceBitWriteCommand(ArrayBuilder<byte> buffer, IPLC plc, PLCDeviceBlock block, ArraySegment<bool> data)
        {
#if DEBUG
            if (data.Count == 0)
                throw new ArgumentException("The data length must be greater than zero.", "data");
#endif
            _byteArray.Clear();

            bool[] values = data.Array;
                       
            int index = 0;
            int count = data.Count & ~1;
            int end = data.Offset + count;

            for (int i = data.Offset; i < end; i += 2)
            {
                byte val = 0;

                if (values[i]) val |= 0x10;
                if (values[i + 1]) val |= 0x01;

                _byteArray[index++] = val;
            }

            bool odd = (data.Count & 0x01) != 0;
            if (odd)
                _byteArray[index] = values[end] ? (byte)0x10 : (byte)0;

            BuildDeviceAccessCommand(buffer, plc, block, true, new ArraySegment<byte>(_byteArray.Buffer, 0, _byteArray.Length));
        }

        private void BuildDeviceAccessCommand(ArrayBuilder<byte> buffer, IPLC plc, PLCDeviceBlock block, bool bitAccess, ArraySegment<byte> dataToWrite)
        {
#if DEBUG
            if (buffer == null)
                throw new ArgumentNullException("buffer");

            IMelsecMCPLC melsec = plc as IMelsecMCPLC;
            if (melsec == null)
                throw new ArgumentException("PLC must implement the interface 'IMelsecMCPLC'.", "plc");

            if (block == null)
                throw new ArgumentNullException("block");
            
            MelsecMCDevice device = block.Device as MelsecMCDevice;
            if (device == null)
                throw new ArgumentException("The plc device must be derived from MelsecMCDevice.", "block");
#else
            IMelsecMCPLC melsec = plc as IMelsecMCPLC;
            MelsecMCDevice device = block.Device as MelsecMCDevice;
#endif

            PrepareMCHeader(buffer, melsec.NetworkNumber, melsec.StationNumber);

            int startIndex = buffer.Length;

            if (dataToWrite.Array != _emptyArray)
            {
#if DEBUG
                if (dataToWrite.Count == 0)
                    throw new ArgumentException("The ArraySegment<byte> count must be greater than 0.", "dataToWrite");
#endif
                buffer[startIndex] = 0x01; // batch write
                buffer[startIndex + 1] = 0x14;
                buffer.Insert(startIndex + 10, dataToWrite.Array, dataToWrite.Offset, dataToWrite.Count);
            }
            else
            {
                buffer.Length = startIndex + 10;
                buffer[startIndex] = 0x01; // batch read
                buffer[startIndex + 1] = 0x04;
            }

            buffer[startIndex + 2] = bitAccess ? (byte)0x01 : (byte)0x00;
            buffer[startIndex + 3] = 0x00;
            _byteConverter.GetBytes(block.BaseAddress, buffer.Buffer, startIndex + 4);
            buffer[startIndex + 7] = (byte)device.BinaryCode;
            _byteConverter.GetBytes(block.Length, buffer.Buffer, startIndex + 8);

            CompleteMCPack(buffer);
        }

        public OperationResult BuildMultipleBlocksReadCommand(ArrayBuilder<byte> buffer, IPLC plc, IEnumerable<PLCDeviceTransportBlock> blocks)
        {
            return BuildMultipleBlocksAccessCommand(buffer, plc, blocks, true);
        }

        public OperationResult BuildMultipleBlocksWriteCommand(ArrayBuilder<byte> buffer, IPLC plc, IEnumerable<PLCDeviceTransportBlock> blocks)
        {
            return BuildMultipleBlocksAccessCommand(buffer, plc, blocks, false);
        }

        private OperationResult BuildMultipleBlocksAccessCommand(ArrayBuilder<byte> buffer, IPLC plc, IEnumerable<PLCDeviceTransportBlock> blocks, bool readOperation)
        {
#if DEBUG
            if (buffer == null)
                throw new ArgumentNullException("buffer");

            IMelsecMCPLC melsec = plc as IMelsecMCPLC;
            if (melsec == null)
                throw new ArgumentException("PLC must implement the interface 'IMelsecMCPLC'.", "plc");

            if (blocks == null)
                throw new ArgumentNullException("blocks");
#else
            IMelsecMCPLC melsec = plc as IMelsecMCPLC;
#endif
            _deviceBlocks.Clear();
            _deviceBlocks.AddRange(blocks);

            if (_deviceBlocks.Count < 1 | _deviceBlocks.Count > 120)
            {
                _deviceBlocks.Clear();
                return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.ArgumentOutOfRange, string.Format(SR.OutOfRange, SR.NumberOfBlocks));
            }

            _deviceBlocks.Sort((x, y) =>
            {
                PLCDevice xdev = x.DeviceBlock.Device;
                PLCDevice ydev = y.DeviceBlock.Device;
                if (xdev.IsBitUnit == ydev.IsBitUnit)
                    return 0;
                return xdev.IsBitUnit ? 1 : -1;
            });

            int batchAccessLimitPerBlock = readOperation ? plc.Type.Spec.WordReadLimitPerBlock : (plc.Type.Spec.WordWriteLimitPerBlock - _deviceBlocks.Count * 4);
            if (batchAccessLimitPerBlock > ushort.MaxValue | batchAccessLimitPerBlock <= 0)
                throw new ArgumentOutOfRangeException("WordAccessLimitPerBlock");
            
            byte numOfWordDeviceBlocks = 0, numOfBitDeviceBlocks = 0;
            int numOfWords = 0;

            foreach (var block in _deviceBlocks)
            {
                MelsecMCDevice device = block.DeviceBlock.Device as MelsecMCDevice;
#if DEBUG
                if (device == null)
                    throw new ArgumentException("The plc device must be derived from MelsecMCDevice.", "blocks");
#endif
                if (device.IsBitUnit)
                    ++numOfBitDeviceBlocks;
                else
                    ++numOfWordDeviceBlocks;

                if (block.DeviceBlock.Length > batchAccessLimitPerBlock)
                {
                    _deviceBlocks.Clear();
                    return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.ArgumentOutOfRange, string.Format(SR.OutOfRange, SR.BlockLength));
                }

                numOfWords += block.DeviceBlock.Length;
            }

            PrepareMCHeader(buffer, melsec.NetworkNumber, melsec.StationNumber);

            int startIndex = buffer.Length;
            buffer.Length = startIndex + 6 + 6 * _deviceBlocks.Count;

            if (readOperation)
            {
                buffer[startIndex] = 0x06;
                buffer[startIndex + 1] = 0x04;
            }
            else
            {
                buffer.Length += numOfWords << plc.ByteOrderPerWord;
                buffer[startIndex] = 0x06;
                buffer[startIndex + 1] = 0x14;
            }
            buffer[startIndex + 2] = 0x00;
            buffer[startIndex + 3] = 0x00;
            buffer[startIndex + 4] = numOfWordDeviceBlocks;
            buffer[startIndex + 5] = numOfBitDeviceBlocks;

            startIndex += 6;
            foreach (var block in _deviceBlocks)
            {
                MelsecMCDevice device = (MelsecMCDevice)block.DeviceBlock.Device;

                _byteConverter.GetBytes(block.DeviceBlock.BaseAddress, buffer.Buffer, startIndex);
                buffer[startIndex + 3] = (byte)device.BinaryCode;
                _byteConverter.GetBytes(block.DeviceBlock.Length, buffer.Buffer, startIndex + 4);

                startIndex += 6;
                if (!readOperation)
                {
                    buffer.Insert(startIndex, block.Buffer);
                    startIndex += block.Buffer.Length;
                }
            }

            CompleteMCPack(buffer);
            return OperationResultFactory.CreateSuccessfulResult();
        }

        public void PackMCCommand(ArrayBuilder<byte> buffer, byte[] content, byte networkNumber = 0, byte stationNumber = 0)
        {
#if DEBUG
            if (buffer == null)
                throw new ArgumentNullException("buffer");
            if (content == null)
                throw new ArgumentNullException("content");
            if (content.Length == 0)
                throw new ArgumentException("The content length must be greater than zero.", "content");
#endif
            PrepareMCHeader(buffer, networkNumber, stationNumber);
            buffer.Append(content);
            CompleteMCPack(buffer);
        }

        private void PrepareMCHeader(ArrayBuilder<byte> buffer, byte networkNumber, byte stationNumber)
        {
            buffer.Clear();
            buffer[0] = 0x50;
            buffer[1] = 0x00;
            buffer[2] = networkNumber;
            buffer[3] = 0xFF;
            buffer[4] = 0xFF;
            buffer[5] = 0x03;
            buffer[6] = stationNumber;
            buffer[9] = 0x10; // monitoring timer
            buffer[10] = 0x00;
        }

        private void CompleteMCPack(ArrayBuilder<byte> buffer)
        {
            int contentLength = buffer.Length - 9; // monitoring timer + request data
            if (contentLength > ushort.MaxValue)
                throw new OutOfLimitException("Packet length is over limitation.");
            _byteConverter.GetBytes((ushort)contentLength, buffer.Buffer, 7);
        }

        public OperationResult<int, int> ParseMessageData(ArrayBuilder<byte> message, IPLC plc, ushort operation)
        {
#if DEBUG
            if (message == null)
                throw new ArgumentNullException("message");
            if (message.Length < 11)
                throw new ArgumentException("Length property of message argument must be greater than or equal to 11.", "message");
            if (plc == null)
                throw new ArgumentNullException("plc");
#endif
            ushort errorCode = _byteConverter.ToUInt16(message.Buffer, 9);
            if (errorCode != 0)
                return OperationResultFactory.CreateFailedResult<int, int>(GetType().FullName, (int)Industry4Dot0Error.OperationBadReturn, errorCode, SR.OperationBadReturn);

            int actualDataStartIndex = 11;
            int actualDataLength = message.Length - actualDataStartIndex;

            switch (operation)
            {
                case (ushort)PLCOperation.DeviceBitRead:
                    _byteArray.Clear();
                    int index = 0;
                    int end = message.Length;

                    for (int i = actualDataStartIndex; i < end; ++i)
                    {
                        byte val = message[i];
                        _byteArray[index++] = (byte)(val >> 4);
                        _byteArray[index++] = (byte)(val & 0xf);
                    }
                    message.Insert(actualDataStartIndex, _byteArray.Buffer, 0, _byteArray.Length);
                    return OperationResultFactory.CreateSuccessfulResult(actualDataStartIndex, message.Length - actualDataStartIndex, SR.Success);
                case (ushort)PLCOperation.MultipleBlocksRead:
                    int byteOrderPerWord = plc.ByteOrderPerWord;
                    int numOfWords = 0;
                    foreach (var block in _deviceBlocks)
                    {
                        numOfWords += block.DeviceBlock.Length;
                    }

                    if (actualDataLength != (numOfWords << byteOrderPerWord))
                    {
                        _deviceBlocks.Clear();
                        return OperationResultFactory.CreateFailedResult<int, int>(GetType().FullName, (int)Industry4Dot0Error.UnexpectedLengthWasReceived, SR.UnexpectedLengthWasReceived);
                    }

                    byte[] srcBuffer = message.Buffer;
                    int srcOffset = actualDataStartIndex;
                    foreach (var block in _deviceBlocks)
                    {
                        int count = block.DeviceBlock.Length << byteOrderPerWord;
                        Buffer.BlockCopy(srcBuffer, srcOffset, block.Buffer, 0, count);
                        srcOffset += count;
                    }
                    _deviceBlocks.Clear();
                    return OperationResultFactory.CreateSuccessfulResult(actualDataStartIndex, actualDataLength, SR.Success);
                case (ushort)PLCOperation.ReadModel:
                    return (actualDataLength == 18) ? OperationResultFactory.CreateSuccessfulResult(actualDataStartIndex, actualDataLength, SR.Success) :
                        OperationResultFactory.CreateFailedResult<int, int>(GetType().FullName, (int)Industry4Dot0Error.UnexpectedLengthWasReceived, SR.UnexpectedLengthWasReceived);
                default:
                    return OperationResultFactory.CreateSuccessfulResult(actualDataStartIndex, actualDataLength, SR.Success);
            }
        }
    }
}
