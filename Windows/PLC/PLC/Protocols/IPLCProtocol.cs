﻿using Industry4._0.IoT;
using SwissKnife;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Protocols
{
    public enum PLCOperation : ushort
    {
        DeviceWordRead = 0,
        DeviceWordWrite,
        DeviceBitRead,
        DeviceBitWrite,
        MultipleBlocksRead,
        MultipleBlocksWrite,
        RemoteRun,
        RemoteStop,
        RemoteReset,
        ReadModel,
        Other = 1024
    }

    public interface IPLCProtocol : IThingMessage
    {
        void BuildDeviceWordReadCommand(ArrayBuilder<byte> buffer, IPLC plc, PLCDeviceBlock block);

        void BuildDeviceWordWriteCommand(ArrayBuilder<byte> buffer, IPLC plc, PLCDeviceBlock block, ArraySegment<byte> data);

        void BuildDeviceBitReadCommand(ArrayBuilder<byte> buffer, IPLC plc, PLCDeviceBlock block);

        void BuildDeviceBitWriteCommand(ArrayBuilder<byte> buffer, IPLC plc, PLCDeviceBlock block, ArraySegment<bool> data);

        OperationResult BuildMultipleBlocksReadCommand(ArrayBuilder<byte> buffer, IPLC plc, IEnumerable<PLCDeviceTransportBlock> blocks);

        OperationResult BuildMultipleBlocksWriteCommand(ArrayBuilder<byte> buffer, IPLC plc, IEnumerable<PLCDeviceTransportBlock> blocks);

        OperationResult<int, int> ParseMessageData(ArrayBuilder<byte> message, IPLC plc, ushort operation);
    }
}
