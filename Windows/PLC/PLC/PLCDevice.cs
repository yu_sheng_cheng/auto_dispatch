﻿using SwissKnife;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC
{
    public class PLCDevice
    {
        public string Symbol { get; }

        public CarryBase Notation { get; }

        public bool IsBitUnit { get; }

        protected PLCDevice(string symbol, CarryBase notation, bool bitType)
        {
            Symbol = symbol;
            Notation = notation;
            IsBitUnit = bitType;
        }
    }
}
