﻿using Industry4._0;
using Industry4._0.Common;
using Industry4._0.IoT;
using Industry4._0.Transports;
using PLC.Protocols;
using SwissKnife;
using SwissKnife.Conversion;
using SwissKnife.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC
{
    public abstract class PLCAbstract<T1, T2> : ThingAbstract<T1>, IPLC where T1 : IByteConverter, new() where T2 : IPLCProtocol, new()
    {
        private readonly SendReceiveBuffer _sendReceiveBuffer = new SendReceiveBuffer();
        private PLCDeviceBlock _deviceBlock;
        
        protected IPLCProtocol PLCProtocol { get; } = new T2();

        public PLCSeries Series { get; private set; }

        public PLCType Type { get; private set; }

        private PLCDeviceBlock DeviceBlock
        {
            get
            {
                if (_deviceBlock == null)
                    _deviceBlock = CreateDeviceBlock();
                return _deviceBlock;
            }
        }

        public PLCAbstract(string makeName, string seriesName, string typeName)
        {
            PLCMake make = PLCEnvironment.QueryPLCMakeInfo(makeName);

            PLCSeries series = make.GetSeries(seriesName);
            if (series == null)
                throw new NotFoundException($"{make.DisplayName} can't search for the {seriesName} series.");

            PLCType type = series.GetType(typeName);
            Type = type ?? throw new NotFoundException($"{seriesName} seriesName can't search for the {typeName} type.");
            Series = series;
        }

        public OperationResult<PLCDeviceTransportBlock> CreateDeviceTransportBlock(string address, ushort length)
        {
            if (address == null)
                throw new ArgumentNullException("address");
            if (length == 0)
                throw new ArgumentException("The length argument must be greater than zero.", "length");

            PLCDeviceBlock block = CreateDeviceBlock();
            if (!block.TryParse(address, length))
                return OperationResultFactory.CreateFailedResult<PLCDeviceTransportBlock>(GetType().FullName, (int)Industry4Dot0Error.NotSupportedAddress, SR.NotSupportedAddress);

            try
            {
                PLCDeviceTransportBlock transportBlock = new PLCDeviceTransportBlock(this, block);
                return OperationResultFactory.CreateSuccessfulResult(transportBlock, SR.Success);
            }
            catch
            {
                int bitsPerWord = BytesPerWord * 8;
                string message = string.Format(SR.LengthMustBeAMultipleOfN, bitsPerWord.ToString());

                return OperationResultFactory.CreateFailedResult<PLCDeviceTransportBlock>(GetType().FullName, (int)Industry4Dot0Error.InvalidArgument, message);
            }
        }

        public Task<OperationResult> ReadMultipleBlocksAsync(IEnumerable<PLCDeviceTransportBlock> blocks)
        {
            return AccessMultipleBlocksAsync(blocks, PLCOperation.MultipleBlocksRead);
        }

        public Task<OperationResult> WriteMultipleBlocksAsync(IEnumerable<PLCDeviceTransportBlock> blocks)
        {
            return AccessMultipleBlocksAsync(blocks, PLCOperation.MultipleBlocksWrite);
        }

        private async Task<OperationResult> AccessMultipleBlocksAsync(IEnumerable<PLCDeviceTransportBlock> blocks, PLCOperation operation)
        {
            if (blocks == null)
                throw new ArgumentNullException("blocks");

            if (!Type.Spec.IsMultipleBlocksAccessSupported)
                return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.FunctionNotSupported, SR.FunctionNotSupported);

            _sendReceiveBuffer.Clear();

            OperationResult result;

            if (operation == PLCOperation.MultipleBlocksRead)
                result = PLCProtocol.BuildMultipleBlocksReadCommand(_sendReceiveBuffer.Send, this, blocks);
            else
                result = PLCProtocol.BuildMultipleBlocksWriteCommand(_sendReceiveBuffer.Send, this, blocks);

            if (!result.IsSuccess)
                return result;

            result = await Transport.SendThenReceiveAsync(_sendReceiveBuffer).ConfigureAwait(false);
            if (!result.IsSuccess)
                return result;

            return PLCProtocol.ParseMessageData(_sendReceiveBuffer.Receive, this, (ushort)operation);
        }

        protected override async Task<OperationResult<int, int>> ReadWords(string address, ushort length, SendReceiveBuffer sendReceiveBuffer)
        {
            PLCDeviceBlock block = DeviceBlock;
            if (!block.TryParse(address, length))
                return OperationResultFactory.CreateFailedResult<int, int>(GetType().FullName, (int)Industry4Dot0Error.NotSupportedAddress, SR.NotSupportedAddress);

            if (block.Device.IsBitUnit)
            {
                int bits = length << (ByteOrderPerWord + 3);
                if (bits > (long)uint.MaxValue - block.BaseAddress + 1)
                    return OperationResultFactory.CreateFailedResult<int, int>(GetType().FullName, (int)Industry4Dot0Error.IllegalAccessRange, SR.IllegalAccessRange);
            }

            int batchLimit = Type.Spec.BatchWordAccessLimit;
            if (batchLimit <= 0)
                batchLimit = int.MaxValue;

            if (length > batchLimit)
            {
                uint baseAddress = block.BaseAddress;

                while (length > 0)
                {
                    block.Length = (ushort)Math.Min(length, batchLimit);

                    _sendReceiveBuffer.Clear();
                    OperationResult<int, int> result = await ReadWords(block, _sendReceiveBuffer).ConfigureAwait(false);
                    if (!result.IsSuccess)
                        return result;

                    int wordsRead = result.Value2 >> ByteOrderPerWord;

                    if (wordsRead == 0)
                        break;

                    if (wordsRead > block.Length)
                        return OperationResultFactory.CreateFailedResult<int, int>(GetType().FullName, (int)Industry4Dot0Error.UnexpectedLengthWasReceived, SR.UnexpectedLengthWasReceived);

                    sendReceiveBuffer.Receive.Append(_sendReceiveBuffer.Receive.Buffer, result.Value1, wordsRead << ByteOrderPerWord);

                    if (wordsRead < block.Length)
                        break;

                    if (block.Device.IsBitUnit)
                        baseAddress = unchecked(baseAddress + (uint)(wordsRead << (ByteOrderPerWord + 3)));
                    else
                        baseAddress = unchecked(baseAddress + (uint)wordsRead);

                    block.BaseAddress = baseAddress;

                    length -= (ushort)wordsRead;
                }

                return OperationResultFactory.CreateSuccessfulResult(0, sendReceiveBuffer.Receive.Length);
            }
            else
                return await ReadWords(block, sendReceiveBuffer);
        }

        private async Task<OperationResult<int, int>> ReadWords(PLCDeviceBlock block, SendReceiveBuffer sendReceiveBuffer)
        {
            PLCProtocol.BuildDeviceWordReadCommand(sendReceiveBuffer.Send, this, block);
            OperationResult result = await Transport.SendThenReceiveAsync(sendReceiveBuffer).ConfigureAwait(false);
            if (!result.IsSuccess)
                return OperationResultFactory.CreateFailedResult<int, int>(result);
            return PLCProtocol.ParseMessageData(sendReceiveBuffer.Receive, this, (ushort)PLCOperation.DeviceWordRead);
        }

        protected override async Task<OperationResult> WriteWords(string address, ushort length, ArrayBuilder<byte> data, SendReceiveBuffer sendReceiveBuffer)
        {
            PLCDeviceBlock block = DeviceBlock;
            if (!block.TryParse(address, length))
                return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.NotSupportedAddress, SR.NotSupportedAddress);

            if (block.Device.IsBitUnit)
            {
                int bits = length << (ByteOrderPerWord + 3);
                if (bits > (long)uint.MaxValue - block.BaseAddress + 1)
                    return OperationResultFactory.CreateFailedResult<int, int>(GetType().FullName, (int)Industry4Dot0Error.IllegalAccessRange, SR.IllegalAccessRange);
            }

            int batchLimit = Type.Spec.BatchWordAccessLimit;
            if (batchLimit <= 0)
                batchLimit = int.MaxValue;

            uint baseAddress = block.BaseAddress;
            byte[] dataBuffer = data.Buffer;
            int dataStartIndex = 0;

            while (length > 0)
            {
                int wordsWritable = Math.Min(length, batchLimit);
                int bytesWritable = wordsWritable << ByteOrderPerWord;

                block.Length = (ushort)wordsWritable;

                sendReceiveBuffer.Clear();
                PLCProtocol.BuildDeviceWordWriteCommand(sendReceiveBuffer.Send, this, block, new ArraySegment<byte>(dataBuffer, dataStartIndex, bytesWritable));
                OperationResult communicationResult = await Transport.SendThenReceiveAsync(sendReceiveBuffer).ConfigureAwait(false);
                if (!communicationResult.IsSuccess)
                    return communicationResult;

                OperationResult<int, int> result = PLCProtocol.ParseMessageData(sendReceiveBuffer.Receive, this, (ushort)PLCOperation.DeviceWordWrite);
                if (!result.IsSuccess)
                    return result;

                if (block.Device.IsBitUnit)
                    baseAddress = unchecked(baseAddress + (uint)(bytesWritable << 3));
                else
                    baseAddress = unchecked(baseAddress + (uint)wordsWritable);

                block.BaseAddress = baseAddress;
                dataStartIndex += bytesWritable;
                length -= (ushort)wordsWritable;
            }

            return OperationResultFactory.CreateSuccessfulResult(SR.Success);
        }

        protected override async Task<OperationResult> ReadBitsAsync(string address, ushort length, SendReceiveBuffer sendReceiveBuffer, ArrayBuilder<bool> buffer)
        {
            PLCSpec spec = Type.Spec;

            if (!spec.IsBitOperationSupported)
                return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.FunctionNotSupported, SR.FunctionNotSupported);

            PLCDeviceBlock block = DeviceBlock;
            if (!block.TryParse(address, length) || !block.Device.IsBitUnit)
                return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.NotSupportedAddress, SR.NotSupportedAddress);

            int batchLimit = spec.BatchBitAccessLimit;
            if (batchLimit <= 0)
                batchLimit = int.MaxValue;

            while (length > 0)
            {
                block.Length = (ushort)Math.Min(length, batchLimit);

                sendReceiveBuffer.Clear();
                PLCProtocol.BuildDeviceBitReadCommand(sendReceiveBuffer.Send, this, block);
                OperationResult communicationResult = await Transport.SendThenReceiveAsync(sendReceiveBuffer).ConfigureAwait(false);
                if (!communicationResult.IsSuccess)
                    return communicationResult;

                OperationResult<int, int> result = PLCProtocol.ParseMessageData(sendReceiveBuffer.Receive, this, (ushort)PLCOperation.DeviceBitRead);
                if (!result.IsSuccess)
                    return result;

                int bitsRead = result.Value2;
                if (bitsRead == 0)
                    break;

                if (bitsRead > block.Length)
                    bitsRead = block.Length;

                byte[] receiveBuffer = sendReceiveBuffer.Receive.Buffer;
                int end = result.Value1 + bitsRead;
                int index = buffer.Length;

                for (int i = result.Value1; i < end; ++i)
                {
                    buffer[index++] = receiveBuffer[i] != 0;
                }

                if (bitsRead < block.Length)
                    break;

                block.BaseAddress = unchecked(block.BaseAddress + (uint)bitsRead);
                length -= (ushort)bitsRead;
            }

            return OperationResultFactory.CreateSuccessfulResult(SR.Success);
        }

        protected override async Task<OperationResult> WriteBitsAsync(string address, ArrayBuilder<bool> values, SendReceiveBuffer sendReceiveBuffer)
        {
            PLCSpec spec = Type.Spec;

            if (!spec.IsBitOperationSupported)
                return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.FunctionNotSupported, SR.FunctionNotSupported);

            ushort length = (ushort)values.Length;

            PLCDeviceBlock block = DeviceBlock;
            if (!block.TryParse(address, length) || !block.Device.IsBitUnit)
                return OperationResultFactory.CreateFailedResult(GetType().FullName, (int)Industry4Dot0Error.NotSupportedAddress, SR.NotSupportedAddress);

            int batchLimit = spec.BatchBitAccessLimit;
            if (batchLimit <= 0)
                batchLimit = int.MaxValue;

            bool[] dataBuffer = values.Buffer;
            int dataStartIndex = 0;

            while (length > 0)
            {
                int lengthWritable = Math.Min(length, batchLimit);

                block.Length = (ushort)lengthWritable;

                sendReceiveBuffer.Clear();
                PLCProtocol.BuildDeviceBitWriteCommand(sendReceiveBuffer.Send, this, block, new ArraySegment<bool>(dataBuffer, dataStartIndex, lengthWritable));
                OperationResult communicationResult = await Transport.SendThenReceiveAsync(sendReceiveBuffer).ConfigureAwait(false);
                if (!communicationResult.IsSuccess)
                    return communicationResult;

                OperationResult<int, int> result = PLCProtocol.ParseMessageData(sendReceiveBuffer.Receive, this, (ushort)PLCOperation.DeviceBitWrite);
                if (!result.IsSuccess)
                    return result;

                block.BaseAddress = unchecked(block.BaseAddress + (uint)lengthWritable);
                dataStartIndex += lengthWritable;
                length -= (ushort)lengthWritable;
            }

            return OperationResultFactory.CreateSuccessfulResult(SR.Success);
        }

        protected abstract PLCDeviceBlock CreateDeviceBlock();
    }
}
