﻿using SwissKnife;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC
{
    public sealed class PLCDeviceTransportBlock
    {
        private IPLC _plc;

        private static readonly Func<byte[], int, sbyte> ToInt8 = (value, startIndex) => { return unchecked((sbyte)value[startIndex]); };
        private static readonly Func<byte[], int, byte> ToUInt8 = (value, startIndex) => { return value[startIndex]; };
        private readonly Func<byte[], int, short> ToInt16;
        private readonly Func<byte[], int, ushort> ToUInt16;
        private readonly Func<byte[], int, int> ToInt32;
        private readonly Func<byte[], int, uint> ToUInt32;
        private readonly Func<byte[], int, long> ToInt64;
        private readonly Func<byte[], int, ulong> ToUInt64;
        private readonly Func<byte[], int, float> ToSingle;
        private readonly Func<byte[], int, double> ToDouble;

        private static readonly Action<sbyte, byte[], int> GetInt8Bytes = (value, buffer, offset) => { buffer[offset] = unchecked((byte)value); };
        private static readonly Action<byte, byte[], int> GetUInt8Bytes = (value, buffer, offset) => { buffer[offset] = value; };
        private readonly Action<short, byte[], int> GetInt16Bytes;
        private readonly Action<ushort, byte[], int> GetUInt16Bytes;
        private readonly Action<int, byte[], int> GetInt32Bytes;
        private readonly Action<uint, byte[], int> GetUInt32Bytes;
        private readonly Action<long, byte[], int> GetInt64Bytes;
        private readonly Action<ulong, byte[], int> GetUInt64Bytes;
        private readonly Action<float, byte[], int> GetSingleBytes;
        private readonly Action<double, byte[], int> GetDoubleBytes;

        public PLCDeviceBlock DeviceBlock { get; }

        public byte[] Buffer { get; }

        public PLCDeviceTransportBlock(IPLC plc, PLCDeviceBlock block)
        {
            _plc = plc ?? throw new ArgumentNullException("plc");

            if (block == null)
                throw new ArgumentNullException("block");
            if (block.Device == null)
                throw new ArgumentException("Device property of block argument cannot be null.", "block");
            if (block.Length == 0)
                throw new ArgumentException("Length property of block argument must be greater than zero.", "block");

            if (block.Device.IsBitUnit)
            {
                if (!IsAlignedOnWordBoundary(block.Length))
                    throw new ArgumentException("Length property of block argument must be aligned on word boundary.", "block");
                int words = block.Length >> (plc.ByteOrderPerWord + 3);
                block.Length = (ushort)words;
            }
            DeviceBlock = block;

            Buffer = new byte[block.Length << plc.ByteOrderPerWord];

            ToInt16 = _plc.ByteConverter.ToInt16;
            ToUInt16 = _plc.ByteConverter.ToUInt16;
            ToInt32 = _plc.ByteConverter.ToInt32;
            ToUInt32 = _plc.ByteConverter.ToUInt32;
            ToInt64 = _plc.ByteConverter.ToInt64;
            ToUInt64 = _plc.ByteConverter.ToUInt64;
            ToSingle = _plc.ByteConverter.ToSingle;
            ToDouble = _plc.ByteConverter.ToDouble;

            GetInt16Bytes = _plc.ByteConverter.GetBytes;
            GetUInt16Bytes = _plc.ByteConverter.GetBytes;
            GetInt32Bytes = _plc.ByteConverter.GetBytes;
            GetUInt32Bytes = _plc.ByteConverter.GetBytes;
            GetInt64Bytes = _plc.ByteConverter.GetBytes;
            GetUInt64Bytes = _plc.ByteConverter.GetBytes;
            GetSingleBytes = _plc.ByteConverter.GetBytes;
            GetDoubleBytes = _plc.ByteConverter.GetBytes;
        }

        public sbyte ReadInt8(uint address)
        {
            if (_plc.BytesPerWord != 1)
                throw new NotSupportedException();
            return Read(address, ToInt8, 1);
        }

        public byte ReadUInt8(uint address)
        {
            if (_plc.BytesPerWord != 1)
                throw new NotSupportedException();
            return Read(address, ToUInt8, 1);
        }

        public short ReadInt16(uint address)
        {
            return Read(address, ToInt16, 2);
        }

        public ushort ReadUInt16(uint address)
        {
            return Read(address, ToUInt16, 2);
        }

        public int ReadInt32(uint address)
        {
            return Read(address, ToInt32, 4);
        }

        public uint ReadUInt32(uint address)
        {
            return Read(address, ToUInt32, 4);
        }

        public long ReadInt64(uint address)
        {
            return Read(address, ToInt64, 8);
        }

        public ulong ReadUInt64(uint address)
        {
            return Read(address, ToUInt64, 8);
        }

        public float ReadFloat(uint address)
        {
            return Read(address, ToSingle, 4);
        }

        public double ReadDouble(uint address)
        {
            return Read(address, ToDouble, 8);
        }

        private T Read<T>(uint address, Func<byte[], int, T> convert, int bytesToConvert)
        {
            ValidateReadWriteRangeInput(address, bytesToConvert);
            return convert.Invoke(Buffer, GetStartingIndex(address));
        }

        public void Write(uint address, sbyte value)
        {
            if (_plc.BytesPerWord != 1)
                throw new NotSupportedException();
            Write(address, value, GetInt8Bytes, 1);
        }

        public void Write(uint address, byte value)
        {
            if (_plc.BytesPerWord != 1)
                throw new NotSupportedException();
            Write(address, value, GetUInt8Bytes, 1);
        }

        public void Write(uint address, short value)
        {
            Write(address, value, GetInt16Bytes, 2);
        }

        public void Write(uint address, ushort value)
        {
            Write(address, value, GetUInt16Bytes, 2);
        }

        public void Write(uint address, int value)
        {
            Write(address, value, GetInt32Bytes, 4);
        }

        public void Write(uint address, uint value)
        {
            Write(address, value, GetUInt32Bytes, 4);
        }

        public void Write(uint address, long value)
        {
            Write(address, value, GetInt64Bytes, 8);
        }

        public void Write(uint address, ulong value)
        {
            Write(address, value, GetUInt64Bytes, 8);
        }

        public void Write(uint address, float value)
        {
            Write(address, value, GetSingleBytes, 4);
        }

        public void Write(uint address, double value)
        {
            Write(address, value, GetDoubleBytes, 8);
        }

        private void Write<T>(uint address, T value, Action<T, byte[], int> convert, int bytesRequired)
        {
            ValidateReadWriteRangeInput(address, bytesRequired);
            convert.Invoke(value, Buffer, GetStartingIndex(address));
        }

        private void ValidateReadWriteRangeInput(uint address, int bytes)
        {
            if (address < DeviceBlock.BaseAddress)
                throw new ArgumentOutOfRangeException("address");

            address -= DeviceBlock.BaseAddress;

            if (DeviceBlock.Device.IsBitUnit)
            {
                if (!IsAlignedOnWordBoundary(address))
                    throw new ArgumentException("The starting address must be aligned on word boundary.", "address");
                address >>= _plc.ByteOrderPerWord + 3;
            }

            int length = DeviceBlock.Length;
            int words = bytes >> _plc.ByteOrderPerWord;

            if (words > length - address)
            {
                if (address >= length)
                    throw new ArgumentOutOfRangeException("address");
                else
                    throw new ArgumentException("The sum of the address and sizeof(type) arguments must be less than or equal to the collection's Count.");
            }    
        }

        private bool IsAlignedOnWordBoundary(uint value)
        {
            return (value & (_plc.BytesPerWord * 8 - 1)) == 0;
        }

        private int GetStartingIndex(uint address)
        {
            address -= DeviceBlock.BaseAddress;
            if (DeviceBlock.Device.IsBitUnit)
                address >>= _plc.ByteOrderPerWord + 3;
            return unchecked((int)(address << _plc.ByteOrderPerWord));
        }

        public void ClearBuffer()
        {
            Array.Clear(Buffer, 0, Buffer.Length);
        }
    }
}
