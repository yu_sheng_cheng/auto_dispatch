﻿using Industry4._0.Common;
using PLC.Protocols;
using PLC.Protocols.Mitsubishi;
using SwissKnife;
using SwissKnife.Conversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC.Mitsubishi
{
    public abstract class MelsecMCNet<T> : PLCNetAbstract<LittleEndianConverter, T>, IMelsecMCPLC where T : IPLCProtocol, new()
    {
        public byte NetworkNumber { get; set; } = 0x00;

        public byte StationNumber { get; set; } = 0x00;

        public MelsecMCNet(string seriesName, string typeName) : this("Mitsubishi", seriesName, typeName) { }

        protected MelsecMCNet(string makeName, string seriesName, string typeName) : base(makeName, seriesName, typeName)
        {

        }

        protected override PLCDeviceBlock CreateDeviceBlock()
        {
            return new PLCDeviceBlock(MelsecMCDevice.TryParse);
        }

        public async Task<OperationResult> RemoteRun()
        {
            return await RemoteOperation(PLCOperation.RemoteRun, null);
        }

        public async Task<OperationResult> RemoteStop()
        {
            return await RemoteOperation(PLCOperation.RemoteStop, null);
        }

        public async Task<OperationResult> RemoteReset()
        {
            return await RemoteOperation(PLCOperation.RemoteReset, null);
        }

        public async Task<OperationResult<string, uint>> ReadModel()
        {
            SendReceiveBuffer sendReceiveBuffer = new SendReceiveBuffer();

            OperationResult<int, int> result = await RemoteOperation(PLCOperation.ReadModel, sendReceiveBuffer);
            if (!result.IsSuccess)
                return OperationResultFactory.CreateFailedResult<string, uint>(result);

            byte[] message = sendReceiveBuffer.Receive.Buffer;
            string modelName = Encoding.ASCII.GetString(message, result.Value1, 16).TrimEnd();
            uint modelCode = ParseModelCode(message, result.Value1 + 16);
            return OperationResultFactory.CreateSuccessfulResult(modelName, modelCode);
        }

        private async Task<OperationResult<int, int>> RemoteOperation(PLCOperation operation, SendReceiveBuffer sendReceiveBuffer)
        {
            if (sendReceiveBuffer == null)
                sendReceiveBuffer = new SendReceiveBuffer();

            PackMCCommand(sendReceiveBuffer.Send, (ushort)operation);

            OperationResult communicationResult = await Transport.SendThenReceiveAsync(sendReceiveBuffer).ConfigureAwait(false);
            if (!communicationResult.IsSuccess)
                return OperationResultFactory.CreateFailedResult<int, int>(communicationResult);

            return PLCProtocol.ParseMessageData(sendReceiveBuffer.Receive, this, (ushort)operation);
        }

        protected abstract void PackMCCommand(ArrayBuilder<byte> buffer, ushort operation);

        protected abstract uint ParseModelCode(byte[] message, int startIndex);
    }

    public class MelsecMCNet : MelsecMCNet<MelsecMCBinaryProtocol>
    {
        public MelsecMCNet(string seriesName, string typeName) : this("Mitsubishi", seriesName, typeName) { }

        protected MelsecMCNet(string makeName, string seriesName, string typeName) : base(makeName, seriesName, typeName)
        {
            LogKeyword = "MelsecMCNet";
        }

        protected override void PackMCCommand(ArrayBuilder<byte> buffer, ushort operation)
        {
            MelsecMCBinaryProtocol mcProtocol = PLCProtocol as MelsecMCBinaryProtocol;

            switch (operation)
            {
                case (ushort)PLCOperation.RemoteRun:
                    mcProtocol.PackMCCommand(buffer, new byte[] { 0x01, 0x10, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00 }, NetworkNumber, StationNumber);
                    break;
                case (ushort)PLCOperation.RemoteStop:
                    mcProtocol.PackMCCommand(buffer, new byte[] { 0x02, 0x10, 0x00, 0x00, 0x01, 0x00 }, NetworkNumber, StationNumber);
                    break;
                case (ushort)PLCOperation.RemoteReset:
                    mcProtocol.PackMCCommand(buffer, new byte[] { 0x06, 0x10, 0x00, 0x00, 0x01, 0x00 }, NetworkNumber, StationNumber);
                    break;
                case (ushort)PLCOperation.ReadModel:
                    mcProtocol.PackMCCommand(buffer, new byte[] { 0x01, 0x01, 0x00, 0x00 }, NetworkNumber, StationNumber);
                    break;
            }
        }

        protected override uint ParseModelCode(byte[] message, int startIndex)
        {
            return ByteConverter.ToUInt16(message, startIndex);
        }
    }
}
