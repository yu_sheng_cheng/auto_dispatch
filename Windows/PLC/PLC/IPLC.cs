﻿using SwissKnife.Conversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLC
{
    public interface IPLC
    {
        PLCSeries Series { get; }

        PLCType Type { get; }

        byte BytesPerWord { get; }

        byte ByteOrderPerWord { get; }

        IByteConverter ByteConverter { get; }
    }
}
