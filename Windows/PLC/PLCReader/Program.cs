﻿using PLC;
using PLC.Mitsubishi;
using SwissKnife;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PLCReader
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                PLCEnvironment.MakeInfoRootFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Makes");

                using (MelsecMCNet melsec = new MelsecMCNet("Q", "Q03UDE"))
                {
                    melsec.InternetTransport.RemoteEndPoint = new IPEndPoint(IPAddress.Parse("192.168.3.100"), 1025);

                    ArrayBuilder<int> intArray = new ArrayBuilder<int>();

                    int dwords = 2048;

                    var readResult = await melsec.ReadInt32Async("D0", dwords, intArray);
                    if (!readResult.IsSuccess)
                    {
                        Console.WriteLine("{0}", readResult);
                        return;
                    }

                    int n = dwords / 2;

                    string[] lines = new string[n];

                    for (int i = 0; i < dwords; i += 2)
                    {
                        lines[i>>1]=
                    }

                        
                    //File.WriteAllLines()
                }
            });

            Console.ReadLine();
        }
    }
}
