﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife
{
    public static class BitOperation
    {
        public static bool GetBit(int data, byte bitOrdinal)
        {
            return ((data >> bitOrdinal) & 0x1) == 1;
        }

        public static bool GetBit(uint data, byte bitOrdinal)
        {
            return ((data >> bitOrdinal) & 0x1) == 1;
        }

        public static bool GetBit(long data, byte bitOrdinal)
        {
            return ((data >> bitOrdinal) & 0x1) == 1;
        }

        public static bool GetBit(ulong data, byte bitOrdinal)
        {
            return ((data >> bitOrdinal) & 0x1) == 1;
        }

        public static int SetBit(int data, byte bitOrdinal, bool value)
        {
            if (value)
                return data | (0x1 << bitOrdinal);
            else
                return data & ~(0x1 << bitOrdinal);
        }

        public static void SetBit(ref int data, byte bitOrdinal, bool value)
        {
            if (value)
                data |= 0x1 << bitOrdinal;
            else
                data &= ~(0x1 << bitOrdinal);
        }

        public static uint SetBit(uint data, byte bitOrdinal, bool value)
        {
            if (value)
                return data | ((uint)0x1 << bitOrdinal);
            else
                return data & ~((uint)0x1 << bitOrdinal);
        }

        public static void SetBit(ref uint data, byte bitOrdinal, bool value)
        {
            if (value)
                data |= (uint)0x1 << bitOrdinal;
            else
                data &= ~((uint)0x1 << bitOrdinal);
        }

        public static long SetBit(long data, byte bitOrdinal, bool value)
        {
            if (value)
                return data | ((long)0x1 << bitOrdinal);
            else
                return data & ~((long)0x1 << bitOrdinal);
        }

        public static void SetBit(ref long data, byte bitOrdinal, bool value)
        {
            if (value)
                data |= (long)0x1 << bitOrdinal;
            else
                data &= ~((long)0x1 << bitOrdinal);
        }

        public static ulong SetBit(ulong data, byte bitOrdinal, bool value)
        {
            if (value)
                return data | ((ulong)0x1 << bitOrdinal);
            else
                return data & ~((ulong)0x1 << bitOrdinal);
        }

        public static void SetBit(ref ulong data, byte bitOrdinal, bool value)
        {
            if (value)
                data |= (ulong)0x1 << bitOrdinal;
            else
                data &= ~((ulong)0x1 << bitOrdinal);
        }
    }
}
