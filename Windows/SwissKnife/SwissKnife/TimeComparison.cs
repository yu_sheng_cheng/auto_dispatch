﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife
{
    public static class TimeComparison
    {
        public static bool IsBefore(uint now, uint target)
        {
            return unchecked((int)(now - target)) < 0;
        }

        public static bool IsBeforeEqual(uint now, uint target)
        {
            return unchecked((int)(target - now)) >= 0;
        }

        public static bool IsAfter(uint now, uint target)
        {
            return unchecked((int)(target - now)) < 0;
        }

        public static bool IsAfterEqual(uint now, uint target)
        {
            return unchecked((int)(now - target)) >= 0;
        }
    }
}
