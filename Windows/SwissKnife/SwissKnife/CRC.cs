﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife
{
    public static class CRC
    {
        public static ushort CRC16(byte[] data, ushort polynomial)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            return CRC16(data, 0, data.Length, 0xFFFF, polynomial);
        }

        public static ushort CRC16(byte[] data, int offset, int count, ushort polynomial)
        {
            return CRC16(data, offset, count, 0xFFFF, polynomial);
        }

        public static ushort CRC16(byte[] data, ushort initialValue, ushort polynomial)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            return CRC16(data, 0, data.Length, initialValue, polynomial);
        }

        public static ushort CRC16(byte[] data, int offset, int count, ushort initialValue, ushort polynomial)
        {
            if (data == null)
                throw new ArgumentNullException("data");
            if (count < 0)
                throw new ArgumentOutOfRangeException("count");
            if (offset < 0)
                throw new ArgumentOutOfRangeException("offset");
            if (count > data.Length - offset)
                throw new ArgumentException("The sum of the offset and count arguments must be less than or equal to the collection's Count.");

            ushort crc = initialValue;

            while (count-- > 0)
            {
                crc ^= data[offset++];

                for (int i = 7; i >= 0; --i)
                {
                    int lsb = crc & 0x0001;
                    crc >>= 1;
                    if (lsb == 0x01)
                        crc ^= polynomial;
                }
            }
            return crc;
        }
    }
}
