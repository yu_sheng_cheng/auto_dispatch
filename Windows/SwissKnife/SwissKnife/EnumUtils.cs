﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife
{
    public static class EnumUtils
    {
        public static bool TryParse<T>(string value, out T result) where T : struct
        {
            return Enum.TryParse(value, true, out result) ? Enum.IsDefined(typeof(T), result) : false;
        }
    }
}
