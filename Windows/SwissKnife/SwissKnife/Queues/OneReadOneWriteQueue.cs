﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SwissKnife.Queues
{
    public class OneReadOneWriteQueue<T> where T : class
    {
        private Segment _head;
        private Segment _tail;

        public OneReadOneWriteQueue()
        {
            _head = new Segment(this);
            _tail = _head;
        }

        public void Enqueue(T instance)
        {
            _tail.Append(instance);
        }

        public T Dequeue()
        {
            return _head.Remove();
        }

        private class Segment
        {
            private struct Element
            {
                public T Instance;
            }

            private const int SEGMENT_SIZE = 32;

            private readonly OneReadOneWriteQueue<T> _queue;
            private readonly Element[] _array = new Element[SEGMENT_SIZE];
            private int _low;
            private int _high = -1;
            private volatile Segment _next;

            public Segment(OneReadOneWriteQueue<T> queue)
            {
                _queue = queue;
            }

            public void Append(T instance)
            {
                ++_high;

                Thread.MemoryBarrier();

                _array[_high].Instance = instance;

                if (_high == SEGMENT_SIZE - 1)
                    Grow();
            }

            private void Grow()
            {
                Segment newSegment = new Segment(_queue);
                _next = newSegment;
                _queue._tail = newSegment;
            }

            public T Remove()
            {
                if (_low > _high)
                    return null;

                int low = _low++;

                SpinWait spinwait = new SpinWait();
                T instance;

                while ((instance = Volatile.Read(ref _array[low].Instance)) == null)
                {
                    spinwait.SpinOnce();
                }

                _array[low].Instance = null;

                if (_low == SEGMENT_SIZE)
                {
                    spinwait.Reset();

                    while (_next == null)
                    {
                        spinwait.SpinOnce();
                    }

                    _queue._head = _next;
                }

                return instance;
            }
        }
    }
}
