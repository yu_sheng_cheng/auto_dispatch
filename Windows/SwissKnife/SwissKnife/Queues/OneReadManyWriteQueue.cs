﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SwissKnife.Queues
{
    public class OneReadManyWriteQueue<T> where T : class
    {
        private Segment _head;
        private volatile Segment _tail;

        public bool IsEmpty
        {
            get
            {
                Segment head = Volatile.Read(ref _head);
                if (!head.IsEmpty)
                    //fast route 1:
                    //if current head is not empty, then queue is not empty
                    return false;
                else if (head.Next == null)
                    //fast route 2:
                    //if current head is empty and it's the last segment
                    //then queue is empty
                    return true;
                else
                //slow route:
                //current head is empty and it is NOT the last segment,
                //it means another thread is growing new segment 
                {
                    SpinWait spinwait = new SpinWait();
                    while (head.IsEmpty)
                    {
                        if (head.Next == null)
                            return true;

                        spinwait.SpinOnce();
                        head = Volatile.Read(ref _head);
                    }
                    return false;
                }
            }
        }

        public OneReadManyWriteQueue()
        {
            _head = new Segment(this);
            _tail = _head;
        }

        public void Enqueue(T instance)
        {
            SpinWait spinwait = new SpinWait();
            while (true)
            {
                Segment tail = _tail;
                if (tail.TryAppend(instance))
                    return;
                spinwait.SpinOnce();
            }
        }

        public T Dequeue()
        {
            return _head.Remove();
        }

        private class Segment
        {
            private struct Element
            {
                public T Instance;
            }

            private const int SEGMENT_SIZE = 32;

            private readonly OneReadManyWriteQueue<T> _queue;
            private readonly Element[] _array = new Element[SEGMENT_SIZE];
            private int _low;
            private int _high = -1;
            private volatile Segment _next;

            public bool IsEmpty { get { return _low > _high; } }

            public Segment Next { get { return _next; } }

            public Segment(OneReadManyWriteQueue<T> queue)
            {
                _queue = queue;
            }

            public bool TryAppend(T instance)
            {
                if (_high >= SEGMENT_SIZE - 1)
                    return false;

                int newhigh = Interlocked.Increment(ref _high);

                if (newhigh >= SEGMENT_SIZE)
                    return false;

                _array[newhigh].Instance = instance;

                if (newhigh == SEGMENT_SIZE - 1)
                    Grow();
                return true;
            }

            private void Grow()
            {
                Segment newSegment = new Segment(_queue);
                _next = newSegment;
                _queue._tail = newSegment;
            }

            public T Remove()
            {
                int high = Math.Min(_high, SEGMENT_SIZE - 1);

                if (_low > high)
                    return null;

                int low = _low++;

                SpinWait spinwait = new SpinWait();
                T instance;

                while ((instance = Volatile.Read(ref _array[low].Instance)) == null)
                {
                    spinwait.SpinOnce();
                }

                _array[low].Instance = null;

                if (_low == SEGMENT_SIZE)
                {
                    spinwait.Reset();

                    while (_next == null)
                    {
                        spinwait.SpinOnce();
                    }

                    _queue._head = _next;
                }

                return instance;
            }
        }
    }
}
