﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife
{
    public class IniParser
    {
        private readonly Dictionary<string, string> _dictionary;

        public string this[string section, string key] { get { return GetValue(section, key); } }

        public IniParser()
        {
            _dictionary = new Dictionary<string, string>();
        }

        public IniParser(string file, bool ignoreCase) : this(file, ";", ignoreCase) { }

        public IniParser(string file, string commentDelimiter = ";", bool ignoreCase = false)
        {
            _dictionary = new Dictionary<string, string>(ignoreCase ? StringComparer.OrdinalIgnoreCase : StringComparer.Ordinal);

            using (StreamReader sr = new StreamReader(file))
            {
                string line, section = string.Empty;

                while ((line = sr.ReadLine()) != null)
                {
                    line = line.Trim();

                    if (line.Length == 0)
                        continue;

                    if (!string.IsNullOrEmpty(commentDelimiter) && line.StartsWith(commentDelimiter))
                        continue;

                    if (line.StartsWith("[") && line.Contains("]"))
                    {
                        int index = line.IndexOf(']');
                        section = line.Substring(1, index - 1).Trim();
                        continue;
                    }

                    if (line.Contains("="))
                    {
                        int index = line.IndexOf('=');
                        string key = line.Substring(0, index).Trim();
                        string val = line.Substring(index + 1).Trim();

                        if (string.IsNullOrEmpty(key))
                            continue;

                        key = $"[{section}]{key}";

                        if (val.StartsWith("\"") && val.EndsWith("\""))
                            val = val.Substring(1, val.Length - 2);

                        if (_dictionary.ContainsKey(key))
                            _dictionary[key] = val;
                        else
                            _dictionary.Add(key, val);
                    }
                }
            }
        }

        public string GetValue(string section, string key, string defaultValue = "")
        {
            return TryGetValue(section, key, out string value) ? value : defaultValue;
        }

        private bool TryGetValue(string section, string key, out string value)
        {
            if (key == null)
                throw new ArgumentNullException("key");

            return _dictionary.TryGetValue($"[{section ?? string.Empty}]{key}", out value);
        }
    }
}
