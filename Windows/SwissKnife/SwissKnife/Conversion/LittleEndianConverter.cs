﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife.Conversion
{
    public class LittleEndianConverter : EndianConverter
    {
        protected override void GetBytes(long value, int bytesRequired, byte[] buffer, int offset)
        {
            for (int i = 0; i < bytesRequired; ++i)
            {
                buffer[offset++] = unchecked((byte)(value & 0xff));
                value >>= 8;
            }
        }

        protected override long FromBytes(byte[] value, int startIndex, int bytesToConvert)
        {
            long result = 0;

            for (int i = startIndex + bytesToConvert - 1; i >= startIndex; --i)
            {
                result = (result << 8) | value[i];
            }
            return result;
        }
    }
}
