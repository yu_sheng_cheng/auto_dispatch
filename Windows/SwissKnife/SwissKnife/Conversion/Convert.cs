﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife.Conversion
{
    public static class Convert
    {
        public static byte BoolArrayToByte(bool[] boolArray)
        {
            if (boolArray == null)
                throw new ArgumentNullException("boolArray");

            int count = Math.Min(boolArray.Length, 8);
            int result = 0;

            for (int i = count - 1; i >= 0; --i)
            {
                if (boolArray[i])
                    result |= 1 << i;
            }
            return (byte)result;
        }

        public static void BoolArrayToByteArray(bool[] boolArray, byte[] byteArray)
        {
            if (boolArray == null)
                throw new ArgumentNullException("boolArray");

            BoolArrayToByteArray(boolArray, 0, boolArray.Length, byteArray, 0);
        }

        public static void BoolArrayToByteArray(bool[] boolArray, int sourceIndex, int count, byte[] byteArray, int destinationIndex)
        {
            if (boolArray == null)
                throw new ArgumentNullException("boolArray");
            if (count < 0)
                throw new ArgumentOutOfRangeException("count");
            if (sourceIndex < 0)
                throw new ArgumentOutOfRangeException("sourceIndex");
            if (count > boolArray.Length - sourceIndex)
                throw new ArgumentException("The sum of the sourceIndex and count arguments must be less than or equal to the collection's Count.");

            if (byteArray == null)
                throw new ArgumentNullException("byteArray");
            if (destinationIndex < 0)
                throw new ArgumentOutOfRangeException("destinationIndex");

            int bytesNeeded = count / 8;
            if ((count & 0x7) != 0)
                bytesNeeded += 1;

            if (bytesNeeded > byteArray.Length - destinationIndex)
                throw new ArgumentException("The sum of the destinationIndex argument and bytes needed must be less than or equal to the collection's Count.");

            while (count > 0)
            {
                int bits = Math.Min(count, 8);
                int result = 0;

                for (int i = 0; i < bits; ++i)
                {
                    if (boolArray[sourceIndex++])
                        result |= 1 << i;
                }

                byteArray[destinationIndex++] = (byte)result;
                count -= 8;
            }
        }

        public static void ByteToBoolArray(byte value, bool[] boolArray)
        {
            ByteToBoolArray(value, boolArray, 0);
        }

        public static void ByteToBoolArray(byte value, bool[] boolArray, int index)
        {
            if (boolArray == null)
                throw new ArgumentNullException("boolArray");

            int length = boolArray.Length;
            int bits = Math.Min(unchecked(length - index), 8);

            if (value == 0)
                Array.Clear(boolArray, index, bits);
            else
            {
                if (index >= length | index < 0)
                    throw new ArgumentOutOfRangeException("index");

                while (bits-- > 0)
                {
                    boolArray[index++] = (value & 0x1) == 0x1;
                    value >>= 1;
                }
            }
        }

        public static void ByteArrayToBoolArray(byte[] values, bool[] boolArray)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            ByteArrayToBoolArray(values, 0, values.Length, boolArray, 0);
        }

        public static void ByteArrayToBoolArray(byte[] values, int sourceIndex, int count, bool[] boolArray, int destinationIndex)
        {
            if (values == null)
                throw new ArgumentNullException("values");
            if (count < 0)
                throw new ArgumentOutOfRangeException("count");
            if (sourceIndex < 0)
                throw new ArgumentOutOfRangeException("sourceIndex");
            if (count > values.Length - sourceIndex)
                throw new ArgumentException("The sum of the sourceIndex and count arguments must be less than or equal to the collection's Count.");

            if (boolArray == null)
                throw new ArgumentNullException("boolArray");
            int maxBitsToConvert = boolArray.Length;
            if (destinationIndex >= maxBitsToConvert | destinationIndex < 0)
                throw new ArgumentOutOfRangeException("destinationIndex");

            int maxBytesToConvert = maxBitsToConvert / 8;
            if ((maxBitsToConvert & 0x7) != 0)
                maxBytesToConvert += 1;

            count = Math.Min(count, maxBytesToConvert);
            int bitsConverted = 0;

            while (count-- > 0)
            {
                int bits = maxBitsToConvert - bitsConverted;
                if (bits > 8)
                    bits = 8;

                byte value = values[sourceIndex++];

                for (int i = 0; i < bits; ++i)
                {
                    boolArray[destinationIndex++] = (value & 0x1) == 0x1;
                    value >>= 1;
                }

                bitsConverted += 8;
            }
        }
    }
}
