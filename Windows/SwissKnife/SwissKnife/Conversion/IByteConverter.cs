﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife.Conversion
{
    public interface IByteConverter
    {
        byte[] GetBytes(bool value);
        void GetBytes(bool value, byte[] buffer, int offset);
        void GetBytes(bool[] values, byte[] buffer, int offset);
        void GetBytes(bool[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex);

        byte[] GetBytes(char value);
        void GetBytes(char value, byte[] buffer, int offset);
        void GetBytes(char[] values, byte[] buffer, int offset);
        void GetBytes(char[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex);

        byte[] GetBytes(short value);
        void GetBytes(short value, byte[] buffer, int offset);
        void GetBytes(short[] values, byte[] buffer, int offset);
        void GetBytes(short[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex);

        byte[] GetBytes(ushort value);
        void GetBytes(ushort value, byte[] buffer, int offset);
        void GetBytes(ushort[] values, byte[] buffer, int offset);
        void GetBytes(ushort[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex);

        byte[] GetBytes(int value);
        void GetBytes(int value, byte[] buffer, int offset);
        void GetBytes(int[] values, byte[] buffer, int offset);
        void GetBytes(int[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex);

        byte[] GetBytes(uint value);
        void GetBytes(uint value, byte[] buffer, int offset);
        void GetBytes(uint[] values, byte[] buffer, int offset);
        void GetBytes(uint[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex);

        byte[] GetBytes(long value);
        void GetBytes(long value, byte[] buffer, int offset);
        void GetBytes(long[] values, byte[] buffer, int offset);
        void GetBytes(long[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex);

        byte[] GetBytes(ulong value);
        void GetBytes(ulong value, byte[] buffer, int offset);
        void GetBytes(ulong[] values, byte[] buffer, int offset);
        void GetBytes(ulong[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex);

        byte[] GetBytes(float value);
        void GetBytes(float value, byte[] buffer, int offset);
        void GetBytes(float[] values, byte[] buffer, int offset);
        void GetBytes(float[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex);

        byte[] GetBytes(double value);
        void GetBytes(double value, byte[] buffer, int offset);
        void GetBytes(double[] values, byte[] buffer, int offset);
        void GetBytes(double[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex);

        byte[] GetBytes(string value, Encoding encoding);
        string GetString(byte[] bytes, int startIndex, int count, Encoding encoding);

        bool ToBoolean(byte[] value, int startIndex);
        void ToBoolean(byte[] values, int sourceIndex, int count, bool[] buffer, int destinationIndex);

        char ToChar(byte[] value, int startIndex);
        void ToChar(byte[] values, int sourceIndex, int count, char[] buffer, int destinationIndex);

        short ToInt16(byte[] value, int startIndex);
        void ToInt16(byte[] values, int sourceIndex, int count, short[] buffer, int destinationIndex);

        ushort ToUInt16(byte[] value, int startIndex);
        void ToUInt16(byte[] values, int sourceIndex, int count, ushort[] buffer, int destinationIndex);

        int ToInt32(byte[] value, int startIndex);
        void ToInt32(byte[] values, int sourceIndex, int count, int[] buffer, int destinationIndex);

        uint ToUInt32(byte[] value, int startIndex);
        void ToUInt32(byte[] values, int sourceIndex, int count, uint[] buffer, int destinationIndex);

        long ToInt64(byte[] value, int startIndex);
        void ToInt64(byte[] values, int sourceIndex, int count, long[] buffer, int destinationIndex);

        ulong ToUInt64(byte[] value, int startIndex);
        void ToUInt64(byte[] values, int sourceIndex, int count, ulong[] buffer, int destinationIndex);

        float ToSingle(byte[] value, int startIndex);
        void ToSingle(byte[] values, int sourceIndex, int count, float[] buffer, int destinationIndex);

        double ToDouble(byte[] value, int startIndex);
        void ToDouble(byte[] values, int sourceIndex, int count, double[] buffer, int destinationIndex);
    }
}
