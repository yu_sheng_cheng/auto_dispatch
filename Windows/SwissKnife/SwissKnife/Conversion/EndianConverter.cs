﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife.Conversion
{
    public abstract class EndianConverter : IByteConverter
    {
        [StructLayout(LayoutKind.Explicit)]
        private struct DoubleLongUnion
        {
            [FieldOffset(0)] public double Double;
            [FieldOffset(0)] public long Long;
            public DoubleLongUnion(double d) { Long = 0; Double = d; }
            public DoubleLongUnion(long l) { Double = 0.0; Long = l; }
        }

        [StructLayout(LayoutKind.Explicit)]
        private struct SingleInt32Union
        {
            [FieldOffset(0)] public float Float;
            [FieldOffset(0)] public int Int;
            public SingleInt32Union(float f) { Int = 0; Float = f; }
            public SingleInt32Union(int i) { Float = 0.0f; Int = i; }
        }

        public byte[] GetBytes(bool value)
        {
            return new byte[] { value ? (byte)1 : (byte)0 };
        }

        public void GetBytes(bool value, byte[] buffer, int offset)
        {
            GetBytesChecked(value ? 1 : 0, 1, buffer, offset);
        }

        public void GetBytes(bool[] values, byte[] buffer, int offset)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            GetBytes(values, 0, values.Length, buffer, offset);
        }

        public void GetBytes(bool[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex)
        {
            ValidateGetRangeInput(values, sourceIndex, count, buffer, destinationIndex, 1);

            while (count-- > 0)
            {
                buffer[destinationIndex++] = values[sourceIndex++] ? (byte)1 : (byte)0;
            }
        }

        public byte[] GetBytes(char value)
        {
            return GetBytes(value, 2);
        }

        public void GetBytes(char value, byte[] buffer, int offset)
        {
            GetBytesChecked(value, 2, buffer, offset);
        }

        public void GetBytes(char[] values, byte[] buffer, int offset)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            GetBytes(values, 0, values.Length, buffer, offset);
        }

        public void GetBytes(char[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex)
        {
            ValidateGetRangeInput(values, sourceIndex, count, buffer, destinationIndex, 2);

            while (count-- > 0)
            {
                GetBytes(values[sourceIndex++], 2, buffer, destinationIndex);
                destinationIndex += 2;
            }
        }

        public byte[] GetBytes(short value)
        {
            return GetBytes(value, 2);
        }

        public void GetBytes(short value, byte[] buffer, int offset)
        {
            GetBytesChecked(value, 2, buffer, offset);
        }

        public void GetBytes(short[] values, byte[] buffer, int offset)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            GetBytes(values, 0, values.Length, buffer, offset);
        }

        public void GetBytes(short[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex)
        {
            ValidateGetRangeInput(values, sourceIndex, count, buffer, destinationIndex, 2);

            while (count-- > 0)
            {
                GetBytes(values[sourceIndex++], 2, buffer, destinationIndex);
                destinationIndex += 2;
            }
        }

        public byte[] GetBytes(ushort value)
        {
            return GetBytes(value, 2);
        }

        public void GetBytes(ushort value, byte[] buffer, int offset)
        {
            GetBytesChecked(value, 2, buffer, offset);
        }

        public void GetBytes(ushort[] values, byte[] buffer, int offset)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            GetBytes(values, 0, values.Length, buffer, offset);
        }

        public void GetBytes(ushort[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex)
        {
            ValidateGetRangeInput(values, sourceIndex, count, buffer, destinationIndex, 2);

            while (count-- > 0)
            {
                GetBytes(values[sourceIndex++], 2, buffer, destinationIndex);
                destinationIndex += 2;
            }
        }

        public byte[] GetBytes(int value)
        {
            return GetBytes(value, 4);
        }

        public void GetBytes(int value, byte[] buffer, int offset)
        {
            GetBytesChecked(value, 4, buffer, offset);
        }

        public void GetBytes(int[] values, byte[] buffer, int offset)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            GetBytes(values, 0, values.Length, buffer, offset);
        }

        public void GetBytes(int[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex)
        {
            ValidateGetRangeInput(values, sourceIndex, count, buffer, destinationIndex, 4);

            while (count-- > 0)
            {
                GetBytes(values[sourceIndex++], 4, buffer, destinationIndex);
                destinationIndex += 4;
            }
        }

        public byte[] GetBytes(uint value)
        {
            return GetBytes(value, 4);
        }

        public void GetBytes(uint value, byte[] buffer, int offset)
        {
            GetBytesChecked(value, 4, buffer, offset);
        }

        public void GetBytes(uint[] values, byte[] buffer, int offset)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            GetBytes(values, 0, values.Length, buffer, offset);
        }

        public void GetBytes(uint[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex)
        {
            ValidateGetRangeInput(values, sourceIndex, count, buffer, destinationIndex, 4);

            while (count-- > 0)
            {
                GetBytes(values[sourceIndex++], 4, buffer, destinationIndex);
                destinationIndex += 4;
            }
        }

        public byte[] GetBytes(long value)
        {
            return GetBytes(value, 8);
        }

        public void GetBytes(long value, byte[] buffer, int offset)
        {
            GetBytesChecked(value, 8, buffer, offset);
        }

        public void GetBytes(long[] values, byte[] buffer, int offset)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            GetBytes(values, 0, values.Length, buffer, offset);
        }

        public void GetBytes(long[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex)
        {
            ValidateGetRangeInput(values, sourceIndex, count, buffer, destinationIndex, 8);

            while (count-- > 0)
            {
                GetBytes(values[sourceIndex++], 8, buffer, destinationIndex);
                destinationIndex += 8;
            }
        }

        public byte[] GetBytes(ulong value)
        {
            return GetBytes(unchecked((long)value), 8);
        }

        public void GetBytes(ulong value, byte[] buffer, int offset)
        {
            GetBytesChecked(unchecked((long)value), 8, buffer, offset);
        }

        public void GetBytes(ulong[] values, byte[] buffer, int offset)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            GetBytes(values, 0, values.Length, buffer, offset);
        }

        public void GetBytes(ulong[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex)
        {
            ValidateGetRangeInput(values, sourceIndex, count, buffer, destinationIndex, 8);

            while (count-- > 0)
            {
                GetBytes(unchecked((long)values[sourceIndex++]), 8, buffer, destinationIndex);
                destinationIndex += 8;
            }
        }

        public byte[] GetBytes(float value)
        {
            return GetBytes(new SingleInt32Union(value).Int, 4);
        }

        public void GetBytes(float value, byte[] buffer, int offset)
        {
            GetBytesChecked(new SingleInt32Union(value).Int, 4, buffer, offset);
        }

        public void GetBytes(float[] values, byte[] buffer, int offset)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            GetBytes(values, 0, values.Length, buffer, offset);
        }

        public void GetBytes(float[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex)
        {
            ValidateGetRangeInput(values, sourceIndex, count, buffer, destinationIndex, 4);

            while (count-- > 0)
            {
                GetBytes(new SingleInt32Union(values[sourceIndex++]).Int, 4, buffer, destinationIndex);
                destinationIndex += 4;
            }
        }

        public byte[] GetBytes(double value)
        {
            return GetBytes(new DoubleLongUnion(value).Long, 8);
        }

        public void GetBytes(double value, byte[] buffer, int offset)
        {
            GetBytesChecked(new DoubleLongUnion(value).Long, 8, buffer, offset);
        }

        public void GetBytes(double[] values, byte[] buffer, int offset)
        {
            if (values == null)
                throw new ArgumentNullException("values");

            GetBytes(values, 0, values.Length, buffer, offset);
        }

        public void GetBytes(double[] values, int sourceIndex, int count, byte[] buffer, int destinationIndex)
        {
            ValidateGetRangeInput(values, sourceIndex, count, buffer, destinationIndex, 8);

            while (count-- > 0)
            {
                GetBytes(new DoubleLongUnion(values[sourceIndex++]).Long, 8, buffer, destinationIndex);
                destinationIndex += 8;
            }
        }

        private byte[] GetBytes(long value, int bytesRequired)
        {
            byte[] buffer = new byte[bytesRequired];
            GetBytes(value, bytesRequired, buffer, 0);
            return buffer;
        }

        private void GetBytesChecked(long value, int bytesRequired, byte[] buffer, int offset)
        {
            if (buffer == null)
                throw new ArgumentNullException("buffer");
            if (offset < 0)
                throw new ArgumentOutOfRangeException("offset");
            if (offset > buffer.Length - bytesRequired)
                throw new ArgumentException("The sum of the offset and bytesRequired arguments must be less than or equal to the collection's Count.");

            GetBytes(value, bytesRequired, buffer, offset);
        }

        protected abstract void GetBytes(long value, int bytesRequired, byte[] buffer, int offset);

        private void ValidateGetRangeInput(Array values, int sourceIndex, int count, byte[] buffer, int destinationIndex, int bytesRequired)
        {
            if (values == null)
                throw new ArgumentNullException("values");
            if (count < 0)
                throw new ArgumentOutOfRangeException("count");
            if (sourceIndex < 0)
                throw new ArgumentOutOfRangeException("sourceIndex");
            if (count > values.Length - sourceIndex)
                throw new ArgumentException("The sum of the sourceIndex and count arguments must be less than or equal to the collection's Count.");

            if (buffer == null)
                throw new ArgumentNullException("buffer");
            if (destinationIndex < 0)
                throw new ArgumentOutOfRangeException("destinationIndex");
            long bytesNeeded = (long)count * bytesRequired;
            if (bytesNeeded > int.MaxValue)
                throw new OverflowException("Buffer capacity overflowed.");
            if (bytesNeeded > buffer.Length - destinationIndex)
                throw new ArgumentException("Insufficient buffer space.");
        }

        public virtual byte[] GetBytes(string value, Encoding encoding)
        {
            if (encoding == null)
                throw new ArgumentNullException("encoding");
            return encoding.GetBytes(value);
        }

        public string GetString(byte[] bytes, int startIndex, int count, Encoding encoding)
        {
            if (bytes == null)
                throw new ArgumentNullException("bytes");
            if (count < 0)
                throw new ArgumentOutOfRangeException("count");
            if (startIndex < 0)
                throw new ArgumentOutOfRangeException("startIndex");
            if (count > bytes.Length - startIndex)
                throw new ArgumentException("The sum of the startIndex and count arguments must be less than or equal to the collection's Count.");
            if (encoding == null)
                throw new ArgumentNullException("encoding");

            if (count == 0)
                return string.Empty;

            return GetStringChecked(bytes, startIndex, count, encoding);
        }

        protected virtual string GetStringChecked(byte[] bytes, int startIndex, int count, Encoding encoding)
        {
            int codePage = encoding.CodePage;

            if (codePage == Encoding.ASCII.CodePage | codePage == Encoding.UTF8.CodePage)
            {
                int index = Array.IndexOf<byte>(bytes, 0, startIndex, count);
                if (index != -1)
                    count = index - startIndex;
            }
            else if (codePage == Encoding.Unicode.CodePage | codePage == Encoding.BigEndianUnicode.CodePage)
            {
                int index = startIndex;
                int endIndex = startIndex + count - 1;

                while ((index = Array.IndexOf<byte>(bytes, 0, index, endIndex - index + 1)) != -1)
                {
                    if (index == endIndex)
                        break;

                    if (bytes[index + 1] == 0)
                    {
                        count = index - startIndex;
                        break;
                    }

                    ++index;
                }
            }

            if (count == 0)
                return string.Empty;

            ConvertByteSequenceToProduceString(bytes, startIndex, count);
            return encoding.GetString(bytes, startIndex, count);
        }

        protected virtual void ConvertByteSequenceToProduceString(byte[] bytes, int startIndex, int count)
        {
            return;
        }

        public bool ToBoolean(byte[] value, int startIndex)
        {
            return FromBytesChecked(value, startIndex, 1) == 0 ? false : true;
        }

        public void ToBoolean(byte[] values, int sourceIndex, int count, bool[] buffer, int destinationIndex)
        {
            ValidateToRangeInput(values, sourceIndex, count, buffer, destinationIndex, 1);

            while (count-- > 0)
            {
                buffer[destinationIndex++] = values[sourceIndex++] > 0 ? true : false;
            }
        }

        public char ToChar(byte[] value, int startIndex)
        {
            return unchecked((char)FromBytesChecked(value, startIndex, 2));
        }

        public void ToChar(byte[] values, int sourceIndex, int count, char[] buffer, int destinationIndex)
        {
            ValidateToRangeInput(values, sourceIndex, count, buffer, destinationIndex, 2);

            int end = sourceIndex + count;
            while (sourceIndex < end)
            {
                buffer[destinationIndex++] = unchecked((char)FromBytes(values, sourceIndex, 2));
                sourceIndex += 2;
            }
        }

        public short ToInt16(byte[] value, int startIndex)
        {
            return unchecked((short)FromBytesChecked(value, startIndex, 2));
        }

        public void ToInt16(byte[] values, int sourceIndex, int count, short[] buffer, int destinationIndex)
        {
            ValidateToRangeInput(values, sourceIndex, count, buffer, destinationIndex, 2);

            int end = sourceIndex + count;
            while (sourceIndex < end)
            {
                buffer[destinationIndex++] = unchecked((short)FromBytes(values, sourceIndex, 2));
                sourceIndex += 2;
            }
        }

        public ushort ToUInt16(byte[] value, int startIndex)
        {
            return unchecked((ushort)FromBytesChecked(value, startIndex, 2));
        }

        public void ToUInt16(byte[] values, int sourceIndex, int count, ushort[] buffer, int destinationIndex)
        {
            ValidateToRangeInput(values, sourceIndex, count, buffer, destinationIndex, 2);

            int end = sourceIndex + count;
            while (sourceIndex < end)
            {
                buffer[destinationIndex++] = unchecked((ushort)FromBytes(values, sourceIndex, 2));
                sourceIndex += 2;
            }
        }

        public int ToInt32(byte[] value, int startIndex)
        {
            return unchecked((int)FromBytesChecked(value, startIndex, 4));
        }

        public void ToInt32(byte[] values, int sourceIndex, int count, int[] buffer, int destinationIndex)
        {
            ValidateToRangeInput(values, sourceIndex, count, buffer, destinationIndex, 4);

            int end = sourceIndex + count;
            while (sourceIndex < end)
            {
                buffer[destinationIndex++] = unchecked((int)FromBytes(values, sourceIndex, 4));
                sourceIndex += 4;
            }
        }

        public uint ToUInt32(byte[] value, int startIndex)
        {
            return unchecked((uint)FromBytesChecked(value, startIndex, 4));
        }

        public void ToUInt32(byte[] values, int sourceIndex, int count, uint[] buffer, int destinationIndex)
        {
            ValidateToRangeInput(values, sourceIndex, count, buffer, destinationIndex, 4);

            int end = sourceIndex + count;
            while (sourceIndex < end)
            {
                buffer[destinationIndex++] = unchecked((uint)FromBytes(values, sourceIndex, 4));
                sourceIndex += 4;
            }
        }

        public long ToInt64(byte[] value, int startIndex)
        {
            return FromBytesChecked(value, startIndex, 8);
        }

        public void ToInt64(byte[] values, int sourceIndex, int count, long[] buffer, int destinationIndex)
        {
            ValidateToRangeInput(values, sourceIndex, count, buffer, destinationIndex, 8);

            int end = sourceIndex + count;
            while (sourceIndex < end)
            {
                buffer[destinationIndex++] = FromBytes(values, sourceIndex, 8);
                sourceIndex += 8;
            }
        }

        public ulong ToUInt64(byte[] value, int startIndex)
        {
            return unchecked((ulong)(FromBytesChecked(value, startIndex, 8)));
        }

        public void ToUInt64(byte[] values, int sourceIndex, int count, ulong[] buffer, int destinationIndex)
        {
            ValidateToRangeInput(values, sourceIndex, count, buffer, destinationIndex, 8);

            int end = sourceIndex + count;
            while (sourceIndex < end)
            {
                buffer[destinationIndex++] = unchecked((ulong)FromBytes(values, sourceIndex, 8));
                sourceIndex += 8;
            }
        }

        public float ToSingle(byte[] value, int startIndex)
        {
            return new SingleInt32Union(ToInt32(value, startIndex)).Float;
        }

        public void ToSingle(byte[] values, int sourceIndex, int count, float[] buffer, int destinationIndex)
        {
            ValidateToRangeInput(values, sourceIndex, count, buffer, destinationIndex, 4);

            int end = sourceIndex + count;
            while (sourceIndex < end)
            {
                buffer[destinationIndex++] = new SingleInt32Union(unchecked((int)FromBytesChecked(values, sourceIndex, 4))).Float;
                sourceIndex += 4;
            }
        }

        public double ToDouble(byte[] value, int startIndex)
        {
            return new DoubleLongUnion(ToInt64(value, startIndex)).Double;
        }

        public void ToDouble(byte[] values, int sourceIndex, int count, double[] buffer, int destinationIndex)
        {
            ValidateToRangeInput(values, sourceIndex, count, buffer, destinationIndex, 8);

            int end = sourceIndex + count;
            while (sourceIndex < end)
            {
                buffer[destinationIndex++] = new DoubleLongUnion(FromBytesChecked(values, sourceIndex, 8)).Double;
                sourceIndex += 8;
            }
        }

        private long FromBytesChecked(byte[] value, int startIndex, int bytesToConvert)
        {
            if (value == null)
                throw new ArgumentNullException("value");
            if (startIndex < 0)
                throw new ArgumentOutOfRangeException("startIndex");
            if (startIndex > value.Length - bytesToConvert)
                throw new ArgumentException("The sum of the startIndex and bytesToConvert arguments must be less than or equal to the collection's Count.");

            return FromBytes(value, startIndex, bytesToConvert);
        }

        protected abstract long FromBytes(byte[] value, int startIndex, int bytesToConvert);

        private void ValidateToRangeInput(byte[] values, int sourceIndex, int count, Array buffer, int destinationIndex, int bytesToConvert)
        {
            if (values == null)
                throw new ArgumentNullException("values");
            if (count < 0)
                throw new ArgumentOutOfRangeException("count");
            if (sourceIndex < 0)
                throw new ArgumentOutOfRangeException("sourceIndex");
            if (count > values.Length - sourceIndex)
                throw new ArgumentException("The sum of the sourceIndex and count arguments must be less than or equal to the collection's Count.");
            if ((count & (bytesToConvert - 1)) != 0)
                throw new ArgumentException($"The count must be a multiple of {bytesToConvert.ToString()}.");

            if (buffer == null)
                throw new ArgumentNullException("buffer");
            if (destinationIndex < 0)
                throw new ArgumentOutOfRangeException("destinationIndex");
            if ((count / bytesToConvert) > buffer.Length - destinationIndex)
                throw new ArgumentException("Insufficient buffer space.");
        }
    }
}
