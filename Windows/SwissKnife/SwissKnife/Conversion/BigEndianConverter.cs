﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife.Conversion
{
    public class BigEndianConverter : EndianConverter
    {
        protected override void GetBytes(long value, int bytesRequired, byte[] buffer, int offset)
        {
            for (int i = bytesRequired - 1; i >= 0; --i)
            {
                buffer[offset + i] = unchecked((byte)(value & 0xff));
                value >>= 8;
            }
        }

        protected override long FromBytes(byte[] value, int startIndex, int bytesToConvert)
        {
            long result = 0;
            int end = startIndex + bytesToConvert;

            for (int i = startIndex; i < end; ++i)
            {
                result = (result << 8) | value[i];
            }
            return result;
        }
    }
}
