﻿using SwissKnife.Threading;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SwissKnife.ObjectPool
{
    public sealed class ConcurrentObjectPool<T> : IObjectPool<T> where T : class, IRecyclableObject
    {
        private readonly ConcurrentStack<T> _stack = new ConcurrentStack<T>();
        private readonly Func<T> _constructor;
        
        private readonly CancellationTokenSource _cts;
        private readonly LightSpinLock _lock = new LightSpinLock();
        private readonly ManualResetEvent _completionEvent = new ManualResetEvent(false);
        private bool _completionEventRaised;
        private bool _disposed;
  
        private int _instancesInPoolCount;
        private int _adjustmentRequestCount;
        private int _adjustPoolSizeIsInProgress;
        private int _instancesDestroyedCount;

        public int MinimumPoolSize { get; }

        public int MaximumPoolSize { get; }

        public int InstancesInPoolCount
        {
            get { return Math.Max(_instancesInPoolCount, 0); }
        }

        public ConcurrentObjectPool(Func<T> constructor, int maximumPoolSize)
            : this(constructor, 0, maximumPoolSize, 0) { }

        public ConcurrentObjectPool(Func<T> constructor, int maximumPoolSize, int initialCapacity)
            : this(constructor, 0, maximumPoolSize, initialCapacity) { }

        public ConcurrentObjectPool(Func<T> constructor, int minimumPoolSize, int maximumPoolSize, int initialCapacity)
        {
            if (minimumPoolSize < 0)
                throw new ArgumentOutOfRangeException("minimumPoolSize");
            if (maximumPoolSize < 1)
                throw new ArgumentOutOfRangeException("maximumPoolSize");
            if (minimumPoolSize > maximumPoolSize)
                throw new ArgumentException("Maximum pool size must be greater than or equals to the minimum pool size.");
            if (initialCapacity < 0)
                throw new ArgumentOutOfRangeException("initialCapacity");

            _constructor = constructor ?? throw new ArgumentNullException("constructor");
            MinimumPoolSize = minimumPoolSize;
            MaximumPoolSize = maximumPoolSize;

            if (initialCapacity < minimumPoolSize)
                initialCapacity = minimumPoolSize;

            if (initialCapacity > 0)
            {
                T[] instances = new T[initialCapacity];

                int i = initialCapacity - 1;

                try
                {
                    for (; i >= 0; --i)
                    {
                        instances[i] = CreateInstance();
                    }
                }
                catch
                {
                    for (++i; i < initialCapacity; ++i)
                    {
                        T instance = instances[i];
                        instance.Bind(null);
                        instance.Dispose();
                    }
                    throw;
                }

                _stack.PushRange(instances);
                _instancesInPoolCount = initialCapacity;
            }

            _cts = new CancellationTokenSource();
        }

        public void Dispose()
        {
            bool shouldWaitSignal;

            try
            {
                _lock.Enter();

                if (_disposed)
                    return;
                _disposed = true;

                shouldWaitSignal = Interlocked.CompareExchange(ref _adjustPoolSizeIsInProgress, 0, 0) != 0;
                if (!shouldWaitSignal)
                    _completionEventRaised = true;
            }
            finally
            {
                _lock.Leave();
            }

            try { _cts.Cancel(); } catch { }

            if (shouldWaitSignal)
                _completionEvent.WaitOne();
            
            T[] instances = new T[128];
            int count;

            while ((count = _stack.TryPopRange(instances)) > 0)
            {
                for (int i = count - 1; i >= 0; --i)
                {
                    T instance = instances[i];
                    instance.Bind(null);
                    instance.Dispose();
                }
            }

            _cts.Dispose();
            _completionEvent.Dispose();
        }

        public T Take()
        {
            if (_disposed)
                throw new ObjectDisposedException(GetType().FullName);

            T instance;
            if (_stack.TryPop(out instance))
            {
                if (Math.Max(Interlocked.Decrement(ref _instancesInPoolCount), 0) < MinimumPoolSize)
                {
                    if (Interlocked.Increment(ref _adjustmentRequestCount) == 1)
                        ThreadPool.QueueUserWorkItem(_ => AdjustPoolSizeToBounds());
                }
                return instance;
            }

            return CreateInstance();
        }

        private T CreateInstance()
        {
            T instance = _constructor();
            instance.Bind(this);
            return instance;
        }

        public void Return(T instance)
        {
            if (_disposed)
                throw new ObjectDisposedException(GetType().FullName);
            if (instance == null)
                throw new ArgumentNullException("instance");

            instance.Recycle();
            _stack.Push(instance);

            if (Interlocked.Increment(ref _instancesInPoolCount) > MaximumPoolSize)
            {
                if (Interlocked.Increment(ref _instancesDestroyedCount) == 1)
                {
                    ThreadPool.QueueUserWorkItem(
                        async (_) =>
                        {
                            if (await Delay(300).ConfigureAwait(false))
                            {
                                Interlocked.Exchange(ref _instancesDestroyedCount, 0);
                                if (Interlocked.Increment(ref _adjustmentRequestCount) == 1)
                                    ThreadPool.QueueUserWorkItem(__ => AdjustPoolSizeToBounds());
                            }
                            // else { ConcurrentObjectPool has been disposed }
                        });
                }
            }
        }

        IRecyclableObject IObjectPool.Take()
        {
            return Take();
        }

        void IObjectPool.Return(IRecyclableObject recyclableObject)
        {
            T instance = recyclableObject as T;
            if (instance == null)
                throw new InvalidCastException($"Conversion to a {typeof(T).ToString()} is not supported.");
            Return(instance);
        }

        private async void AdjustPoolSizeToBounds()
        {
            SpinWait spinwait = new SpinWait();
            while (Interlocked.Exchange(ref _adjustPoolSizeIsInProgress, 1) == 1)
            {
                spinwait.SpinOnce();
            }

            while (true)
            {
                if (CheckDisposed())
                    return;

                int seq = Interlocked.CompareExchange(ref _adjustmentRequestCount, 1, 1);

                AdjustPoolSizeToBoundsCore();

                if (Interlocked.CompareExchange(ref _adjustmentRequestCount, 0, seq) == seq)
                {
                    Interlocked.Exchange(ref _adjustPoolSizeIsInProgress, 0);
                    CheckDisposed();
                    return;
                }

                await Delay(100).ConfigureAwait(false);
            }
        }

        private bool CheckDisposed()
        {
            ManualResetEvent completionEvent = null;
            try
            {
                _lock.Enter();

                if (_disposed)
                {
                    if (!_completionEventRaised)
                    {
                        completionEvent = _completionEvent;
                        _completionEventRaised = true;
                    }
                    return true;
                }
                return false;
            }
            finally
            {
                _lock.Leave();
                if (completionEvent != null)
                    completionEvent.Set();
            }
        }

        private void AdjustPoolSizeToBoundsCore()
        {
            int instancesInPoolCount = Volatile.Read(ref _instancesInPoolCount);

            if (instancesInPoolCount < MinimumPoolSize)
            {
                int max = Math.Min(MinimumPoolSize - instancesInPoolCount, 128);
                int count = 0;

                T[] instances = new T[max];

                try
                {
                    while (count < max)
                    {
                        instances[count] = CreateInstance();
                        ++count;
                    }
                }
                catch
                {

                }

                if (count > 0)
                {
                    _stack.PushRange(instances, 0, count);
                    Interlocked.Add(ref _instancesInPoolCount, count);
                }
            }
            else if (instancesInPoolCount > MaximumPoolSize)
            {
                int max = Math.Min(instancesInPoolCount - MaximumPoolSize, 128);

                T[] instances = new T[max];

                int count = _stack.TryPopRange(instances);
                if (count > 0)
                {
                    Interlocked.Add(ref _instancesInPoolCount, -count);

                    for (int i = count - 1; i >= 0; --i)
                    {
                        T instance = instances[i];
                        instance.Bind(null);
                        instance.Dispose();
                    }
                }
            }
        }

        private async Task<bool> Delay(int millisecondsDelay)
        {
            try
            {
                await Task.Delay(millisecondsDelay, _cts.Token).ConfigureAwait(false);
                return true;
            }
            catch (TaskCanceledException)
            {
                return false;
            }
            catch (ObjectDisposedException)
            {
                return false;
            }
        }
    }
}
