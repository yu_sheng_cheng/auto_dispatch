﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife.ObjectPool
{
    public interface IRecyclableObject : IDisposable
    {
        void Bind(IObjectPool pool);

        void Release();

        void Recycle();
    }
}
