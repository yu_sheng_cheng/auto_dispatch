﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife.ObjectPool
{
    public interface IObjectPool : IDisposable
    {
        IRecyclableObject Take();

        void Return(IRecyclableObject instance);
    }

    public interface IObjectPool<T> : IObjectPool where T : class, IRecyclableObject
    {
        new T Take();

        void Return(T instance);
    }
}
