﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife.ObjectPool
{
    public abstract class RecyclableObject : IRecyclableObject
    {
        private IObjectPool _pool;

        public void Bind(IObjectPool pool)
        {
            _pool = pool;
        }

        public abstract void Dispose();

        public void Release()
        {
            if (_pool == null)
            {
                Dispose();
                return;
            }
            _pool.Return(this);
        }

        public virtual void Recycle()
        {

        }
    }
}
