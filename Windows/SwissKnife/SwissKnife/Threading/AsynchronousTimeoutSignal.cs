﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SwissKnife.Threading
{
    public class AsynchronousTimeoutSignal
    {
        private const int Wait = 0;
        private const int Signaled = 1;
        private const int Masked = 2;

        private object _waiter;
        private readonly Action<object> _signal;
        private int _status;

        public bool IsSignaled
        {
            get
            {
                Thread.MemoryBarrier();
                return _status == Signaled;
            }
        }

        public AsynchronousTimeoutSignal(Action<object> signal)
        {
            _signal = signal ?? throw new ArgumentNullException("signal");
        }

        public void Mask()
        {
            Interlocked.Exchange(ref _status, Masked);
        }

        public void Unmask()
        {
            Interlocked.CompareExchange(ref _status, Wait, Masked);
        }

        public void Register(object waiter, out bool signaled)
        {
            if (waiter == null)
                throw new ArgumentNullException("waiter");
            if (Interlocked.CompareExchange(ref _waiter, waiter, null) != null)
                throw new InvalidOperationException("An asynchronous timeout signal has already been registered.");
            signaled = _status == Signaled;
        }

        public void UnRegister(out bool signaled)
        {
            Interlocked.Exchange(ref _waiter, null);
            signaled = _status == Signaled;
        }

        public bool Raise()
        {
            if (Interlocked.CompareExchange(ref _status, Signaled, Wait) == Wait)
            {
                object waiter = Interlocked.CompareExchange(ref _waiter, null, null);
                if (waiter != null)
                    _signal(waiter);
                return true;
            }
            return false;
        }

        public void Reset()
        {
            Interlocked.CompareExchange(ref _status, Wait, Signaled);
        }
    }
}
