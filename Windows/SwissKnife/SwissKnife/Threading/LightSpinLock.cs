﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SwissKnife.Threading
{
    public class LightSpinLock
    {
        private int _resourceInUse;

        public void Enter()
        {
            SpinWait spinwait = new SpinWait();
            while (true)
            {
                // Always set resource to in-use
                // When this thread changes it from not in-use, return
                if (Interlocked.Exchange(ref _resourceInUse, 1) == 0) return;
                // Black magic goes here...
                spinwait.SpinOnce();
            }
        }

        public void Leave()
        {
            Volatile.Write(ref _resourceInUse, 0);
        }
    }
}
