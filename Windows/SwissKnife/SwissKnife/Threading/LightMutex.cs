﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SwissKnife.Threading
{
    public sealed class LightMutex : IDisposable
    {
        private int _waiters;
        private readonly AutoResetEvent _lock = new AutoResetEvent(false);

        public void Dispose()
        {
            _lock.Dispose();
        }

        public void Enter()
        {
            if (Interlocked.Increment(ref _waiters) == 1)
                return;

            _lock.WaitOne();
        }

        public void Leave()
        {
            if (Interlocked.Decrement(ref _waiters) == 0)
                return;

            _lock.Set();
        }
    }
}
