﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SwissKnife.Threading
{
    public class OneWriteSeqLock
    {
        private uint _sequence;

        public uint ReadSeqBegin()
        {
            SpinWait spinwait = new SpinWait();

            while(true)
            {
                uint seqnum = Volatile.Read(ref _sequence);

                bool even = (seqnum & 1) == 0;
                if (even)
                    return seqnum;

                spinwait.SpinOnce();
            }
        }

        public bool ReadSeqRetry(uint start)
        {
            Thread.MemoryBarrier();
            return start != _sequence;
        }

        public void WriteSeqLock()
        {
            uint unused = unchecked(++_sequence);
            Thread.MemoryBarrier();
        }

        public void WriteSeqUnlock()
        {
            uint sequence = unchecked(_sequence + 1);
            Volatile.Write(ref _sequence, sequence);
        }
    }
}
