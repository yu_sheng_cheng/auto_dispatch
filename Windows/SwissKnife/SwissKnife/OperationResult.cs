﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife
{
    public class OperationResult
    {
        public const int INVALID_LOW_LEVEL_ERROR_CODE = 87878787;

        public bool IsSuccess { get; }

        public string Message { get; set; } = string.Empty;

        public string ErrorSource { get; set; } = string.Empty;

        public Exception Exception { get; set; }

        public int ErrorCode { get; set; }

        public int LowLevelErrorCode { get; set; } = INVALID_LOW_LEVEL_ERROR_CODE;

        public bool IsLowLevelErrorCodeValid { get { return LowLevelErrorCode != INVALID_LOW_LEVEL_ERROR_CODE; } }

        public OperationResult()
        {
            IsSuccess = true;
        }

        public OperationResult(string message)
        {
            IsSuccess = true;
            Message = message ?? string.Empty;
        }

        public OperationResult(int errorCode, int lowLevelErrorCode = INVALID_LOW_LEVEL_ERROR_CODE) :
            this(string.Empty, errorCode, lowLevelErrorCode, string.Empty) { }

        public OperationResult(string errorSource, int errorCode, int lowLevelErrorCode = INVALID_LOW_LEVEL_ERROR_CODE) :
            this(errorSource, errorCode, lowLevelErrorCode, string.Empty) { }

        public OperationResult(string errorSource, int errorCode, string message) :
            this(errorSource, errorCode, INVALID_LOW_LEVEL_ERROR_CODE, message) { }

        public OperationResult(string errorSource, int errorCode, int lowLevelErrorCode, string message)
        {
            IsSuccess = false;
            ErrorSource = errorSource ?? string.Empty;
            ErrorCode = errorCode;
            LowLevelErrorCode = lowLevelErrorCode;
            Message = message ?? string.Empty;
        }

        public OperationResult(Exception exception) : this(string.Empty, string.Empty, exception) { }

        public OperationResult(string errorSource, Exception exception) : this(errorSource, string.Empty, exception) { }

        public OperationResult(string errorSource, string message, Exception exception)
        {     
            IsSuccess = false;
            ErrorSource = errorSource ?? string.Empty;
            Exception = exception ?? throw new ArgumentNullException("exception");
            ErrorCode = -1;
            Message = message ?? string.Empty;
        }

        public override string ToString()
        {
            if (IsSuccess)
                return string.IsNullOrEmpty(Message) ? "Success" : $"Success: {Message}";
            else
            {
                if (Exception != null)
                    return $"ErrorSource: {ErrorSource}, ErrorMessage: {Message}, Exception: {Exception.GetType()}";
                else
                {
                    if (IsLowLevelErrorCodeValid)
                        return $"ErrorSource: {ErrorSource}, ErrorCode: {ErrorCode.ToString()}, LowLevelErrorCode: 0x{LowLevelErrorCode.ToString("X4")}, ErrorMessage: {Message}";
                    else
                        return $"ErrorSource: {ErrorSource}, ErrorCode: {ErrorCode.ToString()}, ErrorMessage: {Message}";
                }
            }      
        }
    }

    public class OperationResult<T> : OperationResult
    {
        public T Value { get; set; }

        public OperationResult() { }

        public OperationResult(string message) : base(message) { }

        public OperationResult(int errorCode, int lowLevelErrorCode = INVALID_LOW_LEVEL_ERROR_CODE) : base(errorCode, lowLevelErrorCode) { }

        public OperationResult(string errorSource, int errorCode, int lowLevelErrorCode = INVALID_LOW_LEVEL_ERROR_CODE) :
            base(errorSource, errorCode, lowLevelErrorCode) { }

        public OperationResult(string errorSource, int errorCode, string message) : base(errorSource, errorCode, message) { }

        public OperationResult(string errorSource, int errorCode, int lowLevelErrorCode, string message) :
            base(errorSource, errorCode, lowLevelErrorCode, message) { }

        public OperationResult(Exception exception) : base(string.Empty, string.Empty, exception) { }

        public OperationResult(string errorSource, Exception exception) : base(errorSource, string.Empty, exception) { }

        public OperationResult(string errorSource, string message, Exception exception): base(errorSource, message, exception) { }
    }

    public class OperationResult<T1, T2> : OperationResult
    {
        public T1 Value1 { get; set; }

        public T2 Value2 { get; set; }

        public OperationResult() { }

        public OperationResult(string message) : base(message) { }

        public OperationResult(int errorCode, int lowLevelErrorCode = INVALID_LOW_LEVEL_ERROR_CODE) : base(errorCode, lowLevelErrorCode) { }

        public OperationResult(string errorSource, int errorCode, int lowLevelErrorCode = INVALID_LOW_LEVEL_ERROR_CODE) :
            base(errorSource, errorCode, lowLevelErrorCode) { }

        public OperationResult(string errorSource, int errorCode, string message) : base(errorSource, errorCode, message) { }

        public OperationResult(string errorSource, int errorCode, int lowLevelErrorCode, string message) :
            base(errorSource, errorCode, lowLevelErrorCode, message) { }

        public OperationResult(Exception exception) : base(string.Empty, string.Empty, exception) { }

        public OperationResult(string errorSource, Exception exception) : base(errorSource, string.Empty, exception) { }

        public OperationResult(string errorSource, string message, Exception exception) : base(errorSource, message, exception) { }
    }
}
