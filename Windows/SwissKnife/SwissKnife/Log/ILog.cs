﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife.Log
{
    public interface ILog
    {
        void d(string message);
        void d(string keyword, string message);
        void i(string message);
        void i(string keyword, string message);
        void w(string message);
        void w(string keyword, string message);
        void w(string message, Exception ex);
        void w(string keyword, string message, Exception ex);
        void e(string message);
        void e(string keyword, string message);
        void e(string message, Exception ex);
        void e(string keyword, string message, Exception ex);
        void f(string message);
        void f(string keyword, string message);
        void f(string message, Exception ex);
        void f(string keyword, string message, Exception ex);
    }
}
