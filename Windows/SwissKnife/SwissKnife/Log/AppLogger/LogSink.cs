﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SwissKnife.Log.AppLogger
{
    public partial class Logger
    {
        private class LogSink : ILogSink
        {
            public ILogSink Sink { get; }

            public string Name { get; }

            public LogLevel Level { get; set; }

            public List<LogEntry> Queue { get; } = new List<LogEntry>();

            public LogSink(ILogSink sink, string name, LogLevel level)
            {
                Sink = sink;
                Name = name;
                Level = level;
            }

            public void Dispose()
            {
                Sink.Dispose();
            }

            public void Log(IEnumerable<LogEntry> logs)
            {
                Sink.Log(logs);
            }

            public void Configure(XmlElement xmlElement)
            {
                Sink.Configure(xmlElement);
            }

            public void Change(string[] options)
            {
                Sink.Change(options);
            }
        }
    }
}
