﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife.Log.AppLogger
{
    public enum LogLevel : byte
    {
        DEBUG,
        INFO,
        WARNING,
        ERROR,
        FATAL,
        None
    }
}
