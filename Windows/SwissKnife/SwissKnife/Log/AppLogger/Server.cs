﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SwissKnife.Log.AppLogger
{
    public partial class Logger
    {
        private class PipeAsyncEventArgs
        {
            public const int BUFFER_SIZE = 512;

            public NamedPipeServerStream Pipe { get; }

            public byte[] Buffer { get; }

            public PipeAsyncEventArgs(NamedPipeServerStream pipe)
            {
                Pipe = pipe;
                Buffer = new byte[BUFFER_SIZE];
            }
        }

        private const int PIPE_SERVER_IDLE = 0;
        private const int PIPE_SERVER_CHANGE_CFG = 1;
        private const int PIPE_SERVER_STOPPED = 2;

        private NamedPipeServerStream _server;
        private int _serverStatus;

        private void StartServer()
        {
            try
            {
                string pipeName = $"{GUID}.{_name}.{Process.GetCurrentProcess().Id.ToString()}";
                _server = new NamedPipeServerStream(pipeName, PipeDirection.In, 1, PipeTransmissionMode.Message, PipeOptions.Asynchronous);
                _serverStatus = PIPE_SERVER_IDLE;
                _server.BeginWaitForConnection(ConnectionCallback, new PipeAsyncEventArgs(_server));
            }
            catch (Exception e)
            {
                if (_server != null)
                {
                    _server.Dispose();
                    _server = null;
                }

                w(LOG_KEYWORD, "Start Server:", e);
            }
        }

        private void StopServer()
        {
            if (_server != null)
            {
                _server.Dispose();
                _server = null;

                SpinWait spinwait = new SpinWait();
                while (true)
                {
                    if (Interlocked.CompareExchange(ref _serverStatus, PIPE_SERVER_STOPPED, PIPE_SERVER_IDLE) == PIPE_SERVER_IDLE)
                        break;
                    spinwait.SpinOnce();
                }
            }
        }

        private void ConnectionCallback(IAsyncResult asyncResult)
        {
            PipeAsyncEventArgs pipeAsyncEventArgs = (PipeAsyncEventArgs)asyncResult.AsyncState;

            try
            {
                pipeAsyncEventArgs.Pipe.EndWaitForConnection(asyncResult);
                pipeAsyncEventArgs.Pipe.BeginRead(pipeAsyncEventArgs.Buffer, 0, PipeAsyncEventArgs.BUFFER_SIZE, ReadCallback, pipeAsyncEventArgs);
            }
            catch
            {

            }
        }

        private void ReadCallback(IAsyncResult asyncResult)
        {
            PipeAsyncEventArgs pipeAsyncEventArgs = (PipeAsyncEventArgs)asyncResult.AsyncState;

            try
            {
                int bytesReceived = pipeAsyncEventArgs.Pipe.EndRead(asyncResult);

                if (bytesReceived > 0)
                {
                    if (!pipeAsyncEventArgs.Pipe.IsMessageComplete)
                    {
                        pipeAsyncEventArgs.Pipe.Disconnect();
                        pipeAsyncEventArgs.Pipe.BeginWaitForConnection(ConnectionCallback, pipeAsyncEventArgs);
                        return;
                    }

                    if (Interlocked.CompareExchange(ref _serverStatus, PIPE_SERVER_CHANGE_CFG, PIPE_SERVER_IDLE) == PIPE_SERVER_IDLE)
                    {
                        try
                        {
                            Change(pipeAsyncEventArgs.Buffer, bytesReceived);
                        }
                        catch (Exception e)
                        {
                            w(LOG_KEYWORD, "Change:", e);
                        }
                        finally
                        {
                            Interlocked.Exchange(ref _serverStatus, PIPE_SERVER_IDLE);
                        }
                    }
                }

                if (pipeAsyncEventArgs.Pipe.IsConnected)
                {
                    pipeAsyncEventArgs.Pipe.BeginRead(pipeAsyncEventArgs.Buffer, 0, PipeAsyncEventArgs.BUFFER_SIZE, ReadCallback, pipeAsyncEventArgs);
                }
                else
                {
                    pipeAsyncEventArgs.Pipe.Disconnect();
                    pipeAsyncEventArgs.Pipe.BeginWaitForConnection(ConnectionCallback, pipeAsyncEventArgs);
                }
            }
            catch
            {

            }
        }

        private void Change(byte[] data, int length)
        {
            string settings = Encoding.ASCII.GetString(data, 0, length);
            string[] options = settings.Split(new char[] { '&' }, StringSplitOptions.RemoveEmptyEntries);

            string name = string.Empty;
            LogLevel level = LogLevel.None;

            foreach (var opt in options)
            {
                if (string.CompareOrdinal(opt, 0, "Name=", 0, 5) == 0)
                    name = opt.Substring(5);
                else if (string.CompareOrdinal(opt, 0, "Level=", 0, 6) == 0)
                    level = ParseLogLevel(opt.Substring(6));
            }

            bool found = false;

            foreach (var sink in _sinks)
            {
                if (sink.Name == name)
                {
                    sink.Level = level;
                    sink.Change(options);
                    found = true;
                    break;
                }
            }

            if (found)
            {
                LogLevel lowestLevel = LogLevel.None;

                foreach (var sink in _sinks)
                {
                    if (sink.Level < lowestLevel)
                    {
                        lowestLevel = sink.Level;
                    }
                }

                _lowestLevel = lowestLevel;
            }
        }
    }
}
