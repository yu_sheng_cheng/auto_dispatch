﻿#define LOCAL_USE_SPINLOCK
using SwissKnife.Threading;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace SwissKnife.Log.AppLogger.Sink
{
    sealed class RollingFileSink : ILogSink
    {
        private struct LogFile
        {
            public string FullPath { get; set; }

            public DateTime FirstLogOccurTime { get; set; }

            public LogFile(string fullPath, DateTime firstLogOccurTime)
            {
                FullPath = fullPath;
                FirstLogOccurTime = firstLogOccurTime;
            }
        }

        private const int FILE_SIZE_UP_LIMIT = 1 << 30;
        private const int MAX_NUMBER_OF_ROTATED_FILES = 1024;

        private string _saveDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "AppLog");
        private string _dateTimePattern = "yyyyMMdd-HHmmss.fff";
        private int _fileMaxSize = 2 << 20;
        private int _maxRotatedFiles = 1;
        private int _lazyWriteTime = 1000;

        private readonly List<LogEntry> _queue = new List<LogEntry>();
#if LOCAL_USE_SPINLOCK
        private readonly LightSpinLock _queueLock = new LightSpinLock();
#else
        private readonly LightMutex _queueLock = new LightMutex();
#endif
        private readonly StringBuilder _stringBuilder = new StringBuilder();
        private readonly Timer _timer;
        private int _timerStarted;
        private int _timerTicks;

        private readonly List<LogFile> _logFiles = new List<LogFile>();
        private LogFile _writingLogFile;
        private StreamWriter _streamWriter;
        private ManualResetEvent _saveCompletionEvent;
        private readonly object _lock = new object();

        public RollingFileSink()
        {
            _timer = new Timer(Timer_Expired, null, Timeout.Infinite, Timeout.Infinite);
        }

        public void Dispose()
        {
            using (var completionEvent = new ManualResetEvent(false))
            {
                if (_timer.Dispose(completionEvent))
                    completionEvent.WaitOne();
            }

            bool shouldWaitSignal = false;

            lock (_lock)
            {
                if (Interlocked.CompareExchange(ref _timerTicks, 0, 0) != 0)
                {
                    _saveCompletionEvent = new ManualResetEvent(false);
                    shouldWaitSignal = true;
                }
            }

            if (shouldWaitSignal)
            {
                _saveCompletionEvent.WaitOne();
                _saveCompletionEvent.Dispose();
                _saveCompletionEvent = null;
            }

            WriteLogToFileAsync().Wait();

            if (_streamWriter != null)
                _streamWriter.Dispose();

#if !LOCAL_USE_SPINLOCK
            _queueLock.Dispose();
#endif
        }

        public void Configure(XmlElement xmlRootElement)
        {
            foreach (XmlNode xmlNode in xmlRootElement.ChildNodes)
            {
                XmlElement xmlElement = xmlNode as XmlElement;
                if (xmlElement == null)
                    continue;

                string value = xmlElement.GetAttribute("Value");

                switch (xmlNode.Name)
                {
                    case "Directory":
                        _saveDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, value);
                        break;
                    case "DateTimePattern":
                        if (!string.IsNullOrWhiteSpace(value))
                            _dateTimePattern = value;
                        break;
                    case "FileMaxSize":
                        var match = Regex.Match(value, "^(?<SIZE>[0-9]+)(?<UNIT>[K|k|M|m|G|g]?)$");
                        if (match.Success)
                        {
                            uint size;

                            if (uint.TryParse(match.Groups["SIZE"].Value, out size))
                            {
                                switch (match.Groups["UNIT"].Value.ToUpper())
                                {
                                    case "K": size <<= 10; break;
                                    case "M": size <<= 20; break;
                                    case "G": size <<= 30; break;
                                }

                                if (size >= 1 && size <= FILE_SIZE_UP_LIMIT)
                                    _fileMaxSize = (int)size;
                            }
                        }
                        break;
                    case "NumberOfRotatedFiles":
                        int nr;
                        if (int.TryParse(value, out nr))
                        {
                            if (nr >= 1)
                            {
                                if (nr > MAX_NUMBER_OF_ROTATED_FILES)
                                    nr = MAX_NUMBER_OF_ROTATED_FILES;
                                _maxRotatedFiles = nr;
                            }
                        }
                        break;
                    case "LazyWriteTime":
                        int timeout;
                        if (int.TryParse(value, out timeout))
                        {
                            if (timeout >= 0)
                                _lazyWriteTime = timeout;
                        }
                        break;
                }
            }

            CreateLogDirectoryIfNotExist();
            FindLogFiles();

            _logFiles.Sort((x, y) => x.FirstLogOccurTime.CompareTo(y.FirstLogOccurTime));

            int count = _logFiles.Count - _maxRotatedFiles;
            if (count > 0)
            {
                LogFile[] recyclingFiles = new LogFile[count];

                for (int i = count - 1; i >= 0; --i)
                {
                    recyclingFiles[i] = _logFiles[i];
                }

                _logFiles.RemoveRange(0, count);

                Task.Run(() =>
                {
                    foreach (var file in recyclingFiles)
                    {
                        try
                        {
                            File.Delete(file.FullPath);
                        }
                        catch
                        {

                        }
                    }
                });
            }
        }

        private void CreateLogDirectoryIfNotExist()
        {
            try
            {
                Directory.CreateDirectory(_saveDirectory);
            }
            catch
            {

            }
        }

        private void FindLogFiles()
        {
            try
            {
                LogFile writingLogFile = new LogFile(string.Empty, DateTime.MinValue);
                long writingLogFileLength = 0;

                DirectoryInfo di = new DirectoryInfo(_saveDirectory);

                foreach (var fi in di.EnumerateFiles("*", SearchOption.TopDirectoryOnly))
                {
                    if (fi.Extension == ".txt")
                    {
                        string fileName = Path.GetFileNameWithoutExtension(fi.Name);
                        DateTime dateTime;

                        if (DateTime.TryParseExact(fileName, _dateTimePattern, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
                        {
                            _logFiles.Add(new LogFile(fi.FullName, dateTime));
                        }
                        else if (string.CompareOrdinal(fileName, 0, "___", 0, 3) == 0)
                        {
                            if (DateTime.TryParseExact(fileName.Substring(3), _dateTimePattern, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
                            {
                                if (dateTime >= writingLogFile.FirstLogOccurTime)
                                {
                                    writingLogFile.FullPath = fi.FullName;
                                    writingLogFile.FirstLogOccurTime = dateTime;
                                    writingLogFileLength = fi.Length;
                                }
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(writingLogFile.FullPath))
                {
                    if (writingLogFileLength >= _fileMaxSize)
                    {
                        string destFile = Path.Combine(_saveDirectory,
                            $"{writingLogFile.FirstLogOccurTime.ToString(_dateTimePattern, CultureInfo.InvariantCulture)}.txt");

                        File.Delete(destFile);
                        File.Move(writingLogFile.FullPath, destFile);
                        writingLogFile.FullPath = destFile;
                        _logFiles.Add(writingLogFile);
                    }
                    else
                    {
                        _streamWriter = new StreamWriter(writingLogFile.FullPath, true, Encoding.UTF8);
                        _streamWriter.AutoFlush = true;
                        _writingLogFile = writingLogFile;
                    }
                }
            }
            catch
            {

            }
        }

        public void Change(string[] options)
        {

        }

        public void Log(IEnumerable<LogEntry> logs)
        {
            _queueLock.Enter();
            _queue.AddRange(logs);
            _queueLock.Leave();

            if (Interlocked.Exchange(ref _timerStarted, 1) == 0)
                _timer.Change(_lazyWriteTime, Timeout.Infinite);
        }

        private async void Timer_Expired(object state)
        {
            Interlocked.Exchange(ref _timerStarted, 0);

            if (Interlocked.Increment(ref _timerTicks) != 1)
                return;

            while (true)
            {
                await WriteLogToFileAsync().ConfigureAwait(false);

                if (Interlocked.CompareExchange(ref _timerTicks, 0, 1) == 1)
                {
                    bool shouldSignal;

                    lock (_lock)
                    {
                        shouldSignal = _saveCompletionEvent != null;
                    }

                    if (shouldSignal)
                        _saveCompletionEvent.Set();
                    return;
                }

                Interlocked.Exchange(ref _timerTicks, 1);
            }
        }

        private async Task WriteLogToFileAsync()
        {
            _queueLock.Enter();
            var logs = _queue.ToArray();
            _queue.Clear();
            _queueLock.Leave();

            if (logs.Length == 0)
                return;

            try
            {
                foreach (var log in logs)
                {
                    _stringBuilder.AppendLine(log.ToString());
                }

                if (_streamWriter == null)
                {
                    DateTime firstLogOccurTime = logs[0].OccurTime;
                    string fileName = $"___{firstLogOccurTime.ToString(_dateTimePattern, CultureInfo.InvariantCulture)}.txt";

                    _writingLogFile.FullPath = Path.Combine(_saveDirectory, fileName);
                    _writingLogFile.FirstLogOccurTime = firstLogOccurTime;
                    _streamWriter = new StreamWriter(_writingLogFile.FullPath, false, Encoding.UTF8);
                    _streamWriter.AutoFlush = true;
                }

                await _streamWriter.WriteAsync(_stringBuilder.ToString()).ConfigureAwait(false);

                if (_streamWriter.BaseStream.Length >= _fileMaxSize)
                {
                    _streamWriter.Dispose();
                    _streamWriter = null;

                    string destFile = Path.Combine(_saveDirectory,
                        $"{_writingLogFile.FirstLogOccurTime.ToString(_dateTimePattern, CultureInfo.InvariantCulture)}.txt");

                    File.Delete(destFile);
                    File.Move(_writingLogFile.FullPath, destFile);

                    _logFiles.Add(new LogFile(destFile, _writingLogFile.FirstLogOccurTime));

                    if (_logFiles.Count > _maxRotatedFiles)
                    {
                        LogFile recyclingFile = _logFiles[0];
                        _logFiles.RemoveAt(0);
                        File.Delete(recyclingFile.FullPath);
                    }
                }
            }
            catch
            {

            }
            finally
            {
                _stringBuilder.Clear();
            }
        }
    }
}
