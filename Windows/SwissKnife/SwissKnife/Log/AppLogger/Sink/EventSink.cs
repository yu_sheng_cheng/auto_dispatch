﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace SwissKnife.Log.AppLogger.Sink
{
    [Serializable]
    public class LogReceivedEventArgs : EventArgs
    {
        List<LogEntry> _logs = new List<LogEntry>();

        public IEnumerable<LogEntry> Logs { get { return _logs; } }

        public LogReceivedEventArgs(IEnumerable<LogEntry> logs)
        {
            if (logs == null)
                throw new ArgumentNullException("logs");

            _logs.AddRange(logs);
        }
    }

    public delegate void LogReceivedEventHandler(object sender, LogReceivedEventArgs e);

    public sealed class EventSink : ILogSink
    {
        public event LogReceivedEventHandler LogReceived;

        public void Dispose()
        {

        }

        public void Log(IEnumerable<LogEntry> logs)
        {
            RaiseLogReceivedEvent(new LogReceivedEventArgs(logs));
        }

        private void RaiseLogReceivedEvent(LogReceivedEventArgs e)
        {
            LogReceivedEventHandler temp = Volatile.Read(ref LogReceived);
            temp?.Invoke(this, e);
        }

        public void Configure(XmlElement xmlElement)
        {

        }

        public void Change(string[] options)
        {

        }
    }
}
