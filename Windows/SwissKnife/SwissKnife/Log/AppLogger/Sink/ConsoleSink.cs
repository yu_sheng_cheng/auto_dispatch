﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SwissKnife.Log.AppLogger.Sink
{
    sealed class ConsoleSink : ILogSink
    {
        private bool _multiColorEnabled;
        private ConsoleColor[] _logColors;
        private StringBuilder _stringBuilder;

        public void Dispose()
        {

        }

        public void Log(IEnumerable<LogEntry> logs)
        {
            if (_multiColorEnabled)
            {
                foreach (var log in logs)
                {
                    Console.ForegroundColor = _logColors[(int)log.Level];
                    Console.WriteLine("{0}", log.ToString());
                }
            }
            else
            {
                foreach (var log in logs)
                {
                    _stringBuilder.AppendLine(log.ToString());
                }

                if (_stringBuilder.Length == 0)
                    return;

                Console.Write("{0}", _stringBuilder.ToString());

                _stringBuilder.Clear();
            }
        }

        public void Configure(XmlElement xmlRootElement)
        {
            LogLevel level = Logger.ParseLogLevel(xmlRootElement.GetAttribute("Level"));

            if (level != LogLevel.None)
                ShowConsoleWindow();

            bool enabled;

            if (bool.TryParse(xmlRootElement.GetAttribute("IsMultiColorEnabled"), out enabled) && enabled)
            {
                _multiColorEnabled = true;

                _logColors = new ConsoleColor[(int)LogLevel.None];

                for (int i = 0; i < (int)LogLevel.None; ++i)
                {
                    _logColors[i] = ConsoleColor.White;
                }

                foreach (XmlNode xmlNode in xmlRootElement.ChildNodes)
                {
                    XmlElement xmlElement = xmlNode as XmlElement;
                    if (xmlElement == null)
                        continue;

                    level = Logger.ParseLogLevel(xmlNode.Name);
                    if (level == LogLevel.None)
                        continue;

                    _logColors[(int)level] = ParseConsoleColor(xmlElement.GetAttribute("Value"));
                }
            }
            else
                _stringBuilder = new StringBuilder();
        }

        private ConsoleColor ParseConsoleColor(string value)
        {
            switch (value)
            {
                case "Black":
                    return ConsoleColor.Black;
                case "Blue":
                    return ConsoleColor.Blue;
                case "Cyan":
                    return ConsoleColor.Cyan;
                case "DarkBlue":
                    return ConsoleColor.DarkBlue;
                case "DarkCyan":
                    return ConsoleColor.DarkCyan;
                case "DarkGray":
                    return ConsoleColor.DarkGray;
                case "DarkGreen":
                    return ConsoleColor.DarkGreen;
                case "DarkMagenta":
                    return ConsoleColor.DarkMagenta;
                case "DarkRed":
                    return ConsoleColor.DarkRed;
                case "DarkYellow":
                    return ConsoleColor.DarkYellow;
                case "Gray":
                    return ConsoleColor.Gray;
                case "Green":
                    return ConsoleColor.Green;
                case "Magenta":
                    return ConsoleColor.Magenta;
                case "Red":
                    return ConsoleColor.Red;
                case "White":
                    return ConsoleColor.White;
                case "Yellow":
                    return ConsoleColor.Yellow;
                default:
                    return ConsoleColor.White;
            }
        }

        public void Change(string[] options)
        {
            foreach (var opt in options)
            {
                if (string.CompareOrdinal(opt, 0, "Level=", 0, 6) == 0)
                {
                    LogLevel level = Logger.ParseLogLevel(opt.Substring(6));

                    if (level != LogLevel.None)
                        ShowConsoleWindow();
                }
            }
        }

        private void ShowConsoleWindow()
        {
            bool hasConsole = GetConsoleWindow() != IntPtr.Zero;

            if (!hasConsole)
            {
                if (AllocConsole())
                {
                    Type type = typeof(Console);
                    type.GetField("_out", BindingFlags.Static | BindingFlags.NonPublic).SetValue(null, null);
                    type.GetField("_error", BindingFlags.Static | BindingFlags.NonPublic).SetValue(null, null);
                    type.GetMethod("InitializeStdOutError", BindingFlags.Static | BindingFlags.NonPublic).Invoke(null, new object[] { true });
                }
            }
        }

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("kernel32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool AllocConsole();
    }
}
