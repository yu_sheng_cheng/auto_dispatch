﻿using SwissKnife.Queues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace SwissKnife.Log.AppLogger
{
    public sealed partial class Logger : ILog, IDisposable
    {
        public static LogLevel ParseLogLevel(string value)
        {
            LogLevel level;

            return EnumUtils.TryParse(value, out level) ? level : LogLevel.None;
        }

        public const string GUID = "9554daa3-04db-4ab8-96e2-b2bff52d2c4f";
        private const string LOG_KEYWORD = "Logger";

        private readonly string _name;
        private readonly string _dateTimePattern = "yyyy-MM-dd HH:mm:ss.fff";
        private readonly int _lazyWriteTime = 100;
        private readonly List<LogSink> _sinks = new List<LogSink>();
        private LogLevel _lowestLevel = LogLevel.None;
        private readonly OneReadManyWriteQueue<LogEntry> _queue = new OneReadManyWriteQueue<LogEntry>();
        private readonly Timer _timer;
        private int _timerStarted;
        private int _timerTicks;
        private bool _disposed;

        public Logger(string xmlFile) : this(string.Empty, xmlFile)
        {

        }

        public Logger(string name, string xmlFile)
        {
            if (name == null)
                throw new ArgumentNullException("name");
            if (xmlFile == null)
                throw new ArgumentNullException("xmlFile");

            _name = name.Trim();

            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load(xmlFile);

            XmlNode xmlRootNode = xmlDoc.SelectSingleNode("AppLogger");
            XmlElement xmlRootElement = xmlRootNode as XmlElement;

            if (xmlRootElement != null)
            {
                string dateTimePattern = xmlRootElement.GetAttribute("DateTimePattern");
                if (!string.IsNullOrEmpty(dateTimePattern))
                    _dateTimePattern = dateTimePattern;

                int lazyWriteTime;

                if (int.TryParse(xmlRootElement.GetAttribute("LazyWriteTime"), out lazyWriteTime))
                {
                    if (lazyWriteTime >= 0)
                        _lazyWriteTime = lazyWriteTime;
                }
            }

            try
            {
                foreach (XmlNode xmlNode in xmlRootNode.ChildNodes)
                {
                    if (xmlNode.Name == "Sink")
                    {
                        XmlElement xmlElement = xmlNode as XmlElement;
                        if (xmlElement == null)
                            continue;

                        string sinkName = xmlElement.GetAttribute("Name");
                        if (string.IsNullOrEmpty(sinkName) || GetSink(sinkName) != null)
                            continue;

                        Type type = Type.GetType(xmlElement.GetAttribute("Type"));
                        if (type == null)
                            continue;

                        var constructor = type.GetConstructor(new Type[0]);
                        if (constructor == null)
                            continue;

                        var obj = constructor.Invoke(null);

                        var sink = obj as ILogSink;
                        if (sink == null)
                        {
                            if (obj is IDisposable disposableObject)
                                disposableObject.Dispose();
                            continue;
                        }

                        sink.Configure(xmlElement);

                        LogLevel level = ParseLogLevel(xmlElement.GetAttribute("Level"));

                        _sinks.Add(new LogSink(sink, sinkName, level));

                        if (level < _lowestLevel)
                            _lowestLevel = level;
                    }
                }

                _timer = new Timer(Timer_Expired, null, Timeout.Infinite, Timeout.Infinite);
            }
            catch
            {
                foreach (var sink in _sinks)
                {
                    sink.Dispose();
                }
                throw;
            }

            StartServer();
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _disposed = true;

            using (var completionEvent = new ManualResetEvent(false))
            {
                if (_timer.Dispose(completionEvent))
                    completionEvent.WaitOne();
            }
            StopServer();

            Thread.MemoryBarrier();

            Flush();
            foreach (var sink in _sinks)
            {
                sink.Dispose();
            }
        }

        public ILogSink GetSink(string name)
        {
            if (_disposed)
                throw new ObjectDisposedException(GetType().FullName);

            foreach (var sink in _sinks)
            {
                if (name == sink.Name)
                {
                    return sink.Sink;
                }
            }

            return null;
        }

        public void d(string message)
        {
            Log(LogLevel.DEBUG, string.Empty, message, null);
        }

        public void d(string keyword, string message)
        {
            Log(LogLevel.DEBUG, keyword, message, null);
        }

        public void i(string message)
        {
            Log(LogLevel.INFO, string.Empty, message, null);
        }

        public void i(string keyword, string message)
        {
            Log(LogLevel.INFO, keyword, message, null);
        }

        public void w(string message)
        {
            Log(LogLevel.WARNING, string.Empty, message, null);
        }

        public void w(string keyword, string message)
        {
            Log(LogLevel.WARNING, keyword, message, null);
        }

        public void w(string message, Exception ex)
        {
            Log(LogLevel.WARNING, string.Empty, message, ex);
        }

        public void w(string keyword, string message, Exception ex)
        {
            Log(LogLevel.WARNING, keyword, message, ex);
        }

        public void e(string message)
        {
            Log(LogLevel.ERROR, string.Empty, message, null);
        }

        public void e(string keyword, string message)
        {
            Log(LogLevel.ERROR, keyword, message, null);
        }

        public void e(string message, Exception ex)
        {
            Log(LogLevel.ERROR, string.Empty, message, ex);
        }

        public void e(string keyword, string message, Exception ex)
        {
            Log(LogLevel.ERROR, keyword, message, ex);
        }

        public void f(string message)
        {
            Log(LogLevel.FATAL, string.Empty, message, null);
        }

        public void f(string keyword, string message)
        {
            Log(LogLevel.FATAL, keyword, message, null);
        }

        public void f(string message, Exception ex)
        {
            Log(LogLevel.FATAL, string.Empty, message, ex);
        }

        public void f(string keyword, string message, Exception ex)
        {
            Log(LogLevel.FATAL, keyword, message, ex);
        }

        private void Log(LogLevel level, string keyword, string message, Exception exception)
        {
            if (_disposed | level < _lowestLevel)
                return;

            if (message == null)
                message = string.Empty;

            _queue.Enqueue(new LogEntry(_name, _dateTimePattern, DateTime.Now, keyword, level, message, exception));

            if (Interlocked.Exchange(ref _timerStarted, 1) == 0)
            {
                try { _timer.Change(_lazyWriteTime, Timeout.Infinite); } catch { }
            }      
        }

        private void Timer_Expired(object state)
        {
            Interlocked.Exchange(ref _timerStarted, 0);

            if (Interlocked.Increment(ref _timerTicks) != 1)
                return;

            while (true)
            {
                Flush();

                if (Interlocked.CompareExchange(ref _timerTicks, 0, 1) == 1)
                    return;
                Interlocked.Exchange(ref _timerTicks, 1);
            }
        }

        private void Flush()
        {
            LogEntry log;

            while ((log = _queue.Dequeue()) != null)
            {
                LogLevel level = log.Level;

                foreach (var sink in _sinks)
                {
                    if (level >= sink.Level)
                    {
                        sink.Queue.Add(log);
                    }
                }
            }

            foreach (var sink in _sinks)
            {
                if (sink.Queue.Count > 0)
                {
                    sink.Log(sink.Queue);
                    sink.Queue.Clear();
                }
            }
        }
    }
}
