﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SwissKnife.Log.AppLogger
{
    public interface ILogSink : IDisposable
    {
        void Log(IEnumerable<LogEntry> logs);

        void Configure(XmlElement xmlElement);

        void Change(string[] options);
    }
}
