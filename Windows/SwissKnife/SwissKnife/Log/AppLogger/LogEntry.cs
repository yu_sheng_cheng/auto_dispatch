﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife.Log.AppLogger
{
    [Serializable]
    public sealed class LogEntry
    {
        private readonly string _dateTimePattern;

        public string LoggerName { get; }

        public DateTime OccurTime { get; }

        public string Keyword { get; }

        public LogLevel Level { get; }

        public string Message { get; }

        public Exception Exception { get; }

        public LogEntry(string loggerName, string dateTimePattern, DateTime occurTime, string keyword, LogLevel level, string message) :
            this(loggerName, dateTimePattern, occurTime, keyword, level, message, null)
        {

        }

        public LogEntry(string loggerName, string dateTimePattern, DateTime occurTime, string keyword, LogLevel level, string message, Exception exception)
        {
            LoggerName = loggerName;
            _dateTimePattern = dateTimePattern;
            OccurTime = occurTime;
            Keyword = keyword;
            Level = level;
            Message = message;
            Exception = exception;
        }

        public override string ToString()
        {
            string name = string.IsNullOrWhiteSpace(LoggerName) ? " " : $" {LoggerName} ";
            string message = Exception == null ? Message : $"{Message}{Environment.NewLine}{Exception}";

            return string.IsNullOrWhiteSpace(Keyword) ?
                $"{OccurTime.ToString(_dateTimePattern, CultureInfo.InvariantCulture)}{name}<{Level.ToString()}> {message}" :
                $"{OccurTime.ToString(_dateTimePattern, CultureInfo.InvariantCulture)}{name}[{Keyword}] <{Level.ToString()}> {message}";
        }
    }
}
