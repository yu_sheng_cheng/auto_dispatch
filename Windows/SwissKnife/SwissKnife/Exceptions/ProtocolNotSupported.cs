﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife.Exceptions
{
    [Serializable()]
    public class ProtocolNotSupported : Exception
    {
        public ProtocolNotSupported() : base() { }
        public ProtocolNotSupported(string message) : base(message) { }
        public ProtocolNotSupported(string message, Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client. 
        protected ProtocolNotSupported(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
