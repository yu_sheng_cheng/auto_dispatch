﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife.Exceptions
{
    [Serializable()]
    public class BugException : Exception
    {
        public BugException() : base() { }
        public BugException(string message) : base(message) { }
        public BugException(string message, Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client. 
        protected BugException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
