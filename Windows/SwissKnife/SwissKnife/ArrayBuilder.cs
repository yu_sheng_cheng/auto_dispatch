﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife
{
    public class ArrayBuilder<T> : IEnumerable<T>
    {
        private const int _defaultCapacity = 4;

        private static readonly T[] _emptyArray = new T[0];
        private int _size;

        public T[] Buffer { get; private set; }

        public int Capacity
        {
            get { return Buffer.Length; }
            set
            {
                if (value < _size)
                    throw new ArgumentOutOfRangeException("Capacity");

                if (value != Buffer.Length)
                {
                    if (value == 0)
                        Buffer = _emptyArray;
                    else
                    {
                        T[] newBuffer = new T[value];
                        if (_size > 0)
                            Array.Copy(Buffer, 0, newBuffer, 0, _size);
                        Buffer = newBuffer;
                    }
                }
            }
        }

        public int Length
        {
            get { return _size; }
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException("Length");

                EnsureCapacity(value);

                if (value < _size)
                    Array.Clear(Buffer, value, _size - value);

                _size = value;
            }
        }

        public T this[int index]
        {
            get
            {
                // Following trick can reduce the range check by one
                if (unchecked((uint)index) >= (uint)_size)
                    throw new ArgumentOutOfRangeException("index");
                return Buffer[index];
            }
            set
            {
                int needCapacity = unchecked(index + 1);

                if (needCapacity <= 0)
                    throw new ArgumentOutOfRangeException("index");

                if (needCapacity > _size)
                {
                    if (needCapacity > Buffer.Length)
                        GrowBuffer(needCapacity);
                    _size = needCapacity;
                }

                Buffer[index] = value;
            }
        }

        public ArrayBuilder()
        {
            Buffer = _emptyArray;
        }

        public ArrayBuilder(int capacity)
        {
            if (capacity < 0)
                throw new ArgumentOutOfRangeException("capacity");

            if (capacity == 0)
                Buffer = _emptyArray;
            else
                Buffer = new T[capacity];
        }

        public void Append(T[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            Insert(_size, data, 0, data.Length);
        }

        public void Append(T[] data, int offset, int count)
        {
            Insert(_size, data, offset, count);
        }

        public void Insert(int index, T[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data");

            Insert(index, data, 0, data.Length);
        }

        public void Insert(int index, T[] data, int offset, int count)
        {
            if (index < 0)
                throw new ArgumentOutOfRangeException("index");
            if (data == null)
                throw new ArgumentNullException("data");
            if (count < 0)
                throw new ArgumentOutOfRangeException("count");
            if (offset < 0)
                throw new ArgumentOutOfRangeException("offset");
            if (count > data.Length - offset)
                throw new ArgumentException("The sum of the offset and count arguments must be less than or equal to the collection's Count.");
            if (count > int.MaxValue - index)
                throw new OutOfMemoryException();

            if (count == 0)
                return;

            int needCapacity = index + count;
            EnsureCapacity(needCapacity);
            Array.Copy(data, offset, Buffer, index, count);
            if (needCapacity > _size)
                _size = needCapacity;
        }

        public void EnsureCapacity(int min)
        {
            if (Buffer.Length < min)
                GrowBuffer(min);
        }

        private void GrowBuffer(int min)
        {
            int newCapacity = Buffer.Length == 0 ? _defaultCapacity : Buffer.Length * 2;
            if (unchecked((uint)newCapacity) > int.MaxValue)
                newCapacity = int.MaxValue;
            if (newCapacity < min)
                newCapacity = min;
            Capacity = newCapacity;
        }

        public void Clear()
        {
            if (_size > 0)
            {
                Array.Clear(Buffer, 0, _size);
                _size = 0;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            int size = _size;

            for (int i = 0; i < size; ++i)
            {
                yield return Buffer[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
