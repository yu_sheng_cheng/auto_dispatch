﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwissKnife
{
    public static class OperationResultFactory
    {
        public static OperationResult CreateSuccessfulResult() { return new OperationResult(); }

        public static OperationResult CreateSuccessfulResult(string message) { return new OperationResult(message); }

        public static OperationResult<T> CreateSuccessfulResult<T>(T value)
        {
            return new OperationResult<T>() { Value = value };
        }

        public static OperationResult<T> CreateSuccessfulResult<T>(T value, string message)
        {
            return new OperationResult<T>(message) { Value = value };
        }

        public static OperationResult<T1, T2> CreateSuccessfulResult<T1, T2>(T1 value1, T2 value2)
        {
            return new OperationResult<T1, T2>() { Value1 = value1, Value2 = value2 };
        }

        public static OperationResult<T1, T2> CreateSuccessfulResult<T1, T2>(T1 value1, T2 value2, string message)
        {
            return new OperationResult<T1, T2>(message) { Value1 = value1, Value2 = value2 };
        }

        public static OperationResult CreateFailedResult(int errorCode, int lowLevelErrorCode = OperationResult.INVALID_LOW_LEVEL_ERROR_CODE)
        {
            return new OperationResult(errorCode, lowLevelErrorCode);
        }

        public static OperationResult CreateFailedResult(string errorSource, int errorCode, int lowLevelErrorCode = OperationResult.INVALID_LOW_LEVEL_ERROR_CODE)
        {
            return new OperationResult(errorSource, errorCode, lowLevelErrorCode);
        }

        public static OperationResult CreateFailedResult(string errorSource, int errorCode, string message)
        {
            return new OperationResult(errorSource, errorCode, OperationResult.INVALID_LOW_LEVEL_ERROR_CODE, message);
        }

        public static OperationResult CreateFailedResult(string errorSource, int errorCode, int lowLevelErrorCode, string message)
        {
            return new OperationResult(errorSource, errorCode, lowLevelErrorCode, message);
        }

        public static OperationResult CreateFailedResult(Exception exception)
        {
            return new OperationResult(exception);
        }

        public static OperationResult CreateFailedResult(string errorSource, Exception exception)
        {
            return new OperationResult(errorSource, exception);
        }

        public static OperationResult CreateFailedResult(string errorSource, string message, Exception exception)
        {
            return new OperationResult(errorSource, message, exception);
        }

        public static OperationResult CreateFailedResult(OperationResult innerFailedResult)
        {
            if (innerFailedResult == null)
                throw new ArgumentNullException("innerFailedResult");

            return innerFailedResult.Exception == null ?
                new OperationResult(innerFailedResult.ErrorSource, innerFailedResult.ErrorCode, innerFailedResult.LowLevelErrorCode, innerFailedResult.Message) :
                new OperationResult(innerFailedResult.ErrorSource, innerFailedResult.Message, innerFailedResult.Exception);
        }

        public static OperationResult<T> CreateFailedResult<T>(int errorCode, int lowLevelErrorCode = OperationResult.INVALID_LOW_LEVEL_ERROR_CODE)
        {
            return new OperationResult<T>(errorCode, lowLevelErrorCode);
        }

        public static OperationResult<T> CreateFailedResult<T>(string errorSource, int errorCode, int lowLevelErrorCode = OperationResult.INVALID_LOW_LEVEL_ERROR_CODE)
        {
            return new OperationResult<T>(errorSource, errorCode, lowLevelErrorCode);
        }

        public static OperationResult<T> CreateFailedResult<T>(string errorSource, int errorCode, string message)
        {
            return new OperationResult<T>(errorSource, errorCode, message);
        }

        public static OperationResult<T> CreateFailedResult<T>(string errorSource, int errorCode, int lowLevelErrorCode, string message)
        {
            return new OperationResult<T>(errorSource, errorCode, lowLevelErrorCode, message);
        }

        public static OperationResult<T> CreateFailedResult<T>(Exception exception)
        {
            return new OperationResult<T>(exception);
        }

        public static OperationResult<T> CreateFailedResult<T>(string errorSource, Exception exception)
        {
            return new OperationResult<T>(errorSource, exception);
        }

        public static OperationResult<T> CreateFailedResult<T>(string errorSource, string message, Exception exception)
        {
            return new OperationResult<T>(errorSource, message, exception);
        }

        public static OperationResult<T> CreateFailedResult<T>(OperationResult innerFailedResult)
        {
            if (innerFailedResult == null)
                throw new ArgumentNullException("innerFailedResult");

            return innerFailedResult.Exception == null ?
                new OperationResult<T>(innerFailedResult.ErrorSource, innerFailedResult.ErrorCode, innerFailedResult.LowLevelErrorCode, innerFailedResult.Message) :
                new OperationResult<T>(innerFailedResult.ErrorSource, innerFailedResult.Message, innerFailedResult.Exception);
        }

        public static OperationResult<T1, T2> CreateFailedResult<T1, T2>(int errorCode, int lowLevelErrorCode = OperationResult.INVALID_LOW_LEVEL_ERROR_CODE)
        {
            return new OperationResult<T1, T2>(errorCode, lowLevelErrorCode);
        }

        public static OperationResult<T1, T2> CreateFailedResult<T1, T2>(string errorSource, int errorCode, int lowLevelErrorCode = OperationResult.INVALID_LOW_LEVEL_ERROR_CODE)
        {
            return new OperationResult<T1, T2>(errorSource, errorCode, lowLevelErrorCode);
        }

        public static OperationResult<T1, T2> CreateFailedResult<T1, T2>(string errorSource, int errorCode, string message)
        {
            return new OperationResult<T1, T2>(errorSource, errorCode, message);
        }

        public static OperationResult<T1, T2> CreateFailedResult<T1, T2>(string errorSource, int errorCode, int lowLevelErrorCode, string message)
        {
            return new OperationResult<T1, T2>(errorSource, errorCode, lowLevelErrorCode, message);
        }

        public static OperationResult<T1, T2> CreateFailedResult<T1, T2>(Exception exception)
        {
            return new OperationResult<T1, T2>(exception);
        }

        public static OperationResult<T1, T2> CreateFailedResult<T1, T2>(string errorSource, Exception exception)
        {
            return new OperationResult<T1, T2>(errorSource, exception);
        }

        public static OperationResult<T1, T2> CreateFailedResult<T1, T2>(string errorSource, string message, Exception exception)
        {
            return new OperationResult<T1, T2>(errorSource, message, exception);
        }

        public static OperationResult<T1, T2> CreateFailedResult<T1, T2>(OperationResult innerFailedResult)
        {
            if (innerFailedResult == null)
                throw new ArgumentNullException("innerFailedResult");

            return innerFailedResult.Exception == null ?
                new OperationResult<T1, T2>(innerFailedResult.ErrorSource, innerFailedResult.ErrorCode, innerFailedResult.LowLevelErrorCode, innerFailedResult.Message) :
                new OperationResult<T1, T2>(innerFailedResult.ErrorSource, innerFailedResult.Message, innerFailedResult.Exception);
        }
    }
}
