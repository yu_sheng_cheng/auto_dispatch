﻿using SwissKnife.Log.AppLogger;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SwissKnifeTool
{
    /// <summary>
    /// AppLoggerWindow.xaml 的互動邏輯
    /// </summary>
    public partial class AppLoggerWindow : Window
    {
        public AppLoggerWindow()
        {
            InitializeComponent();

            var processes = Process.GetProcesses().ToList();
            processes.Sort((x, y) => x.ProcessName.CompareTo(y.ProcessName));

            foreach (var process in processes)
            {
                ProcessComboBox.Items.Add($"{process.Id.ToString()} - {process.ProcessName}");
            }

            SinkNameComboBox.SelectedIndex = 0;
            LogLevelComboBox.SelectedIndex = 0;
        }

        private void ProcessComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ChangeButton.IsEnabled = ProcessComboBox.SelectedIndex != -1;
        }

        private void SinkNameComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxItem item = SinkNameComboBox.SelectedItem as ComboBoxItem;

            OtherSinkSettingsStackPanel.Visibility = item.Content.ToString() == "Other" ? Visibility.Visible : Visibility.Collapsed;
        }

        private void ChangeButton_Click(object sender, RoutedEventArgs e)
        {
            string pid = Regex.Match(ProcessComboBox.Text, @"^(?<PID>[0-9]+)").Groups["PID"].Value;
            NamedPipeClientStream client = null;

            try
            {
                byte[] buffer;

                switch (SinkNameComboBox.Text)
                {
                    case "Other":
                        buffer = string.IsNullOrEmpty(OtherSinkSettingsTextBox.Text) ?
                            Encoding.ASCII.GetBytes($"Name={SinkNameComboBox.Text}&Level={LogLevelComboBox.Text}") :
                            Encoding.ASCII.GetBytes($"Name={SinkNameComboBox.Text}&Level={LogLevelComboBox.Text}&{OtherSinkSettingsTextBox.Text}");
                        break;
                    default:
                        buffer = Encoding.ASCII.GetBytes($"Name={SinkNameComboBox.Text}&Level={LogLevelComboBox.Text}");
                        break;
                }

                client = new NamedPipeClientStream(".", $"{Logger.GUID.ToString()}.{LoggerNameTextBox.Text}.{pid}", PipeDirection.Out, PipeOptions.None);
                client.Connect(2000);
                client.Write(buffer, 0, buffer.Length);
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.ToString(), Title, MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            finally
            {
                if (client != null)
                {
                    client.Dispose();
                }
            }
        }
    }
}
