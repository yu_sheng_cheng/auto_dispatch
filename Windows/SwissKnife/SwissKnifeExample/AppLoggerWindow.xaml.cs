﻿using SwissKnife.Log.AppLogger;
using SwissKnife.Log.AppLogger.Sink;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace SwissKnifeExample
{
    /// <summary>
    /// AppLoggerWindow.xaml 的互動邏輯
    /// </summary>
    public partial class AppLoggerWindow : Window
    {
        private readonly Logger _log = new Logger(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Applogger.xml"));
        private readonly EventSink _logEventSink;
        private readonly DispatcherTimer _timer;

        public ObservableCollection<LogEntry> Logs { get; } = new ObservableCollection<LogEntry>();

        public AppLoggerWindow()
        {
            InitializeComponent();

            LogListView.DataContext = this;

            _logEventSink = _log.GetSink("Event") as EventSink;
            if (_logEventSink != null)
            {
                _logEventSink.LogReceived += LogReceived;
            }

            _timer = new DispatcherTimer();
            _timer.Tick += new EventHandler(DispatcherTimer_Tick);
            _timer.Interval = new TimeSpan(0, 0, 0, 0, 30);
            _timer.Start();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            _timer.Stop();
            _log.i("AppLogger Demo Bye ...");

            if (_logEventSink != null)
            {
                _logEventSink.LogReceived -= LogReceived;
            }

            _log.Dispose();
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            _log.d("Debug Log");
            _log.i("Information Log");
            _log.w("Warning Log");
            _log.e("Error Log");
            _log.f("Fatal Log");
        }

        private void LogReceived(object sender, LogReceivedEventArgs e)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                (ThreadStart)delegate ()
                {
                    foreach (var log in e.Logs)
                    {
                        Logs.Insert(0, log);
                    }
                });
        }
    }
}
